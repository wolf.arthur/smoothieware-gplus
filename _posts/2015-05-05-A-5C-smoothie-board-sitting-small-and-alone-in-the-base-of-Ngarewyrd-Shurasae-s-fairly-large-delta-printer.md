---
layout: post
title: "A 5C smoothie board sitting small and alone in the base of Ngarewyrd Shurasae 's fairly large delta printer.."
date: May 05, 2015 13:09
category: "General discussion"
author: "Electra Flarefire"
---
A 5C smoothie board sitting small and alone in the base of **+Ngarewyrd Shurasae**'s  fairly large delta printer.. The 400 step motors hemming it in on three sides and an e3d volcano soon to be lurking far overhead waiting in readiness for spool upon spool of thermoplastics.. 

![missing image](https://lh4.googleusercontent.com/-tRk4u_YZQb8/VUjAj42Qz-I/AAAAAAAArMk/WHpVj3avRZs/s0/IMG_20150505_225148.jpg)



**"Electra Flarefire"**

---
---
**Electra Flarefire** *May 05, 2015 13:53*

**+Albert Latham** at this stage. Mostly tests. The printed is being built as an experiment and a bit of fun.

Material choices are mostly based on price/performance and availability(frame size is based on material lengths.

For example, we have already uncovered significant flex/stability issues with the frame. This will be solved by cladding two sides in 6mm polycarbonate sheeting.

And we have already moved away from ball bearings + magnets due to mounting and potential wear issues for the Cherry Pi III 'tensioned string' ball and socket, approach. Using delrin/POM printed sockets.


---
**Albert Latham** *May 05, 2015 13:58*

**+Electra Flarefire** You may consider redesigning the plastic parts that joint the frame at each corner. By adding relatively large gussets vertically you could potentially eliminate the majority of the flex. Though, I see the benefit of adding sides, as well. Containing some heat will make for better prints.


---
**Electra Flarefire** *May 05, 2015 14:02*

**+Albert Latham** We have the polycarb from another project and were planing on adding it anyway to create a heated build chamber complete with heater.

The smoothboard has extra IO for things like this.


---
*Imported from [Google+](https://plus.google.com/+ElectraFlarefire/posts/VwLGFiwVWeZ) &mdash; content and formatting may not be reliable*
