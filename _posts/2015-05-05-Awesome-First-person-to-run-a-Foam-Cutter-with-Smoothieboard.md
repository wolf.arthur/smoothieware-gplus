---
layout: post
title: "Awesome ! First person to run a Foam Cutter with Smoothieboard !"
date: May 05, 2015 18:08
category: "General discussion"
author: "Arthur Wolf"
---
Awesome ! First person to run a Foam Cutter with Smoothieboard ! Been saying it'd be easy to do for a long time, and this guy did it :)﻿





**"Arthur Wolf"**

---
---
**Brian Halkett** *May 05, 2015 19:49*

Nice a foam cutter is on my to do list.  Is there a link were I can get more details on this?


---
**Arthur Wolf** *May 05, 2015 19:51*

**+Brian Halkett** No link yet, but he says he'll make a blog post about it. And I'll make a wiki guide from that.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/UkJmzV7ktTP) &mdash; content and formatting may not be reliable*
