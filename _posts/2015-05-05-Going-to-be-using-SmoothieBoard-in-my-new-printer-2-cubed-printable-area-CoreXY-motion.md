---
layout: post
title: "Going to be using SmoothieBoard in my new printer, 2' cubed printable area, CoreXY motion"
date: May 05, 2015 16:30
category: "General discussion"
author: "Chris Purola (Chorca)"
---
Going to be using SmoothieBoard in my new printer, 2' cubed printable area, CoreXY motion.



![missing image](https://lh3.googleusercontent.com/-oZP4DZNZOdI/VUjwEDd5rpI/AAAAAAAAGlw/uzGWRZewHiI/s0/15%252B-%252B1.jpeg)
![missing image](https://lh3.googleusercontent.com/-8A9VMUOpKF8/VUjwEJucNEI/AAAAAAAAGlw/D4ON6Q4myoo/s0/15%252B-%252B3.jpeg)
![missing image](https://lh3.googleusercontent.com/-UhfnmZGM7kg/VUjwEFMOZxI/AAAAAAAAGlw/9avTbOnU-JU/s0/15%252B-%252B2.jpeg)

**"Chris Purola (Chorca)"**

---
---
**Arthur Wolf** *May 05, 2015 16:31*

What kind of extruder will you be putting on there ?


---
**Chris Purola (Chorca)** *May 05, 2015 16:34*

Hoping to use a dual-bowden setup with an E3D Chimera. Barring that, an E3D V6 with some sort of bowden extruder, haven't decided yet.


---
**Arthur Wolf** *May 05, 2015 16:35*

Can't wait to see it moving :)


---
**ThantiK** *May 05, 2015 17:46*

I've actually got some ideas I've been bouncing around with regards to cheapening 3D printer production, and I think it's going to end up being CoreXY as well.  Can't wait to see this thing in action man, it's been too long since I've got to work on my own printers.


---
**Arthur Wolf** *May 05, 2015 17:51*

**+ThantiK** I talk to a lot of people ( when they need some Smoothie help ) working on new printer designs, and CoreXY is definitely the top design people choose now. Deltas make pretty much all of the rest of the population in that sampling :)


---
**Whosa whatsis** *May 05, 2015 22:41*

**+Arthur Wolf** in fairness, most people doing straight-up discrete cartesian bots are more likely to use the lower-powered boards, and even if they use smoothie, the configuration of those systems is much more straightforward, so they're less likely to need help.


---
**Arthur Wolf** *May 05, 2015 22:44*

**+Whosa whatsis** Yeah the sampling might be highly biased :) I was at makerfaire paris this WE, lots of corexy amongst the new "for sale" machines there though ( 2 corexy, 1 hbot, 1 ultimaker-type, no delta, no mendel-type ). But again, maybe a biased sample, they had Smoothieboards too :)


---
**Wolfmanjm** *May 05, 2015 22:51*

having built and rebuilt a large corexy/hbot several times they are definitely easier to get the XY part done. The Z however is a whole other issue. However it is my main goto printer for large things. It currently has a 1mm volcano on it and can print large things relatively fast.


---
*Imported from [Google+](https://plus.google.com/+ChrisPurola/posts/3Qs6T3iMy7E) &mdash; content and formatting may not be reliable*
