---
layout: post
title: "Hi! Is there any news about Smoothieboard 2.0?"
date: May 05, 2015 13:12
category: "General discussion"
author: "Maxim Mluhov"
---
Hi!

Is there any news about Smoothieboard 2.0? Detailed specifications? Plans? Beta-versions? When it will be released? Thank you!





**"Maxim Mluhov"**

---
---
**Arthur Wolf** *May 05, 2015 13:13*

Mark just finished the design of the prototype, we'll be making a few very soon.

We don't give any timelines for it, or people will be upset when we don't meet it.


---
*Imported from [Google+](https://plus.google.com/+MaximMluhov/posts/3hchX33sJmq) &mdash; content and formatting may not be reliable*
