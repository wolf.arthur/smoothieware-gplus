---
layout: post
title: "Again about Smoothieboard 2.0 Pro: -will be Edison included to some versions of kit?"
date: May 06, 2015 10:37
category: "General discussion"
author: "Artem Grunichev"
---
Again about Smoothieboard 2.0 Pro:

-will be Edison included to some versions of kit?

-own Edison software, or premade images based on OctoPrint+Cura or RepetierServer, or user should do that? If third, I'll be glad to do second :)

-any help needed?

-do you know about Intel Quark hiding inside of Edison? In future it could be used to build board without any ARM chips, just with Edison.





**"Artem Grunichev"**

---
---
**Arthur Wolf** *May 06, 2015 10:43*

Hey !



* About the Edison, I don't see why we couldn't make a version with the Edison included, that sounds useful



* About a pre-made Edison image, we definitely want that, I kind of expect the community will work on something like that, we will otherwise ( Will most probably be Octoprint + Cura/Slic3r. Mousse ( the new Smoothie web interface ) would also be included once we have that running.



* Yes, we will need help, as soon as we have a beta board for people to test with.



* About the quark, I don't know enough about it, I'm not sure it's powerful enough. If it is, it'd probably be a project separate from Smoothie. Have not thought about it much.



Cheers :)


---
**Artem Grunichev** *May 06, 2015 11:44*

**+Arthur Wolf** I'll be happy to help with beta testing. I have Edison with Cura and OctoPrint installed and configured at my Rostock MAX


---
*Imported from [Google+](https://plus.google.com/101033487868604831015/posts/7cwAU5dZmU3) &mdash; content and formatting may not be reliable*
