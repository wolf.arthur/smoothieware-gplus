---
layout: post
title: "Time is ticking fast: Could we have a little update on the contest ?"
date: May 08, 2015 14:25
category: "General discussion"
author: "Stephane Buisson"
---
Time is ticking fast:

Could we have a little update on the contest ?



I am particularly interested to know if **+Thomas Oster**  could have a board to develop a driver for his Visicut lasercutter software.



Unfortunately, my previous requests (mail or K40 comunity) stay without answer,

Come on **+Arthur Wolf**  deliver us some good news.





**"Stephane Buisson"**

---
---
**Arthur Wolf** *May 08, 2015 14:27*

**+Stephane Buisson** We've had many more entries than we anticipated. Thomas is amongst those that I'm sure will win, but in fairness to everybody, I must process everybody's entry properly, and it's taking a lot of time.


---
**Stephane Buisson** *May 08, 2015 14:33*

I have no clue about your workload, do you have any rought idea when you could see the end of the tunel ?


---
**Arthur Wolf** *May 08, 2015 14:35*

**+Stephane Buisson** I keep being close to restarting work on the contest, then something else comes up. I really want to get things going, it'll be asap, I can't say anything more than that.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/h6uCkmrh8R2) &mdash; content and formatting may not be reliable*
