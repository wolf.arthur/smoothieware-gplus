---
layout: post
title: "Now running Smoothieboard 4XC .. speed tests soon .."
date: May 10, 2015 10:11
category: "General discussion"
author: "David Bassetti"
---
Now running Smoothieboard 4XC .. speed tests soon ..

OpenBuilds and Smoothie..  truely a great combination

more at [www.3d-seed.com](http://www.3d-seed.com)

![missing image](https://lh3.googleusercontent.com/-0vZ3f8bpyvA/VU8urH7fBbI/AAAAAAAABh4/zTaPyIlv5Yc/s0/IMG_2901.JPG)



**"David Bassetti"**

---
---
**Arthur Wolf** *May 10, 2015 10:15*

Nice looking machine :) Can't wait for the speed tests, and print quality results.


---
**David Bassetti** *May 10, 2015 10:20*

Oh Arthur,  the prints are really superb.. we used simplify3d as well..but now 0.1mm is no problem at all :)


---
*Imported from [Google+](https://plus.google.com/+DavidBassetti/posts/JWq4X6n24Ks) &mdash; content and formatting may not be reliable*
