---
layout: post
title: "PCB ready for first Prototype of Smoothieboard v2 Pro"
date: May 12, 2015 13:31
category: "General discussion"
author: "Arthur Wolf"
---
PCB ready for first Prototype of Smoothieboard v2 Pro.





**"Arthur Wolf"**

---
---
**Artem Grunichev** *May 12, 2015 13:34*

Looks amazing!


---
**David Piwczyk** *May 12, 2015 13:44*

How much more expensive will the final assembled board be compared to smoothie1 in a similar configuration (without the edison)?


---
**Arthur Wolf** *May 12, 2015 13:50*

**+David Piwczyk** This is the Smoothie 2 pro. The smoothie 2 will be about the same price as the smoothie 1. The smoothie 2 pro will be $200+.


---
**Jonathan Lussier** *May 12, 2015 14:03*

Awesome Arthur, looking forward to this one...


---
**Jonathan Lussier** *May 12, 2015 14:08*

4 Servo outputs, nice!  No RS485 or CAN, right?


---
**Arthur Wolf** *May 12, 2015 14:10*

**+Jonathan Lussier** No, will require a small adapter.


---
**y milord** *May 12, 2015 14:15*

Will there be a version with no drivers?


---
**Arthur Wolf** *May 12, 2015 14:16*

**+y milord** There will be a version with only 3 drivers. potentially we can do a version with no drivers at all if there is enough demand.


---
**y milord** *May 12, 2015 14:18*

I'd be interested. The machine i'd like to use this has large NEMA 23's with external drivers. Something like that would be a drop in replacement.


---
**Skyler Ogden** *May 12, 2015 16:58*

Oh man, you're going to get busted for posting porn on google+.


---
**James Rivera** *May 12, 2015 17:43*

Sooo...ARM Cortex M0 or M4?  Also, I see "Intel Edison" printed on the board? What is that for? And "FPGA_JTAG"? Will the FPGA be for debugging only or is there something more interesting planned?


---
**y milord** *May 12, 2015 17:46*

NXP LPC4337 (ARM Cortex-M4 which includes an ARM Cortex-M0 coprocessor). And the Edison is for running a host (i.e. Octoprint) locally on the controller.


---
**Jeff DeMaagd** *May 12, 2015 19:35*

Will you need 4A? The drivers spec'd on the previous announcement show 4A drivers. I don't know if that changed.



[https://plus.google.com/u/0/+ArthurWolf/posts/MEPqaThEaRq?pid=6109126620310037906&oid=111270782037537707264](https://plus.google.com/u/0/+ArthurWolf/posts/MEPqaThEaRq?pid=6109126620310037906&oid=111270782037537707264)


---
**Ondřej Píštěk** *May 12, 2015 19:51*

OK, servos, Edison, many motors, many outputs, better alignment of aditional pins, screws with some space around. Definetly looks like perfect replacement of my semifryied Smothieboard ver.1 Thanks for this work and good luck with testing and finalizing.


---
**Jeff DeMaagd** *May 12, 2015 20:15*

RC servos, servomotors or both?


---
**y milord** *May 12, 2015 20:50*

**+Arthur Wolf**  If you you are looking for beta testers. I'd like to throw my name in the 'hat' so to speak. 


---
**Jonathan Lussier** *May 13, 2015 01:13*

**+Jeff DeMaagd** RC-style servos (3-pin).  You can use "real" servodrivers/servomotors on the unused stepper driver circuits if they support the same protocol type. 


---
**Ngarewyrd Shurasae** *May 13, 2015 09:01*

it wasn't until I looked at the underside of the PCB that I realised what it is you had done, There didn't seem to be enough driver chips to run all seven Motors... However, Putting two extra drivers on the underside? yes, that's clever, and rather well thought out. Though it does mean that one has to think carefully about location of the board now.


---
**James Rivera** *May 13, 2015 15:45*

There is a Xilinx Spartan FPGA on the board! What is that used for?


---
**Ryan Wirth** *May 27, 2015 00:19*

Nice, really looking forward to this! Good luck!


---
**Glenn West** *May 28, 2015 10:40*

Look nice. I beta it in a delta and a cartesian


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/PRjbzbaQRzQ) &mdash; content and formatting may not be reliable*
