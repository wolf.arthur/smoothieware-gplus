---
layout: post
title: "So, only boys ? I propose to find a prize for first girl !"
date: May 12, 2015 14:16
category: "General discussion"
author: "Daniel Dumitru"
---
So, only boys ?  I propose to find a prize for first girl !





**"Daniel Dumitru"**

---
---
**Arthur Wolf** *May 12, 2015 14:19*

Well nobody's getting a free Smoothieboard <b>just</b> for their gender ... :)



This isn't really specific to Smoothie, it's general to the whole 3D printing community, and even more generally it's a problem in the tech industry ...


---
**Daniel Dumitru** *May 12, 2015 14:45*

I wasn't thinking to a full board. Maybe a cap, or at least a sticker... 


---
**Stephanie A** *May 12, 2015 18:19*

Breaking news: Girls exist. More on this story at 11.


---
**Arthur Wolf** *May 12, 2015 18:37*

**+Stephanie S** Well apparently, you win something ! I'll let **+Daniel Dumitru** tell you exactly what as it was his idea :)


---
**Daniel Dumitru** *May 13, 2015 03:20*

First of all a BIG hand of Applause !

Then let me comment : And what a girl ! has user on github, has made a project Teensylu2 , building a quadrocopter ..


---
**Stephanie A** *May 13, 2015 05:34*

Gina is more accomplished than me, she is practically a role model. 

I was lucky enough to have an upbringing that encouraged the mind of an engineer. My grandfather was an engineer, my oldest sister is an engineer, and I lived in a town that was supported by Intel. Right now I'm tutoring my youngest sister to program in python. What can I say, it's in my blood. 

I hope to expand the maker community to everyone. From solving basic problems, to solving economic issues. I hope that one day the gender stereotypes will be broken, and people can do what they're good at without being bullied or pushed into stereotypical roles by corporations, advertising, or parental/community expectations.


---
**Daniel Dumitru** *May 13, 2015 06:05*

Well, "engineering" isn't my day job but I like a lot to play with DIY stuff. This area I think that is not that specific for girls. Otherwise I saw a lot of girls (in my day job as well) doing work of an architect/engineer with good performances but just only on "paper".  Anyway , I consider that this kind of DIY activities will disappear for all in near future for anyone. 

Since you have mentioned about Gina as role model, does she has a G+ page of some kind of electronic profile/resume page ?


---
**Arthur Wolf** *May 13, 2015 07:52*

**+Daniel Dumitru** : **+Gina Häußge** of **+OctoPrint** fame.


---
*Imported from [Google+](https://plus.google.com/107318571191916561952/posts/7bTH9To2vJS) &mdash; content and formatting may not be reliable*
