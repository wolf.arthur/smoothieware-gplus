---
layout: post
title: "The mega delta printer Electra Flarefire and I have been building..."
date: May 14, 2015 06:23
category: "General discussion"
author: "Ngarewyrd Shurasae"
---
The mega delta printer **+Electra Flarefire**​and I have been building... The question we have is... We know the on board LEDs mean something, but we're not entirely sure what the combinations mean



<b>Originally shared by Ngarewyrd Shurasae</b>



Quick movement test of the mega delta **+Electra Flarefire**​ and I have been working on. The little dongle is the switch end for the z-probe. We based a lot of the parts on the cherry pi, with a few of our own mods. 



Running a smoothieboard for the controller.



Things to do... Extruder, wire up the hot end and fans, then investigate the idea of setting up a heated bed and build area. 



So yeah,  there you have it, that thing that has been keeping me occupied for a while


**Video content missing for image https://lh3.googleusercontent.com/-Q7dx8c222tI/VVQ8UEZXZXI/AAAAAAAABjk/Twcqk5DqlUE/s0/VIDEO0003.mp4.gif**
![missing image](https://lh3.googleusercontent.com/-Q7dx8c222tI/VVQ8UEZXZXI/AAAAAAAABjk/Twcqk5DqlUE/s0/VIDEO0003.mp4.gif)



**"Ngarewyrd Shurasae"**

---
---
**Electra Flarefire** *May 14, 2015 11:45*

And **+Arthur Wolf**, who do I thank for the forward planning regarding the 5V rail input diode arrangement. I was trying to work out if it was safe to connect up a 5V always on input(standby from an ATX supply, so we can take advantage of the power switching) with the R-78E-5.0 reg installed.. And thanks to the diodes.. it is! Yay!

Also.. I'd also like to know what the LEDs actually mean.. The blink patterns change between idle and printing and look interesting.. 


---
**Arthur Wolf** *May 24, 2015 20:11*

**+Electra Flarefire** That has been done so long ago, I'm not exactly sure :)

The two center leds indicate the execution of the two loops in smoothie : on_idle and on_main_loop. it'll generally blink faster when not printing ( because it's not busy between blinks ).


---
*Imported from [Google+](https://plus.google.com/106832169737510952186/posts/JavcSKAHAjV) &mdash; content and formatting may not be reliable*
