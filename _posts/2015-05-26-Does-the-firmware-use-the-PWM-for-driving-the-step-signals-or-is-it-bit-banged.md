---
layout: post
title: "Does the firmware use the PWM for driving the step signals, or is it bit-banged"
date: May 26, 2015 23:32
category: "General discussion"
author: "Stephanie A"
---
Does the firmware use the PWM for driving the step signals, or is it bit-banged. Are there any special pin requirements (ie what pins to use) for driving all of the stepper signals? 





**"Stephanie A"**

---
---
**Arthur Wolf** *May 26, 2015 23:38*

**+Stephanie S** There is no special requirement, any GPIO will do.

We did use PWM-capable pins in case somebody wanted to do something with that, but nobody did.


---
**Stephanie A** *May 27, 2015 03:15*

Thanks Arthur, that'll make things easier.

is there also LCD support? I might be able to add it to the board I'm designing.


---
**Arthur Wolf** *May 27, 2015 08:02*

**+Stephanie S** There's "panel" support : [http://smoothieware.org/panel](http://smoothieware.org/panel)

No proper LCD driver, we use SPI to talk to most screens.


---
**Stephanie A** *May 27, 2015 18:20*

Thanks **+Arthur Wolf**​ what about the mosfets for the bed and hotend, do you use the pwm? 


---
**Arthur Wolf** *May 27, 2015 18:21*

**+Stephanie S** Nope. Hardware PWM is only used for spindle and laser control


---
**Wolfmanjm** *May 27, 2015 20:57*

NOTE there are some pins hardwired in the code that you should not use for anything else, otherwise you will need to maintain your own fork (which you may want to do anyway). Details are here (the hardcoded pins are listed at the bottom). [http://smoothieware.org/lpc1769-pin-usage](http://smoothieware.org/lpc1769-pin-usage)


---
**Stephanie A** *May 27, 2015 23:03*

I am not going to maintain my own fork. Hardcoded pins are bad, but looking at those most of them are hardware limitations, not software. 

People are looking at smoothie now as an alternative to marlin, if the firmware can't match the hardware specs, then it isn't going to get picked up by anyone else. Notice the countless forks and hacks to marlin, I think you want to avoid that. 


---
*Imported from [Google+](https://plus.google.com/+StephanieS3D/posts/absYaGTG66R) &mdash; content and formatting may not be reliable*
