---
layout: post
title: "I'm use to calibrate my delta using M666 to give the Z height at some spot"
date: May 26, 2015 03:52
category: "General discussion"
author: "S\u00e9bastien Plante"
---
I'm use to calibrate my delta using M666 to give the Z height at some spot. Can I calibrate a cartesian using something like this? I wish I could not rely on hardware adjustment and using software tuning instant. Don't necessarily want "auto" calibration.





**"S\u00e9bastien Plante"**

---
---
**Wolfmanjm** *May 26, 2015 05:07*

[http://smoothieware.org/zprobe](http://smoothieware.org/zprobe) Use the three point levelling strategy, you can set the plane manually using M codes if you wish. M666 is not available for cartesians, and in fact M666 is not used even in deltas to set z height at all, it sets the plane the head moves in. Using it to set Z height is a misuse of M666. Z height should be set with M206 or M306.


---
**Sébastien Plante** *May 26, 2015 11:20*

okay thanks **+Wolfmanjm**​, I did follow a "fine tuning" procedure using M666 with Marlin.



So as I understand, there's no way to do it manually without a probe? I wish I could go to a point, lowering at the point I wish and then set this value, move to the other points, etc...


---
**Wolfmanjm** *May 26, 2015 19:44*

yes you can do it manually. by measuring the height at three points and calculate the plane manually as described here [https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/tools/zprobe/Plane3D.cpp#L3](https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/tools/zprobe/Plane3D.cpp#L3) getting ABCD then enter them with M561 Axxx Bxxx Cxxx Dxxx and save with M500. It is much easier to do with a probe. The probe can simply be a microswitch clamped to the tool head.


---
**Sébastien Plante** *May 26, 2015 19:46*

Cool thanks **+Wolfmanjm** I'll try this. I will add a switch, but I wont have it when I'll start using the printer ! :)


---
**Glenn West** *May 28, 2015 10:37*

Check out the 626pilot fork of smoothie 

I'm using it till it gets merged really good auto cal and bed map



I use a effector with hinge and switch 

It's on Thingiverse 


---
**Sébastien Plante** *May 28, 2015 11:20*

**+Glenn West**​ This is what I have on my delta (hinge and switch), will look for the same on the CoreXY


---
**Glenn West** *May 28, 2015 11:22*

Cool. The switch really works well. I wonder if it's worth to get a better switch but the repeatability is near perfect with a normal endstop switch


---
**Wolfmanjm** *May 28, 2015 23:08*

**+Glenn West** FYI the 626pilot stuff will not be merged into mainline smoothie.


---
**Glenn West** *May 28, 2015 23:22*

Is there something better in mainline coming for delta?


---
**Wolfmanjm** *May 29, 2015 00:01*

There is the ZGrid which still needs to be tested on a delta, but once it works is IMO the best solution for compensating for inaccurate delta geometry. The builtin delta calibration is great if your delta geometry is accurate.

Lastly there is the ability to print out the 7 probe points which can be fed into an offline least errors algorithm of which there are several floating around, this one seems the easiest.

[https://github.com/DejayRezme/DeltaAutoCalibration/tree/master/src](https://github.com/DejayRezme/DeltaAutoCalibration/tree/master/src)


---
**Glenn West** *May 29, 2015 04:43*

What was the issue on the pilot626 code?


---
*Imported from [Google+](https://plus.google.com/+excessnet/posts/67nK9u2mM47) &mdash; content and formatting may not be reliable*
