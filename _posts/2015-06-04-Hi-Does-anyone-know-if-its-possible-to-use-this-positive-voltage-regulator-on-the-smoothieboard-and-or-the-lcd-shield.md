---
layout: post
title: "Hi, Does anyone know if it's possible to use this positive voltage regulator on the smoothieboard and/or the lcd shield?"
date: June 04, 2015 09:17
category: "General discussion"
author: "Ryan Wirth"
---
Hi, 

Does anyone know if it's possible to use this positive voltage regulator on the smoothieboard and/or the lcd shield?

Thanks.

![missing image](https://lh3.googleusercontent.com/-ZsFYTutzZyY/VXAXxIuZgyI/AAAAAAAAACw/Ma3-DeQLuvA/s0/15%252B-%252B1.jpeg)



**"Ryan Wirth"**

---
---
**Bouni** *June 04, 2015 09:22*

I've just checked both datasheets and i think you can use it.

Maybe you have a lot of heat output and a heatsink is required.



[http://www.recom-international.com/pdf/Innoline/R-78xx-1.0.pdf](http://www.recom-international.com/pdf/Innoline/R-78xx-1.0.pdf)

[https://www.sparkfun.com/datasheets/Components/LM7805.pdf](https://www.sparkfun.com/datasheets/Components/LM7805.pdf)


---
**Triffid Hunter** *June 04, 2015 09:40*

it will get extremely hot if you put it on smoothieboard, that's why we suggest the switching one instead.


---
**Ryan Wirth** *June 04, 2015 10:52*

Ah ok thanks a lot for the info! I don't really need it I have a 5v supply anyway I just want to populate the board and break out all the pins straight up so I don't have to go back later and solder stuff in once it's being used. Out of curiosity is there any advantage or disadvantage to having the regulator on there if you have a dedicated 5v supply? Thanks for the quick replies and great info! :)


---
**Triffid Hunter** *June 04, 2015 10:55*

if you have an external 5v supply, the onboard regulator is completely unnecessary.



Usually you only need a 5v supply of one sort or another when running a graphical LCD, since the additional load of the backlight can pull too much power from USB


---
**Ryan Wirth** *June 04, 2015 11:00*

Ok cool, thanks... I'll be running the rrd Glcd but I'm using a cooler master 1100w ucp psu so it won't be an issue and it's already in use so I might as well use that protected 5v feed instead of stuffing around. Really appreciate the quick help! I'm off to finish soldering so thanks a lot for clearing that up, saved me a heap of time.  :)


---
*Imported from [Google+](https://plus.google.com/103069239814092760255/posts/1R9roV9SU4L) &mdash; content and formatting may not be reliable*
