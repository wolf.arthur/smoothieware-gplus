---
layout: post
title: "Shared on June 19, 2015 20:36...\n"
date: June 19, 2015 20:36
category: "General discussion"
author: "Arthur Wolf"
---






**"Arthur Wolf"**

---
---
**Artem Grunichev** *June 20, 2015 19:41*

Great! Waiting for beta-testing


---
**Bouni** *June 25, 2015 07:02*

Are the LED's controlled by the ARM processor or the FPGA?


---
**Arthur Wolf** *June 25, 2015 07:24*

**+Anatoli Bugorski** The ARM


---
**Artem Grunichev** *July 08, 2015 19:28*

How's your progress, any news?


---
**Arthur Wolf** *July 08, 2015 19:34*

Coding, going slowly as I have other responsibilites.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/bnmos74sZCG) &mdash; content and formatting may not be reliable*
