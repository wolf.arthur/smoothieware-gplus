---
layout: post
title: "Shared on July 12, 2015 15:51...\n"
date: July 12, 2015 15:51
category: "General discussion"
author: "Arthur Wolf"
---






**"Arthur Wolf"**

---
---
**Chris Purola (Chorca)** *July 12, 2015 19:07*

This is really neat, I have an old Roland vinyl cutter but it seems to work alright. Would be really interesting it you were able to add things like tangential emulation and whatnot into the firmware so older machines could have capabilities of newer ones!


---
**Arthur Wolf** *July 12, 2015 19:14*

**+Chris Purola** What's tangential emulation ?


---
**Chris Purola (Chorca)** *July 12, 2015 19:17*

It's a feature, where for sharp corners, instead of letting the blade drag and swivel around the sharp corner, it overcuts the corner slightly, then lifts the blade up, very gently sets it down near the corner, and drags slightly so the blade swivels without cutting.. Then it applies full pressure and cuts with the blade in the proper orientation so you don't get rounded or torn-up corners at 90-degrees or less. It's something that only high-end machines have usually.


---
**Arthur Wolf** *July 12, 2015 19:18*

**+Chris Purola** That's pretty cool. I guess somebody could implement it, doesn't sound too hard.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/fAousLV4TaV) &mdash; content and formatting may not be reliable*
