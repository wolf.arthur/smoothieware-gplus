---
layout: post
title: "I have being having a problem with interference with one of the stepper motors on my smoothie board"
date: July 15, 2015 18:14
category: "General discussion"
author: "Andrew Wade"
---
 I have being having a problem with interference with one of the stepper motors on my smoothie board. How critical is the routing of the wires, any advice on the best way to route them?





**"Andrew Wade"**

---
---
**Arthur Wolf** *July 15, 2015 21:46*

**+Andrew Wade** What kind of interference ? What are the symptoms exactly ?


---
**Andrew Wade** *July 16, 2015 06:26*

One of the stepper motors is getting interference bouncing up and down. I moved the wires and it seems to affected by a mains power cable it crosses above. Not being an expert in  these matters what I am best doing, shielding the cables as the routing will not allow them to not cross.


---
**Arthur Wolf** *July 16, 2015 12:01*

**+Andrew Wade** Wow that's impressive interference. Ideally you want to twist and/or shield the stepper cable. Adding ferrite beads could help too.

Ideally you want to re-route though, even if that's more work.


---
**Andrew Wade** *July 17, 2015 07:09*

I found the problem the cable was dodgy, some of the strands in the core on one of the wires had broken.


---
**Arthur Wolf** *July 17, 2015 07:53*

**+Andrew Wade** Yay for a fixed problem :)


---
*Imported from [Google+](https://plus.google.com/112929738014057369475/posts/dMMS2Aos86d) &mdash; content and formatting may not be reliable*
