---
layout: post
title: "Originally shared by David Bassetti And the BB400 with a Bondtech 2.0 extruder and Smoothieboard as well :)"
date: July 17, 2015 11:01
category: "General discussion"
author: "David Bassetti"
---
<b>Originally shared by David Bassetti</b>



And the BB400 with a Bondtech 2.0 extruder and Smoothieboard as well :)

![missing image](https://lh3.googleusercontent.com/-ULNyjqw7hk4/VajgMi5HUsI/AAAAAAAAB1E/dlygiZ59PzE/s0/IMG_2892.JPG)



**"David Bassetti"**

---
---
**Eric Lien** *July 17, 2015 11:53*

Nice to see some bondtech direct drive printers in the wild. Looks great.


---
**David Bassetti** *July 17, 2015 12:15*

Thanks Eric :)

Yeah we were frustrated with printing at 0.40mm layer heights for BIG prints so we have the Volcano from E3D on it as well with a huge 1.2mm tip diameter :)... more soon


---
**Martin Bondéus** *July 17, 2015 14:53*

Nice work **+David Bassetti** ! Looking forward for your feedback!


---
*Imported from [Google+](https://plus.google.com/+DavidBassetti/posts/3dZtCZ5Nfjq) &mdash; content and formatting may not be reliable*
