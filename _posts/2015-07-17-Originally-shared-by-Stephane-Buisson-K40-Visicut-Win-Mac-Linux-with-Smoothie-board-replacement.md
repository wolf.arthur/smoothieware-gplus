---
layout: post
title: "Originally shared by Stephane Buisson K40 + Visicut (Win/Mac/Linux) with Smoothie board replacement"
date: July 17, 2015 13:58
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Stephane Buisson</b>



K40 + Visicut (Win/Mac/Linux) with Smoothie board replacement.

One big step forward for a usable solution ! 

Thank to **+Thomas Oster**  (Visicut developer)

Community call for help - Beta testers needed



Go there to find out about Visicut open source software -> [https://hci.rwth-aachen.de/visicut](https://hci.rwth-aachen.de/visicut)

You will need a Smoothie board to upgrade your K40 -> [http://smoothieware.org/getting-smoothieboard](http://smoothieware.org/getting-smoothieboard) 

Then please test all Thomas changes and report bugs and stuff in the pull-request [https://github.com/t-oster/LibLaserCut/pull/23](https://github.com/t-oster/LibLaserCut/pull/23) from where you can already download the first build files.



Visicut for Win/Mac/Linux already existing, Visicut driver for Smoothie board (LibLaserCut) was the last missing link for a fully operational chain, hardware to software to mod our cheap K40 laser cutter at reasonable price. (Smoothie is cheaper than DSP board and benefit of Smoothie community support).



**+Arthur Wolf** did told me sometime ago a K40 was modified with a Smoothie board. A hardware Mod tutorial would also be needed for our K40 community, any candidate ?





**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/F1pnLDLKiA7) &mdash; content and formatting may not be reliable*
