---
layout: post
title: "I am just about finished building my X-Carve ~ all I need is to finish a bit of the wiring and then I will Smoothiefy it!!!"
date: July 19, 2015 11:32
category: "General discussion"
author: "Chapeux Pyrate"
---
I am just about finished building my X-Carve ~ all I need is to finish a bit of the wiring and then I will Smoothiefy it!!!  Since the X-Carve (like the ShapeOko has two stepper motors on the Y Axis ~ I will be getting the Smoothieboard 4x to drive the 4 motors ~ slaved Y axis ~ Such a well thought out board ~ SmoothieWare!  Arthur Wolf and crew are doing great work!   Sure wouldn't mind being a tester for V2.0  ;{)

![missing image](https://lh3.googleusercontent.com/-zxQz19qX8cU/VauJOgdHsmI/AAAAAAAAAho/gIbXnZE_S1Q/s0/X-CarveSmoothiefy.jpg)



**"Chapeux Pyrate"**

---
---
**Arthur Wolf** *July 19, 2015 11:34*

Yay :)


---
**Chapeux Pyrate** *July 19, 2015 11:55*

I am an artisan for Pirates ~ hmmm think I might be the first pirate here (diversity mates!) and I design in 3D for pewter casting (pirate coins, pins, pendants, buckles etc...) ~ SmoothieBoard with her 32bit processor seems the best way to go for cutting high detail, high res ~ more organic STL models that I create in ZBrush (not a straight line in the code!!!) Arcs and S-Curves Oh My! ~ so I set my sights on X-Carve and SmoothieBoard primarily cutting milling wax.  


---
**Laurent Flamand** *July 19, 2015 21:47*

Great job ! And great choice with the smoothieboard !


---
**Chapeux Pyrate** *July 20, 2015 12:49*

Thanks Laurent ~ still a work in progress ~ as I originally chose the Azteeg X5 as it seemed to offer 4 drivers ~ not so ~ I found out later ~ not happy with their support either ~ well lack there of ~ so I am trying to return it and I will order the 4 driver SmoothieBoard ~ great community and great support ~ that should always be the first consideration when making such a choice. 


---
**Wolfmanjm** *July 21, 2015 20:05*

azteeg x5 has 4 drivers. xyze.  exactly the same as 4x smoothie 


---
**Laurent Flamand** *July 21, 2015 21:30*

I ran a smoothieboard for a few years now on 3d printers...thank's to it's config file It's easy'to use and adapt to what you built !  I don't have a cnc yet... But it'll be with a smoothieboard inside ! :)






---
**Chapeux Pyrate** *July 22, 2015 20:02*

**+Wolfmanjm** Thanks for you comment ~  alas on the azteeg x5 ~ 4 drivers is what I thought ~ that's why I bought it ~ but from the very folks that make and sell it ~ as I inquired thinking it was just a matter of configuration ~ they replied that it really wasn't ~and that I couldn't use the e driver for a 4th stepper motor ~  this in spite of their wiring diagram showing just such a set up~ 4 drivers to 4 stepper motors ~   so I am taking their word for it... as I don't need the confusion ~ rather stick with a board, company and community that I have some confidence in.  


---
**Wolfmanjm** *July 22, 2015 20:07*

I dunno who you talked to but it does have 4 drivers that can be used for 4 stepper motors... Maybe they got confused about the double Z stepper sockets? or did you mean 5 drivers? it is confusing because both smoothieboard and azteeg have 4 drivers on the board that can be used.

However Smoothie firmware itself is limited to 3 real axis. the extra axis need to be setup as E extruders. you can jumper two drivers on smoothieboard though which you can't on azteeg. See the wiki...  [http://smoothieware.org/3d-printer-guide#toc27](http://smoothieware.org/3d-printer-guide#toc27) that is only for smoothie board.


---
**Chapeux Pyrate** *July 22, 2015 22:08*

**+Wolfmanjm**  I appreciate you taking the time to discuss this matter with me.  Yes I was very thorough in explaining what I was wanting to do ~ drive 4 motors ~ being the X-Carve (similar to the ShapeOko) drive two Y-Axis motors ~ paralleled/slaved/jumped ~ whatever works ~ and their clear and concise response was the Azteeg X5 wasn't meant for that.  They also said the real purpose of the Azteeg X5 and Smoothieware in general was for 3D printing and not so much for CNC (I know better as I have seen others doing so with both boards and software)...  I don't want to speak ill of the company ~ as I think they did their best to respond to my inquiry ~ and even offered to send me a prototype board of a new and better version of the board ~ that would have my needed capabilities ~ and ask me to be a tester ~ I accepted ~ but they have been experiencing some unexpected delays in finishing the development of their new boards and were not being very responsive ~ I understand they were likely caught up in that development process... but I only want to wait so long ~ I have much to learn and do in this endeavor ~ so I decided to request that I send the board back and move on ~ and wished them well.  



And yes ~ I am going with the Smoothieboard X4 and jumping two drivers to the paired Y motors ~ as that is precisely what I need to do considering the amps of my motors and to the amps available on either one of the boards' drivers.    


---
**Wolfmanjm** *July 22, 2015 22:19*

Ok so I think your original post was very misleading wrt the AzteegX5. saying it did not have 4 drivers. This is incorrect. What the issue is that it does not have headers for the step/dir/en pins for each driver as the Smoothieboard does, thus you cannot slave a driver as you can on a SMoothie board.


---
**Chapeux Pyrate** *July 22, 2015 22:36*

**+Wolfmanjm** I was surely not trying to be misleading ~ I do admit I don't have much of the tech speak down ~ that's for sure ~ and perhaps I spoke in terms too simple and not so precise ~ but I am just trying to learn as best I can ~ no offense intended.  I was thinking I said that the board did not have the drivers available to me for my particular purpose ~ I may be confused on the matter ~ but still for all practical purposes ~ for my purpose ~ the board just won't do it ~ and I confirmed that by describing to the support person exactly what I wanted to do (related to how the X-Carve is designed, and they told me that I exactly cannot do that with their board.  



I could copy the actual conversation if you deem it important to you.  


---
**Arthur Wolf** *July 22, 2015 22:45*

**+Chapeux Pyrate** I'm kinda baffled by this idea that Smoothieware is not good for CNC milling.

The first machine it ran on was a laser, the second a CNC mill, and it was originally a port of GRBL, which is only for CNC mills ...

We also have plenty of CNC-specific features, and take a lot of attention to be compatible with CNC CAM packages.


---
**Chapeux Pyrate** *July 22, 2015 23:32*

**+Arthur Wolf** Gosh... I am more baffled than you ~ as I have no recollection of mentioning the Smoothieware was not good for CNC ~ maybe I am just not used to how conversations work on G+  ~ 'cause I totally missed anyone, including myself saying that ~ I get the feeling that  you might be thinking I said something of the sort ~ since you brought it up as a reply to me ~ but I didn't ~ I am nothing but impressed with Smoothieware even knowing just what little I do know about it thus far ~ and have only affirmed that fact ~ am I just totally missing something here ~ 


---
**Arthur Wolf** *July 23, 2015 08:09*

**+Chapeux Pyrate** I didn't say you said that, I was refering to you talking about what the vendor said : «.  They also said the real purpose of the Azteeg X5 and Smoothieware in general was for 3D printing and not so much for CNC »


---
**Chapeux Pyrate** *July 23, 2015 10:07*

**+Arthur Wolf** Got ~ I wasn't offended ~ just confused ~ I did say that they said that ~ well, because they said that ~ it was surprising to me as well as that was not the impression I got from their own website ~ "X5 mini is a 32 bit ARM based Motion controller for 3D printers, CNC Machines and Laser cutters."  Hmmm... CNC ~ that's what I want ~ so I bought ~ and then was told well no it's not really for CNC ~ but you guys have a whole section on your site about using a Smoothieboard (or equivalent) for CNC ~ so I sent the thing back!  The thing worse than a lack of support is mis-support. 


---
*Imported from [Google+](https://plus.google.com/116524126135527852419/posts/Shr1fC3NdJz) &mdash; content and formatting may not be reliable*
