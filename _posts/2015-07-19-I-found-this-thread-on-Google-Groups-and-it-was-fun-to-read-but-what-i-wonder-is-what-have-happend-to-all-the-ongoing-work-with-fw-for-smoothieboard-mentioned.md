---
layout: post
title: "I found this thread on Google Groups and it was fun to read but what i wonder is what have happend to all the ongoing work with fw for smoothieboard mentioned?"
date: July 19, 2015 13:01
category: "General discussion"
author: "Trhuster"
---
I found this thread on Google Groups and it was fun to read but what i wonder is what have happend to all the ongoing work with fw for smoothieboard mentioned? Is the issues Dan and Ryan talk about resolved? 



[https://groups.google.com/forum/#!searchin/3dprintertipstricksreviews/smoothieboard&#x7c;sort:date/3dprintertipstricksreviews/rrVT7VlT29s/MRSO-Z6Ja30J](https://groups.google.com/forum/#!searchin/3dprintertipstricksreviews/smoothieboard%7Csort:date/3dprintertipstricksreviews/rrVT7VlT29s/MRSO-Z6Ja30J)





**"Trhuster"**

---
---
**Arthur Wolf** *July 19, 2015 13:17*

**+Trhuster** Ryan Carlyle has been so dishonest ( and trolly in a weird, non-obvious way ) in other threads, that after many tries at having a discution with him, I have decided never to address him again. So that conversation is you linked is probably not going to continue much. He keeps taking pokes at Smoothie on a regular basis, it's hard to resist answering, but it's necessary for my sanity :)



In that specific thread, they don't really talk much about issues, but more about possible future evolutions. We are implementing some of those step by step.



About issues : Some of the issues he mentions in other threads exist ( as far as I understand, he is not very good at vulgarisation ), some just stem from a misunderstanding of the code ( not most though I think ).

None are major problems as far as I understand, what we have now works well ( Ryan would disagree on what "works well" means :) )



Some people are working actually adding safety features for thermistors for example, which I like much more than when people will take insane amounts of time writing down thing on a mailing list, but never provide code or any help with how to actually implement their solutions.


---
**Trhuster** *July 19, 2015 13:31*

Thx, i am thinking about to converting my core X/Y printer from mightyboard/sailfish to a Smoothie board but i am unsure if it is the right move to do. 


---
**Arthur Wolf** *July 19, 2015 13:38*

**+Trhuster** It depends what you need/are looking for etc. Smoothie is very easy to configure, very extensible, under very active community development, has a huge community that can help you, is very well documented ( just ask people that use it, not me :) ).

I suppose it doesn't have some features sailfish has, the only one I can think of is advance. We are adding that.

It does have lots of features sailfish doesn't have ( composite USB, simple SD-card based config, Ethernet, many others smaller ones ).

I can tell you that we have <s>a lot</s> of users coming from the other 8-bit firmwares that are extremely enthusiastic about smoothie, it's performance, and the user experience.

If you have any specific question, I'll try my best to answer.


---
*Imported from [Google+](https://plus.google.com/105197531932491726329/posts/EBi2Rzc2caj) &mdash; content and formatting may not be reliable*
