---
layout: post
title: "Arthur Wolf ~ so mates ~ I am ready to pull the trigger and order my smoothieboard ~ well I have the coin, but I do want one question answered if you would be so kind Mr"
date: July 22, 2015 19:55
category: "General discussion"
author: "Chapeux Pyrate"
---
**+Arthur Wolf**  ~ so mates ~ I am ready to pull the trigger and order my smoothieboard ~ well I have the coin, but I do want one question answered if you would be so kind Mr. **+Arthur Wolf** ~ my entire goal with my XCarve CNC project is to be able to accurately and smoothly carve very fine and rather organic (curves, arcs and S-Curves Oh My!) ~ see the pic of an example of what I create ~ a personalized custom pirate coin ~ about 1.75" diameter ~ quite fine detail ~ will the X-Carve (I know you are not an expert on the machine) with the Smoothieboard running GRBL  ~ most often carving milling/jeweler's wax (I have a very tiny special wax cutting bit set) ~ am I going to be able to care a thing such as this pirate coin?   (without perceptible step and cut lines ~ savvy?)  



You should always speak honestly with a pirate ~ we have sharp stuff!  ;{P

![missing image](https://lh3.googleusercontent.com/-jRnzKQ8x2fo/Va_076Qw6eI/AAAAAAAAAoI/jEsbhVvShRE/s0/NicksPortraitCoinFaceFinal.jpg)



**"Chapeux Pyrate"**

---
---
**Chapeux Pyrate** *July 22, 2015 20:05*

Done in ZBrush (3D App)  and exported as .STL ~ been looking into software that will then generate GCode from STL.  


---
**Arthur Wolf** *July 22, 2015 20:07*

**+Chapeux Pyrate**  So, the Smoothieboard won't be a problem here ( it doesn't run GRBL btw, it runs Smoothieware ). It can mill those no problem.

The quality of the result, however, is going to entirely depend on the machine itself here.



I'm not sure the X-Carve can produce the results you want. It's belt-driven, which means a certain level of backlash in the axes ( which can be corrected by tightening, but only to a certain point ). It <b>could</b> be fine if you are going to be doing "scanning" surface engraving. But I personally wouldn't try it with the X-Carve without checking first that somebody else has successfully done the same thing on it.

Don't get me wrong : the X-Carve is a great machine, I'd recommend it to somebody milling plates of MDF oh plastics any day. But you do have very strict requirements.



To get the best results for this kind of work, you want a very sturdy machine, with anti-backlash leadscrews.

I own a cheap chinese CNC mill ( [http://www.aliexpress.com/item/230W-Three-axis-Ball-Screw-CNC-Router-Engraver-Engraving-Milling-Drilling-Cutting-Machine-CNC-3040-Z/858626629.html](http://www.aliexpress.com/item/230W-Three-axis-Ball-Screw-CNC-Router-Engraver-Engraving-Milling-Drilling-Cutting-Machine-CNC-3040-Z/858626629.html) this kind ) and I believe it's what you'll get the best results with for your budget. It works fine with Smoothieboard.



But even with a machine that can do the job ( we've already established the Smoothieboard isn't a problem ), it'll probably still take quite a bit of work and time to get it to work right, find the right way to mill things in the CAM software etc.


---
**Chapeux Pyrate** *July 22, 2015 21:54*

**+Arthur Wolf** Thank you Arthur ~ you have given me the kind of thorough and considerate response that I expected to receive from you ~ considering how you have responded to the questions and ideas of others in this community and on other places where I have come across your posts and responses. 



~ you have well earned my appreciation ~ and patronage ~ 



I want this machine to serve my work for sure ~ but I see it as an investment ~ and learning is always a worthwhile investment ~ so I will order the Smoothieboard and use Smoothieware (knowing better now what it is and what not to call it ~ ) ~ and I will build and test and tweak and learn from there.  ~  I think it will not be a loss ~ as the X-Carve I think will serve many of my needs ~ my research toward this end ~ or beginning rather, is also giving me much understanding about the maker world ~ and I am becoming rather a geek more than ever before (a badge of honor it is)



~ this process is also moving me toward an understanding of 3D printing ~ especially dlp/resin printing ~ which would likely serve me even better for the small sculptured coins and such that I do... I figure if I understand the Smoothieboard and it's capabilities ~ and stepper motors and so on and on ~ I should soon have the knowledge to build such a printer as well ~ hmm... perhaps I can even adapt the X-Carve ~ to be a sometimes CNC mill and sometimes Lithographic Resin Printer ~ I have been looking into it!  How's that for transforming from Pirate Artisan to Geek Maker?!  ;{)



Great thanks sir!  


---
**Chapeux Pyrate** *July 23, 2015 11:35*

**+Arthur Wolf** So your helpful advice has got me researching some of the finer points of a machine like the X-Carve ~ namely considering concerns of accuracy, or possible lack there of, relating to the X-Carve being belt driven on the X and Y axises  ~  I am taking all of your advice under thoughtful consideration ~ even looking at a your recommendation to consider going to a Ball Screw machine ~ hey, if I am not happy with the results of the X-Carve I am sure I can sell it in short order ~  but I thought I would give the X-Carve a chance ~ since I have it ~ I did some research on the good and bad of a belt system ~ and the issue of possible backlash ~ First I will mention that I ordered the upgraded Acme Lead Screw for the Z-Axis ~ the better choice for accuracy when using a lead screw ~ so I am told. 



As for the belts and pulleys ~ here we are both learning more about the X-Carve and it's specs ~ GT2 2mm pitch ~ fiberglass reinforced belts ~ what I am reading on the GT2 belts is that the teeth on belt and pulley are designed to fit more precisely ~ this toward eliminating the possibility of any play/slippage (backlash) ~ proper tension of the belt is important here... so hmmm... we shall see ~ I also found some discussion affirming that a synchronous dual motor setup on the Y-Axis like the X-Carve (and ShapeOko) ~ helps to manage backlash as well.   Again ~ we shall see.



I have learned so much already here ~ thanks again for your valuable advice! 


---
**Arthur Wolf** *July 23, 2015 11:45*

**+Chapeux Pyrate** GT2 is a better option than T or XL ( which are not even designed for positioning, and is what they are mostly compared to ), but it's still way below antibacklash leadscrews in accuracy.

As you say : you might as well try it, maybe it'll work.


---
**Chapeux Pyrate** *July 23, 2015 11:57*

**+Arthur Wolf** proofs in the puddin'  ;{)


---
*Imported from [Google+](https://plus.google.com/116524126135527852419/posts/5YC3N8N2g1u) &mdash; content and formatting may not be reliable*
