---
layout: post
title: "Cosine additive doing some fun stuff with G2/G3 NURBS interpolation"
date: July 23, 2015 03:52
category: "General discussion"
author: "Skyler Ogden"
---
Cosine additive doing some fun stuff with G2/G3 NURBS interpolation. Using a derivation of smoothieware as it turns out! Hope they share their fork soon 





**"Skyler Ogden"**

---
---
**Arthur Wolf** *July 23, 2015 08:11*

**+Skyler Ogden** It looks like the are just using the normal Smoothieware, not a fork : we already support G2/G3 moves ( and have for a long time ), it's just that no slicing software generates them ( well Slic3r has an option for interpolation of series of segments, but nobody activates that ).


---
**Topias Korpi** *July 23, 2015 09:56*

I've been thinking exactly this thing for long! I hate the sound(and end result) when printing larger circle from low resolution stl. Looks great!


---
**Skyler Ogden** *July 23, 2015 15:13*

**+Arthur Wolf** But it sounds like none of the slicer support it. Someone from Cosine Additive says S3D used to support it but has since removed the functionality. 



[https://www.reddit.com/r/3Dprinting/comments/3e4282/g2g3_true_vector_printing_and_smoothness/ctccku7](https://www.reddit.com/r/3Dprinting/comments/3e4282/g2g3_true_vector_printing_and_smoothness/ctccku7)


---
**Jeff DeMaagd** *July 23, 2015 15:32*

I think that's a case they still need to check their motion parameters. Using arc moves there is hiding the fact their jerk/accel is out of tune.



I'd been occasionally suggesting that a slicer be built to slice directly from STEPs and maybe other solid models. That way curves do propagate through the work flow to the machine rather than chopping them up into numerous facets.


---
*Imported from [Google+](https://plus.google.com/109854269241399183440/posts/8qhmxjDkt8N) &mdash; content and formatting may not be reliable*
