---
layout: post
title: "I just bought a SmoothieBoard 4XC! ;{)"
date: July 23, 2015 12:24
category: "General discussion"
author: "Chapeux Pyrate"
---
I just bought a SmoothieBoard 4XC!  ;{)

![missing image](https://lh3.googleusercontent.com/-bNs1kuTDUa4/VbDcrfB4sTI/AAAAAAAAAo8/IKSftAPzGuk/s0/Screen%252BShot%252B2015-07-23%252Bat%252B7.22.46%252BAM.jpg)



**"Chapeux Pyrate"**

---
---
**Arthur Wolf** *July 23, 2015 13:24*

**+Chapeux Pyrate** You wont regret it :)


---
**Chapeux Pyrate** *July 23, 2015 14:04*

**+Arthur Wolf** now that I am well informed on the matter I think you are correct sir.  ;{)


---
**Guy Bailey** *July 23, 2015 14:15*

I love my smoothie. Now if the code for z probing was more useable I would be completely satisfied. 


---
**Arthur Wolf** *July 23, 2015 14:17*

**+Guy Bailey** It's fully functional, but its a bit different from Marlin, and the documentation is maybe not clear enough ( mostly due to the great number of different probing setups we have to support ). We are working on it.


---
**Guy Bailey** *July 23, 2015 19:58*

Thanks **+Arthur Wolf** I am anxiously awaiting a new release. I can technically get it to work but it's an ugly work flow. 


---
**Wolfmanjm** *July 26, 2015 05:07*

**+Guy Bailey** Just saying "it doesn't work" is not going to get anything fixed! Please file an issue on github outlining your exact workflow and what is bad about it. Then maybe someone will improve or even fix it. As of now I am not aware of any significant issues, therefore nothing will get changed until problems are explained and reported.-- Thanks :)


---
**Guy Bailey** *July 26, 2015 12:05*

Thanks **+Wolfmanjm**. I had some dialog with **+Arthur Wolf** about it on the smoothie ware forum that outlined exactly what was going on. There were two issues that I was experiencing. And maybe part of the problem was my expectations. I was expecting it to work like my Printrbot in that when I press home Z, it does a probe and sets the Z value to 0 at whatever Z height is found from the probe. The second issue was that when I do a 3point probe, only the first point gets probed. The 2nd and 3rd points are traveled to but there is no Z movement. This was causing the head to drag and a skewed plane to be calculated. I was able to do single point probes and manually set offsets so it did technically work but not the way I was hoping for. The next time I update the smoothie software I'll give it another shot and try to use a more official channel to report any errors. 


---
*Imported from [Google+](https://plus.google.com/116524126135527852419/posts/eooJigTbkNg) &mdash; content and formatting may not be reliable*
