---
layout: post
title: "Are there any updates on the smoothie board 2?"
date: July 27, 2015 20:07
category: "General discussion"
author: "Joe Spanier"
---
Are there any updates on the smoothie board 2? Looking at ordering one in the near future. 



Also what fuses are used on the smoothie? Looking at the board nothing jumped out at me. Is the intent to fuse before the board? I like that idea, its how I do my CNC machines. 





**"Joe Spanier"**

---
---
**Arthur Wolf** *July 27, 2015 20:10*

**+Joe Spanier** We have prototypes of the Smoothie2-pro being assembled, and have begun working on the code. But there is an incredible amount of work to be done. To make sure we don't upset people with not meeting deadlines like we did for v1, there will be no deadlines for v2. All I can say is : it's still early and we have a shitton of work to do.



About fuses, there is none on the Smoothieboard. Your whole printer should have a fuse.


---
**Joe Spanier** *July 27, 2015 20:33*

I like and agree with both those answers. I hadn't seen the v2 mentioned for a while so I thought I'd ask. Thanks for the response!


---
*Imported from [Google+](https://plus.google.com/+JoeSpanier/posts/KZjHTDNbNZA) &mdash; content and formatting may not be reliable*
