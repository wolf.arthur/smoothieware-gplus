---
layout: post
title: "New printer coming up soon :)"
date: July 27, 2015 18:36
category: "General discussion"
author: "David Bassetti"
---
New printer coming up soon :)

![missing image](https://lh3.googleusercontent.com/-1uxX4z0EaaI/VbZ6M6RONMI/AAAAAAAAB34/2vGcoMWJWak/s0/Partly%252Bassembled%252B1.JPG)



**"David Bassetti"**

---
---
**Chris Purola (Chorca)** *July 29, 2015 16:32*

I love the rack-mount design! Someone at our hackerspace wanted to make one in a full-height rack, but this is great. The frame is already built!


---
**David Bassetti** *July 29, 2015 16:35*

Thanks Chris :)


---
**David Bassetti** *July 29, 2015 18:37*

We have the "taller" designs for racks as well if you are interested :)

email me david@3d-seed.com


---
*Imported from [Google+](https://plus.google.com/+DavidBassetti/posts/8W3BRsRiunb) &mdash; content and formatting may not be reliable*
