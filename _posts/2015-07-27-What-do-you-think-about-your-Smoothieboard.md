---
layout: post
title: "What do you think about your Smoothieboard ?"
date: July 27, 2015 18:01
category: "General discussion"
author: "Arthur Wolf"
---
What do you think about your Smoothieboard ?

I'm thinking of starting a "reviews" page for comments ( positive and negative ) about Smoothie and Smoothieboard.

Please try to keep it kinda short, I'd like the page to be easy to read.

Thanks :)





**"Arthur Wolf"**

---
---
**Sébastien Plante** *July 27, 2015 21:23*

Still have to finish my printer to really "use it", but for what I've played with, It seems faster and easier to use! 


---
**Mike Creuzer** *July 28, 2015 04:17*

I love that I can set it up and use it on my Android tablets.  A rebuild of the machine to make it 25% bigger takes only a few minutes to re-calibrate endstops without ever plugging it into a "real" computer. 


---
**Guy Bailey** *July 28, 2015 17:58*

Love it. Lots of functionality for the price and it's nice being able to edit a txt then reboot to make config changes. 


---
**Jack Colletta** *August 03, 2015 11:52*

Love it.  I just wish I had more time to contribute.  Too many hobbies I guess.  Thanks for the development and I look forward to v2.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/hV6DbDF2oSE) &mdash; content and formatting may not be reliable*
