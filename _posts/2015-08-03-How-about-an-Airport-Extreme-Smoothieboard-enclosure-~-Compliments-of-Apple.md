---
layout: post
title: "How about an Airport Extreme Smoothieboard enclosure ~ Compliments of Apple!"
date: August 03, 2015 17:21
category: "General discussion"
author: "Chapeux Pyrate"
---
How about an Airport Extreme Smoothieboard enclosure ~ Compliments of Apple!  I had an old and pretty much dead AirPort Extreme ~ so I salvaged the box and sought to figure out if it would make a good home for my SmoothieBoard ~ it has quite a bit of ventilation, as the AirPort is passively cooled ~ it has slots all along the sides ~ top and bottom ~  and a thousand tiny holes on the bottom (once the rubber base is removed) ~  



I am thinking I might want to put a fan on it for good measure ~ alas it won't be so pretty then ~ but practical trumps pretty!  



Any instruction for a not so technical pirate sort on installing a cooling fan for the board would be most appreciated.



~Still yet to cut holes for the power and stepper driver wiring ~ but I think it's a nice fit ~ ;{) 



![missing image](https://lh3.googleusercontent.com/-zTssrKYekKQ/Vb-iGk2mfVI/AAAAAAAAAys/tvxre6LeRBM/s0/AirPortSmoothieBoard.jpg)
![missing image](https://lh3.googleusercontent.com/-o5gh0ZlhOlQ/Vb-iGvWq8-I/AAAAAAAAAyw/whh_uMPMpzk/s0/AirPortSmoothieBoardFront.jpg)
![missing image](https://lh4.googleusercontent.com/-fzULU1V4Wjc/Vb-iGR8LJwI/AAAAAAAAAyo/gzKTh_zMNLQ/s0/AirPortSmoothieBoardTop.jpg)

**"Chapeux Pyrate"**

---
---
**Chapeux Pyrate** *August 03, 2015 17:39*

Oh.. I set the SmoothieBoard on some little spacers ~ umm... stand offs is what I think they are called ~ so there is air down there!  (just in case you were wondering...) 


---
*Imported from [Google+](https://plus.google.com/116524126135527852419/posts/gQCEhbDrxFQ) &mdash; content and formatting may not be reliable*
