---
layout: post
title: "Yesterday my VFD expansion boards arrived from China"
date: August 04, 2015 07:56
category: "General discussion"
author: "Bouni"
---
Yesterday my VFD expansion boards arrived from China. I will give them away for 3-5 Money units depending on what parts one will have on them. A description can be found here [http://smoothieware.org/spindle-module](http://smoothieware.org/spindle-module)



![missing image](https://lh3.googleusercontent.com/-0wIwIg37r1U/VcBuurHXKHI/AAAAAAAAACs/GPwOy8UnDT4/s0/IMG_20150804_074949.jpg)
![missing image](https://lh3.googleusercontent.com/-fYJZFnOFdQg/VcBu8OAkfgI/AAAAAAAAADU/TrDHJtcQV-4/s0/IMG_20150804_082330.jpg)
![missing image](https://lh3.googleusercontent.com/-zxsDyYrvct8/VcBu6AOOZSI/AAAAAAAAADM/Z5ilK_wggJs/s0/IMG_20150804_080236.jpg)
![missing image](https://lh3.googleusercontent.com/-UVHevsjVBpw/VcBuzckLu_I/AAAAAAAAAC0/cYCijzQmH0E/s0/IMG_20150804_075006.jpg)
![missing image](https://lh3.googleusercontent.com/-5I-zhzQ9Mtg/VcBu2bF8IvI/AAAAAAAAAC8/C7mN5erzqvo/s0/IMG_20150804_075550.jpg)
![missing image](https://lh3.googleusercontent.com/-fUJ3SQBffLI/VcBu4aGUSYI/AAAAAAAAADE/ud43zb3siX8/s0/IMG_20150804_075803.jpg)

**"Bouni"**

---
---
**Arthur Wolf** *August 04, 2015 14:53*

**+Anatoli Bugorski** Yay ! Tell me when you test them :)


---
**Bouni** *August 05, 2015 04:40*

Will do that! I hoped to do it yesterday but i had to little time and a lot of other things to do. But at least i managed to solder the MAX3485. 


---
**Bouni** *August 05, 2015 10:43*

**+Arthur Wolf** Just did a quick test. Not with a real VFD because i'm at work and the VFD is in our Hackerspace, but i verified the data using a USB-RS485 adapter. So it seems to work just fine :)


---
**Bouni** *August 10, 2015 07:17*

**+Arthur Wolf** Did the real test on Saturday and as expected everything worked fine! The only downside of the PCB's is that I messed the silk screen and therefor the labels of the pins are missing.


---
**Arthur Wolf** *August 10, 2015 08:58*

**+Anatoli Bugorski** Yay !!!! I'm so glad this works.


---
**Bouni** *August 10, 2015 10:34*

I'll send you some of the boards if you want to heve them. Including all parts except for the opamp and the opto coupler (they were quite expensive on aliexpress, and most people will go for the modbus solution anyway). Just send me an email with you snail mail address.


---
**Arthur Wolf** *August 10, 2015 16:32*

**+Anatoli Bugorski** Arthur Wolf, 14 rue amiral vallon, 29200 BREST, France.

I only need whatever is needed for modbus, as that's all I'll be using.

Thanks a ton !


---
**Jestah Carnie** *January 07, 2017 10:46*

**+Bouni** Thanks for doing the hard yards and getting this working! Do you have any PCB left? Could you tell me what the SJ1 part is on the modbus version? 



Cheers!


---
**Bouni** *January 07, 2017 10:51*

**+Jestah Carnie** You can buy them from **+Arthur Wolf** 


---
**Jestah Carnie** *January 07, 2017 11:40*

Thanks **+Bouni** for sending me in the right direction. Is SJ1 a solder jumper? If so why would you want the resistor in or out of the circuit? 




---
*Imported from [Google+](https://plus.google.com/103730302715483777197/posts/1xug3TDyLoY) &mdash; content and formatting may not be reliable*
