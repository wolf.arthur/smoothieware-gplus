---
layout: post
title: "Arthur Wolf Bonjour Arthur ~ hey I am just about ready to do the wiring to the smoothieboard ~ X-Carve set up (Inventables.com)~ and I am not so sure of how to connect the spindle ~ of course I would like to control the"
date: August 16, 2015 16:04
category: "General discussion"
author: "Chapeux Pyrate"
---
**+Arthur Wolf**  Bonjour Arthur ~ hey I am just about ready to do the wiring to the smoothieboard ~ X-Carve set up (Inventables.com)~ and I am not so sure of how to connect the spindle ~ of course I would like to control the spindle with Smoothieware ~ the spindle I have (from Inventables) is a 24V (I have a 24V power supply ~ the PS has connections and an on off switch for the spindle) ~ it's 300W ~ not so sure of the Amps ~ but it seems to be a rather common spindle for diy cnc ~  Inventables gives some vague instruction on connecting and controlling relating to the GShield  ~ but between those instructions and the ones for CNC I found on Smoothieware... well I am a bit shy of clueless ~  >Grinn<



anyway, I am a bit unclear looking at the info on Smoothieware.org of just how to go about connecting and controlling the spindle... I would greatly appreciate you sending me to the most proper instructions on the matter.  Merci! 





**"Chapeux Pyrate"**

---
---
**Chapeux Pyrate** *August 16, 2015 16:21*

Here is Inventables description of the Power Supply ~ it mentions "integrated" spindle control ~ 



"24V power supply can power both the 24VDC spindle and the stepper motors. The power supply is designed to work with input voltage ranging from 85-264VAC. There's a switch to toggle between voltages. It also includes integrated spindle speed control."


---
**Arthur Wolf** *August 19, 2015 11:17*

**+Chapeux Pyrate** Hey !

Did you read [http://smoothieware.org/cnc-mill-guide](http://smoothieware.org/cnc-mill-guide) , that should have most of the information you need, what is the problem exactly ?

To help you just in case, the basic steps of controlling a spindle :

* Provide power to the big mosfets

* Choose a big mosfet output, add a diode to it ( see fan documentation ), and connect the spindle to it

* Configure the spindle module ( or a switch module ) to control that big mosfet, and therefore the spindle



Tell me if you need any info that is more specific than this.



Cheers.


---
**Chapeux Pyrate** *August 19, 2015 11:26*

**+Arthur Wolf** Thanks Arthur ~ yes I did read through the cnc mill guide ~ still a bit greek to me ~ but a good lead you have given me is to "see fan documentation" ~ as I only have the slightest idea of what a diode is or how or where to add such a thing.  



I am so utterly new to working on a board, soldering on such a thing, jumping, pinning out etc..  ~ a bit intimidating ~ but I am taking in some instructions on the matter (in general) to try to get a feel for doing so.  


---
**Chapeux Pyrate** *August 19, 2015 12:21*

**+Arthur Wolf**  HA! I was thinking "Fan Documentation" was some place where Fans of SB were sharing their ideas ~ I may be hopeless here!!!  ;{P



Ok.. so I am better informed on the Diode thing ~ (found it on the page that offers documentation on fans!!!)   ~ the diode recommended there is 1N4004 diode  ~ is that what I would use on the big mosfet for the spindle?  


---
**Chapeux Pyrate** *August 19, 2015 12:25*

Huzzah! Thanks to Instructables.com I now actually know what a diode does!!!  Geeze sorry mates ~ I am so pre kindergarten here ~ you all must be embarrassed for me!!!  

>ShaGrin!< 


---
*Imported from [Google+](https://plus.google.com/116524126135527852419/posts/Ai9dyMfGa9n) &mdash; content and formatting may not be reliable*
