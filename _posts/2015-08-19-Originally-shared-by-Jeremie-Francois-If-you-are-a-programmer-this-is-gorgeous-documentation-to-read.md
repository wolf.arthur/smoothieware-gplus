---
layout: post
title: "Originally shared by Jeremie Francois If you are a programmer, this is gorgeous documentation to read!"
date: August 19, 2015 18:27
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Jeremie Francois</b>



If you are a programmer, this is gorgeous documentation to read! Alas, I wish more projects did the same :)

Excellent work by **+Arthur Wolf**: Smoothie is an open source firmware for ARM 32 bits, and how/where to tinker with it is very well explained here.





**"Arthur Wolf"**

---
---
**ThantiK** *August 19, 2015 20:40*

This has been an excellent guide to how the firmware is laid out.  I wish more open source projects had documentation like this.  Maybe some day I'll dive in and fix smoothies stupid alpha beta gamma omega axis assignment and keep a fork.


---
**Wolfmanjm** *August 19, 2015 21:53*

it is not stupid there is a reason for it, which most people do not understand. There is a difference between cartesian axis XYZ and actuator axis. most notable on a delta and corexy, but also on some cartesians.

A smoothie is designed to work on all kinds of systems (CNC, 3D printer, Laser etc) this makes much more sense.

If more people understood the difference between Cartesian coordinates and actuator coordinates things would work much better.



 Just because you do not understand something does not make it stupid ;)


---
**ThantiK** *August 19, 2015 22:54*

I understand actuator axis, my planned change is simply axis a b c d e f g h, etc.  Not alpha omega...because who has the time for that shit? (Not everyone is taught it, but everyone knows their abc's)



It's more about removing a papercut/logical barrier to entry.



I've heard others using the smoothieboard say "it was a bit difficult to configure at first, because of the way everything was named, but after a couple of hours I got it"...



Why should someone have to take hours to learn what your methodology is, simply because you're stubborn?  It's more about practicality than this stubborn need to be different for the sake of being different.


---
**Arthur Wolf** *August 19, 2015 22:56*

**+ThantiK** but ABC etc is used for the rotary axes so we can't use that for actuators ...

If somebody doesn't know the first three letters of the greek alphabet ( which seems very hard to believe if they have any basic education, they are even used in common speak expressions etc ), it's super fast to figure them out from documentation or config examples ...


---
**ThantiK** *August 19, 2015 22:59*

Nowhere in the USA are those used in commonly spoken language.  Maybe in tiny countries it's used more often, but in the most populous consumer nation in the world...nope.


---
**Arthur Wolf** *August 19, 2015 23:04*

Well alpha and beta are used for software releases. "Alpha and Omega" is like, religious stuff if I remember correctly. Alpha-male, Gamma-rays, Alpha waves, Beta-caroten, etc ... What I mean is it's not like people have never heard those in their lives.


---
**Wolfmanjm** *August 19, 2015 23:39*

if you were to make that change I warm you you will forever be in a world of hurt. You would never be able to merge with upstream again. And if you were to use G and F as an axis you would find very little would work at all. I think stubborn is a word that you could apply to yourself :)


---
**Artem Grunichev** *August 20, 2015 08:24*

**+ThantiK** okay, lets use а б в г д е ё ж з, итд., if you don't like greek, everybody know it there :)


---
**Artem Grunichev** *August 20, 2015 08:25*

**+ThantiK** >most populous

- oh, I know! Chinese! Is there alphabet in Mandarin?


---
**Jack Colletta** *August 20, 2015 12:52*

Stupid is a rough word for describing a teams naming convention philosophy.   As all software developer know if you challenge 5 of them to solve a problem you will get 5 code sets.  They may all work, some better than others but to call one solution stupid is wrong.



I come from a XYZ world but when I read smoothie's axis name it only took me a couple of minutes to figure out and once I had the parameters set I have not had to touch them again.



Arthur and Wolfmanjm keep up the good work


---
**ThantiK** *August 20, 2015 13:01*

**+Артем Груничев**, I said consumer nation, not slave nation.


---
**Arthur Wolf** *August 20, 2015 13:17*

**+ThantiK** I think maybe the naming convention is only confusing to people coming from Marlin, that also don't read the documentation ( it's explained in detail and tables there ).

What's your main problem ? That using greek letters means people can't figure out the order of the actuators, or that they can't figure out the distinction between actuators and axes ?



[https://docs.google.com/document/d/1JEcIxRvH6L158aHFcFJtFToKWCGPPMwlF1e7OzqG57g/edit](https://docs.google.com/document/d/1JEcIxRvH6L158aHFcFJtFToKWCGPPMwlF1e7OzqG57g/edit)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/LBen43YkCnX) &mdash; content and formatting may not be reliable*
