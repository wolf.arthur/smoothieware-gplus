---
layout: post
title: "Arthur Wolf ~ wanted you to have a look at what I hope is the final wiring schematic for the X-Carve Spindle Control to SmoothieBoard ~ as per our discussion yesterday ~ the test (connect to 3.3v and gnd) seemed to work fine"
date: August 26, 2015 12:45
category: "General discussion"
author: "Chapeux Pyrate"
---
**+Arthur Wolf** ~ wanted you to have a look at what I hope is the final wiring schematic for the X-Carve Spindle Control to SmoothieBoard ~ as per our discussion yesterday ~ the test (connect to 3.3v and gnd) seemed to work fine ~ spindle turned on when connection was made.  

;{) Thanks **+Wolfmanjm** for that bit of direction.  



So I will seek your nod on this and then if correct proceed with the connection ~ 



Boy the SmoothieBoard is a danged hardy board ~ no blue flames or smoke yet ~ and well ~ it's not like we haven't tried to set it afire!!! >Grinn<

![missing image](https://lh3.googleusercontent.com/-94zapEEBqjY/Vd2023N5ZgI/AAAAAAAAA8Y/nURhAlybfYU/s0/X-CarveSpindleControlFinal.jpg)



**"Chapeux Pyrate"**

---
---
**Arthur Wolf** *August 26, 2015 13:27*

Yeah that looks about right


---
**Chapeux Pyrate** *August 26, 2015 15:00*

**+Arthur Wolf** Ha! I love when you say that! Well nothing melted ~this time!  ;{P


---
**Chapeux Pyrate** *August 26, 2015 15:22*

**+Arthur Wolf** So I guess you might be wondering what happened ~ connected as shown above ~ with output_type to digital ~ PS Spindle control switch to Logic(auto) ~ powered everything up and the Spindle was off on startup ~ as it's supposed to be~  yeehoo!   and in Pronterface I got starts and stops ~ M3 and M5  ~ but no speed changes ~ like sending M3 S50  ~ or M3 S128 ~ or M3 S250 ~ always the same pitch of the spindle ~ I supposed that's how I would gauge if there is a speed change ~  hmmm....



So I changed the switch.spindle.output_type back to pwn ~ and speed changes are happening ~ Yee Hoo again! 



Thanks for you help on this and to **+Wolfmanjm** too ~most appreciated! 



hmm.. while I have you... 

 I understand how the S numbers are related to the 0-255 pwn range ~ S128 would be about half speed ~  and am I to expect that S255 would be running the spindle about it's max rpm rating ~ in my case 12000 rpm ~ is that correct?


---
**Arthur Wolf** *August 26, 2015 15:52*

ok so you probably want the output_type set to hwpwm


---
**Chapeux Pyrate** *August 26, 2015 17:15*

**+Arthur Wolf** Ok.. I'll try that ~ to be clear I am saying that it is working ~ speed ~ values between 0-255 ~ can you explain a bit why you are suggesting hwpwm  ~ I will go ahead and change it just because I am curious however...  

;{)



Got the Limit Switches all working ~ X-Carve uses 3 ~ well that's what they send you ~ so they are testing well M119 etc..  I will need to read further to figure out the homing thing... but I seem to be getting the hang of this.  Merci! 


---
**Arthur Wolf** *August 26, 2015 17:16*

**+Chapeux Pyrate** hwpwm uses hardware pwm so it'll work better ( or the same ) and use less ressources on the microcontroller


---
**Chapeux Pyrate** *August 28, 2015 16:33*

**+Arthur Wolf** Good to know thanks! 


---
**Arthur Wolf** *August 28, 2015 16:34*

**+Chapeux Pyrate** once it works all fine, you owe me a video of the machine cutting, with g-code control of the spindle at the beginning and end, for my youtube channel :)


---
**Chapeux Pyrate** *August 28, 2015 22:19*

**+Arthur Wolf** will do! I will be very happy to document on video and also make up some clean wiring diagrams etc... specific to the X-Carve ~ I am exploring a few different ways to control the SB/XCarve right now ~ pronterface from RPI2 and from my Mac as well ~ learning the ropes ~very basic ~ manual control works well (sending codes and jogging).  I will most likely use the RPI2 connected to my Mac through ethernet ~ figured out how to set the RPI2 to show up (bonjour) on the Finder window ~ one button click to open in Remote Desktop ~ how's them "Apples" ~ so then I can control from my Mac but all is done on the RPI2 ~ streaming to the SmoothieBoard via Ethernet from the Pi.  Not bad for a Pirate Artisan type eh!?


---
**Arthur Wolf** *August 28, 2015 22:19*

:)


---
**Chapeux Pyrate** *August 28, 2015 22:21*

**+Arthur Wolf** Of course once I get some results I will be glad to boast of the SmoothieBoard/X-Carve on Inventables forum ~ I know of a few who are looking at the SmoothieBoard as their controller for the X-Carve ~ I am planning to post a step by step on the forum.  


---
**Arthur Wolf** *August 28, 2015 22:22*

Cool, forum posts are good :)


---
**Chapeux Pyrate** *August 28, 2015 22:23*

**+Arthur Wolf** Sell more SmoothieBoards and get a lively X-Carve community going around the board ~ so I won't have to think so much as I have been doing!  HAR! 


---
**Chapeux Pyrate** *August 28, 2015 22:26*

**+Arthur Wolf** Oh, I am also looking at alternatives to pronterface on the Pi ~ maybe GRBLWeb ~ recently came out with a Raspberry Pi2 version ~ I am checking out how to install it on my Pi2 right now... just to see if it will work with the SB ~GRBLWeb offers CNC specific gui ~ that would be nice to have ~ so not to feel like a second class citizen as with Pronterface. Have you ever tried GRBLWeb? 


---
**Chapeux Pyrate** *August 28, 2015 22:34*

**+Arthur Wolf**  Just asked to join the X-Carve G+ community ~ you may want to have look there ~I know of one other person who is using the Smoothieboard on the X-Carve (well the Shapeoko2) **+Adam Harris**  ~ you may know him already.  


---
*Imported from [Google+](https://plus.google.com/116524126135527852419/posts/azqWcJ5vTwm) &mdash; content and formatting may not be reliable*
