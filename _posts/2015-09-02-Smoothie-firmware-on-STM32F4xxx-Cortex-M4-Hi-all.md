---
layout: post
title: "Smoothie firmware on STM32F4xxx (Cortex-M4) Hi all"
date: September 02, 2015 16:23
category: "General discussion"
author: "Jorge Pinto"
---
<b>Smoothie firmware on STM32F4xxx (Cortex-M4)</b>



Hi all.



At BEEVERYCREATIVE, we are starting investigating the possibility to use an STM32F4xxx for a control board and porting the Smoothie firmware.

We would be happy to get information about a port of Smoothie firmware for STM32F4, if such port exist.





**"Jorge Pinto"**

---
---
**Arthur Wolf** *September 02, 2015 18:18*

**+Jorge Pinto** Clement started a port to STM32F4 : [https://github.com/clementleger/Smoothieware-STM32](https://github.com/clementleger/Smoothieware-STM32)

But as Smoothie is being ported to LPC43XX, he is probably going to join that effort, and try to make Smoothie2 ( Smoothie ported to LPC43XX, with a lot of refactors ) also work on STM32F4.


---
*Imported from [Google+](https://plus.google.com/116288848506537001171/posts/hYH4otdTB4s) &mdash; content and formatting may not be reliable*
