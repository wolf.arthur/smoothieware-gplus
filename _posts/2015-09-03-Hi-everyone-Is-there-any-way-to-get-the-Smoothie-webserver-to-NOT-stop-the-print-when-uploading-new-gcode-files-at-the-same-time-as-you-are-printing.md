---
layout: post
title: "Hi everyone :) Is there any way to get the Smoothie webserver to NOT stop the print when uploading new gcode files at the same time as you are printing?"
date: September 03, 2015 11:42
category: "General discussion"
author: "David Bassetti"
---
Hi everyone :)   

Is there any way to get the Smoothie webserver to NOT stop the print when uploading new gcode files at the same time as you are printing? ?





**"David Bassetti"**

---
---
**Wolfmanjm** *September 04, 2015 04:41*

no there is no way. ANY writes to the sdcard while printing will cause the print to pause, There is a reason that the WIKI states in multiple places to not write to the sdcard while printing and to unmount it from the host while printing. I am not sure why you would even want to upload a file while a print is ongoing. The MCU is dedicated to generating steps as fast as it can, this is not a multi user, multi thread, multi tasking type system.


---
**David Bassetti** *September 04, 2015 10:47*

I just did it by accident :)


---
**Clarence Lee** *September 07, 2015 00:57*

Use FlashAir sd card to upload through wifi instead.


---
**David Bassetti** *September 10, 2015 12:03*

Thanks Clarence .>!  I didn't even know a wifi sdcard existed..!

But it's a standard size so wont fit the smoothie directly ?


---
*Imported from [Google+](https://plus.google.com/+DavidBassetti/posts/j1ytEYAshAV) &mdash; content and formatting may not be reliable*
