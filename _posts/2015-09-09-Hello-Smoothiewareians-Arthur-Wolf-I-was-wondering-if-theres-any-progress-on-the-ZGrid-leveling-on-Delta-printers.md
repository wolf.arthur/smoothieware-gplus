---
layout: post
title: "Hello Smoothiewareians, Arthur Wolf I was wondering if there's any progress on the ZGrid-leveling on Delta printers?"
date: September 09, 2015 15:37
category: "General discussion"
author: "Chris Wilson"
---
Hello Smoothiewareians, **+Arthur Wolf** 



I was wondering if there's any progress on the ZGrid-leveling on Delta printers?﻿. Is this something I can now incorporate into my Kossel pro?  Last I've read, Arthur said not to use it on Deltas. 



Any update would be appreciated. Thanks in advance. 





**"Chris Wilson"**

---
---
**Douglas Pearless** *September 09, 2015 20:56*

Hmm, it should work as I updated it for Rotary Delta mechanisms ( check out [firepick.org](http://firepick.org) for more details) and I have been using it quite a bit.


---
**Chris Wilson** *September 09, 2015 20:57*

I guess I'm not fully understanding the configuration settings.  I've set the defaults, and the calibration only runs on the back half of the plate, and when it comes time to print, the head is about 5 -8mm above the print surface..


---
**Douglas Pearless** *September 09, 2015 21:17*

Ah, While I updated the code to support Rotary Delta mechanisms, I do not recall back-porting the Z-Grid changes to linear delta (e.g. Kossel) mechanisms.  If you want to, look at the Rotary Delta robot arm mechanism code and you will see how I updated the z-grid code to have x,y at 0,0 being in the middle and not a corner of the bed & I suspect that may be your issue.  It was part of the Edge branch and if memory serves me it is "feature/rotary-delta" n the main Smoothie repo.  



If you cannot find it, look at my fork where I did the development for this: [https://github.com/DouglasPearless/Smoothieware](https://github.com/DouglasPearless/Smoothieware) it should be in my "edge" branch if you cannot find it in the current branch in my repo.


---
**Chris Wilson** *September 09, 2015 22:00*

I do have X/Y zeroed at the center of the build plate - which is what's confusing me so much.



Thanks for the links, I'll take a look when I get home.



Cheers.


---
**Wolfmanjm** *September 10, 2015 00:50*

**+Chris Wilson** ask **+Quentin Harley** he wrote it. AFAIK there is a setting for setting 0,0 center of the bed. If you find error please file an issue on github with [ZGRID] in the header so Quentin can fix it ;)


---
**Wolfmanjm** *September 10, 2015 00:51*

**+Chris Wilson** did you try the regular delta calibration strategy? I get really good results with it.


---
**Chris Wilson** *September 10, 2015 00:54*

**+Wolfmanjm** when you say regular delta calibration , which one are referring to?



leveling-strategy.delta-calibration ?


---
**Wolfmanjm** *September 10, 2015 01:01*

**+Chris Wilson** Yes set...

   leveling-strategy.delta-calibration.enable   true            # basic delta calibration

   leveling-strategy.delta-calibration.radius   100             # the probe radius



in the config


---
**Chris Wilson** *September 10, 2015 01:04*

The issue I'm having with that, is when I run a G30, it works fine.  Moves slowly down until it touches the bed, then retracts.  however, when I try to run a full G32, for some reason it's trying to move my effector at the home position, which is causing the belts and motor to skip when hitting the end-stops at the top of the rails.  It's not moving down to the pre-set 5mm mark before running the probe. :/  Its driving me nuts!


---
**Wolfmanjm** *September 10, 2015 05:08*

did you follow the wiki guidelines? and disconnect the host (ie unmount / safely eject) the sdcard before running G32? and if G32 for that doesn't work then none of the ABL will work for the same reason, they all use the same code.


---
**Wolfmanjm** *September 10, 2015 05:09*

that does also sounds like a possible configuration issue... if you paste your current config and results from M503 to pastebin I can take a look at it.


---
**Chris Wilson** *September 10, 2015 05:16*

I did follow the guide and I'm coming across something quite odd. I've managed to fix the issue and successfully completed my first print. Not too bad either, just some blebs to deal with. 



Anyways, I've found an issue with the probe where an M119 shows the probe value at 1 in BOTH positions (up and down), so I found that I can fix this by inverting the probe pin 1.29^ to 1.29!^, restarting the board, then switching back to 1.29^, then restarting one more time. Then the M119 will show the proper 1 and 0 pin settings for up and down respectively. 



Very strange, but it seems to work. I'm actually waiting for a replacement board to be sent, as there seems to be other issues that are popping up.  Poltergeists, if you will... 



Anyways - thanks for all your help with this, I appreciate your patience with my learning curve to the smoothieware platform. ﻿


---
**Chris Wilson** *September 10, 2015 05:20*

I would, however take you up on the offer to browse over my M503 output. You might see some glaring issue that might help. I'll post it tomorrow and let you know. 



Cheers, and thanks again. 


---
*Imported from [Google+](https://plus.google.com/112111122069927621889/posts/8jVTK9zUUHA) &mdash; content and formatting may not be reliable*
