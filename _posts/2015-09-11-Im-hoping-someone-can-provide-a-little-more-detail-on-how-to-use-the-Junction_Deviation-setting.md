---
layout: post
title: "I'm hoping someone can provide a little more detail on how to use the \"Junction_Deviation\" setting"
date: September 11, 2015 16:37
category: "General discussion"
author: "Chris Wilson"
---
I'm hoping someone can provide a little more detail on how to use the "Junction_Deviation" setting.  I understand it's essentially the jerk setting on Marlin, however, I'm not sure how the numbers translate.  For example, I'm running 10-12% jerk of my max speed on my Cartesian and that setting works nicely.  However I feel like the Junction_deviation setting might need to be adjusted on my Kossel pro, i'm just not sure how or what sort of calculations need to be done in order to get the result I'm looking for?



Thanks in advance,





**"Chris Wilson"**

---
---
**Arthur Wolf** *September 11, 2015 16:38*

A good default is 0.05. Multiply it by 10, and test. Divide it by 10 and test. Then choose whichever direction you like most, and adjust from there.


---
**Chris Wilson** *September 11, 2015 16:42*

**+Arthur Wolf** Perfect - Thank you, I'll give that a shot. :)


---
**Jeremie Francois** *September 11, 2015 19:07*

**+Arthur Wolf** weird! it sounds like a "pragmatic" hack (not to say dirty), doesn't it?


---
**Arthur Wolf** *September 11, 2015 19:26*

**+Jeremie Francois** Well that's about how people determine acceleration too. There are so many parameters here you can't be fully deterministic ...


---
**Jeremie Francois** *September 11, 2015 20:03*

**+Arthur Wolf** yep, I never saw any good model, there are always a few semi-arbitrary constants. I would bet that the majors also rely on secret and heavily guarded black magic ;)


---
**Arthur Wolf** *September 11, 2015 20:42*

**+Jeremie Francois** If you had some way to feed Smoothie a "print quality" signal, that'd tell it if a print is more bad, or more good, it'd be able to auto-tune all that. But baring that, it has to be done by a human.


---
**Hakan Evirgen** *September 11, 2015 21:20*

just to understand it, what exactly does the parameter do? I prefer to understand things before changing them.


---
**Arthur Wolf** *September 11, 2015 21:21*

**+Hakan Evirgen** junction_deviation essentially defines how much to slow down when turning, proportional to the angle. Higher values means you'll take corners faster, lower values means you'll slow down for corners more.


---
**Hakan Evirgen** *September 11, 2015 21:45*

so that means if my prints are not exact at the round parts, I should lower this value?


---
**Arthur Wolf** *September 11, 2015 21:46*

**+Hakan Evirgen** Yes, also if your machine shakes too much, lower either acceleration or junction_deviation


---
**Chris Wilson** *September 11, 2015 22:53*

**+Arthur Wolf**  I very much appreciate the time you take to explain this to us. Not everyone who develops and markets something, takes the time with their followers/product owners.  It's very refreshing, I for one am thankful. 


---
**Arthur Wolf** *September 11, 2015 22:53*

**+Chris Wilson** \o/


---
**Hakan Evirgen** *September 12, 2015 00:11*

100% ack with Chris Wilson


---
**Jeremie Francois** *September 12, 2015 08:24*

ditto, thanks **+Arthur Wolf** for explaining it!Understanding what goes on is the first step to improvement! And 100% agreed with some machine learning there. But we need more closed loops to do so indeed. Sensing/detecting the results properly is something yet to be done. I fear that separating the impact of every and each of the sensor may never be possible.

This is also why my next printer will be ARM32 (ie. why I consider the Arduino as obsolete). We need room to program real time feedback, analysis and eventually control.


---
*Imported from [Google+](https://plus.google.com/112111122069927621889/posts/jekkYGaMWsm) &mdash; content and formatting may not be reliable*
