---
layout: post
title: "So I have this Smoothieboard 5X lying around and waiting for my parts for the Delta"
date: September 12, 2015 14:30
category: "General discussion"
author: "Hakan Evirgen"
---
So I have this Smoothieboard 5X lying around and waiting for my parts for the Delta. In the mean time I wanted to test it, so I hooked it up with my Prusa. I also have the Viki2 LCD here.



As per the manual it is not possible to use all the features of the LCD. But after disabling onboard LEDs this is not a problem. And I could use all features. So am I missing something?



And anyway it would be good to have an example wiring in the Wiki for the Viki2. Also the information that for the Smoothieboard it should be ordered with the universal cable.





**"Hakan Evirgen"**

---
---
**Arthur Wolf** *September 12, 2015 14:32*

**+Hakan Evirgen** The Viki2 was not really designed for the Smoothieboard, but it's still possible to wire it. This page : [http://smoothieware.org/panel#toc10](http://smoothieware.org/panel#toc10) has the configuration settings, and from those you can see what to connect where. It'd be nice if somebody showed an example wiring indeed.


---
**Hakan Evirgen** *September 12, 2015 15:00*

Actually I could do an example configuration where all features work even on the 5X board.


---
*Imported from [Google+](https://plus.google.com/+HakanEvirgen/posts/hdHvXmhy1WT) &mdash; content and formatting may not be reliable*
