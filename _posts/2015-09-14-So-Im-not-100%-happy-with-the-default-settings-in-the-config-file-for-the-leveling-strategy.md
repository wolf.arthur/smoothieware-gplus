---
layout: post
title: "So I'm not 100% happy with the default settings in the config file for the leveling-strategy"
date: September 14, 2015 04:02
category: "General discussion"
author: "Chris Wilson"
---
So I'm not 100% happy with the default settings in the config file for the leveling-strategy.



I'm trying to move to the ZGrid-leveling solution, but I'm coming across some odd things happening.  I need to probe a few more areas than just the 3 that are in the delta-calibration setting, as the areas that are not probed are the ones that seem to be causing the problem.



My ZGrid settings are as follows:



leveling-strategy.ZGrid-leveling.enable         true

leveling-strategy.ZGrid-leveling.bed_x          75

leveling-strategy.ZGrid-leveling.bed_y          75

leveling-strategy.ZGrid-leveling.rows           5          # X divisions (Default 5)

leveling-strategy.ZGrid-leveling.cols           5          # Y divisions (Default 5)

leveling-strategy.ZGrid-leveling.probe_offsets  21,0,8.5

leveling-strategy.ZGrid-leveling.slow_feedrate  200





The problem I'm having is it only seems to probe half my surface from the mid-point back to the Z tower.  nothing on the half closer to the X/Y towers.  I'm not sure I understand why this is happening.  Can anyone provide some insight into this?  I've attached a video of what I'm experiencing.  Thanks in advance,





**"Chris Wilson"**

---
---
**Arthur Wolf** *September 14, 2015 08:40*

Also, grid levelling is still new on delta machines, you should get the normal delta calibration ( which will be largely sufficient on a glass bed ) to work first before trying grid.


---
**Chris Wilson** *September 14, 2015 14:17*

**+Alex Skoruppa** I started with the 200 x 200 settings, but the effector would move right off the bed, so I tried shrinking that number.



**+Arthur Wolf** I started with the standard delta calibration which worked, but the variances in bed height seemed to be 90 from where the bed was probed: ie: directly between X/Y towers (X0 Y-100) and so-on.



Mabye there's settings I'm missing in the delta calibration that would help?


---
**Arthur Wolf** *September 14, 2015 20:42*

What sort of bed do you have ? What are you using as a probe for the calibration ?


---
**Chris Wilson** *September 14, 2015 20:52*

The probe is the stock injected plastic probe that comes with the Kossel Pro: [https://onedrive.live.com/view.aspx?cid=070E802A1A3EE13C&resid=70E802A1A3EE13C!1872&app=PowerPoint](https://onedrive.live.com/view.aspx?cid=070E802A1A3EE13C&resid=70E802A1A3EE13C!1872&app=PowerPoint)



I think I'm going to switch to FSRs pretty soon here.



As for the bed, it's a heated PCB with a 3mm borosilicate glass top.  I've checked it with a straight-edge when the bed's at temperature, its not terribly warped, just the bow is where the probe doesn't touch. :/


---
**Arthur Wolf** *September 14, 2015 20:56*

**+Chris Wilson** Yeah then you do indeed need grid. Is your firmware the latest version ?


---
**Chris Wilson** *September 14, 2015 20:59*

I'm running a version of smoothie-edge - it's on an X5.  I will upgrade if you need me to.  lol  Just tell me what to do.  I'd love to get the grid working!! :)


---
**Arthur Wolf** *September 14, 2015 21:01*

**+Chris Wilson** Try flashing the latest : [http://smoothieware.org/flashing-smoothie-firmware](http://smoothieware.org/flashing-smoothie-firmware)


---
**Wolfmanjm** *September 15, 2015 07:11*

I do not think zgrid works on deltas yet. ask **+Quentin Harley**  he wrote it.

You do need to tell it that the 0,0 is the center of the bed not the front left corner.


---
**Chris Wilson** *September 15, 2015 17:36*

That's exactly what **+Arthur Wolf** and I surmised. I'll get in touch with him, thanks. :)


---
*Imported from [Google+](https://plus.google.com/112111122069927621889/posts/JXcQoMm8qB1) &mdash; content and formatting may not be reliable*
