---
layout: post
title: "Big clean prints :)"
date: September 17, 2015 14:14
category: "General discussion"
author: "David Bassetti"
---
Big clean prints :)



![missing image](https://lh3.googleusercontent.com/-X-qabgrH2iE/VfrKnhI4HQI/AAAAAAAAB-g/LNYJJXF8RHI/s0/IMG_3270.JPG)
![missing image](https://lh3.googleusercontent.com/-dRZJ3vIL7bw/VfrKnqRKoQI/AAAAAAAAB-o/6je5nBKMIIc/s0/IMG_3272.JPG)
![missing image](https://lh3.googleusercontent.com/-ZRFlc6yXX4M/VfrKnZyUIPI/AAAAAAAAB-c/V22bIuIPLWQ/s0/IMG_3276.JPG)
![missing image](https://lh3.googleusercontent.com/-0mTXfESybSo/VfrKo9vU5NI/AAAAAAAAB-s/IyZxsBvvboc/s0/IMG_3280.JPG)

**"David Bassetti"**

---
---
**Electra Flarefire** *September 17, 2015 18:53*

Can I get that 3d model?

That's a nice print!


---
**David Bassetti** *September 18, 2015 08:20*

**+Electra Flarefire**

no, its our clients property :)

But you can download hundreds of the same sort of bottles on say thingiverse etc


---
*Imported from [Google+](https://plus.google.com/+DavidBassetti/posts/adspC3EnYLq) &mdash; content and formatting may not be reliable*
