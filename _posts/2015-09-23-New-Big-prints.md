---
layout: post
title: "New Big prints :)"
date: September 23, 2015 13:26
category: "General discussion"
author: "David Bassetti"
---
New Big prints :)



![missing image](https://lh3.googleusercontent.com/-8x0uoe3oN7g/VgKogDkIUFI/AAAAAAAACDM/5wOGhnMiCd8/s0/Big%252Bprint.JPG)
![missing image](https://lh3.googleusercontent.com/-EdtIz3xpyu0/VgKofnVZ4WI/AAAAAAAACDA/ExPPDpvaR8g/s0/Front.JPG)
![missing image](https://lh3.googleusercontent.com/-7UqPnD-IUXI/VgKogRZMGyI/AAAAAAAACDU/QyZadD-oal0/s0/Starting.JPG)
![missing image](https://lh3.googleusercontent.com/-C0eL31n--m0/VgKog3zCPuI/AAAAAAAACDQ/Q9TL_kxhsek/s0/starting%252Bfirst.JPG)

**"David Bassetti"**

---
---
**Hakan Evirgen** *September 23, 2015 14:43*

What is that? What is the height and which printer?


---
**David Bassetti** *September 23, 2015 15:57*

Our Printer the GF 120, the parts a clients and I think it's over 420mm at 0.25mm layer height


---
**Hakan Evirgen** *September 23, 2015 16:09*

that is tall!


---
**Sébastien Plante** *September 24, 2015 22:18*

so, this took 3 days? :o


---
**David Bassetti** *September 25, 2015 07:59*

**+Sébastien Plante**

72 hours No... Simplify3D says 24hours at 80mm per second and 0.20 layer height...  so one day :)


---
*Imported from [Google+](https://plus.google.com/+DavidBassetti/posts/H5RxwrURmNa) &mdash; content and formatting may not be reliable*
