---
layout: post
title: "You eliminate one problem, only to grow another"
date: October 02, 2015 02:22
category: "General discussion"
author: "Chris Wilson"
---
You eliminate one problem, only to grow another.  Anyone have any idea how to eliminate this wood grain type pattern that's showing up in prints?  I was told it could be the "mm_per_line_segment" setting?  I've played with that value, even disabled it all together. Still getting this very odd outcome.



![missing image](https://lh3.googleusercontent.com/-TKszVDJMcJg/Vg3qLkylqLI/AAAAAAAAItI/yhYPaxa1uTc/s0/IMG_3862.JPG)
![missing image](https://lh3.googleusercontent.com/-4HEL1Q66x3M/Vg3qLnxtzXI/AAAAAAAAItI/9Xr0R36qgEA/s0/IMG_3861.JPG)

**"Chris Wilson"**

---
---
**Arthur Wolf** *October 02, 2015 09:01*

That's odd. It could be many things :(



Are you sure your machine is all tight and good on the mechanical side ?


---
**Ross Hendrickson** *October 02, 2015 14:00*

Jerk and acceleration, machine harmonics, not sure on the smoothie side how to play with settings to mess with jerk and acceleration.


---
**Chris Wilson** *October 02, 2015 14:56*

**+Arthur Wolf** Everything's nice and tight.  That was my first thought as well.  I've tripple checked everything that moves. :/  The print itself is very solid.  No issues structurally, you don't really feel the ridges.  They're there, but it's more of a cosmetic issue.  It'd be really handy if I'm printing with some Faux wood filament, but looks odd with everything else.


---
**Arthur Wolf** *October 02, 2015 18:13*

**+Chris Wilson** What sort of extruder do you have ?


---
**Chris Wilson** *October 02, 2015 18:18*

It came with the Printer.  It's a K-head hot end.  It's very similar to an E3D, but definitely not one.



I have ordered an E3D-V6 for it.  Waiting for it to arrive.


---
**Arthur Wolf** *October 02, 2015 18:25*

**+Chris Wilson** I mean is it bowden or direct-drive ?


---
**Chris Wilson** *October 02, 2015 18:30*

Oh.  Sorry.  It's a bowden. 1.75mm


---
**Arthur Wolf** *October 02, 2015 18:30*

Those ripples might be related to the fact it's a bowden ...


---
**Chris Wilson** *October 02, 2015 18:32*

That's odd.  I don't remember seeing those in the past.  But I was focusing on other stuff. 


---
**Arthur Wolf** *October 02, 2015 18:33*

**+Chris Wilson** those sorts of ripples are a very common complaint people with bowden have. It could be something else ... it's so hard to diagnose things with 3D printers.


---
**Chris Wilson** *October 02, 2015 18:35*

**+Arthur Wolf** lol.  I completely understand.  No worries.  I can live with it.  As I said, it's not affecting structural stability of the part, and It really only becomes evident with long straight pieces like the one pictured.


---
**Wolfmanjm** *October 03, 2015 07:11*

**+Chris Wilson** is this a delta? in which case you should not be setting mm_per_line_segment at all you would be setting delta_segments_per_second to 100 If you have the other set then delete it! and set delta_segments_per_second 100 and this kind of pattern would not be caused by that anyway.


---
**Wolfmanjm** *October 03, 2015 07:12*

That actually looks more like the infill bleeding through to the perimeter, try increasing the number of perimeters.


---
*Imported from [Google+](https://plus.google.com/112111122069927621889/posts/RBXZHikzPXV) &mdash; content and formatting may not be reliable*
