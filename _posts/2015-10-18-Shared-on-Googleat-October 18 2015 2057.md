---
layout: post
title: "Shared on October 18, 2015 20:57...\n"
date: October 18, 2015 20:57
category: "General discussion"
author: "Arthur Wolf"
---






**"Arthur Wolf"**

---
---
**ThantiK** *October 18, 2015 21:10*

Wow, I never even realized that the original smoothie was lacking an FPU.  Love the idea of an FPGA (Going with Lattice’s iCE40 FPGA?)


---
**Arthur Wolf** *October 18, 2015 21:12*

**+ThantiK** Yep, LPC1769 doesn't have a FPU. It's not been a real problem, we have plenty of power for what we are doing. But getting one will be very neat :)



For the FPGA we are going for a Spartan6 LX9. The same the papillio.cc guys use.


---
**ThantiK** *October 18, 2015 23:49*

I'm certainly interested in it too, regardless the cost.  I asked about the iCE40 FPGA because it apparently has a completely open source build environment ready for it.  v.s. the Spartan6 LX9, which I believe will require proprietary (potentially windows-only) software to compile.


---
**Jeff DeMaagd** *October 19, 2015 03:49*

It sounds like a lot of potential. I appreciate the updates. I'm eager to see it unfold.


---
**Bouni** *October 19, 2015 06:20*

**+ThantiK** Xilinx offers a linux toolchain free to download but still closed source: [http://www.xilinx.com/support/download.html](http://www.xilinx.com/support/download.html)


---
**Ryan Wirth** *October 19, 2015 09:09*

Very nice, thanks for the update. Particularly interested in the possibilities the fpga opens up.  Can't wait to see what the next instalment brings... Bring it on! 


---
**Arthur Wolf** *October 19, 2015 09:14*

**+Ross Bagley** Well, the low level stuff I'd assume is straightforward. But for example for USB, we need to implement a MTP interface, and that's not trivial. Same for Ethernet for which we need an IP stack, a web server, etc.


---
**Imants Treidis** *October 19, 2015 10:21*

So looks like I need to start saving up money then :)

Anyways, great news, thanks for the update!


---
**Stefano Pavanello** *October 22, 2015 12:30*

Good Work Arthur! I will follow the news on the board development. A question: I see that you will use a "true" SDIO instead of spi for faster SD transfer, but I always ask myself why not use the USB host port for reading directly the gcode files. In my opinioni USB flash disks / dongle are more common and fast than SD cards, especially for loading and exchanging files. I think that the sdcard choice for 3D printers was due to direct interface with AVR 8bit microcontrollor, but I hope that this is superseeded with ARM 32 bit MC. Am I wrong?


---
**Arthur Wolf** *October 22, 2015 12:38*

**+Stefano Pavanello** On the previous board, the USB port was dedicated to communication with the host. On the new board, we have a <b>second</b> USB port that can be used for what you describe ( when that is coded of course ).

We'll still need the on-board SD card for storing configuration etc ... but USB dongles will be usable on the new board.


---
**Stefano Pavanello** *October 22, 2015 12:41*

Thank you! Yes, is what I think as best configuration, the internal SD for storing config etc.. And have a dedicated USB port for example on the front panel to load gcode files from USB stick.


---
**Artem Grunichev** *October 26, 2015 07:59*

**+Arthur Wolf** at Pro version you don't need webserver/usb on smoothie, while you have Edison :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/aJaVEEm993z) &mdash; content and formatting may not be reliable*
