---
layout: post
title: "Want to win a PanelDue and a Smoothieboard ?"
date: November 09, 2015 13:47
category: "General discussion"
author: "Arthur Wolf"
---
Want to win <b>a PanelDue and a Smoothieboard</b> ?  Read up :)



( Please reshare with your circles in case this'd be interresting to someone you know ).



It's pretty simple : PanelDue is very nice, it's a touchscreen that controls your printer, but it doesn't know how to talk to Smoothieboard.



We need to add support in Smoothieboard for the M408 Gcode ( [http://reprap.org/wiki/G-code#Duet-dc42_extension](http://reprap.org/wiki/G-code#Duet-dc42_extension) ), which is only supported on the Duet at the moment.



I actually started writing this code ( I'm at the point where they can talk to each other, but don't say much ) : [https://github.com/Smoothieware/Smoothie2/blob/master/src/modules/tools/reporter/Reporter.cpp](https://github.com/Smoothieware/Smoothie2/blob/master/src/modules/tools/reporter/Reporter.cpp)



All you need to do, is just complete this work, so that it gives a full answer instead of only reporting temperature like it does now.



If you do this, you win a PanelDue, and a Smoothieboard. If you are already a known Smoothie contributor, but do not own a paneldue, I'll even send it in advance so you can work.



Cheers, and please reshare :)





**"Arthur Wolf"**

---
---
**Chris Brent** *November 12, 2015 20:27*

Is there anyway to emulate this if we don't have hardware? I've written some code (it's a little embarrassing as I haven't written C++ since college) but I love to know if it "works".


---
**Chris Brent** *November 12, 2015 20:28*

Oh and where do code discussions happen? Here? The smoothie forum?


---
**Arthur Wolf** *December 16, 2015 19:24*

**+Chris Brent** Can you send me your mailing adress at wolf.arthur@gmail.com ?


---
**Chris Brent** *December 16, 2015 19:38*

Done.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/DtVCEmzhREU) &mdash; content and formatting may not be reliable*
