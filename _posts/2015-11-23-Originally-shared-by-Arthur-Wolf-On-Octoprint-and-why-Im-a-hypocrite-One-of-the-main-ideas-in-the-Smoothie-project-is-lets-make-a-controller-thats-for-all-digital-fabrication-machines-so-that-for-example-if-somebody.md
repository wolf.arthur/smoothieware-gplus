---
layout: post
title: "Originally shared by Arthur Wolf On Octoprint and why I'm a hypocrite : One of the main ideas in the Smoothie project is : let's make a controller that's for all digital fabrication machines, so that for example if somebody"
date: November 23, 2015 14:52
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



<b>On Octoprint and why I'm a hypocrite</b> :



One of the main ideas in the Smoothie project is : let's make a controller that's for all digital fabrication machines, so that for example if somebody codes grid-based bed probing for 3D printers, CNC mill users can profit from it for PCB milling. 

That way work contributors do is useful to many more people than if we had projects for each machine type.

It's all about preferring integrating rather than forking, and compromise : Smoothie has a few years now, and is still ( mostly ) a single project, that has been able to integrate new features rather than requiring their implementers to start their own project, or fork.



Now I've recently had to solve a problem : <b>Smoothie's web interface sucks</b>. We have an ethernet port on the board, it can serve web pages, and all we've been able to put on there is this super minimal/basic/barebones/ugly page. It works, but it doesn't do much.

Now when trying to solve that problem, I went immediately to : « Let's make the best web interface of all times ... and let's start from scratch ». It was going to have modularity, and shiny buttons, and you'd be able to design directly in there, and it'd support lasers, and dropbox, and unicorns and chocolate ( 
{% include youtubePlayer.html id="NKwcuq5O-mc" %}
[https://www.youtube.com/watch?v=NKwcuq5O-mc](https://www.youtube.com/watch?v=NKwcuq5O-mc) ).



I worked on that for 2 months, and in the end all I had was a super great framework to build a web interface on, but it didn't do anything useful. People offered to help, but it was about as much work getting them up to speed with how things were supposed to be done, as it was to do it myself. A year later, still not much more has been added ( [https://github.com/arthurwolf/Mousse](https://github.com/arthurwolf/Mousse) )



*So, I'm a hypocrite. *

Why start something from scratch, when the Open-Source digital fabrication community already has a perfectly fine web interface : the awesome **+OctoPrint** ...



Not only did I fail at getting the project anywhere interesting, but if I had gotten it to work, I'd essentially have been dividing efforts where they don't need to be divided.

After all, not duplicating efforts is what Smoothie is all about ...



So, it took some time to realize that mistake, but I'm throwing Mousse into the garbage can, and I'll be working on adapting +Octoprint to be able to run on Smoothieboards.



There are some obvious problems with that : for example, Octoprint requires a python server to run. You can't run python on a Smoothieboard. 

So we'll essentially need to modify the web interface side of Octoprint to be able to talk directly to a Smoothieboard over http.



I'll start doing this in a fork, but hopefully once we've proven it can work, we can find a way to get this to integrate in a clean manner into the "normal" Octoprint. I'm not sure if that's something **+Gina Häußge** would be interested in merging back in, but we'll cross that bridge when we come to it.



Anybody want to help ? :)



Challenges : 

1. Get all of octoprint into a single html file. Smoothie doesn't like to get many files requested at once. Already did that for Mousse, so should be easy to port.

2. Get octoprint to talk to Smoothieboards. Will probably do this by adding some javascript code that'll replace some critical bits of how octoprint talks to it's server, and make it think it is, when it's actually a Smoothie answering.

3. Add some smoothie-specific features, like wizards for configuration, and figure out how to do these both as addition to the original unmodified Octoprint, and to the modified Smoothie-specific octoprint.



Yay, adventure :)













**"Arthur Wolf"**

---
---
**Arthur Wolf** *November 23, 2015 15:30*

**+Peter van der Walt** It's next on my list :) Smoothie essentially allows you to send gcodes as http requests, and get an answer, that's about it, it's very simple.


---
**Arthur Wolf** *November 23, 2015 15:45*

**+Peter van der Walt** Yes exactly !!


---
**Arthur Wolf** *November 23, 2015 15:48*

**+Peter van der Walt** Actually, the same way you can get laserweb to talk to a Smoothieboard, you can probably get it to talk to an Octoprint server too ^^


---
**ThantiK** *November 23, 2015 17:31*

Would it be more or less work to implement [https://github.com/micropython/micropython/wiki](https://github.com/micropython/micropython/wiki) on the processor you're using?  Heck...will Tornado even run on micropython... Hmm


---
**Arthur Wolf** *November 23, 2015 17:34*

**+ThantiK** Nah, if we tried to run octoprint with that, we'd run out of flash and ram in a heartbeat, it's just not adequate on the sort of microcontroller we have right now.


---
**Artem Grunichev** *November 23, 2015 18:19*

Why you want that when we'll have super-awesome Intel Edison on Pro's? For those without Pros native client isn't that bad solution. As I can remember, Octoprint is mostly Python software, and it's web interface is built on some framework, which makes porting isn't that easy


---
**Arthur Wolf** *November 23, 2015 18:21*

+Артем Груничев Not everybody will have a pro, and not everybody with a pro will pay for an edison to connect to it.

Edison will definitely be able to run Octoprint. We still need a solution for everybody else :)


---
**Mike Miller** *November 23, 2015 19:26*

Is it 'worth' doing when a RaspberryPi+Power+Case is less than $50? It's a nice dividing line as plenty of printers get by with an LCD and SD card reader, and the incremental cost for Octoprint isn't much. 


---
**Arthur Wolf** *November 23, 2015 19:28*

**+Mike Miller** Well, if we do this, people will get octoprint on smoothieboard out of the board, for $0 instead of $50 ... so sure it's worth doing.

Smoothie has an ethernet port, and people want to use it :)


---
**Mike Miller** *November 23, 2015 19:29*

Fair enough. 


---
**Artem Grunichev** *November 24, 2015 09:10*

**+Arthur Wolf** yeaaaah... but if so, users who will get Pro almost wouldn't have advantage from Edison port. Probably camera - but there are no USB for Edison on Pro. Probably wi-fi - bet there are no network connection between Edison and Pro's MCU. File storage? Same story.


---
**Arthur Wolf** *November 24, 2015 09:16*

**+Артем Груничев** The edison might allow things like on-board slicing, on-board CAM, image recognition, and other stuff.


---
**Artem Grunichev** *November 24, 2015 09:22*

wait... I've just realised that you are going to use OctoPrint without slicing feature...



Probably slicing would be better feature to port into Smoothie :)


---
**Stephane Buisson** *November 27, 2015 12:22*

nouvelle done avec new pi zero et je crois avoir lu edison's eof  is already in the calendar


---
**Artem Grunichev** *December 02, 2015 10:43*

Raspberry Pi Zero costs 5$


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/4SQNUPUHVSE) &mdash; content and formatting may not be reliable*
