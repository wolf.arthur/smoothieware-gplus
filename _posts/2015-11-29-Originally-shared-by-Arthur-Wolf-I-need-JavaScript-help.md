---
layout: post
title: "Originally shared by Arthur Wolf I need JavaScript help !!!"
date: November 29, 2015 21:54
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



<b>I need JavaScript help !!!</b>



As mentionned before, I'm trying to get the **+OctoPrint** web client to work without needing to talk to the Octoprint server. This would allow for octoprint with just a smoothieboard, no raspi needed.



The way I'm doing that, is to take Octoprint, put everything into a single huge HTML file ( no includes ), and then at the end of that, adding a bit of custom javascript to make it do what I want ( by overwritting a few things ).

While that is a bit dirty, it would allow for a very fast proof of concept.



I have it <b>nearly</b> working ( yay ! \o/ ), but I'm hitting a problem I can't figure out how to fix.

There is an error with the knockout templating system, which refuses to bind things in my copy, but doesn't refuse in the original octoprint, even though I can't find any difference in the parameters knockout is being passed in each.



If anybody could load the page, open firebug, and figure out why it is refusing to bind, that would help <b>A LOT</b>.



You can find the page here : [https://github.com/arthurwolf/Octofab/blob/master/test.html](https://github.com/arthurwolf/Octofab/blob/master/test.html) ( it's self-contained, which is kind of the point, you just have to download and open the file ).



Thanks a lot in advance to anybody who looks at this. I'm completely lost, and I'd be super thankful for any kind of clue.



Thanks community !! :)



﻿





**"Arthur Wolf"**

---
---
**Eric Walch** *November 30, 2015 14:19*

I'm looking at it right now. It appears to be working, is there a particular control that you're seeing this issue?


---
**Arthur Wolf** *November 30, 2015 14:22*

**+Eric Walch** Somebody found the issue and it's been fixed since. I updated the original post but it seems I missed this one. Thanks a lot for looking !


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/AbZZ4jLnQta) &mdash; content and formatting may not be reliable*
