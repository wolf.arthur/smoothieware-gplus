---
layout: post
title: "Is this my only option for getting some Smoothie hardware right now?"
date: December 01, 2015 18:58
category: "General discussion"
author: "Chris Brent"
---
Is this my only option for getting some Smoothie hardware right now?

[http://shop.uberclock.com/collections/smoothie/products/smoothieboard](http://shop.uberclock.com/collections/smoothie/products/smoothieboard)

Rev 1a boards at uberclock or roboseed are all I can find. 4x is probably all I need. Panucatt are out of stock too.





**"Chris Brent"**

---
---
**Wolfmanjm** *December 02, 2015 07:44*

That is not open source and they do not contribute to smoothieware development so basically they are parasites.




---
**Chris Brent** *December 03, 2015 18:26*

Also found [http://hbot3d.com/sunbeam-2-0-2/?lang=en](http://hbot3d.com/sunbeam-2-0-2/?lang=en) but the shop link just takes me to their printer :(

EDIT:

Found it [http://hbot3d.com/sklep-2/elektronika-en/sunbeam/?lang=en](http://hbot3d.com/sklep-2/elektronika-en/sunbeam/?lang=en)


---
*Imported from [Google+](https://plus.google.com/+ChrisBrent/posts/TxNb1rwtV9Y) &mdash; content and formatting may not be reliable*
