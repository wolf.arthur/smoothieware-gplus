---
layout: post
title: "Making progress on the OctoPrint thing. Right now I have it working fully without the octoprint server, just hosted on the SD card of a Smoothieboard, served by the Smoothie firmware's web server, and it's able to list the"
date: December 01, 2015 17:41
category: "General discussion"
author: "Arthur Wolf"
---
Making progress on the **+OctoPrint** thing. 



Right now I have it working fully without the octoprint server, just hosted on the SD card of a Smoothieboard, served by the Smoothie firmware's web server, and it's able to list the files, send commands, and receive answers.



I think next thing I'm going to tackle is reading temperature so we can get the graph going :)



Once everything ( that can be ) is implemented, we'll ship this with all Smoothieboard on their SD card, so people can have a better web interface, without needing to wire a Raspberry pi. 



If you want to play with it, clone [https://github.com/arthurwolf/Octofab](https://github.com/arthurwolf/Octofab) then copy index.html and static/ to your Smoothieboard's SD card, and access *[http://your_smoothie_ip/sd/index.html*](http://your_smoothie_ip/sd/index.html*)



Any help would be super extra welcome ! Ping me first so we make sure we don't work on the same thing at the same time.



Cheers :)

![missing image](https://lh3.googleusercontent.com/-qk9cLuBweCc/Vl3bwn-j9zI/AAAAAAAALrY/il-_wtgt1cY/s0/Screenshot%252Bfrom%252B2015-12-01%252B18%25253A33%25253A28.png)



**"Arthur Wolf"**

---
---
**Jeremie Francois** *December 01, 2015 18:18*

Great work :)


---
**Arthur Wolf** *December 01, 2015 18:23*

**+Peter van der Walt** Maybe you are looking for something like [http://smoothieware.org/smoothie-on-a-breadboard](http://smoothieware.org/smoothie-on-a-breadboard) ?

If you are looking at even more barebones, take a loot at the LPC1769's datasheet, and [http://smoothieware.org/flashing-the-bootloader](http://smoothieware.org/flashing-the-bootloader)


---
**Arthur Wolf** *December 01, 2015 18:27*

**+Peter van der Walt** eh I can understand that :)


---
**Arthur Wolf** *December 01, 2015 18:35*

**+Peter van der Walt** If you did that, shouldn't be too hard doing the same for Smoothie :)


---
**Arthur Wolf** *December 01, 2015 18:47*

**+Peter van der Walt** Smoothie just got TMC driver support : [https://github.com/Smoothieware/Smoothieware/pull/773](https://github.com/Smoothieware/Smoothieware/pull/773) :) !

I can't wait for you to get laserweb to work with smoothie, ping me when you start, and if you ever need any help.


---
**Arthur Wolf** *December 01, 2015 18:50*

**+Peter van der Walt** **+Wolfmanjm** did all the work ( as usual )


---
**Wolfmanjm** *December 01, 2015 19:45*

I should mention that adding the TMC26X and DRV8711 was sponsored and supported by [panucatt.com](http://panucatt.com)


---
**Jason Richardson** *December 01, 2015 20:33*

great work as always! i saw your previous post about javascript issues but too late to help. as you know i did much of the javascript work in the existing basic web ui so if there are specific tasks you need help with just let me know.


---
**Arthur Wolf** *December 01, 2015 20:40*

**+Jason Richardson** Wow ! Thanks for offering ( and nice work :) ). I'll definitely post stuff on G+ if I need help with anything.

The plan right now is to make this tool that leaves all of octoprint un-touched, and adds a bunch of compatibility code to make it work with Smoothie.

But once that works, I have plenty of ideas of additions to octoprint itself, which I could need help with, and I may try to get some of those integrated into mainstream octoprint ...﻿


---
**Mert G** *December 07, 2015 23:57*

 I hope you can keep plugin features of Octoprint when it is going to work on Smoothieboard. I would be so good. I thought about it if it is possible to do while I was wiring Raspi to smoothieboard. It seems that smoothieboard is going to rock! Really looking forward to see final version!


---
**Arthur Wolf** *December 08, 2015 10:33*

**+Mert G** The plugins are python, we can't get those to work ... if octoprint had "client-side" plugins we could get those to work.


---
**Nate C** *December 11, 2015 01:21*

Arthur have you looked at the Spark platform from Autodesk.  Looks very promising and could do the same as Octoprint plus more.  Just thought i would throw it out there.  I currently have access to the Beta and talked with them last week at the Autodesk University conference. 


---
**Leon Grossman** *December 11, 2015 23:48*

You have no idea how excited I am that you're doing this!


---
**Matt Omond** *January 17, 2016 03:33*

Hey Arthur nice work. I can't edit the printer profile I would like to add another extruder. Also the heater don't appear to turn on with the buttons. Thanks Matt


---
**Matt Omond** *January 17, 2016 03:56*

I get this error when loading an .gcode file 

Upload failed

Could not upload the file. Make sure that it is a GCODE file and has the extension ".gcode" or ".gco" or that it is an STL file with the extension ".stl".




---
**Arthur Wolf** *January 17, 2016 09:44*

**+Matt Omond** Ok, it doesn't know how to tell octoprint the upload succeeded, I'll fix that. But does the file actually get uploaded or not ( refresh the page ) ?


---
**Matt Omond** *January 17, 2016 09:47*

yeah looks like it. I couldn't get it to load and print though




---
**Arthur Wolf** *January 17, 2016 09:47*

Ok that one's a bug :)


---
**Ben Murray** *February 27, 2016 01:59*

I am in the final stages of setting up my printer. Octoprint is the interface I want. Any new progress on this? Will we be able to have a camera and plugins? Thanks


---
**Arthur Wolf** *February 27, 2016 09:44*

**+Ben Murray** It will never have camera or plugins ( it's just not possible ), just the bare basic functions.


---
**Ben Murray** *February 27, 2016 13:15*

OK is there a way to communicate with octoprint raspberry pick thru serial over tcpip? I am thinking that will give me stable info transfer vs usb. Am I correct in my thinking?  Thanks for the quick reply. Ben


---
**Arthur Wolf** *February 27, 2016 13:33*

[http://smoothieware.org/octoprint](http://smoothieware.org/octoprint) :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/jiatQbJoHe2) &mdash; content and formatting may not be reliable*
