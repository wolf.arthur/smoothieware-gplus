---
layout: post
title: "Set up a new testing rig to speed up iterations on the CubeSpawn Modules Yes!"
date: December 15, 2015 06:41
category: "General discussion"
author: "James Jones (CubeSpawn)"
---
Set up a new testing rig to speed up iterations on the CubeSpawn Modules



[https://picasaweb.google.com/103828779781480193226/DougsShopPhase2?noredirect=1#6228237464438321842](https://picasaweb.google.com/103828779781480193226/DougsShopPhase2?noredirect=1#6228237464438321842)



Yes! that's a smoothie running it!

![missing image](https://lh3.googleusercontent.com/-yyDu5v8s7zU/Vm-19Si7_sI/AAAAAAAAFhY/Gc9VoS-h22g/s0/TestingRig.jpg)



**"James Jones (CubeSpawn)"**

---
---
**Sébastien Plante** *December 15, 2015 12:25*

Oh wow, that cable management! 


---
**Jarek Szczepański** *December 15, 2015 14:14*

#cableporn :)


---
**James Jones (CubeSpawn)** *December 15, 2015 14:48*

Hi **+Alex Skoruppa**  and others...



CubeSpawn is open source and is intended to evolve to be fully autonomous and make parts, assemblies, and machines from digital templates (similar in concept to [https://en.wikipedia.org/wiki/STEP-NC](https://en.wikipedia.org/wiki/STEP-NC))



I intend to bootstrap later designs off of the earlier ones as a recursive process -so the focus will be on basic industry machines first - although anyone else can make tissue printer modules or burger flipper modules or footware shaping gear with the system, if they so desire...



The real core is built around ROS, Machinekit, MTConnect and some derivative of STEPNC - Addressing robotics, machine vision, CNC, materials handling, and digital templates - github will provide early version control  - for compatibility between the rapidly evolving designs 



Furthermore - these are being built from repurposed industrial junk - (which is available in any city worthy of the name)  - all the components in the pictures are from scrap at this point so costs are negligible. (and the design is a little clunky, as a result)



Cash outlay for this project in its entirety is under 50k after 6 years of slow thought and evolution - and this price includes feeding the designer and keeping a roof and some travel... 



Material cost for the machines at retail material prices is expected to be under $5000 for heavy, complex designs and under $2000 for something like this printer - and its not in competition with ANY manually operated design so the $300 lightweight machines are not comparable - I have worked as a machine operator in the past and wouldn't wish that fate off on anyone... ;-)



here are a few links to flesh out the project for anyone interested - I am in the Austin/San Antonio TX area if anyone would like a tour... ;-) I am seeking cofounders...



[http://cubespawn.com](http://cubespawn.com) (needs a lot of work)



Moar Pix

[https://picasaweb.google.com/103828779781480193226?noredirect=1](https://picasaweb.google.com/103828779781480193226?noredirect=1)

(DougsShop and DougsShopPhase2 are the latest



3D Models

[https://3dwarehouse.sketchup.com/user.html?useCustomWarehouseUrl=CubeSpawn](https://3dwarehouse.sketchup.com/user.html?useCustomWarehouseUrl=CubeSpawn)



Github

[https://github.com/CubeSpawn](https://github.com/CubeSpawn) (this is still a little weak on actual ROS/Machinekit code, but has the mechanical development fairly up to date)


---
**Arthur Wolf** *December 16, 2015 16:50*

Very nice :)


---
*Imported from [Google+](https://plus.google.com/+JamesJonesCubeSpawn/posts/Zd8kwbTQLy1) &mdash; content and formatting may not be reliable*
