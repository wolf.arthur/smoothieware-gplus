---
layout: post
title: "This is so very very nice. Should work for CNC milling too if I understand things correctly"
date: December 16, 2015 17:55
category: "General discussion"
author: "Arthur Wolf"
---
This is so very very nice.



Should work for CNC milling too if I understand things correctly.



<b>Originally shared by Peter van der Walt</b>




{% include youtubePlayer.html id="s3iyZzqiFXQ" %}
[https://www.youtube.com/watch?v=s3iyZzqiFXQ&feature=youtu.be](https://www.youtube.com/watch?v=s3iyZzqiFXQ&feature=youtu.be)





**"Arthur Wolf"**

---
---
**Bouni** *December 17, 2015 08:41*

So cool, hope I have some time to build my cnc mill in the near future :-)

 **+Peter van der Walt**: You can add line numbers to github links, so it automatically jumps to that lines and even highlights them: [https://github.com/openhardwarecoza/LaserWeb/blob/master/i/main.js#L274-L653](https://github.com/openhardwarecoza/LaserWeb/blob/master/i/main.js#L274-L653)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/iy8wRxf8qH7) &mdash; content and formatting may not be reliable*
