---
layout: post
title: "Hi. Anyone with experience using azsmz board and getting dual Y?"
date: December 28, 2015 03:45
category: "General discussion"
author: "Ariel Yahni (UniKpty)"
---
Hi.  Anyone with experience using azsmz board and getting dual Y? 





**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *December 29, 2015 23:19*

Got it working via parallel connection. I wanted to use 2 drivers in sync but could not do it via firmware


---
**Henri Harbor** *January 01, 2016 22:58*

The X-Carve is just such a machine... dual Y Axis ~ I have my smoothie board running two drivers on the Y ~ one slaved (it's a simple jumper setup and a change or two in the config).  I will be glad to answer your questions on it.  ;{)




---
**Ariel Yahni (UniKpty)** *January 01, 2016 23:05*

**+Henri Harbor**​ thanks.  I assemble an xcarve frame also but my board is an azsmz and haven't figured out how to clone the extruder to other axis. There are no jumpers that I know of as in the official board


---
**Henri Harbor** *January 02, 2016 12:19*

Yea Hmmm I realize that... I assumed the board would have the jumper capability readily present ~ I have been trying to look for such on the schematics... that is your biggest hurdle ~ I went with the SmoothieBoard 4X for that very reason ~ as it has 4 stepper drivers and is easy to set up the jumper ~ jumped the Y(m2) to the E(m4) ~   easy to pin out for the jumper ~ if you solve that problem then it's just a matter of the config file  and if I recall you need to wire one of the Y motors reverse (I can look that up for you if you get to that)  ~ on the config just be sure to set the Delta Current line ~ to match the Beta ~   



delta_current  1.6 (use a correct amp setting for your motors of course)  # Second Y Axis Stepper Motor



beta_current   1.6    # Y stepper motor current



Then wire the Right Y Motor to have it move in the reverse of the Left Y Motor (double check that ~ I am pretty sure it's correct ~ been a while since I did that part). 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/YPJordajvbW) &mdash; content and formatting may not be reliable*
