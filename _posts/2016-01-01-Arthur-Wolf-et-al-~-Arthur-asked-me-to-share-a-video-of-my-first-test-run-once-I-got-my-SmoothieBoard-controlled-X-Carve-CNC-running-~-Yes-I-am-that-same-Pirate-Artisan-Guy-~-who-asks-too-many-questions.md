---
layout: post
title: "Arthur Wolf et al ~ Arthur asked me to share a video of my first test run once I got my SmoothieBoard controlled X-Carve CNC running ~ (Yes, I am that same Pirate Artisan Guy ~ who asks too many questions!"
date: January 01, 2016 22:31
category: "General discussion"
author: "Henri Harbor"
---
**+Arthur Wolf** et al ~ Arthur asked me to share a video of my first test run once I got my SmoothieBoard controlled X-Carve CNC running ~ (Yes, I am that same Pirate Artisan Guy ~  who asks too many questions! Hey it's because I knew so little about this tech stuff! ) ~ Finally with great patience, much learning of things geek and much appreciated help from **+Arthur Wolf**, +Wolfmanjm and others ~ Finally getting back to this project after some respite to do pirate stuff for a good while ~ but finally ~ Z-Probe and Bed Level Compensation were pretty much the final touches needed to be finished ~ and here is it is ~ my first test ~ 

[https://drive.google.com/file/d/0B0RDKYEUhVKCNFNCdUF4S2lrVDA/view?usp=sharing](https://drive.google.com/file/d/0B0RDKYEUhVKCNFNCdUF4S2lrVDA/view?usp=sharing)

![missing image](https://lh3.googleusercontent.com/-SKWD8Kbqx-8/Vob-QiHjkuI/AAAAAAAAASo/jOZPX2qfTb8/s0/Screen%252BShot%252B2016-01-01%252Bat%252B4.22.35%252BPM.jpg)



**"Henri Harbor"**

---
---
**Henri Harbor** *January 01, 2016 22:43*

So X-Carve ~ by Inventables.com ~ perhaps a bit trickier than some machines to connect to the smoothie board ~ mostly because Inventables is a bit not so forthcoming with specs required for using a board other than the GShield and their new X-Controller ~ but I (well... we) figured it out ~ so if there are any X-Carve folks out there trying to work up a SmoothieBoard X-Carve set up ~ I will be happy to help you out if I can.  


---
**Ariel Yahni (UniKpty)** *January 01, 2016 23:08*

Very nice. What was your work flow? 


---
**Henri Harbor** *January 02, 2016 12:39*

**+Ariel Yahni** I worked up the text in Adobe Illustrator to SVG ~ I translated it then to GCode ~ did that a while back and don't remember exactly what program (some web based offering that works well with 2D ~ I have been trying out a number of slicer/gcode offerings) ~ anyway ~ got the Gcode ~ and sent it using Pronterface on a Linux laptop dedicated to my CNC station.  Next I will move on to some actual 3D and cutting! ;{)  Once I get all that blue masking tape off my spindle!!!  ;{P


---
*Imported from [Google+](https://plus.google.com/102189438122169943672/posts/6b22WqHTzWs) &mdash; content and formatting may not be reliable*
