---
layout: post
title: "I recently bought a Smoothieboard 4XC to test and optimize for it"
date: January 11, 2016 20:22
category: "General discussion"
author: "Mathias Dietz"
---
I recently bought a Smoothieboard 4XC to test and optimize #GCodePrintr for it. Currently I'm testing the plain board with a thermistor connected but without stepper motors, heater, etc.. 

As expected the connection and basic function works fine.

<b>But I have a few questions about some different behaviour compared to Arduino/Marlin.</b> 

1) does smoothie support reset using the DTR signal ? It seems to detect DTR and print out "smoothie" but does not really reset the board. 

2) the M115 command to show the firmware version seems to be not supported. Why not just redirect M115 gcode to the "version" command ? 

3) As mentioned above, I test with a thermistor but without a heater connected. That leads to M109 waiting infinitely (when using play). I could not find a way to interrupt a waiting M109 command over serial. Even the reset command does not work anymore. Any suggestions ? 

4) The SD card gets automatically mounted when you connect smoothie to Android. There should be a way to disable MSD support in the config (without having a special firmware build) 

<b>Thanks</b> 

![missing image](https://lh3.googleusercontent.com/-16aYlqOMUoo/VpQO8OehoII/AAAAAAAAR3E/9ZfsiwSbIM4/s0/IMAG3163.jpg)



**"Mathias Dietz"**

---
---
**Arthur Wolf** *January 11, 2016 21:02*

Hey.



1. To reset the board you need to send the "reset" command. no DTR

2. I didn't know M115 was really being used by anything, we might implement it

3. At the moment you can't interrupt a M109

4. I don't believe that's possible ( or easy ), or we'd have it.



Cheers !


---
**Mathias Dietz** *January 11, 2016 22:38*

**+Arthur Wolf**​​ thanks for the quick answer.

Not being able to interrupt M109 is an issue. This makes it impossible to have an emergency off button in the host software. What is the process to request this feature?  

4. a special firmware already exists to disable msd,  I was wondering why it is not in the stock firmware version﻿


---
**Mathias Dietz** *January 11, 2016 22:43*

**+Ross Bagley**​ not sure how this relates to my questions. I know the meaning of M109/M105.. 


---
**Arthur Wolf** *January 11, 2016 22:47*

**+Mathias Dietz** To request a feature you'll want to open an issue on Smoothie's github


---
**Mike Creuzer** *January 12, 2016 06:19*

I configured my new smoothie board on a new printer completely from Android.  Sure is handy! 


---
**Mathias Dietz** *January 12, 2016 07:48*

**+Ross Bagley**  exactly, I need an interruptible M109. I personally could use M104/105 but I'm developing a host software and have no control what people use it for. Other firmwares (e.g. Marlin) allow to interrup M109 therefore this should be fixed in smoothie as well.


---
**Wolfmanjm** *January 12, 2016 08:13*

**+Mathias Dietz** There is no way to interrupt an M109 from the command line (and it would be very hard to add for many reasons), I recommend do not use M109.  There is a kill button on smoothie which will interrupt it, that is the only way to do it, (other than reset).




---
**Wolfmanjm** *January 12, 2016 08:14*

DTR is ignored as it is not a real serial port it is a virtual USB serial (CDC).


---
**Wolfmanjm** *January 12, 2016 08:18*

There is no way to stop automount from the smoothie end, it must be done on the host end that is the way MSD works unfortunately. MSD is the root cause of a lot of issues, and hopefully it will be killed off one day and replaced with some other protocol. There are some uninteresting technical reasons why disabling MSD permanently requires a separate binary. (has to do with BSOD on windows if it is compiled in but not disbaled in the config).


---
**Mathias Dietz** *January 12, 2016 13:44*

**+Wolfmanjm** thanks for your answers.  Arduino/Marlin handles DTR even so it is a virtual USB serial as well. Therefore it should be possible with smoothie too.

As written to Ross already, I can't control if people are using M109. I just develop the host app and people will complain about the app if it hangs in a un-resettable state. 

Thanks for giving some background on the MSD issue.


---
**Mathias Dietz** *January 12, 2016 16:25*

**+Ross Bagley**  I'm glad that you agree that it would be nice to have smoothie handle the sw driven reset :-)

This feature is even more important if you control your printer remotely (e.g. using Octoprint or GCodePrintr web interface). In this scenario it can be dangerous to not being able to reset while heating.


---
**Wolfmanjm** *January 12, 2016 22:26*

**+Mathias Dietz** the USB serial is totally different to the one on an arduino. most arduino use a USB chip (FTDI) and the DTR signal is a pin. The smoothie does a full CDC emulation and there is no DTR pin. You can detect if the USB protocol toggles the DTR, but it is all in S/W. 

Also we allow multiple serial connections to smoothie, and as most serial consoles toggle DTR when they connect this would cause smoothie to reset when we connect to the second Serial port (usually ACM1) and that would not be desired behavior.

If you were to connect to the UART serial port you can interrupt with control X which is caught in the interrupt so will interrupt whatever smoothie is doing. Unfortunately there is no easy way to do that with the USB serial library.

As I said there is a kill switch on smoothie and that is used to interrupt these types of commands which by design must ignore any further G codes until they complete (same as G4).


---
**Wolfmanjm** *January 12, 2016 22:28*

there is a s/w reset, the reset command will reset smoothie remotely. However if you have deliberately told it to ignore any further gcodes until an operation has completed, then that would also include the reset command.

Having M109 wait and not interruptable from the host has not been an issue for any other hosts, so I fail to see why it is any more of an issue for gcodeprintr.


---
**Mathias Dietz** *January 12, 2016 22:42*

**+Wolfmanjm**​ not been able to interrupt m109 it's not a hard issue for gcodeprintr , but it's ugly.  Same with DTR.  

From a host sw perspective the 3D firmwares (Marlin, Repetier, smoothie, ..) should behave be the same to avoid special handling. But it looks like I need to add a bunch of "if smoothie then do something different" statements. Nevertheless thanks for your help !


---
**Wolfmanjm** *January 15, 2016 09:00*

**+Mathias Dietz** Ok I added a GRBL command, control X sent to the USB or uart stream will abort any ongoing process. So M109 will be aborted, so will any print if in process. This is the same as hitting the kill button or sending M112, but will be executed on receipt. Note when smoothie is in the halt state (ALARM state) it will return !! to any gcode sent (this is documented in the reprap wiki and is a standard response for reprap when in such a state). M999 will exit the Halt state (as will a reset).

I also added the ? command which responds the same as GRBL with realtime machine position and workspace position, also with IDLE,RUN,ALARM so you can see if it is busy or not or in the ALARM state (which we call ON_HALT).

Note that this will abort M109 and such as the host will not get ok and should not have sent any more gcode. However due to the flow control in the USB serial protocol if gcode is being streamed and the USB buffers are full then the ^X will not get sent until there is room. There is nothing we can do about that as it is the way USB CDC works.


---
**Mathias Dietz** *January 15, 2016 17:23*

**+Wolfmanjm** this sounds great. Thank you ! I will try the next Nightly build and try it out.


---
**Mathias Dietz** *February 02, 2016 21:03*

**+Wolfmanjm** I tried the CTRL-X command with the nightly build and it works as expected. Thanks a lot. I will publish a new #GCodePrintr version with the option to choose between smoothie reset and CTRL-X soon.


---
**Wolfmanjm** *February 02, 2016 21:12*

**+Mathias Dietz** great... BTW ^X is not the same as reset, you really never need to reset smoothie, M112 or ^X is the same as hitting the kill button, and puts it into halt state, M999 releases it from the reset state (or $X when in grbl mode).




---
**Mathias Dietz** *February 02, 2016 21:20*

**+Wolfmanjm** yes, I noticed that reset is different than ^X. But I was about to expose both options. You are probably right that "reset" is never needed, so I don't need to expose it in GCodePrintr


---
**Wolfmanjm** *February 02, 2016 22:30*

FYI if you do issue the reset command it has a 5 second delay before it resets, and you must close the connection after you issue it, otherwise it comes back with a different port allocation.


---
*Imported from [Google+](https://plus.google.com/+MathiasDietz/posts/SwCBigRgssT) &mdash; content and formatting may not be reliable*
