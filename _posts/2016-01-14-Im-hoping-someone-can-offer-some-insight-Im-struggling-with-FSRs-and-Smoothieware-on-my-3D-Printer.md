---
layout: post
title: "I'm hoping someone can offer some insight - I'm struggling with FSRs and Smoothieware on my 3D Printer"
date: January 14, 2016 02:29
category: "General discussion"
author: "Chris Wilson"
---
I'm hoping someone can offer some insight - I'm struggling with FSRs and Smoothieware on my 3D Printer.  I can't for the life of me get the correct settings.  Either the bed probing (G32) doesn't react to any force applied to them, or I get the error "Zprobe triggered before move, aborting command".  I've changed pin settings, and the arrangement of the "SIG, +, -" wires, I've also changed from NC to NO using the jumper as described on the github repository.



Can anyone offer some insight on this?  I'm not sure what's going wrong.



Thank you everyone in advance,﻿





**"Chris Wilson"**

---
---
**Michael Hackney** *January 14, 2016 02:38*

Chris, I just shared 2 photos with you on how to connect the JohnSL to smoothie. I'll dig up my config file and post it now.


---
**Michael Hackney** *January 14, 2016 02:46*

# optional Z probe

zprobe.enable true                           true             # set to true to enable a zprobe

zprobe.probe_pin                             1.28!^           # pin probe is attached to if NC remove the !

zprobe.slow_feedrate                         5                # mm/sec probe feed rate

#zprobe.debounce_count                       100              # set if noisy

zprobe.fast_feedrate                         50               # move feedrate mm/sec

zprobe.probe_height                          5                # how much above bed to start probe

#gamma_min_endstop                           nc               # normally 1.28. Change to nc to prevent conflict,



# associated with zprobe the leveling strategy to use

leveling-strategy.delta-calibration.enable   true             # basic delta calibration

leveling-strategy.delta-calibration.radius   60               # the probe radius


---
**Michael Hackney** *January 14, 2016 14:16*

**+Michael Johnson** I published all the info on Chris' other post here: [https://plus.google.com/112111122069927621889/posts/BXjjumCfeFe](https://plus.google.com/112111122069927621889/posts/BXjjumCfeFe)



I have FSRs on all my printers and they have been working flawlessly. As I mentioned on the other thread, most of the issues I've seen (I've helped dozens of folks get theirs running) have been over constraining the triggering system or not eliminating sources of friction - especially for 3D printed mounting systems.


---
**Robert Burns** *January 14, 2016 19:42*

the FSR's need to be setup so all the weight of the build plate balances evenly on the center of each FSR. So solid backing behind each FSR (I used a penny) and you need a pad cut to the size of the sensitive part of the FSR (not the size of the outer diameter).



After you have it set, use the LED indicator to verify it takes the same amount of pressure to active over each of the 3 FSR. (and between them)




{% include youtubePlayer.html id="A7SB6rwpL1Q" %}
[https://youtu.be/A7SB6rwpL1Q](https://youtu.be/A7SB6rwpL1Q)


---
**Andrew Wade** *January 16, 2016 19:59*

Robert is right you to design the plate that holds the fsr correctly, check out [http://www.andornot.co.uk/fsr-leveling/](http://www.andornot.co.uk/fsr-leveling/) and look at the design of my delta printer. I stick the fsr to a printed part to the size of the active part of the fsr and the bed has a piece of cork covering all the the fsr so it activates the FSR. 


---
*Imported from [Google+](https://plus.google.com/112111122069927621889/posts/gu3TsmXDR3L) &mdash; content and formatting may not be reliable*
