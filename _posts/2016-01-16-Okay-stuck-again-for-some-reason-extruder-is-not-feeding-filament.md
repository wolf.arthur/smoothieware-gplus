---
layout: post
title: "Okay stuck again for some reason extruder is not feeding filament"
date: January 16, 2016 01:10
category: "General discussion"
author: "James Sinnott"
---
Okay stuck again for some reason extruder is not feeding filament





**"James Sinnott"**

---
---
**Arthur Wolf** *January 16, 2016 18:19*

What's going on exactly. Is they extruder's motor not turning ? 


---
**James Sinnott** *January 18, 2016 05:02*

Okay here is the best explanation.  The extruder motor on the printer is not turning so I hooked up another stepper motor I had and it din't respond.  I used the code to disable waiting for hot end to heat and allowed the hotend to come to temp.  Out of frustration I re hooked up my RAMPS board and it turns the extruder.  Sooo help me **+Arthur Wolf**


---
**Arthur Wolf** *January 18, 2016 09:54*

Hey.



So your extruder motor is not turning, even if you connect another stepper motor ? Did it use to work before ?



Cheers.


---
**James Sinnott** *January 18, 2016 17:16*

I don't think it ever did I just wasn't sure if it was a firmware setting.  Can I use extruder 2 by itself and how do I configure firmware.  Just to check if it's the driver?




---
**Arthur Wolf** *January 18, 2016 17:19*

You just need to change the step/direction/enable pins for your extruder module in config, set the current for the epsilon driver, and plug the motor into the 5th motor driver.

[http://smoothieware.org/3d-printer-guide](http://smoothieware.org/3d-printer-guide) should have most of the info.


---
*Imported from [Google+](https://plus.google.com/109661062702020544665/posts/NRbie9qh2Lq) &mdash; content and formatting may not be reliable*
