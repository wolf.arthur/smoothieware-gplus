---
layout: post
title: "I have designed a box for my new smoothieboard 4XC (the other boxes on Thinigverse where either too small or too large)"
date: January 18, 2016 22:44
category: "General discussion"
author: "Mathias Dietz"
---
I have designed a box for my new smoothieboard 4XC (the other boxes on Thinigverse where either too small or too large) 

[http://www.thingiverse.com/thing:1277247](http://www.thingiverse.com/thing:1277247)





**"Mathias Dietz"**

---
---
**Arthur Wolf** *January 18, 2016 22:45*

Neat :)


---
**Hakan Evirgen** *January 19, 2016 14:40*

Why no cooling fan?


---
*Imported from [Google+](https://plus.google.com/+MathiasDietz/posts/goEhaYBdtU1) &mdash; content and formatting may not be reliable*
