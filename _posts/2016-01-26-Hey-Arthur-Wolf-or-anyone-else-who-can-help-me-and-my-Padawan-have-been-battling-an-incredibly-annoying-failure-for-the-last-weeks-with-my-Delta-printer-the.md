---
layout: post
title: "Hey Arthur Wolf (or anyone else who can help), me and my Padawan have been battling an incredibly annoying failure for the last weeks with my Delta printer, the ."
date: January 26, 2016 18:07
category: "General discussion"
author: "Thomas Sanladerer"
---
Hey **+Arthur Wolf** (or anyone else who can help), me and my Padawan have been battling an incredibly annoying failure for the last weeks with my Delta printer, the  #CerbrisReborn  . Essentially, the printer will dead stop reproducibly in the exact same spot when certain models/gcode files are printed. We've seen a correlation with the heated bed being enabled and the printer hanging, with any M109 and M190 commands removed the print will finish just fine. After a "hang", the printer is still controllable and shows the on, blinking, blinking, on LED pattern.

The heated bed is a 650W 230V heater controlled through an SSR connected to the Smoothieboard's P1_22 output.

We've already switched the 12V power supply, that's not it. We're also sure it's not the USB connection as the error occurs in the exact same way when the SD card and Ethernet is used.



Here's the gcode used: [https://drive.google.com/file/d/0B5lPUVobyQ-3OVNyZUhLUGRDSEE/view?usp=sharing](https://drive.google.com/file/d/0B5lPUVobyQ-3OVNyZUhLUGRDSEE/view?usp=sharing) , the printer stops at line 34495, which doesn't look special in any way to me.



EDIT: And the printer configs:

[https://drive.google.com/file/d/0B9BrHANoGczNZWVFam9zZEJHb1U/view?usp=sharing](https://drive.google.com/file/d/0B9BrHANoGczNZWVFam9zZEJHb1U/view?usp=sharing)

[https://drive.google.com/file/d/0B9BrHANoGczNVlNOZnFPcnNSYzQ/view](https://drive.google.com/file/d/0B9BrHANoGczNVlNOZnFPcnNSYzQ/view)



Any help would be appreciated.





**"Thomas Sanladerer"**

---
---
**Arthur Wolf** *January 26, 2016 18:11*

Ok so : sliced by Cura so it shouldn't be the S3D bug.



When you say <b>exact same spot</b>, it's really exact-exact ? Guess so if you have a line number.

And when you say controllable, you mean including sending gcodes to it ? If so, what does the "progress" command answer once the bug occurs ?


---
**Thomas Sanladerer** *January 26, 2016 18:24*

**+Arthur Wolf** re-running the print with Pronterface now to check the output from "progress". The printer responds to everything, just like it would when finishing a print. Heaters and such remain active and in regulation.

The printer ends up in the same position each time, which is where we've concluded it stopping on the same line number from.


---
**Arthur Wolf** *January 26, 2016 18:25*

**+Thomas Sanladerer** And is this <b>always</b> printing from the SD card, or has it also occured while streaming gcode via serial with pronterface ?


---
**Thomas Sanladerer** *January 26, 2016 18:37*

**+Arthur Wolf** the printer has also stopped in a similar way when fed from USB, we've switched to SD/Ethernet printign since to try and mitigate issue. We're verifying right now if it actually stops in the exact same spot with USB.


---
**Arthur Wolf** *January 26, 2016 18:39*

**+Thomas Sanladerer** Ok so I <b>regularly</b> get people that have the <b>exact</b> same problem, they are printing from SD, file stops always at the same line, and it turns out what's going on is the file on the SD card is corrupted, and is actually shorter than they expect.



It can't be that here though if it doesn't happen when streaming over USB ...


---
**Wolfmanjm** *January 26, 2016 18:56*

so M109 and M190 are stop and heat, wait until temp is reached. It is supposed to stop there... maybe it is never reaching the temperature you are requesting. The fact it works fine if you remove them I would suggest you stop using M109 and M190, and just manually heat up using the non stop and and wait heating commands. That is what I do.


---
**Thomas Sanladerer** *January 26, 2016 18:58*

**+Arthur Wolf** you're right, the file on the card was corrupted shortly after the point of failure. What's surprising, though, is that the upload seems to corrupt the gcode on a very regular basis and in very similar spots when uploading through Ethernet. So that's that error sorted out (even if it's still a very real bug). The correlation with bed heating on/off seems to simply be a corrupted file vs an intact one.

Thanks for the quick help!



Now we'll still have to sort out why printing through USB sometimes fails, but i have a strong feeling that's simply an electrical connection issue.


---
**Arthur Wolf** *January 26, 2016 19:00*

Yay ! It's weird that it would get corrupted during the upload over ethernet. Maybe try another SD card just in case ?


---
**Thomas Sanladerer** *January 26, 2016 19:01*

**+Wolfmanjm** sorry, M140 and M190 - just meant to say bed heating commands in general.

But still, this bug surprisingly had nothing to do with heating...


---
**Thomas Sanladerer** *January 26, 2016 19:04*

**+Arthur Wolf** will do!


---
**Arthur Wolf** *January 26, 2016 21:24*

**+Alex Skoruppa** That sounds more like the S3D bug, do you know about that ? Do you use the online tools to fix the S3D gcode ?


---
**Arthur Wolf** *January 26, 2016 21:45*

**+Alex Skoruppa** [http://smoothieware.org/simplify3d](http://smoothieware.org/simplify3d)


---
**Hakan Evirgen** *January 26, 2016 23:24*

ah good to know about the bug and solution for S3D. I installed Smoothieboard in my BI v2.5 printer just yesterday and am working now on the cabling. Now I can avoid any such problems.



With Slic3r there are no know problems, right **+Arthur Wolf**?



And is this S3D bug the same which Leon had with his BI v2.5 printer?


---
**Arthur Wolf** *January 26, 2016 23:25*

**+Hakan Evirgen** Slic3r is fine

Leon had the S3D bug yes


---
*Imported from [Google+](https://plus.google.com/+ThomasSanladerer/posts/bgmbD4JWt6o) &mdash; content and formatting may not be reliable*
