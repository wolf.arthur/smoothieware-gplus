---
layout: post
title: "Did you know ? Smoothieware has a very very active IRC channel on Freenode : In the IRC channel, you can ask for help, discuss firmware development and talk about your favorite robots"
date: February 02, 2016 13:30
category: "General discussion"
author: "Arthur Wolf"
---
Did you know ? Smoothieware has a very very active IRC channel on Freenode : [http://smoothieware.org/irc](http://smoothieware.org/irc)



In the IRC channel, you can ask for help, discuss firmware development and talk about your favorite robots.



Often peaking at 100 persons connected, it's ( as far as I know ) the most active channel of any 3D printing or CNC milling firmware, and it's a great place to stay connected to, to follow news on firmware dev, or help newcomers.





**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/9rDb5ZjPqvW) &mdash; content and formatting may not be reliable*
