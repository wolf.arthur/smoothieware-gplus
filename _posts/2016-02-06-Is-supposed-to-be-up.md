---
layout: post
title: "Is supposed to be up?"
date: February 06, 2016 20:56
category: "General discussion"
author: "Ross Hendrickson"
---
Is [http://builds.smoothieware.org/](http://builds.smoothieware.org/) supposed to be up? 





**"Ross Hendrickson"**

---
---
**Arthur Wolf** *February 06, 2016 21:00*

It is sometimes, but if it isn't you should get the .bin over on github. We need to move it to a more stable server sometime.


---
**Ross Hendrickson** *February 06, 2016 21:01*

yeah, I snagged it off github. I was going to modify the wiki for the azteeg-x5 but thought I'd ask first. Thanks Arthur. Wish me luck! 


---
**Triffid Hunter** *February 07, 2016 10:17*

We've been asking Arthur and Mark to change the DNS pointer for years but it never gets done.. Builds currently points at the wrong IP address, should be 50.57.83.51


---
**Arthur Wolf** *February 07, 2016 10:24*

**+Triffid Hunter**  Host [builds.smoothieware.org](http://builds.smoothieware.org) updated. Update will be applied in approximately 1 minute.


---
**Triffid Hunter** *February 07, 2016 10:28*

Hmm update came through here already but it seems there's something up with my end; it used to work fine.. I'll fix it in a bit


---
**Triffid Hunter** *February 07, 2016 10:32*

Fixed :D


---
**Arthur Wolf** *February 07, 2016 10:36*

Yay :)


---
*Imported from [Google+](https://plus.google.com/+RossHendrickson/posts/eYH4Qwvb7wt) &mdash; content and formatting may not be reliable*
