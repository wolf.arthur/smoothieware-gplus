---
layout: post
title: "All wired up, got movement, home is not being nice"
date: February 07, 2016 08:01
category: "General discussion"
author: "Ross Hendrickson"
---
All wired up, got movement, home is not being nice. My direction on my jogs are correct and my endstops are at the motors. The trouble is, on G28 it is homing the wrong direction. Looking at the documentation I don't see a clear way to keep them, unless should I reconfigure the pins and set things to home_to_min?

![missing image](https://lh3.googleusercontent.com/-aGbHgaedLWs/Vrb5xovZjRI/AAAAAAAA0Ug/FI1V5nGb-N8/s0/20160207_012415.jpg)



**"Ross Hendrickson"**

---
---
**nick wiegand** *February 07, 2016 14:16*

Couldn't you change the way the steppers are wired to get them to travel in the other direction?


---
**Jason Richardson** *February 07, 2016 18:52*

for a delta, the endstops should be at the top of the travel.


---
**ThantiK** *February 07, 2016 20:01*

Yep - Deltas home towards the top of the machine.  You should be homing toward your max.


---
**Ross Hendrickson** *February 07, 2016 20:04*

Just reversed the dir pins. all good. now I need to slow it down a smidgen because the x moves so fast it jams up the y pillar. Note to self, building machines at 1AM while sick... not a good idea :D


---
*Imported from [Google+](https://plus.google.com/+RossHendrickson/posts/ifiaF7Zbx2M) &mdash; content and formatting may not be reliable*
