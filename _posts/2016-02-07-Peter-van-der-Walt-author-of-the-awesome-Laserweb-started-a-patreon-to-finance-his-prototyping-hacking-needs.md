---
layout: post
title: "+Peter van der Walt, author of the awesome Laserweb, started a patreon to finance his prototyping/hacking needs"
date: February 07, 2016 10:16
category: "General discussion"
author: "Arthur Wolf"
---
+Peter van der Walt, author of the awesome Laserweb, started a patreon to finance his prototyping/hacking needs. 



If you like Open-Hardware, cool web interfaces, fun PCBs, and Smoothieware, go become a patron now :)



[https://www.patreon.com/openhardwarecoza?ty=h](https://www.patreon.com/openhardwarecoza?ty=h)





**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/VzfYi6gMVXW) &mdash; content and formatting may not be reliable*
