---
layout: post
title: "Hi! Can someone explain to me in simple words, what junction deviation is and/or what it does?"
date: February 11, 2016 12:27
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
Hi! Can someone explain to me in simple words, what junction deviation is and/or what it does? Or is it just the SAME as jerk in marlin/repetier?

Pls no link to the GRBL-Git page, I got that googled ;)





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *February 11, 2016 12:28*

**+René Jurack** If you've read grbl's explanation, I'm not sure how much better to explain it.

In the simplest terms I can think of : higher junction deviation means the machine shakes more, lower junction deviation means the machine shakes less.


---
**René Jurack** *February 11, 2016 12:37*

Naaa... Not THAT simple, please :D Ok, maybe I need to be more precise: I did read the explanation and the way I understand it is, that e.g. if the printhead has to move a 90° corner, it would "take a short" and therefore I get no perfect 90° cornerpath but a "curve". Or am I completely wrong here? 


---
**Arthur Wolf** *February 11, 2016 12:38*

**+René Jurack** No, it always follows the exact right path. But depending on how steep the change in direction is, junction deviation determines by how much to slow down. So it's pretty much how_much_to_slow_down = change_in_direction_angle x junction_deviation.


---
**René Jurack** *February 11, 2016 12:45*

Ahh, ok, thx! So it is the same as "jerk" in Marlin/Repetier. Does it have a unit? Or how do I interpret the figures? I have a very rigid and fast printer, acceleration of 10.000mm/s² and more is possible, and now I want to adjust junction deviation as well. Standard setting was 0.1 but I don't know if I should try in 0.1 steps or in bigger ones like 10-20s...


---
**Arthur Wolf** *February 11, 2016 12:46*

It's not <b>exactly</b> the same, but it essentially <b>does</b> the same thing, just differently. It doesn't have a unit, it's an arbitrary ratio.


---
**René Jurack** *February 11, 2016 12:48*

Ok. So it depends on the "change_in_direction_angle" how high I could set this parameter? Like printing a big circle could work with a high setting, but with printing a triangle could result in loosing steps with the same high setting?


---
**Arthur Wolf** *February 11, 2016 12:49*

Pretty much. Just try different values and find what works for you. You can explore by changing orders of magnitude ( divide by 10, multiply by 10 )


---
**René Jurack** *February 11, 2016 12:55*

Ok, thank you very much for the explanation! One last thing: Where is junction deviation generally settled? Let's speak like a rule of thumb for non specialized printers, e.g. some simple mendelmaxes or standard coreXY/H-Bots... Is it more like "go search between 0,1 to 3" or more like "10-50"? 


---
**Arthur Wolf** *February 11, 2016 12:55*

Very very fast and rigid printers do values around 0.1 to 0.5, CNC mills do values around 0.001 , or 0.01 if very very rigid


---
**René Jurack** *February 11, 2016 12:59*

Thank you, **+Arthur Wolf**. I will try some settings this evening. IIRC, currently it is set to 0.3 without issues. Is there a test-STL for testing this or should I try a big triangle?


---
**Arthur Wolf** *February 11, 2016 12:59*

**+René Jurack** Anytihng with plenty of sharp angles will work fine.


---
**Hakan Evirgen** *February 11, 2016 14:53*

René please go on asking such questions here. Soon I will start tweaking config for my delta and then I do not need to ask anymore :D


---
**René Jurack** *February 11, 2016 14:54*

:D


---
**René Jurack** *February 12, 2016 16:45*

currently testing 10.000mm/s² with 0.5 junction deviation @ 100mm/s. Seems to work. Am I right, if I say: the higher the junction deviation, the better the printresults? Given, that the printer can handle high junction deviation in terms of no ghosting / no ringing / not loosing steps etc... 


---
**Arthur Wolf** *February 12, 2016 19:59*

The higher the junction deviation, the faster the prints. How that impacts print quality is going to depend on the machine a lot.


---
**René Jurack** *February 14, 2016 16:05*

So, currently printing at 100mm/s /  10.000mm/s² and junction deviation of 0.5mm with no issues so far. Print-quality looks better, but sharp corners are now a bit "rounder" than with junction deviation of like 0.1 or so.


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/G3j4Sqmh5kz) &mdash; content and formatting may not be reliable*
