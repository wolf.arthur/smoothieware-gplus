---
layout: post
title: "Do I have to keep something in mind, when homing fast and slow in combination?"
date: February 14, 2016 16:11
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
Do I have to keep something in mind, when homing fast and slow in combination?



In the config, I wrote:

gamma_fast_homing_rate_mm_s   20	gamma_slow_homing_rate_mm_s   2



Now, with command "G28 Z", the bed just goes with 20mm/s to the endstop and stays there, homing is finished. But I want it to go homing with 20mm/s and then again home one more time, more precisely, with 2mm/s.



What do I have to do, to get this working?



Edit: Ahhh... I got it... I need to set "gamma_homing_retract_mm"              as well... <b>sigh</b>





**"Ren\u00e9 Jurack"**

---


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/FSoWcSB8hU8) &mdash; content and formatting may not be reliable*
