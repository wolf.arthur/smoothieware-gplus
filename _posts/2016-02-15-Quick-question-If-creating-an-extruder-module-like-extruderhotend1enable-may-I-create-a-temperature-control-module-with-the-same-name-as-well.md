---
layout: post
title: "Quick question: If creating an extruder-module, like \"extruder.hotend1.enable\", may I create a temperature-control-module with the same name as well?"
date: February 15, 2016 08:09
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
Quick question: 

If creating an extruder-module, like "extruder.hotend1.enable", may I create a temperature-control-module with the same name as well? Like "temperature_control.hotend1.enable"? 

Or do I need to name them differently?





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *February 15, 2016 08:57*

They can have the same name no problem.


---
**René Jurack** *February 15, 2016 09:31*

thx


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/7GTRyVjLatf) &mdash; content and formatting may not be reliable*
