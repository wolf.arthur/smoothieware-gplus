---
layout: post
title: "So, I'm using 'temperatureswitch' to toggle the fan of my E3D-V6 with the hotend-temperature, like shown at ."
date: February 15, 2016 10:42
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
So, I'm using 'temperatureswitch' to toggle the fan of my E3D-V6 with the hotend-temperature, like shown at [http://smoothieware.org/temperatureswitch](http://smoothieware.org/temperatureswitch). It works well if I set temperatures manually.

Now, I recognized, if I start a print via USB, the switch doesn't work UNTIL the very print begins. During heating to target-temperature, the fan is off (no matter what temperature is read) and switches to on at the first move of the axis. I think, it has to do with the M109-command.

Is there a workaround?





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *February 15, 2016 10:43*

Yeah, that's due to the M190 command, which is blocking, and I don't know of a solution, appart from pre-heating as soon as the machine starts, maybe.


---
**René Jurack** *February 15, 2016 10:52*

preheating... ok: What if I put a 'M109 S80' in the start-gcode? The generated code will look like:



G28; <- manual start gcode

M104 S80; <- manual start gcode

M109 S80; <- manual start gcode

M104 S245; <- generated via slicer

M109 S245; <- generated via slicer



etc...



Would this be able to trigger the switch? (switch is set to 60°C)


---
**Arthur Wolf** *February 15, 2016 10:56*

Yeah I think that'd work, you should try it.


---
**René Jurack** *February 15, 2016 11:04*

ok, thank you. Again :D


---
**Michael Hackney** *February 15, 2016 14:10*

René, I'm the original author of temperature switch. I tried to find a solution for this issue. The M109 in the start code should do it as long as the temp is above the threshold temp. 



**+Arthur Wolf** is there any way to monitor the raising temp during M190 to activate TemperatureSwitch?


---
**Arthur Wolf** *February 15, 2016 14:12*

**+Michael Hackney** I don't think so.


---
**Jarrid Kerns** *February 15, 2016 18:49*

Why have the fan start during heat? 


---
**Michael Hackney** *February 15, 2016 18:51*

This fan is the fan that cools the cold zone of the hot end. You need the fan to be running if the hot end is warming up or hot.


---
**Wolfmanjm** *February 16, 2016 21:46*

FWIW M190 and its counterparts are evil and should never be used ;) I never use them I always preheat manually.


---
**Wolfmanjm** *February 16, 2016 21:52*

but FWIW the reason it does not work is that M190 blocks all gcodes until the temp is reached and that includes the command that switches the fan on. The temp is monitored all the time in ON_IDLE so it detects the temp and issues the command to turn on the fan, but M190 blocks the on_main_loop so the switch will not turn on until that block is removed. So there is no way to fix it as it is not broken it is just doing what it is being told to do. (and switch must be done in on_main_loop and not on_idle, which would fix it).


---
**Chris Brent** *February 18, 2016 16:42*

I don't believe I'm writing this but... Couldn't we break M190 and allow some pre-emptive commands to run? e.g status M codes, temp dependant stuff like this. I know it would be messy and a terrible idea, but I somehow still want it. I want M109 to be: "Heat to this temperature, but don't start printing, non move/extrude operations are allowed"


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/FJHHvxJzwK3) &mdash; content and formatting may not be reliable*
