---
layout: post
title: "Originally shared by Jeremie Francois The more I get closer to the smoothieboard, the more I am amazed by the quality of the documentation"
date: February 18, 2016 22:33
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Jeremie Francois</b>



The more I get closer to the smoothieboard, the more I am amazed by the quality of the documentation. When you need an ecosystem where you don't need to tinker with bazillions versions and interdependent #defines or lack of memory, then this is real stuff :)

Example? On linux, the doc even gives you copy/paste udev rules to have the smoothieboard recognized in its own class.

PS: yep we're now switching our (so far half-) printer to smoothie after an amazing number of issues with our existing printers and marlin (gods were all against us for the last two days, what a pity!). I wish it soon runs so it can print its own missing parts.

The board is costly and not perfect feature-wise, but what a smooth and refreshing change, thanks **+Arthur Wolf** for all this work! I hope and expect we'll keep on enjoying the journey so far :)





**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/8miTZxRAnq8) &mdash; content and formatting may not be reliable*
