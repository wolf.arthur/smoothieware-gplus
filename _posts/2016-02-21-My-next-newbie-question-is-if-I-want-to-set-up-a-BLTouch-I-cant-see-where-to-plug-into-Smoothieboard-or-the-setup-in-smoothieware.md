---
layout: post
title: "My next newbie question is if I want to set up a BLTouch I can't see where to plug into Smoothieboard or the setup in smoothieware"
date: February 21, 2016 01:53
category: "General discussion"
author: "James Sinnott"
---
My next newbie question is if I want to set up a BLTouch I can't see where to plug into Smoothieboard or the setup in smoothieware.



Thank you





**"James Sinnott"**

---
---
**James Sinnott** *February 22, 2016 03:31*

not simple is it


---
**Gústav K Gústavsson** *March 23, 2016 02:08*

Will probable be doing first run on our printer after complete rebuild in a day or two. Smoothieboard, dual z, Bltouch, Alirubber heater and RepRapDiscount smart graphic controller. Let's see how it goes and will share some info when I have the time.


---
**Gústav K Gústavsson** *March 26, 2016 10:54*

I have connected the BLtouch to Smoothieboard but I am having some problems. Hooked it up to a small mosfet set up for Pwm (2.4!) and I am supplying 5 volt for the small mosfet from the 7805 voltage regulator I added to the board (also powering RepRapDiscount smart controller display). Max PWM set to 255.

The Bltouch powers up and finishes self test. The pin is down after test. 

Sending S90 out of pin does nothing, have to send S247 to S251 to have the pin retracting. Seems it needs a lot more PWM power than expected to operate.

Sending S10 releases the pin.

Sending self test code (S something, can't remember exactly don't have the info right now) does nothing.



If I get the Bltouch to error mode (led blinking) S160 stops the blinking. However if I try to release/retract the pin it immediately returns to error. Have to unplug/plugin again the sensor to get it to operate again.



The voltage on the BLTouch seems to be 4.98, should be enough but will try to hook up a (variable) 5v 2A external supply straight to BLTouch connector to be sure PSU/voltage drop in wiring is not the issue.



Any help much appreciated 




---
*Imported from [Google+](https://plus.google.com/109661062702020544665/posts/K1musNBqUPi) &mdash; content and formatting may not be reliable*
