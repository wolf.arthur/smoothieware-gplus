---
layout: post
title: "With the lack of availability of the Smoothieboard at Uberclock, it got me thinking..."
date: February 24, 2016 02:04
category: "General discussion"
author: "Jonathan Katz"
---
With the lack of availability of the Smoothieboard at Uberclock, it got me thinking...  Should I wait until V2.0 for my 2nd 3D printer?



Just wondering...





**"Jonathan Katz"**

---
---
**Zane Baird** *February 24, 2016 02:12*

If you aren't planning on dual extruders you may consider the Azteeg Mini X5 V2.0 from panucatt. About 1/4 the size of the smoothieboard.


---
**Ross Hendrickson** *February 24, 2016 02:15*

I just got a panucatt too. Smoothie goodness in roughly a printrboard sized package


---
**Jeff DeMaagd** *February 24, 2016 03:57*

I don't understand how they justify getting angry at other sourcings when they're out of stock for months at a time. OK, the folks that violate license terms, I get that, but they were also literally bone dry out of the boards for most of the second half of 2015 too, making it impossible to buy first party.


---
**Chris Brent** *February 24, 2016 04:14*

Yeah it was tricky for a while. I thought Uberclock had some though? **+Arthur Wolf** had some really cheap boards on Ebay (I think) that didn't have a working Ethernet port. Seemed like a good trade off as I never use mine.


---
**Arthur Wolf** *February 24, 2016 09:14*

**+Jeff DeMaagd** We don't get angry at people getting boards that are not made by us. Sure, go get an Azteeg, especially if we are out of stock.

The problem with MKS or AZSMZ has nothing to do with "not made by us", it has to do with "benefits from the community's work but doesn't give anything back".



About being out of stock : once more we didn't manage chinese new year properly, and Uberclock is going to be out of stock for a few weeks more. RobotSeed has stock most of the time, the current outage shouldn't last more than about a week.


---
**Jonathan Katz** *March 11, 2016 19:27*

Any news on Uberclock getting the boards back in stock?


---
**Arthur Wolf** *March 11, 2016 19:27*

**+Jonathan Katz** Currently producing, should be in stock in a few weeks.


---
**Jonathan Katz** *March 11, 2016 19:28*

Thanks!  I'm ready to start the upgrade of my larger printer...


---
*Imported from [Google+](https://plus.google.com/+JonathanKatz/posts/Usaa8TBpWDG) &mdash; content and formatting may not be reliable*
