---
layout: post
title: "Originally shared by Richard Horne Maybe good timing to mention an article I wrote for the Disruptive Magazine recently on and various 3D Printing projects going on - Read Issue 5 of the magazine here >> Let me know if you"
date: February 25, 2016 14:24
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Richard Horne</b>



Maybe good timing to mention an article I wrote for the Disruptive Magazine recently on #OpenSource #Licensing and various 3D Printing projects going on - Read Issue 5 of the magazine here >> [http://disruptivemagazine.com/wp-content/uploads/2015/02/Disruptive_Magazine_Issue5.pdf](http://disruptivemagazine.com/wp-content/uploads/2015/02/Disruptive_Magazine_Issue5.pdf)



#RepRap #smoothie #octoprint 



Let me know if you have any thoughts...

![missing image](https://lh3.googleusercontent.com/-_z0v-ql0hNA/Vs8MZvRcxzI/AAAAAAAAI8Q/hQiEq66RJmE/s0/Disruptive_mag.jpg)



**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/9ffM3tLPmQ9) &mdash; content and formatting may not be reliable*
