---
layout: post
title: "Is there a way to switch a pin depending on wether the printer is active or not?"
date: February 27, 2016 10:20
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
Is there a way to switch a pin depending on wether the printer is active or not? I want a pin to toggle if a motor is active or a heater is on. I want to use it for cooling my general electronics and I want to have less noise when idle...





**"Ren\u00e9 Jurack"**

---
---
**Craig Cavanaugh** *February 27, 2016 10:42*

This is how I do it... create custom MCODES and configure the slicer to call them as needed.



# hot end cooling fan

switch.misc.enable                           true           

switch.misc.input_on_command                 M42               

switch.misc.input_off_command                M43               

switch.misc.output_pin                       2.4               

switch.misc.output_type                      digital    



# chassis fan

switch.cool.enable                           true              

switch.cool.input_on_command                 M44               

switch.cool.input_off_command                M45               

switch.cool.output_pin                       1.22              

switch.cool.output_type                      pwm        

switch.fan.max_pwm                           255         

switch.fan.startup_value                     210  


---
**René Jurack** *February 27, 2016 11:03*

Yeah, but this is only while printing. I need one for moving manually axis around or putting a heater on manually.


---
**Jeff DeMaagd** *February 28, 2016 12:53*

Temperature control will let you automatically turn on a fan whenever a thermistor is above a set temp. It's easy to set up.



I don't know about the other one. Are you experiencing problems with the electronics? I don't think smoothieboard requires special or Herculean cooling. I've run passive even when experimenting with high current motors.


---
**René Jurack** *February 28, 2016 13:07*

I did build a <b>VERY</b> compact and enclosed printer and therefore I need a coolingfan for the complete electronic-compartment (it's all in there, even the PSU). While leaving a small fan always on is no problem, it would be even better, if it is only on by demand.


---
**Jeff DeMaagd** *February 28, 2016 15:53*

I couldn't find anything. <b>maybe</b> use a switch module that reads an axis step pin. I don't know if smoothieware allows that without jumpering to another input pin.


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/EqaFZo1zhFc) &mdash; content and formatting may not be reliable*
