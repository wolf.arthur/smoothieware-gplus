---
layout: post
title: "Can anybody give me advise on what I should do here"
date: February 29, 2016 01:44
category: "General discussion"
author: "Matt Omond"
---
Can anybody give me advise on what I should do here. I am very close to calling it a day and writing this board off. I was removing a fried processor and over heated the board with some wick and lost a pad. See picks. Is there anything that can be done to repair a pad. I have seen stuff done on larger pads, but this is minuscule. Heeelllpppp!!

 



![missing image](https://lh3.googleusercontent.com/-x1NCg_bx4Mo/VtOia9dv3cI/AAAAAAAAGXk/HkCc2YlpFhI/s0/board2.jpg)
![missing image](https://lh3.googleusercontent.com/-U_xndpVcyP8/VtOiawg-njI/AAAAAAAAGXk/onY-vSP8xrg/s0/board1.jpg)

**"Matt Omond"**

---
---
**Sébastien Plante** *February 29, 2016 01:49*

Is this pad used? might have no connection to it if you are lucky :)


---
**Matt Omond** *February 29, 2016 01:53*

funny you say that I was trying to see if it went to something. but couldn't see. I might need to get closer 


---
**Sébastien Plante** *February 29, 2016 01:55*

look at the specsheet of the chip. **+Arthur Wolf**​ might be able to help too :)


---
**ThantiK** *February 29, 2016 02:49*

Looks like pin 18, and that's the pin to the XTAL (clock) - might be able to recover it...if you're good.



RTCX2, output from oscillator circuit.  That looks like it nearby.


---
**ThantiK** *February 29, 2016 02:51*

Nope -- You're in luck.  According to this: [http://smoothieware.org/smoothieboard-schematic](http://smoothieware.org/smoothieboard-schematic) - There's no connection at all.  Look at RTCX1 and 2.  Pin 16 and 18 are not connected in that picture.


---
**Matt Omond** *February 29, 2016 03:15*

Ah how nice would that be. Thanks so much for looking that up. My chances of getting this back together are still low. But at least I can still give it a go.. That may be why that on came off. HAd no conection to anything




---
**ThantiK** *February 29, 2016 03:28*

My guess is that is an output from some sort of internal oscillator. 


---
**Electra Flarefire** *February 29, 2016 04:17*

2 part epoxy should hold the pad down long enough to solder it, if you have to. And if the connections are not damaged.

And RTCX1 and X2 are for a 32kHhz crystalf or the Real Time Clock. :)


---
**Matt Omond** *February 29, 2016 04:22*

**+Electra Flarefire** so they are needed ?




---
**Triffid Hunter** *February 29, 2016 04:26*

Find where the signal goes (if anywhere), solder a wire to a suitable location. See [http://imgur.com/4aMXZZZ](http://imgur.com/4aMXZZZ) for one example where I've rewired stuff of this size


---
**Electra Flarefire** *February 29, 2016 04:27*

**+Matt Omond** they are not connected on the schematic, so no. Just verify that you have your pin numbers right, but you should be good to leave it unconnected. 

It's normally connected to a crystal that looks like: [http://www.mgsuperlabs.co.in/EFL/image/cache/data/32%20Hz%20crystal-500x500.jpg](http://www.mgsuperlabs.co.in/EFL/image/cache/data/32%20Hz%20crystal-500x500.jpg)


---
**Matt Omond** *February 29, 2016 04:28*

You guys are great. Thanks heaps.


---
**Stephanie A** *February 29, 2016 07:11*

If you can't manage it, I'll buy it from you. Or I can be nice and fix it for cost of shipping (you supply the parts).


---
**Matt Omond** *February 29, 2016 08:07*

**+Stephanie S** I could be tempted. I would really like to solve it myself.




---
**Matt Omond** *March 13, 2016 01:31*

**+Stephanie S** Anyone up for a challenge. Reflow is not my bag. Just made a mess. I would love to get this running again. I would really appreciate if someone could donate their skills. I have the bits hopefully haven't stuffed the Processor.  


---
**Stephanie A** *March 13, 2016 08:00*

Ok. Send me a message. 


---
**Matt Omond** *March 13, 2016 10:14*

**+Stephanie S** I thought I had sent you a PM. But stupid Google has changed the way you send messages now and I can't work out how to check if it even sent.


---
*Imported from [Google+](https://plus.google.com/+MattOmondPrintbotOMO/posts/6p44p775MXs) &mdash; content and formatting may not be reliable*
