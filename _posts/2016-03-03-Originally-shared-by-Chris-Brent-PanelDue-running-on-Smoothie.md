---
layout: post
title: "Originally shared by Chris Brent PanelDue running on Smoothie"
date: March 03, 2016 05:01
category: "General discussion"
author: "Chris Brent"
---
<b>Originally shared by Chris Brent</b>



PanelDue running on Smoothie. Still need to get the bed in correct array index. But it's working! **+Arthur Wolf**​

![missing image](https://lh3.googleusercontent.com/-v6CqO1aLzGo/VtfE8P_Xd2I/AAAAAAAAPgg/WHFgaPOIp0M/s0/IMG_20160302_205831.jpg)



**"Chris Brent"**

---
---
**Philipp Tessenow** *March 03, 2016 05:45*

Hi, great stuff... Is there a link on how to do this? Thanks ;)


---
**Arthur Wolf** *March 03, 2016 08:43*

**+Chris Brent** Wow really nice !!! :)


---
**Chris Brent** *March 03, 2016 16:44*

**+Philipp Tessenow** Give me a day to sort out the heater ordering and I'll write it up. I have a branch with the M code to support this status screen.


---
**Chris Brent** *March 10, 2016 00:44*

Errr, make that a few days! Sorry.


---
**Chris Brent** *March 15, 2016 04:00*

Some usable code is here:

[https://github.com/chrisbrent/Smoothieware/tree/feature/due_panel](https://github.com/chrisbrent/Smoothieware/tree/feature/due_panel)

It assumes the Bed has a designator of "B". It's merge with the latest edge so if you're setup to build Smoothieware give it a try. I hooked mine up to the serial debug port.


---
*Imported from [Google+](https://plus.google.com/+ChrisBrent/posts/UZwxcSKoMfD) &mdash; content and formatting may not be reliable*
