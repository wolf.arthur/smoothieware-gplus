---
layout: post
title: "Bounty ! Get a free Smoothieboard v1 and Smoothieboard v2 in exchange for a PCB design :) Please reshare to your PCB-designing friends :) I'm offering one v1 board and one v2 board ( when available ), to the person that will"
date: March 04, 2016 15:50
category: "General discussion"
author: "Arthur Wolf"
---
<b>Bounty ! Get a free Smoothieboard v1 and Smoothieboard v2</b> in exchange for a PCB design :) Please reshare to your PCB-designing friends :)



I'm offering one v1 board and one v2 board ( when available ), to the person that will design the following PCB :

* One-sided PCB, can be CNC-milled at home ( like [http://reprap.org/wiki/Gen7](http://reprap.org/wiki/Gen7) )

* All components PTH, except for the microcontroller ( LPC1769 to run Smoothieware )

* Pololu sockets for 5 drivers

* USB connector

* SD card socket

* 6x Endstop inputs, 4x Thermistor inputs

* 4x Mosfet able to handle at least 12A with adequate connectors

* 2x fan controllers

* ATX PSU main power input

* Connectors for Reprapdiscount GLCD

* As compact as possible

* Must be Open-Hardware ( CERN OHL )



It's time we had a Smoothie-based CNC-millable PCB, for the hardcore DIYers :)



Please reshare, it'll help get this done :)





**"Arthur Wolf"**

---
---
**Triffid Hunter** *March 04, 2016 15:57*

Hmm might be possible without onboard drivers and Ethernet, but it's gonna be pretty damn tricky, good luck!


---
**Arthur Wolf** *March 04, 2016 15:59*

**+Triffid Hunter** Yep, the plan is to have the MCU be the only SMT part. Drivers would be pololu, and no Ethernet. Hope somebody will do it :)


---
**Stephanie A** *March 04, 2016 16:17*

Good luck with a smd sd card socket. And proper decoupling caps and power planes. 

I would attempt this, but not gonna happen on the free version of Eagle. 

Double sided, maybe. The all through hole requirement would make it crazy difficult,I won't even try. 


---
**Arthur Wolf** *March 04, 2016 16:20*

**+Stephanie S** A SD card breakout is a tolerated solution here :)

I forgot to mention it in the post but ideally it should be done in Kicad, as that's what we are doing Smoothieboard v2 dev with, and will probably stick with in the future.


---
**Chris Brent** *March 04, 2016 17:22*

Sounds like a job for **+Peter van der Walt** ....


---
**Arthur Wolf** *March 04, 2016 18:00*

It should have both the option to use a soldered smt chip and the footprint fot a breakout﻿


---
**Jeremie Francois** *March 04, 2016 23:16*

I .. did .. not ... read this post :D


---
**Daniel Dumitru** *March 11, 2016 14:43*

I will try to do it. Do you have a deadline ?




---
**Arthur Wolf** *March 11, 2016 14:43*

**+Daniel Dumitru** No deadline. Can you email me about this, so I can write a spec for it and we can discus it ?



Cheers :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/j5uwNVSfVTr) &mdash; content and formatting may not be reliable*
