---
layout: post
title: "Originally shared by Adafruit Industries HowTo Make Perfectly Clear Ice Spheres Via The Wandering Engineer Ive become obsessed recently with getting perfectly clear ice for my drinks"
date: March 11, 2016 14:03
category: "General discussion"
author: "Artem Grunichev"
---
<b>Originally shared by Adafruit Industries</b>



<b>HowTo Make Perfectly Clear Ice Spheres</b> 

[https://blog.adafruit.com/2016/03/10/howto-make-perfectly-clear-ice-spheres-tips-tricks/](https://blog.adafruit.com/2016/03/10/howto-make-perfectly-clear-ice-spheres-tips-tricks/)



Via The Wandering Engineer



<i>I’ve become obsessed recently with getting perfectly clear ice for my drinks. Here are my experiences getting almost perfectly clear ice spheres. The beginning of this post will talk about how to get clear ice, then how I progressed through cubes, and eventually how to get clear ice spheres every time.</i>



<b>Read more</b>

[https://blog.adafruit.com/2016/03/10/howto-make-perfectly-clear-ice-spheres-tips-tricks/](https://blog.adafruit.com/2016/03/10/howto-make-perfectly-clear-ice-spheres-tips-tricks/)

![missing image](https://lh3.googleusercontent.com/-6flE4AIBTRk/VuGbKrFrbXI/AAAAAAACLHc/_hVNyQ88DRU/s0/img_5341.jpg)



**"Artem Grunichev"**

---


---
*Imported from [Google+](https://plus.google.com/101033487868604831015/posts/EhcdsSBVJgw) &mdash; content and formatting may not be reliable*
