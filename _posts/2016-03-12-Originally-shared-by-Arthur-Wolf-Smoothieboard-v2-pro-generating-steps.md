---
layout: post
title: "Originally shared by Arthur Wolf Smoothieboard v2 pro generating steps"
date: March 12, 2016 17:51
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



Smoothieboard v2 pro generating steps.



This may not look like much, but it's actually very good news.

Smoothie2, the port of Smoothieware to the v2 hardware ( LPC4337 ), is now capable of : 

* Receiving Gcode over serial ( uart only, no USB yet )

* Planning movements

* Generating steps to control stepper motor drivers



The picture, is those steps, read by a logic analyser.



This means that <b>technically</b>, I could right now put the v2 board in a CNC mill, laser cutter or CNC plotter, and actually use it to do useful things.



Pretty cool right ?



There is still A LOT to do, but progress always feels good.



As always, if anybody is interrested in helping with this firmware port, any kind of help is extremely welcome. Contact me at wolf.arthur@gmail.com



Cheers all :)

![missing image](https://lh3.googleusercontent.com/-4TlOPFZlueQ/VuRW9enC4-I/AAAAAAAAL8o/TuL55VuqMmI/s0/Screenshot%252Bfrom%252B2016-03-12%252B18%25253A45%25253A22.png)



**"Arthur Wolf"**

---
---
**Mert G** *March 12, 2016 17:57*

+Arthur Wolf very good news indeed! I hope we can get SB2 before this Christmas. I think it sounds realistic right.﻿


---
**Arthur Wolf** *March 12, 2016 17:58*

**+Mert G** I sure hope we'll be ready by then, but can't know for sure, will depend a lot on how much help we get.


---
**Chris Brent** *March 12, 2016 21:37*

Awesome! I'm about to take some funemployment time so if you need grunt work done move code over, let me know!


---
**Arthur Wolf** *March 12, 2016 21:39*

**+Chris Brent** We most definitely need that ! Can you email me at wolf.arthur@gmail.com so I can add you to the list of folks that offered to help, try to figure out how to get hardware to you etc ... ? Thanks :)


---
**Chris Brent** *March 12, 2016 22:01*

Done.


---
**Eric Lindahl** *March 13, 2016 23:52*

**+Arthur Wolf** is the latest v2pro code stream in github? Can only verify via cross compiler without a board.


---
**Arthur Wolf** *March 14, 2016 08:11*

**+Eric Lindahl** yep the code is here : [https://github.com/Smoothieware/Smoothie2](https://github.com/Smoothieware/Smoothie2)


---
**Bouni** *March 17, 2016 07:43*

**+Arthur Wolf** in my opinion it would be very nice to have at least a markdown file file within the smoothie2 repo that describes detailed what jobs need to be done. Its quite hard to find info about that at the moment.


---
**Arthur Wolf** *March 17, 2016 08:37*

**+Bouni** Working on that very soon, yes. Several folks have offered to help, and I'm going to be making a list of things todo and helping them get started.


---
**Nate C** *April 20, 2016 00:43*

Arthur If you need more help like Chris offered I'm open to help with whatever. 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/4jqnsx7wVbf) &mdash; content and formatting may not be reliable*
