---
layout: post
title: "Alex Skoruppa I was wondering if you could help out - I've been going through your github repository with all the Kossel Pro parts, and I've noticed, the bed standoffs with the holes for the the FSRs wasn't there"
date: March 16, 2016 20:42
category: "General discussion"
author: "Chris Wilson"
---
**+Alex Skoruppa**  I was wondering if you could help out - I've been going through your github repository with all the Kossel Pro parts, and I've noticed, the bed standoffs with the holes for the the FSRs wasn't there.  Could you possibly post those files, I'd like to move to an FSR solution with my Kossel Pro as well.





**"Chris Wilson"**

---
---
**Chris Wilson** *March 16, 2016 21:55*

**+Alex Skoruppa** Thank you!!  Found it.  When you were using it, did you find it worked well for you?  Right now, I'm just having a hell of a time with leveling using the existing probe that came with the kossel...


---
*Imported from [Google+](https://plus.google.com/112111122069927621889/posts/KstqKhoVbiS) &mdash; content and formatting may not be reliable*
