---
layout: post
title: "I have an Azteeg X5 running on my delta, and just recently my cooling fan won't shut off"
date: March 18, 2016 15:08
category: "General discussion"
author: "Chris Wilson"
---
I have an Azteeg X5 running on my delta, and just recently my cooling fan won't shut off. Normally, when I power it up the cooling fan's off with no issues, and it stays off as it should until the second layer of the print.  Now it's on constantly. I haven't changed anything in the config with relation to the fan setting. M107 won't stop it - it's just alwys on.



    # Switch module for fan control

    -----------------------------------------------------------------------------

    switch.fan.enable                                true               #

    switch.fan.input_on_command         M106           #

    switch.fan.input_off_command        M107            #

    switch.fan.output_pin                         2.4                 #







These settings have been the same since I had originally set this up. I've even compared it to some archived config files I've saved and they're the same. But now my fan won't turn off??? Any suggestions?





**"Chris Wilson"**

---
---
**Philipp Tessenow** *March 18, 2016 17:12*

Broken MOSFET? Did you try a different mosfet?

[http://smoothieware.org/3d-printer-guide#toc18](http://smoothieware.org/3d-printer-guide#toc18)
"Fans ( and other active loads like solenoids, mechanical relays, motors, anything with a coil ) can feed power back into the MOSFET and destroy it."



check if you installed a diode across the MOSFET's power output.


---
*Imported from [Google+](https://plus.google.com/112111122069927621889/posts/2ketVEaJRbd) &mdash; content and formatting may not be reliable*
