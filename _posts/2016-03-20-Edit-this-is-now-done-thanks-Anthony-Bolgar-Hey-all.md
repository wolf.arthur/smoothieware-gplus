---
layout: post
title: "Edit : this is now done, thanks Anthony Bolgar Hey all"
date: March 20, 2016 18:01
category: "General discussion"
author: "Arthur Wolf"
---
Edit : this is now done, thanks **+Anthony Bolgar**



Hey all. Need help !



+Peter van der Walt and I are going to be working on a project : [https://github.com/arthurwolf/fabrica](https://github.com/arthurwolf/fabrica)



I've started coding it a bit, and I'm getting to a point where there is a huge non-programming task to do, that would be reasonably easy to get done by many folks at a time.



The task is essentially to describe all of Smoothie's configuration options : their name, a short description of what they are, their type, their unit, and a more complete few-lines documentation.



The file that needs to get added to is here : [https://github.com/arthurwolf/fabrica/blob/gh-pages/src/screens/configuration/definitions.html](https://github.com/arthurwolf/fabrica/blob/gh-pages/src/screens/configuration/definitions.html)

You essentially need to do what has been done for the first few options, but for the rest of them.



Note that you don't even need to know GIT : If you have a github account, you can edit the file directly from the github website.



If anybody wants to help with this, please let yourself known. It's relatively easy, and it will help a lot make something really awesome for everybody.



Counting on you !



Cheers :)





**"Arthur Wolf"**

---
---
**Mert G** *March 20, 2016 21:25*

**+Arthur Wolf**​ simply you want to extract smoothie config options to a HTML code. It might be fairly easy  with a simple copy paste combo. I can't promise as I'm busy nowadays but i'll try.


---
**Arthur Wolf** *March 20, 2016 21:34*

**+Mert G** It's more than that. We also need add some explanation about what the configuration option is for ( think " how would I explain it to somebody who has never used anything like smoothie " ), and some information about the option itself, like "is it a number" etc


---
**René Jurack** *March 20, 2016 21:53*

Maybe I don't get the point, but what about [http://smoothieware.org/configuration-options](http://smoothieware.org/configuration-options) ? Isn't it this, what you are looking for?


---
**Arthur Wolf** *March 20, 2016 21:54*

**+René Jurack** I need that, but in the format mentionned on github, and with more detailled/beginner-friendly explanations of each. And with some formatting/validation information for each value.


---
**René Jurack** *March 20, 2016 21:57*

Would be easier to join, if you would have listed all available options already. And then, link [http://smoothieware.org/configuration-options](http://smoothieware.org/configuration-options) in the description for a little guideness. 


---
**Chris Brent** *March 21, 2016 03:43*

I was thinking today how nice it would be to have a configuration workflow tools for Smoothie newcomers (something like Repetier has). This project sounds like a control panel rather than that, but it would be a good start.


---
**Arthur Wolf** *March 21, 2016 08:53*

**+Chris Brent** The plan is for the project to include wizard-like configuration tools for everything.

It'll also have a "raw" configuration tool for more advanced users, and that's what the "definitions" files is for.

But most definitely planning on doing simpler stuff too.

I originally had a project called "mousse", but the architecture was too complex and the project too ambitious so I scrapped it in favor of fabrica. But I had started doing config tools there too : 
{% include youtubePlayer.html id="NKwcuq5O-mc" %}
[https://www.youtube.com/watch?v=NKwcuq5O-mc](https://www.youtube.com/watch?v=NKwcuq5O-mc)


---
**Eric Lindahl** *March 21, 2016 15:58*

For single page, reviewed code a bit.  Couldn't we use gulp and build a single page?  [https://github.com/shkuznetsov/gulp-minify-inline](https://github.com/shkuznetsov/gulp-minify-inline)


---
**Arthur Wolf** *March 21, 2016 16:01*

**+Eric Lindahl** Thanks that looks great, I'll look into it a bit later !


---
**Eric Lindahl** *March 21, 2016 16:05*

I'll fork and try to add it, but can't get to it until this weekend. If you don't get to it, I'll send you a pull request.﻿. Maybe write a parser to import machine configurations from other hosts, like simplify 3d etc﻿


---
**Arthur Wolf** *March 21, 2016 16:09*

That'd be super neat


---
**Richard Marko** *March 21, 2016 18:44*

Why can't we have such description in code or just included in the smoothie repository so anyone can build on such stuff? Many people talked about this regarding undocumented G/M-codes that needs to be kept in sync with wiki <b>manually</b>. It doesn't make any sense to create a HTML page - that should be generated from something..


---
**Arthur Wolf** *March 21, 2016 18:45*

**+Richard Marko** That'd be neat, we will get there eventually I guess, when somebody does it.


---
**Richard Marko** *March 21, 2016 20:01*

**+Arthur Wolf** Yup, and this looks like a good opportunity to point the volunteer to the right place.


---
**Richard Marko** *March 21, 2016 20:08*

This entry from Smoothie contest comes to my mind: 
{% include youtubePlayer.html id="NI9NCI0UJYs" %}
[https://www.youtube.com/watch?v=NI9NCI0UJYs](https://www.youtube.com/watch?v=NI9NCI0UJYs)



Most host software would benefit from such documentation.

Disclaimer: I'm working on my own host (which currently targets Smoothieware).


---
**Anthony Bolgar** *May 17, 2016 09:41*

Hey, didn't some guy already finish this for you? <grin!>


---
**Arthur Wolf** *May 17, 2016 17:00*

**+Anthony Bolgar** You did :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/FZrGZ3fNaC8) &mdash; content and formatting may not be reliable*
