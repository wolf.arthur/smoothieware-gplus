---
layout: post
title: "+Adam Green is working on a new SD card driver for Smoothie"
date: March 23, 2016 22:26
category: "General discussion"
author: "Arthur Wolf"
---
+Adam Green is working on a new SD card driver for Smoothie.

If that code ends up integrated into Smoothie, possible benefits include insane increases in speed, and being error-tolerant when using an external SD card ( CRC Checks/Error recovery ). Neat !



If you own : 

* A Smoothieboard ( or a compatible board )

* A SD card that did not came with your board ( those were already tested )

* A FTDI cable



You can totally help !

We need testing of the code. It'll help make it better. It's pretty easy : 



1.  Download [https://github.com/adamgreen/SDCard/blob/master/bins/SoakTest-Smoothie1.bin](https://github.com/adamgreen/SDCard/blob/master/bins/SoakTest-Smoothie1.bin)

2. Rename to "firmware.bin"

3. Paste the file to your SD card

4. Reset the board. The board flashes the test program

5. Connect a FTDI cable to the "Serial" ( UART ) port on the Smoothieboard ( near the USB connector ). Note : USB/Serial using Smoothie's USB port doesn't work for this.

6. Connect using your favorite Serial terminal ( I use cutecom ). Baud rate : 9600

7. Reset the board, follow instructions.



If your run shows any error, or if it ran for a full night without errors, report to Adam.



Adam wrote very nice instructions here : [https://github.com/adamgreen/SDCard](https://github.com/adamgreen/SDCard)

If you need help with anything, I'm here.



Cheers, and thanks a lot to all you testers !!!











**"Arthur Wolf"**

---
---
**Arthur Wolf** *March 24, 2016 08:08*

**+Mark Rehorst** Yes, it'll work fine for some people, but not for others. The point of this project is it'll work for everybody.


---
**David Bassetti** *March 26, 2016 13:39*

AWESOME..!!!


---
**Jack Colletta** *March 27, 2016 16:40*

All I ran the test overnight on a SanDisk 4GB Micro HC card with no errors.  I will look around for more sd


---
**Arthur Wolf** *March 27, 2016 16:43*

**+Jack Colletta** Please post the output for your test here : [https://github.com/adamgreen/SDCard/issues/9](https://github.com/adamgreen/SDCard/issues/9)

Thanks A LOT for testing !


---
**Jack Colletta** *March 27, 2016 17:38*

I just posted it.  The only other card I  have is an Adata 8GB card and it appears to have been tested. 


---
**Arthur Wolf** *March 27, 2016 17:40*

Thanks !!!!


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/JGdVbpP3MyF) &mdash; content and formatting may not be reliable*
