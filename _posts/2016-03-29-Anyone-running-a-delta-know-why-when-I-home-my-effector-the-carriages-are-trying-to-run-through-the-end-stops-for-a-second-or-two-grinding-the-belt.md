---
layout: post
title: "Anyone running a delta know why when I home my effector, the carriages are trying to run through the end stops for a second or two grinding the belt"
date: March 29, 2016 03:55
category: "General discussion"
author: "Chris Wilson"
---
Anyone running a delta know why when I home my effector, the carriages are trying to run through the end stops for a second or two grinding the belt. I've checked the right stops are connected to the right towers (X for X, Y for Y, and Z for Z). It's driving me nuts!!  It does stop, all switches trigger, the gear just skips the belt a couple of times before stopping. 





**"Chris Wilson"**

---
---
**Douglas Pearless** *March 29, 2016 04:01*

Can you explain how you wired the end-stops.  Are they optical, mechanical, oullups, etc.


---
**Chris Wilson** *March 29, 2016 04:05*

**+Douglas Pearless** They're mechanical, wired to Common, and NC. 


---
**Douglas Pearless** *March 29, 2016 04:09*

Do you have pull-ups enabled o those pins in the config file on the SD card?  Something like 1.25^


---
**Douglas Pearless** *March 29, 2016 04:10*

so that when they go open, the LPC1768 will immediately see the input go low-to-high


---
**Chris Wilson** *March 29, 2016 04:15*

**+Douglas Pearless** 



Here is the endstop portion of my config:



endstops_enable                              true       

delta_homing                                 true       



alpha_min_endstop                            nc

alpha_max_endstop                            1.25^      

alpha_homing_direction            home_to_max

alpha_min                                    0          

alpha_max                                    0          



beta_min_endstop                             nc

beta_max_endstop                             1.27^      

beta_homing_direction             home_to_max

beta_min                                     0          

beta_max                                     0          



gamma_min_endstop                            nc

gamma_max_endstop                            1.29^      

gamma_homing_direction         home_to_max

gamma_min                                    0          


---
**Douglas Pearless** *March 29, 2016 04:18*

And what about:

alpha_limit_enable 			     true 	      # If set to true, the machine will stop an alpha endstop is hit


---
**Chris Wilson** *March 29, 2016 04:21*

I don't have that setting anywhere.  Should I?


---
**Douglas Pearless** *March 29, 2016 04:22*

Yes!!



alpha_limit_enable 			     true 	      # If set to true, the machine will stop an alpha endstop is hit

beta_limit_enable 			     true 	      # If set to true, the machine will stop an alpha endstop is hit

gamma_limit_enable 		     true 	      # If set to true, the machine will stop an alpha endstop is hit


---
**Chris Wilson** *March 29, 2016 04:24*

I'll put those in right away.  Thank you!


---
**Arthur Wolf** *March 29, 2016 09:13*

**+Douglas Pearless** Folks shouldn't be using "limit" on deltas, it's not supported.



**+Chris Wilson** Try uncommenting debounce, and dividing your homing speed by 5.


---
**Chris Wilson** *March 29, 2016 17:57*

**+Arthur Wolf** I only have a zprobe.debounce_count setting, what's the syntax for the alpha,beta,gamma debounce?


---
**Arthur Wolf** *March 29, 2016 17:58*

**+Chris Wilson**  [https://github.com/Smoothieware/Smoothieware/blob/edge/ConfigSamples/Smoothieboard/config#L251](https://github.com/Smoothieware/Smoothieware/blob/edge/ConfigSamples/Smoothieboard/config#L251)


---
**Wolfmanjm** *March 30, 2016 00:34*

actually limits are supported for deltas now, however it will not help you in this case. I suspect you have your sdcard mounted on your host and the host is trying to scan the sdcard which basically stops the smoothie from being able to control anything while that happens. The wiki says make sure sdcard is not mounted on the host before homing or probing.

Also slow down the fast homing rate significantly.




---
**Glenn Beer** *April 01, 2016 02:35*

That kind of sounds like what happens when the end stops are not connected to the correct towers. With the power off adjust the carriages one higher than the next and the next. When the machine homes, the highest one should stop when it hits the home switch. If one of the others stop, while the first grinds. The end stops are miswired no matter how carefully they were checked. Been there done that.


---
**Glenn Beer** *April 01, 2016 02:51*

Also end stops are not immune to emi. Are your end stops wired to the normally closed terminals of the switch.


---
*Imported from [Google+](https://plus.google.com/112111122069927621889/posts/3WsMpQNZYiJ) &mdash; content and formatting may not be reliable*
