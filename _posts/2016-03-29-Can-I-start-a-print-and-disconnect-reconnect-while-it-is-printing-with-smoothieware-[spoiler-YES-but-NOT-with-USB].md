---
layout: post
title: "Can I start a print and disconnect / reconnect while it is printing with smoothieware [spoiler: YES but NOT with USB]?"
date: March 29, 2016 07:11
category: "General discussion"
author: "Jeremie Francois"
---
Can I start a print and disconnect / reconnect while it is printing with smoothieware [spoiler: YES but NOT with USB]? With **+Peter van der Walt** smoothiebrainz I could get rid of this nasty ethernet cable all through my house at last. I will not print via wifi (risky/unreliable), but I would love to upload a file, start printing, disconnect and check the process. Since I remember that part of octoprint was ported, is this right?

![missing image](https://lh3.googleusercontent.com/-WMFF6nhVFJE/Vvoqm7w_GiI/AAAAAAAAK3A/TZ2qIPo1nC8DfvYQmcHYKZjpzf3pOyNxw/s0/smoothiebrainz.jpg)



**"Jeremie Francois"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 29, 2016 07:13*

Oh, you already have one. Nice. I distinctly recall this particular green board with blue ESP. I think I've commented on it before. 


---
**Jeremie Francois** *March 29, 2016 07:27*

**+Ray Kholodovsky** I wish I have one :) Nope, I just was the guy who asked to use the remaining PCB location for optional BT and ESP modules :) **+Peter van der Walt** yes, I know I already can upload, but I am unsure how it would behave if I drop the connection after starting the print.

Actually I should give it a try with cabled connection & the remote tiny web interface that looks like pronterface. I am lazy :)


---
**Jeremie Francois** *March 29, 2016 10:48*

**+Peter van der Walt** exactly what I was hoping, without actually checking it. Shame on me :)


---
**Wolfmanjm** *March 30, 2016 00:47*

to reiterate if you start a print using the play /sd/fil.g command then no connection is necessary, you can check the status with progress command from a telnet session as you need. However if you start a print from USB then it needs to remain connected (depending on how the host initiates an sdcard print).

FYI no part of Octoprint has been ported to smoothieware.


---
**Jeremie Francois** *March 30, 2016 06:54*

**+Wolfmanjm** thanks! I hate tethered USB also (I almost never ever printed something with USB since 2012 -- only debugging at some time). Now for octoprint, I was thinking about this project [https://github.com/arthurwolf/Octofab](https://github.com/arthurwolf/Octofab) (ok it is not firmware technically)


---
*Imported from [Google+](https://plus.google.com/+JeremieFrancois/posts/8fk5TXETi6L) &mdash; content and formatting may not be reliable*
