---
layout: post
title: "Arthur Wolf Lots of respect for you guys for being able to sell Smoothieboards at the prices that you do"
date: March 30, 2016 04:12
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
**+Arthur Wolf** Lots of respect for you guys for being able to sell Smoothieboards at the prices that you do.  Working on the BOM for the brainz board today, all I can say is components are EXPENSIVE and take up almost the entire margin.  I'll be lucky to break even on building these, much less get any reimbursement for my time.  





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Arthur Wolf** *March 30, 2016 08:21*

**+Ray Kholodovsky** **+Peter van der Walt** Eh, thanks for the kind words :)

I guess we could produce some Smoothiebrainz at the same factory we produce the Smoothieboards, and sell them in our shops. We have a pretty well established process, and it'd probably cost less than it does when you guys make just a few.

Would need the design to be a bit more commonly tested first, and to find a way to properly give money back to the original creator though. But nothing that can't be done.

Tell me what you think :)


---
**Ray Kholodovsky (Cohesion3D)** *March 30, 2016 15:09*

I'm building a pick n place and I live 2 hours away from Openbuilds HQ.  Definitely convenient.  As I've mentioned I have my own plans for this board - primarily shoving as many motor drivers and a few mosfets on to it for a single more compact 3d printer board, a "mini smoothie" if you will.  Obviously I want to be respectful of all the other groups involved and help any way I can.  


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/5LhLyChUhNW) &mdash; content and formatting may not be reliable*
