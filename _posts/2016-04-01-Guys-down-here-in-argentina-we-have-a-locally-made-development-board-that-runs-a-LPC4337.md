---
layout: post
title: "Guys, down here, in argentina, we have a locally made development board that runs a LPC4337"
date: April 01, 2016 14:30
category: "General discussion"
author: "Nicolas Arias"
---
Guys, down here, in argentina, we have a locally made development board that runs a LPC4337. Iv been asked if it can be used to drive machines (printers and cnc). The hardware part is not complex, the firmware is a pain in the butt. If you guys think that smoothie can be run on that micro, i will get a board and start working on it.



the development board is the CIAA-EDU (in spanish):



[http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:edu-ciaa:edu-ciaa-nxp](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:edu-ciaa:edu-ciaa-nxp)





**"Nicolas Arias"**

---
---
**Arthur Wolf** *April 01, 2016 16:14*

Hey **+Nicolas Arias**, if it has a LPC4337, it's likely [https://github.com/Smoothieware/Smoothie2](https://github.com/Smoothieware/Smoothie2) will run on it.

How much does it cost, and what is special about it ?


---
**Nicolas Arias** *April 01, 2016 16:35*

theres nothing special, its just on of the first goverment-privates-universities electronic projects.



thanks!


---
**Arthur Wolf** *April 01, 2016 16:37*

Cool :)


---
*Imported from [Google+](https://plus.google.com/+NicolasArias/posts/G1Cn4FXMqN4) &mdash; content and formatting may not be reliable*
