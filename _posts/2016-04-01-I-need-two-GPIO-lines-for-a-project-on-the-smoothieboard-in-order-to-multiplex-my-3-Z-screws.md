---
layout: post
title: "I need two GPIO lines for a project on the smoothieboard in order to multiplex my 3 Z screws"
date: April 01, 2016 12:40
category: "General discussion"
author: "Jeremie Francois"
---
I need two GPIO lines for a project on the smoothieboard in order to multiplex my 3 Z screws. I would better not use any pin which is used for displays or other existing hardware extension. Which are the most suitable?

I have no need for anything fancy here (no IRQ, no PWM, no fast switching, just plain and slow GPIOs).





**"Jeremie Francois"**

---
---
**Jeremie Francois** *April 01, 2016 12:44*

Actually a plus would be an already soldered header (just for now for laziness) ;) Was thinking about 1.30 and 1.31


---
**Jeremie Francois** *April 01, 2016 13:00*

**+Peter van der Walt** thanks for being so quick! But isn't this a bit zealous (beside safety for the board)? TTL is OK for me, so I would better keep the mosfets for actuators.


---
**Jeremie Francois** *April 01, 2016 13:12*

**+Peter van der Walt** yes and yes... Now, using pins belonging to mosfets is a pity in case I need some later on, which is why unused raw pins are better for me :p


---
**Jeremie Francois** *April 01, 2016 13:13*

... if there are any :p


---
**Jeremie Francois** *April 01, 2016 13:23*

Indeed... we're probably going to be stuck -- and we knew it --. We are looking into X + Y + (Z1+Z2+Z3) + E1 + E2 + E3 (+heaters) + Bed + 1-2 servos.

Now all Z are grouped thanks to multiplexing.

I think we will anyhow have to use a slave board in the end, because of this lack of GPIOs. Actually I probably can ditch the display, as I expect to link the SM with a Raspberry anyway (for a possibly stand-alone machine + wifi, etc: slicing must be possible on board).


---
**Arthur Wolf** *April 01, 2016 16:17*

[http://smoothieware.org/pinout](http://smoothieware.org/pinout) and [http://smoothieware.org/lpc1769-pin-usage](http://smoothieware.org/lpc1769-pin-usage)
I'd probably use the play/pause pins, nobody uses that.

I'm going to be doing exactly the same thing as you in a few weeks on a printer I'm building :)


---
**Jeremie Francois** *April 01, 2016 19:35*

**+Arthur Wolf** yep we have now two prototypes in which we have to put the Z and tool changer -- too bad I am on holidays next week :D


---
**Mert G** *April 02, 2016 00:15*

**+Jeremie Francois**​ do you have any pics or videos of your prototype. It sounds fancy.


---
**Wolfmanjm** *April 05, 2016 04:00*

The play/pause pin is now an immediate kill function, and they are used by many people!! just FYI.




---
**Jeremie Francois** *April 06, 2016 08:56*

**+Wolfmanjm** yep I saw that (thx). No biggie for now. I will sacrify the lcd, since we'll most probably use a raspberry for the slicing, user and web interaction. 


---
**Jeremie Francois** *April 06, 2016 08:56*

**+Mert G** not yet,  but I will :)


---
*Imported from [Google+](https://plus.google.com/+JeremieFrancois/posts/Q49Dg4gSspd) &mdash; content and formatting may not be reliable*
