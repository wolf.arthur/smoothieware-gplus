---
layout: post
title: "Well! Arthur! Do you know what time is it?"
date: April 08, 2016 14:33
category: "General discussion"
author: "Artem Grunichev"
---
Well! Arthur!

Do you know what time is it? Yes, it's Smoothieboard V2 [Pro] questions time!

Of course no 'when' questions! But other usual ones:



How's your progress? What help do you need? How's FPGA doing?



And my personal question: on my delta I have my move steppers on the bottom, and extruders steppers on the top and I want to wire from top to bottom only signal wires like EN, STEP, DIR, instead of powerful and noisy A, A, B, B. I have quadstepper board by sparkfun to do that. That's working well on my old printer with Azteeg X3 Pro, will it work for Smoothie V2? (probably, with some GPIO outputs and firmware edit)







**"Artem Grunichev"**

---
---
**Arthur Wolf** *April 08, 2016 14:43*

Hey.



So, we are making ( slow ) progress. It's a lot of work, so it makes sense it takes a long time.

Adam Green ( author of GCC4Mbed amongst other things ) is working on a new SD card driver for Smoothie which will be a great step-up in the v2 firmware dev.

Many folks have offered to help with firmware, I'm going to contact all of them back in a few days to try to figure out what they can do.

And work on the chainable extruder board has started too.



Wiring your en/step/dir lines over long distances is a horrible idea, and wiring AABB stepper wires over long distances is just perfectly fine. Things are essentially the exact opposite of what you seem to think they are here.



<b>If</b> you really wanted to do that, it's work as well or as badly on any controller really. Doesn't matter if it's Smoothie or Azteeg.



Cheers.


---
**Bouni** *April 08, 2016 14:56*

**+Артем Груничев** 

If you have noise issues with the stepper wires, try to use shielded cable and make sure the shield is properly grounded. Here it is important to have a big surface connected to the ground, not just a simple wire to avoid the skin effect:

[https://en.wikipedia.org/wiki/Skin_effect](https://en.wikipedia.org/wiki/Skin_effect)


---
**Artem Grunichev** *April 08, 2016 16:01*

Well, **+Bouni**, **+Arthur Wolf**, thank you for suggestions! Mainly I am trying to fit all wiring inside of extrusions, where I have only 24 wires of 22 AWG, so idea of having only 7 wires for 3 steppers (STEP and DIR for each and single shared EN) drives me. Other wires are needed to deliver 30 + 20 Amps up and down and for some sensors :)


---
**Arthur Wolf** *April 08, 2016 16:02*

If you have that much power wiring going through there, you definitely don't want any of those to be signals. That's just begging for trouble.


---
**Artem Grunichev** *April 08, 2016 16:09*

In my idea signal steppers wires + sensor wire (8 in total) goes through one extrusion, while all power lines are going through other 2 extrusions :)


---
**Arthur Wolf** *April 08, 2016 16:11*

Even then still much better to have your signal wires as short as possible.


---
**Artem Grunichev** *July 25, 2016 10:17*

Thank you Arthur!


---
*Imported from [Google+](https://plus.google.com/101033487868604831015/posts/ErC2XtVRF9p) &mdash; content and formatting may not be reliable*
