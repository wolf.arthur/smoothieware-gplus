---
layout: post
title: "Hello everybody. I am in need of some assistance with editing the firmware to operate my motors correctly"
date: April 13, 2016 19:26
category: "General discussion"
author: "Alex Hayden"
---
Hello everybody. I am in need of some assistance with editing the firmware to operate my motors correctly. They are 4.2v 1.8degree step 1.5a. They are wired correctly, but when i hit the home button or move button the motor stalls. What voltage is sent through to the motor? Is it definable? If not does that mean these motors will not work? Or will turning down the current to some other value make them work? If so how do i calculate that value?



Thanks.





**"Alex Hayden"**

---
---
**Ariel Yahni (UniKpty)** *April 13, 2016 19:46*

I think you just need to setup the 1.5A on each motor con the config but it depends if you are using a board with integrated drivers. 


---
**Alex Hayden** *April 13, 2016 19:49*

Yeah the current config file has the current per motor set at 1.5a. I am using the smoothieboard with the on board drivers. Forgot to mention that. Really tired was up most of the night trying to get this stuff to work. 


---
**Richard Marko** *April 13, 2016 19:52*

Try moving slowly, you might have your steps/mm set wrong. If you send for example G0 X10 F100 it should move your X axis by 10mm.


---
**Alex Hayden** *April 13, 2016 20:11*

I believe steps per mm will only adjust the pulses to the motor, which if they are the wrong valued pulse it still will stall. Default config is 80 steps per mm.


---
**Ariel Yahni (UniKpty)** *April 13, 2016 23:54*

I would make very sure that the stepper cables are connected in the correct order. That most certainly would make them stall. I have done it myself. If current is correct that would be my best guess


---
**Alex Hayden** *April 14, 2016 00:19*

Do you have to use all 3 wires on the end stop switches? How are they supposed to be wired? Because i think i just ruined my board. Just set xmax pin straight to ground through the switch. It set the trace on fire next to the large cap and chip next to the mosfets. I am guessing that was the 1k pull up resistor chip. Which is most likely fried. Is it possible to still use the board? Can i just redefine the min endstops to be the max endstops and thus avoid the burnt section?


---
**Ariel Yahni (UniKpty)** *April 14, 2016 00:42*

I don't have a smoothieboard but a compatible smoothieware so I can be sure but I can tell you that I have connected the end stops in all possible combinations and nothing happened. I don't think much current goes through that since it's just a mechanical sketch but could be very wrong. Maybe **+Arthur Wolf**​ can clarify 


---
**Alex Hayden** *April 14, 2016 00:49*

Its times like this when i wish these boards were made with replaceable components. Smd is great and all for low profile but not good for replacement. I hate having to buy new boards because one little chip goes poof. 


---
**Ariel Yahni (UniKpty)** *April 14, 2016 00:57*

You can surely fix it. If you know how. Even if you don't know it's a good place to start. 


---
**Alex Hayden** *April 14, 2016 01:11*

I found what i did wrong with the switch. I transposed xmax with the 5v side. That is why it fried. I thought i had moved ground to the right pin, and technically i did, but i didn't move the other pin to the correct side. Mrrrrr


---
**Ariel Yahni (UniKpty)** *April 14, 2016 01:13*

Is this the reason the motors where stalling or this happened after? 


---
**Alex Hayden** *April 14, 2016 01:48*

This happened after. I was looking over the board and found my switches were wired nc through signal to 5v. Thought that might be my problem so i dug out the wiring diagram and changed the pins in my plug. But only changed the signal to ground. My wires were made by someone else who doesn't know anything about color codes for wiring, black is always ground (which was wired to signal, i moved to ground). The other wire is yellow and was wired to 5v, i didnt move this because i didnt double check to see that it was actually in the wrong spot. Instead my brain said "black is next to yellow now it is hooked up right." But it was backwards. Now all i get is a high pitch hum from the motors. But the switches do turn it off when you trigger them so at least they are working now. The are wired to min. Before all this mess with the switches the motors did turn of about a 1\4 turn then stall, but i think they stalled because the swtich was wired signal to 5v. And firmware said signal to ground. So firmware says default signal to ground (says to add an ! To make it NO signal to ground.) and switch is wired nc signal to ground. 


---
**Alex Hayden** *April 14, 2016 01:53*

Beta_min_endstop      1.26^

Beta_max_endstop     1.27^ 



These lines define the pins right? So i can just swap these numbers to swap the pins right? 



Beta_min_endstop   1.27^

Beta_max_endstop   1.26^﻿


---
**Alex Hayden** *April 14, 2016 02:00*

Hurrah turning steps per min down allowed the motors to turn. Switches work fine on the min setting. So maybe the board is still usable. Going to try hooking up on the max side to see if it will work.


---
**Ariel Yahni (UniKpty)** *April 14, 2016 02:12*

But you had 80 spm, it's way low


---
**Alex Hayden** *April 14, 2016 02:19*

80 steps per mm, turned the speed rate down to 300mm per min. That is what did the trick. Now i should be able to change steps per mm to 2700.


---
**Alex Hayden** *April 14, 2016 02:22*

Now i just need to know why after hitting the endstop it doesn't bounce and stop, and shut off the motors. 


---
**Alex Hayden** *April 14, 2016 02:25*

Hmmm nope. Turning steps per mm back up to 2700 stalls them. I wonder what the max steps per mm are for these motors.


---
**Ariel Yahni (UniKpty)** *April 14, 2016 02:38*

Steps per meter need to be set to the correct amount of movement in real life like **+Richard Marko**​ said before. You don't set a max, there should be only one number based on your linear motion system look here [http://prusaprinters.org/calculator/](http://prusaprinters.org/calculator/)


---
**Alex Hayden** *April 14, 2016 02:50*

**+Ariel Yahni** ok. So that says for a 5/16 lead screw you need to be at 2267.72 steps per mm. But when i set that it just makes the motors wine. So something else is set wrong. Should i turn mm per min up higher then 300? Or what?


---
**Ariel Yahni (UniKpty)** *April 14, 2016 02:54*

R u building a cnc? Or this is for the Z axis on a printer? Nevertheless do as Marko said. Lower the spm to something you can get motors moving. Issue a X movement of 100mm but make sure you mark the beginning of physically so you can measure the distance once it move. That number you need to apply the following formula [http://www.aquickcnc.com/wiki/Stepper_Motor_Calculations](http://www.aquickcnc.com/wiki/Stepper_Motor_Calculations)


---
**Alex Hayden** *April 14, 2016 03:06*

This is for a z axis. I just tried changing the 300 to 3000 and then to 30 for the mm per min speed. That didn't help. Same wine. The smoothieboard is set fixed at 1/16 step right? Motors are 1.8 degree steps. I must be missing something here. I am pretty sure i had 2267.72 steps per mm setting on a printrboard that worked just fine. 


---
**Ariel Yahni (UniKpty)** *April 14, 2016 03:54*

Yes 1/16, and your number seems to be correct


---
**Alex Hayden** *April 14, 2016 04:06*

Well i am calling it a night. Need more sleep. This is still not working at that high setting. Will have to go digging if someone doesn't share some settings. I wish there was more information out there about the settings people are using. Would love to know how people get these awesome prints using a bowden style extruder. 


---
**Alex Hayden** *April 14, 2016 04:07*

**+Ariel Yahni** you have been very helpful. Thanks.


---
**Beau H** *April 14, 2016 05:07*

It sounds like your default feed speed for homing and probably max feed speed for the z axis are set to high. I had the same sort of issues when attempting to tuning the z axis of a printer for faster movement. Set the feed speed for homing/max speed to a much lower value and keep going lower until you get smooth movement that you expect.


---
**Bouni** *April 14, 2016 05:31*

**+Alex Hayden** Also try to lower your acceleration! If the board tries to accelerate the stepper to hard, this will end in stall as well!


---
**Alex Hayden** *April 14, 2016 17:20*

Ok i have tried a few different settings on the default feed rate and the max feed rate and the acceleration. Current values are 0.01 0.02 0.1 respectively. This does let the motors move. But they start moving very slowly and will gradually increase till they stall. I have the motors out of the printer for testing purposes. And i am not triggering the endstops untill they stall. There has to be an easier way to get the best setting. I am no math wiz but surely math can be used to find the best setting. 


---
**Alex Hayden** *April 14, 2016 17:35*

200 steps per revolution x 16 micro steps = 3200 microsteps per revolution. 


---
**Alex Hayden** *April 14, 2016 18:00*

According to the step calculator for a 1/16 micro step controller 2267.72 steps = 1mm of travel on a 5/16 threaded rod. Which has a pitch of 1.41111 mm per revolution. Which would be = 3200 micro steps.


---
**Beau H** *April 14, 2016 18:14*

Look for these two settings and lower them down to 1mm/s gamma_fast_homing_rate_mm_s and gamma_slow_homing_rate_mm_s - or what ever axis you are working on. 

See if that helps, if this helps you can increase the rate to a higher speed that you feel comfortable with but doesn't cause stalling. Make sure to set the slow rate much slower than fast to get an accurate home position reading. 


---
**Alex Hayden** *April 14, 2016 18:16*

**+Bouni**​**+Beau H**​**+Ariel Yahni**​**+Richard Marko**​ Doesn't max feed rate limit acceleration? Once you accelerate to the max feed rate it should stay at that speed until it hits the end stop, also in the case on movement for a line it will accelerate to the max feed rate and then decelerate just before the end of the line. That is correct right? 


---
**Alex Hayden** *April 14, 2016 18:17*

**+Beau H** I think you just answered my question. So they have a different feed rate for homing basically. They call it homing rate.


---
**Alex Hayden** *April 14, 2016 18:32*

**+Beau H** yup. That was the right thing to change to get my motors to stop running away with the acceleration till they stalled. Now just have to dial in on the right speed. Thanks.


---
**Alex Hayden** *April 14, 2016 18:35*

Boy that sure was hard to find. I think all motion control and speed setting should be grouped together along with endstops. Would make finding the issue easier.


---
*Imported from [Google+](https://plus.google.com/+AlexHayden/posts/XMsDbNDkLf7) &mdash; content and formatting may not be reliable*
