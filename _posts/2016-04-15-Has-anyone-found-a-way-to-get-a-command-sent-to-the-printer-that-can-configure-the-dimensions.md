---
layout: post
title: "Has anyone found a way to get a command sent to the printer that can configure the dimensions?"
date: April 15, 2016 16:29
category: "General discussion"
author: "Alex Hayden"
---
Has anyone found a way to get a command sent to the printer that can configure the dimensions? Would have to work something like the home command. Current methods to home use firmware defined dimensions. Couldn't we get the firmware to define the dimensions based on a homing sequence? Like this. 



Home to min endstops. 

Bounce 5mm then home to min endstops. 

Then set min endstops to zero. 

Then home to max endstops. 

Bounce 5mm then home to max endstops. 

Then set max endstops to distance traveled from zero.



The machine already counts steps per mm. I think this could work. It would be really great for use on a Delta style printer. You could then look at the EPROM to see if the dimensions are the same for all 3 towers and make adjustments to fix them easily. 





**"Alex Hayden"**

---
---
**Ariel Yahni (UniKpty)** *April 15, 2016 16:35*

Dimension is based on software endstops﻿.  What would you need it like this?  Specially wince smoothie is so easy to change the config file? 


---
**Alex Hayden** *April 15, 2016 19:20*

**+Ariel Yahni**​ Was thinking it would make editing the config file unnecessary. Easier for an end user who doesn't know anything about the configuration. Fool proofing. 


---
**Ariel Yahni (UniKpty)** *April 15, 2016 19:37*

**+Alex Hayden**​ I could agree but if you are in the point where you are building your own machine then your suppose to research that. If you buy a machine Allready assembled be sure there nothing to change ever. I was at that point once and im sure going to be there again, is a learning curve with any new topic. I'm interested in building a community resource for all of this basic stuff


---
**Alex Hayden** *April 15, 2016 19:48*

**+Ariel Yahni** yes you are right. I was planning to make some much needed youtube tutorials for smoothieboard setup and config changes. Once I learned how everything worked. I have had a rough time getting everything setup correctly. Once I do I can go back and do videos so others wont have the same problems. Like fring there board by connecting 5v to ground on the switches. Curious why the 5v pin is even there. Is it so you can use an optical switch?


---
**Ariel Yahni (UniKpty)** *April 15, 2016 19:52*

Not sure but that or servo, etc. I have a compatible smoothieware boars


---
*Imported from [Google+](https://plus.google.com/+AlexHayden/posts/YNxFD4n1qFG) &mdash; content and formatting may not be reliable*
