---
layout: post
title: "I wanted to verify some expansion pin assignments, any help would be appreciated"
date: April 15, 2016 18:20
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
I wanted to verify some expansion pin assignments, any help would be appreciated.  



The board I am working on is described here: [https://plus.google.com/u/0/+RayKholodovsky/posts/YDwDF2Lk95j](https://plus.google.com/u/0/+RayKholodovsky/posts/YDwDF2Lk95j)



I need to assign pins for the 6th stepper driver (3rd extruder) and a hobby servo as this is still a popular approach for probing.  



The pins I have available are P2.4 (psu.output_pin) and I think I will need to re purpose the 4 LED pins 1.18-1.21. 



Documentation does not specify: can a servo can be run on any IO pin, or would a PWM pin be needed/ preferable?  

Also an idea was brought up to do temperature sensing of the board in regard to monitoring the MOSFET temperature, I could easily add an SMD thermistor to the board and potentially use a temperatureswitch module to issue a kill/ pause+cooldown command if exceeding a set threshold.  

If so I can connect that to P1.30 (ADO4) and move glcd click button to one of the previously mentioned pins.



I should be all set on MOSFETs and 4 thermistors for bed + 3 hotends is all I need.



Thanks for the help!





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Arthur Wolf** *April 16, 2016 08:51*

First, two links : [http://smoothieware.org/pinout](http://smoothieware.org/pinout) and [http://smoothieware.org/lpc1769-pin-usage](http://smoothieware.org/lpc1769-pin-usage)



You want Servos tbe controlled by PWM-capable pins.




---
**Ray Kholodovsky (Cohesion3D)** *April 16, 2016 16:16*

Thank you Arthur.  I do read the documentation, you know :)  And I saw some forum threads regarding where to put the 3rd extruder driver for the diamond hotend.

So, I will reassign PWM0_PSU_OUT as the servo pin.  The 6th driver will use 3 of the LED pins.  I like this approach because I can keep the LEDs for feedback when flashing the board, and then the user can disable the LEDs in Config when they want to use the 6th stepper driver (which would not be installed until they wanted it).  

This leaves LED4 for mosfet #6, and with that and the GLCD, this board is completely maxed out.  



Thanks!


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/PNJMnDmYz5t) &mdash; content and formatting may not be reliable*
