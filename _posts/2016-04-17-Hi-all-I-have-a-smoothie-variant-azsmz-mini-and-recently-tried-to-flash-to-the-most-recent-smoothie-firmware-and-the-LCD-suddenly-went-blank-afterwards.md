---
layout: post
title: "Hi all. I have a smoothie variant (azsmz mini) and recently tried to flash to the most recent smoothie firmware and the LCD suddenly went blank afterwards"
date: April 17, 2016 17:40
category: "General discussion"
author: "Vince Lee"
---
Hi all.  I have a smoothie variant (azsmz mini) and recently tried to flash to the most recent smoothie firmware and the LCD suddenly went blank afterwards.  



I made no change to the configuration beforehand.  Is there a difference between firmware versions or manufacturer customizations that could affect this?  I've since tried countless combinations of configuration variations and the two firmware versions linked on the azsmz mini reprap wiki with no luck.  



The board still responds to commands over usb and buzzes when I press the encoder wheel but it just shows backlight with no text.  I am pretty sure the pin assignments are correct from the silkscreen text for the LCD header.  Besides, the only thing I changed was the firmware before it stopped. 



Thanks



The board I have looks like this:



[http://m.ebay.com/itm/New-Ver-2-1-AZSMZ-32bit-controller-AZSMZ-12864-LCD-4-x-DRV8825-/262109482857?nav=SEARCH](http://m.ebay.com/itm/New-Ver-2-1-AZSMZ-32bit-controller-AZSMZ-12864-LCD-4-x-DRV8825-/262109482857?nav=SEARCH)





**"Vince Lee"**

---
---
**Arthur Wolf** *April 17, 2016 17:43*

The board is not open-source, and the seller has been known to change the firmware without actually sharing the modifications, so I'm not sure how much help you can get from the community in this situation.

I strongly recommend you contact the seller as a first step. If they can't help, maybe somebody here will have an idea/


---
**Ariel Yahni (UniKpty)** *April 17, 2016 18:02*

I may be the only user of AZSMZ board here but never connected a LCD to it. Why not go back to the previous firmware to verify?


---
**René Jurack** *April 17, 2016 19:15*

I have some aswell... go back in firmware like suggested from **+Ariel Yahni** to verify. What buildversion did you update to?


---
**Vince Lee** *April 17, 2016 20:43*

**+Ariel Yahni** that's the main problem.  I don't have the original firmware.  The board came preflashed without a tf card.  If there is a way to extract firmware to the card I didn't know about it and now it is too late.  I have already contacted the seller but haven't heard back yet.


---
**Vince Lee** *April 17, 2016 20:45*

**+Ariel Yahni** if you have one of these do you have a copy of the firmware?


---
**Ariel Yahni (UniKpty)** *April 17, 2016 20:46*

**+Vince Lee**​ should be here [http://reprap.org/wiki/AZSMZ_Mini](http://reprap.org/wiki/AZSMZ_Mini)


---
**René Jurack** *April 17, 2016 20:47*

good luck contacting cxandy :D I stalked him in EVERY social media he has and emailed 17 adresses... No answer :D


---
**René Jurack** *April 17, 2016 20:49*

**+Vince Lee** What build-version did you update to?


---
**Vince Lee** *April 18, 2016 04:27*

**+Ariel Yahni** yeah I tried the two firmware versions linked there and they didn't work, but I got a direct link from the seller and I'm up and running again:

[http://www.reprap.org/mediawiki/images/7/76/AZSMZ_FW_Ver2.1.zip](http://www.reprap.org/mediawiki/images/7/76/AZSMZ_FW_Ver2.1.zip)


---
**Michaël Memeteau** *April 18, 2016 09:26*

Make sure your flat connector cable from the LCD to the main board are firmly inserted. That could be another cause (don't ask me how I know...). 


---
**René Jurack** *April 18, 2016 09:45*

**+Vince Lee** This looks like you just messed up the config file... There are 2-3 pins that are assigned differently to the originally smoothieboard. The link you posted contains a "full" config file aswell (I know, because it's mine) nearly at the bottom of the wiki. This config is with ALL possible parameters and explanations <b>AND</b> pinassignment for the AZSMZ-mini, I did put a little work into this... Because of your post here, I flashed the newest firmware (EDGE-build) to my board and it works with my config. So it has nothing to do with the firmware itself.


---
**Vince Lee** *April 19, 2016 03:59*

**+René Jurack** interesting.  I had a working lcd before flashing the new smoothie firmware, and it returned to working again after flashing the firmware I got from the manufacturer.  In neither case did I change the config file to see the effect, just the firmware. My config is based on one provided by the seller and yes it has the different pins from the standard smoothie already specified.  Also, I had previously hand checked them against the azsmz pin outs and the pins were correct.  However,, my guess is that perhaps the manufacturer or seller changed one or more default values in their version of the firmware to allow the board to run without a card, and that one or more of those changed entries is missing from their config file, I tried to track this down using the smoothie documentation and sample config files but was never able to find the relevant entry.  I suppose I could probably do so now with your full config, but I am a little burnt out dealing with this and happy it is up again.


---
**René Jurack** *April 19, 2016 08:28*

Yes, the config from the seller misses a huge amount of settings. This is probably what happened :)


---
**Lukas Müller** *February 13, 2017 20:05*

I think I have somewhat the same problem as you had ... The board worked fine for several month and just stopped to boot all of the sudden(I didnt change anything on the sd card, just switched it off and then on again). I tried reflashing the Firmware but I cant get it to flash on the Board (File just stays .bin). 

It still connects to a PC and it sends a status in the Simplify3D Machine Control Panel but I cant control it ... If I send a command it doesnt do anything :( 

I've got a second board here and it flashes and works fine off the same SD card.

Also tried several different Firmwares and configs and sd cards and formating them ...

Thanks if anyone can help




---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/cJmyzpRf4oc) &mdash; content and formatting may not be reliable*
