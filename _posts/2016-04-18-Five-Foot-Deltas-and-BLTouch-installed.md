---
layout: post
title: "Five Foot Deltas and BLTouch installed"
date: April 18, 2016 02:05
category: "General discussion"
author: "Ronald Whittington"
---
Five Foot Deltas and BLTouch installed



![missing image](https://lh3.googleusercontent.com/-OK2Bsfc-BGE/VxQ-zJA-z2I/AAAAAAAA7xI/ZVmNVS2VctgpcWA9fuUy-Rz8yM-2ClnNw/s0/BLTouchCloseup.jpg)
![missing image](https://lh3.googleusercontent.com/-90Ud6pSDCG0/VxQ_ARlAG2I/AAAAAAAA7xU/0tVyBAI7HRE28rX-OsdDB6caFsZtPXPJw/s0/BillsDelta2.jpg)
![missing image](https://lh3.googleusercontent.com/-JuksJwd3dQo/VxQ_ASpP6LI/AAAAAAAA7w8/ARVmvbmLtuQy0yjJak6sjngMpm9PJXnCw/s0/BillsDelta3.jpg)
![missing image](https://lh3.googleusercontent.com/-eEwj0rSHCEg/VxQ_AR3ZVAI/AAAAAAAA7w8/pu8JsToDDSYemuJ9S0-mFdQtmhcDyferg/s0/BLTouchMounted_and_ready.jpg)
![missing image](https://lh3.googleusercontent.com/-9wkjiwhYE8g/VxQ_AsFycUI/AAAAAAAA7w8/sIUeZxyVoWc1-AIcoD3aVSfOeau67Sp1g/s0/Delta1.jpg)

**"Ronald Whittington"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2016 02:29*

The beer is for print adhesion to the bed, right?


---
**Ronald Whittington** *April 18, 2016 02:29*

Yep😎


---
*Imported from [Google+](https://plus.google.com/+RonaldWhittington/posts/ffmhFkmFCNV) &mdash; content and formatting may not be reliable*
