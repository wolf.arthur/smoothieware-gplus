---
layout: post
title: "Good Afternoon Arthur, I am not an expert on G32 results, how whacked is this reading below ?Thanks Connecting..."
date: April 19, 2016 21:17
category: "General discussion"
author: "Ronald Whittington"
---
Good Afternoon

Arthur, I am not an expert on G32 results, how whacked is this reading below ?Thanks



Connecting...

Printer is now online.

Using tool 0.

>>> G32

SENDING:G32

Calibrating Endstops: target 0.030000mm, radius 120.000000mm

set trim to X:0.000000 Y:0.000000 Z:0.000000

initial Bed ht is 591.162476 mm

T1-0 Z:4.7375 C:379

T2-0 Z:4.8875 C:391

T3-0 Z:5.2750 C:422

set trim to X:0.000000 Y:-0.187830 Z:-0.673057

T1-1 Z:4.9625 C:397

T2-1 Z:4.9750 C:398

T3-1 Z:5.0500 C:404

set trim to X:0.000000 Y:-0.203482 Z:-0.782625

T1-2 Z:5.0000 C:400

T2-2 Z:5.0250 C:402

T3-2 Z:5.0875 C:407

set trim to X:0.000000 Y:-0.234787 Z:-0.892193

T1-3 Z:5.0375 C:403

T2-3 Z:5.0250 C:402

T3-3 Z:4.9875 C:399

set trim to X:-0.062610 Y:-0.281744 Z:-0.892193

T1-4 Z:5.0250 C:402

T2-4 Z:5.0250 C:402

T3-4 Z:4.9875 C:399

set trim to X:-0.109567 Y:-0.328702 Z:-0.892193

T1-5 Z:5.0250 C:402

T2-5 Z:5.0125 C:401

T3-5 Z:5.0375 C:403

trim set to within required parameters: delta 0.025000

Calibrating delta radius: target 0.030000, radius 120.000000

initial Bed ht is 591.212463 mm

CT Z:5.000 C:400

T1-1 Z:4.950 C:396

T2-1 Z:4.950 C:396

T3-1 Z:5.225 C:418

C-1 Z-ave:5.0417 delta: -0.042

Setting delta radius to: 204.6708

T1-2 Z:4.938 C:395

T2-2 Z:4.938 C:395

T3-2 Z:4.988 C:399

C-2 Z-ave:4.9542 delta: 0.046

Setting delta radius to: 204.7854

T1-3 Z:4.963 C:397

T2-3 Z:4.975 C:398

T3-3 Z:5.037 C:403

C-3 Z-ave:4.9917 delta: 0.008

Calibration complete, save settings with M500





**"Ronald Whittington"**

---
---
**Arthur Wolf** *April 19, 2016 21:22*

I think that looks reasonably ok. What kind of probe are you using ?

I'm not really the one to tell you if those are good results or not : tests should tell you that.



Cheers.


---
**Ronald Whittington** *April 19, 2016 21:23*

ah ok, sorry my old age is getting to me...BLTouch on Smoothie 5x, I will read a bit more, thanks again


---
*Imported from [Google+](https://plus.google.com/+RonaldWhittington/posts/jJniM5QQVNP) &mdash; content and formatting may not be reliable*
