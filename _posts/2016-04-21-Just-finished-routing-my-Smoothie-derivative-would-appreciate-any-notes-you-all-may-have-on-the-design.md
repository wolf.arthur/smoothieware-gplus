---
layout: post
title: "Just finished routing my Smoothie derivative, would appreciate any notes you all may have on the design"
date: April 21, 2016 02:21
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Just finished routing my Smoothie derivative, would appreciate any notes you all may have on the design.  Please see the attached post for details and the link to the Github repo with the Eagle files.



<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



Just finished routing my Cohesion3D ReMix (Powered by Smoothie) board. Man this is tight.  Down to 8 mil traces with 6 mil clearance in between them.  I still have redo the last 4 stepper drivers per the new routing pattern you see in the first 2, and spend an inordinate amount of time seeing if I can squeeze in a servo header for a leveling probe, but other than that, it's time for some community review!  Please take a look, the eagle files are in my github, and let me know what you think:  [https://github.com/raykholo/Smoothiebrainz/](https://github.com/raykholo/Smoothiebrainz/)

I also added some new features:  there is an onboard thermistor near the MOSFETs to monitor if they get too hot (keep in mind these things will burn you long before they exceed the temperature they are rated for) and a number of responses can be scripted.  Remind you of anything?  High end PC motherboards perhaps?  

There's also footprints for flyback diodes so you can quickly install these if you need to run electromechanical devices (like relays or the pumps and solenoids you'd find on a Pick n Place) without having to get "creative" with your diode installation.   

Finally, I did manage to squeeze in that 6th MOSFET and this one is selectable between your motor voltage and 5v, so you can run a larger variety of small accessories from it. 

I'd keep going, but with 6 stepper drivers and MOSFETs along with WiFi in a 100mm square board, there's not even space for air in between the components :)

![missing image](https://lh3.googleusercontent.com/-hNv4VRVapKo/Vxg2AlwUb0I/AAAAAAAAVPc/4jOocIoEekIUr6QsJwkTOBhXzSBkkjIAw/s0/ReMix%252BSnapshot%252B-%252B5%252B%252528rev0.8%252529%252B.png)



**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/fksQXNCnKER) &mdash; content and formatting may not be reliable*
