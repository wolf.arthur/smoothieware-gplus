---
layout: post
title: "Originally shared by ANTCLABS ANTCLABS BLTouch : Auto Bed Leveling Sensor for 3D Printers  Smoothieboard (Smoothieware) + BLTouch  Controlling BLTouch on Smoothieboard is little different with other boards"
date: April 21, 2016 07:46
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by ANTCLABS</b>



ANTCLABS BLTouch : Auto Bed Leveling Sensor for 3D Printers

▣ Smoothieboard (Smoothieware) + BLTouch



※ Controlling BLTouch on Smoothieboard is little different with other boards.

Make sure you are using the latest version.

We tested Smoothieboard on the version edge-804489c, which is built on April 19th of 2016.

This may be unusable for former versions.



We had a great time with Smoothieboard, such a gorgeous board.

![missing image](https://lh3.googleusercontent.com/-MNp720WJKEo/Vxho7_b_WQI/AAAAAAAABeM/JC3RBbJT-c8Fz-jQ6uLhFBJ6Bg1pePeRQ/s0/Smoothieboard_BLTouch123.jpg)



**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/QKPqaidc3Ph) &mdash; content and formatting may not be reliable*
