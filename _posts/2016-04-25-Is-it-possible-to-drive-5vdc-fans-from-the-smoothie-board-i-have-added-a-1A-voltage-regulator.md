---
layout: post
title: "Is it possible to drive 5vdc fans from the smoothie board, i have added a 1A voltage regulator?"
date: April 25, 2016 21:40
category: "General discussion"
author: "Andrew Wade"
---
Is it possible to drive 5vdc fans from the smoothie board, i have added a 1A voltage regulator? I would rather not use a separate supply, can i use the supply on the board? It would be control two 5vdc fans at 0.36W each. I also have a glcd display powered by the board





**"Andrew Wade"**

---
---
**Arthur Wolf** *April 25, 2016 21:41*

If you feed 5V to your mosfets ( for example taken from one of the endstop connectors ), they'll output 5V when turned on. Make sure you protect your mosfet when plugging a fan into it ( see documentation ).


---
**Vince Lee** *April 26, 2016 03:57*

Any reason you can't use alternate fans?  I got some small 24v fans from eBay for $3 and drive one of them off the main supply for cheap air assist


---
**René Jurack** *April 26, 2016 07:06*

DCDC-Stepdown converter for 0,50€ should do it.


---
*Imported from [Google+](https://plus.google.com/112929738014057369475/posts/AwVvHKoR67x) &mdash; content and formatting may not be reliable*
