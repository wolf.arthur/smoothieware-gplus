---
layout: post
title: "Originally shared by Sbastien Mischler (skarab) Smoothie-Happy Update - Almost all commands implemented"
date: April 26, 2016 11:25
category: "General discussion"
author: "S\u00e9bastien Mischler (skarab)"
---
<b>Originally shared by Sébastien Mischler (skarab)</b>



<b>Smoothie-Happy</b> Update



- Almost all  #smoothieboard  commands implemented.

- This morning: first automatic update of the firmware.



+ Documentation : [http://lautr3k.github.io/Smoothie-Happy/docs/smoothie-happy/0.0.1-alpha/](http://lautr3k.github.io/Smoothie-Happy/docs/smoothie-happy/0.0.1-alpha/)



Source : [https://github.com/lautr3k/Smoothie-Happy](https://github.com/lautr3k/Smoothie-Happy)



![missing image](https://lh3.googleusercontent.com/-6MOljqF_aL8/Vx8ipXXK_rI/AAAAAAAAMOw/Ds_oR6HVpvYvgyagpA4hR1-y2oO-HiXwA/s0/Smoothie-docs.png)
![missing image](https://lh3.googleusercontent.com/-Jcw95ebYoH8/Vx8ipXSTVII/AAAAAAAAMOw/CdKmjjvdIpYP6zDP6HmeXAjd7dfFA5Dyg/s0/Smoothie-update.png)

**"S\u00e9bastien Mischler (skarab)"**

---
---
**Alex Hayden** *April 26, 2016 12:25*

Wont this change your config file and thus have to go through and renter all the values you changed? 


---
**Sébastien Mischler (skarab)** *April 26, 2016 13:06*

Not at the moment, but I will add a function to upgrad the config file.


---
*Imported from [Google+](https://plus.google.com/+SébastienMischler-Skarab/posts/XtxQQpGsdzz) &mdash; content and formatting may not be reliable*
