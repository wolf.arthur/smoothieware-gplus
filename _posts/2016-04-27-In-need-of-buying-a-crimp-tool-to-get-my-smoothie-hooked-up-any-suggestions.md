---
layout: post
title: "In need of buying a crimp tool to get my smoothie hooked up any suggestions?"
date: April 27, 2016 16:12
category: "General discussion"
author: "Alex Krause"
---
In need of buying a crimp tool to get my smoothie hooked up any suggestions?





**"Alex Krause"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 27, 2016 16:29*

For the included crimp terminals? I prefer small needle nose pliers. 


---
**Alex Krause** *April 27, 2016 16:38*

Yah Ray... I know they make a tool for the task as well just wanted to make sure I have the best connection possible 


---
**Ariel Yahni (UniKpty)** *April 27, 2016 16:41*

I usually look at Amazon customer rating and reviews, specially for tools


---
**Ray Kholodovsky (Cohesion3D)** *April 27, 2016 16:44*

For 2.54mm duponts (slightly different than the included smoothie connectors) I bought a crimper on the lower end of the price spectrum and never really liked it. Stuck with my trusty pliers. 


---
**Triffid Hunter** *April 27, 2016 17:06*

There's a $17 one from hobbyking that's adequate. Doing it with pliers <b>properly</b> is very difficult, doing it badly is easy - avoid that road if possible


---
**Beau H** *April 27, 2016 18:46*

This is the one I use at home [http://www.amazon.com/gp/aw/d/B007JLN93S/ref=mp_s_a_1_2?qid=1461782238&sr=8-2&pi=SY200_QL40&keywords=Pin+crimper&dpPl=1&dpID=41mYV1ZrFYL&ref=plSrch](http://www.amazon.com/gp/aw/d/B007JLN93S/ref=mp_s_a_1_2?qid=1461782238&sr=8-2&pi=SY200_QL40&keywords=Pin+crimper&dpPl=1&dpID=41mYV1ZrFYL&ref=plSrch)

There are a bunch of other options on Amazon if you search for printing crimper. The IWISS crimpers are supposed to be good quality as well. 

Another option would be to look for an molex brand crimper on ebay, they are expensive but they have a nice spring loaded hold down for the pin to hold it in place while you insert your wire. I used a one at my last job all the time and it was the best tool ever, but you would expect that for 500 dollars new. 


---
**Jarrid Kerns** *April 28, 2016 00:31*

**+Beau H** same one I use. Haven't had any issues. 


---
**Stephanie A** *April 28, 2016 01:55*

I have these, and they are fantastic:    [http://www.engineer.jp/en/products/pad11_13e.html](http://www.engineer.jp/en/products/pad11_13e.html)


---
**Alex Krause** *April 28, 2016 02:43*

Would this tool work I like to support members within the open hardware community

[http://www.smw3d.com/crimp-tool-for-dupont-connectors/](http://www.smw3d.com/crimp-tool-for-dupont-connectors/) 


---
**Beau H** *April 28, 2016 03:30*

**+Alex Krause**​ they look like the right type but I haven't used them so I am unable to comment on the quality of crimp you would get with them.


---
**Alex Krause** *April 28, 2016 03:41*

**+Brandon Satterfield**​ would the crimp tool you sell work?


---
**Brandon Satterfield** *April 28, 2016 15:36*

Yes sir. It's the one I use. 

Make a note on check out, " B said add some practice connectors in" they aren't the same as the smoothie connectors but don't think Arthur sends extras. We throw some in to practice on. 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/FYVJQPLgK56) &mdash; content and formatting may not be reliable*
