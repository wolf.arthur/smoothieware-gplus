---
layout: post
title: "Are the thermistors on smoothieboard read by an AI (analog input) and supplied with 3.3V?"
date: May 03, 2016 18:26
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
Are the thermistors on smoothieboard read by an AI (analog input) and supplied with  3.3V?





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *May 03, 2016 18:57*

Hey.

Smoothieboard's pins are read by an ADC ( Analog to Digital Converter ), which is one of the peripherals of the microcontroller.

The thermistor inputs have two connections : one to AGND ( a Ground specially for analog inputs ), and one to the actual ADC pin on the LPC1769.



Why do you need to know ?



Cheers


---
**Ray Kholodovsky (Cohesion3D)** *May 03, 2016 19:16*

Subbed. Curious about application as well. 


---
**René Jurack** *May 03, 2016 19:57*

I try to combine some wires / voltage rails to reduce the amount of individual wires running to the hotend. While I am able to combine the 24V rail and just seperate GNDs for heater, fan, etc I was wondering what can be about the thermistor. I already read the schematic and just wanted to make sure. 


---
**Arthur Wolf** *May 03, 2016 20:06*

You can have several thermistors sharing the same AGND if you really need to. But you can't have AGND and GND mixed up, that'll mess up your temperature reading.


---
**René Jurack** *May 03, 2016 20:20*

ok. Thx for the input **+Arthur Wolf**


---
**Ray Kholodovsky (Cohesion3D)** *May 03, 2016 20:23*

Yes.  From someone who has been doing the electrical layout of these things for the last month, KEEP YOUR AGND SEPARATE!


---
**Jonathon Thrumble** *March 26, 2018 18:20*

My agnd dosent seem to be connected to anything?? It won't read the thermistor temp unless I've got it connected to common ground?? 


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/hTpjbnZPuPC) &mdash; content and formatting may not be reliable*
