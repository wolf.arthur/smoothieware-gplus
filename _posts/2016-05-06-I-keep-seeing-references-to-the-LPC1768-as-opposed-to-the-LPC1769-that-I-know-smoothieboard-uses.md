---
layout: post
title: "I keep seeing references to the LPC1768 as opposed to the LPC1769 that I know smoothieboard uses"
date: May 06, 2016 17:18
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
I keep seeing references to the LPC1768 as opposed to the LPC1769 that I know smoothieboard uses. What's the difference?  Digikey lists identical specs for both ICs.  Will the stock firmware work on the '68?





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Arthur Wolf** *May 06, 2016 17:19*

100Mhz vs 120Mhz is the only difference. the 68 is older and the first boards used it. newer boards use the 69. the firmware can run on both.


---
**Ray Kholodovsky (Cohesion3D)** *May 06, 2016 17:20*

Was there any noticeable improvement at the higher clock rate?  

Also now that you mention it I do notice a subtle 0 changing to a 2 :)


---
**Arthur Wolf** *May 06, 2016 17:22*

Not sure there was much of a noticeable change.


---
**Ray Kholodovsky (Cohesion3D)** *May 06, 2016 17:23*

Noted.  Thanks.


---
**Ray Tice** *May 13, 2016 01:29*

I had smoothieware running on a LPC1768-Mini-DK2 at one point. Smoothie expected LEDs at specific ports, which happened to be the mini-DK2 microSD card, so wasn't a 100% stock Smoothieware image. But it was a very minor tweak.


---
**Ray Kholodovsky (Cohesion3D)** *May 13, 2016 02:02*

Gotcha. I found a good source for genuine LPC1769's so sticking with that. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/d9WF4hnmLye) &mdash; content and formatting may not be reliable*
