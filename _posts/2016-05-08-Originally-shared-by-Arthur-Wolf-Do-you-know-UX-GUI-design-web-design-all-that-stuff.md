---
layout: post
title: "Originally shared by Arthur Wolf Do you know UX, GUI design, web design, all that stuff ?"
date: May 08, 2016 09:05
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



Do you know UX, GUI design, web design, all that stuff ?

If you've ever wanted to help with Smoothie stuff, we have dire need of your craft, so now's the time !



We are creating a new web interface for Smoothie. Its a huge, very ambitious projet. It's intended to make use of the machine much simpler, and to have a ton of features. It'd run on your computer, like classical hosts, or on a tablet fixed to the machine, that'd replace the current LCD screens everybody uses.



You can find the project here : [https://github.com/arthurwolf/fabrica](https://github.com/arthurwolf/fabrica)

There is even a demo here : [http://arthurwolf.github.io/fabrica/](http://arthurwolf.github.io/fabrica/) ( requires an active Smoothieboard on your network ).



We already have folks helping with coding, and documentation writing. But just because you know how to code a web interface doesn't mean you know how to make it pleasant to the user, or nice looking.



There are folks around who have learned how to make interfaces great, and if you are one of those persons, we definitely want your help.

We need help re-designing the currently existing ( few ) screens we have so they are nicer, as well as designing all the new screens we'll need in the future.



We also need icons and graphics if that's your thing.



If you are interested in helping, please contact me at wolf.arthur@gmail.com so we can chat.



Cheers !



Bellow, an example of a screen I've done. As you can see, lots of room for improvement. Please help !

!![images/1d55ebfeb5eaffcc92bb8253ed4d4274.png](images/1d55ebfeb5eaffcc92bb8253ed4d4274.png)



**"Arthur Wolf"**

---
---
**Arthur Wolf** *May 08, 2016 20:26*


{% include youtubePlayer.html id="XeQumaNShpM" %}
[https://youtu.be/XeQumaNShpM](https://youtu.be/XeQumaNShpM)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/i6YtsvrBEXh) &mdash; content and formatting may not be reliable*
