---
layout: post
title: "Shared on May 08, 2016 20:27...\n"
date: May 08, 2016 20:27
category: "General discussion"
author: "Arthur Wolf"
---

{% include youtubePlayer.html id="XeQumaNShpM" %}
[https://youtu.be/XeQumaNShpM](https://youtu.be/XeQumaNShpM)





**"Arthur Wolf"**

---
---
**Steve Anken** *May 09, 2016 00:30*

LOL! 
{% include youtubePlayer.html id="XeQumaNShpM" %}
[1:20](https://youtu.be/XeQumaNShpM?t=1m20s)


---
**Mahan Bastani** *May 09, 2016 03:43*

Idea: Isn't it better to have a general setting that lets you choose the type of the machine you are using (ex: laser cutter/3d printer/delta) and based on the selection hide the settings that are not needed for that selection? Az an example if it's a 3d printer, there should be no need to set laser setting?


---
**Arthur Wolf** *May 09, 2016 06:14*

**+Mahan Bastani** You will have that. This is just the "basic" raw config file edition tool.


---
**Mahan Bastani** *May 09, 2016 06:17*

okay, thanks Arthur! By the way how is the development of Ver2 going? When can we get boards? I was interested in working on the FPGA.


---
**Arthur Wolf** *May 09, 2016 06:19*

**+Mahan Bastani** Making progress too, but can't know when anything will happen.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/BdLocUNt5H2) &mdash; content and formatting may not be reliable*
