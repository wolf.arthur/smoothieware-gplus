---
layout: post
title: "Is there a way to get different versions of smoothieware somewhere?"
date: May 11, 2016 17:49
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
Is there a way to get different versions of smoothieware somewhere? Somewhere between Jan 2016 (worked at that time) and today there was an update of the edge-tree that broke functionality of my AZSMZ-mini Display. Updated today to the latest firmware and the display isn't working anylonger... Want to go back to the 'jan 2016' version...





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *May 11, 2016 17:50*

Sure, there is a full history of the .bin files on github : [https://github.com/Smoothieware/Smoothieware/commits/edge/FirmwareBin/firmware.bin](https://github.com/Smoothieware/Smoothieware/commits/edge/FirmwareBin/firmware.bin)




---
**Triffid Hunter** *May 11, 2016 18:00*

If you want to help us isolate the problem, see the building smoothie guides and do a git bisect


---
**René Jurack** *May 11, 2016 18:07*

**+Arthur Wolf** Thank you, I will try them and report back.

**+Triffid Hunter** I will try, but github is totally confusing for me. Probably because I am no native english speaker...


---
**René Jurack** *May 11, 2016 19:29*

what parameter is used for 'panel.spi_frequency' if I don't put it in the config? There is somehow the problem related. I didn't had it in before and put it in while updating. 


---
**Arthur Wolf** *May 11, 2016 19:30*

Default value is 1000000


---
**René Jurack** *May 11, 2016 19:34*

Thank you, now everything works, even with the newest firmware. Sorry for blaming the firmware-update in the beginning ;)


---
**Arthur Wolf** *May 11, 2016 19:34*

:)


---
**Vince Lee** *May 11, 2016 21:56*

I had the same problem with my azsmz mini when I upgraded.  I contacted the seller and he gave me this link which restored the display. 

[http://www.reprap.org/mediawiki/images/7/76/AZSMZ_FW_Ver2.1.zip](http://www.reprap.org/mediawiki/images/7/76/AZSMZ_FW_Ver2.1.zip)






---
**Arthur Wolf** *May 11, 2016 21:58*

**+Vince Lee** Yeah providing pre-compiled versions of modified open-source firmware without providing the modified source is just the definition of pure-evil.

They did that already a year back when they implemented servo control but refused to share their changes.

Those guys are just the complete opposite of what the open-source 3D printing community stands for, I just don't get why people keep on supporting them.

Do they have to actually start killing puppies before people start getting upset ?


---
**René Jurack** *May 12, 2016 04:40*

**+Vince Lee** It is not the firmware, it is the rather shitty and incomlete config-file from the seller that causes the problem.

**+Arthur Wolf** I didn't knew that they are some 'bad' guys, before. But I also see no alternative board with similar size...


---
**Arthur Wolf** *May 12, 2016 06:10*

**+René Jurack** I believe the v3 of the azteeg x5 board will have removable drivers, also **+Ray Kholodovsky** is working on something like that ( two boards actually, both of which could fit ).


---
**Ray Kholodovsky (Cohesion3D)** *May 12, 2016 06:17*

Yupp, I have a maxed or board with 6 drivers and a mini with 4 drivers,  both are swappable. See my profile for more information, and more in the coming weeks. 


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/ARoTF25wvXd) &mdash; content and formatting may not be reliable*
