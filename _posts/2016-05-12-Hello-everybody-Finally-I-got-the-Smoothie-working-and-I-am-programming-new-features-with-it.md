---
layout: post
title: "Hello everybody!! Finally I got the Smoothie working and I am programming new features with it"
date: May 12, 2016 07:33
category: "General discussion"
author: "David Roncal"
---
Hello everybody!!



Finally I got the Smoothie working and I am programming new features with it. One of them is a thermistor with a timeout, just to set a temperature for a while. 



For this, I copied the struct done in the code of TemperatureControl.cpp about the "set and wait mcode". Well, it did not work at first, and I supposed it was because of my code. However, If I use the code from git (smoothieware-edge, the last update) and I try the set and wait mcode as it is said in the documentation, it also crashes. 



This is what I see, I open a cutecom and I send the command (M109 S40) and it outputs an "ok" and the temperatures (T: 22.3 /25 @31, for example) and it appears several times. The temperature can not change, because I am not connecting anything in the pin (just a led to see if it flashes). After several times of this output, it stops and the leds 1,2 and 4 stay on (not flashing). 



When I use the "set temperature" m code it works okey, it does not stop. 



It also happens that when I am using the pid autotune It stops without finishing... 



I hope I could explain my problem. I was wondering, do you know what is going on?



Thank you very much,





**"David Roncal"**

---
---
**Arthur Wolf** *May 12, 2016 07:49*

It works for absolutely everybody else, so look for anything in your setup that is different from the others. Connecting an actual heater would probably be a good start.


---
**David Roncal** *May 12, 2016 10:49*

**+Arthur Wolf** Well, I already tried with actual heater and good thermistor, however, it was always crashing. Now I found that if I disable the second Uart it works, if I enable, it does not. Could this have sense? Thankyou


---
**Arthur Wolf** *May 12, 2016 11:27*

second uart, or second usb/cdc ?


---
**David Roncal** *May 12, 2016 12:51*

this one in config: 

"second_usb_serial_enable                     false            # This enables a second usb serial port (to have both pronterface

                                                              # and a terminal connected)"


---
**David Roncal** *May 12, 2016 12:51*

so you are right, second usb


---
**Arthur Wolf** *May 12, 2016 12:52*

Do you really nned that ? it's more of an experimental feature.


---
**David Roncal** *May 12, 2016 12:53*

mmm.... well, I am using one uart for debugging and the other to send commands... maybe I am doing anything wrong?


---
**Arthur Wolf** *May 12, 2016 12:54*

Debugging how ?


---
**David Roncal** *May 12, 2016 12:55*

eclipse.


---
**Arthur Wolf** *May 12, 2016 12:56*

No I mean what kind of debugging ??


---
**David Roncal** *May 12, 2016 12:59*

What kind? I don't get it, sorry for my English. I am trying to test some code I am writing at the same time Smoothie is running...


---
**Arthur Wolf** *May 12, 2016 13:00*

So you added code to smoothie that sends text via the serial port ?


---
**David Roncal** *May 12, 2016 14:03*

no, no, I added code to Smoothie to add a temperature control with a timer (to stand for 15 minutes at 200 degrees and then stop it). I want to check it, so I use eclipse to run the program in one uart and with the usb I use the pc with cutecom and send the command I "invented" for this task...


---
**Arthur Wolf** *May 12, 2016 14:04*

then it's likely just a problem with your code.


---
**David Roncal** *May 12, 2016 14:13*

Mmmm...well, I hardly think that because I disable all of my code and this problem happened when I used the generic temperature_control from the last git update....


---
**Arthur Wolf** *May 12, 2016 14:16*

Maybe you can try using MRI to see what's happening ?


---
**David Roncal** *May 12, 2016 14:20*

I already tried, but it seems like the communications on the port got blocked.


---
**Arthur Wolf** *May 12, 2016 14:25*

MRI is not over USB


---
**David Roncal** *May 12, 2016 14:27*

Yeah it goes over uart. It does not work... I dont receive the mri sequence through it i meant


---
**Arthur Wolf** *May 12, 2016 14:28*

Using an unmodified firmware ? What board are you using ?


---
**David Roncal** *May 13, 2016 07:06*

board? the 5xc smoothieboard. Yes, I cloned the git repository one month ago and I compiled, when I set true the second usb it does not work. If I set to false, it works.


---
**Arthur Wolf** *May 13, 2016 07:41*

Can you try with the .bin from github ( not compiled by you ) ?


---
**David Roncal** *May 13, 2016 08:13*

sure! but i can not download... it says there is an error on the website...


---
**Arthur Wolf** *May 13, 2016 08:17*

This page explains the different places you can get it at : [http://smoothieware.org/getting-smoothie](http://smoothieware.org/getting-smoothie)

Try getting it from github


---
**David Roncal** *May 13, 2016 10:51*

Hello, I tested it. It does not work neither... I put the downloaded firmware.bin and the config from there and I only changed the second usb from false to true, and then the autotune for example fails...


---
**Arthur Wolf** *May 13, 2016 11:11*

**+David Roncal** Then this is possibly a smoothie bug, you can open an issue on github. In the meantime, you'll have to only use one serial port.


---
**Wolfmanjm** *May 28, 2016 02:21*

I use the second USB serial all the time so it is highly unlikely a smoothie bug.  I suspect that your config file is huge with obsolete entries and you are running out of memory during the boot/config. Try editing your config and deleting all lines you do not actually need in it.




---
**David Roncal** *May 30, 2016 12:54*

**+Wolfmanjm**

well... it may be, what are obsolete entries? I have a problem with my flash when I try to add 6 temperature controls... the smoothie does not start flashing leds... it can be a flash problem?


---
**Wolfmanjm** *May 30, 2016 18:48*

no your config file is too big. delete all lines in your config that are not used.


---
**David Roncal** *May 30, 2016 19:10*

Even if they are commented?


---
**Wolfmanjm** *May 30, 2016 21:37*

every line would need a comment # at the start. but it is better to delete them


---
*Imported from [Google+](https://plus.google.com/115847461863457551025/posts/i9xGELWGv9a) &mdash; content and formatting may not be reliable*
