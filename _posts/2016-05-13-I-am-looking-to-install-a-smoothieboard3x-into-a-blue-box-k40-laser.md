---
layout: post
title: "I am looking to install a smoothieboard3x into a blue box k40 laser"
date: May 13, 2016 00:54
category: "General discussion"
author: "kyle clement"
---
I am looking to install a smoothieboard3x into a blue box k40 laser.  Do I have to use the rheostat or can I bypass it and adjust the laser power output using the software or firmware for the smoothieboard?





**"kyle clement"**

---
---
**Arthur Wolf** *May 13, 2016 07:48*

Hey.



Yes, you can ( and should ) be using Smoothieboard's PWM to control the laser power. It's required so that power is proportional to speed when accelerating. If you don't do that, you'll get dirty cuts.

See [http://smoothieware.org/laser-cutter-guide](http://smoothieware.org/laser-cutter-guide)



Cheers.


---
*Imported from [Google+](https://plus.google.com/106211725666880587072/posts/f7vMEfALi31) &mdash; content and formatting may not be reliable*
