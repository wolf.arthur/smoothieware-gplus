---
layout: post
title: "Do you know PCB design ? The Smoothie project needs your help !"
date: May 15, 2016 10:31
category: "General discussion"
author: "Arthur Wolf"
---
Do you know PCB design ? The Smoothie project needs your help !

Don't know PCB design but still want to help ? Please re-share this to your PCB-designing friends :)



The Smoothie project is working hard on the next iteration of it's hardware ( [http://smoothieware.org/blog:13](http://smoothieware.org/blog:13) ).

Some major changes include going to a Cortex-M4 chip, better stepper drivers, and playing with FPGAs. 

We are also working on a system that'd allow you to add infinite extruders to your board ( [https://is.gd/peaDqc](https://is.gd/peaDqc) )



We've got the PCB design needs covered on all that so far.



However, we also plan on another addition to the system : a comprehensive set of extension boards.



Need to add servos, a thermocouple, a raspi zero, Wifi to your board ?

We want to have a comprehensive set of easy to use, plug-and-play boards that people can wire to their Smoothieboard to do pretty much anything they want.



And we need people to help us do the PCB design on those boards.



We have written a spec for a first set of boards : [https://is.gd/720xlx](https://is.gd/720xlx) ( please don't hesitate to give us ideas for more boards ).



If you like PCB design, and want to help the Smoothie project move forward, please don't hesitate to contact me at wolf.arthur@gmail.com so we can get you started.



Thanks for reading, thanks for helping ! Please reshare :)



![missing image](https://lh3.googleusercontent.com/-3X9X-WBMAPQ/VzhQHI2_0-I/AAAAAAAAMYY/pfCnVb2Di9IJAdsMMvhgCZyPIk2L-NqeQ/s0/hall-effect-sensor-board.png)



**"Arthur Wolf"**

---
---
**bob cousins** *May 18, 2016 18:06*

Cool idea! Thought I would have a go at designing some extension boards. I can't find gadgeteer.lib, should this be in the repo somewhere?


---
**Arthur Wolf** *May 18, 2016 18:52*

**+bob cousins** What's gadgeteer.lib ? Is this related to the template thing that was contributed ?


---
**bob cousins** *May 18, 2016 19:12*

Yes, I created a project from the template, and on opening the schematic kicad says "libraries not found: Gadgeteer.lib".  Not a big deal for what I am doing, but I guess there may be some  good stuff in there?

Got one board down already!


---
**Arthur Wolf** *May 18, 2016 19:17*

**+bob cousins** Hey, thanks a lot for working on boards, that's a huge help !

However, one thing : you need to tell us in advance what boards you intend on working on, so we can put that in the document, so others don't work on the same thing at the same time. It's a matter of not wasting anybody's time.

Can you tell me what board you did, and what other(s) you plan on working on ?



Cheers :)


---
**bob cousins** *May 18, 2016 19:31*

Sure, I am starting at the top and working down :) So raw pins breakout done, next is screw terminal breakout.


---
**Arthur Wolf** *May 18, 2016 19:34*

**+bob cousins** awesome ! Added you to those two boards, please ping me anytime you want to start working on a new one, just so I can add you to the document :) Thanks again for helping :)


---
**bob cousins** *May 25, 2016 00:00*

No problem. I have submitted a PR for screw terminal breakout, next I would like to work on LED breakout and mechanical endstop.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/LvWriVkkfhT) &mdash; content and formatting may not be reliable*
