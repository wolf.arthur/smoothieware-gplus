---
layout: post
title: "Anybody know how I would interface a TFT touchscreen to a smoothieboard?"
date: May 17, 2016 09:45
category: "General discussion"
author: "Anthony Bolgar"
---
Anybody know how I would interface a TFT touchscreen to a smoothieboard?





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *May 17, 2016 12:59*

Saw the post, do not know what code he has altered to allow connecting via UART to smoothieware/board


---
**Anthony Bolgar** *May 17, 2016 13:08*

Cool. I know I can hook up a small TFT touchscreen to my MKS SBASE board, but I am getting old now, and want a large screen to make it easier to read. Something around 7" would be perfect.


---
**Arthur Wolf** *May 17, 2016 16:59*

You could get a raspi and a tft screen for a raspi, and run octoprint or laserweb on it.


---
**Arthur Wolf** *May 17, 2016 17:06*

:)


---
**Chris Brent** *May 24, 2016 02:33*

I used the serial debugging pins for their UART. I need to set it up somewhere else so that I can run a debugger though. As Arthur said you could also run a raspi and Octoprint (although I think you need the Octopanel port as there's not enough overhead left on a Pi, but that might not matter with Pi 3)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/G9w3143oozz) &mdash; content and formatting may not be reliable*
