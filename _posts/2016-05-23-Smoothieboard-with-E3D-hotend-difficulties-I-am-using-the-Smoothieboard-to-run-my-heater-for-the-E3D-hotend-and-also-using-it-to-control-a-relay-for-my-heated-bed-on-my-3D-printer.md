---
layout: post
title: "Smoothieboard with E3D hotend difficulties: I am using the Smoothieboard to run my heater for the E3D hotend, and also using it to control a relay for my heated bed on my 3D printer"
date: May 23, 2016 19:34
category: "General discussion"
author: "Bruce Lunde"
---
Smoothieboard with E3D hotend difficulties:  I am using the Smoothieboard to run my heater for the E3D hotend, and also using it to control a relay for my heated bed  on my 3D printer.  I should say I just hooked all this up, and have encountered two problems, looking for advice on what to check for.  [I used the 3d Printer-guide to configure as an FYI.]   I have a 12 volt heater for the hotend, with a 30A power supply. When I start up the hotend,  it never gots hotter that 198 C, but I have ABS so have set the target to 230 C.  It does not matter how long I let it heat up.  It is connected to P1_23 big mosfet. Should I change this out for a 24v heater?  



I am using T0 for the hotend thermistor, and T1 for the heated Bed.



The heated bed is a 110 volt version, so I have connected to a relay per the section on Solid State Relays in the guide.  but I never see the relay go on. It is wired just as the guide shows, to pin 1.30, and  made the necessary changes to the configuration file. I am not sure how to go about testing this  safely?





**"Bruce Lunde"**

---
---
**Arthur Wolf** *May 23, 2016 19:35*

Did you do PID tuning ?


---
**Bruce Lunde** *May 23, 2016 19:38*

I have not done that step, I will attempt that this evening.


---
**Douglas Pearless** *May 24, 2016 01:10*

Also, what thermistors are you using and are they correctly set in the CONFIG file?


---
**Daniel Jones** *May 24, 2016 02:08*

Check the green connectors on your smoothie board.  If they feel warm, solder the wires to the board directly.   I haven't seen the diagram,  but I would think that a mosfet is necessary to turn the ssr on.


---
**Bruce Lunde** *May 24, 2016 03:03*

Thanks for the ideas, I will go through these items and check them all out.


---
**Triffid Hunter** *May 24, 2016 05:15*

Many hotends are only designed for 25-30W, and will struggle to maintain temperature if you have a fan blowing on them.



See how high you can crank the output voltage on your power supply; I use 15v to squeeze 45W from my nozzle heater and it never has any problems at all maintaining over 250°C with a fan blowing straight at it.



Also ensure that you have the correct thermistor selected, if you choose the wrong one it may read 198°C when it's actually closer to 300!



I use an SSR as well and it works fine. Ensure that you've connected it correctly! The input is polarised, - must go to ground.



It's possible that smoothie's 3v signal isn't enough to turn it on, if that's the case hook -in to a small mosfet and +in to the +5v from one of the endstop inputs.


---
**Bruce Lunde** *August 10, 2016 13:44*

**+Triffid Hunter** Can you tell me a bit more on the way you configured the heated bed with the SSR?  I am a bit confused as to how I activate and monitor the AC based bed I have installed.  I setup a switch entry in the config file to turn it off and on based on the GPIO pin 1.30, and setup the thermisistor properties , but I am not sure how this gets activated to tun it off and on via the code, since the default points to the heated bed properties. 


---
**Triffid Hunter** *August 11, 2016 05:00*

Just hook the SSR to one of the mosfet outputs (small mosfet is more than adequate), and set the PWM (actually smoothie uses ΣΔ) base frequency to your line frequency, either 50 or 60Hz depending on your country, shouldn't need to do anything special


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/8FrfQGN9XJX) &mdash; content and formatting may not be reliable*
