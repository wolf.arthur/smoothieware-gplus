---
layout: post
title: "Smoothieware rocks. Much better results after switching from Marlin to Smoothiware"
date: May 24, 2016 22:05
category: "General discussion"
author: "David Richards (djrm)"
---
Smoothieware rocks.



Much better results after switching from Marlin to Smoothiware.

Well worth the upgrade, thanks to everyone responsible.


**Video content missing for image https://lh3.googleusercontent.com/-82FaToRDV7o/V0TQGdX9EdI/AAAAAAAB094/ZJnLahoqWs8oBJnwJJtjzHZ3kksPecnWg/s0/VID_20160524_222207733.mp4.gif**
![missing image](https://lh3.googleusercontent.com/-82FaToRDV7o/V0TQGdX9EdI/AAAAAAAB094/ZJnLahoqWs8oBJnwJJtjzHZ3kksPecnWg/s0/VID_20160524_222207733.mp4.gif)



**"David Richards (djrm)"**

---
---
**Øystein Krog** *May 25, 2016 17:01*

It looks really good :)

What did you upgrade from?

How is it better?


---
**David Richards (djrm)** *May 25, 2016 20:30*

The improvements are to both the speed and quality of objects made.



Until recently my printer used a megatronics controller card, this is an arduino / ramps like all in one board running Marlin software. Its processor is underpowered to run the delta calculations at the speed the printer is capable of. 



Problems in the finish including little bumps which may have been caused by the carriage slowing briefly. This problem has now vanished with the new controller.



I could never get the inductive probe to give consistant results before, this was partly caused by the slightly warped 2mm aluminium bed, this has been replaced by a 6mm bed together with a 500w / 230v  heater which warms up faster than the extruder instead of having to wait ages for it to reach temperature.



Hth David.


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/GpwJRcFKV3p) &mdash; content and formatting may not be reliable*
