---
layout: post
title: "Hello, I am working with Smoothieware and I am developing a software to upload files to it through USB (not using the files explorer)"
date: May 25, 2016 09:49
category: "General discussion"
author: "David Roncal"
---
Hello,



I am working with Smoothieware and I am developing a software to upload files to it through USB (not using the files explorer). I know I have to use M28 and the name, and finish with M29. When the file is small, it all works right, but if file is longer than 3KB, Smoothie crashes (serial port crashes too and it says on the error: "device reports readiness to read but returned no data".

Any Idea about what is happening?

Thank you very much,





**"David Roncal"**

---
---
**Arthur Wolf** *May 25, 2016 09:50*

Are you waiting for smoothie to answer "ok" before sending a new line ?


---
**Triffid Hunter** *May 25, 2016 10:04*

**+Arthur Wolf**

That shouldn't be necessary, smoothie's USB stack implements USB flow control, ie smoothie will tell the host to stop sending for a while when the buffer is full, and then tell the host it can send again when the buffer drains a bit.



If it is now necessary to wait for OK over USB, something has been broken since I wrote the USB stack.


---
**David Roncal** *May 25, 2016 10:09*

**+Arthur Wolf**

Yes, I am waiting to receive the ok from Smoothie...


---
**Arthur Wolf** *May 25, 2016 10:10*

Then I have no idea


---
**David Roncal** *May 25, 2016 10:38*

Ok I found it. Just I streamed another data and it got confused. Sorry and thank you


---
**Wolfmanjm** *May 26, 2016 23:21*

**+Triffid Hunter** You do however have to consume the returned Oks otherwise it will block.


---
*Imported from [Google+](https://plus.google.com/115847461863457551025/posts/5TpzVn87bT7) &mdash; content and formatting may not be reliable*
