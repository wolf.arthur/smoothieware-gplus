---
layout: post
title: "I'm developing an image-to-gcode parser, for laser engravers"
date: May 25, 2016 01:43
category: "General discussion"
author: "Shachar Weis"
---


I'm developing an image-to-gcode parser, for laser engravers. I've run into a problem. 



The software will translate each pixel into a tiny gcode movement (while changing the laser power). The problem is that due to acceleration limits, the head never reaches the desired speed. Even though it's moving in a straight line, the look-ahead fails to recognize this, and I get a very slow and sometimes jerky movement.



If I set the acceleration value really high, the printer will miss steps at the borders of the image, where it actually needs to change direction.



I'm using an Azteeg X5 running Smoothieware.



Does anyone have an idea how to solve this, or maybe a workaround?﻿

Is there a way to manually specify acceleration in a G1 move?





**"Shachar Weis"**

---
---
**Shachar Weis** *May 25, 2016 04:18*

I'm not. I tried LaserWeb2, it doesn't work. It occasionally crashes my browser, the controls are very lacking (and it doesn't remember them), and ultimately, the gcode generated has the same acceleration problems.


---
**Triffid Hunter** *May 25, 2016 05:11*

Consolidate moves into longer ones. Do line and arc fitting to subsets of your points then extend the line or arc through any subsequent points that are close enough.



I find that specifying moves of less than about 0.5mm in gcode can cause the issues with look-ahead that you're experiencing - smoothie's CPU only has so much memory, it can't do infinite lookahead..


---
**Arthur Wolf** *May 25, 2016 07:37*

**+Shachar Weis** The fact you never get to full speed for very small segments is simply due to the fact we have limited RAM on the chip. Nothing can be done about that.

You can do what **+Triffid Hunter** is suggesting and consolidate moves. You can also use M204 to set acceleration whenever you need to : [http://smoothieware.org/supported-g-codes](http://smoothieware.org/supported-g-codes)



Cheers.


---
**bob cousins** *May 25, 2016 08:41*

G-code was designed for milling machines etc, so is not ideal for this type of use. For burning images pixel by pixel, what is really needed is a raster scan mode


---
**Shachar Weis** *May 25, 2016 12:28*

**+Peter van der Walt** perhaps something is wrong with my setup, but I tried to engrave a square using LaserWeb2 and it was very dark on the vertical lines and very light on the horizontal lines, due to acceleration issues.


---
*Imported from [Google+](https://plus.google.com/117479393665221551027/posts/DbED34rLurs) &mdash; content and formatting may not be reliable*
