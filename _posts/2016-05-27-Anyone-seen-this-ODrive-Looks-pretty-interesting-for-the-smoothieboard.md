---
layout: post
title: "Anyone seen this: ODrive? Looks pretty interesting for the smoothieboard"
date: May 27, 2016 16:48
category: "General discussion"
author: "Dani Epstein"
---
Anyone seen this: ODrive?



[https://hackaday.io/project/11583-odrive-high-performance-motor-control](https://hackaday.io/project/11583-odrive-high-performance-motor-control)



Looks pretty interesting for the smoothieboard. The developer says it would be possible to get the ODrive to work with the smoothieboard, but it would require some fiddling.





**"Dani Epstein"**

---
---
**Arthur Wolf** *May 27, 2016 16:49*

Yep, it's nothing special really, the hard part is going to do the firmware right, and on pro servo controllers, that's insanely complex. Not the first project to attempt it, and I don't know of a single one that succeeded.


---
**Dani Epstein** *May 27, 2016 16:59*

What a shame. It looks kind of pretty, and the idea of getting so much power and precision out of relatively cheap motors makes it so tempting. Well, you never know. Maybe this guy is insane enough to pull it off.


---
**Arthur Wolf** *May 27, 2016 17:00*

Yep, wait and see :)


---
*Imported from [Google+](https://plus.google.com/+DaniEpsteinCGI/posts/6Bibezvugas) &mdash; content and formatting may not be reliable*
