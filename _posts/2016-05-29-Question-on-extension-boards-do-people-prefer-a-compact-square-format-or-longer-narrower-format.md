---
layout: post
title: "Question on extension boards, do people prefer a compact square format or longer, narrower format?"
date: May 29, 2016 12:20
category: "General discussion"
author: "bob cousins"
---
Question on extension boards, do people prefer a compact square format or longer, narrower format?

It seems more convenient to have connectors on one side, even if less efficient in terms of board area.



![missing image](https://lh3.googleusercontent.com/-D6ROtLTGi7o/V0rdX4MxrFI/AAAAAAAAAKw/p9gu2QBVIe8faxOA8csoiuUt1vvFo5ZFg/s0/ExtraEndstops.jpg)
![missing image](https://lh3.googleusercontent.com/-pezl9D6diIw/V0rdaC4-sOI/AAAAAAAAAKw/ZKt5bVywlB0-iUYZjci_tpJtGII60f6jw/s0/ExtraEndstops.jpg)

**"bob cousins"**

---
---
**Arthur Wolf** *May 29, 2016 12:21*

I like the long one.


---
**Thomas Sanladerer** *May 29, 2016 14:03*

Also, non-square boards give you the option which way you want to orient the board. Plus what **+Peter van der Walt**​​ said. ﻿


---
**Marc Pentenrieder** *May 29, 2016 14:41*

I also prefer the long version


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2016 15:14*

The long one makes more sense. But both look good. 


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2016 15:19*

Surface mount gadgeteer port? Yikes. Someone will give that a pull and take half the copper of the board with it. 

Also I'd like to suggest you use the reverse biased diode that Peter came up with. This way you don't have to worry about pull up voltage and could take any input voltage on the logic. Either of us can provide you with the circuit. 

I would still have a selector though for the V+ (5v for mech endstop, 12v or higher for inductive)


---
**bob cousins** *May 29, 2016 16:06*

Ok, looks like a consensus. :)

I don't think there is any risk of the connector pulling off, the weak point of the gadgeteer system is the cable.


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2016 16:10*

I like it a lot too. I put it on all my boards now. My goal is to make that a standard across the board (haha no pun intended). 

**+Miguel Sánchez** awesome work on that. 


---
**bob cousins** *May 29, 2016 16:12*

I suggest you publish the design somewhere.


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2016 16:20*

Peter's G+ link is the one.  The only thing is that the endstop connector has different pin order.  I have V+, Gnd, Logic.  I use the 1N4148WS signal diode which is the surface mount same size as a 0603 and works great.


---
**bob cousins** *May 29, 2016 16:20*

If we are worried about heavy handed users, then we shouldn't use the Gadgeteer port. the cable has no strain relief and will break long before the connector does. Samtec do the ESHF series which would be more robust.

The SMT connector is the one in the Gadgeteer spec, I've never heard of any problems with the connector, only the cables.


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2016 16:21*

**+Arthur Wolf** this concerns you ^^


---
**Arthur Wolf** *May 29, 2016 16:33*

It's likely we'll have boards made with both types of connectors. We'll collect experience/feedback, and go with what's best after a first batch.

If something breaks, we'll go with what breaks the least. If nothing does, we'll go with what's cheapest ( PTH adds a lot of cost to a board at factory, if it can be all SMT it's a huge gain )


---
**René Jurack** *May 29, 2016 17:26*

The long one. But shorter! Make an effort to cramp things closer together. E.g. using other connectors or at least rotating them 90°... And there are connectors like HX or ordinary Pins which are much more compact.


---
**Douglas Pearless** *May 29, 2016 19:53*

The long wide purely to make it easier and cleaner for wiring.  The size saving is minor compared to the overall need.


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2016 20:02*

Once you start doing full panels the "aspect ratio" of the board doesn't matter so much anymore :)


---
**bob cousins** *May 29, 2016 22:01*

Thanks for the feedback everyone, I will try to incorporate it where possible.


---
**Jeff DeMaagd** *May 30, 2016 03:35*

I like René's suggestion of rotating the connectors so the board isn't as long, and it's still workable otherwise.


---
**Jeremie Francois** *May 30, 2016 19:28*

idem long one. .. and through-hole connectors as much as possible :)


---
*Imported from [Google+](https://plus.google.com/101860134916884222481/posts/guYZ7sn5tQY) &mdash; content and formatting may not be reliable*
