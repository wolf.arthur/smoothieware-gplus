---
layout: post
title: "Hey everyone, does Smoothieware support G7 raster gcode?"
date: June 01, 2016 20:24
category: "General discussion"
author: "Shachar Weis"
---
Hey everyone, does Smoothieware support G7 raster gcode? Is there any plans for supporting it?





**"Shachar Weis"**

---
---
**Arthur Wolf** *June 01, 2016 20:27*

G7 is already taken in the Gcode standard, for something that has nothing to do with laser, so when we implement raster, it won't be using that Gcode.

There are several folks working on native raster support in Smoothie, but nothing functional that I know of right now.

Though as you can just raster using Gcode and it works fine, most folks use that anyway.


---
*Imported from [Google+](https://plus.google.com/117479393665221551027/posts/bovNAnU5Tm2) &mdash; content and formatting may not be reliable*
