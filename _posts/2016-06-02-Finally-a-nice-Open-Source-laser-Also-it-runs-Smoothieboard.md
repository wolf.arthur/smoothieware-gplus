---
layout: post
title: "Finally a nice Open-Source laser. Also it runs Smoothieboard"
date: June 02, 2016 08:15
category: "General discussion"
author: "Arthur Wolf"
---
Finally a nice Open-Source laser. Also it runs Smoothieboard. And they support Laserweb's work.

If you need a laser, go get one there. If you don't re-share please !



<b>Originally shared by Bonne Wilce</b>



FabCreator Lasers now live on kickstarter!



[https://www.kickstarter.com/projects/fabcreator/fabcreator-lasers](https://www.kickstarter.com/projects/fabcreator/fabcreator-lasers)





**"Arthur Wolf"**

---
---
**Shachar Weis** *June 02, 2016 12:39*

Interesting project, too bad it's unlikely they will get funded.


---
**Arthur Wolf** *June 02, 2016 12:41*

**+Shachar Weis** 20k in a few hours ... it's a good start.


---
**Shachar Weis** *June 02, 2016 12:44*

Oh, I thought I'ts been a few days. Yeah, that's not bad


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/1vRUXJv37av) &mdash; content and formatting may not be reliable*
