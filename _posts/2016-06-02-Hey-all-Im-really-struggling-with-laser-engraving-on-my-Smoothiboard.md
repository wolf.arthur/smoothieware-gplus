---
layout: post
title: "Hey all, I'm really struggling with laser engraving on my Smoothiboard"
date: June 02, 2016 12:36
category: "General discussion"
author: "Shachar Weis"
---
Hey all, I'm really struggling with laser engraving on my Smoothiboard. Everyone says it should work fine but it doesn't. I am taking an image and translating it into lots of tiny G0 movements, with M106 SXX thrown in there to change laser power. I'm seeing two problems:



1. There seems to be a top speed which I can't surpass. I increase the feedrate but it doesn't go any faster. It will go faster if I perform a single big movement, but lots of tiny movements and it slows way down.



2. Even though there are no speed changes at all in the gcode - there is just a single "F" in the entire file - I can see (and hear) the head slow down when the laser is turned on. I'm thinking the M106 command introduces a tiny delay?



I think #1 is acceleration problem and I have no idea what's going with #2. Any ideas? My accl value is 10000 and jerk is set to 0.1



Thanks.





**"Shachar Weis"**

---
---
**Shachar Weis** *June 02, 2016 12:40*

Thanks, I'll try that. Do I need to configure the board to be in "laser" mode or something?


---
**Arthur Wolf** *June 02, 2016 12:42*

[http://smoothieware.org/laser-cutter-guide](http://smoothieware.org/laser-cutter-guide)


---
**Shachar Weis** *June 02, 2016 13:32*

I followed the guide and setup a laser module. If I set the PWM to 20 microseconds, the laser power drops to zero at S0.76 and at S0.77 it's still quite bright. If I increase the PWM it starts to flicker. Any ideas? It was working perfectly when I was using the fan module and M106 commands.


---
**Shachar Weis** *June 02, 2016 16:00*

PSA: When engraving use the SD card. I was maxing out the USB with tiny gcode movements and my engraver was stuttering randomly. Printing from the SD card works great.


---
**Shachar Weis** *June 02, 2016 16:04*

Is there a host that streamlines SD printing? Right now I have to do it all manually and it sucks. Pronterface tries to upload to the SD card via the serial connection and it's super slow.


---
**Arthur Wolf** *June 02, 2016 20:46*

**+Peter van der Walt** Ping me a few days before the 23rd so I know I can put aside a few days to be available when you start.


---
**Arthur Wolf** *June 20, 2016 21:59*

**+Shachar Weis** Do you have everything working now ?


---
**Shachar Weis** *June 21, 2016 00:14*

Pretty much, yeah. Thanks.


---
*Imported from [Google+](https://plus.google.com/117479393665221551027/posts/J9ShrwekZxU) &mdash; content and formatting may not be reliable*
