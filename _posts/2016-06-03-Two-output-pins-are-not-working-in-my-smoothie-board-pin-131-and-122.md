---
layout: post
title: "Two output pins are not working in my smoothie board pin 1.31 and 1.22"
date: June 03, 2016 12:39
category: "General discussion"
author: "Athul S Nair"
---
Two output pins are not working in my smoothie board pin 1.31 and 1.22. I was using it to make TL active low. 1.31 was connected via a logic level converter to TL. It was working fine but today when I checked it it wasn't. So i changed pin to 1.22 and checked it(1.22!^ 3.3V with out any command) and connected directly to TL. but no laser produced so I checked again But this time there was no voltage.

Any idea what could've caused the problem?



Now I'm using 1.23 through LLC and it's working (for now)





**"Athul S Nair"**

---
---
**Athul S Nair** *June 03, 2016 13:55*

**+Peter van der Walt** 

I only changed the resistor in the PWM channel and it doesn't have any problem. I didn't modify any channel. If 5V is applied to a smoothie pin will it get damaged?


---
**Athul S Nair** *June 03, 2016 14:07*

**+Peter van der Walt** TXB0104 [https://www.sparkfun.com/products/11771](https://www.sparkfun.com/products/11771)



this won't have any voltage division problem , right? like my current MOSFET LLC


---
**Athul S Nair** *June 04, 2016 06:19*

**+Peter van der Walt** Can I directly connect PWM and laser fire PIN to smoothie(without level      shifter ). 

Would that cause any other problem other than reduction in laser power?


---
*Imported from [Google+](https://plus.google.com/114978037514571097180/posts/jaTKpF2XWtQ) &mdash; content and formatting may not be reliable*
