---
layout: post
title: "Any news on Parallelshield for Smoothieboard?"
date: June 05, 2016 15:01
category: "General discussion"
author: "Matt Wils"
---
Any news on Parallelshield for Smoothieboard?





**"Matt Wils"**

---
---
**Arthur Wolf** *June 05, 2016 15:02*

No news. It's in a pause. we have something similar planned for v2, I think that's what will happen instead of the v1 version.


---
**Matt Wils** *June 05, 2016 15:14*

I'll keep watch for v2. 


---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2016 17:36*

Explain the use case please 


---
**Arthur Wolf** *June 05, 2016 17:43*

**+Ray Kholodovsky** You have a machine that you normally control using a computer/parralel port/linux-cnc/mach3, and want to replace the computer with a smoothieboard. Then you just have to plug the parralel cable into the smoothieboard's parralel port shield.


---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2016 17:46*

Oh that. **+Peter van der Walt** has been talking about that type of thing. We might do something along those lines. Eventually. 


---
*Imported from [Google+](https://plus.google.com/112060605079047605964/posts/HWHZvRTYdzK) &mdash; content and formatting may not be reliable*
