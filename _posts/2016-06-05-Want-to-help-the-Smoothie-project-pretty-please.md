---
layout: post
title: "Want to help the Smoothie project ( pretty please !"
date: June 05, 2016 16:24
category: "General discussion"
author: "Arthur Wolf"
---
Want to <b>help the Smoothie project</b> ( pretty please ! ), but don't want to code or design PCBs ?

We've got just the thing for you !



We have this project of creating a very complete <b>series of video tutorials</b>, covering most things about using Smoothie.

We'd write complete synopses for every video ( about 5 minutes each ), and then get an <b>awesome youtuber</b> like +Thomas Sanladerer to shoot those with all of his professionalism and charisma.



The videos would be very beginner-friendly, and would be a friendlier/funner alternative to the written documentation, which can be frightening to newcomers.



You can see the <b>current status</b> of this project here : [https://is.gd/81MJG5](https://is.gd/81MJG5)



We are looking for folks who have already used Smoothie, and are ready to do some writting on these topics. You don't need to be Shakespeare, you don't need to know everything. Everything you go through proof-reading, improvement by the community etc. But even just writing a page or two helps move the project forward very much.



If you are interrested, <b>please contact me at wolf.arthur@gmail.com</b>



We are trying to find some funding to finance this project. That's mostly intended for the person who will shoot the videos, but if somebody does significant work on the synopses, and thinks some money would help them in some way, that's something we can look into.



If you are a company in this community interested in these videos existing, you can also sponsor this work, that'd be very welcome !



Please re-share this, it'll help getting more folks to help, which will help getting the videos done sooner.





**"Arthur Wolf"**

---
---
**bob cousins** *June 08, 2016 17:55*

Hey Arthur, are you still interested in extension boards? I don't get responses to my emails, and you did ask for people to reserve boards before working on them. If you are too busy to update the document, maybe someone else could?


---
**Arthur Wolf** *June 08, 2016 21:53*

**+bob cousins** It's crazy around here, have a lot of email backlog. I'll get to the boards soon, and I immensely appreciate your work on the extension boards. I'll give you access right to the document too.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/P483VpgBX18) &mdash; content and formatting may not be reliable*
