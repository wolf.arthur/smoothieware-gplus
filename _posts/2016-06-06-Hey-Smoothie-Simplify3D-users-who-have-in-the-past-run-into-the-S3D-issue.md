---
layout: post
title: "Hey. Smoothie/Simplify3D users, who have in the past run into the S3D issue"
date: June 06, 2016 18:42
category: "General discussion"
author: "Arthur Wolf"
---
Hey.



Smoothie/Simplify3D users, who have in the past run into the S3D issue.

Would you mind updating S3D, and tell us if it's better now ?



[https://www.simplify3d.com/software/release-notes/](https://www.simplify3d.com/software/release-notes/)



S3D FINALLY did something about it ( I hope ).



Cheers.





**"Arthur Wolf"**

---
---
**Ariel Yahni (UniKpty)** *June 06, 2016 18:57*

Good to know


---
**Ariel Yahni (UniKpty)** *June 06, 2016 19:08*

I'm smoothying ( if thats the correct term)  my life lately. Don't ever want to deal with something else 


---
**Arthur Wolf** *June 06, 2016 19:10*

**+Ariel Yahni** \o/

I use "smoothiefying", but I'm not sure it's correct. "Smoothing" is for "making smooth", but I don't know the word for "making smoothie".


---
**Ondřej Píštěk** *June 06, 2016 19:18*

Ouch. Check for update >  "Your software is up to date." Tell me about it. Thanks for this post. I will test it.


---
**René Jurack** *June 06, 2016 19:34*

Same as **+Ondřej Píštěk** here. Running on 3.0.2 with no update available...


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2016 19:39*

I agree with Arthur. You are smoothiefying, while the board is smoothying (doing its own thing), and it is also smoothing your motion. 


---
**bob cousins** *June 06, 2016 21:31*

"Smoothiefy your life"

I like it :)


---
**Ariel Yahni (UniKpty)** *June 09, 2016 20:07*

Just got email from S3D with 3.1 update 


---
**Ondřej Píštěk** *June 09, 2016 20:40*

**+Ariel Yahni** me too, probably like everyone. But I found just right now, that both versions are instaled in computer after update. So I was using old one and didn't notice :-D Realy weird way how to make update.


---
**Jared Roy** *June 15, 2016 15:03*

Please excuse me from my own ignorance but I am looking at moving to smoothie board and currently use S3d, can someone point me to the issue you guys are talking about?



And also what are these mentioned gcode scrubbers used for?



Thanks 


---
**Arthur Wolf** *June 15, 2016 15:04*

**+Jared Roy** [http://smoothieware.org/simplify3d](http://smoothieware.org/simplify3d)


---
**Jared Roy** *June 15, 2016 15:16*

**+Arthur Wolf** That was fast. Thank you!


---
**Leon Grossman** *June 18, 2016 01:14*

The number of removed lines has been dramatically reduced but not completely eliminated. I'm about to run my first test with a complicated part through the machine. I'll let you know how it goes.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/WovbWWYNnqr) &mdash; content and formatting may not be reliable*
