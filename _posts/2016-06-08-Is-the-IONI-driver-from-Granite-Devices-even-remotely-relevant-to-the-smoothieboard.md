---
layout: post
title: "Is the IONI driver from Granite Devices - - even remotely relevant to the smoothieboard?"
date: June 08, 2016 19:44
category: "General discussion"
author: "Dani Epstein"
---
Is the IONI driver from Granite Devices - [https://granitedevices.com/miniature-servo-drive-ioni](https://granitedevices.com/miniature-servo-drive-ioni) - even remotely relevant to the smoothieboard? I am such a noob I have no idea, hence the question. 







**"Dani Epstein"**

---
---
**ThantiK** *June 08, 2016 20:00*

Not even remotely.  Unless you had the know how to create an adapter board for it.


---
**Dani Epstein** *June 08, 2016 20:03*

Well there you have it. I have all the know-how for creating an adaptor board for my ironing board, mainly to hold the iron on the other side as well. That probably won't translate to the smoothieboard. I'm just guessing, of course.


---
**ThantiK** *June 08, 2016 20:13*

It has a step/direction interface; it COULD work with the smoothieboard for sure.  It would just need adapter hardware.  The smoothie breaks out the pins for this, you'd just have to do some custom work.  It's certainly not a plug and play thing; no external board is.


---
**Jeff DeMaagd** *June 08, 2016 21:40*

I can't tell for sure yet, but the IONICUBE seems to act as a backplane for 4 drivers, and it looks like it can take the S/D signals that way. And it's 150UKP for the backplane and at least 150UKP (and up!) for each driver board, then there's the motors and encoders. There's ways to make servos work with smoothieboard but there's no tutorial for that yet. It's not a newbie project.


---
**Dani Epstein** *June 08, 2016 21:50*

Oh it's a bit pricey, alright. Just ocurred to me that for bigger printers it might be a solution, or perhaps for greater speed and accuracy.


---
**Jeff DeMaagd** *June 08, 2016 22:20*

That system does look pretty attractive in many ways. There's a simpler, more self-contained system I hope to hook up and install soon.


---
**Bouni** *June 09, 2016 05:29*

**+Dani Epstein** Its actually very easy to drive servo motors using the smoothieboard! As long as the servodriver has a pulse/direction interface. 

I've tested that with an industrial servo driver a while ago: 
{% include youtubePlayer.html id="zM838mYFjcA" %}
[https://www.youtube.com/watch?v=zM838mYFjcA](https://www.youtube.com/watch?v=zM838mYFjcA)


---
**Dani Epstein** *June 09, 2016 07:09*

Oh well, things are beginning to look interesting now. Jeff, you are making me quite curious. Bouni, very interesting blog (most of it way over my head) but clearly all this sort of thing looks quite plausible after all. I feel marginally less stupid.


---
*Imported from [Google+](https://plus.google.com/+DaniEpsteinCGI/posts/fQFJDMi3ZP7) &mdash; content and formatting may not be reliable*
