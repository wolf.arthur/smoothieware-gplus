---
layout: post
title: "Does smoothieware keep track of the total time of actual operation?"
date: June 09, 2016 00:14
category: "General discussion"
author: "Ariel Yahni (UniKpty)"
---
Does smoothieware keep track of the total time of actual operation? 





**"Ariel Yahni (UniKpty)"**

---
---
**Arthur Wolf** *June 09, 2016 06:37*

what do you mean exactly by operation ?


---
**Ariel Yahni (UniKpty)** *June 09, 2016 11:36*

Well how much time it has executed a job.  For example in 3d printing it would count how many hours of print


---
**Arthur Wolf** *June 09, 2016 11:37*

**+Ariel Yahni** «progress» command


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/cFCa13H6edb) &mdash; content and formatting may not be reliable*
