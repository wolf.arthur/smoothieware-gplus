---
layout: post
title: "I am sure someone is going chastise for my lousy research and then point me right to the answer but I searched everywhere and could not find..."
date: June 11, 2016 21:35
category: "General discussion"
author: "Don Kleinschnitz Jr."
---
I am sure someone is going chastise for my lousy research and then point me right to the answer but I searched everywhere and could not find...

What is the connector manufacture & PN for the "green" 4 pin DC power connector on the Smoothie's VBB.

I suppose I can use 2x of the two pins types that come in the kit but Id like to not have the chance of plugging them in wrong.





**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *June 11, 2016 21:38*

Is it this [https://goo.gl/YsL1XK](https://goo.gl/YsL1XK)


---
**Arthur Wolf** *June 11, 2016 21:42*

[https://docs.google.com/spreadsheets/d/1bT7xw4z405cyhR7MyXegm4-C2oinxpVefuf3KV13v_A/edit#gid=0](https://docs.google.com/spreadsheets/d/1bT7xw4z405cyhR7MyXegm4-C2oinxpVefuf3KV13v_A/edit#gid=0)




---
**Don Kleinschnitz Jr.** *June 11, 2016 21:49*

Holy Crap .... very nice. 

Many thanks....


---
**Don Kleinschnitz Jr.** *June 11, 2016 22:55*

**+Arthur Wolf** I wonder (tongue in cheek) if there is an error in the spreadsheet.



The given part no. for VBB power input on row 23 is:20020006-G021B01LF which resolves to a 2 position female connector and it is the same part # as listed for the "big MOSFETS" row 27.



Is it supposed to be OSTTJ047150 or  20020006-G041B01LF which I hope is a 4 position version of the same thing. 



Or did you intend to use 2x two position, if so the qty is listed a only one.



Coincidentally my Smoothie came with enough 5mm connectors to populate the board +1, should that +1 have been a 4 position by any chance :)?


---
**Johan Jakobsson** *June 12, 2016 08:27*

**+Don Kleinschnitz**  afain 20020006-G021B01LF is the connector that is used to connect to both the VBB and the Big mosfets. The male part differs though, as the mosfets are angled at 90 degrees.


---
**Arthur Wolf** *June 12, 2016 08:35*

**+Don Kleinschnitz** Yes, you are supposed to use 2x2. And we don't provide one of those ( the 5V one ) by default.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/NSkfHKkPwKs) &mdash; content and formatting may not be reliable*
