---
layout: post
title: "I got some nice Lug style thermistors, got them connected up, and roughly reading right using B Value, and R0"
date: June 15, 2016 13:07
category: "General discussion"
author: "Glenn West"
---
I got some nice Lug style thermistors, got them connected up, and roughly reading right using B Value, and R0. The datasheet says it can do .001 of a degree in accuracy with steinhart/hart, but the datasheet is rather fun on the polynomial, and then on top of that getting it into smoothie compatible values. Anyone have some mojo on how to do this.



Im using this one:

[http://www.analogtechnologies.com/document/ATH10KL2C.pdf](http://www.analogtechnologies.com/document/ATH10KL2C.pdf)





**"Glenn West"**

---
---
**Zane Baird** *June 15, 2016 13:39*

I used the calculator here: [http://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm](http://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm)



SInce we know the resistance at 25C (R@25=10000) and are given the values for R@25/R@50 and R@25/R@85 in the data sheet:



R@50= 10000/2.771 = 3608.8

R@85= 10000/9.271 = 1078.6



Plugging these into the calculator at the above link give A, B and C values of:



A = 0.001110398075

B = 0.0002370571727

C = 0.00000007710205145



To configure smoothieware for this, first comment out any temperature control definitions you are currently using (beta value, and R0) and replace with the following (on a single line):



temperature_control.hotend.coefficients 0.001110398075,0.0002370571727,0.00000007710205145



That is assuming you are using a single hotend with the default settings.


---
**Glenn West** *June 15, 2016 14:04*

My hero thanks man 


---
*Imported from [Google+](https://plus.google.com/+GlennWest/posts/De2PcTPuoq3) &mdash; content and formatting may not be reliable*
