---
layout: post
title: "Please help us test this : It changes Smoothie to do acceleration every step instead of 1000 times per second"
date: June 17, 2016 22:13
category: "General discussion"
author: "Arthur Wolf"
---
Please help us test this : [https://github.com/Smoothieware/Smoothieware/pull/946](https://github.com/Smoothieware/Smoothieware/pull/946)



It changes Smoothie to do acceleration every step instead of 1000 times per second.

It should solve quite a few problems, result in better step generation, and is a first step towards adding things like S-curve, 6-axis, etc.



Please test !







**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/S1X4bSLqrn5) &mdash; content and formatting may not be reliable*
