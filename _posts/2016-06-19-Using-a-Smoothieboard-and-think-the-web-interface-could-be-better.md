---
layout: post
title: "Using a Smoothieboard and think the web interface could be better ?"
date: June 19, 2016 15:29
category: "General discussion"
author: "Arthur Wolf"
---
Using a Smoothieboard and think the web interface could be better ? We do too !



You can now upgrade your interface by installing files on your SD card.



Please follow the instructions here : [http://smoothieware.org/install-web-interface](http://smoothieware.org/install-web-interface)



If you need any help, don't hesitate to ask.





**"Arthur Wolf"**

---
---
**Daniel Benoit** *June 19, 2016 19:45*

Arthur, when I try to open the zip file it says the file is invalid and won't open? This will allow me to connect my printer via the Ethernet cable right? Can I still use Pronterface or slicer 3D?



Thanks.


---
**Arthur Wolf** *June 19, 2016 19:49*

Are you doing exactly what it's saying here : [http://smoothieware.org/install-web-interface](http://smoothieware.org/install-web-interface) ?

It works for me.

Did you watch the video ?



Also, this assumes you did setup network for your board : [http://smoothieware.org/network](http://smoothieware.org/network)


---
**Claudio Prezzi** *June 20, 2016 07:33*

Daniel: It seems your download is corrupted. I downloaded it too and was able to unzip without problems.


---
**Anthony Bolgar** *June 24, 2016 19:16*

I had problems unzipping with the built in unzipper in Windows 10. I used WinRar instead, and it unzipped fine.


---
**Arthur Wolf** *June 24, 2016 19:17*

7Zip has been reported to work too, I added a link to it in the installation documentation.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/D6d5Gaz3hH7) &mdash; content and formatting may not be reliable*
