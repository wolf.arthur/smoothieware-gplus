---
layout: post
title: "OK, I got a client with a k40 laser (with Chinese crap for software/controller, but we got it working) who wants to try the Smoothieboard/laserweb upgrade after I convinced him you guys know what you are doing"
date: June 20, 2016 06:01
category: "General discussion"
author: "Steve Anken"
---
OK, I got a client with a k40 laser (with Chinese crap for software/controller, but we got it working) who wants to try the Smoothieboard/laserweb upgrade after I convinced him you guys know what you are doing. So, where best to order to get ASAP in California? Any good pointers to replacing their controller, any gotchas?



 We have completed a number of CNC machines using Mach3 at first but migrated to TinyG and Chilipeppr, which has been really working well even for our non-computer woodworker clients. We have had a number of people interested in laser engraving and cutting so now that I have a client who has one and will pay for the board I want to join in the fun.





**"Steve Anken"**

---
---
**Ross Hendrickson** *June 20, 2016 06:08*

Uberclock or Panucatt are both US retailers.


---
**Arthur Wolf** *June 20, 2016 08:29*

Yes, for a Smoothieboard in the US you want Uberclock.

The "general" laser cutter guide is at [http://smoothieware.org/laser-cutter-guide](http://smoothieware.org/laser-cutter-guide) and we have two K40-specific guides being written at [http://smoothieware.org/bluebox-guide](http://smoothieware.org/bluebox-guide) and [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)



Don't hesitate to ask if you need help with anything.



Also if you like CNCs, look up bCNC, which works with Smoothie now.


---
**Steve Anken** *June 21, 2016 22:09*

Thanks Arthur, it's on the way.


---
**Steve Anken** *June 25, 2016 16:47*

Won't arrive until Monday, bummer.


---
*Imported from [Google+](https://plus.google.com/109943375272933711160/posts/YPXE4PHziSi) &mdash; content and formatting may not be reliable*
