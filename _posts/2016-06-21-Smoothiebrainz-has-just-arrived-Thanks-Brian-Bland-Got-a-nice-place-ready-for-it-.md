---
layout: post
title: "Smoothiebrainz has just arrived ! Thanks Brian Bland Got a nice place ready for it ;)"
date: June 21, 2016 16:08
category: "General discussion"
author: "Maxime Favre"
---
Smoothiebrainz has just arrived ! Thanks **+Brian Bland**​​​

Got a nice place ready for it ;)﻿



![missing image](https://lh3.googleusercontent.com/-Bk8FseyK8VM/V2lmeeKfMoI/AAAAAAAAEMs/22XgvC5fTyIxmnBUgWegvaLv3tJmpqU7A/s0/2016%252B-%252B1.jpeg)
![missing image](https://lh3.googleusercontent.com/-UVFhXRKjl3I/V2lmebmRnkI/AAAAAAAAEMs/rG4EBu7pIi8TMadFiWcDhmCbbOoQQSEfQ/s0/2016%252B-%252B2.jpeg)

**"Maxime Favre"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 16:31*

Oh wow. All the green terminals. Nice. 


---
**Maxime Favre** *June 21, 2016 20:42*

**+Ray Kholodovsky**  The green terminals plug nicely on the headers. you or **+Peter van der Walt**  have a config file laying around? I just want to be sure of the pining.


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2016 20:46*

Stock smoothie config (google it) FTW! You may need to put a ! after the endstop pins to invert them depending on whether you have NC or NO switches. That's it :)


---
**Maxime Favre** *June 21, 2016 20:49*

Already working on it. I'm just a bit lost with PWM1&0 do you know the corresponding pins numbers?


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/Pb9gaJDJe9v) &mdash; content and formatting may not be reliable*
