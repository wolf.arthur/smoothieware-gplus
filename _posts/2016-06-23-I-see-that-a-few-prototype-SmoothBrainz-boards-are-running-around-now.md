---
layout: post
title: "I see that a few prototype SmoothBrainz boards are running around now"
date: June 23, 2016 14:06
category: "General discussion"
author: "Ryan Fritts"
---
I see that a few prototype SmoothBrainz boards are running around now.  Was wondering if anyone was planning on making a production version of the board or something similar?





**"Ryan Fritts"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 23, 2016 17:24*

There's something bigger, better, and badder on the way that will do what you want.  Check my profile for some clues. 


---
**Ryan Fritts** *June 23, 2016 20:28*

When will this new board be out?  Will you also have spots for terminal posts for using external stepper drivers?




---
**Ray Kholodovsky (Cohesion3D)** *June 23, 2016 20:32*

We've had some good results with rev1 of prototypes, rev2 boards are on the way (added features and fixed a few small bugs).  There will be a board more feature-powerful than any existing product in the 32 bit/ smoothie category and a mini that sets a new low price point.  Both have sockets for pololu drivers which is where you can plug in your external driver wire connections (STEP, DIR, EN, GND).  There is also a design for a quick adapter board **+Peter van der Walt** made that would break out those 4 connections into an easy header.  We're testing these boards on a large variety of machines as we speak and gearing up for a kickstarter in 6-8 weeks, as soon as we can make a video and spread the word.


---
**Thomas “Balu” Walter** *June 23, 2016 20:57*

I hate open source because there's always something bigger, better, and badder on the way... How should I decide what to get? ;) 


---
**Ray Kholodovsky (Cohesion3D)** *June 23, 2016 21:15*

**+Thomas Walter** it all depends on your expectations and requirements. If you have a conventional machine and want a simple, proven design, get a smoothieboard.  If you want something a little more cutting edge, message me regarding testing my boards, or wait for the kickstarter. 


---
**Thomas “Balu” Walter** *June 23, 2016 22:25*

**+Ray Kholodovsky** I don't have enough experience with boards to be a good tester, but I will definitely have a look at the Kickstarter. 


---
**Stephane Buisson** *July 03, 2016 11:02*

**+Ray Kholodovsky** can't wait to know more about it, I think a  2 parts solution make sense, main board with ethernet but no stepper driver, and  specific 2nd boards to plug into. like K40,CNC, 4-5-6 steppers drivers, etc... 

the first one being low price entry (large production), 2nd being specific margin one (add value).


---
**Ray Kholodovsky (Cohesion3D)** *July 03, 2016 13:08*

**+Stephane Buisson** look here: [https://plus.google.com/+RayKholodovsky/posts/YWWsa9QECyD](https://plus.google.com/+RayKholodovsky/posts/YWWsa9QECyD)


---
*Imported from [Google+](https://plus.google.com/103448858896852231120/posts/b4vB839rzMn) &mdash; content and formatting may not be reliable*
