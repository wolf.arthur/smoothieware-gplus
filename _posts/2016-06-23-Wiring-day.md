---
layout: post
title: "Wiring day !"
date: June 23, 2016 15:43
category: "General discussion"
author: "Maxime Favre"
---
Wiring day !

![missing image](https://lh3.googleusercontent.com/-iHKT_qhsgNc/V2wDhn1MyoI/AAAAAAAAENY/s2ovjycv7YoZFaPeybtzxGPIF6OcvNQdQ/s0/2016%252B-%252B1.jpeg)



**"Maxime Favre"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 23, 2016 17:32*

Yes, I will take <b>all</b> the information about how you did that, please. 


---
**Stephane Buisson** *June 23, 2016 17:35*

that's clean, love it all Smoothiebrainz, Pipo,...


---
**Maxime Favre** *June 23, 2016 18:03*

I work in industrial automation so it's the least I can do 😂

**+Ray Kholodovsky**​

- cut all wires, crimp the beginning ends if needed, connect.

- take the first group of wires, 3 oranges 3 blues on the in this case, place a zip tie but keep it a bit loose.

- now arrange the wires so they don't cross and are nicely straight. Then tighten the zip tie with pliers.

- add the next round of wire, zip tie loose, arrange, tighten, repeat until your wires are where they should go.

The key is to start from one end with the other loose.

I should write a 101 wiring guide but where is the best place to put it? Open builds?


---
**Ray Kholodovsky (Cohesion3D)** *June 23, 2016 18:07*

Awesome. Add the info about the exact connectors/ crimp pieces used + any tools you needed. 


---
**Panayiotis Savva** *June 23, 2016 18:14*

**+Maxime Favre** that should be awesome. Please do the write-up


---
**Arthur Wolf** *June 23, 2016 18:19*

Nice :)


---
**Arthur Wolf** *June 23, 2016 18:19*

**+Maxime Favre** The Smoothieware wiki would love a page on wiring and how to do it right ...


---
**Jérémie Tarot** *June 23, 2016 18:56*

**+Maxime Favre** great wiring indeed, and thanks for the how to :) 

A writing on OB would of course be great... In the mean time, folks can watch videos on how to wire network cabinets to find inspiration.  At least, that's how I've learned it :) 


---
**Maxime Favre** *June 23, 2016 19:04*

Will do that ! **+Arthur Wolf**​ that would be nice


---
**Jeff DeMaagd** *June 23, 2016 21:22*

The connectors are crimp on ferrules with an insulated collar. There are ferrule crimpers too that crimp them evenly. I bought mine from Inventables but you can get them from any number of places.


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/8s9QUh9ejYJ) &mdash; content and formatting may not be reliable*
