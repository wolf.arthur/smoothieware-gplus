---
layout: post
title: "Hello, I am about to get a delta printer, and considering using a controller with smoothieware on it"
date: June 25, 2016 05:14
category: "General discussion"
author: "Michael Andresen"
---
Hello, I am about to get a delta printer, and considering using a controller with smoothieware on it.



I got a probe to mount on the hot-end for setting Z0, and I would like to also use that for auto leveling. To find out more about that I looked at the website, but there it says for deltas you only run it once, and then save it, where I see others do it pretty much every time before they print. Did I understand that right? 



And another question, the page also says to eject the SD card before doing any of the probing, is that still a issue? Seems like a thing that could easily be forgotten to do every now and then. 





**"Michael Andresen"**

---
---
**Andrew Wade** *June 25, 2016 05:37*

I always z probe before each print as this gives a more accurate bed height especially if using different bed temperatures.


---
**Arthur Wolf** *June 25, 2016 07:17*

So : You can either probe once, save the results, and be done with it, or probe before each print. You choose. If your machine is minimally sturdy you probably just need to probe once.



About ejecting the SD card ( which is something you do with your mouse, not a physical action ), you do need to do it every time. If you want to avoid having to do that, using Ethernet is a good option.


---
**Michael Andresen** *June 25, 2016 13:05*

**+Arthur Wolf** the printer should be very sturdy, but I am planning on experimenting with quick change of build platforms and such, so they can change a bit in position and such. 



Need to read more about the features over ethernet. 


---
*Imported from [Google+](https://plus.google.com/+MichaelAndresen/posts/6pKMGdkFv5Z) &mdash; content and formatting may not be reliable*
