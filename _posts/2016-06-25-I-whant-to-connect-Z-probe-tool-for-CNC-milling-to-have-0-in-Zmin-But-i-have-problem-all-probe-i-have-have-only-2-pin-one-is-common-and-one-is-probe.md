---
layout: post
title: "I whant to connect Z probe tool for CNC milling to have 0 in Zmin But i have problem all probe i have have only 2 pin one is common and one is probe"
date: June 25, 2016 13:51
category: "General discussion"
author: "Christian Lossendiere"
---
I whant to connect Z probe tool for CNC milling to have 0 in Zmin

But i have problem all probe i have  have only 2 pin one is common and one is probe. So i can't connect like endstop common always on "0" when not push and "Vcc" when touch



Have fonction like marlin firmware to overcome this problem a intern resistor can activate to pull down or pull up when can connect only 2 wire for endstop ?



I don't see example of wiring with this page

[http://smoothieware.org/pin-configuration](http://smoothieware.org/pin-configuration)

Someone can help me to wiring 2 wire for probe cnc tools and smoothieboard config ?





**"Christian Lossendiere"**

---
---
**Arthur Wolf** *June 25, 2016 13:53*

Just invert the pin : 



gamma_min   1.21 becomes

gamma_min   1.21!



if you need a pull-down you can do 

 

gamma_min   1.21!v




---
**Christian Lossendiere** *June 25, 2016 14:08*

Thanks Arthur


---
*Imported from [Google+](https://plus.google.com/113177179325470084866/posts/JEceVrKp5NM) &mdash; content and formatting may not be reliable*
