---
layout: post
title: "Shared on June 25, 2016 13:52...\n"
date: June 25, 2016 13:52
category: "General discussion"
author: "Christian Lossendiere"
---


![missing image](https://lh3.googleusercontent.com/-ZEI5v3bu-iM/V26MhS0bOHI/AAAAAAAABXo/Fmzo5jLCVZU3quFk6uxNn7AVQIaMIFThg/s0/z-prob.jpg)



**"Christian Lossendiere"**

---
---
**Christian Lossendiere** *June 25, 2016 13:53*

How connect this king of z probe tools for CNC mill with smoothieboard ?


---
**Maxime Favre** *June 25, 2016 16:49*

COM to board GND, Prob to Z endstop signal. The power to whatever power supply you have in your machine (since the power and contacts are separated)


---
**Christian Lossendiere** *June 25, 2016 16:56*

Thanks Maxime, now it's Ok all works


---
*Imported from [Google+](https://plus.google.com/113177179325470084866/posts/7L785Vr6vUj) &mdash; content and formatting may not be reliable*
