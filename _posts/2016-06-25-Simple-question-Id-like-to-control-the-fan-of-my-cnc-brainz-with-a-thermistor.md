---
layout: post
title: "Simple question: I'd like to control the fan of my cnc brainz with a thermistor"
date: June 25, 2016 16:41
category: "General discussion"
author: "Maxime Favre"
---
Simple question:



I'd like to control the fan of my cnc brainz with a thermistor. Something like over valueA = on, under valueB = off (or a value with hysereris like bang-bang). 

Is there a way to do that in smoothie ? 

I want the function to be active all the time when the board is powered, not gcode activated like "temperature_control" or "switch".

It can easily be done with an arduino trinket-like but if I can use those spare thermistor inputs...









**"Maxime Favre"**

---
---
**Maxime Favre** *June 25, 2016 16:45*

-_- Just saw it on the wiki I think I'm blind.... Thanks Peter


---
**Maxime Favre** *June 26, 2016 13:04*

Smoothiebrainz thermistor pins are T1 0.23, T2 0.24 and T3 0.25 ?


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/TX2aVrLJEFD) &mdash; content and formatting may not be reliable*
