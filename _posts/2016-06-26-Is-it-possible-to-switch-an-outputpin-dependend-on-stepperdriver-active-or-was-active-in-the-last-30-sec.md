---
layout: post
title: "Is it possible to switch an outputpin dependend on \"stepperdriver active\" or \"was active in the last 30 sec\"?"
date: June 26, 2016 11:04
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
Is it possible to switch an outputpin dependend on "stepperdriver active" or "was active in the last 30 sec"? Goal is to switch a coolingfan to on only when the drivers are in use.





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *June 26, 2016 11:39*

You can setup a switch to read any gcode, including those that other modules use. so you could set a switch to turn a fan on/off based on the stepper driver on/off gcodes.

There is no way to do this for when the steppers automatically turn on when a movement is requested.

If you have a smoothieboard you could do that by soldering a wire between a driver enable pin and a mosfet control pin ( all are broken out )


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/cpuM61WiTJe) &mdash; content and formatting may not be reliable*
