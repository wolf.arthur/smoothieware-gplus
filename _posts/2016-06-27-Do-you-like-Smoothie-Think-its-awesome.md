---
layout: post
title: "Do you like Smoothie ? Think it's awesome ?"
date: June 27, 2016 18:00
category: "General discussion"
author: "Arthur Wolf"
---
Do you like Smoothie ? Think it's awesome ? It's that way because for years now, many folks have helped making it as best as it can be.



We are always looking for new people to come in and help, and there are <b>many</b> things to do.



If you feel like you could spend a bit of your free time helping the project back, please consider taking some time to read this page : 



[http://smoothieware.org/todo](http://smoothieware.org/todo)



and see if anything in there sounds like something you could help with.



Thank you very much no matter what you choose to do, we have an awesome community, and just using the project already helps :)



<b>Also please share this post to others, because you know ... the more the merrier</b>





**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/EqdEbsGeQ8u) &mdash; content and formatting may not be reliable*
