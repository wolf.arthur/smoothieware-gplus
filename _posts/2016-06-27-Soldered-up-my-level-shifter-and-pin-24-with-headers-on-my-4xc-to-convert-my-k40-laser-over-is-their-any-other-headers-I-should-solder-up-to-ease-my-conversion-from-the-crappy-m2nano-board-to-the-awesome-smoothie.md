---
layout: post
title: "Soldered up my level shifter and pin 2.4 with headers on my 4xc to convert my k40 laser over is their any other headers I should solder up to ease my conversion from the crappy m2nano board to the awesome smoothie?"
date: June 27, 2016 06:26
category: "General discussion"
author: "Alex Krause"
---
Soldered up my level shifter and pin 2.4 with headers on my 4xc to convert my k40 laser over  is their any other headers I should solder up to ease my conversion from the crappy m2nano board to the awesome smoothie? Really want to do this the right way and not fry a board #Smoothiefy





**"Alex Krause"**

---
---
**Stephane Buisson** *June 27, 2016 08:31*

**+Peter van der Walt** can't wait your K40 arrived, and got your view on point 5. I have a feeling for the need of a resistor to adapt the signal into PSU PWM or to find out why it's not optimum without.


---
**Stephane Buisson** *June 27, 2016 08:34*

**+Alex Krause** pin 2.5 or 2.6, and agree 4XC should come with  connectors soldered, if you have big fingers and poor sight, that make that point tricky (**+Arthur Wolf** )


---
**Ariel Yahni (UniKpty)** *June 27, 2016 11:52*

Does this make sense [https://goo.gl/fgDHJj](https://goo.gl/fgDHJj)


---
**Alex Krause** *June 27, 2016 16:23*

**+Peter van der Walt**​ step four led and resistor are in series of each other but parallel to the pwm output? 


---
**Arthur Wolf** *June 27, 2016 16:39*

**+Peter van der Walt** No soft-endstops yet, but seriously working on it ( just wrote a spec for the feature to weed things out )


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/fiZSoHr5ApR) &mdash; content and formatting may not be reliable*
