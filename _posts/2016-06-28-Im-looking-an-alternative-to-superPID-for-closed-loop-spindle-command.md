---
layout: post
title: "I'm looking an alternative to superPID for closed loop spindle command"
date: June 28, 2016 11:58
category: "General discussion"
author: "Maxime Favre"
---
I'm looking an alternative to superPID for closed loop spindle command. Anyone tried smoothie'one?





**"Maxime Favre"**

---
---
**Arthur Wolf** *June 28, 2016 12:31*

Smoothie's spindle module supports a PWM output, and an encoder input for closed-loop, and PID, so all you should need is a way to control the spindle via PWM


---
**Maxime Favre** *June 28, 2016 15:34*

Yep I can borrow a drive at work to do some tests. We use them with enable and 0-10V or 4-20mA for speed but it should be ok with pwm.﻿


---
**Maxime Favre** *June 28, 2016 15:58*

Depends how the vfd is configured. For now I'm just looking the smoothie's side of the thing for my CNC brainz. I'd like to have the option available later. Since I can have a good vfd for almost nothing, I am thinking about hacking my dewalt to closed loop. But that's for later and things may change before that.


---
**Maxime Favre** *June 29, 2016 04:36*

Do you sleep sometimes? :P great project btw !


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/HUcrks6vLVx) &mdash; content and formatting may not be reliable*
