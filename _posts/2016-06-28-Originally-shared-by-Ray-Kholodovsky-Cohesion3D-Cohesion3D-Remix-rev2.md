---
layout: post
title: "Originally shared by Ray Kholodovsky (Cohesion3D) Cohesion3D Remix (rev2)"
date: June 28, 2016 04:22
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



Cohesion3D Remix (rev2). Isn't it just majestic? 6 swappable stepper drivers, high current automotive grade MOSFETs including one large enough to run a 20 amp heatbed (or more), protected endstops, and (not shown populated here) wifi control over an ESP8266 module. All powered by the super awesome 32 bit highly configurable smoothieware. 



And if you've got a particularly large machine, crimping and extending all your wires for motors, endstops, and extruders can be a real pain, which is why there's also a RJ45 shield that will let you wire everything with off the shelf CAT5 cable. 



If you want a fast and safe board that can handle anything from your simple cnc mill or laser cutter to your 2 ft square 3D printer with triple extruders, Cohesion3D ReMix is for you. 



![missing image](https://lh3.googleusercontent.com/-eVeDijotc-g/V3H7XWebd-I/AAAAAAAAcK4/iT49MmYczOY0SddN67fWijvHnllyiI5TQ/s0/95473fe4-2c44-486c-b162-13d6bcfa8505.jpeg)
![missing image](https://lh3.googleusercontent.com/-jyXXOFGf2nY/V3H7XSA48OI/AAAAAAAAcK4/mP-qCX3xHjkBypGG1KceX0go5Bno4g5NA/s0/070347f5-83da-4148-857d-e535fb315b8e.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Marc Pentenrieder** *June 28, 2016 08:39*

Hi Ray, nice work. Can I already buy a board ?


---
**Gary Hangsleben** *June 28, 2016 08:48*

Great work, nice layout 


---
**Christopher Seward** *June 28, 2016 11:32*

Want


---
**Samer Najia** *June 28, 2016 14:24*

And having been involved in testing, lemme tell ya, getting this up and running is easier than anything.  I am converting all 10 of my printers to this.


---
**quillford** *June 28, 2016 15:47*

What esp firmware are you using with this? I'm trying to get fabrica working with my azteeg x5.


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2016 15:48*

**+Marc Pentenrieder** **+Christopher Seward** I hand make these right now and can put some together if you want.  Or wait until we launch on Kickstarter so I can get these mass produced. 


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2016 15:51*

**+quillford** I've been playing with esp-link and I like it. There's a few more firmware so that **+Peter van der Walt** and I are exploring for our various assorted board designs. 


---
**Erik Cederberg** *June 28, 2016 22:17*

**+Ray Kholodovsky** This looks like just what i has been looking for, when will it be available to buy? 


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2016 22:26*

**+Erik Cederberg** we're gearing up for a Kickstarter ASAP. If you'd like to help test I can get you a handmade board (which is what you see in the pic). Most  of the kinks are worked out at this point, it's just not a "production" version yet. 


---
**Marc Pentenrieder** *June 29, 2016 06:41*

How much is a handmade version ? I am building a large 3D printer and want to test your board, perhaps with the shield ?!




---
**Christopher Seward** *June 29, 2016 11:40*

**+Ray Kholodovsky** I would be glad to test.  I have a large format dual extruder printer (Cobblebot) sitting already assembled.  I have a ramps board and a arduino mega but I was hoping to find a board to run smoothieware.


---
**Erik Cederberg** *August 13, 2016 13:10*

**+Ray Kholodovsky** Any news for a kickstarter, or would it be possible to get two pre-production boards from you? :-)


---
**Ray Kholodovsky (Cohesion3D)** *August 13, 2016 16:10*

**+Erik Cederberg** Thanks for checking in, still doing a lot of testing, currently waiting for round 3 of blank PCBs to arrive so that I can confirm the absolute final design. Might be able to handmake some more, have started a hangout with you to discuss further.


---
**Karan Chaphekar** *August 21, 2016 17:04*

Can rj45 conduct current for heater?


---
**Ray Kholodovsky (Cohesion3D)** *August 21, 2016 17:10*

**+Karan Chaphekar** Yes, we tripled them up so running 3.3 amps is not problem.  It is 3 wires for the 12/24v and 3 wires for ground inside the cat5.  The cable does not even get warm.


---
**Karan Chaphekar** *August 21, 2016 22:29*

I am not concerned about cable, but more about the connecotor because a connection with small contact will heatup and burn eventually 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/YWWsa9QECyD) &mdash; content and formatting may not be reliable*
