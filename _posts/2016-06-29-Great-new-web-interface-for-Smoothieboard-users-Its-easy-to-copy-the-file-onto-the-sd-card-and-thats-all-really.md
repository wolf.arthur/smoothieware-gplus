---
layout: post
title: "Great new web interface for Smoothieboard users :) Its easy to copy the file onto the sd card and thats all really..!!"
date: June 29, 2016 14:56
category: "General discussion"
author: "David Bassetti"
---
Great new web interface for Smoothieboard users :)



Its easy to copy the file onto the sd card and thats all really..!!



[http://smoothieware.org/install-web-interface](http://smoothieware.org/install-web-interface)



**+Arthur Wolf**

![missing image](https://lh3.googleusercontent.com/-qN0u-OQq0Pk/V3PhkiMrL6I/AAAAAAAAEZI/HoikJkzImNYufb3wo40OjJ4rle8j3NLlg/s0/SmoothieWARE%252Bnew%252BWeb%252Binterface.png)



**"David Bassetti"**

---
---
**Jarek Szczepański** *June 29, 2016 18:02*

huh... these temperature graphs shouldn't be like that... :) I will fix it in next version!


---
**David Bassetti** *June 30, 2016 08:05*

**+Jarek Szczepański**

Hey Jared is it possible to put the ¨progress¨ for the print in there like the classic version BUT change it to minutes and hours instead of seconds maybe


---
*Imported from [Google+](https://plus.google.com/+DavidBassetti/posts/QDXZoetxE2Z) &mdash; content and formatting may not be reliable*
