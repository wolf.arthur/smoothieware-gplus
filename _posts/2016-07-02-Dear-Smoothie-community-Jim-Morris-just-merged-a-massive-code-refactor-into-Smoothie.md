---
layout: post
title: "Dear Smoothie community. Jim Morris just merged a massive code refactor into Smoothie ( )"
date: July 02, 2016 21:43
category: "General discussion"
author: "Arthur Wolf"
---
Dear Smoothie community.

 

Jim Morris just merged a massive code refactor into Smoothie ( [https://github.com/Smoothieware/Smoothieware/pull/961](https://github.com/Smoothieware/Smoothieware/pull/961) ).

Thanks to him for this truly massive chunk of work, and to all those who tested it before merging.

 

This changes Smoothie to calculate acceleration for every step instead of 1000 times a second, which results in even smoother/better quality movement.

It also comes with many many smaller improvements to planning and step generation. It solves many small problems users experienced in some circumstances. Amongst those, it is expected to fix the infamous "S3D bug", so please test that.

It is also expected to make Smoothie more sturdy overall, and is a first step towards implementing some more refactors.

 

A few things have changed for users, so please read [https://github.com/Smoothieware/Smoothieware/blob/edge/upgrade-notes.md](https://github.com/Smoothieware/Smoothieware/blob/edge/upgrade-notes.md) before upgrading.

 

To upgrade, the procedure is as usual, and you can find it here : [http://smoothieware.org/flashing-smoothie-firmware](http://smoothieware.org/flashing-smoothie-firmware)

 

Please tell us if you encounter any problems, and what you think about your Smoothie's new step generation.

 

Cheers :)

 

PS : The code does not support laser yet, do not upgrade to this new version if you are using a laser. We will implement laser as soon as possible.





**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/b23wNA79Tyq) &mdash; content and formatting may not be reliable*
