---
layout: post
title: "Hello folks ! If you have been using a Smoothieboard in a 3D printer, you probably want to take a look at this : We updated the temperature control ( hotend ) documentation safety section to be clearer, and include more"
date: July 03, 2016 20:59
category: "General discussion"
author: "Arthur Wolf"
---
Hello folks !



If you have been using a Smoothieboard in a 3D printer, you probably want to take a look at this : 



[http://smoothieware.org/temperaturecontrol#toc19](http://smoothieware.org/temperaturecontrol#toc19)



We updated the temperature control ( hotend ) documentation safety section to be clearer, and include more options for safety, and reflect the latest changes.



If you read, and implement the safety precautions described on this page, you can make your 3D printer much safer.



We strongly recommend you do so, and give us feedback.



Cheers.





**"Arthur Wolf"**

---
---
**Anthony Bolgar** *July 03, 2016 21:14*

Thanks **+Arthur Wolf** Safety seems to be put on the back burner in most projects. I am glad to see that you and the boys over at Smoothie care about safety. When I got my K40, the first thing I did with it was add interlocks and temp sensors before I even test fired the tube. New firmware features are great, but not at the expense of safety. This one is a hone run!


---
**Eric Lien** *July 03, 2016 21:15*

Glad to see this as a priority. As someone who runs prints overnight regularly... these simple changes really help you sleep at night.



Are the changes fairly new, or did I miss these being implemented a while back?


---
**Arthur Wolf** *July 03, 2016 21:15*

**+Anthony Bolgar** Well some folks have been unhappy that we didn't have some of those. It's not that we didn't want to have them, but it's quite a bit of work to figure out, implement, test, etc. I'm glad we have them now.


---
**Arthur Wolf** *July 03, 2016 21:17*

**+Eric Lien** Some of it has been here for a while, some of it is recent. I just added runaway detection, which was a major thing missing, but most of the rest has been in for a long time.




---
**René Jurack** *July 03, 2016 21:44*

I love it! Kudos! I am going to implement all of these! Only thing missing now: a watchdog. (or is it already there?)


---
**Arthur Wolf** *July 03, 2016 21:44*

**+René Jurack** It's already there, and because it's not temperature-specific, and you don't have to enable it yourself, it's not in the documentation.


---
**René Jurack** *July 03, 2016 21:46*

But you could mention it, makes it even better!


---
**Arthur Wolf** *July 03, 2016 21:47*

**+René Jurack** Eh, doesn't cost much. I'll add it.


---
**Christian Lossendiere** *July 03, 2016 21:49*

It's possible to use 12v 40W hotend heater cartdrige with 24V PSU ?

No risk ? need change some setting in config file ?


---
**Arthur Wolf** *July 03, 2016 21:51*

**+Christian Lossendiere** I wouldn't recommend it, if something goes wrong that's A LOT of power going into your hotend.

I guess if you implement all the safety features it would be safe ...



To use a 12V hotend at 24V, you must limit the PWM. Go to [http://smoothieware.org/temperaturecontrol](http://smoothieware.org/temperaturecontrol) and search "max_pwm" in the page.


---
**Arthur Wolf** *July 03, 2016 21:51*

**+René Jurack** Added.


---
**Jeff DeMaagd** *July 04, 2016 04:16*

Thanks to those that implemented and tested it. I'll upgrade my firmware soon.


---
**Christian Lossendiere** *July 04, 2016 06:57*

**+Arthur Wolf**

Thanks Arthur


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/ERYWteT19FG) &mdash; content and formatting may not be reliable*
