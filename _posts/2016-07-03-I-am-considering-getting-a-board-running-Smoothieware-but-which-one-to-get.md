---
layout: post
title: "I am considering getting a board running Smoothieware, but which one to get?"
date: July 03, 2016 21:24
category: "General discussion"
author: "Michael Andresen"
---
I am considering getting a board running Smoothieware, but which one to get? 



I am going to use it for a single hot-end delta printer, and going to run it all on 24v.





**"Michael Andresen"**

---
---
**Anthony Bolgar** *July 03, 2016 21:28*

I would purchase a Smoothieboard 4XC so that you get the ethernet connectivity. And by getting an original Smoothieboard, you are supporting the guys that made it possible.


---
**Arthur Wolf** *July 03, 2016 21:35*

A 4XC Smoothieboard would fit your needs yes. If you need any help with it, you can email me directly at wolf.arthur@gmail.com


---
**Michael Andresen** *July 04, 2016 13:42*

I am looking at getting a 4XC from [http://robotseed.com/index.php?id_product=11&controller=product&id_lang=2](http://robotseed.com/index.php?id_product=11&controller=product&id_lang=2) they list it as being a 1.0A, is that still the newest one?



Pictures show it as without ethernet connector, but in the text it says it is with, so I assume it is just an old picture?


---
**Arthur Wolf** *July 04, 2016 18:32*

**+Michael Andresen** Robotseed sells the latest boards ( 1.0b ), with the ethernet connector pre-soldered.



Cheers.


---
**Michael Andresen** *July 04, 2016 18:34*

**+Arthur Wolf** Okay, I guess they just didn't update the text either then.


---
*Imported from [Google+](https://plus.google.com/+MichaelAndresen/posts/dX2Qr9JXmbS) &mdash; content and formatting may not be reliable*
