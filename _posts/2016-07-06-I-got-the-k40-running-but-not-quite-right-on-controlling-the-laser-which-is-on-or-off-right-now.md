---
layout: post
title: "I got the k40 running but not quite right on controlling the laser which is on or off right now"
date: July 06, 2016 19:07
category: "General discussion"
author: "Steve Anken"
---
I got the k40 running but not quite right on controlling the laser which is on or off right now. First time working with Laserweb3 and not sure what is working and what is not. It has hung several times on two machines, Win10 and Win7, during generate Gcode. Still getting familiar with this software but I can only do bitmaps and the mouse disappears a lot. Sometimes after generating it goes into slow motion. Some workflow things are unfamiliar to me and not sure what to expect but I have lost communication a number of ways with no clear way to reconnect other than rebooting everything to get going again.



I know the developers want help with the interface but all I can do is give feedback and hope it helps you see the potential for people who are not techies. I've lost money and time on this but was hoping to be able to get this stable enough to offer along with our CNC machines and education program. Please think about how this can be used as a replacement for the Chinese software when they can already get some good results after a bit of help. I really wanted the vector stuff but the only image and gcode I get is from bitmaps. Opening gcode files does not do anything but open an empty tab and generate an empty(except Gcode config begin and end codes). Still not clear on how to get S range of 0-1. 



Anyway, I may have jumped in too soon and in over my head but I'd sure like to make this work. Any suggestions welcome. I have pictures if that helps. I think I am doing multiple things wrong but  have nobody here who I can bounce this off. Wish I had more time.





**"Steve Anken"**

---
---
**Arthur Wolf** *July 06, 2016 19:17*

Hey.



You are not the first to go in "over your head", and everybody always ends up with a working setup, so don't worry. It just takes time sometimes, but if you are patient you'll get there.



Could you describe exactly how you have wired the laser control ?



Cheers.


---
**Arthur Wolf** *July 06, 2016 19:17*

**+Peter van der Walt** I think there are some laserweb-specific questions in there.


---
**Ariel Yahni (UniKpty)** *July 06, 2016 19:39*

Masked svg also give trouble 


---
**Ariel Yahni (UniKpty)** *July 06, 2016 19:43*

**+Peter van der Walt**​ agree but many use logos, etc and they all come masked. I don't think anyone will use masked on parts. I'm also starting to like dxf better than svg


---
**Ariel Yahni (UniKpty)** *July 06, 2016 20:12*

Just remember the majority of users come from a non CAD background so as with 3DP a teaching / learning curve is necesary specially since most users I believe would think using a laser is like printing 2D


---
**Steve Anken** *July 07, 2016 03:30*

Thanks for all the feedback. I got my groups all tangled up so I'll try to separate the hardware and software issues. Client is just not as happy but I feel I am getting close and and getting familiar with the environment.



As for the speed of the systems, I am running a lot of things that should be sucking up WAY more bandwidth like Fusion360 and ArtCAM. Shop classes like dedicated hardware and they are used to things like Mach3 running their machines on cheep machines very reliably for years and years.



 It only dies during the Gcode generation second pass 3/4 mark or so. Very consistent on some files but bigger is worse. I run Chilipeppr and TinyG with no problem on this laptop. The client machine is even slower. I am even able to pulll the Gcode from Laserweb3 into Chilipeppr and look at it.



  Thanks for the s setting explanation I had played with it and now understand it better and will try it tomorrow if the client does not kill me first for not returning his calls as I write this. :-o


---
**Steve Anken** *July 08, 2016 01:36*

Well got it going sorta and posted images to my home account, not sure how to link it to this post. Images are reversed and too dark but at least x y looks ok.


---
**Ariel Yahni (UniKpty)** *July 08, 2016 01:51*

**+Steve Anken**​ read this for a fix on that matter [https://plus.google.com/117180320763045071687/posts/gttCAcG8rNG](https://plus.google.com/117180320763045071687/posts/gttCAcG8rNG)


---
**Steve Anken** *July 08, 2016 02:55*

I need to read it again, i was looking at this to ground the TL on the PSU to fire the laser. 



# Switch module for laser TTL control

switch.laser.enable                            true             #

switch.laser.input_on_command                  M106             #

switch.laser.input_off_command                 M107             #

switch.laser.output_pin                        1.31             #


---
*Imported from [Google+](https://plus.google.com/109943375272933711160/posts/RWjZWXzGU27) &mdash; content and formatting may not be reliable*
