---
layout: post
title: "Can smoothieware handle a dual X carriage 3d printer at this time?"
date: July 09, 2016 23:40
category: "General discussion"
author: "\u00d8ystein Krog"
---
Can smoothieware handle a dual X carriage 3d printer at this time?

I'm talking about a setup where each extruder has it's own carriage, like BCN3D.

Marlin does this quite well now, both in fully automatic mode (firmware moves tools for you) and in manual mode (gcode needed to move/swap tools).





**"\u00d8ystein Krog"**

---
---
**Ariel Yahni (UniKpty)** *July 09, 2016 23:50*

interested




---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2016 00:37*

As far as I know that was something that was written into Marlin firmware and is not something I have seen in the smoothie code. It could potentially be handled in the program that makes the gcode, but not as seamlessly as Marlin does it. 


---
**Arthur Wolf** *July 10, 2016 07:15*

Hey.



Nobody added this to Smoothie yet.

You could implement it with custom gcodes and a bit of special wiring, (ie without having to change the code ), this has been done and is known to work, but it's not "natively" supported.



Cheers.


---
*Imported from [Google+](https://plus.google.com/+ØysteinKrog/posts/4DRpPJyWw7d) &mdash; content and formatting may not be reliable*
