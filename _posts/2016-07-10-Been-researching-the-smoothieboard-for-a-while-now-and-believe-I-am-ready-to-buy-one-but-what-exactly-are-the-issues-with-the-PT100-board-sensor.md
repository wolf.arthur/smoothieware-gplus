---
layout: post
title: "Been researching the smoothieboard for a while now and believe I am ready to buy one, but what exactly are the issues with the PT100 board/sensor?"
date: July 10, 2016 18:49
category: "General discussion"
author: "Soup3y gnome"
---
Been researching the smoothieboard for a while now and believe I am ready to buy one, but what exactly are the issues with the PT100 board/sensor?  I just found this in a few other threads and would like to know as I am using one on my printer right now and would really like to transfer it to the smoothieboard.  If this is not possible yet what other cartridge style thermocouples can be used with the board?  

(If this is not the place to post this sort of thing please let me know where I should, my google fu is not very strong.)  Thank you.





**"Soup3y gnome"**

---
---
**Arthur Wolf** *July 10, 2016 18:58*

Hey.



Smoothie doesn't support PT100. We are very close to supporting it, somebody added support, but did not have time to test it. They will when they have free time, but I don't know when.

Maybe you could test and tell us if it works ? If it doesn't I'm sure it'd get fixed fast.



Cheers.


---
**Soup3y gnome** *July 10, 2016 19:00*

All I would need are the files and directions on wiring and I will order the board today and test around Friday.


---
**Arthur Wolf** *July 10, 2016 19:02*

Cool ping me when you get there, i'll figure it out, give you instructions.


---
**Soup3y gnome** *July 10, 2016 19:49*

Be glad to do whatever I can, just ordered the board.  I will let you know when I am ready.  Thank you for this chance to help.


---
**Soup3y gnome** *July 15, 2016 01:52*

**+Arthur Wolf**  I have the board and will finish soldering and setting up firmware tomorrow, so I should be ready for those instructions as soon as you are able to get them to me.  I also sent an e-mail as well.




---
*Imported from [Google+](https://plus.google.com/103765596183993757536/posts/JH4uz8wYC8q) &mdash; content and formatting may not be reliable*
