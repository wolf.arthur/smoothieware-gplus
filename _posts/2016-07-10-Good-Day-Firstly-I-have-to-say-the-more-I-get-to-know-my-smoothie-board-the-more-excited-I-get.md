---
layout: post
title: "Good Day, Firstly, I have to say the more I get to know my smoothie board the more excited I get!"
date: July 10, 2016 16:48
category: "General discussion"
author: "Daniel Benoit"
---
Good Day,



Firstly, I have to say the more I get to know my smoothie board the more excited I get! I finally have everything working on my Gilson conversion and I'm getting closer to testing a print job. Not having done any 3D printing before presents new challenges every day, but it is a great journey.



Question: I have my board in an enclosure and I would like to install some external LED's 1 to 4. If I solder a header at Pins P1.18 to P1.21, and take these to the positive of the external LED's do I just take the negative from somewhere on the board? What is the voltage at the above pins? Can I wire those directly to the LED's without resisters?



Thanks,



Dan





**"Daniel Benoit"**

---
---
**Arthur Wolf** *July 10, 2016 16:58*

**+Daniel Benoit** Eh, glad you like it :)

For the LEDs, you need to take GND anywhere on the board, the output is 3.3v, and you need to add resistors yourself.


---
**Daniel Benoit** *July 10, 2016 22:11*

Great thanks for the info Arthur. That's kinda what I thought.


---
*Imported from [Google+](https://plus.google.com/+sandcrab123/posts/U9obBJ1grH4) &mdash; content and formatting may not be reliable*
