---
layout: post
title: "Can we give Arthur Wolf or any other willing developers some free PT100 amplifiers and sensors (as well as thermocouple boards if you fancy them) so that they can be implemented in smoothieware?"
date: July 14, 2016 09:52
category: "General discussion"
author: "E3D Support"
---
Can we give **+Arthur Wolf** or any other willing developers some free PT100 amplifiers and sensors (as well as thermocouple boards if you fancy them) so that they can be implemented in smoothieware?



Our customers ask for it a lot, and it seems from your forums that your customers are after it too! Happy to work together to end up with a solution.



FWIW: I think the fears about it blowing an ADC are unfounded, as if you feed the board 3.3v power instead of 5v it will work just fine, and scale it's output to be between 0-3.3v. It's just a fancy opamp really.





**"E3D Support"**

---
---
**Soup3y gnome** *July 14, 2016 12:15*

Once my smoothieboard arrives (which is hopefully today), I will hopefully be able to get it setup and get instructions from **+Arthur Wolf** on how to setup the firmware and wire the pt100 to the board.  Cross your fingers it works out.


---
**Ray Kholodovsky (Cohesion3D)** *July 14, 2016 13:17*

Yes, I also design boards that run on smoothie firmware and would be interested in adding direct hardware support on a future board. But I think the software devs need to finish the support first before I can test anything. 


---
**Zane Baird** *July 14, 2016 14:13*

Once the firmware is set up for testing I would be happy to try out a PT100 on both my Smoothieboard and Azteeg Mini X5


---
**Jeff DeMaagd** *July 14, 2016 14:40*

What's the analog range though? If the scaling is the same 0.01V per ˚C as an analog thermocouple amp then you don't have a practical range above 300˚C. I wouldn't count on that amp being correct for the top 0.3V, and you need some safety margin anyways. I think that's the reason why Smoothieboard has a digital thermocouple amp.


---
**John Driggers** *July 14, 2016 18:30*

**+Wolfmanjm** 


---
**Soup3y gnome** *July 15, 2016 17:07*

I hope this can get resolved fairly quickly, as I just purchased the smoothieboard and would not like to go back to the original thermistor.  I always had issues with those things not seating right.  Still in the process of soldering all the pins on the board, but it should be ready soon as long as the diodes arrive before 8 today.


---
**Jeff DeMaagd** *July 15, 2016 17:11*

Short term, they do support thermocouples with a digital thermocouple board, which I think is cheaper to get and support.


---
**Soup3y gnome** *July 15, 2016 17:34*

**+Jeff DeMaagd**  You may be right, but since it was easier to install the pt100 on my Arduino that is what I have.  I just think it would be a waste to order another thermocouple and board if it could be supported.


---
**Arthur Wolf** *July 16, 2016 21:30*

**+E3D Support** I told **+Steven Campbell** we had code ready to test for this, but after contacting the author the code is not as advanced as I thought. Which means I put **+Steven Campbell** in trouble, and I'd like to fix that if possible. 

So if you send me some PT100 hardware, I'll do my best to finish the smoothie code for it.

Please email me at wolf.arthur@gmail.com for details.



Cheers :)


---
**Soup3y gnome** *July 16, 2016 22:26*

**+Arthur Wolf** Hope all goes well sir.


---
*Imported from [Google+](https://plus.google.com/+E3DSupport/posts/2DmCCCwA2VQ) &mdash; content and formatting may not be reliable*
