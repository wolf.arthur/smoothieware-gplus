---
layout: post
title: "I have the Smoothie setup to run a laser diode"
date: July 16, 2016 21:27
category: "General discussion"
author: "Phil Aldrich"
---
I have the Smoothie setup to run a laser diode.  Is there an easy way to turn on the laser at the lowest power level while there is no motion?  It would be nice to do this to manually position the laser and to also focus it. 





**"Phil Aldrich"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 16, 2016 21:36*

Yes, there is a parameter for this in config. I believe it is called trickle power. That said, I don't know if it will work the way you want it to or how safe that is. 


---
**Arthur Wolf** *July 16, 2016 21:37*

There is no way to do this ( power laser while not moving ). We plan on adding a gcode for it, but it's not there yet. Maybe you can add a button to do this manually ?


---
**Ray Kholodovsky (Cohesion3D)** *July 16, 2016 21:41*

Looks like someone else had the same idea: [http://smoothieware.org/forum/t-1683314/enable-laser-for-focusing](http://smoothieware.org/forum/t-1683314/enable-laser-for-focusing)


---
**Ariel Yahni (UniKpty)** *July 16, 2016 21:42*

Well if your laser dosent have a Z then you could do a very small move with very low power in that axes


---
**Ariel Yahni (UniKpty)** *July 16, 2016 21:43*

So G1 Z0.1 S10 and then G0 Z-0.1 


---
**Phil Aldrich** *July 17, 2016 02:32*

I have an active Z axis so I think I will try a short G1 move and see if it will work good enough. Thanks for the info. 


---
**Arthur Wolf** *July 17, 2016 07:54*

**+Phil Aldrich** I <b>think</b> you can cheat and setup an extruder ( that actuates nothing ) , and then do G1 E10 S0.1


---
**Phil Aldrich** *July 17, 2016 12:55*

**+Arthur Wolf** thanks Arthur. I'll try that first and let you know the results. 


---
**Phil Aldrich** *July 17, 2016 18:55*

**+Arthur Wolf** I tried the extruder "cheat" by uncommenting the hotend config settings and issuing the G1 command as you stated but I saw no results. I'm not convinced I am doing everything I need to set this up correctly however. 


---
**Ariel Yahni (UniKpty)** *July 17, 2016 18:59*

You could use M106/M107 instead to drive the laser instead of G0/G1 moves


---
**Phil Aldrich** *July 30, 2016 15:45*

After more playing around with this, I was able to coax it into working. I had to set a very small laser_module_tickle_power (for my diode, 0.05 was the lowest I could go). I issued a G0 E1000 move and the laser came on dimly for about 10 seconds. I had to change the E value to something different to repeat the results. Not ideal but it will work for now. I may work up a hardware solution when I have more time unless, of course, you make a firmware change later. Thanks for the help on this


---
**Matt Wils** *April 20, 2017 18:06*

Could you please share your settings for your Smoothieboard 5w laser Diode setup. 


---
*Imported from [Google+](https://plus.google.com/105080379699420524192/posts/cKcQyZFdmsB) &mdash; content and formatting may not be reliable*
