---
layout: post
title: "I read somewhere that there's some config changes required for deltas with the latest major code merge?"
date: July 16, 2016 04:01
category: "General discussion"
author: "Shai Schechter"
---
I read somewhere that there's some config changes required for deltas  with the latest major code merge? Can't seem to find where I read it. Anyone know about this?





**"Shai Schechter"**

---
---
**Arthur Wolf** *July 16, 2016 08:02*

**+Shai Schechter** Hey, I think this is what you are looking for : [http://smoothieware.org/blog:14](http://smoothieware.org/blog:14)



Cheers.


---
**Shai Schechter** *July 18, 2016 19:46*

Thanks! Read it and made the .hotend. changes, but regarding inverting the pin for extruder, is that necessary? I didn't do it and it still works fine...


---
**Arthur Wolf** *July 18, 2016 21:11*

If it works fine, don't change it :)


---
**Wolfmanjm** *July 19, 2016 00:21*

if the extruder did not reverse then you are not using edge but still using master. If you are using the firmware I sent you then you are on master not edge as you requested.y


---
**Wolfmanjm** *July 19, 2016 00:21*

and you should not make any of the changes in the blog post if you are on master branch.


---
**Shai Schechter** *July 19, 2016 01:26*

Ok thanks for the info **+Wolfmanjm** . I changed the .hotend.  but that was all that was changed. Seems to still work, but I'll revert it back.


---
**Wolfmanjm** *July 19, 2016 03:12*

actually the .hotend stuff is valid for master and edge. so yu can leave it as .hotend.


---
*Imported from [Google+](https://plus.google.com/+shaischechter/posts/17r8PjsRJLQ) &mdash; content and formatting may not be reliable*
