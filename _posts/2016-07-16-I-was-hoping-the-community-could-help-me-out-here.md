---
layout: post
title: "I was hoping the community could help me out here"
date: July 16, 2016 18:16
category: "General discussion"
author: "Michael Forte"
---
I was hoping the community could help me out here. I have this Smoothieboard that I was about to install on my OX CNC but have been reading that it is not recommended. Not because it can't do CNC, which it can, but because the motor drivers are only rated at 2A and you won't get the full power you need for a NEMA 23 motor.



The first question I have to those using Smoothie, is that true?

If it is true, then is there a way to use external motor controllers powered by the Smoothie? Has anyone done this? I thought that I saw a post by **+Arthur Wolf** that this could be done and since I'm new to all of this I'm not sure what I would need to do it.



So any help from people that have done this and opinions on whether or not to do it would be greatly appreciated.





**"Michael Forte"**

---
---
**Sebastian Szafran** *July 16, 2016 19:10*

I have been testing DM 522 stepper drivers with Arduino Uno (directly connected step and dir pins), as well as with ArduinoDue via level shifters (3.3V to 5V). This should work the same way with Smoothie. 


---
**Robert Burns** *July 16, 2016 19:17*

[http://smoothieware.org/3d-printer-guide#toc9](http://smoothieware.org/3d-printer-guide#toc9)


---
**Sebastian Szafran** *July 16, 2016 20:40*

**+Michael Forte**​ The link above (thanks **+Robert Burns**​) will open this page: [http://smoothieware.org/general-appendixes#external-drivers](http://smoothieware.org/general-appendixes#external-drivers) where you will easily all necessary information. 


---
**Arthur Wolf** *July 16, 2016 21:41*

**+Michael Forte** Yes, Smoothieboard's on-board driver's maximum current rating is 2A.



About wiring external drivers, it is supported, easy, and extremely common. It's fully documented here : [http://smoothieware.org/cnc-mill-guide#toc31](http://smoothieware.org/cnc-mill-guide#toc31)



Cheers.


---
**Michael Forte** *July 17, 2016 04:45*

Thanks for the replies everyone! After reading all of the links you sent, I believe I understand how to wire it up. Now the question is what motor drivers can I get for a reasonable price. I would hate to pay so much for 4 drivers that I could just buy a different board with sufficiently strong drivers already built in. I really want to use my Smoothieboard though.

I just looked at my NEMA 23 and it says it's a 2.80 A motor. I saw people using TinyG but that only goes to 2.5A. Do you have any recommendations on motor drivers that can comfortably handle this load? The ones I'm seeing on AliExpress run about $30 each! Here's a link to one of them. 

[http://bit.ly/29GxaOg](http://bit.ly/29GxaOg)

If this is overkill just tell me where my thinking is off.


---
**Arthur Wolf** *July 17, 2016 07:13*

**+Michael Forte** If the $30 ones are too much, you can take a look at TB6600


---
**Michael Forte** *July 17, 2016 19:03*

**+Arthur Wolf** The price on those are definitely better! I noticed they go up to 32 microsteps vs. the higher price point going up to 128. Does that make a difference for a CNC? Is 32 sufficient or even 16? What is the minimum I should look for?

Sorry for all the newbie questions here.


---
**Arthur Wolf** *July 17, 2016 19:34*

It's going to depend on the machine and what you do with it. For an OX, 32 is likely plenty enough.


---
**Michael Forte** *July 17, 2016 21:47*

Thanks. It is for an OX to do some wood carvings and aluminum plate cutting.


---
**Daniel Benoit** *August 04, 2016 19:03*

Hi, I've done just what you want to do. I bought some HY-DIV268N-5A drivers from Aliexpress and Ebay.  Two of the four I bought from Aliexpress were Dead on arrival. The ones I bought off Amazon were all good. I soldered the headers on the board and used four wire connectors to plug the drivers on the board and drive nema 23's for a 3D printer. Works great. 


---
**Michael Forte** *August 24, 2016 22:40*

**+Daniel Benoit** Where did you mount the drivers? I'm thinking the x-axis would be mounted directly on the gantry and the 2 y's would be on the plates so they can move up and back with the motor. Do you have any pictures of your wiring and installation?


---
*Imported from [Google+](https://plus.google.com/+MichaelForte66/posts/JCnuL6JKnce) &mdash; content and formatting may not be reliable*
