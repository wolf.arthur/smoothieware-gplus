---
layout: post
title: "There may be other groups I could ask this in, but this is the only one I know of that may be using this board for the same thing I am"
date: July 17, 2016 01:46
category: "General discussion"
author: "Soup3y gnome"
---
There may be other groups I could ask this in, but this is the only one I know of that may be using this board for the same thing I am.  I am looking to find out about a formula/algorithm for the pt100, specifically what it is.  I am new to this sort of thing, but it is good to add a new wrench to the toolbox every now and then.  If anyone can point me in a good direction to help me it will be appreciated as well.  If it is something that the adult/people with degrees should handle I will drop it and leave it to them, if not I will give it a go and see what I can do.





**"Soup3y gnome"**

---
---
**Arthur Wolf** *July 17, 2016 07:59*

[https://techoverflow.net/blog/2016/01/02/accurate-calculation-of-pt100-pt1000-temperature-from-resistance/](https://techoverflow.net/blog/2016/01/02/accurate-calculation-of-pt100-pt1000-temperature-from-resistance/) ?


---
*Imported from [Google+](https://plus.google.com/103765596183993757536/posts/hAwASgukxna) &mdash; content and formatting may not be reliable*
