---
layout: post
title: "Do I need to solder pin headers to this location to connect to the LLC board for laser control or is this also routed to one of the green screw connectors?"
date: July 18, 2016 00:35
category: "General discussion"
author: "Custom Creations"
---
Do I need to solder pin headers to this location to connect to the LLC board for laser control or is this also routed to one of the green screw connectors? Board is a Smoothieboard 5x.



TIA!﻿

![missing image](https://lh3.googleusercontent.com/-uXJT3RbXRuI/V4wkWZnQKRI/AAAAAAAAdeU/GqVJIDwGflA5Orqtvu3Xrw8ue6Xak9Ktg/s0/16%252B-%252B1.jpeg)



**"Custom Creations"**

---


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/GjjQKvhdimp) &mdash; content and formatting may not be reliable*
