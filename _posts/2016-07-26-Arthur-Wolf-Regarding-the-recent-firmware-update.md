---
layout: post
title: "Arthur Wolf Regarding the recent firmware update..."
date: July 26, 2016 21:59
category: "General discussion"
author: "Zane Baird"
---
**+Arthur Wolf**  Regarding the recent firmware update... Arc g-codes now REQUIRE that the extrusion amount be set to 0 in order to function the same as previously. For instance, the following code tries to extrude/retract at a rate the motor could never reach before executing the arc movements as expected:



;****Arc portion of break in code

G1 X180 Y170 F12000 ;move to center of bed 



;50mm radius, center

G1 Y220 F12000 ;move to arc start

G2 X179.999 Y220 I0 J-50 F12000 ;50 mm radius clockwise arc

G1 X180 F12000 ;return to arc start position

G3 X180.001 Y220 I0 J-50 F12000 ;50mm radius counter-clockwise arc



However, if I set relative extrusion distances and include "E0" before the feedrate specification it works as expected.



Just thought you should be aware.





**"Zane Baird"**

---
---
**Wolfmanjm** *July 26, 2016 23:08*

FWIW G2/G3 never did support extrusion correctly. and it is not currently supported as no slicer I am aware of generates G2/G3. As of now G2/G3 is purely a CNC operation. It will require someone who needs to use G2/G3 for 3D printer operations to figure out how to make this work and provide a pull request. I do not think it would be too hard.


---
**Zane Baird** *July 26, 2016 23:15*

Hmm... good to know. I was just using it for manually written code without any extrusion and it has been working fine until I updated the firmware. Good news is that the S3D issue of stuttering on small segments has completely gone away for me. I don't require it so its not a really problem, but it sure made for efficient use of print area for a nozzle prime on my delta (haven't updated the firmware on that yet)


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/3XWMCd8epvB) &mdash; content and formatting may not be reliable*
