---
layout: post
title: "Has anyone tried an SSD1306 OLED LCD ( ) with smoothie?"
date: July 28, 2016 22:02
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Has anyone tried an SSD1306 OLED LCD ([http://s.aliexpress.com/QNzQRnaQ](http://s.aliexpress.com/QNzQRnaQ)) with smoothie? This is SPI not I2C, and according to smoothie documentation some SPI panels are supported but this particular driver is not listed. If there is an incompatibility, any thoughts on what needs to get solved to add support? 





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Wolfmanjm** *July 29, 2016 05:02*

it is almost identical to the ST7565 that is supported, there are just a couple of initialization bytes that need changing. I have a patch somewhere that is supposed to work, but never bothered to merge it.


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2016 21:00*

I have such a display on the way, if you can point me in the right direction or pull up that patch, I can test. 


---
**Ray Kholodovsky (Cohesion3D)** *October 26, 2016 16:33*

**+Peter van der Walt** I have the display I ordered sitting on my bench. If you or **+Wolfmanjm** advise what needs to be changed in the code I can test. 


---
**Wolfmanjm** *October 26, 2016 21:05*

I know someone who has it working it was like two lines of changes to the existing code. But I need to do some searching to find which lines were changed.


---
**Wolfmanjm** *October 26, 2016 22:39*

Ok I'm adding it to the panel as an variant on the ST7565


---
**Wolfmanjm** *October 26, 2016 22:42*

please test this branch [https://github.com/wolfmanjm/Smoothie/tree/add/ssd1306-oled](https://github.com/wolfmanjm/Smoothie/tree/add/ssd1306-oled)

set panel.lcd ssd1306_oled


---
**Wolfmanjm** *October 26, 2016 22:43*

contrast may need some tweaking.

and reverse is not supported.


---
**Wolfmanjm** *October 26, 2016 23:08*

Someone else will need to do that the only reason I did this was someone sent me their changes a while back and it was on my todo list to integrate their changes.


---
**Ray Kholodovsky (Cohesion3D)** *October 26, 2016 23:51*

Was able to compile the firmware branch. 

No activity on the LCD though. 

I will have to get a known arduino sketch going to verify it works first, as I have not used it yet. 



We are trying to get 3 pin SPI working, which is how this module is configured. (R3 and R4 jumpers installed)



The pins on the LCD header are gnd, vdd, sck, sda, res, dc, and CS. 



I have mine hooked up:

Gnd-gnd

Vdd-3.3v

Sck-Sck1

Sda-Mosi1

CS-Ssel1



I have tried having res(reset) disconnected and when that did not work I tried grounding it, then holding down smoothie reset, unground, release reset, and still nothing. ![missing image](https://lh3.googleusercontent.com/akLn2WtlBMO_DadRS27vOfwCf65MZsxQvIaGFjra7hleKwnMg0gqaI2D_LuEToafGn9wa9XXxSzYYM0=s0)


---
**Wolfmanjm** *October 27, 2016 00:20*

**+Ray Kholodovsky** AFAIK the reset must be connected and configured in smoothie. without it the thing will not initialize.


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2016 00:40*

**+Wolfmanjm**this is the only reset reference I found in the code: [https://github.com/wolfmanjm/Smoothie/blob/add/ssd1306-oled/src/modules/utils/panel/panels/ST7565.cpp#L99](https://github.com/wolfmanjm/Smoothie/blob/add/ssd1306-oled/src/modules/utils/panel/panels/ST7565.cpp#L99)



Can you advise if this is what you are referring to, and if so what the config line should be? 

[github.com - Smoothie](https://github.com/wolfmanjm/Smoothie/blob/add/ssd1306-oled/src/modules/utils/panel/panels/ST7565.cpp#L99)


---
**Wolfmanjm** *October 27, 2016 01:38*

yes you need to hook up a pin to the reset on the oled. Then configure it in config with...  panel.rst_pin   2.13  # or whatever pin you use

Note rst is high normal then brought low to reset then high, I think that works for the oled. if not you can invert the pin in the config.


---
**Wolfmanjm** *October 27, 2016 01:40*

looks like you do not need an a0 pin so set that to nc




---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2016 01:41*

**+Wolfmanjm** will there be a problem if a0 is not in the config at all? If that is a problem can you advise on the config line for that as well please? 


---
**Wolfmanjm** *October 27, 2016 01:44*

yea you need to explicitly set it to nc so



panel.a0_pin  nc # set to not connected


---
**Wolfmanjm** *October 27, 2016 23:47*

**+Ray Kholodovsky** did you get it working?


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2016 23:52*

Sry I forgot to reply back. No, that didn't help. There's no activity on the screen at all.  I'm gonna try to get an arduino set up to test it before fiddling further, to make sure my screen works. 


---
**Wolfmanjm** *October 28, 2016 00:11*

ok I have a 1306 chinese clone display someone gave me, I'll hook it up and test it. I've seen it working, but maybe I made an error in copying the setup sequence.


---
**Wolfmanjm** *October 28, 2016 02:30*

**+Ray Kholodovsky** what is the pinout on your display, there are several different types.. one has no reset like this one, and you need to use a0_pin...

[http://www.hotmcu.com/ssd1306-096-128%C3%9764-oled-display-%E2%80%93-i2cspi-interface-p-144.html](http://www.hotmcu.com/ssd1306-096-128%C3%9764-oled-display-%E2%80%93-i2cspi-interface-p-144.html)


---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2016 03:40*

**+Wolfmanjm** **+Peter van der Walt** **+Samer Najia** it be working. ![missing image](https://lh3.googleusercontent.com/WO5hMgtlph90stvw__sghO4BF65RAi7gnbBD_7OWQiM6s5VMakn9XO6z6A6jDsU2qy3Nz_jzhLJma50=s0)


---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2016 03:48*

I placed a DP order just a few hours ago. I may need to make some additions/ revisions to it now :) 



If you'd answer me on hangouts I'll gladly walk you through the wiring on this. 


---
**Wolfmanjm** *October 28, 2016 03:56*

ok so mine is an odd one it has no CS but does have D/C and reset, where A0 is connected to D/C.


---
**Wolfmanjm** *October 28, 2016 03:58*

you need to be using the firmware-cnc.bin BTW if this is for a laser.


---
**Wolfmanjm** *October 28, 2016 03:58*

you will also need to add at least an encoder with a click button on it.


---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2016 04:02*

I think Peter and I will both be working on that immediately.


---
**Samer Najia** *October 28, 2016 10:07*

Documentation as to how to make it work with your Remix pls **+Ray Kholodovsky**​


---
**Samer Najia** *October 28, 2016 10:19*

Fantastic.  I can buy a couple of these in 1-2 weeks to help it along


---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2016 16:16*

Remixing! I'll walk you thru it later **+Samer Najia** ![missing image](https://lh3.googleusercontent.com/bh5GBFhpQm8LKFyrfJMIWhpiMEVMtACQ-UHSgNkfMrVUZZOEs4n2TgKWnZcnHIxxM9pszgtnmKi9cJ0=s0)


---
**Samer Najia** *October 28, 2016 16:18*

Roger.  I can put it on the SD3


---
**Richard Kalaf** *July 27, 2017 03:59*

Hello Guys, so how did this go? I´m trying to get that display to work  myself. I have exactly the same one as **+Ray Kholodovsky** the wiki instructions mentions that it´s supported but doesn´t tell much more on how to connect and configure it.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/bFSjtEa8qbx) &mdash; content and formatting may not be reliable*
