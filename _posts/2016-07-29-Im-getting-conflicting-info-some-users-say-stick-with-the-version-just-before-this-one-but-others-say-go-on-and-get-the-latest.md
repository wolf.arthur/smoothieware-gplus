---
layout: post
title: "I'm getting conflicting info.. some users say stick with the version just before this one, but others say go on and get the latest"
date: July 29, 2016 13:10
category: "General discussion"
author: "Jon Bruno"
---
I'm getting conflicting info.. some users say stick with the version just before this one, but others say go on and get the latest.

I'm a Laser user so I thought this module may be required for my happiness.



Has this been resolved and if so should I then be able to use the latest?



I just checked and the line is still commented but since I have no idea why I thought I should ask.



[https://github.com/Smoothieware/Smoothieware/commit/1843a68fdcdc15e793bd34ee9ede0c28ca414e0e](https://github.com/Smoothieware/Smoothieware/commit/1843a68fdcdc15e793bd34ee9ede0c28ca414e0e)



Thanks for everything you guys do!





**"Jon Bruno"**

---
---
**Wolfmanjm** *July 29, 2016 20:34*

laser is not currently supported in edge. so use master. **+Arthur Wolf** needs to work on getting laser working in edge.


---
**Arthur Wolf** *July 31, 2016 08:18*

**+Jon Bruno** Hey. **+Wolfmanjm** just wrote some code for this ( thanks ! ), I'll try to test it as soon as possible.


---
**Jon Bruno** *July 31, 2016 13:08*

Thanks for the update. (thumbs up)


---
**Wolfmanjm** *July 31, 2016 18:07*

[https://github.com/Smoothieware/Smoothieware/tree/add/new-laser](https://github.com/Smoothieware/Smoothieware/tree/add/new-laser)


---
*Imported from [Google+](https://plus.google.com/105850439698187626520/posts/HW4jiGbzY6p) &mdash; content and formatting may not be reliable*
