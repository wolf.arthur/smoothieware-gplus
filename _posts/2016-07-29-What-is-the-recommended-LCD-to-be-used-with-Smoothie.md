---
layout: post
title: "What is the recommended LCD to be used with Smoothie?"
date: July 29, 2016 18:55
category: "General discussion"
author: "Sebastian Szafran"
---
What is the recommended LCD to be used with Smoothie?



Currently I have:

- RepRap style LCDs with encoders and two EXP1 and EXP2 connectors

- 16x2 LCD or 20x4 LCD with the option to use I2C to connect to Arduino and then SPI between Arduino and Smoothie

- or should I get something else than that?





**"Sebastian Szafran"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2016 19:01*

The Graphic LCD 12864, reprap style with the 2 EXP connectors. 

Other options possible but not friendly. 


---
**Sebastian Szafran** *July 29, 2016 19:11*

Thx **+Ray Kholodovsky**​, do I need an adapter or is it possible to connect the LCD directly  to EXP ports and adapt config.txt?


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2016 19:16*

Smoothieboard - yes need adapter. 

Brainz and my Remix, connect directly to EXP1 and EXP2 on the boards.  Config is stock, just need to uncomment the first glcd section.  Make sure to power externally not over USB.  If any issues (not seeing activity on LCD) try unplugging EXP2 and try again.  SD card circuit has been problematic once or twice.


---
**Hakan Evirgen** *July 29, 2016 22:36*

Viki2 is an option.


---
**Alex Krause** *July 30, 2016 03:26*

This is in the smoothie section at uberclock: [http://shop.uberclock.com/collections/smoothie/products/smoothieboard-glcd-shield](http://shop.uberclock.com/collections/smoothie/products/smoothieboard-glcd-shield)


---
**Alex Krause** *July 30, 2016 03:28*

Robotseed also has one in there store


---
**Jeff DeMaagd** *July 30, 2016 19:07*

The legacy character LCDs don't have the correct signaling. SPI signaling is the default unless you make an adapter with a chip that interprets SPI to Hitachi style signaling. I don't know if anyone made adapter boards for the converter chip and cabling.


---
**Arthur Wolf** *July 31, 2016 08:38*

This is the official documentation on the subject : [http://smoothieware.org/panel](http://smoothieware.org/panel)


---
**Sebastian Szafran** *July 31, 2016 19:13*

Than for all comments, my LCD is working fine, more details here: [https://plus.google.com/+SebastianSzafran/posts/6LbeNBqjowX](https://plus.google.com/+SebastianSzafran/posts/6LbeNBqjowX)


---
*Imported from [Google+](https://plus.google.com/+SebastianSzafran/posts/bmaTMpCRSDZ) &mdash; content and formatting may not be reliable*
