---
layout: post
title: "The LCD Panel is working almost out of the box - well done Smoothie"
date: July 30, 2016 18:40
category: "General discussion"
author: "Sebastian Szafran"
---
The LCD Panel is working almost out of the box - well done Smoothie. Surprisingly yesterday before asking the community (thanks **+Ray Kholodovsky**​, **+Hakan Evirgen**​ and **+Alex Krause**​) I connected EXT1 and EXT2 the same way as today, but got only some strange pixels displayed. Today it simply worked OK. 



Configuration changes:

- set: panel.enable true

- uncomment panel-related lines

- reversing panel.encoder_a_pin and panel.encoder_b_pin values, as encoder was increasing values anticlockwise

- adding panel.encoder_resolution 4 to allow moving by one line per encoder detent

- (optional) commenting out #panel.buzz_pin to disable buzzer 



Current panel related config:

## Panel

#Uncomment panel related lines and set panel.enable to true (originally false)

panel.enable                                 true             # set to true to enable the panel code

# Example for reprap discount GLCD

# on glcd EXP1 is to left and EXP2 is to right, pin 1 is bottom left, pin 2 is top left etc.

# +5v is EXP1 pin 10, Gnd is EXP1 pin 9

panel.lcd                                   reprap_discount_glcd

panel.spi_channel                           0                 # spi channel to use  ; GLCD EXP1 Pins 3,5 (MOSI, SCLK)

panel.spi_cs_pin                            0.16              # spi chip select     ; GLCD EXP1 Pin 4

# panel.encoder_a_pin has been originally set to 3.25!^            # encoder pin         ; GLCD EXP2 Pin 3

panel.encoder_a_pin                         3.26!^            # encoder pin         ; GLCD EXP2 Pin 5

# panel.encoder_b_pin has been originally set to 3.26!^            # encoder pin         ; GLCD EXP2 Pin 5

panel.encoder_b_pin                         3.25!^            # encoder pin         ; GLCD EXP2 Pin 3

#added panel.encoder_resolution with value of 4

panel.encoder_resolution                    4

panel.click_button_pin                      1.30!^            # click button        ; GLCD EXP1 Pin 2

#commented panel.buzz_pin to disable buzzer

#panel.buzz_pin                              1.31              # pin for buzzer      ; GLCD EXP1 Pin 1

panel.back_button_pin                       2.11!^            # back button         ; GLCD EXP2 Pin 8

![missing image](https://lh3.googleusercontent.com/-2g8AFUx0OdY/V5z0lcUbhNI/AAAAAAAAPYs/tD1pjHJ3jkQHR-6zO7DaViJw4661lkO2g/s0/16%252B-%252B1.jpeg)



**"Sebastian Szafran"**

---
---
**Samer Najia** *July 30, 2016 19:33*

Another thing I learned....if the base and secondary board get pressed together i.e. when screwed on too tight to something, you get unpredictable outcomes.  Slide a piece of Kapton between them to stop that.


---
**Maxime Favre** *July 31, 2016 13:18*

Thanks for sharing!


---
*Imported from [Google+](https://plus.google.com/+SebastianSzafran/posts/6LbeNBqjowX) &mdash; content and formatting may not be reliable*
