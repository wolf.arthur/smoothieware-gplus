---
layout: post
title: "How do I slave 2 external stepper drivers together?"
date: July 31, 2016 14:51
category: "General discussion"
author: "Anthony Bolgar"
---
How do I slave 2 external stepper drivers together? I have 2 Y axis steppers, and they are running on external stepper drivers. How would I configure them to act as a single axis?





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *July 31, 2016 15:01*

OK, figured it out, just run them in parallel.


---
**Sébastien Plante** *July 31, 2016 16:35*

Just be aware that in parallel, you'll double the current need.



This is one way of doing it

[http://reprap.org/mediawiki/images/b/b1/Reprappro-huxley-z-motor-wiring.png](http://reprap.org/mediawiki/images/b/b1/Reprappro-huxley-z-motor-wiring.png)


---
**Anthony Bolgar** *July 31, 2016 16:37*

I have a driver for each Y axis stepper.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/cHXRAptW9Qe) &mdash; content and formatting may not be reliable*
