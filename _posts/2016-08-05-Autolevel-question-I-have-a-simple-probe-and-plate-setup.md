---
layout: post
title: "Autolevel question: I have a simple probe and plate setup"
date: August 05, 2016 23:25
category: "General discussion"
author: "Phil Aldrich"
---
Autolevel question:  I have a simple probe and plate setup.  I have configured the config file to set pin (1.28!^) and enabled probing.  When I issue a M119 command to test, everything seems to look correct (Probe: 0 when no contact and Probe: 1 when there is contact).  When I run a G30 command, the probe goes down and contacts the plate, retracts and reports the correct Z number.  All seems well at this point.  When I try to run bCNC's autolevel routine (simple 4 corner level) it does the first probe and gets a reading and then, upon retracting, generates an error of "ZProbe triggered before move, aborting command." and continues to go to the other 3 corners without probing.  This appears to be generated from the smoothie controller and not the bCNC software.  Am I missing something?  Could there be noise on the input line?  I also tried setting the zprobe.debounce_count to 100, 1000, 10000 but it didn't seem to help.  I am running build version edge-3332442 Apr. 22 2016 of the smoothie firmware.





**"Phil Aldrich"**

---
---
**Wolfmanjm** *August 06, 2016 05:13*

use the builtin three point levelling. I doubt the bCNC one will work with such an old version of smoothie. or you could try the latest edge and use the cnc build version.


---
**Phil Aldrich** *August 06, 2016 12:00*

I'll give it a try. Does the CNC version also support lasers?


---
**Wolfmanjm** *August 06, 2016 16:33*

not yet


---
*Imported from [Google+](https://plus.google.com/105080379699420524192/posts/BRf4PqsjWiK) &mdash; content and formatting may not be reliable*
