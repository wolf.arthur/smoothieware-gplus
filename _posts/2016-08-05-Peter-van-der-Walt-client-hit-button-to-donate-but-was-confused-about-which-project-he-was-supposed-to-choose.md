---
layout: post
title: "Peter van der Walt, client hit button to donate but was confused about which project he was supposed to choose"
date: August 05, 2016 22:14
category: "General discussion"
author: "Steve Anken"
---
Peter van der Walt, client hit button to donate but was confused about which project he was supposed to choose. Posted here because I can't post to Laserweb group any more and see no way for him to donate directly to you. :-(





**"Steve Anken"**

---
---
**Ariel Yahni (UniKpty)** *August 05, 2016 22:28*

You need to use private messages. Here is the PayPal.me for **+Peter van der Walt**​ [https://www.paypal.com/cgi-bin/wapapp?cmd=_s-xclick&hosted_button_id=45DXEXK9LJSWU](https://www.paypal.com/cgi-bin/wapapp?cmd=_s-xclick&hosted_button_id=45DXEXK9LJSWU)


---
**Steve Anken** *August 05, 2016 22:54*

Cool, I'll call and tell them. Thanks.


---
*Imported from [Google+](https://plus.google.com/109943375272933711160/posts/HgfuwctxXiW) &mdash; content and formatting may not be reliable*
