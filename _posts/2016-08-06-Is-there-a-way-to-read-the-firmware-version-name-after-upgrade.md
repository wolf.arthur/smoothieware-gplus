---
layout: post
title: "Is there a way to read the firmware version/name after upgrade?"
date: August 06, 2016 13:33
category: "General discussion"
author: "Ariel Yahni (UniKpty)"
---
Is there a way to read the firmware version/name after upgrade? 





**"Ariel Yahni (UniKpty)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 06, 2016 13:47*

In general, when you connect to the board from a terminal, it will spit out all the info including the version #. 


---
**Arthur Wolf** *August 06, 2016 13:51*

the "version" command ( "@version" if using pronterface )


---
**Ariel Yahni (UniKpty)** *August 06, 2016 14:17*

Thanks guys


---
**Phil Aldrich** *August 06, 2016 16:02*

I was wondering that myself. Thanks


---
**Arthur Wolf** *August 06, 2016 16:03*

You guys know we have a documentation right ? :)


---
**Phil Aldrich** *August 06, 2016 16:08*

Yes but sometimes it's a bit overwhelming to find what you are looking for when "in the heat of battle". 


---
**Arthur Wolf** *August 06, 2016 16:09*

**+Phil Aldrich** Well in this case, it was documented here : [http://smoothieware.org/console-commands](http://smoothieware.org/console-commands) . But yes I agree : the more you document, the more there is to search through.


---
**Phil Aldrich** *August 06, 2016 16:38*

Related to this, it seems that there are many "flavors" of the firmware (CNC vs laser) and I am guessing also versions within each flavor. Is there an easy way to tell the versions numbers/series of each type on Github?  I'm still somewhat new to all of this but learning. Thanks


---
**Arthur Wolf** *August 06, 2016 16:40*

There are no flavors. There is "normal" Smoothie that can run CNCs, lasers and 3D printers, and there is a special "CNC" build that has a few special CNC things enabled ( I think a bit different panel, maybe grbl mode enabled by default etc ).


---
**Phil Aldrich** *August 06, 2016 17:44*

**+Arthur Wolf** Thanks for the info.  I was a bit apprehensive to flash the firmware thinking I might end up losing capability.  I use my machine for both laser and CNC jobs. 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/FrEubpPJYaF) &mdash; content and formatting may not be reliable*
