---
layout: post
title: "Quick question, hope it's not a dumb one :) does smoothie treat a change in spindle speed like most cnc controllers do?"
date: August 09, 2016 20:00
category: "General discussion"
author: "Alex Krause"
---
Quick question, hope it's not a dumb one :) does smoothie treat a change in spindle speed like most cnc controllers do? By this I mean is there a delay in motion to allow the spindle a change in acceleration? The reason I question this is for laser purposes where the change in power from off to on is nearly instantaneous not needing to overcome an inertial load like a spindle does





**"Alex Krause"**

---
---
**Wolfmanjm** *August 09, 2016 20:02*

there is no delay when it encounters an Sxxx but the Sxxx must be on a lin ewith G1 G2 or G3




---
**Wolfmanjm** *August 09, 2016 20:03*

Sxxx on a line by itself would be ignored for laser


---
**Alex Krause** *August 09, 2016 20:08*

So if there is a G0 line with S0. Preceding a line with say s0.01 will there be a delay in the motion? Example: 

G0 x53.5 S0.

G1 x53.2 S0.01 f28000


---
**Alex Krause** *August 09, 2016 20:18*

If it helps to know I have a single pin laser setup M3 and M5 commands aren't present 


---
**Wolfmanjm** *August 09, 2016 20:26*

The S0 on the G0 would be ignored as all G0 commands turn off the laser. the power for the G1 would be set to 0.01, and there would be no delay other than waiting for G0 X53.5 to execute. (FYI all code should be upper case)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/eHEnWgErkuj) &mdash; content and formatting may not be reliable*
