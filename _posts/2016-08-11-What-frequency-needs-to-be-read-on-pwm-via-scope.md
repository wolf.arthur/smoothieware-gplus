---
layout: post
title: "What frequency needs to be read on pwm via scope?"
date: August 11, 2016 21:52
category: "General discussion"
author: "Ariel Yahni (UniKpty)"
---
What frequency needs to be read on pwm via scope? ﻿





**"Ariel Yahni (UniKpty)"**

---
---
**Arthur Wolf** *August 11, 2016 22:01*

Which PWM ? For laser, the default period is 20us which would be 50khz


---
**Triffid Hunter** *August 12, 2016 07:48*

Laser PWM should be 50khz, however regular PWM for fans and heaters actually uses sigma delta modulation with a default base frequency of 1khz, so transitions will always be a multiple of 1ms apart, with no discernible "frequency" except for specific modulation values


---
**Wolfmanjm** *August 14, 2016 04:58*

**+Peter van der Walt** **+Ariel Yahni**

Ok I'd like to finally put this to bed.... I checked the PWM and step pulses with a logic analyzer, I can confirm the PWM frequency does not change it is a constant 50KHz until it reaches full power then it is full on and there is no frequency.The PWM duty cycle starts at a few percent as it accelerates, then the duty cycle increases following the acceleration curve when it reaches the plateau speed the PWM is on all the time 100% duty cycle, until it starts to decelerate then I see a relatively smooth decrease in duty cycle on the PWM until it turns off completely. This is exactly what I expected to see ;)


---
**Wolfmanjm** *August 14, 2016 05:33*

I also tested G1 X10 F1000 S0.5 and sure enough once it is at cruising speed the duty cycle is 50%, at 50Khz


---
**Wolfmanjm** *August 14, 2016 05:38*

yes


---
**Domenic Di Giorgio** *August 14, 2016 07:23*

Hi Everyone.

Sorry I've been MIA lately. Just working through the last design aspects of our machine and ended up damaging our laser driver prototype. 



We were getting very unusual results originally which is why we asked Wolfmanjm about it. Reverting to stable build as of a few weeks back, the results on the scope were correct. 



We will test the edge build and confirm the results wolfmanjm reported.



Thanks everyone.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/Rga449mf7BG) &mdash; content and formatting may not be reliable*
