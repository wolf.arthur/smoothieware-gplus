---
layout: post
title: "Thanks Guys for all the awesome work with the Smoothie project..."
date: August 12, 2016 23:36
category: "General discussion"
author: "Alex Krause"
---
Thanks Guys for all the awesome work with the Smoothie project... really enjoying playing with my Smoothiefied K40 using Laserweb

![missing image](https://lh3.googleusercontent.com/-6basAgAaSR0/V65dbnkFfdI/AAAAAAAAQl4/z_JHsnAcMsg0vYg8UUnwdOaxNYEQUsXTACJoC/s0/16%252B-%252B1.jpeg)



**"Alex Krause"**

---
---
**Arthur Wolf** *August 14, 2016 09:25*

Glad you like it :) Really nice engrave.


---
**cory brown** *August 14, 2016 19:37*

Alex, have you noticed that when you do a engrave like that, that on the edges where the laser head is decelerating, the engrave is not as deep kinda bowl shaped? And you can't get really sharp edges?


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/UiCfjUhuUB2) &mdash; content and formatting may not be reliable*
