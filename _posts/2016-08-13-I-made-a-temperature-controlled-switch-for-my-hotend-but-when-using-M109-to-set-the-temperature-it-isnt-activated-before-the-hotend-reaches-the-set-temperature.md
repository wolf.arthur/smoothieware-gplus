---
layout: post
title: "I made a temperature controlled switch for my hotend, but when using M109 to set the temperature, it isn't activated before the hotend reaches the set temperature"
date: August 13, 2016 09:25
category: "General discussion"
author: "Michael Andresen"
---
I made a temperature controlled switch for my hotend, but when using M109 to set the temperature, it isn't activated before the hotend reaches the set temperature. With M104 it triggers the switch when it passes the temperature set for the switch in the config.



The switch is set up like this [http://pastebin.com/939FRDxA](http://pastebin.com/939FRDxA) are there something that should be different, or is the M109 totally blocking anything else from running?





**"Michael Andresen"**

---
---
**Michael Andresen** *August 21, 2016 16:02*

I guess the solution is to add a sub command to the fan command, to turn the fan on, then send the M109 right after, which then will keep it from being able to turn it off again. 


---
*Imported from [Google+](https://plus.google.com/+MichaelAndresen/posts/HaGfBrQv5gw) &mdash; content and formatting may not be reliable*
