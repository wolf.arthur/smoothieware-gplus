---
layout: post
title: "Was hoping someone could shed some light on my results..."
date: August 15, 2016 03:43
category: "General discussion"
author: "Alex Krause"
---
Was hoping someone could shed some light on my results... testing out the new Edge branch on my laser and I've noticed some things that make it hard for me to do raster engraves with and it deals with changes in acceleration and direction of movement while the laser fires pwm... I have adjusted acceleration settings in my config file along with Pwm period and junction deviation... in the software I have made changes to engraving speed and max output power... even after all the changes made I still notice more power being transfered to the material and burning noticeable lines at the change in acceleration. Please note I have done some very successful rasters with the Master build and don't notice these issues... can someone please help me figure out what I am doing wrong.attached are the photos of some of the simple grayscale rasters I have completed with the edge branch. Thanks in advance :) 



![missing image](https://lh3.googleusercontent.com/-Lh1Z3x1p__Y/V7E6asb1r4I/AAAAAAAAEfU/n3UkqOdAlLMacsfxGhxXtBVl29TH33SXw/s0/16%252B-%252B1.jpeg)
![missing image](https://lh3.googleusercontent.com/-q3a_cYvQ-fg/V7E6asF4NAI/AAAAAAAAEfU/K47AWZufUUIOnUh2qhxjYkg9x_FOTpCzg/s0/16%252B-%252B2.jpeg)
![missing image](https://lh3.googleusercontent.com/-wnPBdAkXGyQ/V7E6ao2Mw6I/AAAAAAAAEfU/B1vxvF3LKj8OPhGd8hPulM1jYrtSgSE9Q/s0/16%252B-%252B3.jpeg)
![missing image](https://lh3.googleusercontent.com/-rFVSrvTJIyA/V7E6auGB2II/AAAAAAAAEfU/pgkmM9xeFIAJvlQoajFCWS4vkwQi1D_2Q/s0/16%252B-%252B4.jpeg)
![missing image](https://lh3.googleusercontent.com/-99SvKRBi9og/V7E6aqYRO1I/AAAAAAAAEfU/Q-Bc6sL3gqkPHsC_AZrd3mL2pe5Qz22cg/s0/16%252B-%252B5.jpeg)
![missing image](https://lh3.googleusercontent.com/-9PgqGqe8dX8/V7E6atiVz5I/AAAAAAAAEfU/IMm3_qkPZBAqjIDWBoUIClQh3PxCLhzdQ/s0/16%252B-%252B6.jpeg)

**"Alex Krause"**

---
---
**Wolfmanjm** *August 15, 2016 06:44*

The main difference between master and edge is that in edge acceleration, happens every step whereas in master it happens every 1ms. However in both branches  the laser power is only updated every millisecond. Make sure your speeds are slow enough that 1 millisecond is not a significant movement. you could also try increasing the acceleration. Junction deviation will not make any difference and you should leave that at 0.05.

I am not sure what I am looking at here either can you explain? is it comin gon too strong at the start? what direction is the raster going? (eg from left to right then down).


---
**Alex Krause** *August 15, 2016 06:54*

**+Wolfmanjm**​ for some reason my camera glitched out when I took the pictures it's all suppose to be left to right movement the darker is on the left moving to light on the right. Notice the dark black lines where the gantry is switching directions... they are cut twice as deep as the rest of the engraves in that box it happens durring changes in acceleration... which makes it very difficult to do image engraves with grayscale tones because dark tones reach full power before the laser head moves


---
**Arthur Wolf** *August 15, 2016 08:33*

Maybe **+Peter van der Walt** could add something to laserweb that'd make acceleration not important ( no matter the firmware btw ) : one or two millimeters ( configurable ) of "non-engraving" movement left and right of each engraving line. That way it's always engraving at full speed.


---
**Arthur Wolf** *August 15, 2016 08:50*

It's the only place we are not at full speed, in theory.

It'd be great that engraving always happens at full speed, since it's possible that it could be very difficult to get the accel/laser-power proportional thing smoothie does to work perfectly for very fine engraving.

If we manage to make sure engraving happens at full speed, and there is still a problem, then that's a problem we can adress on it's own.


---
**Arthur Wolf** *August 15, 2016 08:59*

It's possible we got it right only by accident. Or this is a different problem. Or indeed we used to get it right. We need more data to figure it out, constant-speed will help us get there :)


---
**Arthur Wolf** *August 15, 2016 09:03*

**+Peter van der Walt** It's possible we got it wrong before, get it more right now ( the code would clearly hint at that ), but there was some kind of setting/tuning that made it work before, that doesn't work now that it changed. We've seen this sort of effect happen already with this sort of code improvement.


---
**Wolfmanjm** *August 15, 2016 21:36*

**+Alex Krause** I still don't know what i am looking at, can you post the gcode with explanations?


---
**Wolfmanjm** *August 15, 2016 22:08*

**+Alex Krause** have you tried increasing your acceleration? see what happens. The thing is that with the new code the acceleration is adhered to a lot closer than it used to, so potentially every step could be at a different speed as it accelerates. but the PWM will only change every millisecond. What PWM frequency do you use?  It could be that I need to increase the frequency that the PWM is changed, however this could produce an overall decrease in performance, but may be worth a try, but it requires a lot of extra coding as I will need to dedicate a timer to that.


---
**Wolfmanjm** *August 15, 2016 22:09*

Maybe adding an option to disable the proportional Power setting would be interesting to try.


---
**Alex Krause** *August 15, 2016 22:18*

**+Wolfmanjm**​ thank-you for all the hard work you have put into this... I will increase acceleration first see if that helps... when you say increase the frequency of pwm are you wanting me to adjust the time in microseconds down to increase the output frequency in Hertz or to increase the overall microseconds ? As I have done both of those things already with varying results... the setting I found to work best with Master was 5khertz/200 microseconds I have tried 50kherts -2k hertz so far 


---
**Wolfmanjm** *August 15, 2016 22:24*

I wasn't suggesting you increase the PWM frequency I just wanted to know what you had it set to.


---
**Wolfmanjm** *August 16, 2016 01:52*

Another issue that occurs to me is that the power is adjusted linearly in proportion to the current speed, and maybe in some cases the power should not be linear but maybe logarithmic or some other relationship, This may explain why it is so dark at the start where it is ramping up as the laser actually needs a quarter power there instead say half.


---
**Wolfmanjm** *September 02, 2016 18:26*

hopefully I'll have a fix for this today, at least it may improve it a bit. I still think there is an issue with linear proportional power. and the source to this image is darker on the edges.


---
**Wolfmanjm** *September 02, 2016 19:26*

please try this firmware... it should work better... [blog.wolfman.com - blog.wolfman.com/files/laser-fixed-1.bin](http://blog.wolfman.com/files/laser-fixed-1.bin)


---
**Alex Krause** *September 03, 2016 02:58*

**+Wolfmanjm**​ thanks brother :) will give it a spin


---
**Alex Krause** *September 03, 2016 04:04*

G28 isn't working correctly for me **+Wolfmanjm**​. And it looks like my pwm power is all the same power output but I will fiddle with my config file a bit to see if I have made a mistake


---
**Wolfmanjm** *September 03, 2016 04:31*

**+Alex Krause** did you read this [https://github.com/Smoothieware/Smoothieware/blob/edge/upgrade-notes.md](https://github.com/Smoothieware/Smoothieware/blob/edge/upgrade-notes.md)


---
**Wolfmanjm** *September 03, 2016 04:38*

one quick way to test is set acceleration to 10 (M204 S10) then you can see it getting brighter and dimmer at start and end of lines. I did make a last second change but I don't think I broke anything.. but I

ll check a soon as my wheezy to jessie update is finished :)


---
**Wolfmanjm** *September 03, 2016 04:47*

seems to work here ok I see it getting dimmer at the end and ramping up at the start


---
**Alex Krause** *September 03, 2016 05:11*

**+Wolfmanjm**​ I'm using code from laserweb are you typing in commands from pronterface or some other interface? I have defined the homing procedure per the readme and still unable to get homing G28 to work properly... I have to give up testing for the evening I have BBQ in the smoker I am feeding 10 people  at work tomorrow for our last 13 hour shift before the holiday. I will try to pick up where I left off tomorrow assuming that my ole lady will let me


---
**Wolfmanjm** *September 03, 2016 05:21*

I connect direct to smoothie, or use bCNC laserweb does not run on that version of debian I have, which is why i am updating it. not sure if laserweb allows commands? anyway post your config somewhere i'll see why homing doesn't work for you


---
**Wolfmanjm** *September 03, 2016 05:24*

oh **+Alex Krause** G28 is not home on a cnc build, it is $H or G28.2, G28 is goto predefined park position. The binary I put up is a CNC build which is what laser users should be using. You were probably using the 3d printer build if G28 was homing for you before.


---
**Alex Krause** *September 05, 2016 02:16*

**+Wolfmanjm**​ just completed a successful test. 

![missing image](https://lh3.googleusercontent.com/CuPma2bFF6PtLaLD18iaj1ck06dqm6V6_diYPObg_McaUeh-Y7jODOLFaSrcTh6ldh7u7hCKlXkfe0l73FWmSt6SKVyIVaZjuWM=s0)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/QjRFYDsyv3p) &mdash; content and formatting may not be reliable*
