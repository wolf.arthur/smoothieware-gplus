---
layout: post
title: "Wolfmanjm this is the image I use to do grayscale raster tests ...I can post the code in a few hours when I get home"
date: August 15, 2016 21:53
category: "General discussion"
author: "Alex Krause"
---
**+Wolfmanjm**​​ this is the image I use to do grayscale raster tests ...I can post the code in a few hours when I get home. I use this image to give me an approximation of how the material will take the effects of the laser.... scale of 100-10% black﻿

![missing image](https://lh3.googleusercontent.com/-sTylkmlJdU8/V7I56i8bLBI/AAAAAAAAEig/Li5X_qd28pMQyUb7kaYM_B9dL6A-suuZw/s0/16%252B-%252B1.png)



**"Alex Krause"**

---
---
**Sébastien Plante** *August 15, 2016 22:21*

No 0% black? :(


---
**Alex Krause** *August 16, 2016 04:10*

**+Wolfmanjm** here is the link to the gcode file for this raster test [https://drive.google.com/file/d/0B338aPM_B6R1OXZaRVN3MjlBQnM/view?usp=sharing](https://drive.google.com/file/d/0B338aPM_B6R1OXZaRVN3MjlBQnM/view?usp=sharing)


---
**Wolfmanjm** *August 16, 2016 04:34*

**+Alex Krause** Why is there a different F speed on every move? that alone is kind of strange. It means planning will be awkward as no line ever gets up to speed.


---
**Alex Krause** *August 16, 2016 04:35*

This is code generated straight from Laserweb


---
**Alex Krause** *August 16, 2016 04:36*

**+Peter van der Walt**​


---
**Wolfmanjm** *August 16, 2016 04:44*

I think the results reflect exactly what the gcode is telling it to do. look at the first scan line of the first block.... There are a few spots to start that are high power then the rest of the line is lower power then a few more that are different power.

I would expect for what you are trying to do there would be one long line at one power. Looks like laserweb is doing the acceleration adjustment maybe? but getting it wrong, and it would fight with what smoothie is trying to do as well.


---
**Alex Krause** *August 16, 2016 04:49*

I am going to run another line of code this time equalizing the proportional light/dark feed rate and do another gcode dump


---
**Alex Krause** *August 16, 2016 04:52*

[https://drive.google.com/open?id=0B338aPM_B6R1d1p0NEVYejVoeTQ](https://drive.google.com/open?id=0B338aPM_B6R1d1p0NEVYejVoeTQ)

equalized proportional light/dark


---
**Wolfmanjm** *August 16, 2016 05:10*

Ok you are welcome to continue using master, but it is frozen and does not get any bug fixes or new features.


---
**Wolfmanjm** *August 16, 2016 05:13*

G1 X10.100 S0.81 F14256

G1 X10.200 S0.79 F14544

G1 X10.300 S0.68 F15864

G1 X10.400 S0.57 F17172

G1 X20.500 S0.53 F17604

G1 X20.600 S0.55 F17412

G1 X20.700 S0.73 F15252

G1 X20.800 S0.91 F13080

G1 X20.900 S0.82 F14112

G1 X21.000 S0.52 F17784

G1 X21.100 S0.22 F21408

G1 X21.200 S0.13 F22452

G1 X21.300 S0.04 F23484





Ok so IMO the results are exactly what this gcode is telling it to do...



first 0.4mm is higher power (therefore darker) next 10.1mm is at 0.53 power and is therefore lighter then the last few mm is darker again. It was probably just pure fluke that the old firmware didn't reflect this because it was unable to accelerate soon enough. The new motion control is able to do exactly what it is being told to do.

I am sorry you are miffed about this, but smoothie can only do what it is being told to do.


---
**Arthur Wolf** *August 16, 2016 09:17*

**+Peter van der Walt** Master is doing things much less correctly than edge is. And master won't evolve anymore. 



It's pretty obvious here you just got lucky "tuning" things around to a way that got it to work with master, but that's not a sustainable way of doing things.



Edge does laser engraving much closer to perfect than master did, and even more so if compared to 8-bit boards. But this also means that the "hacks" that were used to get it to work with master now cause problems.



Changing speed is a horrible idea. You just got lucky that it worked, but the way ( which is the <b>right</b> way ) planning works, Smoothie will <b>never</b> be exactly the right power output if you keep changing speed around. If it worked before, that was just sheer luck ( and I'm pretty certain once you get it to work correctly, people will see an improvement in engraving quality over what you are doing now, which <b>can not possibly</b> be working correctly, just "close" to it )



You need to go at a constant speed, and change power whenever you need with S. It's very simple, and it will work. 



I want to insist this is not a bug, we are doing things <b>better</b> than before, and we are never going back to doing things worse than they are now. You just need to teach LW to do things better too ( which also happens to be even simpler than what you are doing now ).



Once that is implemented, if there are problems with shades not being the right ones, it simply means power is not linear ( which is to be expected ), and LW will need to implement a simpler adjustment/config for that. 

It's very possible the «variable speed» + «master imperfections» combination just acted as a lucky hacky non-linear filter. That's not the right way to do it though, and it's not sustainable.



We moved forward, you need to do so too, it's what all the LW and Smoothie users want.



In the long term, if we both move forward with this, it <b>will</b> mean more consistent precise and beautiful engraving for everyone.


---
**Wolfmanjm** *August 16, 2016 19:05*

**+Alex Krause** equalizing the speed is not the problem (although won't actually hurt) The issue is pretty clear the power is higher on the first 0.3mm and lower for the following 10 or so mm,...



G1 X10.100 S0.81 F12000

G1 X10.200 S0.79 F12000

G1 X10.300 S0.68 F12000

G1 X10.400 S0.57 F12000

G1 X20.500 S0.53 F12000

G1 X20.600 S0.55 F12000



so of course you get a dark band at the start followed by a lighter band.



Why not try the same power for the entire line?

 I presume this is supposed to be a square with equal shade so why is the power varying at the start? I do understand the issue of using power and speed to adjust the shade however that was probably for low resolution PWM (8bit), Smoothie has high resolution PWM (12bit) so that is most likely unnecessary but I am no expert in laser power. However it is clear to even my uneducated eye that the dark band to the left is exactly what the gcode is telling it to do..



Another issue I thought of is that if LW is using power and speed to select a specific shade, then it probably expects the machine to run at the speed given. But this is not the case when you have short segments and high speeds (and potentially low acceleration) you would need to set acceleration insanely high for that to be the case.

for instance in this case..



G1 X21.000 S0.52 F17784

G1 X21.100 S0.22 F21408



You are expecting the machine to accelerate from 17784 to 21408 in 0.1mm which unless you have very high acceleration set will not happen.


---
**Wolfmanjm** *August 20, 2016 07:16*

yea I think you hit the issue on the head :) I was making a test PNG and it had dithered the edges causing odd Gcode. I edited the image with GIMP removed all the edge dithering and ran the result through laserweb3, and got some gcode which makes a lot more sense to me. One long line all at the same power. I'd love to see what this looks like when lazered with the new edge code. It will confirm one way or another if the acceleration PWM stuff is working. [http://blog.wolfman.com/files/laser-test.png](http://blog.wolfman.com/files/laser-test.png)


---
**Wolfmanjm** *August 20, 2016 07:21*

so it would be nice to confirm if **+Alex Krause** image has this dithering issue... can you post a link to the PNG file? or whatever source you load into laserweb.


---
**Alex Krause** *August 20, 2016 12:51*

I won't be able to check the original image till Sunday night when I get off work. **+Wolfmanjm**​ thank-you for continuing to try and figure out if this is an error on my end or a hardware/software issue. What filter did you use in gimp to remove the dither


---
**Wolfmanjm** *August 20, 2016 18:27*

**+Alex Krause** i hand edited the dither out pixel by pixel :) the fill tool helped. There probably is a filter to do it, but I am not really a gimp expert :) however it should be possible to create a test strip like you have that is exactly a  block of the desired color, I'll see if I can do that :) it would make a better baseline test.


---
**Alex Krause** *August 20, 2016 18:28*

I ended up making my own using vectors and filling with paint tool... 


---
**Wolfmanjm** *September 02, 2016 08:22*

Good news... I think I found the bug in the new acceleration stuff that makes the start and end during acceleration higher power. At least it will be the same algorithm as it was in master branch. Although I am not sure it is what should be done. Basically the power should be proportional to the current speed vs the requested speed. this is the issue short segments never reach the requested speed, so will always be lower power than requested. However that is what the master branch did too. Currently in edge the power is proportional to the current speed vs the maximum speed that block can reach, which means even very short segments reach full power, which is obviously wrong. Having a PWM'able laser to test with helped me find this, so thanks to **+Peter van der Walt** for that :) I'll try to fix this tomorrow and get a binary up for testing.


---
**Alex Krause** *September 02, 2016 14:55*

:)


---
**Wolfmanjm** *September 02, 2016 19:22*

please try this firmware... it should work better... [blog.wolfman.com - blog.wolfman.com/files/laser-fixed-1.bin](http://blog.wolfman.com/files/laser-fixed-1.bin)

The proportional acceleration/power should more closely match what master branch does. 

My tests certainly show a significant difference especially with PNG files that have "soft edges" which seems to be any PNG imported into LW3, even if the image has hard edges. This issue fixes cases where there are very short segments at the start and end of a raster line.


---
**Wolfmanjm** *September 02, 2016 19:25*

FYI this firmware also includes a very useful feature that allows you to change the power on the fly...



M221 S50 will set the laser power to 50%

M221 S100 sets it back to 100%



this basically scales the power from whatever is specified.



Also there is now a 

fire 70 

command which will fire the laser at 70% power

and 

fire off 

which will return it to auto mode and turn the laser off, this is great for positioning the laser etc.




---
**Anthony Bolgar** *September 02, 2016 20:05*

**+Wolfmanjm**  Have these new features been merged into the smoothie master yet? Or are they only in [http://blog.wolfman.com/files/laser-fixed-1.bin](http://blog.wolfman.com/files/laser-fixed-1.bin)? I do not want to post the features until the masses have access to them.


---
**Wolfmanjm** *September 02, 2016 20:08*

**+Anthony Bolgar** they will never be merged into master, the master branch is frozen and will never get any updates. We only actively develop on the edge branch. This is currently a pull request to smootheware edge, and the binary I posted is for testing. Once it is tested it'll get merged into edge. probably in a few days.




---
**Anthony Bolgar** *September 02, 2016 20:08*

**+Wolfmanjm** Sorry, I meant the smoothie edge, not master.


---
**Wolfmanjm** *September 02, 2016 20:10*

Yes it needs some testing feedback before I merge it into edge. I also wanted to add a few more panel features like changing power on the fly from the panel, and test firing from the panel.


---
**Anthony Bolgar** *September 02, 2016 20:19*

Thanks for all the hard work **+Wolfmanjm** .I have posted the info to the community and the LaserWeb wiki. I mentioned that it will be a few days before the changes are added to the edge branch.


---
**Anthony Bolgar** *September 02, 2016 20:20*

When the new features go live I will let the community know.


---
**Alex Krause** *September 03, 2016 02:57*

**+Wolfmanjm**​ I wasn't planning on firing up the laser tonight but I will make it a point to do atleast a few tests :) awesome work with power on the fly adjustments I was just talking to **+Ariel Yahni**​ the other night how awesome that would be :) you must be reading my mind


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/a3uNfH8j6Gm) &mdash; content and formatting may not be reliable*
