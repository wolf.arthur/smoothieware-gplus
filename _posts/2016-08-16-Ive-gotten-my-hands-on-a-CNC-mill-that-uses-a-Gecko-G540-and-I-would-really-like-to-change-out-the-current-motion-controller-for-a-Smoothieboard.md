---
layout: post
title: "I've gotten my hands on a CNC mill that uses a Gecko G540 and I would really like to change out the current motion controller for a Smoothieboard"
date: August 16, 2016 19:05
category: "General discussion"
author: "Stephen Baird"
---
I've gotten my hands on a CNC mill that uses a Gecko G540 and I would really like to change out the current motion controller for a Smoothieboard. I understand (or think I understand) that the easiest way to accomplish that would be with a parallel shield, but the shield is "coming soon" and not in stock... does anyone know when they're likely to be back in stock?



The github repo for the shield shows the last activity as being two years ago, so I have a feeling it may not be coming soon to a retailer near me. As it's open source (hooray!) I could always make my own from the provided source... but it's so much easier (and usually a bit cheaper) to buy from a commercial source. Is there another source for the shield that I'm not finding, or am I probably doing to be making my own?





**"Stephen Baird"**

---
---
**Jeff DeMaagd** *August 17, 2016 03:58*

It's usually easy and not too expensive to feed board files to OSH Park.


---
**Jeff DeMaagd** *August 17, 2016 04:01*

I take that back, there's some surface mount parts. You can get the boards easily but getting those parts soldered on might be a challenge if you don't have such soldering experience.


---
**Stephen Baird** *August 17, 2016 04:25*

I'm actually not bad with smd stuff, I even have a hot air reflow gun and some solder paste. 


---
**Wolfmanjm** *August 17, 2016 04:33*

I really do not think the parallel board is what you need. Take a closer look :)


---
**Wolfmanjm** *August 17, 2016 04:34*

you can drive geckos as external drives from a smoothieboard, check the wiki under the cnc section [http://smoothieware.org/cnc-mill-guide](http://smoothieware.org/cnc-mill-guide)


---
**Jeff DeMaagd** *August 17, 2016 04:36*

This is replacing an existing controller, presumably with that connector. But yeah, it can be wired more directly.


---
**Stephen Baird** *August 17, 2016 04:43*

The Gecko drive basically only needs step and dir inputs, so I could chop up an old parallel cable and wire the appropriate pins as external drivers, but I was under the impression that that's effectively what the parallel shield does, just a lot more neatly than a chopped up cable. 


---
**Arthur Wolf** *August 17, 2016 08:33*

You really don't need the parralel shield, just wire the drivers to the Smoothieboard directly and you are good to go, it's really easy.

Note that we've had quite a few reports now about geckos being much more difficult to tune/get to work right than other drivers ( like leadshine and friends ), but I guess if you already have the drivers you can't do much about that.


---
**Stephen Baird** *August 17, 2016 14:14*

Hm... well, the Gecko drive unexpectedly came with the machine. I was actually expecting to have to completely replace the electronics, and had planned to either just use a Smoothieboard or use a Smoothie with the MassMind high powered drivers as external drivers depending on what I went with for steppers. So I'm not opposed to pulling out and selling off the electronics I've got now.



The machine itself is very old, from the mid-80s, and I had expected equally old proprietary electronics with geared servo motors but found steppers and modern electronics to my pleasant surprise. It would be nice to just have to swap out the motion controller, but it would also be nice to have it work well, so a full refit may still be in order.


---
**Jeff DeMaagd** *August 17, 2016 14:24*

The level shifters on the parallel port adapter might help with the Gecko drives. It shouldn't be necessary, but Gecko with smoothie seems to be hit or miss. I initially had a hard time but it worked well later. I never did figure out exactly what I did wrong before.


---
**Arthur Wolf** *August 17, 2016 15:26*

**+Jeff DeMaagd** You can wire smoothie's pins as open-drain, then they can be used as a 5V output, so there is not much need for a level shifter in that situation.


---
**Toby Martin** *August 30, 2016 11:29*

**+Arthur Wolf** Do I need to wire in an external pull-up to use Smoothie's pins as a 5v open-drain? Does configuring with internal pull-up make it 5v or 3.3v out? Can I configure the PWM (laser control) pin as a 5v output?  Say,  laser_module_pin 2.5^o  ? ...maybe have to invert it.


---
**Arthur Wolf** *August 30, 2016 11:31*

I generally find I don't need any pull-ups, but do have to invert them, so that'd be 2.5o!


---
**Toby Martin** *August 30, 2016 11:58*

Cool - I need an o-scope, to check things out.   I just looked over the smoothie schematic... or I think I could use one of the small FETs, just use 5v in for the power side, if I understand it correctly.  I need to look at the CPU data sheet.


---
*Imported from [Google+](https://plus.google.com/+StephenBaird/posts/FgZBJNtfCAr) &mdash; content and formatting may not be reliable*
