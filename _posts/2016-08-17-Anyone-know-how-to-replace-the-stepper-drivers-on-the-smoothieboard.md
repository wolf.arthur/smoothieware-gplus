---
layout: post
title: "Anyone know how to replace the stepper drivers on the smoothieboard?"
date: August 17, 2016 20:44
category: "General discussion"
author: "Soup3y gnome"
---
Anyone know how to replace the stepper drivers on the smoothieboard?  I just hooked mine up and M1 and M2 are not working at all.  Checked and double checked my wiring as well as config file, and no problems there.  If you know of a place to pick up the pieces I would appreciate, if it is possible.





**"Soup3y gnome"**

---
---
**Arthur Wolf** *August 17, 2016 21:04*

Are drivers other than M1 and M2 working ?


---
**Soup3y gnome** *August 17, 2016 21:06*

**+Arthur Wolf** Yes, the other 2 are working.


---
**Arthur Wolf** *August 17, 2016 21:07*

You just received the board ? Where did you purchase it ?


---
**Soup3y gnome** *August 17, 2016 21:09*

**+Arthur Wolf** I received the board about a month ago from Uberclock.  I just got around to soldering pins and setting it up today though.


---
**Arthur Wolf** *August 17, 2016 21:11*

**+Steven Campbell** This is a strange situation because the Uberclock boards are actually fully functionally tested, so it's certain the drivers worked before it was shipped.

However, if you didn't break them, if you contact them at uberclockllc@gmail.com they should exchange the board.


---
**Soup3y gnome** *August 17, 2016 21:18*

**+Arthur Wolf** I am not sure if I broke them, but I will contact them after I am 100% sure they are broken.  When I try to move the motor it seems like it gets a little power and then it cuts off and doesn't move at all.  It is almost like the motor is not getting enough current, but I have it set to 2a and not connected to anything.


---
**Johan Jakobsson** *August 18, 2016 07:18*

**+Steven Campbell** Bery basic advice, but it could be an incorrect/low current value in your override file. Easy way to check is to Remove/rename the override file. 

Suggesting since that was my problem when I first set up my smothieboard.


---
**Christian Lossendiere** *August 18, 2016 11:46*

I was have same problem with 2 smoothieboard 5x already many time ago but still don't find enough time to overcome the problem. Drivers don't have power, can easy stop motor with hand even driver setting at 1.6A in file config.

Johan what is override file ? Is when have 2 file config ?


---
**Soup3y gnome** *August 18, 2016 14:33*

**+Johan Jakobsson** Thank you, but it didn't work.  No override file on my board.  I have been changing everything in the config file itself so far.


---
*Imported from [Google+](https://plus.google.com/103765596183993757536/posts/jnNLem75DRN) &mdash; content and formatting may not be reliable*
