---
layout: post
title: "Originally shared by Sebastian Szafran Continuing my K40 conversion questions: I have a digital Volt/Amper meter from eBay to show laser current and voltage"
date: August 19, 2016 20:41
category: "General discussion"
author: "Sebastian Szafran"
---
<b>Originally shared by Sebastian Szafran</b>



Continuing my K40 conversion questions:

I have a digital Volt/Amper meter from eBay to show laser current and voltage. Am I right to connect the Voltmeter between FG and L- (GND) and Ampermeter between FG and Laser wire disconnected from FG?

!![images/e3460cdcc77235def819d5cdb6a14213.jpeg](images/e3460cdcc77235def819d5cdb6a14213.jpeg)



**"Sebastian Szafran"**

---
---
**Bouni** *August 22, 2016 06:28*

Do you want to read the actual High Voltage that goes into the tube an its current? I don't think that a cheap ebay Volt/ampere meter can handle such high voltages. Be VERY careful when dealing with high voltage, a minor lack of attention can result in you death (really, no joking here!). In my opinion FD is Frame Ground and has to be connected to Earth. L- is the negative pole of the high voltage and is maybe internally connected to FG, so I don't know if the connections in the graphic are correct.


---
*Imported from [Google+](https://plus.google.com/+SebastianSzafran/posts/FU1JFY6aVmm) &mdash; content and formatting may not be reliable*
