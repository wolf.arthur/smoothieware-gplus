---
layout: post
title: "Originally shared by Sebastian Szafran This is my K40 PSU"
date: August 19, 2016 19:46
category: "General discussion"
author: "Sebastian Szafran"
---
<b>Originally shared by Sebastian Szafran</b>



This is my K40 PSU. P+ and G has been bended and soldered by default. I unsoldered those pins to allow interlock switches (door, water flow, temperature sensors) connections. 



Based on the Smoothie conversion diagram ([http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)) what is the best way to do the conversion:

1. Does the diagram work in way that L current value will be 'directed' by Smoothie PWM while POT sets the maximum current (ceiling = 100% PWM) value for the laser tube?

or

2. Is it better to connect Smoothie PWM to IN and remove POT?

or

3. Smoothie PWM1 connected to IN and Smoothie PWM2 connected to L and directed separately?

or other possibilities? 



![missing image](https://lh3.googleusercontent.com/-ARcriSq2nUg/V7dh0BibJkI/AAAAAAAAPes/vEe48T5T_FguN5t4XNNU2CXUWffsnFt7wCJoC/s0/19.08.2016%252B-%252B1.jpeg)
!![images/0016a6ff03b4a6fe255f40554372088a.jpeg](images/0016a6ff03b4a6fe255f40554372088a.jpeg)

**"Sebastian Szafran"**

---
---
**Sebastian Szafran** *August 19, 2016 19:54*

PSU = DIAGRAM

P+ = P

G = G

K- = G

K+ = L

G = G

IN = IN

5V = 5V

24V = +24V

G = G

5V = 5V

L = L



All G are connected. 

K+ and L are connected.


---
**Arthur Wolf** *August 19, 2016 19:56*

People have wired drivers like this in several ways, and several ways are known to work. I've never done the one in the diagram and I don't know if it actually works.

1. I think that's what it's trying to achieve yes

2. That's what I do, but I don't know if it's better.

3. There is only one PWM output on the smoothieboard. I connect it to IN, but I've heard reports of users using it on L, though I don't know with what success.






---
*Imported from [Google+](https://plus.google.com/+SebastianSzafran/posts/L2fGVTTRZXQ) &mdash; content and formatting may not be reliable*
