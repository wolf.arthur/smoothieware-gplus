---
layout: post
title: "First let me confess that I am new to 3D printing!"
date: August 20, 2016 22:32
category: "General discussion"
author: "Daniel Benoit"
---
First let me confess that I am new to 3D printing! I'm finally at the stage where I can start to print on my smoothie board Cartesian printer. I have everything working and I am trying to print a test block. I am using Simplify3D to slice and prepare the g code. This is what is happening. When the printer starts to run a print, the print head does a cleaning cycle with the z axis at the correct level. It then moves to the center of the bed raises the print head approximately 5 or 6 MM and starts to print. Of course all this does is make a mess and I abort the print. I've tried to adjust the Z offset in Simplify 3D, but this doesn't seem to help. Any ideas to correct this problem would be appreciated. I can do a video if that would help.



Thanks Dan







**"Daniel Benoit"**

---
---
**Arthur Wolf** *August 20, 2016 22:42*

Hey **+Daniel Benoit**

We really need to know what the start of your gcode file looks like, and probably what your config looks like, to make any sense of the situation.

Cheers.


---
**Daniel Benoit** *August 22, 2016 15:50*

Thanks Arthur, I'll get that organized today!


---
**Daniel Benoit** *October 25, 2016 01:06*

Well guys, after several weeks remodeling my kitchen I'm back on my printer. Still can't figure out why the Z axis retracts before it starts to print. I drew up a little test piece in Alibre and sliced it with Simplify3D. Could you please look over these files and see if you can spot anything weird. I'm a complete newbie and this is my first print on my converted unit. Here are the files: [http://pastebin.com/VrRGJFRY](http://pastebin.com/VrRGJFRY)



[http://pastebin.com/q2apA1Lx](http://pastebin.com/q2apA1Lx)



Thanks again for all your help,



Dan

[pastebin.com - Dan's Test gcode generated with S3D. - Pastebin.com](http://pastebin.com/q2apA1Lx)


---
*Imported from [Google+](https://plus.google.com/+sandcrab123/posts/gSns5bRYCEW) &mdash; content and formatting may not be reliable*
