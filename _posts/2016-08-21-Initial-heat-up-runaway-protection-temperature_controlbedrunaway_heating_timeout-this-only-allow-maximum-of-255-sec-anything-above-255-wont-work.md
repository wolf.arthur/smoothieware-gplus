---
layout: post
title: "Initial heat-up runaway protection temperature_control.bed.runaway_heating_timeout this only allow maximum of 255 sec, anything above 255 wont work"
date: August 21, 2016 13:07
category: "General discussion"
author: "David Wong"
---
Initial heat-up runaway protection  temperature_control.bed.runaway_heating_timeout

this only allow maximum of 255 sec, anything above 255 wont work. my bed definitely take longer than 255s to reach desire temperature.

if i set 600, the bed will heat up like about 30-40sec and HALT.

  firmware limitation?? bug?? 







**"David Wong"**

---
---
**Arthur Wolf** *August 21, 2016 13:30*

This is a firmware limitation. Do it by steps : M190 S20 then M190 S40 then M190 S60 etc, that should work.


---
**David Wong** *August 21, 2016 14:07*

Add in every gcode till it reached target temperature?


---
**Arthur Wolf** *August 21, 2016 14:08*

I'm not sure I understand


---
**David Wong** *August 22, 2016 02:13*

I'm using cura2.1 as my slicer and copy the gcode to printer via sdcard. I'm not quite sure how to do the m190 s20, m190 s40 as u suggested.😊 


---
**René Jurack** *August 22, 2016 03:56*

google for "cura startscript M190"


---
**David Wong** *August 22, 2016 12:10*

i cant edit m190 in cura2.1 ..dafault m190 will be the first command 


---
**David Wong** *August 22, 2016 14:53*

**+René Jurack**​ can u give me some guide on how to add m190 to cura2.1 slicer? Or point me to the right site? 

I can edit the gcode manually but it is possible to add in to my cura? 

Cura seem to auto add m190 s{print_bed_temperature}, I can't add anything before that auto generated m190 


---
*Imported from [Google+](https://plus.google.com/109021423242312372852/posts/EAYdDxftmVy) &mdash; content and formatting may not be reliable*
