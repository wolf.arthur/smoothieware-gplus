---
layout: post
title: "try to use delta grid on my 340x * 300y Cartesian printer"
date: August 21, 2016 12:19
category: "General discussion"
author: "David Wong"
---
try to use delta grid on my 340x * 300y Cartesian printer. when i start G31 code, it only probe square and not rectangle no matter how i play with the delta-grid.max_x, delta-grid.max_x and  radius value. how do i set this?

leveling-strategy.delta-grid.enable          true

leveling-strategy.delta-grid.radius          90

leveling-strategy.delta-grid.size            7

mm_per_line_segment 			     1       

leveling-strategy.delta-grid.is_square       true

leveling-strategy.delta-grid.max_x           170

leveling-strategy.delta-grid.max_y           150





**"David Wong"**

---
---
**Arthur Wolf** *August 21, 2016 12:27*

It sounds like right now it only knows how to do a square


---
**David Wong** *August 21, 2016 12:39*

oh, didnt know that. thanks for the confirmation :)




---
**David Wong** *August 21, 2016 12:49*

**+Arthur Wolf** im using proximity sensor permanently attached  to my printer (not using z endstop) 

so i add  using G30 Znnn to automatically find the bed  in gcode. now my problem is can i adjust the z offset in RepRapDiscount LCD Controller? 

adjust configure> z home ofs  in LCD doesnt seem to do anything.

sometime i need to fine tune the z offset to make it stick better, manually edit the g30 znnn in gcode everytime is very inconvenient. possible to make z home offset in LCD to work ?? 


---
**Arthur Wolf** *August 21, 2016 12:50*

**+David Wong** I don't know I have never used this that way, maybe somebody else will know


---
**Wolfmanjm** *August 22, 2016 00:21*

proximity sensors make very bad, inaccurate probes FWIW.

 setting homing offset requires you to home in z to make use of it. that is why it is called a homing offset. tuning the z offset live requires you to use WCS which is documented (G54, G10 L2 etc)


---
**David Wong** *August 22, 2016 14:38*

So it is possible for me to use the probe (proximity sensor in my case) as a leveling probe and z  homing endstop? 

Possible?


---
**Wolfmanjm** *August 22, 2016 18:57*

you could but as I said it will make a useless probe as you need a point contact to do zgrid probing, not some approximate area on the bed. The work well as endstops though ,sop long as they detect the exact same spot on the bed each time. Proximity probes generally return different results depending on where on the bed they probe or if they are close to an edge


---
*Imported from [Google+](https://plus.google.com/109021423242312372852/posts/fQbhKQRh3Ek) &mdash; content and formatting may not be reliable*
