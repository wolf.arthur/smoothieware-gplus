---
layout: post
title: "Hi everyone, i've been running a smoothk40 laser for a while now and i'm really stuck with the burnt edges on raster engraving, is there a setting i'm missing to tune the power ramping?"
date: August 26, 2016 11:14
category: "General discussion"
author: "Marc Rogers"
---
Hi everyone, i've been running a smoothk40 laser for a while now and i'm really stuck with the burnt edges on raster engraving, is there a setting i'm missing to tune the power ramping? i'm using visicut to generate the gcode which doesn't have an acceleration space option for smoothie. any thoughts?

![missing image](https://lh3.googleusercontent.com/-kH5YAWT2Y3Y/V8AkCZrhJXI/AAAAAAAAPlY/tA7knMcA6fI92fqWdG3Xr4zWv0Q2LaBSwCJoC/s0/14079896_10153740070422341_4498359150620177955_n.jpg)



**"Marc Rogers"**

---
---
**Ariel Yahni (UniKpty)** *August 26, 2016 11:46*

try [laserweb.xyz](http://laserweb.xyz) :)


---
**Marc Rogers** *August 26, 2016 12:30*

i've got laserweb just not used it, is there a way of changing the homing point on screen as my laser homes top left?


---
**Ariel Yahni (UniKpty)** *August 26, 2016 12:36*

Thats a hardware change, in smoothie change home to Y max and make that endstop Y max


---
**Marc Rogers** *August 26, 2016 12:47*

ok changed that bit, do i have swap the min 0, max 200 around because it is now homing in the wrong direction


---
**Marc Rogers** *August 26, 2016 12:54*

ignore that, i inverted the stepper and now seems fine




---
**Wolfmanjm** *August 26, 2016 20:38*

This is a bit of a thorny subject at the moment :) IMO the latest edge does acceleration correctly, and the laser power ramps linearly with the acceleration. The master branch accelerates very roughly (about every millisecond) and ramps in sync with that. You do not say which one you are using. However last time I looked at the gcode for something that looked similar to this you could see the gcode generated higher power levels at the start of lines causing these darker cuts. My guess is that the dithering on the edges had confused the rasterizer making this happen. Once you check your gcode for this issue, I suggest trying latest edge and see if it is any better.

One thing I suspect is a linear mapping of power to current speed on the acceleration curve may not be optimal.


---
**Marc Rogers** *August 27, 2016 18:00*

that's the kind of answer i was looking for thanks, i'm running the latest stable but i'll give edge a go, i wouldn't have thought a linear map would work at all as most co2 lasers don't output in direct proportion to input, mine for instance hits a peak wattage at about 92% pwm and then tails off towards 100%, with 40%-50% being a considerably bigger jump in output compared with 60%-70%

 i'm guessing for a cut and paste coder like myself implementing a configurable set of points in the config file is out of my range 


---
**Marc Rogers** *August 27, 2016 18:03*

much like in marlin where you can create a lookup table for a thermistor




---
**Wolfmanjm** *August 27, 2016 19:59*

Now I have a PWMable  controller I'll be experimenting with the acceleration/PWM profile, I think either logarithmic or I² should work, failing that a LUT will be needed. Basically as far as I can see the PWM controls the current to the LD proportionally, but current is not a linear relationship to power AFAIK. Although laser diodes behave oddly in that respect.


---
**Wolfmanjm** *August 28, 2016 06:28*

ok I plotted the current vs power for the laser diode I have and it is almost linear, not perfectly, but closer than I would have expected [https://goo.gl/photos/1cQs4gQH8rtyNK8A8](https://goo.gl/photos/1cQs4gQH8rtyNK8A8) This puts my theory about the PWM not being proportional in doubt.


---
**Marc Rogers** *August 28, 2016 07:35*

hmmm interesting, although i'd expect a diode to be more accurate than a gas laser, i've got a bit of downtime on the cutter today so i'll give edge a go, reducing the effects of acceleration seems to help too


---
**Marc Rogers** *August 28, 2016 08:10*

edge has improved it but only a touch, [https://drive.google.com/open?id=0Bz4MXA1LvDdFUlY1QjdrbGtndms](https://drive.google.com/open?id=0Bz4MXA1LvDdFUlY1QjdrbGtndms)


---
**Marc Rogers** *August 28, 2016 10:53*

i'm going to cheat for now and do a bounding box around the work to fake an acceleration zone for now


---
**Wolfmanjm** *August 28, 2016 18:39*

I'm wondering if the burning power is not a linear relationship... like will 1 watt burn at half the amount that 2 watts would? or is that non linear, if so what is the relationship.


---
**Wolfmanjm** *August 28, 2016 18:46*

did you try increasing your acceleration and see if the dark band narrowed? that would confirm it was this issue or an image conversion issue.


---
**Arthur Wolf** *August 29, 2016 16:51*

CO2 lasers are definitely not linear, you totally need to set up minimal and maximal value for engraving, and the range is pretty small ( less than 10% when I last set this up. Depends on materials ). And even then ideally the raster software should have some way to compensate for the fact that even in that range it's not linear.


---
**Marc Rogers** *August 30, 2016 12:33*

**+Wolfmanjm** hi, yes the effects are reduced by lessening the effects of the acceleration, but it's at a point where it's jerking so hard it's going to damage something. i'm running a commercial job at the moment but i'll do a sample scale for you with 500, 1000, 2000, 3000 accel value with nothing else changed


---
**Wolfmanjm** *September 02, 2016 18:22*

**+Marc Rogers** I found a bug in the new edge related to the proportional power during acceleration. i'll fix it today, i'd be interested to see if this helps or not.


---
**Wolfmanjm** *September 02, 2016 19:18*

please try this firmware... it should work better... [http://blog.wolfman.com/files/laser-fixed-1.bin](http://blog.wolfman.com/files/laser-fixed-1.bin)


---
**Marc Rogers** *September 03, 2016 10:15*

that's great, I'll give it a try :)


---
*Imported from [Google+](https://plus.google.com/100818067744008604646/posts/7QQ5fVnb3ue) &mdash; content and formatting may not be reliable*
