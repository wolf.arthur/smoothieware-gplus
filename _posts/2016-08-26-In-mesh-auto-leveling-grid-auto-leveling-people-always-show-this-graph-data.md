---
layout: post
title: "In mesh auto leveling / grid auto leveling, people always show this graph data"
date: August 26, 2016 00:58
category: "General discussion"
author: "David Wong"
---
In mesh auto leveling / grid auto leveling, people always show this graph data. Any software that I can import the probe data and see the bed leveling graph like this?

![missing image](https://lh3.googleusercontent.com/-lAcpO98UfRE/V7-UP3UdKJI/AAAAAAAAfc8/pTSZAmdBOuYuqvzUcEgUSqZggBUooG5bACJoC/s0/16%252B-%252B1.jpeg)



**"David Wong"**

---
---
**Ross Hendrickson** *August 26, 2016 01:27*

People plot it in several things


---
**Dushyant Ahuja** *August 26, 2016 03:01*

You can easily do this in excel


---
**René Jurack** *August 26, 2016 06:19*

yeah, excel... copy n paste the log into excel, do some cleanup, click on "add diagram" and boom, there you are...


---
**Olivier Bilodeau** *August 26, 2016 14:13*

How do you even get so much measure point?


---
**Walter White** *September 12, 2016 09:17*

For the graph try this online tool [http://www.maui-3d.com/cgi-bin/plotG29](http://www.maui-3d.com/cgi-bin/plotG29) work fine with marlin, should work even with smoothie.

How can I use the G29 on a delta? Output message says "g29 no levelling strategy".


---
*Imported from [Google+](https://plus.google.com/109021423242312372852/posts/HSixLQN9gNk) &mdash; content and formatting may not be reliable*
