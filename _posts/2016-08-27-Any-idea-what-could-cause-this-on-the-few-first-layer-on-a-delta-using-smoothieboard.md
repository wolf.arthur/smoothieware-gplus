---
layout: post
title: "Any idea what could cause this on the few first layer on a delta using smoothieboard?"
date: August 27, 2016 04:01
category: "General discussion"
author: "S\u00e9bastien Plante"
---
Any idea what could cause this on the few first layer on a delta using smoothieboard? 

![missing image](https://lh3.googleusercontent.com/-kS6UfN1maqs/V8EQiUjFmhI/AAAAAAAAJ_Y/9dbMGArP-V8weMlyBtUm_GkxZz45aLN5wCJoC/s0/20160826_233733.jpg)



**"S\u00e9bastien Plante"**

---
---
**Whosa whatsis** *August 27, 2016 04:08*

I've seen similar artifacts that turned out to be due to frame flexing or axis tilting at the extreme of Z motion, but only on Cartesian bots. The same might be possible on a delta, but unless your linear guides are bent, it's hard to imagine how. An invalid belt path might do it, but a Bowden tube that is too short is probably a more likely cause.


---
**Sébastien Plante** *August 27, 2016 12:37*

Oh, I just replaced the extruder (for a bondtech, along with the upgrade to smoothieboard)... must be that! I'll change the tube for a longer one.



Thanks for the idea! 


---
**Patrick Darrieulat** *August 28, 2016 04:35*

Bonjour

Désolé en Français voir le réglage du courant des drivers moteurs mettre à la valeur préconisée par le fabricant diminuer l'accélération bon courage


---
**Sébastien Plante** *August 28, 2016 13:30*

**+Whosa whatsis** Wasn't that :(



**+Patrick Darrieulat**​ Je vais essayer, j'ai remarquer que j'ai seulement ce problème avec les cercles... un cube par exemple sera très droit.  



Le courant des stepper est peut-être trop haut et le moteur chauffe (je sais qu'il n'est pas trop bas, il ne skip pas de steps). 



L'accélération, je vais lire la dessus! 


---
**Sébastien Plante** *August 29, 2016 03:57*

Just an update, seems that one of my pulley was a bit loosen, causing slip only from one side. I fonds that trying to move the belt while the motor were holding.



Thanks for suggestions, I've tuned some settings hehe :D


---
*Imported from [Google+](https://plus.google.com/+excessnet/posts/S91sa3eRa5d) &mdash; content and formatting may not be reliable*
