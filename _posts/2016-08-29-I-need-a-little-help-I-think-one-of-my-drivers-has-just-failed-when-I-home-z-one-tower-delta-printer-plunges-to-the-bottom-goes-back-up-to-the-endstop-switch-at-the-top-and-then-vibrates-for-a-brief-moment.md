---
layout: post
title: "I need a little help. I think one of my drivers has just failed - when I home z, one tower (delta printer) plunges to the bottom, goes back up to the endstop switch at the top and then vibrates for a brief moment"
date: August 29, 2016 18:00
category: "General discussion"
author: "Dean Rock"
---
I need a little help. I think one of my drivers has just failed - when I home z, one tower (delta printer) plunges to the bottom, goes back up to the endstop switch at the top and then vibrates for a brief moment. Any z movement sends this tower downward if starting from home position. My fault - I was showing someone how the delta moves with it turned off. My question: can I reconfigure the M5 driver to run the z motor and, how?





**"Dean Rock"**

---
---
**Arthur Wolf** *August 29, 2016 18:25*

This should help you : [http://smoothieware.org/3d-printer-guide#toc30](http://smoothieware.org/3d-printer-guide#toc30)




---
**Dean Rock** *August 29, 2016 19:16*

Thanks, Arthur - I thought so  but wanted to check before doing it!


---
**Dean Rock** *August 29, 2016 23:04*

And... got it in one. A rarity for me!


---
**Triffid Hunter** *August 30, 2016 03:31*

What are the specs of the motors on your printer? Recommended motors usually don't cause problems from manual movement but there are many motors with poor specs which will


---
**Dean Rock** *August 30, 2016 11:52*

I don't know specifics for certain but it is a standard, Chinese, NEMA 17 running @ 1.5a. I wouldn't doubt the "poor specs" at all. I've replaced many parts on this printer so far. Too many!


---
**Triffid Hunter** *August 30, 2016 12:12*

What's the winding resistance, winding inductance and rated voltage?


---
**Dean Rock** *August 30, 2016 12:13*

Well, rated unknown but running @ 12 volts.


---
*Imported from [Google+](https://plus.google.com/116087024084558080588/posts/3NXGfBpsnB7) &mdash; content and formatting may not be reliable*
