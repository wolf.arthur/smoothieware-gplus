---
layout: post
title: "Anyone have files for a laser cut smoothie enclosed..."
date: August 31, 2016 14:27
category: "General discussion"
author: "Alex Krause"
---
Anyone have files for a laser cut smoothie enclosed... all I could find is 3d printed enclosure. I will make my own if no one has a source





**"Alex Krause"**

---
---
**Ariel Yahni (UniKpty)** *August 31, 2016 14:40*

[https://github.com/lautr3k/SmoothieBox](https://github.com/lautr3k/SmoothieBox)


---
**Alex Krause** *August 31, 2016 14:55*

**+Ariel Yahni**​ thanks brother!


---
**Alex Krause** *August 31, 2016 15:08*

**+Sébastien Mischler**​ thank-you for designing the smoothiebox I'm going to work on making this today :)


---
**Sébastien Mischler (skarab)** *August 31, 2016 15:11*

:)


---
**Ariel Yahni (UniKpty)** *August 31, 2016 16:38*

Images replay are liveeeeee at least on android

![missing image](https://lh3.googleusercontent.com/CYdzLVuxoXh-Hu6pmJ-2-U-PpfxeLpW50nXX-W5skb4QYGNgSYMQfayiGqMx-mZ1IwCuww68qkzb9zJKzxOiql1SLToN3Lbjy9Kq=s0)


---
**Ariel Yahni (UniKpty)** *August 31, 2016 16:38*

**+Peter van der Walt**​


---
**Alex Krause** *August 31, 2016 16:50*

**+Ariel Yahni**​ I don't have the option for image reply on Android and no updates are available for me on the play store for G+


---
**Ariel Yahni (UniKpty)** *August 31, 2016 16:52*

**+Alex Krause**​ if you are comfortable get it from here, but it should show on the updates in play store [apkmirror.com - Google+ 8.6.0.131660008 APK Download by Google Inc. - APKMirror](http://www.apkmirror.com/apk/google-inc/google/google-8-6-0-131660008-release)/ 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/5SXf8KZiAaG) &mdash; content and formatting may not be reliable*
