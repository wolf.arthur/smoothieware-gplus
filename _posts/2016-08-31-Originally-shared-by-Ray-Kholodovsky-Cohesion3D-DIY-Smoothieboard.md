---
layout: post
title: "Originally shared by Ray Kholodovsky (Cohesion3D) DIY Smoothieboard"
date: August 31, 2016 19:48
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



DIY Smoothieboard.  

A while back, **+Arthur Wolf**  put a bounty on a PCB design for a single sided board that could be milled on a CNC which ran smoothie and had everything you'd need: ATX PSU input, 5 motor driver sockets, MOSFETs, GLCD connectors, and more!  The original post is here: [https://plus.google.com/+ArthurWolf/posts/hg7bv7CA7ev](https://plus.google.com/+ArthurWolf/posts/hg7bv7CA7ev)



The board is under 100x150mm so you can make it from a relatively common size of copper clad that's available at all the usually places, and the main tracks are 20/ 20 mil (width and spacing) while the finer details are 10/ 10 mil.  This means you could make a basic motion board with a 0.5mm end mill and a crappy machine, but realistically you should use a 0.25mm/ 10 mil/ #87 end mill which is still very accessible.  



But you may say to yourself, Ray, I see the sockets for the pololu drivers, the MOSFETs, and the connectors, but where is the smoothie?  All you've got here is a dumb breakout board.  Precisely!  



What I've done is take the entire brain behind Smoothie: the LPC1769 chip, MicroSD socket, USB jack, and all the tiny and high speed components needed for those to function, and put it onto this tiny 1.2 inch square (that's 30x30mm) that I call the Smoothie Stamp. It is entirely self contained and breaks out all the signals for the 5 motor drivers, 6 mosfets, thermistors, endstops, and even regulates its own 3.3v from 5v USB power.  



This lets anyone embed a fully functioning smoothie brain into a carrier board of their own creation, with exactly the drivers, peripherals, and connectors they want. 



I'm looking forward to my smoothieboard v2, please :)



![missing image](https://lh3.googleusercontent.com/-YbxHr_X9QHc/V8cxA75xIeI/AAAAAAAAgjc/mwMDukdf0WQm-plIdY3WB_tmhkHt6ZK8QCJoC/s0/Board%252B1.png)
![missing image](https://lh3.googleusercontent.com/-Tt3VdSY3Ld4/V8cytoWJWuI/AAAAAAAAgks/u8CPXH0kpRQkyqx3gygtzJZtJ3JqB1BngCJoC/s0/Smoothie%252BSTAMP%252BEagle%252B1.png)
![missing image](https://lh3.googleusercontent.com/-YJE91WDsnVY/V8c0C8hDe3I/AAAAAAAAgks/-5v7tgWdcEoSmNxA-OikI7Ujsw6IpyICQCJoC/s0/OSHPARK%252BFRONT.png)
![missing image](https://lh3.googleusercontent.com/-vD2guG-ngmg/V8c0Ephta8I/AAAAAAAAgks/bfRsZxfqnTcmRHZmiWj0EyorBQ1MN4TbgCJoC/s0/OSHPARK%252BREAR.png)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Arthur Wolf** *August 31, 2016 20:02*

Yep, you just won a Smoothieboard v2 :) Do you plan on manufacturing one of those babies ?


---
**Ray Kholodovsky (Cohesion3D)** *August 31, 2016 20:08*

Well, the "stamp" will arrive from **+OSH Park** this Saturday. I've actually never milled a PCB before... I can get the necessary 10 mil bit and give it whirl on my 3040, but it will take some time. I've asked **+Sebastian Szafran** if he can help in the "prototyping" department. :)


---
**Arthur Wolf** *August 31, 2016 20:10*

Awesome


---
**Wolfmanjm** *August 31, 2016 20:23*

is the stamp 4 layer? if not I suspect it will not be very stable. I have bad experience running these 120MHz chips on 2 layer boards. the last one I made from OSH was very unstable.


---
**Ray Kholodovsky (Cohesion3D)** *August 31, 2016 20:34*

**+Wolfmanjm** the stamp, and also the ReMix and Mini boards I make are all 2 layer.  I have not had issues with stability on these other boards. It is important to route power to the chip properly by having the 3.3v hit the decoupling caps first and only then go to the lpc's power in terminals. I would agree that having a dedicated 3.3v and gnd plane would shorten the path, but ultimately the routing method I described mitigates that well. 


---
**Wolfmanjm** *August 31, 2016 20:37*

When I got min eback it was extrememly unstable, an EE friend of mine explained you can;t really run 120MHz chips on 2 layer boards, it is not just the power traces, it is the signal traces as well. I hope yours is stable, trouble is unstable boards are very hard to diagnose, they work fine for a few hours then just crash.


---
**Ray Kholodovsky (Cohesion3D)** *September 03, 2016 16:33*

STAMP is here. Just made a new post about it too. ![missing image](https://lh3.googleusercontent.com/TqIPUrMO3XP_nBBDxcCiFG0jGxkcsSWi6iiA1-lQCvc9cuFQOhD74m379WJfazIeOuysRgUHYJywlQc=s0)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/fqcDax8Hzc6) &mdash; content and formatting may not be reliable*
