---
layout: post
title: "What is the best text editor to use to edit the smoothiware configuration file?"
date: August 31, 2016 22:26
category: "General discussion"
author: "Anthony Bolgar"
---
What is the best text editor to use to edit the smoothiware configuration file?





**"Anthony Bolgar"**

---
---
**Sébastien Plante** *August 31, 2016 22:34*

I go with Notepad++ or Sublime Text


---
**Alex Krause** *August 31, 2016 23:09*

I use Gedit


---
**Michaël Memeteau** *September 01, 2016 00:23*

Sublime here.




---
**Anthony Bolgar** *September 01, 2016 00:30*

I have been using Gedit, and was told Notepad puts hidden characters in the file that Smoothie does not like. At first I did not like Gedit, but it is growing on me. I will have a look at sublime.


---
**Triffid Hunter** *September 01, 2016 04:35*

I use kate


---
**Michael Stanich** *September 01, 2016 04:36*

I use EMACS.


---
**Arthur Wolf** *September 01, 2016 08:39*

Gedit is the recommended smoothie editor


---
**Thomas Sanladerer** *September 01, 2016 08:49*

Vim is the only editor you should be using! 



Jk - Notepad++ is pretty good. 


---
**Bouni** *September 01, 2016 13:01*

**+Thomas Sanladerer** So true, vim for the win :-)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/3MVZuQPxAYM) &mdash; content and formatting may not be reliable*
