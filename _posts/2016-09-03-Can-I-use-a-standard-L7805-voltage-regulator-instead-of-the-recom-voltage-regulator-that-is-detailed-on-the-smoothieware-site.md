---
layout: post
title: "Can I use a standard L7805 voltage regulator instead of the recom voltage regulator that is detailed on the smoothieware site?"
date: September 03, 2016 23:23
category: "General discussion"
author: "Anthony Bolgar"
---
Can I use a standard L7805 voltage regulator instead of the recom voltage regulator that is detailed on the smoothieware site?





**"Anthony Bolgar"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 03, 2016 23:43*

I do recall reading that section, and I believe there was a notation that the 7805 style regulators do work. But please check do search that page again for this statement. 


---
**Anthony Bolgar** *September 05, 2016 00:09*

OK, question answered. The LM7805 will produce way too much heat, that is why using the Recom switching regulator is reccomended. Especially if using a 24V PSU, the extra 19 volts has to be shed as heat.


---
**Ray Kholodovsky (Cohesion3D)** *September 05, 2016 00:11*

Very true.  Personally, I don't trust linear regulators (my limit is going from 5v to 3.3v in the 100's of mA, that's fine).  I would recommend getting a LM2596 circuit for under a buck (no pun intended) and backfeeding in.  Aka run wires and solder them to the v-reg contacts. I have one, can send to you so you don't have to wait for china.


---
**Ray Kholodovsky (Cohesion3D)** *September 05, 2016 00:14*

Oh, the Recom is a self contained switching regulator! Never mind. 


---
**Anthony Bolgar** *September 05, 2016 00:14*

I am just going to order a Recom 5V 1A switching regulator on Tuesday. Only $8 bucks Canadian from Digikey, will be shipped to my local parts place in one day, no shipping charges doing it this way.


---
**Triffid Hunter** *September 05, 2016 06:41*

1A * 7v = 7W of heat. You would require a moderately sized heatsink on a 7805 to handle that current, whereas the recom device is switchmode and requires no heatsink at all. If your input voltage is 24v, the situation is drastically worse with 19W of heat to deal with - that's almost as much as some hotend heaters!


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/1cS1Myf7VC8) &mdash; content and formatting may not be reliable*
