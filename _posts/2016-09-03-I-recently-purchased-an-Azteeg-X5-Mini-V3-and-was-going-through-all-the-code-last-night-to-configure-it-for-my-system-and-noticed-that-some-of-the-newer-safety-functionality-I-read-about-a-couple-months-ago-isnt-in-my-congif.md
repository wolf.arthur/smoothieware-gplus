---
layout: post
title: "I recently purchased an Azteeg X5 Mini V3 and was going through all the code last night to configure it for my system and noticed that some of the newer safety functionality I read about a couple months ago isn't in my congif"
date: September 03, 2016 18:42
category: "General discussion"
author: "Sean B"
---
I recently purchased an Azteeg X5 Mini V3 and was going through all the code last night to configure it for my system and noticed that some of the newer safety functionality I read about a couple months ago isn't in my congif (downloaded from github for V3 through Panucatt's website).



The config was also missing all the code for bang bang control of the bed.  Is it as simple as adding these lines into the config?  Can this also be done for the safety controls?





**"Sean B"**

---
---
**Arthur Wolf** *September 03, 2016 18:49*

Yep, the config file is just an example file, add anything you need to it, make sure you are using the latest firmware.


---
**Sean B** *September 03, 2016 18:53*

Yes, I downloaded the most recent firmware.  Thanks for the quick response!  So far smoothie has been 100 times more user friendly to understand and edit compared to Marlin. 



Thanks Arthur!


---
**Arthur Wolf** *September 03, 2016 18:55*

Glad you like it :)


---
**Eric Lien** *September 03, 2016 19:13*

**+Sean B** here is a list of all the configuration settings you have at your disposal: [smoothieware.org - Configuration Options - Smoothie Project](http://smoothieware.org/configuration-options)


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/53dNbxEF2qZ) &mdash; content and formatting may not be reliable*
