---
layout: post
title: "Originally shared by Anthony Bolgar Here is a little public service announcement"
date: September 05, 2016 09:03
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Anthony Bolgar</b>



Here is a little public service announcement.



Since Smoothieware/Smoothiebaord is Open Source, most people just assume that the derivatives and clones are also Open Source. This is not the case with the MKS Sbase board. It is a closed source clone of the Smoothieboard, and MKS does not give back to the community, or help the Smoothie project in any way.  **+Arthur Wolf** from the Smoothie project nails it pretty well: "MKS is (also) a cancer that is destroying the smoothie project. Everything people like about MKS, comes from the smoothie project, they did none of it, they are not helping in any way, they are leeches.

It is a closed-source derivative of open-source work, which is going against the wishes of the people who contribute to the smoothie project, it is violating the license, it is pissing on the community and on the work of hundreds of volunteers."



So the next time you are considering buying an MKS Sbase board, think back on this and ask yourself is it really worth saving some money, but at the same time helping to destroy a product you actually like, and hindering the development of new features and hardware by the SMoothie team.



I will now get down off my soap box and stop pontificating. ;)





**"Arthur Wolf"**

---
---
**Brian W.H. Phillips** *September 05, 2016 09:21*

Perhaps a lower price could be offered to mks board owners who are willing to buy an opensource product. Or maybe the open source product could be built on China so that the not so well off people in our community could afford it. Not all of us find it easy to pay over 50 for a board.


---
**Brian W.H. Phillips** *September 05, 2016 09:24*

Note. The reason that China sells mks boards is that they don't clamber for a quick instant high profit. That is not my opinion but reality




---
**Arthur Wolf** *September 05, 2016 09:31*

**+Brian W.H. Phillips** The reason China sells MKS is because people buy it. People buy it because it's cheaper ( and often because they don't realize it's not open-source ). It's less expensive because they didn't have to do any R&D, they don't have to do any for future evolutions, they don't code the firmware, they don't fix the firmware, they don't write the documentation, they don't help users, they don't provide support, etc. That's why it's less expensive.

By buying a MKS, users are essentially "stealing" all of that stuff from the community.



The Smoothie community "gave" the Smoothie project to the world, with the understanding that the world would respect it's license and the work the community does on it. The MKS is just a way for people to disrespect the project and the work.


---
**Brian W.H. Phillips** *September 05, 2016 09:39*

If that s the case who is paying the developers here if it is opensource then i have never heard of open source developers being paid, other than by voluntary payments to cover expenses.or are you referring to a startup company that wants to make a living out of Small scale production.


---
**Arthur Wolf** *September 05, 2016 09:57*

**+Brian W.H. Phillips** Robotseed sells Smoothieboard and uses a large part of the profits to pay me to work on firmware, documentation, and community management. Uberclock sells Smoothieboards and uses a large part of the profits to pay Mark to work on hardware and firmware development. Both companies also send a lot of free boards and money to contributors. Panucatt also helps and finances firmware dev from time to time. And we are currently working on a plan to have an even greater number of companies pulling together to finance full-time work on a user interface project for Smoothie.


---
**Brian W.H. Phillips** *September 05, 2016 09:58*

If anyone puts a project out as opensource then they loose their copyright s , it maybe that they think when the project is well tested and proven, that they can call it something else and sell it to big company. It is not surprising that a China company would steal the idea. I agree with you in general, but it is no good complaining after the horse has bolted!


---
**Arthur Wolf** *September 05, 2016 10:04*

**+Brian W.H. Phillips** « If anyone puts a project out as opensource then they loose their copyright »



This shows you have <b>no idea</b> what Open-Source and Copyright are ...



We keep the Copyright no matter what happens. And putting it out as Open-Source just means we are letting people use the source <b>under a specific set of conditions</b>. That's it.



If those conditions are not respected ( which is the case with MKS ), the license, copyright, and wishes of the community/volunteers, is not being respected. That is a bad thing we should fight against, not a fatality.



« it maybe that they think when the project is well tested and proven, that they can call it something else and sell it to big company. »



I'm not sure what you mean here. Nobody intends to sell anything.



« it is no good complaining after the horse has bolted! »



What does that mean ?


---
**Brian W.H. Phillips** *September 05, 2016 10:36*

Well I guess you are all correct and I am wrong. But is it any good

complainingl about the Chinese ripoffs that people have already bought? It

is disappointing for you and those who have bought the mks board as in my

case. I bought the mks board as when I looked for smoothy that was what I

first found, of course you will say I should have looked deeper but as I

was only interested in trying smoothy I thought the low cost was worth it

for trial purposes. As I have had problems with the mks board, I will

layer, when I have saved a bit of cash, buy a real smoothy from you


---
**Arthur Wolf** *September 05, 2016 10:51*

**+Brian W.H. Phillips** It's worth "complaining" because some people do not know MKS is not open-source, and will not buy it if they know it is not open-source. We have seen this happen many times.


---
**Anton Fosselius** *September 05, 2016 12:36*

I see the smoothie project as two projects, smothieware (software) and smoothieboard (hw). 



What license are used for the two?



Are MKS clones or their own design? Is the fw the same or a fork that is not up streamed? 



Regarding the license, what does it permit? 



If I build an open hardware platform can I run smothieware on it?



If I sell an open platform can I have smoothiware pre-flashed? 



If I build a closed hardware platform can I run smothieware on it?



If I sell a closed hardware platform  can I sell it with smothieware pre-flashed?



What if I buy a MKS board and make FW changes that I upstream? ( preferably with a bribe/contribution to the FW developers )?




---
**Anthony Bolgar** *September 05, 2016 13:06*

I think it is time to write up an "Open Source For Dummies" handbook. The biggest misconception a  lot of users have is that Open Source means that anybody can take it and do what they want with it. This can not be further from the truth. There is a license that it is released under and this license must be respected and followed. Otherwise you are out right stealing. Also Open Source does not mean non-profit, does not mean that there cannot be paid employees, does not mean that everyone who works on it is a volunteer (even though a great number of Smoothie contributors are volunteers)and definitely does not entitle the world at large to claim it as there own. What it does mean is that the curtain has been drawn back and the wizard is exposed (Wizard of OZ reference....lol)

It allows someone to build there own based upon the publicly released files. Why would anyone want to do Open source projects? The best reason is that it fuels innovation for the product, when users can modify the project and make it better. If the Open Source licensing is followed not just to the letter of the law, but in the spirit it was intended in the first place, everyone wins. A good example of this is the LasserWeb project. Smoothieboard was a catalyst for **+Peter van der Walt** to create LaserWeb, because he could not find a software package that worked with smoothieboard to his liking. And LaserWeb is helping to drive changes to Smoothieware. **+Wolfmanjm** has been adding features to the firmware based upon needs of the LaserWeb project. Each project drives the other project forward.


---
**Arthur Wolf** *September 05, 2016 13:31*

> I see the smoothie project as two projects, smothieware (software) and smoothieboard (hw).



It's a single project.



> What license are used for the two?



GPL v3 for the firmware, CERN OHL and GPL v3 for the hardware



> Are MKS clones or their own design? 



It's a derivative, by their own admission. They have very little idea what they are doing, as the many many problems in their very first design show. They wouldn't have been able to do the design without the Smoothieboard as a guide, and even then they got much help from the community without which their design would have been worthless. Note that the people from the community who helped them did so under the false impression that the design was Open-Source, before this was clarifie.



> Is the fw the same or a fork that is not up streamed?



It's a fork with very minimal modification. You can technically use the mainstream firmware on their borad too.



> Regarding the license, what does it permit?



You'll have to read it or ask a specific question ...



> If I build an open hardware platform can I run smothieware on it?



Yes.



> If I sell an open platform can I have smoothiware pre-flashed?



Sure.



> If I build a closed hardware platform can I run smothieware on it?



Yes, but that's a dick move. You legally can, but it's going agains the wishes of the people who open-sourced it in the first place.

You <b>legally</b> can, the same way you legally can insult people who just helped you in a big way.



> If I sell a closed hardware platform can I sell it with smothieware pre-flashed?



Same thing.



> What if I buy a MKS board and make FW changes that I upstream? ( preferably with a bribe/contribution to the FW developers )?



That's better than if you don't. But that's not going to be the colossal majority of MKS purchases.


---
**Jeff DeMaagd** *September 05, 2016 13:34*

**+Daniel Seiler** That may well be but that still doesn't absolve them of failing to or choosing to not provide design files or code changes like the license stipulates.


---
**Arthur Wolf** *September 05, 2016 13:35*

**+Daniel Seiler** « Imagine that the next generation of smoothieware contributors are making their first steps with an sbase board somewhere in a chinese tier 3 city »



You can always imagine things, imagination is great. I don't think this matches with any reality though. If the future proves me wrong, I'll be glad.

However I'm not sure this hypothetical is worth sacrificing the currently working system when Smoothieboard sales finance future development ...



Also, we sell plenty of Smoothieboards to China : it's not <b>that</b> expensive ... You know China's standard of living is increasing all the time right ? I'd get the "for this market" argument for Africa, but it gets less and less valid for China all the time.


---
**Jeff DeMaagd** *September 05, 2016 13:56*

**+Daniel Seiler** I didn't say you were. I was offering an explanation to what they were doing wrong. Doing good doesn't make up for doing wrong, especially when doing the right thing is pretty easy here.


---
**Anthony Bolgar** *September 05, 2016 14:08*

I started this post to educate the users out there that were unaware of the problems buying an MKS board can create. It was not intended to create a divide in the community. Please be civil with one another, but please continue discussing this matter if you have any more questions, concerns, or alternate points of view. Civil discussion leads to education, and that was my goal.


---
**Stephen Baird** *September 05, 2016 16:18*

I didn't know Smoothieboard clones existed, although I shouldn't be too surprised. Good open source designs tend to get copied by Chinese manufacturers, copying existing products has been a business model in Chinese manufacturing for a very long time now and open source designs cut out the need for in-house expertise in making the copy. Can't say I'm tempted by them, though. I'd much rather spend a few extra bucks and support Smoothie.


---
**Arthur Wolf** *September 05, 2016 16:19*

**+Stephen Baird** \o/


---
**Alex Krause** *September 05, 2016 16:21*

I bought my 4xc from Uberclock because I knew I would get support from the community when I ran into problems and knew the components used on the boards wouldn't be "knock-offs". The added cost for support has more than paid for itself. I have looked through the smoothie forums and notice that members aren't keen on helping you with an MKS issue. And having a board that has gone thru an extensive QC test is awesome because nothing is more frustrating than getting a project going and being held back because of a component was faulty from the get go


---
**Thomas Sanladerer** *September 05, 2016 16:39*

I feel like this discussion could use **+Jan Wildeboer**​'s voice 


---
**Jan Wildeboer** *September 05, 2016 17:23*

I will first have to take a closer look at the licensing before I can swing The Mighty Sword.



Hm. [http://smoothieware.org/smoothieboard](http://smoothieware.org/smoothieboard) tells me it's "all Open Hardware (GPL licensed). But the GitHub repo at [https://github.com/Smoothieware/Smoothieboard](https://github.com/Smoothieware/Smoothieboard) switches that to CERN OHL v1.2 or later. That's already a mismatch that needs to be fixed IMHO.



Is Smoothie/Smoothieboard a registered trademark? Is there some kind of documented diff of what MKS has changed? I see you guys are calling it a derivative, so I guess it's not the original BOM/layout?


---
**Stephanie A** *September 05, 2016 18:55*

Funny thing about open source hardware, because the design is utilitarian and not artistic, then people are free to make similar designs with any license they wish. If it was patented, then you would be eligible for royalties or legal action. Since it is open source what you are licencing is the design files, not ownership of the usage or function. If smoothie is trademarked, then you can deny usage of that trademark. Otherwise  the functional design can be replicated with zero attribution or licensing. 

The gray area occurs if the designers used your open source licensed files to create a closed source derivative. If the files were directly involved in design of the derivative, then it's possible that the license was violated. However this has yet to be used in court for an open source hardware license. 



If you want your hardware protected, use a patent and license the usage. 


---
**Jan Wildeboer** *September 05, 2016 19:05*

**+Stephanie S** you cannot patent a board design. But you can enforce share-alike licenses. Patents are useless. 


---
**Arthur Wolf** *September 05, 2016 19:16*

**+Stephanie S** The fact that open-hardware licenses are hard to enforce is pretty well known at this point.

The thing is : we are not saying MKS is illegal, we are saying they are pissing on the project. We did not open-source with the intent of MKS being able to do what they are doing, we had other intents. And MKS is pissing on those ...

Pretty much ... :)


---
**Jan Wildeboer** *September 05, 2016 19:30*

**+Arthur Wolf** they said the same about the GPL. I did just that. Fought for the GPL in court. Back in 2000. And we won. But before we go that road, what exactly is MKS accused of? Did they make changes without giving back? Or are they just taking the plans and spit out 1:1 copies at a very low price?


---
**Arthur Wolf** *September 05, 2016 19:37*

**+Jan Wildeboer** They made a derivative, which does the same thing. I think they would be successful arguing it's a new design, even though many things show they wouldn't have been able to do that design without having the Smoothieboard design. Their derivative is not Open-Source, even though it's a derivative of the Smoothieboard which is Open-Source.

On top of that, many users get their board just <b>assuming</b> it's Open-source because it's obviously descending from the Smoothieboard. We've had many cases of users regretting their order or even trying to get their money back once they learned the board was closed. And then on top of that, they don't produce proper documentation, they don't provide user/customer support, they don't participate in the project, etc. It's all taking, no sharing. And sharing was one of the most important things to the project and to the people who created it and improved it.



But as I said, our argument against them is not a legal one. The argument is that we didn't share the files with the intent of the files being used that way. And while we think it's probable that what they are doing is legal, most of the many contributors to both the firmware and the hardware feel like MKS is not respecting their work and wishes.



This is a matter of them being dicks, not of them stealing.


---
**Stephanie A** *September 05, 2016 19:39*

I totally agree that they are abusing the spirit of the license. If you want to fight back, there are ways you can do it (like discontinue support) but that only hurts the end users. 



**+Jan Wildeboer**​ you can't enforce share-alike unless you can prove the source material was used. Just like how you can see thousands of works that have the same functionality but were independently developed. Legal costs to prove a derivative are prohibitive for most open source projects, and even then an outcome is not guaranteed. Only the most obvious cases would be worth pursuing. Information is the best route, including boycotts and campaigning against the offender. 


---
**Jan Wildeboer** *September 05, 2016 19:46*

**+Arthur Wolf** I guess there is no direct communication between the Smoothieboard community and MKS. I also guess that the changes they made are of the cost saving kind. Now. Just calling them dicks doesn't help in any way. So here's my proposal. #1 register a trademark. When you are in Europe that'll cost you some 700€. Happy to pay for that out of my own pocket. Equipped with that trademark you can immediately stop the "Smoothieboard compatible" claim. #2 try to establish direct contact. Again. I can happily provide contacts in Shenzen for that. What exactly is the contribution you expect from them? I'm looking at a solution. Not at shouting ..,


---
**Jan Wildeboer** *September 05, 2016 19:47*

**+Stephanie S** you didn't answer my question how a patent would help when it is about a specific BOM and a board layout. While I pointed out that I actually went to German court and enforced the GPLv2 ...


---
**Arthur Wolf** *September 05, 2016 19:52*

**+Jan Wildeboer** There has been plenty of communication, they have an english-speaking advertise-to-community guy. What do you think we should have been communicating with them about ? ( we mostly asked if it was open-source, which they said it isn't, because they don't want chinese cloners to copy it, ironically ).

They also say they aren't interested in helping or contributing in any way, because they don't have to.



Also, the changes were not of the cost-saving kind, that I know of. It's just a different but mostly equivalent board, clearly derivative from the Smoothie design.


---
**Jan Wildeboer** *September 05, 2016 19:55*

**+Arthur Wolf** so what exactly do you want from them? I'm not being passive-aggressive or sth. I just want to know and understand how what MKS is doing is detrimental. Are their boards technically incompatible to your reference design? If yes how? Because that would give you a string argument against their compatibility claim. 


---
**Arthur Wolf** *September 05, 2016 20:00*

**+Jan Wildeboer** We want them to do the following :

* Be Open-Source

* Provide proper customer support via email

* Ideally also do so in the community channels

* Participate in hardware or firmware dev, or both, either with time or money

* Provide proper documentation for their board

* Participate in the documentation effort for the project as a whole



The other Smoothie-compatible board sellers do this to varying degrees.

They do none of it ( they have been asked ), and are unapologetic about it, which is pissing a lot of contributors off.


---
**Jan Wildeboer** *September 05, 2016 20:05*

**+Arthur Wolf** understood. So it's IMHO #1. File a trademark and enforce it. That will give you the power to stop them from claiming compatibility. 


---
**Arthur Wolf** *September 05, 2016 20:19*

**+Jan Wildeboer** We have contacted a few resellers of MKS and "acted" as if Smoothieboard was our trademark, and some have removed the mention of it from their product page. However, MKS themselves, and their biggest distributors don't care even though they think we have a trademark.

I guess that's the situation in which we actually need to have the trademark for real ... we have delayed filing it for two reasons : 

* It costs money, and time

* We want a "smoothie foundation" to hold the trademark, not a company, but that foundation doesn't exist yet.


---
**Jan Wildeboer** *September 05, 2016 20:25*

**+Arthur Wolf** I know the situation all too well. Feel free to contact me directly to help. My offer to help is honest. Check my credentials. And the foundation thing is complex. Not needed IMHO. There are simpler solutions. And I'll happily chip in a few hundred € to help. 


---
**Arthur Wolf** *September 05, 2016 20:26*

**+Jan Wildeboer** Can you ping me at wolf.arthur@gmail.com with a small explanation of why the foundation is not a good idea ? It's our current plan so if there's a problem with it I want to know :) Also thanks a lot for offering to help.


---
**Jan Wildeboer** *September 05, 2016 20:29*

**+Arthur Wolf** will do. 


---
**Michaël Memeteau** *September 06, 2016 09:57*

**+Arthur Wolf** Curious to know if AZSMZ situation is comparable to the one of MKS... It looks the same to me. I bought one of their board at a time where all of this wasn't as clear as it is to me now (so what you're doing isn't useless. Education helps for sure). As for support, I wasn't able to get a decent response from them about why the SD6128 driver from Panucatt  wasn't working on their board (AZSMZ-Mini has no current control and no network interface. I guess that would be the main difference with the original SmoothieBoard). My next buy will be different, it's a promise...




---
**Arthur Wolf** *September 06, 2016 10:06*

**+Michaël Memeteau** The situation with AZSMZ is the same, though the design is so incredibly sucky and flawed, it hasn't gained much traction, probably in part due to how many customers have had problems with usb disconnects and burnt tracks ( see forums ).


---
**Wolfmanjm** *September 10, 2016 00:07*

additionally the AZSMZ board does not use mosfet drivers and they use mosfets that canoot switch reliably at 3.3v, so the Mosfets burn out or stop working very quickly.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/i6Tf2mcBsfe) &mdash; content and formatting may not be reliable*
