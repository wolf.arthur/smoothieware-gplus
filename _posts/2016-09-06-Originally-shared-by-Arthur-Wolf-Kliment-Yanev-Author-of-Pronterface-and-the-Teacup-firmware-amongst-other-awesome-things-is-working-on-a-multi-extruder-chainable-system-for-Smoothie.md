---
layout: post
title: "Originally shared by Arthur Wolf Kliment Yanev ( Author of Pronterface and the Teacup firmware amongst other awesome things ) is working on a multi-extruder chainable system for Smoothie"
date: September 06, 2016 09:16
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



**+Kliment Yanev** ( Author of Pronterface and the Teacup firmware amongst other awesome things ) is working on a multi-extruder chainable system for Smoothie.



It essentially allows you to add as many ( within reason ) extruders to any machine, simply by chaining boards together ( connecting one to the next, and that one to the next, etc ).



He' just finished prototyping the first boards, and is looking it writing the firmware for it. This is a big part of the planned Smoothieboard v2 ecosystem.



You can find more information about this project at [http://smoothieware.org/todo#toc24](http://smoothieware.org/todo#toc24)

![missing image](https://lh3.googleusercontent.com/-YY_OKbcKPos/V86JaeJmAzI/AAAAAAAAN-k/1004sH0SlQU8L5e_ed824FPJXe4uqAPrQCJoC/s0/bZdoAix.jpg)



**"Arthur Wolf"**

---
---
**Michaël Memeteau** *September 06, 2016 09:34*

I would love to see them drive a diamond hotend... Or even a 4 input version (CMYK for better color accuracy). Anyone with me?


---
**Arthur Wolf** *September 06, 2016 09:52*

**+Michaël Memeteau** They are supposed to be able to ultimately, yes


---
**Triffid Hunter** *September 06, 2016 09:57*

Kliment has nothing to do with Teacup - I started it, and some years ago I handed it over to Traumflug who has done an excellent job of advancing the project


---
**Arthur Wolf** *September 06, 2016 10:08*

**+Triffid Hunter** Damn ... he worked on one of the firmwares though right ? I need to look this up more. Sorry for the mistake.


---
**Daniel Fielding** *September 06, 2016 10:26*

Didn't he work on Sprinter?


---
**Arthur Wolf** *September 06, 2016 10:39*

That must be what confused me ...


---
**Jeff DeMaagd** *September 07, 2016 11:18*

**+Michaël Memeteau** probably WCMYK because CMYK assumes a white surface is already there.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/MJ6hjC5LY6b) &mdash; content and formatting may not be reliable*
