---
layout: post
title: "Originally shared by Arthur Wolf THE SMOOTHIE PROJECT NEEDS YOUR HELP"
date: September 07, 2016 22:37
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



THE SMOOTHIE PROJECT NEEDS YOUR HELP.



Smoothie is made by the Smoothie community. We need your help with many things to improve and push the project forward.

If you feel like Smoothie is super nice, and you want to give back, or just do something interesting, your help is extremely welcome.



We need help with many different things, so it's likely your special talent/knowledge will be useful.

You can see a list at : [http://smoothieware.org/todo](http://smoothieware.org/todo)



If you feel like helping the Smoothie project is something you want to do, please email me at wolf.arthur@gmail.com



Thanks a lot to all in advance.



Cheers.











**"Arthur Wolf"**

---
---
**Arthur Wolf** *September 07, 2016 22:48*

Also : please reshare. this is important :)


---
**Anton Fosselius** *September 08, 2016 05:38*

Hmm. Might end up contributing something down the line. Have a 3d printer, k40 laser cutter and are waiting for a CNC mill. And are working as a firmware developer for a living. Will probably move all devices to Smothieware ;)



What I do not have is time.. but some day maybe ;)


---
**Arthur Wolf** *September 08, 2016 08:26*

**+Anton Fosselius** Thanks for the intention, ping me as soon as you have some time :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/c8detf5KN5E) &mdash; content and formatting may not be reliable*
