---
layout: post
title: "Do you speak Chinese ? The kind of taobao ?"
date: September 09, 2016 10:06
category: "General discussion"
author: "Arthur Wolf"
---
Do you speak Chinese ? The kind of taobao ?

We need a Smoothie community member to help us send a message to somebody, if you would be able and willing to do that, please email me at wolf.arthur@gmail.com.



Thank you very much in advance.



Cheers.





**"Arthur Wolf"**

---
---
**Douglas Pearless** *September 09, 2016 10:14*

My daughter who speaks a bit of Mandarin suggest finding a tabao agent ( and check out reviews of them online ) 


---
**Arthur Wolf** *September 09, 2016 10:36*

It's not for buying though, it's more for requesting they ( a specific seller there ) stop violating our trademark. I could find a chinese lawyer and stuff, but sometimes just politely asking works, so i thought I'd try that.


---
**Douglas Pearless** *September 09, 2016 10:50*

As Taobao is owned by the alibaba group, suggest you consider [http://ipp.alibabagroup.com/](http://ipp.alibabagroup.com/) <[http://ipp.alibabagroup.com/](http://ipp.alibabagroup.com/)>  (click on the English button in the top right hand corner.


---
**ekaggrat singh kalsi** *September 09, 2016 10:56*

in beijiing right now.( work here not a chinese ). can contact taobao.. but they wont listen from my past experience... i can try  


---
**Arthur Wolf** *September 09, 2016 11:19*

**+Douglas Pearless** I tried that many times it never worked. The only thing I've seen work is contacting the seller directly.



**+ekaggrat singh kalsi** Would you mind emailing me at wolf.arthur@gmail.com ? Thanks a lot !


---
**Christian Lossendiere** *September 09, 2016 11:32*

Salut Arthur, ma femme est Taiwainaise et parle Chinois tu peux m'appeller au 06-31-07-37-50 elle veux bien t'aider




---
**Xiaojun Liu** *September 09, 2016 11:42*

I'm native Chinese. If you have any need，please let me know！


---
**wei liu** *September 09, 2016 15:49*

i can speak Chinese and iam Doing Business on Taobao over 5 years what can i do for you 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/ADXGUvAz1zV) &mdash; content and formatting may not be reliable*
