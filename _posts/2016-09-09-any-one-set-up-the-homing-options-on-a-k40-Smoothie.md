---
layout: post
title: "any one set up the homing options on a k40 Smoothie"
date: September 09, 2016 16:07
category: "General discussion"
author: "Brian W.H. Phillips"
---
any one set up the homing options on a k40 Smoothie. I wish it to home when owered up. Anyone help please, thank you in advance





**"Brian W.H. Phillips"**

---
---
**Jeff DeMaagd** *September 09, 2016 16:16*

Put your homing sequence as g code in a g code file, and have the config file run it on bootup. See the first section here:



[smoothieware.org - Player - Smoothie Project](http://smoothieware.org/player)


---
*Imported from [Google+](https://plus.google.com/104656853918904556475/posts/XmBsvoHkCxB) &mdash; content and formatting may not be reliable*
