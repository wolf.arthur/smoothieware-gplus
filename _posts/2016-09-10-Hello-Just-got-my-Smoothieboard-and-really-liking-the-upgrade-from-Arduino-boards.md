---
layout: post
title: "Hello! Just got my Smoothieboard and really liking the upgrade from Arduino boards"
date: September 10, 2016 04:01
category: "General discussion"
author: "Cid Vilas"
---
Hello!  Just got my Smoothieboard and really liking the upgrade from Arduino boards.  Im taking a Prusa i3 style printer to the Smoothieboard.  These printers have two steppers for Z.  Normally i would tie the two into a splitter and drive them from the same driver.  Is it possible to configure Smoothieboard to mirror the axis onto another driver output?  I have an unused axis. :)  Thanks in advance





**"Cid Vilas"**

---
---
**Brian Bland** *September 10, 2016 04:46*

[smoothieware.org - 3D Printer Guide - Smoothie Project](http://smoothieware.org/3d-printer-guide#toc27)



Look here for how to.


---
**Cid Vilas** *September 10, 2016 05:01*

**+Brian Bland** Thanks!  That works.


---
**Arthur Wolf** *September 10, 2016 08:39*

This is what you want to do : [smoothieware.org - 3D Printer Guide - Smoothie Project](http://smoothieware.org/3d-printer-guide#toc27) :)




---
**Arthur Wolf** *September 10, 2016 08:40*

Oh, thanks **+Brian Bland** I didn't see your answer.


---
*Imported from [Google+](https://plus.google.com/107181016487248875065/posts/KZRQXi7tS26) &mdash; content and formatting may not be reliable*
