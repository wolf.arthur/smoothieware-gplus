---
layout: post
title: "Still trying to sort out the USB serial problem with data stopping after a few bytes"
date: September 10, 2016 06:22
category: "General discussion"
author: "Brian W.H. Phillips"
---
Still trying to sort out the USB serial problem with data stopping after a few bytes. Can anyone help with this plaese? The win 10 system automatically selects the overclock driver and I cannot seem to force it to recognise the smoothie as a mass storage device. When I inspect the driver log it always reports "Device USB\VID_1D50&PID_6015&MI_00\7&2d8bad9b&0&0000 requires further installation." so what is the further installation ? 

Thank you. By the way I have contributed a small amount of cash as support for the group!





**"Brian W.H. Phillips"**

---
---
**Brian W.H. Phillips** *September 10, 2016 07:18*

I would if I could buy one locally at the right price. !


---
**Brian W.H. Phillips** *September 10, 2016 07:21*

and I don't want to have to use only laserweb to run it!


---
**Brian W.H. Phillips** *September 10, 2016 07:39*

OK, thank you, I will try another aproach to the problem!




---
**Brian W.H. Phillips** *September 10, 2016 07:43*

I'll see what my spectrum analyser can find, used to be part of my prevous work EMC testing etc. Unfortunately I don't have all the gear I used to have. ut I will give it a go.




---
**Alex Krause** *September 10, 2016 08:34*

**+Brian W.H. Phillips**​ please heed **+Peter van der Walt**​'s advise I have run several really massive Gcode files on my genuine smoothie board over USB. 1.7 million line gcode files that are around 42mb. 


---
**Alex Krause** *September 10, 2016 08:38*

I have done all of that using USB. Not a single file I've done was from the sd card


---
**Arthur Wolf** *September 10, 2016 08:38*

Maybe use Ethernet instead of USB ? Lots of users report USB problems with MKS.

More generally, no matter the board, good ways to help with USB include : using a short ( 1 foot ) cable, shielded, with ferrite at both ends. Making sure you don't have a ground loop ( short cables, both machine and computer in the same power strip, not across the room ).

Also, you can try using a different computer and see if that helps.


---
**Alex Krause** *September 10, 2016 08:43*

Thanks for the tip on the USB cable **+Arthur Wolf**​ I'm using the cable that came with my K40 it's a 12 footer sheilded but only has one ferrite choke on it... I will see if a better (shorter) cable will make my 4xc preform even better than it already does


---
**Roberto Fernandez** *September 10, 2016 10:47*

**+Brian W.H. Phillips**​ for my mks does not work fine with Ethernet, but usb with win10 works fine with a short cable.


---
*Imported from [Google+](https://plus.google.com/104656853918904556475/posts/XKhD9qY54Rz) &mdash; content and formatting may not be reliable*
