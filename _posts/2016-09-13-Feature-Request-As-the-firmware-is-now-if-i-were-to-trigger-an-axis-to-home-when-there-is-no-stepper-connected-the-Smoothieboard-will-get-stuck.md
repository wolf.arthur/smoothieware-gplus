---
layout: post
title: "Feature Request: As the firmware is now, if i were to trigger an axis to home when there is no stepper connected, the Smoothieboard will get stuck"
date: September 13, 2016 23:08
category: "General discussion"
author: "Cid Vilas"
---
Feature Request:  As the firmware is now, if i were to trigger an axis to home when there is no stepper connected, the Smoothieboard will get stuck.  I had an axis i had sitting on the end stop. When Home was called, Smoothieboard would just sit there.  Locked up until i reset.  It would be nice if the firmware would see that it only has a certain travel length.  When that length is traveled and no flag has been seen, pop an error out and allow communications again.  Am i right, or am i missunderstanding what is happening here?  Thanks!





**"Cid Vilas"**

---
---
**Douglas Pearless** *September 14, 2016 00:12*

This is something I have discovered too,  for example, when homing there does not appear to be code to stop the travel if the endstop is never hit (e.g. Broken belt, stepper failure, mechanical issue etc). It is something I am mulling over as I am assisting in the Smoothie2 firmware migration.


---
**Cid Vilas** *September 14, 2016 00:28*

Agreed.  This was a difficult issue to resolve and i spent a few hours trying to figure it out on my first Smoothieboard.  When i'd home X and Y, it was fine, but when i sent a general home command it would lock up immediately.  Very frustrating.  Luckily a google search turned up someone else who had this very issue.  Really, considering the axis lengths are in the config file, there is no reason not to use this information to determine whether the axis hasnt already gone too far and there is an issue.  Just my two cents. :)


---
**Jeff DeMaagd** *September 14, 2016 04:25*

Did you set axis dimensions in the config file? I thought it gives up after axis length / homing speed. Another trick might be to bump the switch a couple times.


---
**Arthur Wolf** *September 14, 2016 09:05*

This was added a while back, make sure you upgrade both your firmware and your config file to the latest edge.

Thanks **+Wolfmanjm** :)



Cheers.


---
**Cid Vilas** *September 14, 2016 12:19*

Hmm.  I thought I did.  Since configuration files are unique to each machine, how do you upgrade it to edge? I am running the latest firmware though.


---
**Jeff DeMaagd** *September 14, 2016 12:32*

You take a copy of the latest config file sample and plug in the parameters from the old file. Some parameter names have changed and there are new parameters now too.


---
**Wolfmanjm** *September 14, 2016 17:50*

yea current edge has a max_travel setting for each actuator, it is documented in the wiki under the endstops section. Also even with master if you hit the kill button or type control X it will abort the homing procedure.


---
**Cid Vilas** *September 14, 2016 18:58*

Are there updated configuration files for various setups? For example delta, Cartesian, etc.  I only see one configuration download option on the site.

Also, has there been a Configurator made for smoothie? Like what repetier has for their firmware?


---
**Jeff DeMaagd** *September 14, 2016 19:47*

[https://github.com/Smoothieware/Smoothieware/tree/edge/ConfigSamples](https://github.com/Smoothieware/Smoothieware/tree/edge/ConfigSamples)


---
*Imported from [Google+](https://plus.google.com/107181016487248875065/posts/XYz2DPdFtgX) &mdash; content and formatting may not be reliable*
