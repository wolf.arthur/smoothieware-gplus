---
layout: post
title: "Originally shared by Arthur Wolf 5mm green pluggable screw terminals on all sides"
date: September 13, 2016 21:23
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



5mm green pluggable screw terminals on all sides. Metal box. Opinions ?

![missing image](https://lh3.googleusercontent.com/-MS128r4b9Ms/V9huQ-HGxlI/AAAAAAAAOEQ/4Fczc0ZC4ZUoPeJ62QVCbKjQN_2O_CoCgCJoC/s0/Smoothie-central.png)



**"Arthur Wolf"**

---
---
**Jeff DeMaagd** *September 13, 2016 22:30*

Looks pretty thorough. 5mm seems bigger than it really has to be. Most of that is low power stuff. The next size down would be plenty for most of the connections.


---
**Arthur Wolf** *September 13, 2016 22:35*

**+Jeff DeMaagd** This is meant as a drop-in replacement for chinese CNC and Laser controllers. The 5mm connector thing is what we are replacing, so it's easier for people to use it everywhere. The 3.5mm alternative is also a bit more annoying to use.


---
**Anthony Bolgar** *September 13, 2016 23:11*

Looks good to me.


---
**Dean Rock** *September 13, 2016 23:14*

Nice opportunity to label each terminal to save any confusion about what goes where without a diagram.




---
**Jim Christiansen** *September 14, 2016 02:33*

When I read above about the metal box I thought... Plasma controller???  I run a 5x10 plasma water table and Mach3 that uses stepper motors.  The options for electronics are still limited.


---
**Jeff DeMaagd** *September 14, 2016 03:56*

**+Jim Christiansen** Sounds like a good candidate for Smoothie


---
**Jeff DeMaagd** *September 14, 2016 03:58*

**+Arthur Wolf** Are there pins/room left for LCD header, or is this just the terminal diagram?


---
**Anton Fosselius** *September 14, 2016 04:28*

Sounds expensive? But high quality ;) 


---
**Christian Lossendiere** *September 14, 2016 06:05*

good




---
**Arthur Wolf** *September 14, 2016 09:07*

**+Jeff DeMaagd** We don't really intend this to run any kind of LCD. The two gadgeteer ports should be able to run one though.

For interfaces, this is more intended to talk to something like fabrica.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/JXNVJUhL7nj) &mdash; content and formatting may not be reliable*
