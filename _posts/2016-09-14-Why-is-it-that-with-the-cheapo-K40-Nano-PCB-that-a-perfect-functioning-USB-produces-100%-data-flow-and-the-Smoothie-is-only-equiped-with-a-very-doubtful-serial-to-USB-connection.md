---
layout: post
title: "Why is it that with the cheapo K40 Nano PCB that a perfect functioning USB produces 100% data flow and the Smoothie is only equiped with a very doubtful serial to USB connection?"
date: September 14, 2016 10:08
category: "General discussion"
author: "Brian W.H. Phillips"
---
Why is it that with the cheapo K40 Nano PCB that a perfect functioning USB produces 100% data flow and the Smoothie is only equiped with a very doubtful serial to USB connection?

 I cannot see a reason why, unless it is the cost of USB licencing!





**"Brian W.H. Phillips"**

---
---
**Triffid Hunter** *September 14, 2016 10:32*

Smoothie has native USB, how do you think we implement multiple serial ports, SD card access and DFU update over USB? Have you done any research at all into Smoothie?


---
**Brian W.H. Phillips** *September 14, 2016 10:39*

Perhaps you can explain why so many users have problems with graphic input over USB!


---
**Arthur Wolf** *September 14, 2016 10:44*

I'm not sure what you mean, Smoothieboard has native USB. On that we implement serial ( at full speed ), mass storage, dfu. What's the problem ?


---
**Arthur Wolf** *September 14, 2016 10:45*

**+Brian W.H. Phillips** What does "graphic input over USB" mean ? Also, what sorts of problems exactly ? We definitely don't have any widespread reports of USB issues. Some users have to adjust their setup to mitigate issues with EMI or similar interference issues, but they always end up with a perfectly working setup after taking a few steps.


---
**Brian W.H. Phillips** *September 14, 2016 10:51*

I have been told that it is an inherrent problem with Smoothie USB and that I must try better USB leads and use chokes etc. But none were helpful. So my problem may be that I have a klone smoothie and must go to the expense of a genuine smoothie board to cure the problem. I don#T mind doing that if I can be gauranteed no USB problems!


---
**Arthur Wolf** *September 14, 2016 10:56*

You can never be guaranteed no USB problems. However unless you live 3 meters from the LHC rings ( understand : in a very electromagnetically noisy environment ), it's very unlikely problems can't be mitigated.

I think we see more reports of USB problems with the MKS than with the Smoothieboard, but it's hard being absolutely sure there is a relevant conclusion to be taken there. It's possible their design is correct too but we see more reports because they don't do any customer support.

Smoothieboard definitely is designed the correct way, a lot of care was taken to that, and it's existed for years now and we are still to see a user for whom we can't get USB to work correctly.



You still don't say <b>exactly</b> what your problem with USB is though ... that would be helpful.


---
**Brian W.H. Phillips** *September 14, 2016 11:07*

OK understood thank you: The problem is that after a few bytes of data the smoothie board stop engraving and then eventually returns to start mode. The amount it scans before stopping is dependant on the data density. This problem is constant with graphic data and only occurs occasionally with large detail cutouts. I had the impression that the Smoothie ram was full (if indeed the ram is being used as data short term store) or no buffer space etc.

I am using visicut which I find to be a user friendly program.


---
**Triffid Hunter** *September 14, 2016 11:08*

So wait, we've gone from "smoothie only has usb-serial" to " I have an ostensibly smoothie-compatible board of questionable design and the usb connection is crappy no matter what I do" ?



I've regularly had connection uptimes of literally months to my smoothies (genuine but home-assembled) simply by using a decent USB cable and sensible host; never tried a clone but I hear they can be pretty bad..



I've seen one clone that routed the USB traces under a stepper driver which frankly is an <b>excellent</b> way to have incessant USB problems due to the high current high dv/dt signals that stepper drivers produce.. I've also seen boards with a hopelessly inadequate amount of decoupling capacitance which is another excellent way to ensure constant, unsolveable problems.



We were reasonably careful with the design of smoothieboard wrt noise and interference, but we have absolutely no way to control the design of other's clones...


---
**Arthur Wolf** *September 14, 2016 11:15*

**+Brian W.H. Phillips** Visicut's Smoothie support is extremely new, I don't expect it to be ready for general use. That's your problem, extremely likely.

The method that should work best for you at this point, is instead of doing "execute", you "export" the file as gcode. Then paste that gcode onto the Smoothieboard sd card, then execute it with "play /sd/file.gcode".


---
**Brian W.H. Phillips** *September 14, 2016 11:37*

Many thanks I will try that.


---
**Johan Jakobsson** *September 14, 2016 14:12*

You'll get better and faster support the more accurate you are in your problem description. 

If you've got a MKS Base you don't have a Smoothieboard which is a product of it's own. You have a board that runs smoothiware.




---
**Brian W.H. Phillips** *September 14, 2016 14:38*

Tell me where I can buy a smoothie board in the EU at a sensible price, and I will buy one!


---
**Arthur Wolf** *September 14, 2016 14:47*

**+Brian W.H. Phillips** Robotseed is in the EU and should be back in stock next week.


---
**Hakan Evirgen** *September 14, 2016 19:38*

you can also buy from [filafarm.de - Filafarm](http://www.filafarm.de)




---
**Wolfmanjm** *September 15, 2016 05:45*

yea you get what you paid for. You bought a cheap chinese clone, you have usb problems (not surprising as it is a 2 layer board). Please don't tar smoothie with your problems. go talk to MKS.


---
**Brian W.H. Phillips** *September 15, 2016 05:46*

Very kind thank you


---
**Wolfmanjm** *September 15, 2016 05:55*

I'm curious why you expect to get support from us? You bought a board from a company that leaches off our hard work. They do not support the ecosystem, they help to actually destroy it (this thread is a perfect example, you point the finger at smoothie when the problem is the cheap board you bought). What do you expect? Why should we support a board that blatantly does not support us or help the project in any way?


---
**Brian W.H. Phillips** *September 15, 2016 06:06*

Why are you so arrogant young   

man.you tell everyone that smoothie is open source and then claim that all the design and development questions and answers should only refer  to your product.if you want your product to be unique then complete it and test it 100% and then sell it as a package.




---
**Wolfmanjm** *September 15, 2016 06:29*

I really do not think it is worth arguing with this guy. He appears to be totally clueless. Blames S/W for a H/W problem. and calls me "young man" When I have been in the business for over 40 years. (Wish I was a young man :)


---
**Brian W.H. Phillips** *September 15, 2016 06:31*

Engineers should confer politely with one another not try to get one up all the time. I  asked some questions that are general to the smoothie project, and when I mention that I have bought an MKS board I  am then treated like an idiot. I assure you I am not an idiot, I just happened to take an interest in the smoothie development and as I needed somewhere to start I saw a smoothie board advertsed and bought it. That may not have been a sensible move, but I had very little info t go on. Because I found some problems I asked people here, that was obviously a mistake!

I appreciate that many of you have spent a long time and much effort on the smoothie project and find that very satisfying as I am aware of the  types of problems involved .


---
**Wolfmanjm** *September 15, 2016 07:01*

Ok so to be clear I personally have no issue with you buying an MKS (although **+Arthur Wolf** certainly does as it directly affects him). My issue is that I do not really want to have to support MKS users, I think MKS should support their customers. As far as your question I see it was answered by someone (**+Triffid Hunter**) who has more knowledge on the issue than anyone as he designed much of the current smoothieboard H/W. He pointed out your issue was with bad PCB design and routing of the USB lines. As such there is nothing we can do in the firmware to fix your issue.

As for people jumping on the MKS issue, yes it is unfortunately a hot topic, and we take every opportunity to try to explain why buying an MKS hurts the Smoothie project as a whole. I apologize for getting overly heated in this argument and should have just left it be.


---
**Wolfmanjm** *September 15, 2016 07:06*

Oh and **+Triffid Hunter** also wrote all the USB stack in Smoothieware, so is very knowledgeable about that. I think we were insulted by the phrase "a very doubtful serial to USB connection" insinuating that the fault lies with the designers of smoothieboard or the writers of smoothieware. Neither of which is the case in this instance.


---
**Brian W.H. Phillips** *September 15, 2016 07:11*

Thank you and I appreciate your fair answer. I understand your feelings about the klone mks.i also appreciate the advice you and several of others have given .I will also appologise for the young man but as I am 80 years old I may have been in order to say that.﻿you may call me old man! 


---
**Wolfmanjm** *September 15, 2016 07:42*

Well I hope we answered your questions :) I have to say that maybe if the question had been phrased slightly differently you may have only got the fair answers and none of the nasty ones :) I may be reading something into the way the question was phrased that was not intended, but I suspect if the question had been simply "why does my MKS USB disconnect frequently", without the implication that it was a Firmware failure, the answers would have been less harsh, just a thought :)

and again my apologies for getting heated, it was unjustified in this case.

Also some of the suggestions here may be worth trying...

[http://smoothieware.org/troubleshooting#toc16](http://smoothieware.org/troubleshooting#toc16)

The MSD in windows can cause the whole USB to reset which would also disconnect the serial.


---
**Brian W.H. Phillips** *September 15, 2016 07:56*

Thank you Wolfmanjm, I agree and say sorry that I worded my request open and so as to say all smoothies had this problem, I will try to be more thughful how I phrase queations in future. kind regards


---
**Arthur Wolf** *September 15, 2016 07:56*

Well, what I take away from this is that Smoothie is so great 80 year-olds use it.

:)


---
**Brian W.H. Phillips** *September 15, 2016 10:33*

Just a personal note: being 80 means a life of experience and always learning something new. If one retires and doesn't use the brain then one dies very quickly: I try to keep up with as much as I can! Yes the Smoothie is great and deserves to go further, keep at it please.


---
**Arthur Wolf** *September 15, 2016 10:37*

**+Brian W.H. Phillips** I guess what makes it note-worthy is that CNC control wasn't around when you were born, so we find less people of your age in our community. The same way there aren't many 80-year old web developpers I guess. There are a few, they are people who learned this stuff when they already had learned something else.


---
**Brian W.H. Phillips** *September 15, 2016 10:51*

Just out of interest maybe,  I started programming 6800 Motorola instruction by instruction and data step by step with a hex Dec keyboard (we didn't have assemblers and compiler's in those days ) back in the time when computers were just chips and bead memories. And 100 bit memorie was enormous. I could tell you much more but this is not the place. 


---
**Arthur Wolf** *September 15, 2016 10:53*

**+Brian W.H. Phillips** I figured something like that :) That must be quite a contrast with today ...


---
**Brian W.H. Phillips** *September 15, 2016 11:00*

Well the world has got more complicated but the actual programming much easier  (the inspiration is still required and the sleepless nights ).I still program pic.s etc. But that's all.


---
**Wolfmanjm** *September 15, 2016 20:13*

yea I first wrote  firmware for a M6800 using paper tape and an old teletype terminal :) I think it all fit in 512 bytes :)


---
*Imported from [Google+](https://plus.google.com/104656853918904556475/posts/9nuu5N9zTLF) &mdash; content and formatting may not be reliable*
