---
layout: post
title: "Hi all! I'm integrating a Smoothieboard into my K40 laser with Laserweb3 (which is AWESOME btw)"
date: September 15, 2016 19:49
category: "General discussion"
author: "Rich Barlow"
---
Hi all!  I'm integrating a Smoothieboard into my K40 laser with Laserweb3 (which is AWESOME btw).  I've been using GRBL on a Protoneer CNC shield and seperate helper Arduino to measure water temps and activate fans, vacuums, and other accessories.  It works well but the lack of power scaling with speed makes my engravings a bit sub-par.  

Right now I'm using DS18b20 i2c temp probes to measure the temp of the cooling fluid for the laser.  Instead of using a separate Arduino to measure and display temps is there a way to use the i2c bus in the Smoothieboard to display the temps?  I see there is an i2c connector on the board but I can't seem to find any info about how to actually use it and display data from it.



Thank all!







**"Rich Barlow"**

---
---
**Wolfmanjm** *September 15, 2016 20:04*

No the I2C on the LPC1769 is very noisy, and the Mbed I2C library is a blocking library. This combines to make I2C unusable on Smoothieboard at this time. You would need to use extremely short connections to any I2C device (in fact it really needs to be on board), and you would need to write a non blocking I2C driver, and then a module to read your specific temperature chip. Seems like a lot of work when a thermistor would likely do the job, and is supported.


---
**Wolfmanjm** *September 15, 2016 20:06*

You could use an arduino to read the I2C temp and have it output a voltage 0-3v or an SPI transfer that simulates the supported thermistors or SPI thermocouples. That may be the easier way if you have to use those I2C temp sensors.


---
**Rich Barlow** *September 15, 2016 20:14*

Ahh . . . so SPI is the preferred way to get this data right?  I guess the Arduino helper is the way to go for now.  

Wolfmanjm - thank you SO MUCH for all of your hard work on this product!


---
**Rich Barlow** *September 15, 2016 20:16*

Just as a note - thermistors are a bit delicate in my experience.  That's why I'm thinking about sticking with the i2c devices - they are very durable and allow fairly long cables.  Have you ever seen a decently durable thermistor?




---
**Wolfmanjm** *September 15, 2016 20:34*

I haven't really looked for one :) But mine seem to survive being stuck in a hotend for thousands of hours :)

BTW if you need an example project for converting I2C to SPI on an arduino I have one here... [https://github.com/wolfmanjm/universal-panel-adapter](https://github.com/wolfmanjm/universal-panel-adapter)


---
**Rich Barlow** *September 16, 2016 02:44*

Wow!  Awesome and thanks sir!


---
**Triffid Hunter** *September 16, 2016 03:53*

The Honeywell axial leaded ones are quite durable in my experience


---
*Imported from [Google+](https://plus.google.com/+RichBarlow70/posts/Ak5CLewC8A6) &mdash; content and formatting may not be reliable*
