---
layout: post
title: "Originally shared by Sbastien Mischler (skarab) My SmoothieWare|Board logo proposals"
date: September 16, 2016 11:56
category: "General discussion"
author: "S\u00e9bastien Mischler (skarab)"
---
<b>Originally shared by Sébastien Mischler (skarab)</b>



My SmoothieWare|Board logo proposals.

<b>If you do not like these! Go ahead and submit yours to the community :</b>



[http://smoothieware.org/logo-proposals](http://smoothieware.org/logo-proposals)



 #smoothieware     #smoothieboard     #smoothiefirmware    #smoothielogoproposals  



![missing image](https://lh3.googleusercontent.com/-kciMKD1URX4/V9vZSSCMONI/AAAAAAAAO_c/41hDSnhU2h8yhtrSOXPgNHJNFpm_HZMPQCJoC/s0/smoothie-logo-green.png)
![missing image](https://lh3.googleusercontent.com/-3OPtMoRyePw/V9vZUoIQXqI/AAAAAAAAO_c/ub4P2sv1ZTE9qmgIKrg0lDuX5l3Yd6gpgCJoC/s0/smoothie-logo.png)

**"S\u00e9bastien Mischler (skarab)"**

---


---
*Imported from [Google+](https://plus.google.com/+SébastienMischler-Skarab/posts/8GVXBHFZ7Wf) &mdash; content and formatting may not be reliable*
