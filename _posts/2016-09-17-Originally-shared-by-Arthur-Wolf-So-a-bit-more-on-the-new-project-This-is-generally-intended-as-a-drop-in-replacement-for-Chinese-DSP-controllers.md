---
layout: post
title: "Originally shared by Arthur Wolf So, a bit more on the new project : This is generally intended as a drop-in replacement for Chinese DSP controllers"
date: September 17, 2016 08:39
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



So, a bit more on the new project : 



This is generally intended as a drop-in replacement for Chinese DSP controllers. The kinds you'll find in larger laser cutters, and in CNC routers. They don't handle any power by themselves, but connect to external drivers and other peripherals and have very clean 5mm screw terminals everywhere for easy wiring.

This would be usable in any kind of machine that uses external drivers really, so this isn't just for china-conversions, but also for custom machines.



Some highlights : 

* This uses the v1 microcontroller ( LPC1769 ), but the plan is to have this be a part of the v2 series too just by changing the microcontroller ( to LPC4337 ) and a few things.

* Takes power from 12-48V and turns it into 5V for it's own logic and outputs, so you don't need a separate 5V PSU, you can just use the motors PSU.

* All connectors on the sides, designed to be enclosable in a metal box ( Which we'll optionally provide )

* RS485 ( Really "fake" modbus ) to talk to chinese VFDs, allowing you full Gcode control of the spindle. We have working code for this ( thanks bouni ! ) it's just not merged into edge yet.

* A special CAN+step+interrupt interface that allows chaining as many external "extruder" controllers as you want, allowing you to use this for a 3D printer, with as many extruders and hotends as you want. See [http://smoothieware.org/todo](http://smoothieware.org/todo) for details on that project.

* Full 5V laser outputs making wiring much easier to control laser PSUs. Also an optional 5V DAC output which should be even better than PWM for laser power control.

* 5 stepper driver step/dir outputs

* A probe port designed to handle most probing systems ( including inductive, servo + switch, and bltouch )

* Gadgeteer outputs for remaining free GPIO, so this is compatible with the new extension board system v2 will have.



What do you folks think ? Anyone would use one of those ? I will, but if I know there's going to be interest for it maybe we can accelerate it's development. Right now this is a secondary project to be worked on when things are slow on v2 ( which is not often ).



Cheers :)







![missing image](https://lh3.googleusercontent.com/-JZS4U7HsAS8/V90BQJsTi_I/AAAAAAAAOIM/C-iYvQO9F3MQrTsVXo2A6rGaTN0HbCFNACJoC/s0/Smoothie-central.png)



**"Arthur Wolf"**

---
---
**Christian Lossendiere** *September 17, 2016 10:27*

I'm interesting for drive heavy big CNC




---
**Arthur Wolf** *September 17, 2016 10:49*

Cool :)


---
**Anthony Bolgar** *September 17, 2016 11:13*

Will the VFD code be in the V1 edge or only in V2?


---
**Arthur Wolf** *September 17, 2016 11:29*

It'll be in v1. you can find it in bouni's fork


---
**Anthony Bolgar** *September 17, 2016 11:40*

Great news, I was hoping to control my VFD via software.


---
**Matt Wils** *September 17, 2016 15:28*

I'm in. 


---
**Stephen Baird** *September 17, 2016 16:46*

I could put this to good use both in an old mill refit and in an old cnc lathe refit. It would be an especially nice solution for the lathe, I think. 


---
**Arthur Wolf** *September 17, 2016 16:58*

**+Stephen Baird** Note Smoothie doesn't yet support axis/chuck synchronisation for threading.


---
**Stephen Baird** *September 19, 2016 17:57*

That's actually ok, for the time being. The solutions I've found that already support synchronization are multiple times more expensive than smoothieboard and are far more limited in most other ways. 



To be honest, I'm not sure if the lathe I'm using has a speed output for its chuck either, although it seems that if I have a model without one it can be added with some work. 



Mostly the lathe is a fun toy that got tossed into the sale of other surplus stuff, so for the time being it's just for experimentation and I have no real plans for it. 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/9JjWnheejGW) &mdash; content and formatting may not be reliable*
