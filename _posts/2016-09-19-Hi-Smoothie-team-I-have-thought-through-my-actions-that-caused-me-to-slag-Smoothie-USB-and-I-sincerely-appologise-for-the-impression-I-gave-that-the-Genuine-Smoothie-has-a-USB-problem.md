---
layout: post
title: "Hi Smoothie team. I have thought through my actions that caused me to slag Smoothie USB and I sincerely appologise for the impression I gave that the Genuine Smoothie has a USB problem"
date: September 19, 2016 05:56
category: "General discussion"
author: "Brian W.H. Phillips"
---
Hi Smoothie team. I have thought through my actions that caused me to slag Smoothie USB and I sincerely appologise for the impression I gave that the Genuine Smoothie has a USB problem.

 The Alibaba site has this heading "3D drucker 32bit Arm plattform Glatte steuerkarte MKS SBASE V1.3 open source MCU-LPC1768 unterstützung Ethernet vorinstalliert kühlkörper" plus "*Smooth * motherboard: high performanc". I am sure that you are aware of this, although the don't strictly say Smoothie they are implying it!

 Hence I fell into thier trap because of the low price etc. Further to this the internet describes the MKS board as a Klone of smoothie which it is not! If it were  a true klone it would have all the nice Smoothie features! (including a usb that works).

 Once again please accept my appologie for the way I presented my question which was completely out of order, and thank you also for your eventual understanding of my mistake.

best regards, Brian









**"Brian W.H. Phillips"**

---
---
**Arthur Wolf** *September 19, 2016 08:54*

**+Brian W.H. Phillips** It's all good. We are going to be taking more active measures to try to get those derivatives to stop using the "Smoothieware" and "Smoothieboard" terms, hopefully it'll help others in the future.



Cheers.


---
**Cid Vilas** *September 19, 2016 13:58*

**+Arthur Wolf**​ When you say you will take active measures, what do you mean?


---
**Arthur Wolf** *September 19, 2016 14:31*

**+Cid Vilas** For now, just reporting them to the websites they sell on, wherever it's justified. In the future it's possible we'll try to make it so you can't use the firmware as-is on closed-source boards ( you'll still be able to use the firmware, it'll just need to be slightly modified by the cloners ), it's something we are looking into.


---
**Brian W.H. Phillips** *September 19, 2016 14:31*

incidently, I have justpurchased a genuine Smoothie, so I hope the USB works!




---
**Arthur Wolf** *September 19, 2016 14:32*

**+Brian W.H. Phillips** If you have trouble with USB, tell us, we'll work it out.


---
**Cid Vilas** *September 19, 2016 18:11*

**+Arthur Wolf**​ I realize it's not your job to support these cloners, but what about the user base who have already purchased those boards? Can we expect future firmwares will brick our boards? 


---
**Arthur Wolf** *September 19, 2016 18:29*

**+Cid Vilas** This is only for v2 hardware and firmware, and we won't ever do anything that bricks anything. It's just possible that in the future, cloners will try to use the firmware as-is, and realize they can't, and therefore have to provide a modified version of the firmware instead of instructing their users to use the "normal" firmware. Again : only for v2


---
**Cid Vilas** *September 19, 2016 18:59*

**+Arthur Wolf**​  Relieved to hear it! Thanks


---
*Imported from [Google+](https://plus.google.com/104656853918904556475/posts/EFG9rCLb8DN) &mdash; content and formatting may not be reliable*
