---
layout: post
title: "Hi all, I've just tried out the newest release of Smoothieware-edge on my Smoothieboard driven Kossel mini"
date: September 19, 2016 16:53
category: "General discussion"
author: "Idris Nowell (Moriquendi)"
---
Hi all,

I've just tried out the newest release of Smoothieware-edge on my Smoothieboard driven Kossel mini. I'm excited about the delta grid compensation but I'm having trouble getting the config file right. Specifically the probe is crashing the bed because it's moving on a curved path between probe points.



Where would be the best place to get help with that?  





**"Idris Nowell (Moriquendi)"**

---
---
**Wolfmanjm** *September 19, 2016 20:57*

is this a delta? if so you need to start the probe much higher up, so it clears the bed on the curved trajectory


---
**Idris Nowell (Moriquendi)** *September 19, 2016 21:37*

Yes this is a delta. Is this a change from previous versions of the firmware? I didn't have this issue before the update.



Currently

 zprobe.probe_height = 5

 Is this what I need to increase?



or is it:

leveling-strategy.delta-calibration.initial_height = 10



Thanks for the quick reply.


---
**Wolfmanjm** *September 19, 2016 21:55*

probe_height should be increased


---
**Idris Nowell (Moriquendi)** *September 19, 2016 21:56*

Thanks, I'll try that in the morning


---
**Wolfmanjm** *September 19, 2016 22:06*

No it is not a change, it will depend on how badly out your delta radius is, if it is way off it will traverse in a bowl shape. If it is close it will be ok. You need to set the probe height high enough to avoid hitting the bed. It also depends on your delta_segments_per_sec which should be the default of 100.


---
**Idris Nowell (Moriquendi)** *September 20, 2016 07:47*

It was my mistake, I'd forgotten to include the delta_segments_per_sec when I made my new config file.



Thanks for your help


---
*Imported from [Google+](https://plus.google.com/103611455673036215511/posts/1HkD6dnBQJM) &mdash; content and formatting may not be reliable*
