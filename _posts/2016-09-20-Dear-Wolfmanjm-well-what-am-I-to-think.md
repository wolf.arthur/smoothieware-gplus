---
layout: post
title: "Dear Wolfmanjm, well what am I to think!"
date: September 20, 2016 16:00
category: "General discussion"
author: "Brian W.H. Phillips"
---
Dear Wolfmanjm, well what am I to think! I buy a new smoothie board and guess what the USB stil doesn't function correctly. No different to the klone board exactly same problem. Tried different computers, tried different drivers, using a short USB lead with four ferrites on it. No difference, engraves a few lines, stops and then falls goes off line. can you help over this now or do I give up?





**"Brian W.H. Phillips"**

---
---
**Arthur Wolf** *September 20, 2016 16:14*

Which would logically lead to conclude this isn't a problem with the board(s).

MKS boards have been reported to have worse USB, however we can't really know if that's the problem you are running into, or if you are running into another problem.



I don't know if you missed it or not, but I already told you this is very likely a problem with your software.

You are using Visicut right ?

Can you please do the following : 

* Export your engraving as a gcode file instead of "executing" it ( file menu )

* Deposit that gcode file on the SD card

* Connect to the Smoothieboard over network ( [smoothieware.org - Network - Smoothie Project](http://smoothieware.org/network) ) and launch the Gcode file using the web interface.

* Tell me if that is working better or not



This is <b>not</b> a solution/alternative, just something you should try because it will tell us more about the problem.



Thank you in advance.



Cheers.


---
**Brian W.H. Phillips** *September 20, 2016 16:46*

My router is too far away to use ethernet and I have tried direct computer to smoothie and I cannot get it to work. I went through a test procedure with Thomas to see if the problem was with visicut, after doing the tests and reporting the results he said that something was telling the us to stop sending.

It may be that laserweb3 can overcome this problem , I don't know if it can but also I cannot understand why a USB connecton is so difficult  to get working, surely everyone is not expected to use ethernet! I don't want to drive you with this problem and I am happy to be a guinipig until the problem is resolved. I have my China K40 Nano board  that works well but is a noisy stepper driver and the graphics will never be as good as a PWM drive.

regrads Brian




---
**Arthur Wolf** *September 20, 2016 16:54*

Again : this is very likely a problem with the software, as I said several times now, Visicut support for Smoothieboard is very new and used by very few people, it's <b>to be expected</b> that it doesn't work properly all the time. This is <b>extremely likely</b> nothing to do with the USB hardware or drivers in any way.



If you can't use Ethernet that is fine, please do the following : 



* Export your engraving as a gcode file instead of "executing" it ( file menu ), to "test.gcode"

* Deposit that gcode file on the SD card

* Connect to the Smoothieboard over USB using Pronterface. 

* Tell the board to print that file, using the following command : « @play /sd/file.gcode »

* Tell me if that is working better or not



Thanks.



[smoothieware.org - Network - Smoothie Project](http://smoothieware.org/network)


---
**Brian W.H. Phillips** *September 20, 2016 17:50*

That works, sending from pronterface as gcode.

but slow!


---
**Arthur Wolf** *September 20, 2016 17:51*

Hey. I didn't ask you to send gcode from pronterface.

You need to do exactly this : 



* Export your engraving as a gcode file instead of "executing" it ( file menu ), to "test.gcode"

* Deposit that gcode file on the SD card

* Connect to the Smoothieboard over USB using Pronterface.

* Tell the board to print that file, using the following command : « @play /sd/file.gcode »

* Tell me if that is working better or not




---
**Brian W.H. Phillips** *September 20, 2016 17:53*

I did what you said above!




---
**Brian W.H. Phillips** *September 20, 2016 17:55*

I guess you misunderstood my answer, the pronterface ran the sd card file as you said above, and it works but slow


---
**Arthur Wolf** *September 20, 2016 17:56*

Well you mentionned " sending from pronterface as gcode " which refers to another method of using Pronterface, but I'll just guess you didn't know that.



So you did it, and it worked, but you say "but slow!". What exactly is slow ?


---
**Brian W.H. Phillips** *September 20, 2016 18:00*

the scan is slow, but the in between moves are fast. I used a fairly dense bitmap.


---
**Arthur Wolf** *September 20, 2016 18:01*

Yes, speed is going to depend on configured parameters, bitmap density, and ultimately is limited by Smoothieboards processing power.

Does it engrave correctly ( assuming you have your power parameters tuned correctly ) ?


---
**Brian W.H. Phillips** *September 20, 2016 18:16*

No at this point I havn't activated the laser, only checking mechanical functions. I will finish setting everything up tomorrow and then let you know how it is. Thank you for your help, regards Brian




---
*Imported from [Google+](https://plus.google.com/104656853918904556475/posts/VLLmi695Uft) &mdash; content and formatting may not be reliable*
