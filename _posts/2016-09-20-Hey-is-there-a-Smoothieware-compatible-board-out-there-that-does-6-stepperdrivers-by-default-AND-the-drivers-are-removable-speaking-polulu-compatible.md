---
layout: post
title: "Hey, is there a Smoothieware-compatible board out there, that does 6 stepperdrivers by default AND the drivers are removable, speaking \"polulu-compatible\"?"
date: September 20, 2016 17:06
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
Hey, is there a Smoothieware-compatible board out there, that does 6 stepperdrivers by default <b>AND</b> the drivers are removable, speaking "polulu-compatible"? Just searched a bit, but found only workarounds...





**"Ren\u00e9 Jurack"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 20, 2016 17:23*

**+René Jurack** my larger board, the ReMix. I believe we may have discussed it before. The factory just sent me pics, the samples are coming along well :)  This pic is not from the factory though:

![missing image](https://lh3.googleusercontent.com/Cp617uSf5-hs0QkQ62RWFQrv4CUHFl5VfqQtqE6FbsiKeACtKkvgb-5W6uu7utHX7dhXeTOcQYyhCT8=s0)


---
**René Jurack** *September 20, 2016 17:33*

Yeah, I remember. But we were mainly talking about you mini-version :D I have the following requirements to the board:

- 24V!

- 3 hotend-heaters (40W PWM mosFET)

- 4 Thermistors

- 4 additional PWM-able output-pins (direct 3.3-5V or mosFET is good, it doesnt matter)

- 4 additional (3,3V-5V) DO-Pinouts to drive some extensions via an own external mosFET-board (lights, fans, etc)



As I look at your provided picture, it looks like it has it all?!


---
**Ray Kholodovsky (Cohesion3D)** *September 20, 2016 17:42*

-24v YES. 

-4 Thermistors YES.

I have a total of 6 MOSFETs.  One is for heated bed, 4 more for heaters/ fans/ other stuff, and 1 remaining tiny mosfet for fan/ small LED.  

This leaves exactly 1 pwm pin which is broken out to servo/ expansion header in the lower right.  Those are all the pins the smoothie IC has (nothing left unused), but keep in mind you may be able to pull a few additional digital outputs from the GLCD connectors if you do not hook up a GLCD.  You can also use the unused mosfet outputs as DO, just the signal will be inverted (quick fix with a 10k resistor inverter, or just use the really good mosfets on my board :) )


---
**René Jurack** *September 20, 2016 17:46*

Yeah, using the DOs from the GLCD-connector will work for me. Now the important part: When will I be able to hold one in my hands? (Germany here) ;)


---
**Artem Grunichev** *September 20, 2016 18:34*

**+Ray Kholodovsky** wow, where could I read about your board?


---
**Ray Kholodovsky (Cohesion3D)** *September 20, 2016 18:37*

**+Артем Груничев** Please see this post: [https://plus.google.com/+RayKholodovsky/posts/5yYm2NKvU2j](https://plus.google.com/+RayKholodovsky/posts/5yYm2NKvU2j)  and you can also scroll through my G+ profile for more info as I was designing the board.


---
**Thomas Sanladerer** *September 20, 2016 22:36*

**+René Jurack**​ i take it wiring on more drivers to spare pins won't do it for you? 


---
**René Jurack** *September 20, 2016 23:50*



**+Thomas Sanladerer** It needs to stay simple in terms of "other people should be able to do it too". At the moment I use a Due/RADDS combo with repetier, but since I got to know smoothieware, everything else is just bothersome... But the thing I don't like in a smoothieboard are these integrated stepperdrivers. It is like with their firmware... If you go smoothie once, you dont want to come back. And since I did TMC2100 with their 256ustepping, I never want to do less microstepping... 


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/EUhrFP6XK7b) &mdash; content and formatting may not be reliable*
