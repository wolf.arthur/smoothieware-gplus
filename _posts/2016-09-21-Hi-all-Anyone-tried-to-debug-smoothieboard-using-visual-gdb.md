---
layout: post
title: "Hi all, Anyone tried to debug smoothieboard using visual gdb"
date: September 21, 2016 10:26
category: "General discussion"
author: "Athul S Nair"
---
Hi all,  Anyone tried to debug smoothieboard using visual gdb. 

i am following this tutorial [http://visualgdb.com/tutorials/arm/nxp_lpc/](http://visualgdb.com/tutorials/arm/nxp_lpc/)



does anyone know about the gpio group and led port of smoothie board ?? i stucked in that section :)



thank u





**"Athul S Nair"**

---
---
**Douglas Pearless** *September 22, 2016 00:44*

I do not use VisualGDB, the details you require should be buried in the Smoothie source code (somewhere!). I am debugging using Eclipse, GDB and a Segger JLink, it is cross platform (OSX/Linux/Mac) I am using both OSX and Linux and seems to do everything VisualGDB does but doesn't cost $89, its free.  So while I am not criticising your choice of tool, just curious as too why you are using it (i.e. some neat feature, etc)  Cheers Douglas


---
**Julien Tanteri** *September 30, 2016 13:57*

Hi **+Douglas Pearless**. Can you please give me some details on how to debug with Segger JLink ? I own a LPC-Link2 (compatible Segger JLink), but I'm unable to find a relevant tutorial (wiring, setup, GDB attach...). I did modify the smoothie firmware, but it crashes on the first command when I reconnect it after few minutes. I suspect an exception, but have not much to catch it.  

Any help will be appreciated. 


---
*Imported from [Google+](https://plus.google.com/114978037514571097180/posts/iZis21xaaU5) &mdash; content and formatting may not be reliable*
