---
layout: post
title: "I can't seem to issue a @play command from pronterface is it possible I'm missing something in my config file that would allow me to play a .gcode file from the sd card"
date: September 21, 2016 02:56
category: "General discussion"
author: "Alex Krause"
---
I can't seem to issue a @play command from pronterface is it possible I'm missing something in my config file that would allow me to play a .gcode file from the sd card





**"Alex Krause"**

---
---
**Wolfmanjm** *September 21, 2016 06:43*

you can use the pronterface SD menu to run files from the sdcard.


---
**Arthur Wolf** *September 21, 2016 08:23*

Does @version work ?

What does @ls /sd/ tell you ?


---
**Wolfmanjm** *September 21, 2016 11:35*

not all versions of pronterface support @


---
**Arthur Wolf** *September 21, 2016 11:54*

**+Wolfmanjm** The latest one does though right ?

**+Alex Krause** Are you using the latest version of Pronterface ?


---
**Alex Krause** *September 21, 2016 14:10*

I will have to look when I get home tonight... I'm using pronterface that was supplied with my smoothie 4xc most the files on the sd card have a modified/creation date in April of this year but without actually being on my computer no way for me to tell right now


---
**Alex Krause** *September 23, 2016 03:09*

**+Arthur Wolf**​ **+Wolfmanjm**​ sorry for the late response I just now had a chance to play with the laser again it looks like it was my fault in the syntax of issuing the command I added a space between the last / and the file name for the path. I'm sorry if I have wasted your time I just miss read the instructions on playing from sd card. Thank you for the help guys I appreciate it


---
**Arthur Wolf** *September 23, 2016 09:27*

**+Alex Krause** Glad you got it to work :)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/GcCMfRcnGTC) &mdash; content and formatting may not be reliable*
