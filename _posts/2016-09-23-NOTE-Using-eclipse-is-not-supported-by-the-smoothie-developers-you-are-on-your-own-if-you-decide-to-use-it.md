---
layout: post
title: "\"NOTE Using eclipse is not supported by the smoothie developers, you are on your own if you decide to use it.\""
date: September 23, 2016 20:29
category: "General discussion"
author: "Andrew Wade"
---
"NOTE Using eclipse is not supported by the smoothie developers, you are on your own if you decide to use it."



The above statement is in your documentation, what software is recommended, i do not mind what i use i just want to designing code for delta sla resin printer





**"Andrew Wade"**

---
---
**Arthur Wolf** *September 23, 2016 20:46*

**+Andrew Wade** You can use any text editor or IDE you want to edit the code.

And then you use the make command to compile, and gdb to debug.

We wrote this because it looks like integrating eclipse and gdb is a lot of work, which we were asked for a lot of help with, and weren't able to help ...


---
**Andrew Wade** *September 23, 2016 20:48*

Why do you not support eclipse?




---
**Arthur Wolf** *September 23, 2016 20:54*

**+Andrew Wade** Because we don't know how to ...

We've had several people "getting it to work with Eclipse" over the years. Every time somebody wants to do it, the previous instructions don't work, and they have to figure it out all over again.

This sounds very complicated. The instructions are in general very long.

And none of us use Eclipse so we can't help with that process.

Therefore : we don't support Eclipse.


---
**Andrew Wade** *September 23, 2016 21:11*

Fair enough, i would rather use the same as all the developers rather than something complex, i am rusty at programming but i want to write my own functions and hopefully people will use them.


---
**Douglas Pearless** *September 23, 2016 21:51*

I use Eclipse for my Smoothie and Smoothie2 development, so if you have questions, just ask and we can see if we can help,  but I am no Eclipse expert.  



I use a variety of setups

JlinkGDBServer on Linux where I access via tcp/ip from several OSX boxes running Eclipse and Jlink ( I have the Jlink Education hardware as  not for profit).i also use Eclipse and JLink directly from OSX as well.  I do not use Windows.



I am also exploring using LPCXpresso and LPC-Link2 to see if I can get the ETM trace profiling working to help debug the timing for Smoothie2.


---
*Imported from [Google+](https://plus.google.com/112929738014057369475/posts/Lqc8zJggcu5) &mdash; content and formatting may not be reliable*
