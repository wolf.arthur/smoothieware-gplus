---
layout: post
title: "I'm just moving to smoothie from the typical ramps setup with Repetier"
date: October 03, 2016 01:49
category: "General discussion"
author: "Dushyant Ahuja"
---
I'm just moving to smoothie from the typical ramps setup with Repetier. Just started to research stuff and one of the things I could not find is the ability to change the z position while printing. Called babystepping in marlin and Repetier. It's huge time saver when I change nozzles - I don't need to measure the new z offset from the bed and can basically adjust the first layer on the fly. 

Can someone point me in the right direction...







**"Dushyant Ahuja"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2016 01:56*

No baby stepping in smoothie as far as I am concerned. Lots of probing options though. 


---
**Dushyant Ahuja** *October 03, 2016 01:58*

Hmmmm. How do others handle this situation. Do you level the bed and adjust the Z offset every time you change a nozzle? Or is it that I'm crazy and change my nozzle size too often?


---
**Jeff DeMaagd** *October 03, 2016 09:16*

It's been a while, but bumping Z while running a program certainly caused me some really odd results. I was going to suggest reprobing but that's not going to work right if the probe height doesn't change with the nozzle.


---
**Arthur Wolf** *October 03, 2016 09:46*

**+Wolfmanjm** We don't have this, right ?


---
**Zane Baird** *October 03, 2016 14:04*

**+Dushyant Ahuja** I'm not sure what hot end you are running, but I change nozzles quite frequently on my smoothied-herculien (I have both an e3d and volcano on the same carriage). The variation in z-offset is so minor that I don't even bother adjusting my z-height at all. However, I can see a potential problem if you were trying to put down a first layer with a height of 0.1mm or less. Running a first layer height of ~0.2mm I haven't had any problems to speak of. 


---
**Dushyant Ahuja** *October 03, 2016 14:12*

**+Zane Baird** the main issue I have is when I change the volcano nozzles to the normal ones. That's a huge difference due to the different sizes of the heated block. I guess I need to do things properly now. Repetier had allowed me to be simply lazy. Unless **+Wolfmanjm** can add something quickly. Pretty please :-)


---
**Zane Baird** *October 03, 2016 14:18*

**+Dushyant Ahuja** Ah, I see your problem. You can always just fix this with a z-offset change in the starting script of your slicer. However, this would mean you would need to set your z-axis to home to the maximum to avoid crashing into the endstop or crashing the nozzle into the bed (depending on whether switching from the volcano to the v6 or vice versa). Should be relatively easy if you properly tune the offset to account for the differences in hotend length


---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2016 14:23*

**+Dushyant Ahuja** Zane is giving you the right idea.  Here's what I suggest. 

Get a proper probe on the head so that you don't have to move your endstop constantly.  Otherwise you'd need to get a optical endstop so that you can go through it (below) without crashing.  I'd do that so you can still use a min endstop.  Then you use the Z offset in slicer.  For example, the volcano is 8.5mm taller than the v6.


---
**Dushyant Ahuja** *October 03, 2016 14:34*

**+Ray Kholodovsky** **+Zane Baird** thanks for the suggestions. Like I was saying, it was just a lazy man's way out. I have a glass bed, and have recently ordered a capacitive probe. Let's see how that works. 

I had tried an inductive one with aluminum tape on the bottom of my glass plate - but that did not give me good results. Let's see if this works. 

I also have optical endstops that are yet to be incorporated into the printer. Just plain lazy. 


---
**Dushyant Ahuja** *October 03, 2016 14:37*

**+Ray Kholodovsky** the offset would also depend on how much the throat goes into the heat sink. So every time I change the heater block - I would need to measure the new Z offset. Not difficult, but having the ability to babystep was just plain easier. My LCD screen is primarily used for that - as I have an octopi setup for the actual control.  Well - you win some and you lose some. 


---
**Zane Baird** *October 03, 2016 14:43*

**+Dushyant Ahuja** The amount of insert into the heatsink should be very reproducible assuming you screw the two together tightly (to spec, hand tight) as this will create a flush fit with the heat brake and the heat sink. Because everything on the e3d hotends fits together flush in z, you end up with a fairly reproducible offset


---
**Dushyant Ahuja** *October 03, 2016 14:46*

Well - on a Chimera the heatbreak is used to adjust the height of the two nozzles so that they are level. That causes some uncertainty in the actual offset. Again - not difficult, just inconvenient to what I am currently used to. 


---
**Zane Baird** *October 03, 2016 14:57*

**+Dushyant Ahuja** Didn't realize you were using the Chimera... That does complicate it a bit, even with a probe.


---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2016 15:04*

Back in the day I built a 2'x4' printer with dual chimera and a basic z probe (switch on a servo) 


---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2016 15:05*

Oh, so I just stuck all the chimeras in the uppermost position (thus putting the nozzles into a straight line) and using the left/right leadscrews to align them parallel to the bed. Probe took care of the rest. 


---
**Wolfmanjm** *October 03, 2016 17:14*

We do Z offset correctly like all CNC machines do! No need to give it a special name like baby stepping or whatever. You can change Z offset on the fly with workspace coordinate system... G10 L2 P0 Zxxx where xxx is the offset you want. works immediately


---
**Dushyant Ahuja** *October 03, 2016 17:22*

**+Wolfmanjm** thanks - will look into the workspace coordinate system. Is there any way to adjust this using the knob on the LCD. 


---
**Wolfmanjm** *October 03, 2016 17:58*

No not really.


---
**John Herrington** *October 03, 2016 22:32*

could you use a the Switch module and have it execute something like:



G91 G10 L2 P1 Z-.01;

G90;



and then 



G91 G10 L2 P1 Z.01;

G90;



tie those two commands to the dial





I don't think it would work well unless the print was segmented.  And I have no idea how the look ahead buffer would effect something like this.



Just spit balling a bit.


---
**Dushyant Ahuja** *October 04, 2016 02:49*

**+John Herrington** would something like the following work:



custom_menu.babystepdown.enable               true              #

[custom_menu.babystepdown.name](http://custom_menu.babystepdown.name)                 Babystep_Down        #

custom_menu.babystepdown.command              G91_G10_L2_P1_Z-.01_G90               #



custom_menu.babystepup.enable               true              #

[custom_menu.babystepup.name](http://custom_menu.babystepup.name)                 Babystep_Up        #

custom_menu.babystepup.command              G91_G10_L2_P1_Z.01_G90               #


---
**Wolfmanjm** *October 04, 2016 02:54*

No it wouldn't  read the docs on G10 L2 here... [http://linuxcnc.org/docs/2.6/html/gcode/gcode.html#sec:G10-L2_](http://linuxcnc.org/docs/2.6/html/gcode/gcode.html#sec:G10-L2_)

[linuxcnc.org - G Codes](http://linuxcnc.org/docs/2.6/html/gcode/gcode.html#sec:G10-L2_)


---
**Dushyant Ahuja** *October 04, 2016 03:07*

**+Wolfmanjm** I see - so Z needs to increment / decrement after every run. Any suggestions on how this can be implemented easily? Maybe I can do something in Octoprint. Does this command skip the buffer - so that the change is instantaneous, or it takes into effect only when it reaches the head of the buffer.



Apologies for so many questions, but this is my first time moving to Smoothie, and am still figuring out stuff


---
**Wolfmanjm** *October 04, 2016 03:20*

you should be able to do it in octoprint, increment and decrement the value. it acts on any gcode that is received after the change in setting


---
**Dushyant Ahuja** *October 04, 2016 03:22*

**+Wolfmanjm** so doesn't skip the buffer - I can probably work with that. And, as a last resort, do things properly and not act like a lazy ass :-). Thanks for your help.


---
**John Herrington** *October 04, 2016 09:49*

G91 does effect G10 in FANUC word, I even tested it on a 15T yesterday.



Like I said I was just spit balling a bit.


---
**Dushyant Ahuja** *May 27, 2018 07:41*

Was revisiting this; but turned out that after changing the coordinate system offsets using G10 L2,  you need to even issue a G1 Z command. In effect to get psudo-babystepping you would have to give a G1 Z (first layer height) in addition to the G10 L2 command with an incrementing Z value..




---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/JmpoewcdkAs) &mdash; content and formatting may not be reliable*
