---
layout: post
title: "Originally shared by Sbastien Mischler (skarab) Smoothie-Happy - v0.2.0 - Network scanner - Multiple boards manager - Get board version/info - Liste all files on the board (recursive) - Detection of loss of connection and"
date: October 03, 2016 16:56
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Sébastien Mischler (skarab)</b>



<b>Smoothie-Happy - v0.2.0</b>

- Network scanner

- Multiple boards manager

- Get board version/info

- Liste all files on the board (recursive)

- Detection of loss of connection and automatic reconnection.

- API documentation

- To be continued...



<b>DEMO :</b> [http://lautr3k.github.io/Smoothie-Happy/](http://lautr3k.github.io/Smoothie-Happy/)

<b>REPO :</b> [https://github.com/lautr3k/Smoothie-Happy](https://github.com/lautr3k/Smoothie-Happy)

<b>DOCS :</b> [http://lautr3k.github.io/Smoothie-Happy/docs/smoothie-happy/0.2.0-dev/](http://lautr3k.github.io/Smoothie-Happy/docs/smoothie-happy/0.2.0-dev/)



 #smoothieboard           #smoothieware          #SmoothieHappy



![missing image](https://lh3.googleusercontent.com/-AtKuxFrZvxE/V_KKDMZopBI/AAAAAAAAPSc/BuPvijTuBsEStu_4aKXV8wkWP9V0nMBnwCJoC/s0/sh1.png)
![missing image](https://lh3.googleusercontent.com/-03E7vJ7i3Sk/V_KKDOUVb8I/AAAAAAAAPSc/oiw4K816B6cB98k0fj75F4iKpBfd-XUXgCJoC/s0/sh2.png)

**"Arthur Wolf"**

---
---
**David Bassetti** *October 04, 2016 06:00*

Fantastic Smoothie..!!!




---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/DDYVtmbT6kD) &mdash; content and formatting may not be reliable*
