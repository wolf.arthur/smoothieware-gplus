---
layout: post
title: "Can a config entry such as the kill button be doubled up in some fashion?"
date: October 04, 2016 03:07
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Can a config entry such as the kill button be doubled up in some fashion? 

I would like both pins 2.12 (on board kill button) and 2.11 (kill button on glcd) to be able to be used for this purpose. 





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Arthur Wolf** *October 04, 2016 07:26*

You could setup a switch module for the second pin.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/2Z5Jp1qqYUQ) &mdash; content and formatting may not be reliable*
