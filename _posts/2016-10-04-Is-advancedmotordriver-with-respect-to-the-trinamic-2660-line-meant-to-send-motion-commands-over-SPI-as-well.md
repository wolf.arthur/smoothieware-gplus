---
layout: post
title: "Is advancedmotordriver (with respect to the trinamic 2660 line) meant to send motion commands over SPI as well?"
date: October 04, 2016 23:56
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Is advancedmotordriver (with respect to the trinamic 2660 line) meant to send motion commands over SPI as well? 

Is there a way to set it up so that we initialize the IC over SPI but still use the STEP/ DIR pins for motion? 





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 04, 2016 23:57*

**+James Rivera** this is what you just asked about so you may want to subscribe to this thread. 


---
**Douglas Pearless** *October 05, 2016 00:59*

I am adding that exact functionality to Smoothie2 right now 😄


---
**Douglas Pearless** *October 05, 2016 01:05*

for the TMC2130 SilentStepStick




---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2016 01:06*

**+Douglas Pearless** awesomeness! But can we run that on current smoothie1 too? 


---
**Wolfmanjm** *October 05, 2016 04:19*

**+Ray Kholodovsky** it ONLY does step and dir over the strep and dir lines, it cannot do motion over SPI as SPI CANNOT be used during an interrupt. **+Douglas Pearless** not sure what you mean the motorcontroll only sets up the over SPI and motion control can only be done over step/dir lines. it already does that.


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2016 04:25*

**+Wolfmanjm** can you elaborate on the config for this please, as none of the examples on that documentation page show how to define a step, dir, or enable pin.


---
**Wolfmanjm** *October 05, 2016 04:36*

step dir and enable are configured exactly the same as any other driver. no difference. the advance motor driver only handles the chip setup via SPI. [http://smoothieware.org/advancedmotordriver](http://smoothieware.org/advancedmotordriver)

The step/dir/enable are the same as for any other onboard chip


---
**Wolfmanjm** *October 05, 2016 04:38*

**+Ray Kholodovsky** [https://github.com/Smoothieware/Smoothieware/blob/edge/ConfigSamples/Smoothieboard/config#L39](https://github.com/Smoothieware/Smoothieware/blob/edge/ConfigSamples/Smoothieboard/config#L39)


---
**Wolfmanjm** *October 05, 2016 04:57*

I updated the wiki docs to add a comment about step/dir FYI enable is NOT done via a pin but via the SPI commands.


---
**Douglas Pearless** *October 05, 2016 05:04*

Fantastic,



I hadn’t been into the Smoothie1 code base for a while and had not noticed this new feature!!!!



I will have a look at it and it may be easier to port this code to Smoothie2 that figure out why my code breaks the step generation code.



Cheers

Douglas


---
**Wolfmanjm** *October 05, 2016 05:18*

**+Douglas Pearless** it isn't a new feature, it has been in smoothie for about a year.


---
**Douglas Pearless** *October 05, 2016 05:24*

Well that is embarrassing, I never noticed it :-)


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2016 18:32*

**+Wolfmanjm** Awesome, thanks. I should be able to use the existing EN pin from smoothie as the SPI select pin.  Should make things easy. OShpark just shipped my TMC2660 board for testing all this.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/UeZFNTH6C2x) &mdash; content and formatting may not be reliable*
