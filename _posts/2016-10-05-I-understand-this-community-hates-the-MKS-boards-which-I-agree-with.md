---
layout: post
title: "I understand this community hates the MKS boards (which I agree with)"
date: October 05, 2016 18:55
category: "General discussion"
author: "Dushyant Ahuja"
---
I understand this community hates the MKS boards (which I agree with). But what's the consensus on others Aztech boards, Ray's boards, Peter's smoohiebrainz, etc. 



Is it OK to ask questions about those here? 







**"Dushyant Ahuja"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2016 18:59*

I'll leave the main question itself up to the Smoothie guys but they seem to be quite open. 



I will add that when I see questions from my customers here I always jump in to answer them as it is not the Smoothie's team burden to support my customers (but it is appreciated when they help out :) )



But yeah, **+Arthur Wolf** can give an official answer. 


---
**Arthur Wolf** *October 05, 2016 19:19*

It's ok to ask questions about all boards, it's just in the case of MKS, we will make sure you know they are evil.

All the other boards you mentionned are fine. The general rule is : if it's Open-Source, we love it.


---
**Dushyant Ahuja** *October 05, 2016 19:22*

Thanks. Just wanted to confirm before I start with my questions. 


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2016 19:26*

See, this attitude is why all the people in this thread are awesome. (Mentions of MKS do not qualify it for the aforementioned awesomeness).  Like seriously, who still uses a 12-5v linear regulator? It's like they want their customers to experience a fire.


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/JNMRkURtR5f) &mdash; content and formatting may not be reliable*
