---
layout: post
title: "Last question for now I promise :) Do/ can we have a real Feedhold?"
date: October 05, 2016 19:29
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Last question for now I promise :) 

Do/ can we have a real Feedhold? For example:  I send a kill command from control program, it is smart enough to not buffer the command on its end and send it directly. But can Smoothie get such a command, know to react on it immediately, and perform a halt <b>while maintaining position</b>? 

I think this is the last big gripe people have.  The Kill button results in a position loss, and I've talked to a few people that wired a button configured with the pause switch, but that seems very sketchy to me.  Any thoughts appreciated.  





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Arthur Wolf** *October 05, 2016 19:39*

We don't have anything perfect there atm, and there is some thinking going on on implementing something better, but it's going to be a while I think.


---
**Wolfmanjm** *October 05, 2016 23:27*

Kill will attempt to maintain position, it is just not guaranteed. We have grbl feedhold command, however it is not instant. It would be very very hard to make it instant.  not sure it will happen anytime soon. although I would like a real feedhold like grbl has.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/9beb9b5AFYx) &mdash; content and formatting may not be reliable*
