---
layout: post
title: "On Hackaday again. Woot woot !"
date: October 05, 2016 20:01
category: "General discussion"
author: "Arthur Wolf"
---
On Hackaday again. Woot woot ! [http://hackaday.com/2016/10/05/tiny-smoothies-at-maker-faire/](http://hackaday.com/2016/10/05/tiny-smoothies-at-maker-faire/)





**"Arthur Wolf"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2016 20:19*

Told you.


---
**Arthur Wolf** *October 05, 2016 20:19*

**+Ray Kholodovsky** :)


---
**Gary Hangsleben** *October 06, 2016 01:13*

Very cool, nice work.


---
**David Bassetti** *October 06, 2016 08:44*

It says  ..  "The SmoothieBoard itself is coming up on version two, or so we’re told,  and of course that board will have enough new features to make things very interesting"

Any news ?  :D




---
**Arthur Wolf** *October 06, 2016 09:02*

**+David Bassetti** Working very hard. Making progress. Colossal project. Progress depends on contributors so can't have a timetable.


---
**James Newton** *October 19, 2016 01:11*

Is that little board available somewhere? I'm interested because since it doesn't have the stepper drivers built in, it can be used with larger drivers even servo drivers... :cough: like mine.




---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2016 01:17*

**+James Newton** that little board, the "stamp", was really just for fun and as a basis for certain other projects in the pipeline. If you scroll thru my profile, you'll find 2 other boards that I've been working on and either of those can easily be used with external drivers using an adapter board which I also have designed. A few other people have expressed interest in that board as well but it's not something I can have manufactured unless an OEM wants 250 or 1,000 of them at once :)![missing image](https://lh3.googleusercontent.com/-4HjoJTZDwms-8N5b48am5mImTChvVj8fcx6F_zXG3Dl63Br_4f1bRk2Ed3arVn0-5ydp1SNe1aP7Hc=s0)


---
**James Newton** *October 19, 2016 01:28*

**+Ray Kholodovsky** So you call that little guy the "Smoothie STAMP" and the other one that accepts other drivers is... the DIY Smoothieboard?


---
**Arthur Wolf** *October 19, 2016 09:35*

**+Ray Kholodovsky** Once v2 smoothie is a bit more mainstream, if you design a lpc4330 version of this ( no need right now ), we will likely be making 100 of them.


---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2016 15:40*

**+Arthur Wolf** I can design that as soon as reference hardware designs are made available. 


---
**Arthur Wolf** *October 19, 2016 15:45*

**+Ray Kholodovsky** Right now, this is very low priority. What we really need a ton of help with ( like : pretty please ! ), is the extension boards.

Those are easy to design, they will be insanely useful to the community, and we need a lot of help on them.

Want to do some ?

[https://github.com/Smoothieware/Smoothieboard2](https://github.com/Smoothieware/Smoothieboard2)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/HUUmHwhrCuQ) &mdash; content and formatting may not be reliable*
