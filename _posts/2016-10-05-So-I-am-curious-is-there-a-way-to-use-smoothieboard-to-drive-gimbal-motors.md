---
layout: post
title: "So I am curious, is there a way to use smoothieboard to drive gimbal motors?"
date: October 05, 2016 16:08
category: "General discussion"
author: "Alex Hayden"
---
So I am curious, is there a way to use smoothieboard to drive gimbal motors? They use pwm signals so as long as you use an external driver designed to drive the gimbal, can you use the step pin to deliver this signal? What do you do about the enable pin? If anyone has done this I could use some instructions please.





**"Alex Hayden"**

---
---
**Arthur Wolf** *October 05, 2016 19:24*

That's just not supported, and there is no easy way to add it. I expect the fastest way to something that works would be using an arduino or similar to convert the step/dir signals into whatever you need.


---
**Josh Rhodes** *October 06, 2016 02:12*

**+Alex Hayden**​ if you look around there are already very cheap (~$30) boards made for exactly that function. 


---
**Alex Hayden** *October 06, 2016 06:10*

**+Josh Rhodes** are you referring to the boards used in FPV helicopters? I am really interested in how precise the steps per signal translates to the correct distance of rotation. 


---
*Imported from [Google+](https://plus.google.com/+AlexHayden/posts/JQk7suZaHUV) &mdash; content and formatting may not be reliable*
