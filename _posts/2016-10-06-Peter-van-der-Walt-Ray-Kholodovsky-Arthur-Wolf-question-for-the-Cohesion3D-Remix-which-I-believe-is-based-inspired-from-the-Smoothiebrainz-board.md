---
layout: post
title: "Peter van der Walt Ray Kholodovsky Arthur Wolf - question for the Cohesion3D Remix (which I believe is based / inspired from the Smoothiebrainz board"
date: October 06, 2016 01:24
category: "General discussion"
author: "Dushyant Ahuja"
---
**+Peter van der Walt** **+Ray Kholodovsky** **+Arthur Wolf** - question for the Cohesion3D Remix (which I believe is based / inspired from the Smoothiebrainz board.



What's the best way to use the ESP8266 that's on board. I've been able to send commands using the ESP-Link firmware on it, and have been able to reset the LPC, etc. What I would really like to do is:



1. Access the Smoothie's web interface through it. As I understand it - right now we can only access the web interface through the Ethernet interface.

2. Alternatively setup Octoprint to print over wifi.



Can you please help me understand the process.



Also, the smoothie documentation mentions that sending Ctrl-X over the serial port would kill the print. [http://smoothieware.org/killbutton](http://smoothieware.org/killbutton)

How do I send Ctrl-X over the serial port???





**"Dushyant Ahuja"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 06, 2016 01:27*

Right now you'd essentially be using Pronterface on your computer to bind to the IP address of the ESP (and port 23) and stream a job to it that way.

I'm not entirely sure if/ that Octoprint supports sending to an IP Address to begin with. You can connect from ReMix to Octoprint over USB for sure, that's how it's usually done.


---
**Dushyant Ahuja** *October 06, 2016 01:28*

**+Ray Kholodovsky** That is going to be the short-term solution; but I would like to free up my Raspi Zero; and preferably use the SD card for printing.


---
**Ray Kholodovsky (Cohesion3D)** *October 06, 2016 02:43*

**+Dushyant Ahuja**

I've been working on this for the last few hours.  We do have the ability to save the file to SD Card using M28 filename.gcode command, stream gcode, M29 to end. However, I have not yet found a software that does this.  I can't even find the button to open Repetier Host's SD Card manager window.  I'm going to try Octoprint next.


---
**Liam Jackson** *October 06, 2016 07:26*

I've been trying to work on it just too little free time! 

Trying to best solve the problem of how to identify the end of the response of a command, they don't all send 'ok'. could do with a prompt like a console. 



Following each command with M400 delays too long while printing and doesn't make things much better because I don't know if im waiting for one 'ok' or two. Something with a different predictable output like pwd is fine but a bit of a hack. And that approach doesn't work while streaming gcode to a file or the file ends up full of M400s. 



Basically means I end up with the unreliable system of checking the UART buffer for characters and sleeps, I'm testing it but all of these corner cases make it a lengthy process. 


---
**Arthur Wolf** *October 06, 2016 08:23*

[http://smoothieware.org/octoprint](http://smoothieware.org/octoprint)



but streaming over telnet is not reliable

[smoothieware.org - Octoprint - Smoothie Project](http://smoothieware.org/octoprint)


---
**Liam Jackson** *October 06, 2016 13:44*

**+Peter van der Walt** thanks, seen that (but not the last months updates). Seems like there are more engineering issues with streaming non text files over serial. 



I put [index.html](http://index.html) in the esp flash to get around the speed issue. 



Looks like he's about to hit the issues I have already found with /command. 


---
**Dushyant Ahuja** *October 07, 2016 13:41*

This is one of the reasons I chose to go with Smoothie. You ask a question - and find 10 people willing to help you. Thanks 


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/T9LreMBUiGV) &mdash; content and formatting may not be reliable*
