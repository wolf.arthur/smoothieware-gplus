---
layout: post
title: "This is nice, I wish it were Open-Source : It makes me think of fabrica a lot"
date: October 06, 2016 22:23
category: "General discussion"
author: "Arthur Wolf"
---
This is nice, I wish it were Open-Source : [http://www.yuriystoys.com/p/android-dro.html](http://www.yuriystoys.com/p/android-dro.html)

It makes me think of fabrica a lot.



Edit : It <b>is</b> open-source : [https://bitbucket.org/ycroosh/android-dro/src](https://bitbucket.org/ycroosh/android-dro/src) , it just didn't mention it on the "product" page. Thanks **+Maxime Favre** for the find.





**"Arthur Wolf"**

---
---
**Josh Rhodes** *October 07, 2016 01:54*

How Hard could it be? 


---
**Brian W.H. Phillips** *October 07, 2016 05:52*

The new version of smoothie must have Bluetooth? 


---
**Maxime Favre** *October 07, 2016 05:54*

I think it cost nothing to ask him if he can open it or do something for smoothie. This DRO interface is more designed for conventional machining not CNC. So different "customers". I really like how he did the workspace and points generation, simple and clean.


---
**Maxime Favre** *October 07, 2016 06:38*

[https://bitbucket.org/ycroosh/android-dro/src](https://bitbucket.org/ycroosh/android-dro/src)



[bitbucket.org - ycroosh / Android DRO 
  / source  /  
 — Bitbucket](https://bitbucket.org/ycroosh/android-dro/src)


---
**Arthur Wolf** *October 07, 2016 07:30*

**+Maxime Favre** Nice !!!!


---
**Jérémie Tarot** *October 27, 2016 06:01*

Glad you like it too ! 

Posted it several times in G+ threads and OpenBuilds forum but didn't get much reactions... 

Anyway, this is a great inspiration and has more features than just DRO among which, the cut list support.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/dXVEsfRMzNt) &mdash; content and formatting may not be reliable*
