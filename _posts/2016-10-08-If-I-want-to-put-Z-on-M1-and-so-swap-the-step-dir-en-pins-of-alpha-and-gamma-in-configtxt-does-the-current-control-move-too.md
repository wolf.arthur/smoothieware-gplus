---
layout: post
title: "If I want to put Z on M1 and so swap the step/dir/en pins of alpha and gamma in config.txt does the current control move too?"
date: October 08, 2016 18:59
category: "General discussion"
author: "Liam Jackson"
---
If I want to put Z on M1 and so swap the step/dir/en pins of alpha and gamma in config.txt does the current control move too? 

Or will alpha_current still control the M1 current?





**"Liam Jackson"**

---
---
**Arthur Wolf** *October 08, 2016 19:48*

Alpha_current still controls the M1 current.


---
**Anthony Bolgar** *October 08, 2016 20:12*

That is one of the great things about Smoothieware/board....so easy to configure changes like that.


---
**Liam Jackson** *October 09, 2016 07:28*

Thanks **+Arthur Wolf**​ :-)



Yes **+Anthony Bolgar**​ and a great community to ask for answers! 


---
*Imported from [Google+](https://plus.google.com/+LiamJackson/posts/DujZnYg5GYW) &mdash; content and formatting may not be reliable*
