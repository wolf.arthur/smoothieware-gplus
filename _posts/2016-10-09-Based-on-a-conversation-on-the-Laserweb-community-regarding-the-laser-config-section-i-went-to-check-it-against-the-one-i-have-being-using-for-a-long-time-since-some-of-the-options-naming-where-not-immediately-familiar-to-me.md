---
layout: post
title: "Based on a conversation on the Laserweb community regarding the laser config section, i went to check it against the one i have being using for a long time since some of the options naming where not immediately familiar to me"
date: October 09, 2016 15:53
category: "General discussion"
author: "Ariel Yahni (UniKpty)"
---
Based on a conversation on the Laserweb community  regarding the laser config section, i went to check it against the one i have being using for a long time since some of the options naming where not immediately familiar to me. 



Why does the sample smoothie config file [https://raw.githubusercontent.com/Smoothieware/Smoothieware/edge/ConfigSamples/Smoothieboard/config](https://raw.githubusercontent.com/Smoothieware/Smoothieware/edge/ConfigSamples/Smoothieboard/config) has a different naming than  [http://smoothieware.org/laser](http://smoothieware.org/laser) / Does it impact the outcome in anyway?





**"Ariel Yahni (UniKpty)"**

---
---
**Arthur Wolf** *October 09, 2016 16:06*

Both syntaxes are valid, and it doesn't impact anything.


---
**Ariel Yahni (UniKpty)** *October 09, 2016 16:11*

Shouldn't for the sake of other users both reflect the same systaxis? Or it was done this way for some specific reason? 


---
**Arthur Wolf** *October 09, 2016 16:14*

It should, I just don't have time to update everything right now, and keep forgetting about it.


---
**Ariel Yahni (UniKpty)** *October 09, 2016 16:17*

Ill do it if you want.


---
**Ariel Yahni (UniKpty)** *October 09, 2016 16:19*

or **+Anthony Bolgar**


---
**Arthur Wolf** *October 09, 2016 16:22*

You can if you want yes, it's very welcome.


---
**Ariel Yahni (UniKpty)** *October 13, 2016 16:19*

**+Arthur Wolf**​ which one should it be


---
**Arthur Wolf** *October 13, 2016 16:33*

The most recent ( longer ) one


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/Efh1VzwNUaW) &mdash; content and formatting may not be reliable*
