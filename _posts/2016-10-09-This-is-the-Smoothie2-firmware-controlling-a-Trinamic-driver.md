---
layout: post
title: "This is the Smoothie2 firmware controlling a Trinamic driver"
date: October 09, 2016 08:55
category: "General discussion"
author: "Arthur Wolf"
---
This is the Smoothie2 firmware controlling a Trinamic driver. Thanks **+Douglas Pearless**  :)





**"Arthur Wolf"**

---
---
**René Jurack** *October 09, 2016 15:11*

It was about time to get the smoothie silent! 


---
**Arthur Wolf** *October 09, 2016 15:25*

**+René Jurack** Several of the v2 boards will use the trinamic drivers. Even the next batch of v1 is likely to go 1/32 with the A5984


---
**René Jurack** *October 09, 2016 15:27*

hmm... 1/32... why not (interpolating) to 1/256 or more? This is so much more silent...


---
**Arthur Wolf** *October 09, 2016 15:28*

**+René Jurack** Because the A5984 is a drop-in replacement for the A4982 we currently use.

And because higher microstepping also means lower maximum speeds, which not everyone will like.


---
**Ray Kholodovsky (Cohesion3D)** *October 09, 2016 15:33*

Is this the 2100 or the 2130? I'd like to know if we can set the various silent modes over SPI (this configuration is 2130 specific) 

And generally, if the stallguard can be used as an endstop (totally possibly in hardware just needs to be done in smoothie) that would be amazing!


---
**René Jurack** *October 09, 2016 15:37*

**+Arthur Wolf** Aww... C'mon... 256µStepping on 32bit Smoothie-compatible board does easily reach 833mm/s: 
{% include youtubePlayer.html id="1yvwE0MtqXo" %}
[https://www.youtube.com/watch?v=1yvwE0MtqXo](https://www.youtube.com/watch?v=1yvwE0MtqXo)


{% include youtubePlayer.html id="1yvwE0MtqXo" %}
[youtube.com - DICE - Micro-coreXY speedtest up 833mm/s - Topview](https://www.youtube.com/watch?v=1yvwE0MtqXo)


---
**Arthur Wolf** *October 09, 2016 15:37*

**+René Jurack** That's going to depend on the mechanical side of things :) 1/256 microstepping with leadscrews will back you into a corner pretty fast :)


---
**René Jurack** *October 09, 2016 15:40*

Sure, but the trinamic steppers are able to be configured like one needs them. They are doing 16µStepping too... But then they are not silent anylonger :D


---
**Anton Fosselius** *October 09, 2016 16:12*

Almost bought a smoothie today. It was to convert a K40 laser cutter from moshidraw to anything sane. Then I realised that I had ~15 arduinos ~30 cortex M4 boards, 3 BBB, 2 rpi2, and about 15 stepper drivers collecting dust.. and I only do vector cutting any way. So smoothie will have to wait a bit more... it's simpler faster to throw something together..


---
**Arthur Wolf** *October 09, 2016 16:13*

**+Anton Fosselius** [http://smoothieware.org/smoothie-on-a-breadboard](http://smoothieware.org/smoothie-on-a-breadboard)

However, note that "breadboard" / "wire nest" designs tend to be very sensitive to EMI, in particular for 32bit/3.3V systems ... You are at strong risk of frequent disconnects, compared to an integrated all-in-one board.


---
**Anton Fosselius** *October 09, 2016 16:27*

Indeed. Let's solve that by adding a couple of laps of ground around the rats nest to create a Faraday cage and then add a ferrite bead for good looks ;) going for quick and dirty. If it works bad, it's your win because then I will buy a smoothie. Otherwise I have a cnc build coming up. So then I will probably go with a smoothieboard.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/hpKGNzgUK1e) &mdash; content and formatting may not be reliable*
