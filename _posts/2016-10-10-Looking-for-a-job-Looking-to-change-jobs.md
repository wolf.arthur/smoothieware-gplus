---
layout: post
title: "Looking for a job ? Looking to change jobs ?"
date: October 10, 2016 21:55
category: "General discussion"
author: "Arthur Wolf"
---
<b>Looking for a job</b> ? Looking to change jobs ? Looking to make some money on the side ? Want to do <b>something awesome for a living</b> ?



The Smoothie project is looking for people with : 

* Awesome <b>Javascript</b> and HTML5 skills

* A boundless love for <b>Open-Source</b>

* Prior knowledge of Smoothie, as a user, or even better as a contributor

* Ideally some User Experience ( UX ) experience

* Ideally good graphic design tastes



If you think you match this description even remotely, please contact us at wolf.arthur@gmail.com

We do not know yet how much we can put into this, but we are looking for options. The more options we have, the more we can make happen.



<b>Please re-share</b>, in particular to your Javascripter friends. They'll thank you !







**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/R6myPmCEJ8o) &mdash; content and formatting may not be reliable*
