---
layout: post
title: "Another noob question: Does Smoothieware have an equivalent to Mesh Bed Leveling (on Marlin) - is Delta Grid Compensation same as Mesh Bed Leveling in Marlin or auto bed leveling?"
date: October 12, 2016 02:58
category: "General discussion"
author: "Dushyant Ahuja"
---
Another noob question:

Does Smoothieware have an equivalent to Mesh Bed Leveling (on Marlin) - is Delta Grid Compensation same as Mesh Bed Leveling in Marlin or auto bed leveling?





**"Dushyant Ahuja"**

---
---
**Arthur Wolf** *October 12, 2016 07:57*

We have both 3-point bed levelling and mesh ( grid, also called delta grid in smoothie ) levelling yes. See [http://smoothieware.org/zprobe](http://smoothieware.org/zprobe)

[smoothieware.org - Zprobe - Smoothie Project](http://smoothieware.org/zprobe)


---
**Dushyant Ahuja** *October 12, 2016 08:58*

**+Arthur Wolf** thanks. Mesh bed leveling compensates for irregularities in the bed by simply raising or lowering Z at the current XY point, using a simple averaging of the slopes in X and Y, multiplied by the relative position within the underlying grid square. 

In contrast to MBL, the grid and 3-point leveling systems tilt the entire coordinate system. 



Does delta grid compensation work like mesh leveling, or grid leveling. 

[reprap.org - Mesh Bed Leveling - RepRapWiki](https://goo.gl/5ZAu55)


---
**Dushyant Ahuja** *October 12, 2016 09:17*

OK - on further reading it seems that it's more like mesh bed leveling. But seems kind of inconvenient for Cartesian systems. I guess will come to know more once I try. 


---
**Arthur Wolf** *October 12, 2016 19:40*

3-point tilts the platform, grid adjusts Z based on an average of slopes. Both are useful, even on cartesians.


---
**Wolfmanjm** *October 13, 2016 06:13*

one is used on flat beds the other on non-flat beds.


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/TJEw8BUqMni) &mdash; content and formatting may not be reliable*
