---
layout: post
title: "I got a problem with my delta, where it does not seem to home correctly"
date: October 12, 2016 19:51
category: "General discussion"
author: "Michael Andresen"
---
I got a problem with my delta, where it does not seem to home correctly.



The problem is if it got the hotend in a radius about 100mm from center, and I press home. It then starts to move all towers up, bumps one switch, lets the two remaining towers move a bit further, stops them, retract from the endstop that was triggered synchronized with all axis, bumps the endstop again, and stops the homing. It it just done homing, with only one tower homed, and the remaining two 80-100mm from the endstop, but believes all o them has been homed, because afterwards it just goes full speed into the build plate.



What can cause this? I have checked the endstops for noise with my oscilloscope, and found nothing that should trigger the remaining two endstops to believe they have been pressed. And just to try, I tried to also add the debounce to them.





**"Michael Andresen"**

---
---
**Arthur Wolf** *October 12, 2016 19:58*

Check that the right endstops are at the top of the right towers, and also check that they work correctly with M119.


---
**Michael Andresen** *October 12, 2016 20:04*

Looks like it was the problem with the firmware version, right after posting I rebemmered I was on an edge build (Build version: edge-b77d053, Build date: Aug  1 2016 04:05:46, MCU: LPC1768, System Clock: 100MHz) so I just switched to Build version: master-27197b7, Build date: Jul  4 2016 04:10:42, MCU: LPC1768, System Clock: 100MHz and finished the G32 successfully with that. So it works fine with the master version.


---
**Arthur Wolf** *October 12, 2016 20:09*

You don't want to be using master, you should always use edge. Please try again with a fresh firmware off github <b>and</b> a fresh config file ( which you edit to add your settings again )


---
**Jesper Krog Poulsen** *October 12, 2016 21:23*

I have seen this problem too, with a firmware update some time ago, but I didn't have time to do anything about it at the time and forgot it again. I have updated the firmware again since that and the problem seems to have vanished. Please tell us, if a fresh firmware solves the problem for you, so we can know if it is still an issue.


---
**Michael Andresen** *October 13, 2016 04:24*

Put edge from the 10th on, and now it works. :) 


---
**Wolfmanjm** *October 13, 2016 06:03*

FWIW he probably needed to add the max_travel settings to the config, they are new and mentioned in both the wiki and the upgrade notes.


---
**Michael Andresen** *October 13, 2016 07:38*

max_travel were in, instead of following **+Arthur Wolf**'s advise of doing a fresh config, I started out by just trying the firmware change to see what happened, and without changing the config, it went from not working, to working without a problem.


---
*Imported from [Google+](https://plus.google.com/+MichaelAndresen/posts/763XGUfHj96) &mdash; content and formatting may not be reliable*
