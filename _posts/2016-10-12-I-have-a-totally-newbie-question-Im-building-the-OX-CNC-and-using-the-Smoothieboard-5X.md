---
layout: post
title: "I have a totally newbie question. I'm building the OX CNC and using the Smoothieboard 5X"
date: October 12, 2016 22:45
category: "General discussion"
author: "Michael Forte"
---
I have a totally newbie question.



I'm building the OX CNC and using the Smoothieboard 5X. I've read where people build the whole thing in 2 days. Well, I've been working on it for a few months and most of that time has been spent trying to figure out how to get the Smoothie to send a command. I don't understand circuit boards very well and I'm learning on the Smoothieboard but need some help.



My setup is as follows.



24V PSU is powering the board and a 48V PSU is powering the spindle.



I have the spindle working using the M3 and M5 commands just like it says on the site.



The stepper motors are another issue.

Since I need more than 2A for each NEMA 23 stepper motor, I am using motor controller boards and have them set up according to the external stepper section on the site. There is an X, 2 Ys, and a Z motor but only the Z is connected at the moment for testing purposes.

Z is connected as described in the "external driver requires 5V" section as Open-Drain (see [http://arthurwolf.github.io/smoothieboard-graphics/schematics/external-driver-open-drain.svg](http://arthurwolf.github.io/smoothieboard-graphics/schematics/external-driver-open-drain.svg)). I am showing 4.1V on all of the ENA, PUL, and DIR pins as measured with a multimeter but I'm not sure if that is correct or not since I thought I would see 5V. I'm guessing it's a voltage drop across a diode.



I have added the 'o' to the three gamma pins in the config file.



When I access through Pronterface or the browser interface I get the same results. I click on all step sizes of Z both positive and negative and don't get so much as a click from the stepper motor.



Since I'm learning as I go, it might be something obvious in my config file. Is there something else I would need to set besides the ena, dir, pul, and current in the file to make this work?





**"Michael Forte"**

---
---
**Ross Hendrickson** *October 12, 2016 22:49*

Have you been able to get anything to move? Could you post your configuration file? When did you last update the board firmware?  Have you been able to get it to move with the onboard stepper drivers?


---
**Michael Forte** *October 13, 2016 01:27*

I have not been able to get the steppers to move yet but I have been able to get the spindle to spin up using the big MOSFET 1.23. I last updated the firmware in July. 

[dropbox.com - config](https://www.dropbox.com/s/pyteh4b5duyyda6/config?dl=0)


---
**Michael Forte** *October 13, 2016 01:28*

Also, here's a pic of my board so you can see the pins I'm using.

![missing image](https://lh3.googleusercontent.com/9G8cKTL5MnPJHJW1zIjH_UfrOmP8xkxYT0Q5yciHDkCEaxOi4AXv2I_M04dmOsYu-NcrXib-uXxQZUlxd6um9QVN7xQNeFB3OA=s0)


---
**Triffid Hunter** *October 13, 2016 03:16*

You do have ground connected to your external drivers right? Try using smoothie's onboard drivers for testing, your motors will be a bit weak but should still turn well enough


---
**Michael Forte** *October 13, 2016 05:14*

**+Triffid Hunter** Yes the external drivers are grounded. Here's a pic of one of the motor controllers.

So are you saying I go straight from M1 to my X stepper motor and bypass the motor controller even though the motor requires 2.8A? It will still work with such a low current? 

![missing image](https://lh3.googleusercontent.com/IMdkSbN5uwhCSfA4gsPndFDxG1bytWkr8QV-iO0hCA9kO2Mr8rA-ALhuqrkjllsb6z6_Ec80xvBlYO3eBrpuLrDvHGvGLCxzFA=s0)


---
**Wolfmanjm** *October 13, 2016 05:58*

try inverting the enable pin in config. when the motor is enabled it will energize and you won't be able to turn it.


---
**Michael Forte** *October 13, 2016 06:14*

**+Wolfmanjm** Right now I have 

alpha_step_pin                             2.0o      

alpha_dir_pin                                0.5o        

alpha_en_pin                                 0.4o 

alpha_current                                2.8

alpha_max_rate                             30000.0



You want me to change alpha_en_pin only to 0.4o!

Is that correct?



By the way, do I even need alpha_current if I go through external motor drivers?




---
**Wolfmanjm** *October 13, 2016 06:18*

no I want you to change whatever channel you are using (you said Z) and change the gamma_en_pin so it has a ! after it. (or alpha if that is the channel you are testing)


---
**Michael Forte** *October 13, 2016 06:21*

Got it! I'll try it out in the morning and report back what happens.






---
**Michael Forte** *October 13, 2016 06:49*

I will first try inverting the pin in the config file and if I get no response from that then I will do what **+Triffid Hunter** suggests and bypass the external driver and wire straight from the board to the motor.


---
**Arthur Wolf** *October 13, 2016 08:58*

So you have it wired the same way as is shown in the "open-drain" section of [http://smoothieware.org/general-appendixes#external-drivers](http://smoothieware.org/general-appendixes#external-drivers) ?

Like this : [http://arthurwolf.github.io/smoothieboard-graphics/schematics/external-driver-open-drain.svg](http://arthurwolf.github.io/smoothieboard-graphics/schematics/external-driver-open-drain.svg) ?

[smoothieware.org - General Appendixes - Smoothie Project](http://smoothieware.org/general-appendixes#external-drivers)


---
**Triffid Hunter** *October 13, 2016 09:25*

Torque is loosely proportional to current, 2.8A motors at 2A should still be pretty strong. Keep in mind that many repraps are running 1.5-1.8A motors at only 1A with great results. Also keep in mind that if you run a motor at it's rated current, it'll heat up to about 80°C - usually we run them lower than their rating to prevent the motor getting too hot.



For best results at 2A, organise some airflow underneath the onboard stepper drivers - on smoothie, their heatsink is the back of the PCB. If your ambient temperature is hot, you might have to turn them down to 1.8A or so


---
**Michael Forte** *October 13, 2016 15:38*

**+Arthur Wolf** I'm wired up as shown in this pic with the open drain. I'm going to go try the suggestions from **+Wolfmanjm** and **+Triffid Hunter** right now and report back.

![missing image](https://lh3.googleusercontent.com/rsmpDtcpTKMpMbwzl7xoZimwA6QmXiEu3ZdRUe6YQeHekFrim0PCOFlh8HG2axRtC0F5-0BYXAcf4sWzCA_GT2ULyGHOhloFoA=s0)


---
**Michael Forte** *October 13, 2016 15:49*

I inverted the enable pins and now something is happening. I now have 

gamma_step_pin    2.2o  # Pin for gamma stepper step signal

gamma_dir_pin     0.20o  # Pin for gamma stepper direction

gamma_en_pin     0.19o! # Pin for gamma enable

gamma_current    1.68    # Z stepper motor current

gamma_max_rate 300.0 # mm/min



So here's the next problem. I go into the web browser UI and press the Z up or down buttons and I get that high pitch stepper motor sound but nothing is moving. 




---
**Michael Forte** *October 13, 2016 16:00*

I just tried it again but this time put slight force with my hand and it moved. It almost seems like it's not getting enough power. I know mechanically it's not too tight since I can move it pretty easily when no power is on. Any ideas?


---
**Arthur Wolf** *October 13, 2016 16:05*

**+Michael Forte** high pitch when moving Z 99% of the time means too high speed and/or too high accel for Z


---
**Michael Forte** *October 13, 2016 16:07*

I also tried with X and had the same result. Let me check my speed and accel settings




---
**Michael Forte** *October 13, 2016 16:15*

I just have the default settings in the config file. Do you have some ballpark figures for a NEMA 23 motor as I'm not sure where to start with this. All I know is I have 20 tooth gears and an GT3 belt on my XY and a screw for the Z. Here's that top portion from my config file.



# Robot module configurations : general handling of movement G-codes and slicing into moves

default_feed_rate                            4000             # Default rate ( mm/minute ) for G1/G2/G3 moves

default_seek_rate                            4000             # Default rate ( mm/minute ) for G0 moves

mm_per_arc_segment                           0.5              # Arcs are cut into segments ( lines ), this is the length for

                                                              # these segments.  Smaller values mean more resolution,

                                                              # higher values mean faster computation

mm_per_line_segment                          5                # Lines can be cut into segments ( not usefull with cartesian

                                                              # coordinates robots ).



# Arm solution configuration : Cartesian robot. Translates mm positions into stepper positions

alpha_steps_per_mm                           80               # Steps per mm for alpha stepper

beta_steps_per_mm                            80               # Steps per mm for beta stepper

gamma_steps_per_mm                           1600             # Steps per mm for gamma stepper



# Planner module configuration : Look-ahead and acceleration configuration

planner_queue_size                           32               # DO NOT CHANGE THIS UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING

acceleration                                 3000             # Acceleration in mm/second/second.

#z_acceleration                              500              # Acceleration for Z only moves in mm/s^2, 0 uses acceleration which is the default. DO NOT SET ON A DELTA

acceleration_ticks_per_second                1000             # Number of times per second the speed is updated

junction_deviation                           0.05             # Similar to the old "max_jerk", in millimeters,



# Stepper module configuration

microseconds_per_step_pulse                  1                # Duration of step pulses to stepper drivers, in microseconds

base_stepping_frequency                      100000           # Base frequency for stepping, higher gives smoother movement



# Cartesian axis speed limits

x_axis_max_speed                             30000            # mm/min

y_axis_max_speed                             30000            # mm/min

z_axis_max_speed                             300              # mm/min




---
**Arthur Wolf** *October 13, 2016 16:17*

Set acceleration to 100, and in the software you are controlling Smoothie from modify the speed to 100mm/minute, and try again.

Did you set the current on the stepper drivers correctly ( using the switches ) ?


---
**Michael Forte** *October 13, 2016 16:45*

That did it! Now I just need to tweak the numbers and calibrate it since the movements are at least 10 times what they should be. 



I sent a command M92 X2.0 which sets the X to 2 steps/mm from the original 80 until I could get a reasonable distance traveled for what I input through the browser. My driver dip switches are set to provide 16 steps/mm. I know the 2 can't be right and triple checked my dip switch settings. Is there some other variable in the config that would change this step size? In addition to the X, I'm also seeing excessive distances traveled on the Z with 1600 steps/mm set for the screw.



It just seems that the stepper motor is taking much bigger steps than it's supposed to.



I'm not sure if this is normal but one other thing is that my Z motor is very warm to the touch and I'm not even moving it. It's a NEMA 17 rated at 1.68A and I'm sending 1.5A from the driver. Maybe it's related, maybe not.


---
**Arthur Wolf** *October 13, 2016 16:46*

The switches do not adjust steps/mm, but microstepping. There are lots of information on that on the net ( and on the smoothie wiki ) which will help you figure out exactly what the right value is for your machine and given microstepping setting.


---
**Wolfmanjm** *October 13, 2016 19:09*

steppers are designed to run hot, they do not need to be moving as they are always energized. the specs for most steppers says they can run at 80°C but that is why most people in reprap run at 75% of rated current. However with CNC you want to run at full current.


---
**Michael Forte** *October 13, 2016 21:34*

**+Arthur Wolf** I will go and read up on the wiki and if I run into any problems I'll ask here.



**+Wolfmanjm** That's good to know about running motors at 100% current and the 80C limit. 


---
**Michael Forte** *October 14, 2016 02:14*

I found my other problem. The motor drivers were set at 16 for microsteps but that corresponded to 3200 steps/rev. If only I had taken the time to look past the first column to the second column. My motors are 1.8 deg/step which equates to 200 steps/rev. That setting is for 1 microstep. Once I set that everything seems to be working well now.



The question I have though is I thought you wanted to user higher microsteps for better accuracy??? Am I looking at this the wrong way? I'm sure it's just my understanding of how this works.


---
**Arthur Wolf** *October 14, 2016 08:57*

**+Michael Forte** You can use any microstepping you want. Higher microstepping only gets you higher resolution up to 1/16 ( approximately ) microstepping, microstepping above that doesn't really get you much more accuracy but reduces noise and problems with resonnance in leadscrew-driven systems.


---
**Michael Forte** *October 14, 2016 16:16*

My motor is 1.8 deg/step which equates to 200 steps/rev. Looking at the picture of the driver you see that 200 pulses/rev equals 1 microstep accuracy. The distance travel is accurate now when I test it.



Is there something in the config file that will let me use the 1/16 - 3200 setting on my driver without making my motor travel 16 times further?



I see the following for motor configuration in the config file:

===========

# Stepper module configuration

microseconds_per_step_pulse                  1 # Duration of step pulses to stepper drivers, in microseconds



base_stepping_frequency                      100000 # Base frequency for stepping, higher gives smoother movement

===========



If I change the microseconds_per_step_pulse to 16 and set the appropriate dip switches on my motor driver will that work? I'm not sure what the base_stepping_frequency is for so would I need to adjust that as well?



![missing image](https://lh3.googleusercontent.com/yRcMrvTNVpa8OLvl1live-7tO2YiqldYCJvr90zZTfcoGoLuZ1mbzMze34hs3CXnPSOB3MORKiiyG5d09lKeDF6m_ZXv4B4fpw=s0)


---
**Arthur Wolf** *October 14, 2016 16:21*

You want to read [http://smoothieware.org/cnc-mill-guide#toc10](http://smoothieware.org/cnc-mill-guide#toc10) ( the steps_per_mm section )

And the whole guide too :p


---
**Michael Forte** *November 03, 2016 02:29*

I have read the CNC mill guide so many times I can probably rewrite a lot of it from memory. ;0

I have an update and I'm getting closer but still have a few issues. The X and Y stepper motors now move in the proper directions which is a good thing. Z, on the other hand is not moving; I just hear a click as if the motor is engaging but there is no movement. Z was the first motor I had working so I'm not sure why this is happening now??? 



The other possibly related annoyance is that I can only get movement when my motor driver is set to 1 microstep for X,Y, and Z motors. When I try higher numbers of microsteps on the external motor driver, nothing happens in the motor, i.e., there is no change no matter what my step settings are. I have tried changing steps_per_mm and acceleration values but still no change. My X and Y motors seem to work fine but only when 1 microstep is set. Isn't it true that I can get better accuracy with a slightly higher microstep count like 4 or 8?



Anyway, I'm real close but I'm not sure what combinations of things to try anymore. Does anyone have config settings for NEMA 23 motors on an OX CNC using more than 1 microstep on the external motor driver?


---
**Wolfmanjm** *November 03, 2016 05:16*

take a look at the results at M503 is there an override effective? you would usually use x16 microsteps, this works fine. but you need to set steps/mm correctly.


---
**Michael Forte** *November 03, 2016 06:01*

**+Wolfmanjm** I will check the M503 tomorrow. Meanwhile, I want to make sure I'm adjusting the correct variables in the config file for 16 steps. Here is what I think I need to change. Does this look correct or am I way off base here? Are there variables I shouldn't touch or some I'm missing from this list?



Motor Controller: set dip switches to 16 microsteps (3200 pulses/rev)

default_feed_rate: FROM 4000 TO 250 (4000/16)

acceleration: FROM 3000 TO 187.5 (3000/16)

alpha_steps_per_mm: FROM 80 TO 1280 (80*16)

beta_steps_per_mm: FROM 80 TO 1280 (80*16)

gamma_steps_per_mm: FROM 1600 TO 25600 (1600*16)


---
**Michael Forte** *November 04, 2016 02:06*

No config override. Here is my M503 dump.

============

; No config override

;Steps per unit:

M92 X80.00000 Y80.00000 Z1600.00000

;Acceleration mm/sec^2:

M204 S1000.00000 Z500.00000 

;X- Junction Deviation, Z- Z junction deviation, S - Minimum Planner speed mm/sec:

M205 X0.05000 Z-1.00000 S0.00000

;Max cartesian feedrates in mm/sec:

M203 X500.00000 Y500.00000 Z5.00000

;Max actuator feedrates in mm/sec:

M203.1 X500.00000 Y500.00000 Z5.00000

;WCS settings

G54

;Digipot Motor currents:

M907 X2.00000 Y2.00000 Z1.68000 E1.50000 

============

Since I'm using external motor drivers, I'm assuming that it doesn't matter what is set in the motor currents, right?

The other numbers look pretty straight forward.



What about these potential settings. Does this make sense if I go to 16 microsteps?

Motor Controller: set dip switches to 16 microsteps (3200 pulses/rev)

default_feed_rate: FROM 4000 TO 250 (4000/16)

acceleration: FROM 3000 TO 187.5 (3000/16)

alpha_steps_per_mm: FROM 80 TO 1280 (80*16)

beta_steps_per_mm: FROM 80 TO 1280 (80*16)

gamma_steps_per_mm: FROM 1600 TO 25600 (1600*16)﻿


---
**Wolfmanjm** *November 04, 2016 04:44*

feed rate need not change nor should acceleration. as for steps/mm I cannot say it depends on what belts you are using and the teeth on the pulleys. or the lead on the lead screw.


---
**Michael Forte** *November 04, 2016 14:55*

3mm belts on 20T pulleys. Acme 8mm screw for Z. Pretty standard. 80 steps/unit looks fairly close to what I would expect just eyeballing it when I move the X or Y with 1 microstep on the driver. I'm just wondering the basic idea of what I'm supposed to do. 

If I set my external motor driver to 16 microsteps (3200 steps/rev) does that mean I just multiply my alpha_steps_per_mm (for X) by 16? Would I also need to change microseconds_per_step_pulse from 1 to 16? 

I just want to make sure I understand what needs to be changed so I don't go changing parameters that have no bearing on it. Those 2 variables I would think need to change in conjunction with each other.


---
**Wolfmanjm** *November 04, 2016 18:21*

no [http://prusaprinters.org/calculator/#stepspermmbelt](http://prusaprinters.org/calculator/#stepspermmbelt)

it would 

be 80steps/mm for 1/16 so that is you r problem. it would be 16 less for 1 step.

You do not change microseconds_per_step_pulse unless your eternal drivers need more than 1us step pulses.


---
**Michael Forte** *November 06, 2016 00:04*

Well after double and triple checking everything my Z still didn't work. I decided to start from one end and buzz out all the way to the other end and lo and behold my NEMA 17 wire colors are the same as my NEMA 23 wire colors BUT the colors don't correspond to coil pairs. The NEMA 23s are the same but the 17 swapped one color which meant I was sending a pulse to both ends of the same motor coil. (slaps hand to face). 



Well that only took a few weeks to figure out. Thanks for all the help in here. I'm sure I'm not done asking but now I have all motors moving properly.




---
*Imported from [Google+](https://plus.google.com/+MichaelForte66/posts/BBGMmmSLtz6) &mdash; content and formatting may not be reliable*
