---
layout: post
title: "Peter van der Walt Arthur Wolf New CHIP for Smoothiev2?"
date: October 13, 2016 17:07
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
**+Peter van der Walt** **+Arthur Wolf**

New CHIP for Smoothiev2?



[https://getchip.com/pages/chippro](https://getchip.com/pages/chippro)





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Arthur Wolf** *October 13, 2016 17:31*

**+Ray Kholodovsky** This sort of boards goes on extension boards in the Smoothie2 world.


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2016 17:32*

I understand :) 


---
**Triffid Hunter** *October 14, 2016 09:24*

I'm working in the same office as the NTC(chip) team :p


---
**Eric Lindahl** *October 14, 2016 15:07*

Does this mean most of you trust the AW R8?


---
**Arthur Wolf** *October 14, 2016 15:11*

**+Eric Lindahl** We'll try it. We are adventurers :p


---
**Ray Kholodovsky (Cohesion3D)** *October 15, 2016 16:04*

**+Triffid Hunter** are you in Shenzhen or do they have an office in the States too?  


---
**Venelin Efremov** *January 15, 2017 00:05*

You can also try the omega 2 by Onion ([http://onion.io](http://onion.io))


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/CwAb8zG6QTx) &mdash; content and formatting may not be reliable*
