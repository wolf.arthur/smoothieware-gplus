---
layout: post
title: "Arthur Wolf - have you ever considered adding the capability to fire more than one laser from the Smoothie?"
date: October 15, 2016 16:47
category: "General discussion"
author: "Phil Aldrich"
---
Arthur Wolf - have you ever considered adding the capability to fire more than one laser from the Smoothie?  I saw some references on the web to people who were using multiple LED lasers in much the same way a printer is configured with multiple nozzles in parallel. This would definitely speed up the laser engraving process. Interesting concept for us led laser users. 





**"Phil Aldrich"**

---
---
**Arthur Wolf** *October 15, 2016 17:33*

I don't think there is anyone in this community with such a setup, and from what I know from the industry they are very rare.

It's imaginable technically, though it'd be quite a bit of work so I'm not sure anyone who doesn't actually own such a machine would go through the effort ( which is I think why it doesn't exist yet ).


---
**Anthony Bolgar** *October 15, 2016 17:57*

Could you not just send the output signal to both lasers (I know they would be doing the same job, but with a high volume run, it would cut time in half)

 Sort of the same way we do double Y axis motors.


---
**Arthur Wolf** *October 15, 2016 17:57*

**+Anthony Bolgar** Yes, that's something you can do right now if you are engraving multiple images.


---
**Greg Nutt** *October 15, 2016 17:59*

**+Phil Aldrich** Could you provide any links demonstrating what you mean by this?  I'm trying to visualize what real differences in hardware/software you're asking for.


---
**Phil Aldrich** *October 15, 2016 19:16*

I don't know that I can find them again but I will look. The idea is that you have say 4 LED lasers aligned in a row (on the Y axis) so that each laser beam is spaced exactly one dot width from the next (somewhat like the old dot matrix printers are). When this array is moved in the X axis across a surface and each laser would be feed one unique row of gcode, you would effectively only need to make 1/4 the X axis passes and thus reduce your laser time by 1/4. 



I do like Anthony's idea of driving 2 lasers off the same output if you are mass producing objects of the same type. You would need to beef up your driver to handle it but it seems like it would work. 


---
*Imported from [Google+](https://plus.google.com/105080379699420524192/posts/M9jZXLnsSpR) &mdash; content and formatting may not be reliable*
