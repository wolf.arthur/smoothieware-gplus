---
layout: post
title: "I've got an issue with an X5 and a laser PSU"
date: October 17, 2016 03:28
category: "General discussion"
author: "K"
---
I've got an issue with an X5 and a laser PSU. This PSU uses IN for PWM (Not L and 5V like the stock K40) as well as ground. I have the INPUT connected to pin 2.4 on my X5 and Ground on the PSU connected to GND on my X5. Whenever the GND pin is connected the laser won't fire manually or otherwise. When I remove the GND connection I can then fire the laser with the test button. Has anyone else encountered this? If so, what might I be doing wrong?

This is my PSU: [http://www.lightobject.com/20W45W-PWM-CO2-Laser-Power-Supply-AC110V-P71.aspx](http://www.lightobject.com/20W45W-PWM-CO2-Laser-Power-Supply-AC110V-P71.aspx)





**"K"**

---
---
**K** *October 17, 2016 03:56*

**+Peter van der Walt** It is connected. I have the laser on/off button connected to WP and Ground. 


---
**K** *October 17, 2016 04:09*

Ok, I just tried leaving the ground connected to the X5 and pulling the 2.4 pin connection, and it works. So the question is, why does having the board hooked up stop the laser from firing.


---
**K** *October 17, 2016 04:55*

I switched to pin 2.5 (Hot end neg terminal) and now it fires manually just fine with everything connected, but it doesn't fire when the job is running.


---
**Alex Krause** *October 17, 2016 06:45*

Did you change your config file?


---
**K** *October 17, 2016 06:47*

**+Alex Krause** I did. I can't get the PWM to work using Light Object's recommended IN and ground. I switched to this because in some emails with them they insist that I should not be using 5V for PWM. Just IN and ground. 


---
**Alex Krause** *October 17, 2016 06:51*

**+Kim Stroman**​ I'm off work tomorrow... pm me in the AM. I have to hit the hay to take the kids to school


---
**Alex Krause** *October 17, 2016 06:52*

This power supply might require 2 active pins to enable


---
**K** *October 17, 2016 06:53*

**+Alex Krause** That's the dual pin thingie, right? It's frustrating because the K40 way (5V, L, G) works, but they make it sound like I'm going to damage the PSU using it that way. 


---
**Alex Krause** *October 17, 2016 06:56*

Light objects sells DSP controllers... and they are going to recommend you hook up your connections as if a smothieboard is a DSP... can you send them an email for a wiring diagram for a DSP to your psu? It may show something we are missing 


---
**K** *October 17, 2016 06:57*

**+Alex Krause** I think I have something already that they sent me. I'll PM it to you in the morning. 


---
**Arthur Wolf** *October 17, 2016 09:29*

So, you need to use a free GPIO pin ( a pin that has nothing on it like pull-ups or pull-downs, which I don't know which that would be on an azteeg ) that is <b>also</b> a PWM pin.

Then you connect that pin to GND on the PSU, and you connect GND from the Azteeg to the IN pin. Then you set that pin to be Open-Drain ( change 1.30 to 1.30o or 1.30o! for example ) and it should work.


---
**Joe Spanier** *October 17, 2016 13:20*

I had that same setup a couple months ago. Give me a couple hours and I'll get back to you with how I had it hooked up. 



I didn't have the open drain. I used the switch module and M3/M5 and the pwm pins. 


---
**Arthur Wolf** *October 17, 2016 13:21*

**+Joe Spanier** On the Smoothieboard pins output 3.3V so if you don't use open-drain on the IN/PWM pin you won't be getting full power on most power supply.


---
**Anthony Bolgar** *October 17, 2016 13:48*

**+Arthur Wolf** ,what pins would that be on a genuine Smoothieboard going to a PSU that has IN and L ?  (Like the one in the [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)) I ask because on my K40, even with a level shifter, I can only get about 10mA max with the single pin setup going to L


---
**Arthur Wolf** *October 17, 2016 13:49*

something like P1.30 is good


---
**Joe Spanier** *October 17, 2016 14:21*

Even with a Level shifter? **+Arthur Wolf** This is where my electronics knowledge hurts me.


---
**Arthur Wolf** *October 17, 2016 14:22*

Nah a level shifter means you can use any pwm pin, but I recommend open-drain it's simpler, and some level shifter chips can cause problems.


---
**Joe Spanier** *October 17, 2016 14:45*

It is simpler. Do you know what Pin would work well on the x5GT? **+Roy Cortes** 



The pin Roy recommended was 1.23. It was the servo pin on his board. 



For the record I was using the sparkfun level shifter.


---
**Anthony Bolgar** *October 17, 2016 14:51*

Is piun 2.5 capable of being set to open drain? If it is then all I would need to do is just get rid of the level shifter and make a small config file change to 2.5o


---
**K** *October 17, 2016 16:24*

**+Arthur Wolf** 

So something like this:  

pin 1.3 (in the EXP 1 group of pins) to GND on my PSU

GND on the X5 to IN on my PSU



Then in my config change "#laser_module_pin to 1.3o"



Edit: Also, will this setup allow a pot?


---
**Don Kleinschnitz Jr.** *October 17, 2016 17:38*

You might justifiably be skeptical of this cause I have not tested it since I have been waiting to see the LPS internals.  

Now that we have a glimpse the internals of the LPS as I suspected the Pot biases the current in a diode on the transmitter side of a opto-coupler which on the receiver side adjusts the DF of the LPS internal PWM. The other controls grnd the transmitting diode on similar opto-couplers to enable disable the internal PWM of the supply.

So putting a level shifter and pwm IN creates a complex circuit and the current in the power control opto-couplers transmitter depends on the position of the pot and the PWM setting from the controller. To my thinking using IN does not provide predicable control.



All we should have to do is connect an "Open drain" to the L pin. The below  picture shows my wiring soon to be tested. I  plan to use P2.4 as the open drain running grnd true. 

I pick it up on the board edge connector for P2.4 and grnd on the "power in" connector two to the right.



The pot can be left alone and it will still set a power limit. However I expect the real power will be the product of the two settings. 

So set the pot to 50% power and the PWM to 50% power and you get  25% power.



I don't know anything about when the smoothie outputs its PWM I assume if a job is not running it is High. I have also heard there is some kind of reset glitch problem.

![missing image](https://lh3.googleusercontent.com/3EArnT6teGP0LCchdif22kFRIYqjLYvS7EwvKE5iL9p1uWtTyPwePvGzlrL3FUnJKClAeGH26qD8m9d2cPO6FSkT7UiuLRzf2zKy=s0)


---
**Don Kleinschnitz Jr.** *October 17, 2016 17:38*

Here is the PS end of my PWM setup.

Also using a level shifter on the L pin still seems unnecessary to me since the pin is connected to the cathode of a LED and just needs a grnd so the LED can turn on the opto's receiver and in turn the LPS.



BTW consider that the L pin is the only connection between the Nano and the LPS on a stock K40.

![missing image](https://lh3.googleusercontent.com/wawf1r-EzqKZ8aswNCk16fzulsERMD0jjeTEWWM_pEdGHSEpQdlNAXjumFGwvZepFleBxkAaEtNlKbzH-aBVZv9Ulipfc8tv5vVe=s0)


---
**K** *October 17, 2016 21:45*

I tried this: pin 2.4o (in the EXP 1 group of pins) to GND on my PSU

GND on the X5 to IN on my PSU



And I have the same issue. Whenever the board is connected to the laser in this manner the laser itself won't fire manually. Once I disconnect either the ground or the 2.4 pin the laser will fire.


---
**K** *October 17, 2016 21:46*

It will, however, fire if I have it set up through the shifter (5v, L, G).


---
**K** *October 18, 2016 00:26*

I ended up going back to using the shifter and 5V, L, G. I had moved to the other setup because LightObject seemed to think using those pins was a bad idea, but it works, so I'm going with it.


---
**Don Kleinschnitz Jr.** *October 18, 2016 01:56*

**+Kim Stroman** I'm  lost ..... you connected 2.4 to gnd, and gnd to IN??? if that is correct that will not work.



Is it all working now including the current adjustment. Do you have a pot connected?


---
**K** *October 18, 2016 02:04*

**+Don Kleinschnitz**  Correct. I followed this post: "So, you need to use a free GPIO pin ( a pin that has nothing on it like pull-ups or pull-downs, which I don't know which that would be on an azteeg ) that is also a PWM pin.

Then you connect that pin to GND on the PSU, and you connect GND from the Azteeg to the IN pin. Then you set that pin to be Open-Drain ( change 1.30 to 1.30o or 1.30o! for example ) and it should work."


---
**K** *October 18, 2016 02:09*

I have it working now using the level shifter and k40 style setup. The new pot seems to have made a difference. I'm cutting fine, but so far it's not looking like I'm actually getting any PWM when rastering. I'm testing that as we speak.


---
**Don Kleinschnitz Jr.** *October 18, 2016 02:19*

**+Kim Stroman** when you get a minute I would appreciate a diagram that shows how you are currently wired.... cause I am lost :)


---
**K** *October 18, 2016 02:21*

**+Don Kleinschnitz** I have it done just like this: [http://chibidibidiwah.wdfiles.com/local--files/blue-box-guide/k40SmoothieWiring4.png](http://chibidibidiwah.wdfiles.com/local--files/blue-box-guide/k40SmoothieWiring4.png)

but I had to connect ground to both sides of the shifter to get it to work.


---
**Don Kleinschnitz Jr.** *October 18, 2016 12:11*

is this the level shifter you have used?

[https://www.sparkfun.com/products/12009](https://www.sparkfun.com/products/12009)


---
**K** *October 18, 2016 12:13*

**+Don Kleinschnitz**  Here's the one I used. I think it's a straight knockoff of the SparkFun shifter because it uses the same description.  XCSOURCE 5PCS IIC I2C Logic Level Converter Bi-Directional Module 5V to 3.3V TE291 [https://www.amazon.com/dp/B0148BLZGE/](https://www.amazon.com/dp/B0148BLZGE/)[amazon.com - Amazon.com: XCSOURCE 5PCS IIC I2C Logic Level Converter Bi-Directional Module 5V to 3.3V TE291: Electronics](https://www.amazon.com/dp/B0148BLZGE/ref=cm_sw_r_cp_api_hgHbybPAD8CN6)


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/idfiycYcfFp) &mdash; content and formatting may not be reliable*
