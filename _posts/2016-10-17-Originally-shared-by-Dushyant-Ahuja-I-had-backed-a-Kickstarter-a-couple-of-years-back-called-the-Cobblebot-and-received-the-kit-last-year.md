---
layout: post
title: "Originally shared by Dushyant Ahuja I had backed a Kickstarter a couple of years back called the Cobblebot and received the kit last year"
date: October 17, 2016 14:11
category: "General discussion"
author: "Dushyant Ahuja"
---
<b>Originally shared by Dushyant Ahuja</b>



I had backed a Kickstarter a couple of years back called the Cobblebot and received the kit last year. The kit and instructions were abysmal to say the least; and looks like I was one of the lucky ones – that I was able to print using my kit. However, I set about modifying the kit, based on suggestions from the Cobblebot Google+ Community, as well as my designs.



While I had been slowly but steadily improving the mechanics of the printer, adding dual extruders, et al; I had ignored the electronics till now. I was still printing through the RAMPS + Arduino Mega that Cobblebot had provided. I had been thinking of upgrading to a 32-bit board and had done some preliminary research – narrowing down to a Smoothieware based system. While the documentation on the Smoothieware wiki is great, there was no step-by-step document that detailed out how to upgrade from Marlin / Repetier to Smoothieware; there was no way to find out which settings were to be transposed where. So.. here it is.





**"Dushyant Ahuja"**

---
---
**Alex Krause** *October 17, 2016 14:54*

**+Ray Kholodovsky**​


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/382cwby5xQs) &mdash; content and formatting may not be reliable*
