---
layout: post
title: "I am trying to run some GCode on the Smoothieboard SD card by pressing a button (eg pin on the smoothieboard) Can anyone help with the config for the smoothieboard to accomplish this"
date: October 18, 2016 13:08
category: "General discussion"
author: "Alexander B"
---
I am trying to run some GCode on the Smoothieboard SD card by pressing a button (eg pin on the smoothieboard)

Can anyone help with the config for the smoothieboard to accomplish this.

I have tried the following with no luck



switch.playgcode1.enable                     true

switch.playgcode1.input_pin                  1.28

switch.playgcode1.output_on_command          play /sd/test.gcode 



Thanks in advance for your help

Alexander





**"Alexander B"**

---


---
*Imported from [Google+](https://plus.google.com/113176822755889418215/posts/52kxVbQrDyr) &mdash; content and formatting may not be reliable*
