---
layout: post
title: "Smoothie project looking for talented JS Devs !"
date: October 18, 2016 20:55
category: "General discussion"
author: "Arthur Wolf"
---
<b>Smoothie project looking for talented JS Devs !</b>

<b>Looking for work ? Looking to help the community ? Read ahead !</b>



The Smoothieboard has this awesome Ethernet interface that allows users to control the board with a web interface, and we have been under-utilizing that for years now. Lots of untapped potential.



We've been working on a really great project called fabrica. Think of it as what you get with a classic reprap monochrome LCD screen, but using a web interface, and 100x better.

Github : [https://github.com/arthurwolf/fabrica](https://github.com/arthurwolf/fabrica)

Presentation : [goo.gl/CFjujJ](http://goo.gl/CFjujJ)



We are looking for people to help us as volunteers ( it's an Open-Source project a lot of people want to exist ), but we are also looking for people who would be ready to work on this for hire ( on a per-bounty basis ).



If you are a web developer with good javascript knowledge, please don't hesitate to contact us at wolf.arthur@gmail.com



<b>Please reshare !</b> ( like : don't +1, reshare, you'd really be helping :) Please, pretty please ! )





**"Arthur Wolf"**

---
---
**David Bassetti** *October 25, 2016 18:20*

I'll help... !!




---
**Arthur Wolf** *October 25, 2016 18:33*

**+David Bassetti** Email me about it :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/WUiP2a3oJpN) &mdash; content and formatting may not be reliable*
