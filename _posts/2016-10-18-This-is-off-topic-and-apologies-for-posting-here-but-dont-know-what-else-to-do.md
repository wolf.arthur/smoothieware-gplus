---
layout: post
title: "This is off topic and apologies for posting here - but don't know what else to do"
date: October 18, 2016 12:47
category: "General discussion"
author: "Dushyant Ahuja"
---
This is off topic and apologies for posting here - but don't know what else to do. I've been banned from the 3D printing community and would simply like to know why. If anyone here know the moderators - can you please help. 







**"Dushyant Ahuja"**

---
---
**Arthur Wolf** *October 18, 2016 12:54*

**+ThantiK** ?


---
**Griffin Paquette** *October 18, 2016 13:15*

**+Ray Kholodovsky** 


---
**Dushyant Ahuja** *October 18, 2016 14:09*

**+Peter van der Walt** thanks - but this is what I see when I go to the page. ![missing image](https://lh3.googleusercontent.com/kmTerhkC_wHQOZsulvSE6rHlPCTBE1FxFYcBXa3aNXjXljU4LfEIF_AD3EAEXMm1ONBAJEobpae-vGs=s0)


---
**Dushyant Ahuja** *October 18, 2016 14:15*

**+Peter van der Walt** thanks for looking, but please don't waste any more time on this. I was just curious - that's all. I can still browse the posts - so I'm good. I'll keep sharing my stuff on the other communities. 


---
**Dushyant Ahuja** *October 18, 2016 15:02*

**+Peter van der Walt** thanks a lot. 


---
**Ray Kholodovsky (Cohesion3D)** *October 18, 2016 16:01*

Thanks Peter. (The man with a plan)

**+Dushyant Ahuja** post stuff! 


---
**ThantiK** *October 18, 2016 19:35*

Given that he shares a plethora of stuff, could he have accidentally shared something with the 3D printing community that was meant for a different community?  I don''t hesitate to ban if something is in the mod queue and doesn't even remotely look 3D printing related.


---
**Dushyant Ahuja** *October 18, 2016 19:37*

Generally I'm careful about what I post. But it could have happened. Just wanted to understand if there was a specific reason. Thanks. 


---
**Dushyant Ahuja** *October 18, 2016 19:38*

**+Peter van der Walt** I'm just one of the authors on 3dprinterchat. Not all the posts are mine :-)


---
**Dushyant Ahuja** *October 18, 2016 19:43*

**+ThantiK** I was more concerned with the fact that I may have been banned after I recommended the tevo tarantula on one of the posts. If that was the case - I guess there is a problem. If not - and this was a mistake - all is good. 


---
**ThantiK** *October 18, 2016 23:32*

I don't know what tevo tarantula is.  It doesn't stick out in my mind.  I am quick with the ban hammer, because it can easily be undone, and it doesn't get rid of <i>all</i> of the users posts.  If it looks like spam, I ban it quickly because we've had things like child porn, beastiality, etc after not quickly getting other stuff down.



[3dprinterchat.com](http://3dprinterchat.com) is one that miffs me a little bit, because we've had a few people who spammed nothing but [3dprinterchat.com](http://3dprinterchat.com) - I suspected they were affiliated, and when asked...the profile went dead-silent.  So those do get a bit of harsh treatment.


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/TJ7t3LtRACX) &mdash; content and formatting may not be reliable*
