---
layout: post
title: "WooHoo! 5 smoothieboards showed up in the mail today"
date: October 20, 2016 17:30
category: "General discussion"
author: "Anthony Bolgar"
---
WooHoo! 5 smoothieboards showed up in the mail today. Time to start converting machines!





**"Anthony Bolgar"**

---
---
**Ariel Yahni (UniKpty)** *October 20, 2016 17:35*

5??? I need to start building more machines


---
**Anthony Bolgar** *October 20, 2016 17:54*

:)


---
**Todd Fleming** *October 20, 2016 20:04*

Are you connecting them together for a 25-axis machine?


---
**Anthony Bolgar** *October 20, 2016 21:09*

lol...a few of them are broken boards for parts, 2 of them are basically good boards with minor problems (one has a bad stepper driver, the other a bad FET, but still usable)\


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/ZGZLp1R5TP4) &mdash; content and formatting may not be reliable*
