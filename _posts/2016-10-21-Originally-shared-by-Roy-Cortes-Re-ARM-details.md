---
layout: post
title: "Originally shared by Roy Cortes Re-ARM details"
date: October 21, 2016 04:33
category: "General discussion"
author: "Wolfmanjm"
---
<b>Originally shared by Roy Cortes</b>



Re-ARM details.



The Re-ARM is basically designed for the RAMPS and machine control in general running smoothie. Most of the usable pins of the LPC chip are broken out including the Ethernet/LAN pins.



The Re-ARM can take up to 30V input but we all know some RAMPS can only do 16V due to component voltage limitation, others can do 24V no problem.



On-board mSD card slot for config file and firmware updates

DC jack for external supply

USB mini B with ESD protection ( USB A shown on prototypes) 

LAN daughter board available



What works on the RAMPS.

-  All 3 heaters ( 5V gate drive)

-  All 6 endstops ( with RC filter on each pin)

-  All 3 thermistor inputs ( also available on separate pins using analog GND)

-  All 5 drivers

-  Servo pin ( 5V signal)

-  Viki2 , mini Viki and RRD graphic GLCD works ( Character LCDs needs an adapter)

-  Some expansion pins on the RAMPS are available but not all are connected. A bunch of expansion/free pins are available on the Re-ARM.



Thinking of making custom shields? A diagram showing pin designations is in the works.



Quick video of Re-ARm with CNC shield with 3 external drivers on my C-Beam.









**"Wolfmanjm"**

---


---
*Imported from [Google+](https://plus.google.com/101797329177621784159/posts/AxoT5WWpYba) &mdash; content and formatting may not be reliable*
