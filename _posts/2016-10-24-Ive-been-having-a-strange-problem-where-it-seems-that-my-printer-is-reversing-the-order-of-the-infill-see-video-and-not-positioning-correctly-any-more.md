---
layout: post
title: "I've been having a strange problem where it seems that my printer is reversing the order of the infill (see video) and not positioning correctly any more"
date: October 24, 2016 02:09
category: "General discussion"
author: "Oliver Seiler"
---
I've been having a strange problem where it seems that my printer is reversing the order of the infill (see video) and not positioning correctly any more. This started after I paused a print yesterday with M600. I've checked everything, the file looks valid and plays correctly on another printer, the config is still the same, I've updated the fw to the latest edge but cannot get rid of this behaviour.

If I run the G-code manually line by line the carriage moves as desired, but not when I play the whole file.

I'm running a Smoothieboard 5xc. 

Any ideas what might be causing this?


{% include youtubePlayer.html id="wu15hz8tylI" %}
[https://youtu.be/wu15hz8tylI](https://youtu.be/wu15hz8tylI)





**"Oliver Seiler"**

---
---
**Oliver Seiler** *October 24, 2016 02:14*

[https://goo.gl/photos/aU9MLaQVXM2PKAHa8](https://goo.gl/photos/aU9MLaQVXM2PKAHa8)



[photos.google.com - Neues Foto von Oliver Seiler](https://goo.gl/photos/aU9MLaQVXM2PKAHa8)


---
**Oliver Seiler** *October 24, 2016 02:14*

[https://goo.gl/photos/xUQu3mqT3wJbRNCZ6](https://goo.gl/photos/xUQu3mqT3wJbRNCZ6)

[photos.google.com - Neues Foto von Oliver Seiler](https://goo.gl/photos/xUQu3mqT3wJbRNCZ6)


---
**Oliver Seiler** *October 24, 2016 02:17*

Gcode: [https://drive.google.com/open?id=0B2br-OijkAgwdlp1OXJveklIR2c](https://drive.google.com/open?id=0B2br-OijkAgwdlp1OXJveklIR2c)



[drive.google.com - square.gcode - Google Drive](https://drive.google.com/open?id=0B2br-OijkAgwdlp1OXJveklIR2c)


---
**Wolfmanjm** *October 24, 2016 02:58*

what exactly is the problem?

 it looks like it is working perfectly well to me.


---
**Oliver Seiler** *October 24, 2016 03:13*

**+Wolfmanjm** Sorry, it's hard to see. When printing the infill it seems to jump a line, print the next line first leaving a gap and then reverses to print where it left the gap. Until yesterday it printed line after line, now it does line 2, 1, 4, 3, 6, 5 - if that makes sense. Also if you look at the photos, you can see a gap between perimeters and infill on the left. That's not supposed to be there and wasn't there when I printed the same part using the same gcode  the day before.




---
**Wolfmanjm** *October 24, 2016 03:19*

that can't possibly be a firmware issue, smoothie can only do what it is told to do in the gcode in the order it is told to do it, it is impossible to swap the order. maybe the host is scrambling the lines? what host are you using?


---
**Oliver Seiler** *October 24, 2016 03:35*

I'm playing from the SD card




---
**Oliver Seiler** *October 24, 2016 03:37*

And I've tried a completely new SD card with nothing but the firmware, config and gcode on - same result




---
**Wolfmanjm** *October 24, 2016 03:46*

that is very odd. how do you initiate the gcode on the sdcard?


---
**Wolfmanjm** *October 24, 2016 03:47*

and how do you load the gcode onto the sdcard?


---
**Oliver Seiler** *October 24, 2016 03:53*

I use either play on the lcd menu or the web interface. I usually load the files via the USB cabel, but I've also tried loading onto the SD directly in my laptop and playing on the printer. 

The same card in a Repetier based printer printer in the expected way.

I know this sounds really weird, hence I have no idea what might be causing it.


---
**Oliver Seiler** *October 24, 2016 04:02*

BTW I don't think that it is actually reversing the order of the commands - it looks more like it's almost mirroring or having some other sort of positioning issues.




---
**Wolfmanjm** *October 24, 2016 04:11*

sorry i have no idea what is wrong. i'd suggest trying to stream the gcode file from pronterface over usb. network is flaky so i would not trust it. streaming would eliminate potential adcard issues and corrupt gcode files. also do not allow the network to be connected while printing. it can cause odd behavior.


---
**Oliver Seiler** *October 24, 2016 07:03*

I'm getting the same issue printing via USB directly from Simplify3d, with the network disabled :(




---
**Wolfmanjm** *October 24, 2016 07:20*

oh you sliced with s3d... no wonder, it produces crap sometimes. also s3d is not a host we support for smoothie, try pronterface or octoprint. also

 check your nozzle is not partially clogged.


---
**Wolfmanjm** *October 24, 2016 07:20*

also try slicing with slic3r.


---
**Oliver Seiler** *October 24, 2016 07:34*

I've just found the problem... the x axis had a little play. The motor mount must have slipped on the extrusion when it halted yesterday. This is slightly embarrasing =:o)

Thanks **+Wolfmanjm** for your time and sorry I didn't pick this up earlier!

BTW I've been using Simplify3D with my Smoothieboard for 1.5 years now and I'm pretty happy with it. (I think Sommthieware has done something to tackle the tiny wobbly track breakdown, hasn't it?)


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/3rxfZAgc2fQ) &mdash; content and formatting may not be reliable*
