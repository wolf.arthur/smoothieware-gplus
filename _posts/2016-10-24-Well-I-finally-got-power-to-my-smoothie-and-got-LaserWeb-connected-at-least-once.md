---
layout: post
title: "Well I finally got power to my smoothie and got LaserWeb connected at least once"
date: October 24, 2016 01:15
category: "General discussion"
author: "Don Kleinschnitz Jr."
---
Well I finally got power to my smoothie and got LaserWeb connected at least once.

HOWEVER...

I got smoothie up and running installed the windows drivers (win7) and got LaserWeb connected.

This was with the original SD card.

I could see the SD card mounted and see its files



Things looked good:

What I could see was Smoothie's green lights blinking but the LCD display was backlit and blank. 

Then I remembered I had to enable the display. 

So I got a new SD card formatted it (Fat32) loaded the latest firmware.bin on it with a config file that had the panel turned on. 

I plugged that in and powered on. The green LED counted up so I thought I was good, except no control panel and the PC said that it did not recognize the device???



I powered down the smoothie and swapped the card with the original. Now no matter what SD card I put in it all the LED's are on solid all the time and the PC does not recognize the device. I even tried reinstalling the drivers and that makes no difference.



The new SD card still has the firmware.bin on it so I guess it did not flash the new code.



Now ..... when I apply power all the LED's come on and stay that way. Pushing the reset button doesn't do anything?



Uggh ....

Some Ideas to try? I searched the community and am out of ideas.





**"Don Kleinschnitz Jr."**

---
---
**Wolfmanjm** *October 24, 2016 01:21*

could be a number of things. Did you unmount/safely eject the sdcard after writing to it? if not it is corrupted. Did you download and check the md5sum of the firmware.bin? if not then you may have downloaded the HTML instead of the binary and flashed HTML which won't work.

Also check the troubleshooting guide in the wiki.


---
**Don Kleinschnitz Jr.** *October 24, 2016 01:56*

So I no more than got off this post and it dawned on me that  perhaps it was a problem with the display hanging up the processor. I unplugged the display and found a bent pin.



That led me to find a melted connector. Must have touched my  iron when I was soldering in the regulator. (see lower connector).



[https://goo.gl/photos/YvRSvCF9nZCtKTvh7](https://goo.gl/photos/YvRSvCF9nZCtKTvh7)






---
**Don Kleinschnitz Jr.** *October 24, 2016 01:56*

I have a happy smoothie tonight



[https://goo.gl/photos/osdKLwQFd8xNWp56A](https://goo.gl/photos/osdKLwQFd8xNWp56A)




---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/NCTvAQtQYW6) &mdash; content and formatting may not be reliable*
