---
layout: post
title: "Hi Group, I'm just getting back to my 3D printer project after a complete kitchen remodel and I need a little help"
date: October 26, 2016 16:36
category: "General discussion"
author: "Daniel Benoit"
---
Hi Group,



I'm just getting back to my 3D printer project after a complete kitchen remodel and I need a little help. I'm a complete newbie when it comes to printing, but have quite a bit of experience converting machines to new electronics. I have everything working great mechanically on the printer and calibrated it as best I could the way it is. I designed a very simple test piece in Alibre and sliced it with Simplify3D. (I've also tried printing other test pieces with the same problem) When I try to print, the printer does a little routine to clean the head near the home position at the correct Z height off the build plate then it raises the Z axis approximately 5mm, proceeds to the center of the build plate and starts to print (Z axis) without lowering the Z axis back down to the plate. Of course printing in the air just doesn't work. I know this is a software or firmware problem, but I just don't know where to start looking. I can't see anything obvious in the G code.



I've attached both the g code file and the config file from my smoothie board.



[http://pastebin.com/VrRGJFRY](http://pastebin.com/VrRGJFRY)



[http://pastebin.com/q2apA1Lx](http://pastebin.com/q2apA1Lx)





I'd sure like to start printing with my new machine. I apologize if some of this is duplicated from a previous post.



Thanks for your thoughts,



Dan





**"Daniel Benoit"**

---
---
**Daniel Benoit** *October 28, 2016 20:22*

I solved the problem. It was an error in the Smoothie Config file. I changed Gamma Min from -5 to 0. I probably screwed that up during my initial setup. Now I'm getting socket error 10054 during a print kicking me off from pronterface while printing over a network connection. Damn it!


---
*Imported from [Google+](https://plus.google.com/+sandcrab123/posts/9RnvMezYisR) &mdash; content and formatting may not be reliable*
