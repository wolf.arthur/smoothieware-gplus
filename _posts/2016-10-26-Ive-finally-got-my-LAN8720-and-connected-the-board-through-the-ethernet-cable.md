---
layout: post
title: "I've finally got my LAN8720 and connected the board through the ethernet cable"
date: October 26, 2016 01:43
category: "General discussion"
author: "Dushyant Ahuja"
---
I've finally got my LAN8720 and connected the board through the ethernet cable. The web interface is amazing, but, as usual, I have some requests:

1. Can we have a REST interface, so that I can switch on/off the relay connected to one off the pins. e.g. <SmoothieIP>/webif/REST/M80 - sends the M80 command to the board

2. Is it possible to add/ remove buttons to the interface - similar to the Custom Control Editor plugin in Octoprint.

3. Upload to SD via the web interface takes a looooooong time. Is there any way to speed this up? What do others use?



Thanks as always.





**"Dushyant Ahuja"**

---
---
**quillford** *October 26, 2016 05:33*

1. this is how the web interface works. just open a console in your browser and look at the requests being sent to the board. i think it's ip/command or something.



2. yes. you can modify the page and put it on your sd. then to use it, go to ip/sd/index.html or wherever you put it on the sd. [https://github.com/Smoothieware/Smoothieware/tree/edge/src/libs/Network/uip/webserver/httpd-fs-src](https://github.com/Smoothieware/Smoothieware/tree/edge/src/libs/Network/uip/webserver/httpd-fs-src)



3. not really. i think some people use one of those wifi sd cards and directly upload to the sd that way to get around this

[github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/tree/edge/src/libs/Network/uip/webserver/httpd-fs-src)


---
**Arthur Wolf** *October 26, 2016 08:50*

1. No, we are not changing the API, it currently works perfectly, does what you want, just with a different syntax, and POST instead of GET.

2. Yes, just edit the interface in the firmware and compile it, or setup your own interface on the SD card and edit it ( [http://smoothieware.org/install-web-interface](http://smoothieware.org/install-web-interface) )

3. No way to speed this up, v2 will be faster.

[smoothieware.org - Install Web Interface - Smoothie Project](http://smoothieware.org/install-web-interface)


---
**Dushyant Ahuja** *October 26, 2016 11:57*

**+Arthur Wolf** can you please explain how 1 would work with the current API - I'm not very good at web development. 




---
**Arthur Wolf** *October 26, 2016 12:01*

**+Dushyant Ahuja** You can just look at the source code for the current web interface, I think it is very simple and shows it very well, just do the same.


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/YZWXkv4GGFi) &mdash; content and formatting may not be reliable*
