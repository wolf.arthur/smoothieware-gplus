---
layout: post
title: "Problem with Smoothie. Yesterday I was getting the best prints yet and finished one print, disconnected, reconnected later, now no steppers are working at all"
date: October 26, 2016 18:23
category: "General discussion"
author: "Dean Rock"
---
Problem with Smoothie. Yesterday I was getting the best prints yet and finished one print, disconnected, reconnected later, now no steppers are working at all. Reset sequence gives me red, orange, green, two blinkers, green, LEDs. Hot bed and extruder heat up when requested but the green LED next to the bottom goes off when stepper movement is requested and no steppers move at all. I'm guessing a short somewhere but am clueless where it might be. Any thoughts +Wolfmanjm?﻿





**"Dean Rock"**

---
---
**Arthur Wolf** *October 26, 2016 19:04*

That does sound bad ... red means the drivers are getting power. Do the motors have torque after you ask them to move at least once ?


---
**Dean Rock** *October 26, 2016 20:27*

Just checked. Nope.


---
**Arthur Wolf** *October 26, 2016 20:37*

Well I really have no idea what it could be ...


---
**Dean Rock** *October 26, 2016 21:21*

My only guess is that some component has failed between power and steppers but I don't know what the primary point of that distribution is. Could something like the Ethernet connectors short the board? I am using the USB but bumbled around once and tried to insert that cable it in the Ethernet slot. However, the problem began before that or I wouldn't have been messing around down there. Things were good.


---
**Arthur Wolf** *October 26, 2016 22:16*

A short would mean the red light wouldn't turn on. And nothing between the power input and the drivers can really fail for all drivers at the same time.

Is this a 5XC ?


---
**Dean Rock** *October 26, 2016 22:32*

No, Smoothieboard.


---
**Arthur Wolf** *October 26, 2016 22:35*

I meant is it a Smoothieboard 4XC or 5XC ?


---
**Dean Rock** *October 26, 2016 22:47*

5


---
**Arthur Wolf** *October 26, 2016 22:48*

Do you have M5 configured ? Are you able to move that one ?


---
**Dean Rock** *October 26, 2016 22:59*

M3 failed about a month ago so I switched it to M5.


---
**Arthur Wolf** *October 26, 2016 23:14*

Ok so it's not the digipots, they wouldn't fail both at the same time. I'm really at a loss ...


---
**Dean Rock** *October 26, 2016 23:19*

Well, geez, if you're at a loss...   I'm not likely to do any better! I guess it is back to the ol' ordering board... so to speak. Thanks for your time!

Dean


---
*Imported from [Google+](https://plus.google.com/116087024084558080588/posts/EHWi2EAaeEw) &mdash; content and formatting may not be reliable*
