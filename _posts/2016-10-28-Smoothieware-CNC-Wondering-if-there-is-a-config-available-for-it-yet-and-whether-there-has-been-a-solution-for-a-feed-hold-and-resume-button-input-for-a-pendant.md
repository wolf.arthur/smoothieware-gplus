---
layout: post
title: "Smoothieware CNC Wondering if there is a config available for it yet and whether there has been a solution for a feed hold and resume button input for a pendant?"
date: October 28, 2016 14:43
category: "General discussion"
author: "Ryan Fritts"
---
Smoothieware CNC  Wondering if there is a config available for it yet and whether there has been a solution for a feed hold and resume button input for a pendant?





**"Ryan Fritts"**

---
---
**Arthur Wolf** *October 28, 2016 17:29*

The default config should work pretty well for a CNC, just comment out the extruder and temperaturecontrol stuff ( or just delete it and whatever else you don't use ).

For feed hold, you can use the suspend and resume commands. You can tie those to a pin using the switch module, see the examples in [smoothieware.org/switch](http://smoothieware.org/switch)


---
*Imported from [Google+](https://plus.google.com/103448858896852231120/posts/bSLMM1iuEKL) &mdash; content and formatting may not be reliable*
