---
layout: post
title: "Hello! I accidentally broke one of the small mosfets on my smoothieboard and now the broken mosfet stays always on"
date: October 30, 2016 20:07
category: "General discussion"
author: "Topias Korpi"
---
Hello!



I accidentally broke one of the small mosfets on my smoothieboard and now the broken mosfet stays always on. I was playing with waterpump and I don't have a diode for the mosfet, so i guess that destroyed it.

My question is, if I replace this component should the board work again as intended?

[https://octopart.com/zxmn4a06gta-diodes+inc.-660114](https://octopart.com/zxmn4a06gta-diodes+inc.-660114)





**"Topias Korpi"**

---
---
**Arthur Wolf** *October 30, 2016 20:16*

Yes replacing it should work. Also buy a diode !


---
**Topias Korpi** *October 30, 2016 20:27*

**+Arthur Wolf** Thanks for your fast response! I will replace that mosfet and definitely put a diode in at this time...


---
**Triffid Hunter** *October 31, 2016 08:23*

Note that you will need hot air to replace that mosfet, a normal soldering iron will likely damage the board before the mosfet comes loose.


---
**Topias Korpi** *October 31, 2016 08:25*

**+Triffid Hunter** Okay, thanks for the info.


---
**Griffin Paquette** *October 31, 2016 11:43*

I've had to replace the linreg on my Arduinos with a pencil iron before (was the same package config). If you carefully clip the lets with wire cutters you should be okay with a normal iron. Be very careful though. Hot air is definitely better if you have access. 


---
**Topias Korpi** *October 31, 2016 11:58*

**+Griffin Paquette** I have butane powered soldering iron, which can be changed to spot heater torch. Would that be correct tool for the job?


---
*Imported from [Google+](https://plus.google.com/+TopiasKorpiTK/posts/YycgYoczMmr) &mdash; content and formatting may not be reliable*
