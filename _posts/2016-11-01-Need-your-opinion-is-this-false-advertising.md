---
layout: post
title: "Need your opinion, is this false advertising ?"
date: November 01, 2016 21:53
category: "General discussion"
author: "Arthur Wolf"
---
Need your opinion, <b>is this false advertising</b> ?



We are using the bambino as the developpment platform for Smoothie2 and the Smoothieboard v2 hardware : 

[http://www.micromint.com/component/content/article/39-products/sbc/196-bambino200.html](http://www.micromint.com/component/content/article/39-products/sbc/196-bambino200.html)



On the product page, it says : « The Micromint Bambino 200 is the first multi-core SBC compatible with the .NET Gadgeteer and mbed frameworks. »

This is a big part of why we chose that chip and that development board : mbed is a HAL, if this board works with it, it's a huge amount of work we don't have to do.



However, it turns out their port of mBed to this chip is extremely weak : it only has very basic stuff, and nothing complex like USB, SDIO or Ethernet works.

Needless to say we were very dissapointed, and it's part of why v2 has taken so long ( it's taking up more and more speed now ).



So the question is : looking at the product page, do you think this is false advertising ?







**"Arthur Wolf"**

---
---
**Douglas Pearless** *November 01, 2016 22:08*

Knowing what I know now, I would have instead started with the LPC4337 based board from NXP. Hopefully they will have the drivers out in the next few weeks; very frustrating!!!!!


---
**Arthur Wolf** *November 01, 2016 22:13*

**+Douglas Pearless** That wouldn't have given us the missing code in the HAL though ...


---
**Anthony Bolgar** *November 01, 2016 22:18*

Not much help here I am afraid, I don't know enough about it to make an informed decision, but my gut feeling is they are verging on misleading, maybe legally OK, but bending the definitions of what it really is.


---
**Arthur Wolf** *November 01, 2016 22:37*

**+Michael Johnson** I emailed them and they seemed to understand the problem. Their excuse was a complete non-sequitur ( somebody else got more recognition from somebody else about something so we gave up on doing this work ( won't give more detail as email was private ) ). They didn't change the product description still ..


---
**Douglas Pearless** *November 01, 2016 22:39*

I have been working with their support people to get their code updated for the current mbed and released on their site; I am hopeful it is only a few weeks away, but then again….


---
**Arthur Wolf** *November 01, 2016 22:40*

**+Douglas Pearless** They mentioned they had more code than what was on their github for a long time but it just wasn't published. I really hope they publish it soon.


---
**Arthur Wolf** *November 01, 2016 23:01*

Yeah it's a bit sad that other people will fall in this trap ... and in the end it's really hurting everybody ( but their sales I guess ... )


---
**Stephen Baird** *November 01, 2016 23:43*

I'd say it's not false advertising in a legal sense, the board does support mbed frameworks... It just does so poorly. Now, if you talked to someone in their sales department and told them about your specific need for mbed support for things it didn't support and they sold it to you anyway there's a bunch of other legal claims that open up. But their site isn't itself false advertising. 



It's the worst kind of overstating capabilities, though, and a good way to lose customers and get a bad reputation. 


---
**Douglas Pearless** *November 02, 2016 20:03*

I am expecting to receive some embed support code for USB and Ethernet on 7th of November, they don't had the Micro-SD on their roadmap (yet)


---
**Arthur Wolf** *November 02, 2016 20:04*

**+Douglas Pearless** That's great news :)


---
**Marcos Gomes** *November 13, 2016 12:07*

I became a bit disappointing with the board. Although they advertise a bunch of capabilities they only made code available for a few of the simplest, and even these were not as complete as expected. An example of ir was the analog inputs to which they only support the shared pins and not the analog only. The good news is more people are using the LPC43xx and I believe will then contribute.


---
**Yale Zhang** *February 19, 2017 14:05*

What was the reasoning for choosing the LPC4330? Was it because it's dual core so that the Cortex M0 can be dedicated to real time tasks? Or was mbed the attraction? Does the NuttX RTOS you plan to switch to support all the peripherals? I expect a RTOS to be built on top of the manufacturer's HAL, so if that's incomplete, then they'd have to reimplement it :(



I know it's too late to change (maybe not too late since now that you're using a RTOS, having a separate core is less important), but in my opinion, ST Microelectronics'  STM32F4 & STM32F7 have a very complete and reliable HAL. I've been using them for an embedded computer vision project (using a cheap cell phone camera sensor) and all the peripherals (DVP camera, USB, ethernet, SDIO) work flawlessly. The documentation is a bit lacking, but there are example projects for every peripheral (even hardware JPEG encoding!) and if you're unsure what the HAL is doing, you can check the register documentation to find out.


---
**Arthur Wolf** *February 19, 2017 14:42*

**+Yale Zhang** The main reason we switched to the LPC4330 was the peripherals ( including the M0 core ), the power and the price. We do indeed plan to do the step generation on the M0 core. Doesn't have much to do with mBed.

NuttX didn't support the chip very well, but we paid one of the nuttx devs $10k ( $5k of which was paid by **+BEEVERYCREATIVE** ) to add a lot of things, including C++ support, SDIO, USB, Ethernet etc, all of which have now been merged into nuttx main for all to enjoy, not just us.

About STM, we hear a lot of good things, and a lot of bad things about them. It was considered this time ( and was mostly dismissed due to documentation and licensing issues ), it will be considered next time, it just didn't win this time around.


---
**Yale Zhang** *February 20, 2017 07:32*

That's kind of you guys to share it with everyone and cut down the development time. BTW, the main attraction of v2 for me are the 2.8A stepper drivers so I can extrude faster with a 0.8mm nozzle (currently using ANET A8 upgraded with E3D V6 hotend & bowden extruder).



"power and the price" - I see. So besides being dual core, the LPC 4330 is also unusual by not having any FLASH memory, saving $5. It seems the code will be executed in place (not copied to RAM) from the SD card ([https://www.digikey.com/en/articles/techzone/2014/feb/using-nand-flash-for-run-time-code](https://www.digikey.com/en/articles/techzone/2014/feb/using-nand-flash-for-run-time-code)). That's quite innovative.


---
**Arthur Wolf** *February 20, 2017 09:29*

**+Yale Zhang** If all you need is current, you can get 3.5A external drivers ( toshiba ) for about $10 a piece. They are extremely simple to wire, will work with your Smoothie v1, and cost much much less than buying a v2-pro.



About flash, we will be using a 8MB external flash chip, we won't be executing from the SD card.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/Ybx3LBpw6Hj) &mdash; content and formatting may not be reliable*
