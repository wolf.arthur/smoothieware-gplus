---
layout: post
title: "Ray Kholodovsky Arthur Wolf I'm back with comparing to my earlier setup and need some help"
date: November 01, 2016 01:10
category: "General discussion"
author: "Dushyant Ahuja"
---
**+Ray Kholodovsky** **+Arthur Wolf**

I'm back with comparing to my earlier setup and need some help.

First of all I love the prints I'm getting out of Smoothie.



However, I would like to understand the best practices and haven't reached the sweet spot. 



Before I moved to Smoothie, I was working with a RAMPS running Repetier with a raspberry pi zero running Octoprint. There is a relay connected to one of the pins on the raspberry pi zero that switches on / off the printer. I would upload the g-code to Octoprint using the web interface and print. Worked smoothly. 



I've been trying to replicate the same with the new board, but have run through problems. Have tried the following:



1. Tried using the esp8266 with laserweb as suggested by **+Peter van der Walt** - but somehow the esp8266 is not connecting to my home network. I had updated the firmware, but it did not work. Admittedly, I did not spend too much time debugging this. (I do have considerable experience using esp8266 and have setup my home automation system using a series of these)

2. Recently received a LAN8270 board and connected the board through an ethernet cable. Installed the webif interface:

a) None of the interfaces are working on Chrome - the page simply goes blank. Tried clearing my cache - worked once, and stopped working again

b) The interfaces work through Internet Explorer and I've printed some objects successfully using this. However, and this is a big one, if I close the page and then later reload the page (to view progress), the printer stops immediately. I'm not sure if this is due to the board, or the firmware, or something else altogether. 

c) gcode upload via the web interface is extremely slow

3. Installed Pronterface and connected to the board via the TCP interface and tried printing gcode files off the SD card (that I had copied earlier by connecting the board through USB). Works, but the workflow is not streamlined - requires me to connect the board via USB, copy the gcode file, disconnect the USB, connect through the TCP interface, print.

4. Tried to go back to Octoprint through my raspberry pi zero, unfortunately the USB port of the zero is not able to provide enough power - and hence, when the main power supply is off - the board keeps rebooting or hanging. 



Other than permanently connecting a laptop to the board to print, what do you suggest I should do. What do other members of the community do. 



Thanks in advance. 





**"Dushyant Ahuja"**

---
---
**Arthur Wolf** *November 01, 2016 09:35*

1. The esp8826 is very very new, I wouldn't try to use it unless you are ready to do a lot of trial and error

2. I know nothing about the LAN8270 board thing, I expect you need to contact your seller for help with that, it's not well documented yet as far as I know.

3. If the zero can't provide enough power, just put a usb hub between the pi and the board.



Cheers.


---
**Griffin Paquette** *November 01, 2016 11:08*

I tried to get a zero to run octoprint correctly but it boiled down to requiring a powered USB hub to do so. At that point I said screw it and just went out and bought a PI 3. Has worked reliably since. 


---
**Dushyant Ahuja** *November 01, 2016 11:11*

**+Arthur Wolf** is the web interface supposed to reboot the microcontroller if you refresh the page? Does that happen on the smoothieboard? What is the best way to upload gcode to the board?


---
**Arthur Wolf** *November 01, 2016 11:18*

**+Dushyant Ahuja** You can't do anything to the web interface while it's printing, the firmware doesn't support that. All ways to upload to the SD card are slow, that's due to the way the SD cards are interfaced ( SPI ), it's the same on 8-bit boards. Smoothie v2 won't have that limitation.


---
**Dushyant Ahuja** *November 01, 2016 11:20*

**+Arthur Wolf** thanks. When is the Smoothie v2 coming. :-)


---
**Arthur Wolf** *November 01, 2016 11:20*

**+Dushyant Ahuja** It depends on community contributors so we can't have a timeline.


---
**Tiago Vale** *November 01, 2016 16:22*

The only way to upload blazing fast is by mounting smoothie in raspberry, sometimes I have to upload again so smoothie lists the file, but still its fast. 


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2016 22:16*

**+Dushyant Ahuja** I sent you some messages on hangouts yesterday to discuss your questions. 



I have not yet had the time to play with Peter's new ESP websocket code nor with the new Smoothie web interface so afraid I don't have much to advise on.  It's highly experimental. 



Regarding the LAN8720, it doesn't sound like there is any hardware issue, but if you can elaborate on the exact circumstances that the interface does not load in Chrome I can attempt to replicate. Again, the new web interface is worth ruling out first. 



The ReMix generates its own 5v from 12/24v but also takes 5v from USB.  The Pi cannot provide enough juice.  If the 12/24 Vin is connected first this is not an issue.  If you would like to make this permanent change you can cut the VUSB trace on the bottom of the board under the USB jack area. Then it will only power up from the Vin. 


---
**Dushyant Ahuja** *November 02, 2016 02:57*

**+Ray Kholodovsky** that's an idea. Looks like octoprint is still my best bet. 


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/bg3Ssrk8w2C) &mdash; content and formatting may not be reliable*
