---
layout: post
title: "Smoothieboard v1.1 coming in a few weeks"
date: November 01, 2016 21:04
category: "General discussion"
author: "Arthur Wolf"
---
Smoothieboard v1.1 coming in a few weeks.



[http://gerblook.org/pcb/Yh2czpp7JQGb34f7sfp4bg#front](http://gerblook.org/pcb/Yh2czpp7JQGb34f7sfp4bg#front)



TL;DR : We are getting 1/32 allegro drivers, and diode protections for small mosfets running fans.



Changelog : 

Smoothieboard v1.1 (2016/10)

- upgraded stepper drivers to A5984 drivers with 32x microstepping

- added a solder jumper (JP36) for MS3 on new A5984 drivers

- changed the current limiting resistors (R78,R65-R68,R84-R85) for VBB powered LEDs to 8.2K

- added an electrolytic capacitor (C84) to the 5V power system

- moved the electrolytic capicitor (C63) for the 5x mosfet set and increased its footprint size

- added flyback diodes (D7-D9) to small mosfet outputs to protect from inductive loads like small fans





**"Arthur Wolf"**

---
---
**Jesper Krog Poulsen** *November 02, 2016 07:25*

Looks awesome. I might want to wait until 2.0 to upgrade, though :)



You mention flyback diodes, which I assume 1.0 doesn't have.  If I wanted to add those myself, should they be placed close to the mosfet, like inserted into the connector together with the wires, or should I solder them as  close to the fan as possible? Or does it matter at all?


---
**Arthur Wolf** *November 02, 2016 07:39*

**+Jesper Krog Poulsen** There are pins right next to each mosfet connector to add those, see [http://smoothieware.org/3d-printer-guide#toc21](http://smoothieware.org/3d-printer-guide#toc21)


---
**Jesper Krog Poulsen** *November 02, 2016 08:10*

Ah, didn't know that. Thanks


---
**Dieter Kedrowitsch** *November 08, 2016 13:24*

Perhaps a bit late to make a change but I noticed the thermal pads for the M1-M4 stepper drivers have thermal reliefs but M5 does not.  Was the intentional or an oversight?  Won't this have a negative impact on the heat dissipation capabilities of the M1-M4 drivers since it essentially insulates heat conduction to the large upper foil?


---
**Arthur Wolf** *November 08, 2016 13:27*

They all have very good heatsinking, this has been very well tested.


---
**Dieter Kedrowitsch** *November 08, 2016 13:40*

**+Arthur Wolf**  Understood, I just wanted to bring the inconstancy to your attention since it seems odd to not have a uniform thermal design for all 5 drivers.  So it looked like an "oops" to me that might have been overlooked.

![missing image](https://lh3.googleusercontent.com/TmLj2NmzbzBZ-3MVaRFJByK8TvU6q7qgq_Vyj-gaolPCUBYjUNGRUuuMExtxFOaHxAEY3BiZR6yMg0HkzBrz7wpD00sGv3JoPwE=s0)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/cp75kbRkcqa) &mdash; content and formatting may not be reliable*
