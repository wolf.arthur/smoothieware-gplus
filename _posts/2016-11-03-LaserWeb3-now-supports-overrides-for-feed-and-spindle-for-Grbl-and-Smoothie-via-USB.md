---
layout: post
title: "LaserWeb3 now supports overrides for feed and spindle for Grbl and Smoothie (via USB)"
date: November 03, 2016 13:50
category: "General discussion"
author: "Claudio Prezzi"
---
LaserWeb3 now supports overrides for feed and spindle for Grbl and Smoothie (via USB). Please test: [https://github.com/openhardwarecoza/LaserWeb3/tree/overrides](https://github.com/openhardwarecoza/LaserWeb3/tree/overrides).

![missing image](https://lh3.googleusercontent.com/-a8dWgoxw-Zc/WBtAtGBLXRI/AAAAAAAAMUw/GRjNTwL0Qlk_KO9zwRkUL3Dp4D2LIcFNgCJoC/s0/DRO%25252BOverrides.jpg)



**"Claudio Prezzi"**

---
---
**Steve Anken** *November 03, 2016 15:22*

Any plan for scaling per axis like Mach3? I assume you are replacing the spindle and feedrate gcode before sending it.. if so scaling would be similar, no?




---
**Claudio Prezzi** *November 03, 2016 16:09*

This feature uses the override function of the firmware, which reacts immediatly instead of being delayed by the queue. 

Scaling would have to be done in the cam module, which is a complete different story.


---
**Steve Anken** *November 03, 2016 18:18*

Makes, sense, Thank you.




---
*Imported from [Google+](https://plus.google.com/+ClaudioPrezzi/posts/hqrpcBehZdD) &mdash; content and formatting may not be reliable*
