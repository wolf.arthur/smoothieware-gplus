---
layout: post
title: "I've been digging through the Smoothieboard's eagle files and BOM as I am working on a smoothieware compatible board"
date: November 05, 2016 13:50
category: "General discussion"
author: "Griffin Paquette"
---
I've been digging through the Smoothieboard's eagle files and BOM as I am working on a smoothieware compatible board. I am using drivers that do not use direct SPI control/config but rather through digi pots much like the Allegro chips on the Smoothieboard, and noticed that a 50k MCP4451 controls the IREF from Alpha through Delta drivers while a 10k version is used for Epsilon. I was curious as for the reason behind this, and ideally would like to use two of the 50k versions on my board too keep all drivers consistent. Would this need any significant firmware changes over the 10k version?





**"Griffin Paquette"**

---
---
**Arthur Wolf** *November 05, 2016 14:00*

That's linked to penuries of the chips. You can use whatever you want.


---
*Imported from [Google+](https://plus.google.com/111302122377301084540/posts/DVuYMrAjFoJ) &mdash; content and formatting may not be reliable*
