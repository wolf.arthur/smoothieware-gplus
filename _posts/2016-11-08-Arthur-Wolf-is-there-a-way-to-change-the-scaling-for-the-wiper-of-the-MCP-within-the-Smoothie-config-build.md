---
layout: post
title: "Arthur Wolf is there a way to change the scaling for the wiper of the MCP within the Smoothie config/build?"
date: November 08, 2016 22:42
category: "General discussion"
author: "Griffin Paquette"
---
**+Arthur Wolf** is there a way to change the scaling for the wiper of the MCP within the Smoothie config/build? I am not sure as to what reference current scaling is used with the 4982, but I don't think that it is the same as my drivers... therefore I would like to be able to adjust that scaling so that the current defined by the user within the config.txt is consistent with my drivers.

I see the digipot_factor is a value of 113.5, but I don't understand what that value is with respect to. Is this the factor that I multiply the desired driver output current by to set what the wiper code value will be (0-256)? 





**"Griffin Paquette"**

---
---
**Arthur Wolf** *November 08, 2016 22:57*

Hope this helps : [https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/utils/currentcontrol/mcp4451.h](https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/utils/currentcontrol/mcp4451.h)

[github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/utils/currentcontrol/mcp4451.h)


---
**Griffin Paquette** *November 09, 2016 02:33*

**+Arthur Wolf** quick readthrough cleared it right up. Thank you!


---
*Imported from [Google+](https://plus.google.com/111302122377301084540/posts/dLUjmRiW9ST) &mdash; content and formatting may not be reliable*
