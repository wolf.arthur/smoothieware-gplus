---
layout: post
title: "Originally shared by Sbastien Mischler (skarab) New Rasterizer library in progress..."
date: November 08, 2016 20:44
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Sébastien Mischler (skarab)</b>



New Rasterizer library in progress... Needs testing before integrating in LaserWeb ! [https://github.com/lautr3k/lw.Rasterizer.js](https://github.com/lautr3k/lw.Rasterizer.js)

#LaserWeb #Raster





**"Arthur Wolf"**

---
---
**Sébastien Mischler (skarab)** *November 08, 2016 21:55*

Thanks for sharing :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/845CmW2sNUC) &mdash; content and formatting may not be reliable*
