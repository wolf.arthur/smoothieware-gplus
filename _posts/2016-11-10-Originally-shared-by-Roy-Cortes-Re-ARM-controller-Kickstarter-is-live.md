---
layout: post
title: "Originally shared by Roy Cortes Re-ARM controller Kickstarter is live"
date: November 10, 2016 22:06
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Roy Cortes</b>



Re-ARM controller Kickstarter is live. Please help me spread the word.



Thanks.



[https://www.kickstarter.com/projects/1245051645/re-arm-for-ramps-simple-32-bit-upgrade](https://www.kickstarter.com/projects/1245051645/re-arm-for-ramps-simple-32-bit-upgrade)









**"Arthur Wolf"**

---
---
**Samer Najia** *November 11, 2016 04:04*

As much as I like this isn't it simpler, albeit NOT cheaper to use a Smoothie or Azteeg or C3D remix?  I am inclined to buy a couple but I think the problem is both the RAMPS and the Arduino not just one or the other, especially given the proliferation of junk boards.


---
**Josh Rhodes** *November 11, 2016 05:59*

This would make a perfect upgrade path for the two shapeokos we have at **+Mobile Makerspace**​, No rewiring!!


---
**René Jurack** *November 11, 2016 22:41*

backed!


---
**Soup3y gnome** *November 13, 2016 01:41*

Love it, backed it, I hope it all works the way it should.




---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/KNGWDCeQE2t) &mdash; content and formatting may not be reliable*
