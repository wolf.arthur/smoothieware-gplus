---
layout: post
title: "MKS MINI12864 panel on my smoothieware compatible board"
date: November 11, 2016 09:42
category: "General discussion"
author: "Bray pp"
---
MKS MINI12864 panel on my smoothieware compatible board. LCD on panel is viki2 compatible but pinout on ext1 and ext2 is strange. ;) 

Both connectors are mirrored according to "standard" GLCD, LCD and SD shares SPI pins. But it works...

![missing image](https://lh3.googleusercontent.com/-for4JBp_oVY/WCWSloBF6gI/AAAAAAAAPkQ/kBXRR4HuRKoKfmdlSNJNxZeWl7c5mShAQCJoC/s0/DSC_0770.JPG)



**"Bray pp"**

---
---
**Arthur Wolf** *November 11, 2016 09:49*

Is that board open-source ?


---
**Bray pp** *November 11, 2016 10:33*

Yes, it's open-source. It's based on smoothieboard.


---
**Arthur Wolf** *November 11, 2016 10:34*

Is the source somewhere ?


---
**Bray pp** *November 11, 2016 10:45*

[http://in-lights.com/SMINTx6/](http://in-lights.com/SMINTx6/)

Site is still "under construction" ;) But everything about board will be there.


---
**Arthur Wolf** *November 11, 2016 10:47*

And what will the license be ?


---
**Bray pp** *November 11, 2016 11:43*

[http://unlicense.org/](http://unlicense.org/) ;) 

Seriously I really don't know which HW license  suits best...

Cern OSH, GPL,...I am programmer not a lawyer ;) But for sure this is a thing to discus. 

I can provide schematics, PCB layout,  even gerbers,...

[unlicense.org - Unlicense.org » 
        Unlicense Yourself: Set Your Code Free](http://unlicense.org/)


---
**Arthur Wolf** *November 11, 2016 11:43*

Well Smoothieboard is CERN OHL so I think it'd make sense if this were too.


---
**Griffin Paquette** *November 25, 2016 20:53*

Are you running the MCP4451 digipots like on the smoothieboard?


---
**Bray pp** *November 25, 2016 21:29*

Yes, you can set current in config file.


---
**Griffin Paquette** *November 25, 2016 22:02*

Any eta when the source files will be up? Looks like a great little board!


---
**cory brown** *February 14, 2017 07:11*

**+Bray pp** I have been trying to get the MKS MINI12864 to work with a smoothie board for a while. Could you share the pin configuration and how you set up your config,txt file? That would be supper awesome. 


---
*Imported from [Google+](https://plus.google.com/105651585221064903278/posts/T9dgNjNAWtK) &mdash; content and formatting may not be reliable*
