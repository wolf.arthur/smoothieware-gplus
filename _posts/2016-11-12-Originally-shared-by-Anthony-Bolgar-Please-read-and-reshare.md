---
layout: post
title: "Originally shared by Anthony Bolgar Please read and reshare!!!"
date: November 12, 2016 09:15
category: "General discussion"
author: "Anthony Bolgar"
---
<b>Originally shared by Anthony Bolgar</b>



Please read and reshare!!!



You may have noticed a new name on the development team for Laserweb. **+Todd Fleming** has joined up and is doing some incredible work on the next version, LaserWeb4. He brings some valuable experience to the team, and will be adding his [jscut.org](http://jscut.org) tools to the project. This will enhance the project, especially the CNC CAM functions.  What we need is to get Todd a laser of his own to use in development. I am sure with the support of the community, we can get him the tools he requires to take LaserWeb to the next level. Donations can be sent to [https://www.paypal.me/tbfleming](https://www.paypal.me/tbfleming) . Please help us set him up, everyone will be a winner once Todd can start testing his work real time. As well as financial donations, we could use some Smoothiebased hardware to modify the laser, and of course any safety equipment such as goggles would be very welcome. Any and all donations are welcome, and please reshare within your circles. Thank you in advance.





**"Anthony Bolgar"**

---
---
**Mark Leino** *November 13, 2016 05:16*

$ donated! Excited to see laserweb4!


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/BwbQiDKe2NH) &mdash; content and formatting may not be reliable*
