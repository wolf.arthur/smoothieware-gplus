---
layout: post
title: "I'm in the process of evaluating a Low cost CNC Project, I'm currently looking at the Solsylva 25x25 (I'm open to suggestion)"
date: November 13, 2016 01:35
category: "General discussion"
author: "S\u00e9bastien Plante"
---
I'm in the process of evaluating a Low cost CNC Project, I'm currently looking at the Solsylva 25x25 (I'm open to suggestion).



For the electronics, the only controler that come ti my mind is the Smoothieboard (got one for my 3D Delta) with external HY-DIV268N-5A stepper driver to use Nema23 270 oz-in+ stepper (suggestions?). What software I could use to slice the 3D and make it work? bCNC seems okay running GRBL on the Smoothie.



Thanks! 





**"S\u00e9bastien Plante"**

---
---
**Griffin Paquette** *November 13, 2016 03:06*

If you are going to use external drivers I would check out **+Ray Kholodovsky** Remix. Smoothie and has plug able drivers


---
**Arthur Wolf** *November 13, 2016 11:25*

Hey !



So, GRBL can not run on Smoothieboard, the only firmware that runs on the Smoothieboard is the Smoothieware firmware. It's better than GRBL, and compatible anyway.

You use bCNC to control the machine from your computer, and send gcode files to the Smoothieboard.

But to generate Gcode files, you need to use a CAM package, like fusion360, cambam, or pycam.

Is that clearer ?


---
**Sébastien Plante** *November 13, 2016 12:58*

**+Griffin Paquette**​ Thanks, I'll look into this.



**+Arthur Wolf**​ With one of those CAM software, can I play the file directly on the Smoothie (web interface or whatever) ?


---
**Arthur Wolf** *November 13, 2016 12:59*

**+Sébastien Plante** Sure. bCNC is neat for positioning the bit at the corner of the part etc though.


---
**Sébastien Plante** *November 13, 2016 16:57*

Amazing, thanks! <b>- continuing evaluation -</b>


---
*Imported from [Google+](https://plus.google.com/+excessnet/posts/M63N7oaDARB) &mdash; content and formatting may not be reliable*
