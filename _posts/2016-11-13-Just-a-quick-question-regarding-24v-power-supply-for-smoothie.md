---
layout: post
title: "Just a quick question regarding 24v power supply for smoothie"
date: November 13, 2016 19:11
category: "General discussion"
author: "Alex Krause"
---
Just a quick question regarding 24v power supply for smoothie. What amperage rating is recommended ? It is not specified in this link [http://smoothieware.org/main-power-input](http://smoothieware.org/main-power-input)





**"Alex Krause"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2016 19:14*

It is not. Your 24v is entirely dependent on how many motors/ mosfets/ other stuff you are going to drive.  Roughly speaking, at 12v we use 1-2 amps per motor, 3-4 amps for a extruder heater cartridge and the SB can handle 8-11 amps for a small heatbed. For 24v you can halve those values. And add a margin of at least 20% 


---
**Maxime Favre** *November 13, 2016 19:14*

Smoothie itself draw not so much current. What other elements do you want to power with the +24V ? Motors ? hotends ? Bed ? Coffee machine ?


---
**Alex Krause** *November 13, 2016 19:18*

I'm just wondering what I should recommend people to purchase along side a smoothie board to ensure that regardless of their setup, If the internal stepper drivers are used and all other 24v components are in operation they will not be pulling more load than the power supply can handle. 


---
**Arthur Wolf** *November 13, 2016 19:22*

Yep, it depends on your bed's rating, hotend rating, number of motors, rating of motors etc, you need to do the math. Remember the power inputs have a 12.5A limit. If you read the whole guide you should have a complete view of how to figure this out.


---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2016 19:24*

**+Alex Krause** I think we can qualify that a bit.

For a basic CNC a 24v 10A should work.  That'll comfortably get you a few motors going.

For a basic printer I'd say a 24v 20a.  That should get you a set of motors, hotend, and basic bed (like 5 amps at 24v)  Those are very comfortable values for me.


---
**Jeff DeMaagd** *November 13, 2016 19:50*

For me, a 360W PSU easily handles a 225W bed, two hotends / extruders, totalling 5 motors, an LCD, fans, lighting and everything else that I might be forgetting.


---
**Eric Lien** *November 13, 2016 20:23*

And if you want a beefier DC bed **+Ray Kholodovsky**​ has a sweet remote MOSFET board to handle greater loads. But if you want a really high output bed I recommend going to a mains AC powered bed stitching via a good quality SSR. Then the DC power supply can be much lower output since the bed is the greatest load.


---
**Don Kleinschnitz Jr.** *November 13, 2016 22:50*

I did this one for my K40 conversion. 24VDC @ 5A. $16.67. 



[https://www.amazon.com/gp/product/B018TG7I4W/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B018TG7I4W/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)



But I am planning to power my lift table and potentially a DC water pump. I eventually want to get my pump out of the water and be able to control the flow.



BTW: if I had it to do over again I would have used DIN rails. 

[amazon.com - Amazon.com: DMiotech® DC 24V 5A 120W Power Supply Switching Converter for LED Strip: Home Audio & Theater](https://www.amazon.com/gp/product/B018TG7I4W/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Joe Spanier** *November 13, 2016 23:34*

Ac beds ftw


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/CNEeittt3C8) &mdash; content and formatting may not be reliable*
