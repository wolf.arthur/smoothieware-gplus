---
layout: post
title: "Hej everybody I just started with the whole smoothieware thing and have to say it's quite impressive so far"
date: November 14, 2016 20:23
category: "General discussion"
author: "Sven Eric Nielsen"
---
Hej everybody 



I just started with the whole smoothieware thing and have to say it's quite impressive so far. 



I spent a lot of time studying the documentation but could find a answer for my question. So maybe someone in this community can help me somehow. 



Is something like this actually possible with smoothieware :



I want to use one stepper for two hotends. As soon as I switch from "extruder" 1 to "extruder" 2 in the model, I need to activate a servo first and then I want to run the same stepper from "extruder" 1 but in the other direction. 



Any ideas? 





**"Sven Eric Nielsen"**

---
---
**Wolfmanjm** *November 14, 2016 21:30*

i do not think this use case is supported. you may have to issue gcodes to achieve it, and setup a switch to control the servo. not sure how you will be able to reverse the extruder though. 


---
**Arthur Wolf** *November 14, 2016 21:46*

You could have a bit of electronics that would reverse the extruder, it's not much, and if you don't know how to do it somebody at a local fablab could help you.

For the rest, it's just a matter of setting up a switch and teaching your slicer to send new gcodes, as **+Wolfmanjm** said.

It's possible, but it's a bit of work.


---
**Sven Eric Nielsen** *November 15, 2016 12:28*

Thanks for the quick reply! 

I've already seen the switch modules and I have a lot of ideas how I could use this smart thing. 



I just had the hope that there is a simpler method because it's a lot of work to solve this with a separate electronics <i>compared</i> to 2-3 lines of code. Based on my experience there is not more needed to let the stepper turn in the opposite direction. 



But your feedback means basically that it's in general possible to use one stepper motor for 2 hotends? And especially without a second stepper driver? Because it doesn't make sense to use 2 stepper driver for one stepper motor. ;) ﻿


---
**Wolfmanjm** *November 15, 2016 20:04*

yes just define one extruder in config and two hotends..


---
*Imported from [Google+](https://plus.google.com/+SvenEricNielsen/posts/4JgzzfrXiZQ) &mdash; content and formatting may not be reliable*
