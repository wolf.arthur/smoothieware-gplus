---
layout: post
title: "Originally shared by HACKADAY When it comes to 3D printer controllers, there are two main schools of thought"
date: November 14, 2016 20:13
category: "General discussion"
author: "Alex Krause"
---
<b>Originally shared by HACKADAY</b>



When it comes to 3D printer controllers, there are two main schools of thought. The first group is RAMPS or RAMBo which are respectively a 3D printer controller ‘shield’ for the Arduino Mega and a stand-alone controller board. These boards have been the…





**"Alex Krause"**

---
---
**Gary Tolley - Grogyan** *November 14, 2016 23:24*

Already backed it


---
**K** *November 15, 2016 03:41*

Backed! I'm looking forward to converting my D-Bot over to Smoothie.


---
**Jim Christiansen** *November 18, 2016 21:24*

Backed x3!


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/gVVhbvBfQqu) &mdash; content and formatting may not be reliable*
