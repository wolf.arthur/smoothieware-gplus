---
layout: post
title: "I have started having an issue tonight where my tube stops firing in the middle of my job"
date: November 15, 2016 02:12
category: "General discussion"
author: "K"
---
I have started having an issue tonight where my tube stops firing in the middle of my job. I can hold the test button down and it fires as it cuts, but the board (X5) itself isn't sending the signal? The LCD says 100% (I'm using a pot), but there isn't anything happening. I've upgraded LaserWeb, and I'm using the CNC firmware. The job I'm running starts out vector engraving at 25mm/s, and roughly 4mA. When it gets to the cut I have it set at 10mm/s and 9mA. It's during the cut that the tube stops firing. I don't think it's the water being too warm, as I just tried again after the machine was off for 6 or so hours. I've double checked my connections (I'm using a K40 with a LO power supply), and everything seems snug and connected. The tube doesn't have any cracks or bubbles, and the water seems to be flowing properly. Any thoughts?



Edit: Other strange behavior I've noticed yesterday and today. For some reason right after running a job LW decided that it wanted to home to 0,0, instead of 250,0 as it should. I don't know if that's related, but I thought I'd throw it in. The problem corrected itself after rebooting my computer a couple of times.





**"K"**

---
---
**K** *November 15, 2016 03:33*

This is puzzling. On a whim I decided to run a cut job and tell LW 80% power instead of 100%. It now runs as it should. I noticed on the 100% job I tried right before that the laser would shoot on and off usually around corners, but the ammeter showed a power level of only 2ish mA.


---
**K** *November 15, 2016 03:38*

I tried 99%, and it works great. I wonder now if this is a LW issue.


---
**Arthur Wolf** *November 15, 2016 10:40*

This is really weird. Any clues looking at the gcode file and identifying what line it stops working at ?


---
**K** *November 15, 2016 14:18*

It stopped firing right when the power hits 100%. I have since rebooted (three times, actually), and as of last night it decided to work again. I'm not really sure what's up. It's one of a few issues I've had over the last few days.


---
**Arthur Wolf** *November 15, 2016 15:26*

Wiring issue ? Wrong connector ? Faulty PSU ?


---
**K** *November 15, 2016 15:29*

**+Arthur Wolf** I'm wondering if it's the PSU as I'm also having an issue with the laser firing when I move the gantry when the X5 is off. As for wiring, I feel like maybe not since I've double checked it and it's all wired as it should be and has been. 


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/UVadUVPzB29) &mdash; content and formatting may not be reliable*
