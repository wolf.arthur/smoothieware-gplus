---
layout: post
title: "Hello. How likely is it that a Smoothieware system could some day drive a Plasma CNC system?"
date: November 18, 2016 01:26
category: "General discussion"
author: "Jim Christiansen"
---
Hello.  How likely is it that a Smoothieware system could some day drive a Plasma CNC system?  Thc wouldn't be needed for smaller tables but the motors are 48v...





**"Jim Christiansen"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 18, 2016 01:38*

Typically you'll use external drivers to run steppers at 48v.  Here's a pic of my board the Cohesion3D ReMix demonstrating some of the capabilities for a CNC application, including Signal Breakouts to send the STEP DIR EN GND to the external drivers, buffered endstops allowing you to use inductive sensors at 12/24v directly, a header for external kill button, and flyback diodes allowing you to run a wide range of peripherals including pumps and valves (for dispensing lubricant or clearing chips).

The factory is finishing up the first 100 unit run right now and I hope to have them available and for sale next week, if you're interested.

Hope this helps,

Ray

![missing image](https://lh3.googleusercontent.com/TOcTHT-ENBfvK2IavmZl95DE4qmCFICvWbi3X53vbrzQxvZcQLVFAU-BZRRSPuvZ571lTqyloW7DFEc=s0)


---
**Jim Christiansen** *November 18, 2016 02:00*

My floating Z refs with a micro switch.  I only understood about 1/2 of what you've explained :-) !  I'll look into your board.  If you have any more information please share!  Thank you.


---
**Jim Christiansen** *November 18, 2016 02:08*

So I wonder if it can talk to my Hyperthem?  For arc established, peirce and continue to cut path? My gcode is generated in Mach3.  I guess I'd need different cam software that can output to Smoothie or would the gcode be compliant as is? So many questions!


---
**Ray Kholodovsky (Cohesion3D)** *November 18, 2016 03:09*

No worries, just demonstrating that we're equipped to drive quite a few different peripherals that people may need.



That's a great question.  It depends on what your gcode looks like.  G0, G1, G2, and G3 commands are fair game. What controls whether the plasma is on (ie cutting) or off (travel move)? For CNC and Laser this is usually an M3 (tool on) and M5 (tool off).



You would need a smoothie compatible Gcode sender, for this a lot of people use LaserWeb/ CNCWeb (and it can make the toolpaths for you too!)  or Pronterface.  

A full list can be found here: [http://smoothieware.org/software](http://smoothieware.org/software)



There's some info floating around about the new board and the new site will be up soon which will tell you everything you need to know.


---
**Arthur Wolf** *November 18, 2016 08:33*

Yep several people have already used Smoothie for plasma it shouldn't be a problem.


---
**Jérémie Tarot** *November 18, 2016 12:02*

**+Peter van der Walt** is there a donation address for this ? 


---
**Jérémie Tarot** *November 18, 2016 14:00*

**+Peter van der Walt** tipped again :) 


---
**Jim Christiansen** *November 18, 2016 15:37*

Yes, Jim Colt is always helpful.  He has helped me a few times.  My table is entirely homemade.  I'm using CandCNC electronics right now, but it is so expensive when things go down and everything is closed... Here's my setup:


{% include youtubePlayer.html id="RjJLk1PleN8" %}
[https://www.youtube.com/watch?v=RjJLk1PleN8](https://www.youtube.com/watch?v=RjJLk1PleN8)

My table is perfectly level and can accomdate sheet sizes 4x8 ft.  I've just used FreeCAD for the first time for a design to plasma cut.  I usually teach and use Creo Parametrics, but I'll be installing FreeCAD under Linux in my lab at school to give students a taste of something on the wild side!  ;-)


{% include youtubePlayer.html id="RjJLk1PleN8" %}
[youtube.com - YouTube](https://www.youtube.com/watch?v=RjJLk1PleN8)


---
**Brandon Satterfield** *November 18, 2016 16:45*

I am running a hypertherm on an R7 CNC with a GRBL shield. It would work the same with a hacked smoothie. Or with Rays board and external steppers. 



I can say it is possible to run smoothieware or TinyG to run a plasma CNC. Both are amazing boards. 



Adaptable THCs that can pick up voltage directly from your hypertherm terminals are available. 



Tool offset code to interrupt the current flow per hole or slot does present an issue. 



If your plasma torch is at height Z .1 in one corner of the bed and you cut a few lines and holes, then your next operation is at the opposite corner of the bed, you need a new Z. 



Ideally your gcode would set height for one corner then set height in the other corner before processing. 



The gcode is easily created to do this but interrupting the flowing gcode and resetting Z height has been an issue for me. 



Your Mach 3 controller should be able to do this though. I guess depending on what BOB you have. 



I have been deep into experimenting with all boards that have external drivers, including Mach 3/4 controllers. 



I'll drop my knowledge when I have everything tuned in. Right now though, it works! 




---
**Jim Christiansen** *November 18, 2016 17:58*

Yes, referencing Z often (before each new pierce) mostly alleviates the need for THC except for large area jobs.  A steady TH with THC is a secondary need for most small shop  jobs.  The down side to fouling up pierce height, losing arc or dragging a tip is not only a failed cut job, but damaged $$$ consumables (or more costly parts) and wasted steel that is way too expensive to buy twice, transport, load or to move to the scrap pile.  I wish I had more experience with electronics.  I'd love to see a reliable open system for plasma work.  I salute you guys.


---
**Brandon Satterfield** *November 18, 2016 20:46*

**+Peter van der Walt** I'll have to check it out later, network I'm on now has GitHub blocked.. 


---
**Jim Christiansen** *November 18, 2016 21:11*

I'm retiring next year if the world doesn't go into melt-down so I'm trying to be fiscally responsible.  Taking small steps, and carefully, I've just bought 3 Re-ARM boards that can be used in my school.  I'll learn how you cool guys are doing the 32 bit ARM Smoothieware Tango, then go from there to plasma, smoke and high voltage!


---
**Brandon Satterfield** *November 19, 2016 14:47*

**+Peter van der Walt** Yes I do like your approach. Also note that a proper voltage divider would remove the need for current reading (which at 30 and 40 amps is a hard signal for our little boards to read) you can get a stepped down voltage reading that pretty safe and down to a level our little boards can read... 

My approach thus far has been more for tool offsets, but this has been unsuccessful in less breaking apart every piece of code for every hole. I.E.

( do a Z probe cycle )

N0004 G31 Z-100 F100 (Or in GRBL G38.3)

( set this as Z=0 )

N0005 G92 Z0

( retract 5mm until tip clears plate )

N0006 G0 Z5.0

( set this as Z=0 )

N0007 G92 Z0

( Profile1 )

N0008 G0 Z1.5

( pierce height )

N0009 G0 Z5

( torch on )

N0010 M3

( plunge to cuting depth )

N0011 G1 Z1.5 F1000.0

This needs to be in the code and resets the Z per hole. There are better tricks than using a limit switch, a piece of copper bent and touching tip and a ground clamp on the material reduces extra load on the system. This does need to happen on a board that can handle/ filter big spikes. Not all I have tested can handle it. 



**+Jim Christiansen** In less you are building 2" x 2" squares, it might surprise you how much you need THC. Even travelling 3" in something like 18g the metal can react and move almost and inch in the Z direction. 



The above needs to be added to every action and THC is required... This is all just my opinion.


---
**Jim Christiansen** *November 19, 2016 15:23*

Hello **+Brandon Satterfield** .  Oh, absolutely required for the thin stuff, sorry.  It's difficult for me to cover all of the other conditions talking about THC... ( I have a bad habit in discussions of trying to communicate thoughts that usually requires 20 words in 5 words.  my bad).  I only cut 10 Ga and thicker.  I run my floating Z offset .2 mm higher than book and since running a smidgen higher have never dragged a tip...   My table is always super clean and level.  BUT ;-)  I have lost my plasma touch-off arc occasionally very likely from being too high.  When I lower the tip to all book settings I never have a bad start.  My Z right now is a rack-geared setup and my motors don't have the resolution to maintain a working THC.  I have purchased at considerable expense and new lead-screw driven floating Z that will easily be capable of reliable THC resolution.  I've just gotta find time to install it.  If I did have THC I would be cutting more artistic art-work type jobs for my family!


---
**Brandon Satterfield** *November 19, 2016 15:30*

**+Jim Christiansen** I completely understand. I am fairly new at this adventure and have a product in mind. In my quest like this I often find every scenario I can. I was cutting 1/8" stuff without a THC and without issue. I was feeling pretty proud of myself. Then I threw some 18g up there for another project and found that the system was inadequate. I completely agree with you though, thick stuff flat bed, no worries. :-)


---
**Aaron Pence** *April 08, 2017 23:41*

wish it had an ok to move output. it takes away control of z  axis in thc then returns it when not in use.

[eagleplasma.com - Eagle Plasma &#x7c; Proma THC SD](http://eagleplasma.com/proma-thc-sd.html#main)


---
**Aaron Pence** *April 09, 2017 00:07*

posted there first to see if there had been progress got my smoothie board friday I like the way the config file is on the sd card would make switching table from router to plasma painless. 


---
**Aaron Pence** *April 09, 2017 02:59*


{% include youtubePlayer.html id="Nif3uZHhUF0" %}
[youtube.com - CNC Torch Height Controller (THC) Interface Overview](https://www.youtube.com/watch?v=Nif3uZHhUF0)


---
**Aaron Pence** *April 09, 2017 03:07*

the torchmate at work has a hypertherm thc 

the pierce delay is controlled in it.

so the only thing smoothie would have to do is 

have a torch on signal M3 or M50 and then wait to move.

so only thing were really missing is a wait to move input.


---
**Aaron Pence** *April 09, 2017 04:22*

I think the pause button could be used pin 2.12 or 2.11 

use the m3 to start plasma but it also triggers the pause button.

use the thc to release the pause button.


---
**Brandon Satterfield** *April 19, 2017 14:05*

**+Aaron Pence** with a little handy work on the driver you can make the eagle plasma work great. You need something with removable stepper sticks. A GRBL board works fine. 



Pull the STP/DIR pins out from the top of the stepper stick and out of the bottom of the board. 



Output the pins from the board to the eagle product, input from the eagle product to the stepper stick. Done. 



Works well. No Gcode or Mcode mods required. Im sure there is a way to get a smoothie to do this too, and we are working with another board group to make these pins accessible for this and another control interface we need. 


---
**Aaron Pence** *February 13, 2018 16:46*

went with a vhc-300 THC with a 24vdc torch lifter off amazon mounted on z next to spindle. THC has pierce delay, arc voltage height control, pierce height, and ok to move all built in. 



planing to us switched output G1- G3 to turn plasma on.

not sure if this is best. could use custom m code.



I'm worried the pause input wont be executed fast enough in smoothie. 


---
**Aaron Pence** *February 24, 2018 23:36*

thc I'm using from aliexpress $250 with divider board.

 the divider board for my cutmaster would almost cost more.



![missing image](https://lh3.googleusercontent.com/PKvlb-h8-9WCY2rdyaWUNBFiE4xtcsxjCprnoq1f_rNYO30_2Tq2P77MCta8DjVWb2SENgnb33Dktd9htLcDRg--OZNEhRpdBk4=s0)


---
**Aaron Pence** *February 24, 2018 23:38*

![missing image](https://lh3.googleusercontent.com/OyrQYPQ8mlNT26WQRECr8fgZG5QJom8H2rGF_kBGabdXsiaMidC4DomQuaI5UPpVb2UQQ4JlQCWmQVqFp3foGWUgXsYW7G5av4Y=s0)


---
**Aaron Pence** *February 24, 2018 23:39*

![missing image](https://lh3.googleusercontent.com/E4SOm--7BAmCgzvGDpu6Me4tR7vTl0ZzOwNx5Sx5SIx3QQvN2Bi676tJwmO9gxfAIkEoMMxOzHUuziHvbjzg9aJPIlehGie_4cQ=s0)


---
**Aaron Pence** *February 26, 2018 03:12*


{% include youtubePlayer.html id="l0A7RUBdORs" %}
[youtube.com - Smoothie board plasma](https://youtu.be/l0A7RUBdORs)


---
*Imported from [Google+](https://plus.google.com/+JimChristiansen/posts/SXnuvCq3pE1) &mdash; content and formatting may not be reliable*
