---
layout: post
title: "Im running a firmware from mid 2015"
date: November 19, 2016 22:16
category: "General discussion"
author: "Guy Bailey"
---
Im running a firmware from mid 2015. Is there any compelling reason to get the lastest stable build and do I need to update my config? 







**"Guy Bailey"**

---
---
**Anthony Bolgar** *November 19, 2016 22:21*

Yup, new features have been added, better stability of the firmware, and compatibility with newer open source software like LaserWeb. Also makes it easier for us to troubleshoot if you ever need help. It only takes a minute or two to upgrade, so I feel that it is in your best interests to upgrade, you can always go back to the previous version if you save the firmware.bin file and your config.txt file before upgrading.


---
**Guy Bailey** *November 19, 2016 22:23*

ok. Thanks. I am looking at the latest sample config and comparing to my existing config. Anything in particular that I need to change?




---
**Arthur Wolf** *November 19, 2016 22:26*

Take a look at [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/edge/upgrade-notes.md), upgrade both firmware and config ( port your old values to a new example config )


---
**Guy Bailey** *November 19, 2016 22:40*

OK, going through the config now and it looks like PID for the heat bed has been removed. I had tuned this value and set it before. Is that handled differently now, or are the old commands still valid?




---
**Arthur Wolf** *November 19, 2016 22:42*

**+Guy Bailey** Just port your old value to the new config, we just removed them from the example


---
**Guy Bailey** *November 19, 2016 22:48*

OK, going good so far. What about panel config? Same thing, just port over? panel.external_sd                     true              # set to true if there is an extrernal sdcard on the panel

panel.external_sd.spi_channel         1                 # set spi channel the sdcard is on

panel.external_sd.spi_cs_pin          0.28              # set spi chip select for the sdcard (or any spare pin)

panel.external_sd.sdcd_pin            0.27!^            # sd detect signal (set to nc if no sdcard detect) (or any spare pin)


---
**Arthur Wolf** *November 19, 2016 22:48*

Yep just port anything that's missing, it's an example


---
**Guy Bailey** *November 19, 2016 23:03*

ok. Good so far but I notice that my jog dial is now travelling two rows instead of one with each tick. Is there a config setting for that?


---
**Arthur Wolf** *November 19, 2016 23:05*

panel.encoder_resolution , try values 1, 2 and 4


---
**Guy Bailey** *November 19, 2016 23:24*

OK, using a value of 4 fixed it. Tried to print something and its doing something new with the homing. [https://goo.gl/photos/45NQezS2zJCAGfan7](https://goo.gl/photos/45NQezS2zJCAGfan7)



[photos.google.com - New video by Guy Bailey](https://goo.gl/photos/45NQezS2zJCAGfan7)


---
**Arthur Wolf** *November 19, 2016 23:50*

Not sure what I'm supposed to see, that looks normal.


---
**Guy Bailey** *November 19, 2016 23:51*

OK, I think I must have had it homing each axis individually then. It appears to be doing all at the same time. Thanks for helping me get updated! Now on to updating Slic3r and Repetier. 


---
**Arthur Wolf** *November 20, 2016 00:01*

:)


---
*Imported from [Google+](https://plus.google.com/+GuyBailey/posts/L7szRyzk42F) &mdash; content and formatting may not be reliable*
