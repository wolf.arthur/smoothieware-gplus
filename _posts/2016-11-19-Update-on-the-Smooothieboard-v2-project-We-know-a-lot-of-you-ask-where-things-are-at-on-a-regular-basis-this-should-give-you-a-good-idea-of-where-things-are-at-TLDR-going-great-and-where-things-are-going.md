---
layout: post
title: "Update on the Smooothieboard v2 project : We know a lot of you ask where things are at on a regular basis, this should give you a good idea of where things are at ( TL;DR : going great ), and where things are going"
date: November 19, 2016 12:46
category: "General discussion"
author: "Arthur Wolf"
---
Update on the Smooothieboard v2 project : [http://smoothieware.org/blog:15](http://smoothieware.org/blog:15)



We know a lot of you ask where things are at on a regular basis, this should give you a good idea of where things are at ( TL;DR : going great ), and where things are going.



As always, we need all the help we can get, contributors are <b>*extremely*</b> welcome.



Please help spread your enthusiasm for the Smoothie v2 project, re-share this post, it takes only a few seconds.



Thanks all :)





**"Arthur Wolf"**

---
---
**ƸӜƷ** *November 19, 2016 18:13*

Looks interesting. What kind of contributions are you looking for? More on the development of hardware, software, firmware or in general support and fund raising?

(I'm an experienced electronics engineer and I own and like your SmoothieBoard X5 V1)


---
**Arthur Wolf** *November 19, 2016 18:23*

**+ExtraBase** We are looking for all of those, don't hesitate to email me at wolf.arthur@gmail.com


---
**Artem Grunichev** *November 21, 2016 06:44*

Thank you for update!


---
**Greg Nutt** *November 21, 2016 14:07*

I see the NuttX, by none other than Gregory Nutt, is being considered as the RTOS.  Sadly, this is not me but another fellow by the same name!


---
**Arthur Wolf** *November 21, 2016 14:40*

**+Greg Nutt** That would have been quite the coincidence :)


---
**Dieter Kedrowitsch** *November 21, 2016 21:00*

So, if I'm understanding this correctly to add additional stepper drivers one must purchase an expansion board that interfaces via CAN bus?


---
**Arthur Wolf** *November 21, 2016 21:03*

**+Dieter Kedrowitsch** No. That's one way of adding entire extruder+hotend boards.

But you can also just wire an external driver, or wire an external mosfet board etc ... and we'll have very easy plug-and-play extension boards just for that too.


---
**Dieter Kedrowitsch** *November 22, 2016 14:52*

Good to know!


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/E5e8NWytToa) &mdash; content and formatting may not be reliable*
