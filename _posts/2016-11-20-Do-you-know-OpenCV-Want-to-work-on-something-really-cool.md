---
layout: post
title: "Do you know OpenCV ? Want to work on something really cool ?"
date: November 20, 2016 17:41
category: "General discussion"
author: "Arthur Wolf"
---
Do you know OpenCV ? Want to work on something really cool ?



We have this nice HMI project called fabrica for CNC/laser/3DP machines : [goo.gl/hQPjxY](http://goo.gl/hQPjxY)

As part of that, we want to implement something really fancy : using cameras to take pictures of the work area and stock on a CNC or laser, and use 3D reconstruction to automatically detect thing's position and help the user in positioning the toolhead before starting a job.

You can see a presentation of the project here : [goo.gl/QMCV8b](http://goo.gl/QMCV8b)



We are looking for volunteer contributors as always, but if somebody feels they can actually do this project in it's entirety, we can probably find funds to actually pay them to do it all ( consulting work ).

Contact : wolf.arthur@gmail.com



Please re-share with your OpenCV wizard friends !





**"Arthur Wolf"**

---
---
**Steve Anken** *November 20, 2016 18:23*

YES! Please, an appeal to the folks with CV chops to help get this technology into the hands of small shops and schools. The vision stuff is the killer app that can open all the maker machines to a larger (less technical) community. It also leverages the Maker's time with remote 24/7 shops that use 6 axis arms (with vision) to run the CNCs, lasers, 3d printers, etc...


---
**Arthur Wolf** *November 20, 2016 18:31*

**+Steve Anken** Yes, exactly :)

However, this requires finding someone who can do it, has time to do it, wants to do it, and can do it for a reasonable amount of money. This is sort of a shot in the dark ...


---
**Mark Moissette (ckaos)** *November 20, 2016 19:29*

I'll clone myself so I have spare time and I am all yours !!

Jokes aside,  this is a great project , really wish I had the time :(

you could also do this to do automatic printed/cut object validation, etc , so much possible stuff !

(I did the upgrade of the Node Opencv C bindings to Nan around 2 years ago when coding a small 3d scanning & machine vision, app + a few other things)


---
**Arthur Wolf** *November 20, 2016 19:36*

**+Mark Moissette** Ping me if you break a leg and end up with a lot of free time on your hands :)


---
**Steve Anken** *November 20, 2016 20:04*

**+Arthur Wolf** , yep. It's a lot of work to learn and lots of well paid jobs, and need, in industry for people with that skill set. I know one job opening now for anybody who has any skills with Open CV. The CEO has vision systems on some machines now but it is bloody expensive. It is such a killer tech and I really hope more people realize how much it can do to change the automation game. It's a big part of closing the loop.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/AqveVL2aE7x) &mdash; content and formatting may not be reliable*
