---
layout: post
title: "I do not know who owns the content for: ....below are some suggestions"
date: November 20, 2016 14:41
category: "General discussion"
author: "Don Kleinschnitz Jr."
---
I do not know who owns the content for: 

[http://smoothieware.org/laser-cutter-guide](http://smoothieware.org/laser-cutter-guide)



....below are some suggestions.



The link: Don's guide to K40+Smoothie : Part1 / Part2

points to individual posts regarding the LPS.

I now have an index post to all the posts that make up my K40-S conversion.



I suggest we change this link to:

"K40-S conversion": @ [http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



--------------------

In the section that instructs on how to use Switch it has references to 1.7 (which I believe does not exist). Although this may be an example it is confusing to some as they try to wire up 1.7. 

[http://smoothieware.org/switch](http://smoothieware.org/switch)



This post outlines what we have tested to work: 

[http://donsthings.blogspot.com/2016/11/k40-s-lift-table-integration.html](http://donsthings.blogspot.com/2016/11/k40-s-lift-table-integration.html)







**"Don Kleinschnitz Jr."**

---
---
**Anthony Bolgar** *November 20, 2016 15:00*

I am almost done my upgrade guide for the way I did it. Once that is comlete I wanted to do up a multi way guide that has different options for the wiring and setup, along with an EXPLANATION of why each way may be the way to go for that person.


---
**Arthur Wolf** *November 20, 2016 17:23*

Hey ! I don't have time to do the modifications right now, but it's a wiki, you can edit it, and you really should !


---
**Don Kleinschnitz Jr.** *November 20, 2016 20:28*

Ok I edited it ... been a while since I had to build a document directly with HTML tags :) ... uggh!


---
**Arthur Wolf** *November 20, 2016 20:28*

**+Don Kleinschnitz** That's not HTML, that's like wiki-tag :)


---
**Don Kleinschnitz Jr.** *November 20, 2016 20:31*

Even worse .... :)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/heX9RBrL86K) &mdash; content and formatting may not be reliable*
