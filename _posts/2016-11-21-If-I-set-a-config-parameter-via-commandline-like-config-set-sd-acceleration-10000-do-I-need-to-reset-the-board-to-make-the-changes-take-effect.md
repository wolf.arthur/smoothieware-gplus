---
layout: post
title: "If I set a config-parameter via commandline, like \"config-set sd acceleration 10000\", do I need to reset the board to make the changes take effect?"
date: November 21, 2016 18:56
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
If I set a config-parameter via commandline, like "config-set sd acceleration 10000", do I need to reset the board to make the changes take effect?





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *November 21, 2016 19:16*

Yep


---
**René Jurack** *November 21, 2016 19:18*

Thx, **+Arthur Wolf**. Maybe this info should be added to [http://smoothieware.org/console-commands](http://smoothieware.org/console-commands)

[smoothieware.org - Console Commands - Smoothie Project](http://smoothieware.org/console-commands)


---
**Arthur Wolf** *November 21, 2016 19:22*

yep

added to documentation, thanks :)


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/CNR3UZanJQL) &mdash; content and formatting may not be reliable*
