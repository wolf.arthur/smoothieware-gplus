---
layout: post
title: "Originally shared by Arthur Wolf Update on the Smooothieboard v2 project : We know a lot of you ask where things are at on a regular basis, this should give you a good idea of where things are at ( TL;DR : going great ), and"
date: November 21, 2016 06:43
category: "General discussion"
author: "Artem Grunichev"
---
<b>Originally shared by Arthur Wolf</b>



Update on the Smooothieboard v2 project : [http://smoothieware.org/blog:15](http://smoothieware.org/blog:15)



We know a lot of you ask where things are at on a regular basis, this should give you a good idea of where things are at ( TL;DR : going great ), and where things are going.



As always, we need all the help we can get, contributors are <b>extremely</b> welcome.



Please help spread your enthusiasm for the Smoothie v2 project, re-share this post, it takes only a few seconds.



Thanks all :)





**"Artem Grunichev"**

---
---
**Arthur Wolf** *November 21, 2016 10:38*

Thanks for re-sharing :)


---
*Imported from [Google+](https://plus.google.com/101033487868604831015/posts/GzrRp4iaDFi) &mdash; content and formatting may not be reliable*
