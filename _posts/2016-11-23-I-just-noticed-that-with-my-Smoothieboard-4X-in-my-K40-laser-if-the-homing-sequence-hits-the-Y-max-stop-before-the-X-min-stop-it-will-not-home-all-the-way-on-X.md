---
layout: post
title: "I just noticed that with my Smoothieboard 4X in my K40 laser, if the homing sequence hits the Y max stop before the X min stop it will not home all the way on X"
date: November 23, 2016 03:55
category: "General discussion"
author: "Steve Prior"
---
I just noticed that with my Smoothieboard 4X in my K40 laser, if the homing sequence hits the Y max stop before the X min stop it will not home all the way on X.  But if it hits the X min stop before the Y max stop it works correctly.  Is there some setting I've missed?





**"Steve Prior"**

---
---
**Wolfmanjm** *November 23, 2016 05:16*

check the max_travel settings are correct. If they are then check you have the endstops hooked up correctly, the headers on different boards have different pinouts. Check the independent functioning of both endstops using M119.


---
*Imported from [Google+](https://plus.google.com/+StevePrior/posts/GW8WJAS95CW) &mdash; content and formatting may not be reliable*
