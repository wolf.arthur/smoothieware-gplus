---
layout: post
title: "Question on the new webpages for Smoothie"
date: November 23, 2016 22:34
category: "General discussion"
author: "Jack Colletta"
---
Question on the new webpages for Smoothie.  When I try to access the sd/webif/index.html the page is displayed ok.  If i select the imrahil-ui interface the board locks up.  



I have chased it down to possibly an access denied error when trying to display the motor control section in the upper right corner.  



I am using Ubuntu 16.04, Firefox 50.0 and the latest edge/config of smoothie.  Is there a security setting i need to change in Firefox?



Thanks











**"Jack Colletta"**

---
---
**Arthur Wolf** *November 23, 2016 22:46*

The new web interface stuff is still experimental, howerer the one you are trying to access should work.

Can you try formatting your SD card ?


---
**Jack Colletta** *November 23, 2016 23:04*

I already did that, due to other thing...Bad me



I unzipped the files on another computer and found the imrahil interface would display correctly if i accessed the classic one first.  No such luck on the real thing.  I found the access errors in one of the developer views in firefox on my other computer.



I will dig some more and possibly try another browser.



Thanks!


---
**Arthur Wolf** *November 23, 2016 23:10*

Does the computer have internet access ?

Also does a complete reload ( ctrl F5 or alt f5 or maj f5 depending on browser I think ) help ?


---
**Jack Colletta** *November 23, 2016 23:21*

Yes the computer has internet access.  I will check the browser after this print finishes...




---
**Jack Colletta** *November 24, 2016 01:40*

I messed with this a bit more before calling it a night. 



1. Board completely locks up so F5 refresh does nothing..Firefox browser. 



2. I installed Opera browser and I can select classic page but when I click imrahil page it does nothing. It seems to catch the error before drawing the page. The board stays alive since the page does not load. 



If someone could point me to what to trap on the developer pages of these browser maybe I could help in fixing it.  




---
**Jarek Szczepański** *November 24, 2016 20:06*

Hi Jack! Check the latest version of my UI ([https://github.com/imrahil/smoothieware-webui/raw/3.1/dist.zip](https://github.com/imrahil/smoothieware-webui/raw/3.1/dist.zip)) - I compiled everything into one index.html (requests to smoothie reduced from 4 to 1 file)


---
**Jack Colletta** *November 25, 2016 03:21*

I will give it a try in the morning and let you know


---
**Jack Colletta** *November 26, 2016 14:26*

**+Jarek Szczepański** That index file worked great!  Thank you!

Great work.


---
**Arthur Wolf** *November 26, 2016 18:25*

**+Jarek Szczepański** Would you mind contributing it to [https://github.com/Smoothieware/Webif-pack](https://github.com/Smoothieware/Webif-pack) ?


---
**Jarek Szczepański** *November 26, 2016 22:19*

**+Arthur Wolf** done! :)


---
*Imported from [Google+](https://plus.google.com/110069292734161585037/posts/NkAo3281KDE) &mdash; content and formatting may not be reliable*
