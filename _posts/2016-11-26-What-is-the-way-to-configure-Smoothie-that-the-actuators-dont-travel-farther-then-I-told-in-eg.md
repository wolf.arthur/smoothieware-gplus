---
layout: post
title: "What is the way to configure Smoothie, that the actuators don't travel farther then I told in e.g"
date: November 26, 2016 18:11
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
What is the way to configure Smoothie, that the actuators don't travel farther then I told in e.g. "alpha_max"?





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *November 26, 2016 18:23*

That feature ( called "soft endstops" ) does not exist yet. It is more complicated to implement <b>cleanly</b> than one might expect.

[https://docs.google.com/document/d/1U6nzx1boqF-J2GGPWF4yIaaVib0JNodVWSKBfwiyp_M/edit#](https://docs.google.com/document/d/1U6nzx1boqF-J2GGPWF4yIaaVib0JNodVWSKBfwiyp_M/edit#)


---
**René Jurack** *November 26, 2016 18:25*

Thx for the fast response again :)


---
**Thomas “Balu” Walter** *November 26, 2016 19:59*

So to be safe I better add more limit switches for now? :)


---
**Arthur Wolf** *November 26, 2016 20:12*

Well it's not like on most machines you are going to break something if you go over limits. But yes, limit switches are the solution if you are worried.


---
**René Jurack** *November 26, 2016 20:48*

I never had this happen during printing. It only happened during calibration-orgies...


---
**Thomas “Balu” Walter** *November 26, 2016 20:48*

True, but I hate the noise of motors missing steps. And it kind of tickles my "perfectionism" - must be a German thing ;)


---
**René Jurack** *November 26, 2016 20:49*

**+Thomas Walter** What do you say about not having any endstops at all at X&Y? :D


---
**Thomas “Balu” Walter** *November 26, 2016 20:52*

**+René Jurack** I'm seriously thinking about paypaling you 10 bucks, so you can order a few. :-D


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/eNpoLp32NDZ) &mdash; content and formatting may not be reliable*
