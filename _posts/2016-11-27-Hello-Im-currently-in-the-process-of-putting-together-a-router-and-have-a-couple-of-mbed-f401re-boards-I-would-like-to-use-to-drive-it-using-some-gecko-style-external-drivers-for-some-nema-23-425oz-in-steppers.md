---
layout: post
title: "Hello, I'm currently in the process of putting together a router and have a couple of mbed f401re boards I would like to use to drive it using some gecko style external drivers for some nema 23 425oz/in steppers"
date: November 27, 2016 04:34
category: "General discussion"
author: "Michael Bawden"
---
Hello, I'm currently in the process of putting together a router and have a couple of mbed f401re boards I would like to use to drive it using some gecko style external drivers for some nema 23 425oz/in steppers. I've only quickly looked over your code and  I see you have a port to an mbed board and my question is where to change the pin numbers in the source code to suit the ST board. I'm hoping to use Chiipeppr on my mac or an android tablet to send the Gcode from Onshape to do the whole fabrication.

Thanks

Mike





**"Michael Bawden"**

---
---
**Marc Miller** *November 27, 2016 05:47*

**+Michael Bawden**​  I've become a fan of Onshape and would be interested to hear more about your project if you're sharing details somewhere.  Sounds cool.


---
**Michael Bawden** *November 27, 2016 06:21*

Hello Mark and Smoothiers,

 I've really only started to use Onshape now that I need a cad system for a gantry style router with a 800mmx800mmx300mm build volume so I'm really a beginner at this sort of thing although I'm a mechanical engineer of 40 years.  It'll be belts on the X and Y and ballscrew on the Z, with the option of linear encoders later on. I'm really building the  router as an exercise and have no real projects yet although I'm looking at motorcycle cylinder heads and making castings using the latest thinking in ports and airflows and PCB routing as veroboard is getting tiresome. I'm also looking at building an SLS device as a way of making intricate clay pottery as a money making sideline. 


---
**Arthur Wolf** *November 27, 2016 10:31*

**+Michael Bawden** Getting Smoothie to work on the f401re board would be weeks of work, it is a complete port, and it is not as simple as changing pins. Smoothie currently only runs on the LPC1769 ( and the v2 version somewhat runs on the LPC4337 ).


---
**Michael Bawden** *November 28, 2016 08:56*

Hello Arthur, I always thought that one of the "selling points" of mbed was supposed to be that you could port a program to any board easily, apparently not. I suppose I'd better have a good look at the code and see if I'm up to the task, you'll probably curse my name after a while of incessent questions. Thanks for the heads up Peter, I'll have a look at CNCWeb and Octoprint this week but nothing is cast in stone  yet as I've still got parts arriving and a busy work schedule to contend with leading up to Christmas. And as I stated before this is also a learning exercise, there's no fun in bolting things together that will just work, the sleepless nights and gotchas are part of the journey.


---
**Arthur Wolf** *November 28, 2016 09:30*

**+Michael Bawden** Well, that's true if your program is "blink a LED", but Smoothie uses pretty much everything on the chip intensively, so unless the port to any given chip is fully complete ( which is rare ), you have work on your hands. And on top of that, Smoothie does some things ( step generation, pin toggling, usb, ethernet ) without mBed due to severe limitations in mBed. So yeah, lots of work.

If you are going to be working on a port, why not instead help us with the port to LPC4337 ? That's definitely gonna be more immediately useful to a <b>lot</b> of people.


---
**Michael Bawden** *November 28, 2016 20:14*

I see the only supplier linked from the mbed site (element14) says the LPC4337 is no longer manufactured is there an alternative supplier or are we moving on to another board like the LPC43S67?

Cheers

Mike


---
**Arthur Wolf** *November 28, 2016 20:45*

**+Michael Bawden** Most Smoothie2 contributors use this : [http://www.micromint.com/component/content/article/39-products/sbc/196-bambino200.html](http://www.micromint.com/component/content/article/39-products/sbc/196-bambino200.html)


---
**Michael Bawden** *December 01, 2016 09:51*

OK, so I bought 2 boards from micro mint and I guess my next question is, how do I get involved in the rewrite, I'll have to devote a couple of days to getting my head around what's been written and where you want to go.

Cheers

Mike


---
**Arthur Wolf** *December 01, 2016 09:52*

**+Michael Bawden** Well right now we are at a critical point where we are going to stop using mBed, and are going to rewrite everything on top of NuttX. I need to ping the current developers about it to. I think the best thing to do is just email me at wolf.arthur@gmail.com, and we'll figure it out.


---
**Michael Bawden** *December 25, 2016 23:46*

I finally got around to plugging my bambino200 boards into my Mac and they don't show up in the finder. According to micromint I need a DFU utility , My question is, can I use any DFU program or is there a specific one for the Bambino board?


---
**Arthur Wolf** *December 25, 2016 23:48*

**+Michael Bawden** This page : [github.com - Smoothie2](https://github.com/Smoothieware/Smoothie2/blob/master/notes/Bambino210E-board.creole#bambino210e-board-programmingdebugging-notes) should have the info you need. if it doesn't tell me and we'll look it up


---
*Imported from [Google+](https://plus.google.com/114796427917907920335/posts/7ycdn8dmzJv) &mdash; content and formatting may not be reliable*
