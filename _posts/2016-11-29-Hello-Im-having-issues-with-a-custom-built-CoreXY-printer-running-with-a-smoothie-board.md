---
layout: post
title: "Hello, I'm having issues with a custom built CoreXY printer running with a smoothie board"
date: November 29, 2016 22:44
category: "General discussion"
author: "Ryan Lewis"
---
Hello,



I'm having issues with a custom built CoreXY printer running with a smoothie board. I am getting growth in the Z axis on the prints. It seems to be an inconsistent issue, because I've printed parts completely fine without seeing it.



Usually the entire print will start with the issue, so I'll shut down the machine and turn it back on then attempt to re-print and it doesn't happen. I attempted to run a print over night and the issue popped up about midway through. 



I've tried swapping motors, swapping boards, ran external drivers, power supply, and even put ferrite beads on the usb cord to see if there was some kind of noise. 



I'm stumped. Any idea what's going on? Any help is greatly appreciated.



![missing image](https://lh3.googleusercontent.com/-_nJcOIPmMTA/WD4Et7LXiEI/AAAAAAAAOd8/KxzfZCEtDycSGU_CwvR96oh2yvnqufSwQCJoC/s0/IMG_20161129_172742.jpg)
![missing image](https://lh3.googleusercontent.com/-uvz6AraPXRo/WD4Et0iScaI/AAAAAAAAOd8/1zSs0jYDfpwOVF8VkAEfjlJw9bVvZTYBgCJoC/s0/IMG_20161129_172756.jpg)
![missing image](https://lh3.googleusercontent.com/-P3st3o-9vq0/WD4Et1UpG6I/AAAAAAAAOd8/7G2Sq5zszgIRO8s-raNAPwQPslAdfkk2wCJoC/s0/IMG_20161129_172811.jpg)

**"Ryan Lewis"**

---
---
**Griffin Paquette** *November 29, 2016 22:48*

I had this happen back on my very first printer. Turned out to be my laptop not being able to keep up. Do you run off of octoprint or some dedicated computer? I like getting my machines to run standalone which is why I run a dedicated Pi 3 running octoprint. 


---
**Arthur Wolf** *November 29, 2016 22:52*

It could be a problem with communication, or with air flow, or with temperature control, or ( less likely ) with Z axis wobble.



Can you check your temperature is very stable while printing ( for example via pronterface ) ?




---
**Ryan Lewis** *November 29, 2016 23:00*

**+Griffin Paquette** It has a dedicated computer. An Intel NUC i5 w/ 16GB ram. That's interesting though. Maybe I should try running off of my laptop for a few prints.


---
**Ryan Lewis** *November 29, 2016 23:15*

**+Arthur Wolf**

What do you consider stable? +/- 2 or 3 degrees? I use Pronterface for control and I've never seen any dramatic swings in temperature. 



As far as communication, is there something I can do to check to see if that's what is causing it?



And what do you mean by airflow?



Thanks for the help!


---
**Arthur Wolf** *November 29, 2016 23:17*

Ideally you'd want it to be between 1 degree, though 2 shouldn't cause problems that bad.

Swings in the part ( possibly due to the bed swinging temp ) could cause things that look like this.



My best bet is this is purely mechanical though ( bed plate not attached well enough/bending/something ? )


---
**Griffin Paquette** *November 29, 2016 23:23*

Could be bed sag like **+Arthur Wolf** said. Makes sense since it doesn't happen at first but as the bed drops down it occurs more. 


---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 23:49*

I would jump directly to the z axis ringing conclusion. 


---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 23:50*

But that could also be under extrusion. It's serious enough that it's kind hard to be sure of just one culprit. 


---
**Ryan Lewis** *November 29, 2016 23:53*

**+Griffin Paquette** **+Arthur Wolf**

Hmm. Well, the heat mat is mounted to 1/2"  thick fiber insulated board riding on 4 linear rods and 2 acme lead screws. It's pretty darn sturdy. I like the thought process, though. I thought it was very odd that the Z height of the benchy physically grew taller than it was supposed to be. The back window shows it pretty well. It's almost as if the Z started moving twice as far as it was supposed to every layer. I have no idea how it would suddenly do that, though.


---
**Matt Wils** *November 30, 2016 00:49*

Check Z-Axis set screw on your coupler. 


---
**Alex Krause** *November 30, 2016 02:27*

Are you running a genuine smoothie or a clone? The clones have notorious comm issues 


---
**Ryan Lewis** *November 30, 2016 02:44*

**+Ray Kholodovsky** I don't think underextrusion is the culprit because I can go from printing like this in one print and completely fine in the next.


---
**Ryan Lewis** *November 30, 2016 02:45*

**+Matt Wils** Just checked them, still tight.



**+Alex Krause** Genuine smoothie. I've tried multiple boards.


---
**Griffin Paquette** *November 30, 2016 02:48*

I think you've just answered your own question. It has to be the z axis or your computer then. If you messed with/replaced everything else I wouldn't worry about it being the board.


---
**Ryan Lewis** *November 30, 2016 02:57*

**+Griffin Paquette** I guess you're right. I was kind of hoping someone had experienced something similar. I've always used megatronics boards and never had an issue like this, so my mind jumped to the board. 



Thanks for all the help!


---
**Stephanie A** *November 30, 2016 03:10*

The fact that it's happening at regular intervals is telling you something. It also appears to happen more on layers with less material. 

Try printing a large cylinder, maybe 30mm in diameter. This will rule out issues with layer cooling. 

While it's printing, check the temperature of the motors and the motor drivers, look at the z motors and the extruder motors and drivers.



It can also be your bed expanding and contracting with bed temperature. Large temperature swings along with a material that has a high thermal expansion rate could be moving the bed up and down. It doesn't take much to have a negative effect. 


---
**Griffin Paquette** *November 30, 2016 03:14*

[photos.google.com - New photo by Griffin Paquette](https://goo.gl/photos/ahjjJ6c6mc6fTcyy8)


---
**Griffin Paquette** *November 30, 2016 03:15*

Sorry for the crap quality photo but this is all I could dig up of mine. Very strange and was easily solved when I switched computers. The hard drive on this computer failed shortly after which just made me more sure of my issue. 


---
**Xiaojun Liu** *November 30, 2016 04:33*

I had a similar issue for my UM2 clone,with periodic z wave pattern. People said it was caused by z-wobbling. But afterwards I figured out it was the wrong e-motor stepping per unit. I set it the twice for it should be.


---
*Imported from [Google+](https://plus.google.com/102836622691608796151/posts/cAquKZiGfjt) &mdash; content and formatting may not be reliable*
