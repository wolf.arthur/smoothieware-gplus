---
layout: post
title: "Meanwhile, in the Smoothieboard v2 design cave ..."
date: November 30, 2016 17:36
category: "General discussion"
author: "Arthur Wolf"
---
Meanwhile, in the Smoothieboard v2 design cave ...

![missing image](https://lh3.googleusercontent.com/-lsLUFLh0V0k/WD8OLKRv9mI/AAAAAAAAOaU/TTNZmPcQ0Mg9f6atmWr5HAq5UZsaAIJ9wCJoC/s0/slack-imgs.com.png)



**"Arthur Wolf"**

---
---
**Douglas Pearless** *November 30, 2016 20:09*

Which tool are you using?


---
**Arthur Wolf** *November 30, 2016 20:12*

That's **+Mark Cooper** using Kicad :)


---
**Albert Latham** *December 03, 2016 19:22*

I see a BGA. Will there be a board layout that's friendlier to hand assembly?


---
**Arthur Wolf** *December 03, 2016 19:36*

**+Albert Latham** For the v2 series we are moving away from being able to hand-assemble. Not because we want to, but because we have to, really. v2 is all about solving the limitations of v1, and lack of free gpio is one of those.


---
**Albert Latham** *December 03, 2016 19:50*

Awesome! Is there a repository somewhere that I can look at?



[edit: found the github repo]


---
**Arthur Wolf** *December 03, 2016 19:51*

**+Albert Latham** Not yet, but soon. It will be here : [https://github.com/Smoothieware/Smoothieboard2](https://github.com/Smoothieware/Smoothieboard2) the repo already contains extension boards contributed by the community.


---
**Albert Latham** *December 03, 2016 20:21*

Excellent. I think there is a "pro" variant in my future.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/3jcx9MP9rRM) &mdash; content and formatting may not be reliable*
