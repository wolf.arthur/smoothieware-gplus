---
layout: post
title: "Hi, can someone of the smoothie team please check, if the solution Ray and I developed will work or not?"
date: December 05, 2016 18:53
category: "General discussion"
author: "Marc Pentenrieder"
---
Hi, can someone of the smoothie team please check, if the solution Ray and I developed will work or not?



<b>Originally shared by Marc Pentenrieder</b>



Hi, 

I just discussed with Ray how to connect my external servo controller to the remix board with external stepper adapters.



We got to the conclusion, that it would be best to use "open collector, option 2" (see pictures) with a 5V psu and smoothieware configured as open drain on the EN,DIR(Sign),STEP(Pulse) pins.



It would be nice if someone can have a look at it and proove it right or possible wrong.

Any suggestions are apreciated.



![missing image](https://lh3.googleusercontent.com/-YDpE6B3-AgQ/WEW1c4wFQSI/AAAAAAAAFOQ/EVBxfnoRNV003VWqSofws89Dp7nDMBWDQCJoC/s0/05.12.16%252B-%252B1.jpeg)
![missing image](https://lh3.googleusercontent.com/-i4FGW7QWXfM/WEW1c9q4u6I/AAAAAAAAFOQ/CZ5CXOfyKGA6p2FvMOCu0P-VhPYXFjCEwCJoC/s0/05.12.16%252B-%252B3.jpeg)
!![images/21e79d76ee289dab713185d68103acb5.jpeg](images/21e79d76ee289dab713185d68103acb5.jpeg)

**"Marc Pentenrieder"**

---
---
**Arthur Wolf** *December 05, 2016 19:08*

This is pretty much the same as any external driver, see the "CNC mill guide" appendix on external drivers on the smoothieware website


---
**Marc Pentenrieder** *December 05, 2016 19:21*

Yes thats what we wanted to use "Wiring an external driver with a common anode".

Thanks Arthur.


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/HivoEkB9oW4) &mdash; content and formatting may not be reliable*
