---
layout: post
title: "More progress on the Smoothieboard v2-mini-alpha design"
date: December 06, 2016 01:07
category: "General discussion"
author: "Arthur Wolf"
---
More progress on the Smoothieboard v2-mini-alpha design.

![missing image](https://lh3.googleusercontent.com/-L6NHu5twPG8/WEYPPifQs3I/AAAAAAAAOfw/jxsvoALgCFYHrnGVJAj4p_U-7LGUu8ACwCJoC/s0/Smoothie2Mini-pre12-layout.png)



**"Arthur Wolf"**

---
---
**James Rivera** *December 06, 2016 04:34*

Is the v2 going to have a more powerful/faster ARM CPU than v1?


---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2016 05:20*

I'm not Arthur and I don't play him on TV :) but I can answer this.

Yeah, Smoothie v2 is upgrading to a Cortex M4 with M0 coprocessor for step generation.  Current Smoothie v1 uses a Cortex M3. Our M3's run at 120MHZ and the M4's are in the 200MHZ range, don't know the exact value off the top of my head. 


---
**James Rivera** *December 06, 2016 07:48*

LOL! Thanks **+Ray Kholodovsky**. :)


---
**Arthur Wolf** *December 06, 2016 09:45*

204 Mhz, 264kB ram, 8MB flash, FPU. The board will also have higher microstepping.

That's just the v2-mini.

The v2 itself will have even better drivers, improved safety features, higher current capacity, and a lot of goodies.

And the v2-pro will have even better everything on top of that, plus a FPGA.


---
**Phil Aldrich** *December 06, 2016 11:44*

I see an upgrade in my future. Many thanks Arthur and team!


---
**Thomas “Balu” Walter** *December 07, 2016 08:14*

I just got my fully equipped v1 board, but with the speed you guys are working at, I guess there's no need to unpack that...


---
**Arthur Wolf** *December 07, 2016 10:24*

**+Thomas Walter** v2 is a huge project, even though a lot of progress is being made, it's still a long way away


---
**Thomas “Balu” Walter** *December 07, 2016 10:34*

**+Arthur Wolf** I'm not sure if I should be happy about that or not. ;)


---
**James Rivera** *December 08, 2016 01:14*

**+Arthur Wolf** I have a layman's understanding of FPGAs. I know this is a broad question, but what are some of the uses you imagine it being good for?


---
**Arthur Wolf** *December 08, 2016 10:03*

**+James Rivera** step generation at higher speeds/quality, raster engraving on lasers at high speeds, closed-loop motion control, logic analyzer of the outputs of the microcontroller for debugging, are a few.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/im3ozc4dZZj) &mdash; content and formatting may not be reliable*
