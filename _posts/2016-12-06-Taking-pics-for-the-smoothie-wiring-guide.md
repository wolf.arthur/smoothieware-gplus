---
layout: post
title: "Taking pics for the smoothie wiring guide"
date: December 06, 2016 15:55
category: "General discussion"
author: "Maxime Favre"
---
Taking pics for the smoothie wiring guide 

![missing image](https://lh3.googleusercontent.com/-CjWWngmdp3s/WEbfa47gN9I/AAAAAAAAE30/s6PXBNyjcuk_rlcmlKNHysR1pp1-mDQlwCJoC/s0/IMG_20161206_153312.jpg)



**"Maxime Favre"**

---
---
**Todd Fleming** *December 06, 2016 16:06*

Nice setup. It looks like you'll get some nice-looking shots!


---
**Arthur Wolf** *December 06, 2016 16:27*

Nice !! Maybe on the wiki mention that one must click on the arrows, they are not very visible maybe someone might miss them.


---
**Maxime Favre** *December 06, 2016 16:29*

Will do. Btw do you have an idea how to change the font color on the caroussel captions ?


---
**Arthur Wolf** *December 06, 2016 16:31*

Unfortunately it looks like you can't : [http://snippets.wikidot.com/code:carousel](http://snippets.wikidot.com/code:carousel)

One can write raw html into the pages, maybe you can integrate new css rules that way ?


---
**James Rivera** *December 08, 2016 19:20*

That tripod setup is exactly what I need for some jewelry photography!  Brand and model info? Better yet, link?


---
**Maxime Favre** *December 08, 2016 19:24*

It's a manfrotto 190xpro4 (there's a new model since). Ball head ref is: 468MGRC4


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/EUFRzR7qYx2) &mdash; content and formatting may not be reliable*
