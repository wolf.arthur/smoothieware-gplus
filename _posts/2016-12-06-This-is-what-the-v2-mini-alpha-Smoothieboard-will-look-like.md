---
layout: post
title: "This is what the v2-mini alpha Smoothieboard will look like"
date: December 06, 2016 23:21
category: "General discussion"
author: "Arthur Wolf"
---
This is what the v2-mini alpha Smoothieboard will look like.

What do you think ?

Re-share to get others as excited by this as we all are :p

![missing image](https://lh3.googleusercontent.com/-BJ1UgYZ1CCk/WEdIBU5qybI/AAAAAAAAOgA/G_hui9ndYgQYtN83qJdIc27Fyf4Qu1ABgCJoC/s0/Smoothie2Mini-pre13-3d.png)



**"Arthur Wolf"**

---
---
**Douglas Pearless** *December 06, 2016 23:31*

A work of art :-)


---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2016 23:48*

You know where I live


---
**Stephen Baird** *December 06, 2016 23:56*

I can't quite tell from the image, but will the mini support external drivers or will that be a full size only option? 


---
**Arthur Wolf** *December 07, 2016 00:04*

**+Stephen Baird** It has 6 gadgeteer connectors, each with 7 free GPIO pins, and you only need two GPIO pin to control an external driver.

I just posted another picture with labels to G+


---
**Stephen Baird** *December 07, 2016 00:05*

Ah, true, I forgot the gadgeteer connectors are general purpose. I'm getting very excited for v2. 


---
**Jim Christiansen** *December 07, 2016 01:06*

So for $10 we can hang the v2 mini on a network with a J11D Module ...  Sounds great!


---
**Arthur Wolf** *December 07, 2016 01:08*

**+Jim Christiansen** Well no you can't. There's no PHY either on the v2-mini or the J11D. You'll have to get a v2 or a v2-pro, not a v2-mini.


---
**Alex Hayden** *December 07, 2016 01:43*

What about dual extruder? I only see 4 motor drivers, where is the 5th?


---
**Jim Christiansen** *December 07, 2016 02:08*

Thanks for the info, **+Arthur Wolf**​.


---
**Jeff DeMaagd** *December 07, 2016 02:20*

**+Alex Hayden** I think dual extruder users would buy the normal version of smoothieboard v2. Other "mini" controllers available also tend to assume single extruder.


---
**Alex Hayden** *December 07, 2016 02:27*

Looks like v2 pro for duel. Have you started on board layout for that one yet? 


---
**Shai Schechter** *December 07, 2016 07:06*

What program(s) do you guys use?


---
**Alex Hayden** *December 07, 2016 07:08*

**+Shai Schechter**​ pretty sure they said KiCad. 


---
**Shai Schechter** *December 07, 2016 07:10*

**+Alex Hayden** Yeah I know they use that, just figured there's additional programs possibly used in conjunction with KiCad for the development of smoothie.


---
**Maxime Favre** *December 07, 2016 08:59*

Looks great ! SSR output, XT60 power connector, cant wait to see the advanced boards :) 


---
**dstevens lv** *December 08, 2016 08:43*

In the main announcement post Kilment is working on extruder boards that can interface multiple extruders. one extruder per board, multiple board compatible.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/erDKQQQmgEX) &mdash; content and formatting may not be reliable*
