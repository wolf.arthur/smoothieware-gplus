---
layout: post
title: "Will there be support for the Trinamic 2130 drivers in the near future?"
date: December 06, 2016 19:41
category: "General discussion"
author: "Griffin Paquette"
---
Will there be support for the Trinamic 2130 drivers in the near future? Working on some designs and figured that they would be a good applicant, however I know that their current SPI setup isn't part of Smoothieware.



Thanks!

Griffin





**"Griffin Paquette"**

---
---
**Arthur Wolf** *December 06, 2016 20:01*

We have code for several trinamic chips via SPI in smoothieware : [smoothieware.org - Advancedmotordriver - Smoothie Project](http://smoothieware.org/advancedmotordriver)



I expect the protocols to be very similar for chips of the same manufacturer, maybe it's not too hard to add your specific one ?


---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2016 20:08*

**+Douglas Pearless** and I tried porting his 2130 code back into smoothie1.  It's a whole different animal. Never got the SPI setup working properly. 


---
**Douglas Pearless** *December 06, 2016 22:28*

I have it working for SmoothieV2 but have not had time to back port to SmoothieV1 and also don't have suitable v1 hardware to try.


---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2016 22:42*

**+Douglas Pearless** I'm working on getting a few boards together to send to you and **+Claudio Prezzi** 


---
**Wolfmanjm** *December 06, 2016 23:04*

Most trinamic drivers are the same so there should not need to be any changes needed to the existing TMC2xx driver already in smoothie.


---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2016 23:08*

**+Wolfmanjm** 2660 (26x) and 2130 have different SPI protocols according to **+Douglas Pearless** 

Also of note is that the enable pin has to be wired as usual and is not handled via SPI for the 2130. 


---
**Griffin Paquette** *December 06, 2016 23:13*

^ that's been the battle so far. I know the 60's work, but aside from one Marlin post of someone forking the 2130's into the firmware, there is little to read on the 2130's being used to date...


---
**Wolfmanjm** *December 06, 2016 23:20*

I don't think it does have a different SPI protocol, I checked the docs and it looks exactly the same. the entrire range of trinimacs seem to be the same SPI protocol. Having enable as a pin rather than over SPI is trivial and does not warrant a new driver.


---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2016 23:22*

**+Wolfmanjm** to be clear: "it's different registers and stuff in the spi communications" plus it uses a dedicated enable pin. 


---
**Douglas Pearless** *December 06, 2016 23:28*

I have added the TMC2130 drivers to my SmoothieV2 repo and it works.  The internal registers and SPI communications are different and I needed to make a  number of changes to get it working :-)  And it

 does work, my FirePick delta is running V2 code. [https://github.com/DouglasPearless/Smoothie2](https://github.com/DouglasPearless/Smoothie2)


---
**Douglas Pearless** *December 07, 2016 00:07*

Note: When driving this at speed and 256 micro steps on three 0.9 degree stepper motors, the internal timing of the code is critical as we are talking more than 102,400 step and unstep calls per revolution of the motor(s).  I had to increase the slow ticker by 10x speed otherwise the mechanism drove into the endstops even at relatively slow speeds.  I've been considering at either moving the stepping code code to the M0 or using a Spartan 6 to implement NCO's, endstop and probes, etc.


---
*Imported from [Google+](https://plus.google.com/111302122377301084540/posts/K1Zt3E9dqs6) &mdash; content and formatting may not be reliable*
