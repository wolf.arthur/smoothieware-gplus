---
layout: post
title: "Did you know ? If your CNC mill or router controls the spindle with a VFD, you can interface that VFD with Smoothieboard using Modbus ( or the chinese derivative of it the VFD uses ), to control the spindle's rotation via"
date: December 07, 2016 22:45
category: "General discussion"
author: "Arthur Wolf"
---
Did you know ? 



If your CNC mill or router controls the spindle with a VFD, you can interface that VFD with Smoothieboard using Modbus ( or the chinese derivative of it the VFD uses ), to control the spindle's rotation via Gcode. 

Just takes a breakout board and two wires to the VFD : 



[http://smoothieware.org/spindle-module](http://smoothieware.org/spindle-module)





**"Arthur Wolf"**

---
---
**Steve Anken** *December 07, 2016 23:42*

That helps, the best documentation I've seen on interfacing to the Huanyang VFD. Are there Eagle files for the boards?


---
**Arthur Wolf** *December 07, 2016 23:48*

**+Steve Anken** I believe they are on bouni's github. You can also use the rs485 breakout from sparkfun I think.


---
**Steve Anken** *December 08, 2016 00:05*

Thank you.


---
**Bouni** *December 08, 2016 10:24*

Be aware that I've just implmented the Huanyang "Protocol" (crippled chines sh**) and not yet had the chance to test on a real Modbus device. But It should be fairly easy to get that up and running if it adheres to the specs and has a real documentation :-)


---
**Jestah Carnie** *January 07, 2017 21:38*

How may I order a Modbus PCB? Can you post to New Zealand?

 


---
**Arthur Wolf** *January 07, 2017 22:13*

**+Jestah Carnie** Contact stephane@robotseed.com


---
**Antonio Hernández** *May 20, 2017 03:44*

Hello **+Arthur Wolf**. I would like to use a board or breakout (I don't know the right term) that could bring me the chance to control my vfd ( [https://www.automationtechnologiesinc.com/products-page/cnc-spindle/kl-vfd22/](https://www.automationtechnologiesinc.com/products-page/cnc-spindle/kl-vfd22/) ) over modbus, using smoothieboad. I'm not specialized over electronics. You said sparkfun breakout, and, looking that page, I could found three products that could match the requirement. Is it posible to use one of them ?. If not, could you give more breakout examples ?. [sparkfun.com - SparkFun Transceiver Breakout - RS-485 - BOB-10124 - SparkFun Electronics](https://www.sparkfun.com/products/10124)          [https://www.sparkfun.com/products/9822](https://www.sparkfun.com/products/9822) ,          [https://www.sparkfun.com/products/12965](https://www.sparkfun.com/products/12965) . Thanks for your help.


---
**Arthur Wolf** *May 20, 2017 09:01*

**+Antonio Hernández** It's the first one


---
**Antonio Hernández** *May 20, 2017 16:11*

Great **+Arthur Wolf**. Thanks for your help. If somebody has the same trouble with this (wiring), this image could help to know the breakout's spec. [https://www.taroumaru.jp/img/arduino/4.png](https://www.taroumaru.jp/img/arduino/4.png)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/CbMshMkKDKX) &mdash; content and formatting may not be reliable*
