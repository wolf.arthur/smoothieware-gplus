---
layout: post
title: "Hello everyone, I am running Smoothie in a k40 and I cannot seem to the LAN to work, in my config I have it set as (see picture) and when it turns on I get a crazy IP address"
date: December 07, 2016 22:15
category: "General discussion"
author: "Kelly S"
---
Hello everyone, I am running Smoothie in a k40 and I cannot seem to the LAN to work, in my config I have it set as (see picture) and when it turns on I get a crazy IP address.   Any help please? 

![missing image](https://lh3.googleusercontent.com/-LsquPTHJ804/WEiKA0FaBFI/AAAAAAAAAn0/1O5LXzjt5-AyzRS5JzgphAPH1dN1q3ZAgCJoC/s0/Capture.GIF)



**"Kelly S"**

---
---
**Kelly S** *December 07, 2016 22:15*

And this is the IP address I get, I have tried to power cycle a few times to see if it will change, but it does not. 

![missing image](https://lh3.googleusercontent.com/rFyMB_yVpWn0C0_RpSywoIFvIIanPklgUuUdERDb1qeVp0o_1-SaVj7jWn9-CoQvdOuS0v4dHzT7TIyny6A_G-X464EbJssoA64=s0)


---
**Arthur Wolf** *December 07, 2016 22:34*

Can you try on another router ?


---
**Kelly S** *December 07, 2016 22:45*

I can attempt to find one, should have a box full.  Will report back.


---
**Kelly S** *December 07, 2016 22:46*

It is currently going directly to the router that came from the service provider. 


---
**Arthur Wolf** *December 07, 2016 22:46*

**+Kelly S** Some routers in rare cases don't like smoothieboard and DHCP does not work. In that case setting up a static IP should work.


---
**Kelly S** *December 08, 2016 01:42*

Got it to work, it did't use the address I gave it, probably because I kept dhcp to true, but it is now on the network.  :) 

![missing image](https://lh3.googleusercontent.com/78P9zVyXvjYcLC-FoyPNfO83PNQRAgc9eVkQI-iuGoVwMK0Lxs7a-7ldebJYXFTxiLV58If_WUonk12Ae-S8BdUpaMuLKMOezek=s0)


---
**Jeff DeMaagd** *December 08, 2016 05:06*

You wanted to use a given IP address but left it commented out?


---
**Kelly S** *December 08, 2016 05:59*

Nothing was commented out of the three settings in the static IP address.  I am not sure what made it work for sure.  I am just happy it is working.  It is a stationary device and nothing is going to change in the foreseeable future.  :)


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/EiSGoPf3s2Q) &mdash; content and formatting may not be reliable*
