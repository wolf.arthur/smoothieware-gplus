---
layout: post
title: "I'm thinking of what it takes to add 4th axis support to LW4's CAM"
date: December 07, 2016 00:40
category: "General discussion"
author: "Todd Fleming"
---
I'm thinking of what it takes to add 4th axis support to LW4's CAM. I need a reliable way to control surface speed. Let's say I compute a factor "Q" (deg/mm) based on stock diameter (varies with job) and roller diameter (varies with machine). I use this to generate X (in mm), A (in deg) moves. User says they want surface speed to be 500 mm/s (F). How do I tell smoothie what "Q" is so it can maintain (within acceleration limits) 500 mm/s surface speed and scale back the laser properly when it has to slow down?





**"Todd Fleming"**

---
---
**Arthur Wolf** *December 07, 2016 00:51*

This is "laser on a cylinder" correct ? You'll still only be using two axes right ? Y and rotation ? Then it really shouldn't be any different from XY motion.

If you are talking proper XYZA CAM then we don't have any way to do what you are talking about, expect if you do the math in advance and just use F to setup speeds to match all your requirements.


---
**Todd Fleming** *December 07, 2016 00:54*

Yes. Laser on a cylinder, 2 axis. XA motion is different from XY motion since F (mm/s) has the wrong units for A (deg).


---
**Arthur Wolf** *December 07, 2016 00:55*

I'd expect the CAM knows the diameter, so you are really working on the surface, and it's really XY, with both units being millimeters, no degrees at all.


---
**Todd Fleming** *December 07, 2016 00:57*

If A's units were mm then A's steps-per-mm setting would vary between jobs.


---
**Arthur Wolf** *December 07, 2016 00:59*

Yep, there's a gcode for that :) I think it's the best solution.


---
**Todd Fleming** *December 07, 2016 01:00*

Oooh. I thought steps-per-mm was a config file setting.


---
**Arthur Wolf** *December 07, 2016 01:00*

It is, but you can override it with a gcode


---
**Todd Fleming** *December 07, 2016 01:04*

M92? Example shows it with E; does it also work with A?


---
**Arthur Wolf** *December 07, 2016 01:05*

I think so yes.


---
**Todd Fleming** *December 07, 2016 01:06*

Fantastic! If I pass it a negative value will it invert the axis?


---
**Arthur Wolf** *December 07, 2016 01:08*

I'm not sure :) we'd have to test that


---
**Wolfmanjm** *December 07, 2016 04:26*

no negative M92 will not work. and yes the value can be degreess/step it is irrelevant it just steps the set amount in synch with the XY move. ABC is not in edge it is in a PR which you will need to fetch and compile as specified in the PR notes...  ABC is handled as per standard NIST gcode specifications (look at linuxcnc). [https://github.com/Smoothieware/Smoothieware/pull/1055](https://github.com/Smoothieware/Smoothieware/pull/1055)


---
**Todd Fleming** *December 07, 2016 04:34*

Does F control A like it does XYZ?


---
**Wolfmanjm** *December 07, 2016 04:39*

as per the gcode spec yes.


---
**Wolfmanjm** *December 07, 2016 04:40*

**+Peter van der Walt** it will be merged soon it needs just a bit more testing as there were a lot of changes, i added homing for abc, and some other improvements


---
**Todd Fleming** *December 07, 2016 04:45*

Let's make sure my understanding is correct. Assuming everything calibrated to mm:

G0 X0 A0

G1 X100 A100 F100



Will the 2nd line run the X and A motors at 70.7 mm/min, just like I had used XY instead of XA?



Edit: mm/min not /s


---
**Wolfmanjm** *December 07, 2016 04:51*

**+Peter van der Walt** generally people will need to compile these for themselves as you need to specify how many axis you need. However I can post a 4 axis CNC version as that seems to be what is needed here. There are also some notes on the changes in this version and how to specify the 4th axis, do you have somewhere you can put the firmware and the notes?


---
**Wolfmanjm** *December 07, 2016 04:54*

**+Todd Fleming** Yes, but the A is not necessarily mm/sec it could be degrees/sec but basically what you are saying is that X moves 100mm and in that time A will also move 100 degrees (or mm) in synch and the time will be 1 minute (F is mm/min not mm/sec)




---
**Todd Fleming** *December 07, 2016 05:01*

Then my understanding isn't right since the equivalent XY move would take 1.4 min. The A axis doesn't obey F the same way XYZ do.


---
**Wolfmanjm** *December 07, 2016 05:02*

ok 1.4 is correct I was approximating






---
**Wolfmanjm** *December 07, 2016 05:03*

it will be sqrt(100² + 100²) / 100 mins






---
**Todd Fleming** *December 07, 2016 05:05*

Ah good!


---
**Wolfmanjm** *December 07, 2016 05:06*

**+Peter van der Walt** link is here [blog.wolfman.com - blog.wolfman.com/files/firmware-4axis.bin](http://blog.wolfman.com/files/firmware-4axis.bin) and notes are here... [https://github.com/Smoothieware/Smoothieware/pull/1055](https://github.com/Smoothieware/Smoothieware/pull/1055)



The notes must be read.


---
**Todd Fleming** *December 07, 2016 05:11*

My guess: may need junction deviation on A to prevent slowdowns 


---
**Wolfmanjm** *December 07, 2016 05:14*

junction deviation is only applied to XYZ I am afraid

also I may be incorrect above I also think only cartesian speed is calculated for XYZ not ABC ABC are auxiliary moves and follow the primary axis XYZ so my first post was correct, A is not part of the cartesian movement. Thi smay be incorrect and may need to change but that would break a lot of other things especially 3d printing.




---
**Todd Fleming** *December 07, 2016 05:19*

Could we have a config file setting that makes A cartesian?


---
**Wolfmanjm** *December 07, 2016 05:23*

it would be a lot of work I am afraid. It will 

break a lot of other stuff. You will need to convince me and arthur that it is actually an issue. I don't think it is actually, but am willing to be proven wrong. I have in the past wanted a way to designate which axis are primary and which are not and I did that in another firmware I wrote which was inherently n-axis. However smoothie is not well designed to be true n-axis. The ABC PR is about 90% of the way there.


---
**Todd Fleming** *December 07, 2016 05:28*

OK, I'll compensate F and S in CAM for every move. What do I do to set speed when only A moves? Will the laser adjust during A's acceleration when XYZ are still?


---
*Imported from [Google+](https://plus.google.com/101442607030198502072/posts/Sqjj72p5Lsx) &mdash; content and formatting may not be reliable*
