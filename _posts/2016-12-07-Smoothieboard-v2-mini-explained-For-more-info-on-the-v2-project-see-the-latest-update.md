---
layout: post
title: "Smoothieboard v2-mini explained. For more info on the v2 project, see the latest update :"
date: December 07, 2016 00:01
category: "General discussion"
author: "Arthur Wolf"
---
Smoothieboard v2-mini explained.



For more info on the v2 project, see the latest update : [http://smoothieware.org/blog:15](http://smoothieware.org/blog:15)

![missing image](https://lh3.googleusercontent.com/-WvYN0fnil4E/WEdRO69t7II/AAAAAAAAOf0/zpPvoa_NxLMDAat2WV5-4yiAwSLVeFktwCJoC/s0/v2-mini-labels.svg.png)



**"Arthur Wolf"**

---
---
**Brent Crosby** *December 08, 2016 04:46*

No output for heated bed?


---
**dstevens lv** *December 08, 2016 08:33*

That's what the SSR is for.  It's a much more effective way to heat a bed than using DC from a FET.


---
**Imants Treidis** *December 08, 2016 12:40*

Hey, Arthur, what are the physical dimensions of the mini board?


---
**Arthur Wolf** *December 08, 2016 12:41*

**+Imants Treidis** About 100x80mm


---
**Brent Crosby** *December 08, 2016 14:44*

So the idea is to use the line voltage (120 VAC) directly in the board heater? Wow. 


---
**Arthur Wolf** *December 08, 2016 14:47*

**+Brent Crosby** Either that ( AC SSR ), or use a DC SSR to do it the "usual" way.

AC SSR control is actually pretty common for "large" beds, I do it all the time. But it also works for smaller beds. It's cheaper than wasting ressources on using a PSU to bring the voltage down for no good reason.


---
**Erik Cederberg** *December 11, 2016 16:18*

I would argue that low-voltage heated beds do have a good reason to exist, since it is safer to wire up something where a hobbyist will not have to worry about getting zapped by mains voltage by accident...


---
**Arthur Wolf** *December 11, 2016 16:35*

**+Erik Cederberg** Hobbyists have to handle mains voltage anyway when wiring the PSU ( on most printers ), this is just some more of that ...


---
**dstevens lv** *December 12, 2016 04:09*

**+Erik Cederberg** On higher end builds AC powered SSR controlled bed heating elements have been the standard for the last couple of years.  Once you get over 200x200 the heat times are considerably longer as well as taking a fair amount more current from your DC psu.  On a 300x300 bed heat times are a couple of minutes (or less) compared to 12-15 min for a 400w 24 volt DC psu.  I someone isn't comfortable with it they should pick another board for their build.


---
**Arthur Wolf** *December 12, 2016 10:35*

Also you can use a DC SSR, then you can get the safety of a DC heated bed even with this board.


---
**dstevens lv** *December 12, 2016 19:34*

Indeed a DC element with the SSR could be used but it would come at greater cost and offer slow heating times or instances where a larger element would have a difficult time reaching 110*C.  I think the small format Cohesion 3D Smoothie based board would be a better fit for those build configs that desire DC powered bed elements.


---
**Arthur Wolf** *December 12, 2016 19:37*

**+dstevens lv** we'll also have an extension board for a mosfet for a heated bed too.


---
**dstevens lv** *December 12, 2016 19:42*

Excellent idea.  The SSR headed bed users could use that board for hotend barrel fan control.  I put that fan on a controlled output so it's not running expect when the hot end is in use.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/4VfLG1A5mdd) &mdash; content and formatting may not be reliable*
