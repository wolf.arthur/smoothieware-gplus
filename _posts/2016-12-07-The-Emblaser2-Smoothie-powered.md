---
layout: post
title: "The Emblaser2 : Smoothie powered"
date: December 07, 2016 00:03
category: "General discussion"
author: "Arthur Wolf"
---
The Emblaser2 : Smoothie powered



[https://vimeo.com/194131846](https://vimeo.com/194131846)





**"Arthur Wolf"**

---
---
**Glenn West** *December 07, 2016 00:33*

Looks really nice. Does it have a camera alignment ? Assembled or kit? Price point 


---
**Arthur Wolf** *December 07, 2016 00:33*

**+Glenn West** Eh, I just realized I don't know the answer to any of those. i'll look it up :)


---
**Glenn West** *December 07, 2016 00:38*

Thanks - it's really difficult to find a smoothie powered laser - everyone does conversions - which is just a hassle or sets a price point so high the conversion is still cheaper. 


---
**Arthur Wolf** *December 07, 2016 00:40*

**+Glenn West** Actually robotseed has a plan to start selling pre-converted good quality chinese lasers. might come sometime in 2017, after I'm not so busy with smoothie2 and fabrica. it won't be as inexpensive as the emplaser though. it'll be "a bit more expensive than a normal chinese lasercutter"


---
**Glenn West** *December 07, 2016 00:46*

That would be great - I loved glowforge but the delays are hurting them  look forward to finding out about the emplaser


---
**Arthur Wolf** *December 07, 2016 00:47*

The emblaser is very different from the glowforge : it's a diode laser, much less powerful. Can still have a lot of fun, but won't cut the same things.


---
**Domenic Di Giorgio** *December 07, 2016 07:16*

**+Glenn West** The E2 is fully assembled, has camera for material alignment, has z-axis laser height control and a bunch of other cool features.

Currently 50% offer running until machines start shipping. See [darklylabs.com](http://darklylabs.com)


---
**Domenic Di Giorgio** *December 07, 2016 07:25*

**+Arthur Wolf** Diode based lasers can basically cut the same things as a CO2 laser except they do not work on transparent materials. They tend to be based around 445nM lasers as opposed to 10knM which allows CO2 lasers to affect transparent materials.

They are less powerful than CO2 lasers which means they take longer to cut through thicker materials and are limited on how thick they can cut. 

They are generally more compact, reliable and less expensive (apart from a cheap Chinese system). This is very attractive to many customers and suits their needs.


---
**Glenn West** *December 07, 2016 07:28*

All cool - can i get it into singapore 


---
**Domenic Di Giorgio** *December 07, 2016 07:30*

Yes, absolutely. We currently ship the Emblaser 1 all over the world.

Feel free to email sales@darklylabs.com if you have any specific delivery questions.


---
**Thomas “Balu” Walter** *December 07, 2016 08:19*

The Fabcreator Fabkit will run on smoothie, won't it?


---
**Glenn West** *December 07, 2016 14:19*

If ordered soon when is delivery 


---
**Bonne Wilce** *December 07, 2016 21:46*

**+Thomas Walter** yup the FabKit from **+FabCreator** Runs on a smoothie 5xc board. so we have the room for some upgrades like rotation axis and a motorised pass through :) 

it is more expensive than the emblaser 2 and a flat pack kit but it is more powerful, faster and larger.




---
**Domenic Di Giorgio** *December 07, 2016 22:39*

**+Glenn West** We are expecting to start fulfilling customer orders from Feb 2017.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/U1karqJa31p) &mdash; content and formatting may not be reliable*
