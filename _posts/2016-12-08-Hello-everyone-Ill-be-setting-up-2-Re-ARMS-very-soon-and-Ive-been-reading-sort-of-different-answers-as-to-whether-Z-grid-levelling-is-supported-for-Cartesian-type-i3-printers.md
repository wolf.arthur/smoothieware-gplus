---
layout: post
title: "Hello everyone. I'll be setting up 2 Re-ARMS very soon and I've been reading sort of different answers as to whether Z grid levelling is supported for Cartesian type i3 printers"
date: December 08, 2016 15:31
category: "General discussion"
author: "Jim Christiansen"
---
Hello everyone.  I'll be setting up 2 Re-ARMS very soon and I've been reading sort of different answers as to whether Z grid  levelling is supported for Cartesian type i3 printers.  This wouldn't be a big deal but I have one printer that has an out of level aluminium hotbed.  It has a slight crown to it in one location that 9 point grid levelling in Marlin totally compensates for.  Can I do this Smoothieware?  Thanks.





**"Jim Christiansen"**

---
---
**Arthur Wolf** *December 08, 2016 15:38*

Yep, see : [http://smoothieware.org/zprobe](http://smoothieware.org/zprobe)


---
*Imported from [Google+](https://plus.google.com/+JimChristiansen/posts/4deCmmzxGX1) &mdash; content and formatting may not be reliable*
