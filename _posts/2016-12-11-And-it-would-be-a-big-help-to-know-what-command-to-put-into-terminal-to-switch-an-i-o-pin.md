---
layout: post
title: "And it would be a big help to know, what command to put into terminal to switch an i-o pin"
date: December 11, 2016 11:26
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
And it would be a big help to know, what command to put into terminal to switch an i-o pin. I tried M42 and M106 without success. Its hard to look for the culprit without being able to methodical search for it :/





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *December 11, 2016 12:51*

To control pins you want to create a switch module for the pin you want to control ( [smoothieware.org - Switch - Smoothie Project](http://smoothieware.org/switch) ). No other method at the moment.

What are you trying to do exactly ?


---
**René Jurack** *December 11, 2016 12:52*

**+Arthur Wolf** trying to figure out, what is wrong ;-) See my other post with the dual hotends...


---
**René Jurack** *December 11, 2016 12:53*

[plus.google.com - I stumble on how to configure multiple extruders with it's heating-switches...…](https://plus.google.com/+ReneJurack/posts/PBrwHvuF53r?iem=4&gpawv=1&hl=de-DE)


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/2RtuK9xT8sc) &mdash; content and formatting may not be reliable*
