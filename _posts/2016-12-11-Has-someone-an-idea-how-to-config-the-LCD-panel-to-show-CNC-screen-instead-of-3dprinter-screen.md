---
layout: post
title: "Has someone an idea how to config the LCD-panel to show CNC-screen instead of 3dprinter-screen?"
date: December 11, 2016 10:20
category: "General discussion"
author: "java lang"
---
Has someone an idea how to config the LCD-panel to show CNC-screen instead of 3dprinter-screen? I have a Reprap-SmartController LCD connected and see the 3dprinter screens(Extruder etc.) but I use smoothieboard for a lasercutter. I also grabbed out the sources and can see that there are two screen-types but can't figure out where to swich/configure the right one...

Many thanks in advance





**"java lang"**

---
---
**Arthur Wolf** *December 11, 2016 12:55*

Yes, you want to use firmware-cnc.bin

If you want to compile it yourself, follow [smoothieware.org - Compiling Smoothie - Smoothie Project](http://smoothieware.org/compiling-smoothie) but do "make CNC=1"


---
**java lang** *December 11, 2016 13:02*

**+Peter van der Walt**  Thank you Peter, I'm working on source level but you gave me the right hint, I've found a comment in the [common.mk](http://common.mk) "define -DCNC if a CNC build", thank you


---
**java lang** *December 11, 2016 14:38*

**+Arthur Wolf** Yes, I checked it, thank you very much




---
*Imported from [Google+](https://plus.google.com/111347667569437072694/posts/9CGKrZ7wYSn) &mdash; content and formatting may not be reliable*
