---
layout: post
title: "I stumble on how to configure multiple extruders with it's heating-switches..."
date: December 11, 2016 10:58
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
I stumble on how to configure multiple extruders with it's heating-switches... Every command I send via a host lets heat all the hotends together instead of a single selected one. What do I do wrong?



For example, this is (at least I think it is) the part of the code, where the magic happens: (I removed unnessessary code lines)



# GENERAL SETTINGS

temperature_control.hotend1.enable				true		

temperature_control.hotend1.designator			T0

temperature_control.hotend1.thermistor_pin		 0.23

temperature_control.hotend1.heater_pin			2.7	

[...]

temperature_control.hotend1.get_m_code			105	

temperature_control.hotend1.set_m_code			104	

temperature_control.hotend1.set_and_wait_m_code	109



temperature_control.hotend2.enable				true		

temperature_control.hotend2.designator			T1

temperature_control.hotend2.thermistor_pin		0.25

temperature_control.hotend2.heater_pin			1.23

[...

temperature_control.hotend2.get_m_code			105	

temperature_control.hotend2.set_m_code			104	

temperature_control.hotend2.set_and_wait_m_code	109



Now, sending either "M104 T0 S200" or "M104 T1 S200" via terminal sets both heaters on. 



terminal reports back while idle:

SENT: M105

READ: ok T0:38.2 /0.0 @0 T1:41.1 /0.0 @0 B:22.7 /0.0 @0 



And when I want to heat <b>only one</b> heater:

SENT: M104 S245 T0

READ: ok T0:45.5 /245.0 @255 T1:47.8 /245.0 @255 B:22.7 /0.0 @0



Until now I tried it manual via terminal, via S3D control panel (clicking the controls) and Repetier Host.





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *December 11, 2016 12:53*

You want to follow this : [http://smoothieware.org/3d-printer-guide#toc33](http://smoothieware.org/3d-printer-guide#toc33)


---
**René Jurack** *December 11, 2016 12:58*

Thanks to the fast repsonse, **+Arthur Wolf** , like always. I did follow your guide and have it setup like recommended. There is a switch for eatch output, a temp-control switch, too. Both have corresponding identifiers and stuff. 


---
**René Jurack** *December 11, 2016 13:00*

I'll get you the config.txt in a minute...


---
**Jeff DeMaagd** *December 11, 2016 13:13*

Try T0 on its own line before setting it. Your command should hypothetically work your way but I've tended to use it on its own line with 3D printers. But I've not done smoothieboard dual yet.



T0

M104 S245




---
**René Jurack** *December 11, 2016 13:16*

here is the config: [http://pastebin.com/HiasrYfT](http://pastebin.com/HiasrYfT)


---
**René Jurack** *December 11, 2016 15:05*

mark the "dual heating problem" solved. I had a hyphen where it shouldn't have been -.- typo ftw...


---
**Jeff DeMaagd** *December 11, 2016 15:50*

Where was the hyphen? To help the rest of us avoid the problem.


---
**René Jurack** *December 11, 2016 15:52*

I had used a hyphen in the extruder module "extruder.hotend_1.xxx" and no hyphen in the temperature_controle module "temperature_control.hotend1.xxx"


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/PBrwHvuF53r) &mdash; content and formatting may not be reliable*
