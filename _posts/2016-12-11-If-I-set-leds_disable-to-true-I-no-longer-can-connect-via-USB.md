---
layout: post
title: "If I set \"leds_disable\" to true, I no longer can connect via USB"
date: December 11, 2016 15:54
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
If I set "leds_disable" to true, I no longer can connect via USB. As soon as I set it back to false, I can connect via USB-host. Is this a bug or a feature? :D





**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *December 11, 2016 16:43*

That is a very weird one. What do the LEDs do when it's not working ?


---
**Arthur Wolf** *December 11, 2016 16:44*

Wait ... that's stupid, realized it <b>while</b> clicking "post" ...


---
**Arthur Wolf** *December 11, 2016 16:45*

Well this is a pickle ... the only way of debugging what's going on is disabled while you are doing the only thing you can do to reproduce the bug ...

Board is azsmz right ?


---
**René Jurack** *December 11, 2016 17:07*

Nope. Its a cohesion3d-Remix. The one with 6 stepper sockets. In order to being able to use the 6th stepper socket, I have to disable the leds, to make free some I/Os


---
**Arthur Wolf** *December 11, 2016 17:28*

Maybe **+Ray Kholodovsky** has an idea ?


---
**René Jurack** *December 11, 2016 17:29*

Yeah, I think so. I was just asking to make sure. I didn't know if this is like it has to be or not.


---
**Wolfmanjm** *December 11, 2016 21:25*

disabling leds cannot affect USB at all. period.

I suspect you either have a config file corruption where the setting in the config file is also affecting some other setting, or there is something weird about the board you are using.

NOTE you cannot entirely disable leds, they are set to output and toggled before it even reads the config, so anything you use for the leds pins needs to be able to tolerate that. disabling leds in config will only stop the leds from toggling once smoothie has booted and configured itself. TBH if there are no leds then it is not a truly smoothie compatible board.


---
**René Jurack** *December 11, 2016 21:28*

Here is the config.txt: [https://www.dropbox.com/s/i175g01sllfzcoq/config.txt?dl=0](https://www.dropbox.com/s/i175g01sllfzcoq/config.txt?dl=0)

If I change the parameter disable_leds, I can't connect via usb anylonger. The board in question is a cohesion3d-remix, handtested by the creator himself...

[google.com - Weiterleitungshinweis](https://www.google.com/url?q=https%3A%2F%2Fwww.dropbox.com%2Fs%2Fi175g01sllfzcoq%2Fconfig.txt%3Fdl%3D0&sa=D&sntz=1&usg=AFQjCNEhzd9umP5MvBQQG69ZsfjkAD6p2Q)


---
**René Jurack** *December 11, 2016 21:30*

And **+Ray Kholodovsky** told me, that I have to disable the LEDs when using the 6th stepper.


---
**Arthur Wolf** *December 11, 2016 21:45*

**+René Jurack** Did you write that config file or was it provided for your Cohesion board ?


---
**René Jurack** *December 11, 2016 21:56*

I wrote that from scratch via the smoothieware wiki and adjusted the needed pins to the ones, **+Ray Kholodovsky** told me to.


---
**Arthur Wolf** *December 11, 2016 21:57*

**+René Jurack** I think it's just too large for Smoothie to handle. Can you try again by taking Smoothie's example config file and modifying it to fit your needs ?


---
**René Jurack** *December 11, 2016 21:58*

I have to say, everything works with 2 extruders and leds on. For the 3rd extruder, I have to disable leds and put 3 pins to dir, enable and steps. Looks like this: [https://www.dropbox.com/s/7vmq765y4vk7yo3/config_3ext.txt?dl=0](https://www.dropbox.com/s/7vmq765y4vk7yo3/config_3ext.txt?dl=0)


---
**Arthur Wolf** *December 11, 2016 21:58*

**+René Jurack** Yeah Smoothie was designed to handle the config file we provide, I think you were just lucky it worked up to that point


---
**René Jurack** *December 11, 2016 21:59*

**+Arthur Wolf** Where is said example config? I looked for one and didn't find it. Thats why I wrote it from scratch via wiki...




---
**René Jurack** *December 11, 2016 22:01*

But, sidenote... You are telling me, that the config file can be too big? o0


---
**Arthur Wolf** *December 11, 2016 22:02*

[http://smoothieware.org/configuring-smoothie](http://smoothieware.org/configuring-smoothie) it's linked to in many places I think you need to read the documentation before moving forward, if you missed this you must have missed a lot


---
**Arthur Wolf** *December 11, 2016 22:03*

**+René Jurack** Yep, we use pretty much all of the chip to it's maximum, there are some limits like that that users don't normally meet. But rewriting config from scratch and making it much larger than the standard one would do it ...


---
**René Jurack** *December 11, 2016 22:04*

That big blue button. Hmm. Ok. Dunno how I oversaw that. But: I read nearly all of the documentation... And I don't ask the simple questions ;-)


---
**René Jurack** *December 11, 2016 22:37*

Ok, I will try again with a more minimalistic config. 


---
**René Jurack** *December 12, 2016 16:33*

So, I am back with a smaller config. Down to 422 lines (95% of the deleted parts were commented out anyways...). Updated to the latest edge-bin, too. leds_disable true is working now like expected and I am still able to connect the board. I am currently in talk with **+Ray Kholodovsky** and he is looking into configuring the 6th stepper.

A question that came up: I saw the remark in the sample-config about only 132caracters per line... Does that take comments into account? Or only parameters without the parameter itself?


---
**Arthur Wolf** *December 12, 2016 16:34*

I believe it includes comments, which is why you see a lot of lines being cut. I could be wrong though.


---
**René Jurack** *December 12, 2016 16:37*

are they simply "cut and ignored" or do I get trouble using extensive long comments?


---
**Arthur Wolf** *December 12, 2016 16:40*

I think too long lines can actually cause trouble yes.


---
**René Jurack** *December 12, 2016 16:41*

ok, thank you :)


---
**Wolfmanjm** *December 12, 2016 19:02*

no they are simply truncated at character 132, so long as it is a comment it should be no problem.


---
**René Jurack** *December 13, 2016 10:43*

Again a response to this topic: **+Ray Kholodovsky** did investigate this issue and found that with the april2016 edge build it does work properly. He tested it with his own config and board and told me to try the firmware out of april. I did so and now everything works :) Even my first "extensive long config with deprecated settings" I showed here before, shows no problems at all. I haven't fully tested it all yet, but every moving and heating I tried with the 6 steppermotors connected worked for me until now. 


---
**Wolfmanjm** *December 13, 2016 19:01*

april was a long time ago and is very ancient and unsupported code. you have been told what the issue is, I suggest you follow the advice given you, or continue to use unsupported old firmware. your choice.


---
**René Jurack** *December 13, 2016 20:55*

**+Wolfmanjm** Even with minimal config it doesn't work with the newest build. So the issue isn't the config. And you didn't told what the issue is, you just said the led-pins should be outputs and thats what they are. Why don't you take this as a helpfull "heavy testing" of smoothieware instead of blaming me using "ancient unsupported code"? I chose smoothie because it is very nice software and I like how it works. I even promote smoothieware everywhere I can. Bringing Smoothieware to it's limits (if that is even the case - I doubt it) should be appreciated.


---
**Wolfmanjm** *December 13, 2016 21:04*

I suggest you get the person you bought the board from to help you then. As you are using a non supported configuration for smoothie.. As I said no leds is not a 100% smoothie compatible configuration. I can't help you anymore.


---
**René Jurack** *December 13, 2016 21:24*

Yeah, I get that you can't support all variants of your open source design. And like I told, the creator of the cohesion remix **+Ray Kholodovsky** did investigate, helped and supported. I don't want to blame, instead I just thought, that it is helpfull for you to know additional info, like "aprils build works". I exaggerate if I say, that updating smoothie firmware does "brick" 3rd-party boards, but that's simply the case here. Noone knows who is the culprit (3rd-party-board or original firmware), but <b>maybe</b> investigating such info from users using smoothie-variants can make smoothieware even better. Anyways, keep up the good work, make smoothieware great and like always, thank you very much for all the info and help provided.


---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2016 22:05*

The issue is pretty simple.  

I have my <b>genuine</b> Smoothieboard 5x here. I have gone ahead and flashed it with the latest edge from 3 days ago, and loaded up a brand new clean config file from the Smoothieware Github.  Plug into USB, LEDs blink, Windows recognizes it, all is good. 

Now let me go ahead and define a 3rd extruder module using some spare pins (2.11, 3.25, and 3.26).  LEDs are still enabled in config, and we're using non status LED pins here. Save, safely eject, and reset the board.  Boom, board will not boot: 3.3v LED is on but L1 - L4 are off and Windows says device not recognized.   

Here is the config file: [https://www.dropbox.com/sh/n02hw02cwaar30e/AACBxSXdyT_JrEAMZNzAv6oOa?dl=0](https://www.dropbox.com/sh/n02hw02cwaar30e/AACBxSXdyT_JrEAMZNzAv6oOa?dl=0)

The same issue occurs on all firmware edge builds I have tried going back to August 2016.  Is there anything else you would like me to do to demonstrate the issue or to help debug? 

![missing image](https://lh3.googleusercontent.com/ZiWNNFQI5Y9A8EwRx3uniFhyW0xt2hFgKDkYL-SMgz_SFfHMM9LGVkib-I4-WzWswCNRFC-SSgDBIeY=s0)


---
**Wolfmanjm** *December 13, 2016 22:11*

maybe config file too big


---
**Wolfmanjm** *December 13, 2016 22:16*

ok **+Ray Kholodovsky** did you read the upgrade notes for edge? Probably not because the exact cause is actually documented there :) sorry it may not be a large config file although that could also be an issue. If you hooked up a serial uart then booted you would actually see what the error was as it should print it out. If you are going to be selling boards I suggest you  learn how to debug some of these issues yourself :) I will not always be around to do it for you. Also reading the documentation helps :)


---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2016 22:21*

**+Wolfmanjm**Ok, let me try that.

Attempt 1:  I have kept the hotend3 switch module in the config, but have set:

 extruder.hotend3.enable                          false

In this case the board boots fine.



Attempt 2:  Now I have gone ahead and removed <b>everything</b> below extruder from config (laser, temp, switch, endstop, panel, and network all gone).  

With all that removed, and hotend3.enable still false, I verify that the board does indeed boot - so I did not screw anything up by deleting those lines. 



Now I will go ahead and set hotend3.enable  true

Board does not boot, usb device not recognized, all same as before.  



Here is that config file: [dropbox.com - Smoothie Clean Config Hotend 3 Defined - Deleted Rest of Config](https://www.dropbox.com/sh/0x13jxe1p2922uz/AABdYdJ__mSY3b6IKFSISuGna?dl=0)



What can I do next to debug?


---
**Wolfmanjm** *December 13, 2016 22:31*

read my last post :)

FWIW I think you need to learn how to hook up the uart and read the section in the wiki about how to use MRI debugging :)


---
**Wolfmanjm** *December 13, 2016 22:32*

That would have solved this issue in 10 seconds for you :) and I wouldn't have needed to be involved at all :) and I wouldn't of pissed everyone off by being grumpy about it :)


---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2016 22:52*

I have found notation #7 in that file now and understand that I will have to recompile the firmware with MAX_ROBOT_ACTUATORS set to 6.



Thank you for helping find the root cause of the issue. 



I will use MRI next time there is a question like this. 



Thanks for your time!

-Ray




---
**Wolfmanjm** *December 13, 2016 23:11*

also note that with 6 actuators there is even less memory available, so small configs are even more necessary. you may encounter out of memory errors during printing as well, so enable as few modules as needed. MRI will determine if you are hitting out of memory issues as well as the mem command.


---
**Ray Kholodovsky (Cohesion3D)** *December 15, 2016 04:06*

**+René Jurack** Give this firmware (latest December edge build compiled for 6 actuators) a shot: [dropbox.com - Smoothie Firmware Dec 2016 6 Axis Build](https://www.dropbox.com/sh/856comgiai59gcr/AADf_TI7JpOs_6uXsvnFImGaa?dl=0)


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/AYAX6dT8ATj) &mdash; content and formatting may not be reliable*
