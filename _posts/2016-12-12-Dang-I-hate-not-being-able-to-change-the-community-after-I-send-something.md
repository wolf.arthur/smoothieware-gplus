---
layout: post
title: "Dang I hate not being able to change the community after I send something"
date: December 12, 2016 13:10
category: "General discussion"
author: "Don Kleinschnitz Jr."
---
Dang I hate not being able to change the community after I send something.





<b>Originally shared by Don Kleinschnitz Jr.</b>



I did some searching for an answer to this on the smoothie site but only found bits of info. 



I am interested in learning what all the functions of the panel are including the "laser" module.



Is there some documentation that explains each  of the panel menu items including the sub-menus? 



I am looking for something that explains the machines behavior when  a menu item is selected or a parameter set.



I am kinda hacking my way through it but found out the hard  way you can slam the head to its extreme if your not careful.





**"Don Kleinschnitz Jr."**

---
---
**Arthur Wolf** *December 12, 2016 15:11*

We don't have any such documentation, users who own a panel just explore the tree and discover it themselves traditionally.

What are you looking for exactly ?


---
**Don Kleinschnitz Jr.** *December 12, 2016 16:07*

**+Arthur Wolf** for example on the "Jog" menu :



... what is MPG mode: it doesn't seem to do anything

... what is the "Park Position" and what is the "Set Park Position" supposed to do. I selected the "Park Position" and it drove the head hard to the lower right and would not release until I powered it off. I was surprised it did not blow the drive or stepper.



If there is no documentation I guess I will just keep "hacking and asking" .... but that may be annoying. I will however document what I find...




---
**Arthur Wolf** *December 12, 2016 16:08*

**+Wolfmanjm** ?


---
**Wolfmanjm** *December 12, 2016 19:04*

Park position is G28 look it up in linuxcnc documentation (it is NOT home) by default unless you set a park position it will be G0 X0 Y0


---
**Don Kleinschnitz Jr.** *December 12, 2016 22:08*

**+Wolfmanjm** X0,Y0 is not the lower right is it? I thought it was lower left?

So then how do you use the "Set Park Position" item, it doesn't allow an entry or anything when you select it.

Is it supposed to execute a G28.1 and store the current position?

Sorry but I am still learning G-code.


---
**Wolfmanjm** *December 12, 2016 22:16*

yes as per the linuxcnc docs it sets the current position to the park position. fwiw most CNC people know what park is especially those using or having used linuxcnc


---
**Wolfmanjm** *December 12, 2016 22:18*

0,0 is usually lower left or front left, but it depends on how you set up homing. if you did not home then 0,0 is wherever the head was on power up (not a good thing)


---
**Don Kleinschnitz Jr.** *December 12, 2016 22:23*

**+Wolfmanjm** I home in the upper left. I thought I homed the machine and then "Set Park Position" but it still went to the lower right. I guess I will test it again. 

BTW is there a motor release function other than from the panel or just powering off the machine for times when I am testing....


---
**Wolfmanjm** *December 12, 2016 22:28*

Yes M84 releases the motors.

Homing upper left is fine that is homing Y to max but it does not change where 0,0 is which should always be front left.


---
**Wolfmanjm** *December 12, 2016 22:29*

this is unless your config is incorrect in the endstops section. check out the wiki [http://smoothieware.org/endstops](http://smoothieware.org/endstops)


---
**Wolfmanjm** *December 12, 2016 22:39*

I also presume you are using the cnc build of smoothie, otherwise G28 will home not park.


---
**Don Kleinschnitz Jr.** *December 12, 2016 22:40*

**+Wolfmanjm** I have it set as 

alpha_homing direction     home_to_min

alpha_min                        0

alpha_max                       200

beta_homing direction     home_to_max

beta_min                        0

beta_max                       200



BTW if I define a max_endstop but nothing is connected will that cause a problem?


---
**Don Kleinschnitz Jr.** *December 12, 2016 22:41*

**+Wolfmanjm** yes it is CNC build.


---
**Wolfmanjm** *December 12, 2016 22:42*

so given your config then 0,0 will be front left. or more accurately 0,200 will be back left and you are saying your Y movement is 200mm


---
**Don Kleinschnitz Jr.** *December 12, 2016 22:52*

**+Wolfmanjm** BTW if I define a max_endstop but nothing is connected will that cause a problem?﻿


---
**Wolfmanjm** *December 12, 2016 23:15*

not unless limit is also enabled


---
**Wolfmanjm** *December 12, 2016 23:16*

but it will try to home to it if the pins are defined


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/QmtRh5fmowM) &mdash; content and formatting may not be reliable*
