---
layout: post
title: "Originally shared by Arthur Wolf Current state of the Smoothieboard v2-mini board design"
date: December 12, 2016 22:16
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



Current state of the Smoothieboard v2-mini board design. Looking good.

Soon we are sending it to the dozen people who offered to review the schematic and layout. Then some more improvements. Then production of a batch for v2 firmware contributors.

![missing image](https://lh3.googleusercontent.com/-Ep_EX0MjjmU/WE8hwKnoXCI/AAAAAAAAOh0/VObk3e8gv8sfbUFs91vexC0WGSXgHzJaQCJoC/s0/slack-imgs.com-3.png)



**"Arthur Wolf"**

---
---
**jinx OI** *December 12, 2016 22:27*

filling the jar in anticipation


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/Bbfy691Npzo) &mdash; content and formatting may not be reliable*
