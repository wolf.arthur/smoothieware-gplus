---
layout: post
title: "Second \"pull request\" on fabrica's github. Added preventDefault to all click events so that location won't be redirected to #"
date: December 12, 2016 12:52
category: "General discussion"
author: "Basile Laderchi"
---
Second "pull request" on fabrica's github.



Added preventDefault to all click events so that location won't be redirected to #.

In configuration.html added classes and click events to the 2 remaining buttons and corrected a misspelling error.





**"Basile Laderchi"**

---
---
**Arthur Wolf** *December 24, 2016 20:59*

Thanks ! :)


---
*Imported from [Google+](https://plus.google.com/+BasileLaderchi/posts/HXY4B6sWyVp) &mdash; content and formatting may not be reliable*
