---
layout: post
title: "What would be an easy way to configure the config.txt of a smoothieboard while it is connected to a raspberry Pi running octoprint?"
date: December 13, 2016 16:20
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
What would be an easy way to configure the config.txt of a smoothieboard while it is connected to a raspberry Pi running octoprint?



I know about the config-set / config-get command, but is there another way? I imagine ( if I could wish) editing the txt-file "in browser" or something like that...





**"Ren\u00e9 Jurack"**

---
---
**Triffid Hunter** *December 13, 2016 16:44*

You can edit it over msd (USB disk) but you unfortunately have to unmount the disk then reset smoothie for the changes to take effect.. I believe there's a reset command in simple shell


---
**Arthur Wolf** *December 13, 2016 16:45*

If you have access to smoothie's web interface some way, you can just download and upload the config file via there. If not, you could use the "gcode upload" function to upload a new config file, but whether that works or not depends on how much octoprint checks what it uploads.


---
**Tiago Vale** *December 14, 2016 11:17*

you can mount smoothie as a drive in raspberry, just don't acess it while printing.









sudo mkdir /mnt/smoothie

sudo chown -R pi:pi /mnt/smoothie

sudo chmod -R 775 /mnt/smoothie



sudo setfacl -Rdm g:pi:rwx /mnt/smoothie

sudo setfacl -Rm g:pi:rwx /mnt/smoothie





sudo nano /etc/fstab

/dev/sda1        /mnt/smoothie      vfat    nofail,uid=pi,gid=pi    0   0










---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/TbRsxtMHcua) &mdash; content and formatting may not be reliable*
