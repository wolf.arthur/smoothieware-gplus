---
layout: post
title: "Hi, I got my first controller running smoothieware today (Cohesion3D)"
date: December 14, 2016 20:56
category: "General discussion"
author: "\u00d8yvind Amundsen"
---
Hi, I got my first controller running smoothieware today (Cohesion3D). I`m going to use mine controlling a CNC milling mashine (external stepper drivers). I`m using Fusion 360 for my designs and plan to use Fusion 360 CAM.

What is the best software to use as a "interface" between Fusion and the controller?



![missing image](https://lh3.googleusercontent.com/-ThT7eYDtbUo/WFGx96Ze2mI/AAAAAAAANPI/-fhL1-qdHP0Dcgb8BQ6Kr8NKOCLhSFqVQCJoC/s0/b01fbb93-280d-47ba-b05b-184c99f9f259.jpeg)
![missing image](https://lh3.googleusercontent.com/-_hwpzD28nwQ/WFGx91CbiAI/AAAAAAAAIC4/Eds0xjUf0BwMHOH8MOUTvsSLUf4G2bZLQCJoC/s0/a1.png)
![missing image](https://lh3.googleusercontent.com/-w5OTeFmYlIs/WFGx9zsFITI/AAAAAAAAIC4/XkmmmKgN9pURABIhcIwJoyYAyChi-eBWwCJoC/s0/15338822_10210250580401458_2272270505905524965_n.jpg)

**"\u00d8yvind Amundsen"**

---
---
**Arthur Wolf** *December 14, 2016 21:05*

You want bCNC


---
**Øyvind Amundsen** *December 14, 2016 21:06*

**+Arthur Wolf** Ok, I`ll test it


---
**Brian Mitchell** *March 09, 2017 17:58*

bCNC works great!


---
*Imported from [Google+](https://plus.google.com/102309741047174439430/posts/1mrwUyMyC2g) &mdash; content and formatting may not be reliable*
