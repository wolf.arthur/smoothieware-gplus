---
layout: post
title: "I am fiddling around with configuring smoothie in more detail for my more complex printer and learned about the limit of temperatureswitch that is: \"If the printer has multiple hot ends, TemperatureSwitch will monitor all of"
date: December 14, 2016 21:38
category: "General discussion"
author: "Ren\u00e9 Jurack"
---
I am fiddling around with configuring smoothie in more detail for my more complex printer and learned about the limit of temperatureswitch that is: "If the printer has multiple hot ends, TemperatureSwitch will monitor all of them and if any one goes over the threshold, will turn on the switch. It will only turn the switch off if all of them are below the threshold temperature."

May I ask, what the reason behind this is?  And what to do, if I want independent temperature-switches? Currently, ALL the different temperatureswitches I have are called, when 1 hotend is hot. So I can't use temperatureswitch for e.g. chamber-control, watercooling motors or other stuff :/





**"Ren\u00e9 Jurack"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2016 21:41*

Post your config please. Are you referring to them as T0, T1, T2?  


---
**René Jurack** *December 14, 2016 21:43*

Yes, Octoprint needs all temperatures except bed to be named as T+figure


---
**Wolfmanjm** *December 14, 2016 22:24*

well that would appear to be a limitation of octoprint not smoothie. smoothie uses the designator to reference temps. it is like that for a very good reason. if you name the heated chamber C for instance then you could control it separately.


---
**Wolfmanjm** *December 14, 2016 23:04*

Tx in reprap is the designator for hotends, B is for beds. Calling all your temperature controls Tx says they are all hotends.

You have two options IMO modify Octoprint to accept other temperature designators in addition to T and B, or modify Smoothie to handle your specific use case (but that is the sub optimal path IMO).


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/Hahhzq5ujEs) &mdash; content and formatting may not be reliable*
