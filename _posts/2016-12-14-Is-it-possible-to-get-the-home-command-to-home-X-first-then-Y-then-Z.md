---
layout: post
title: "Is it possible to get the home command to home X first then Y then Z?"
date: December 14, 2016 15:02
category: "General discussion"
author: "Alex Hayden"
---
Is it possible to get the home command to home X first then Y then Z? Normally I have all move at the same time but I am changing the location of my end stops. Wanted to make sure I could do this.





**"Alex Hayden"**

---
---
**Arthur Wolf** *December 14, 2016 15:22*

sure take a look at the homing_order parameter on the wiki


---
**Topias Korpi** *December 14, 2016 15:22*

I'm not sure if you can configure it so but you could put to your g-code it like this:

G28 X0

G28 Y0

G28 Z0


---
**Alex Hayden** *December 14, 2016 15:44*

Thanks.


---
**Don Kleinschnitz Jr.** *December 14, 2016 17:37*

Took me a minute to find it .....



homing_order	XYZ	   [Optional order in which axis will home, default is they all home at the same time, if this is set it will force each axis to home one at a time in the specified order. For example XZY means : X axis followed by Z, then Y last. NOTE This MUST be 3 characters containing only X,Y,Z or it will be ignored]



[smoothieware.org - Endstops - Smoothie Project](http://smoothieware.org/endstops)






---
*Imported from [Google+](https://plus.google.com/+AlexHayden/posts/dLJyUGeTThe) &mdash; content and formatting may not be reliable*
