---
layout: post
title: "*We need your input as a user, help us !* ( doesn't require any other skill than knowing what a Smoothie is :) ) We want to make labels clearer on the v2 series of boards"
date: December 14, 2016 21:26
category: "General discussion"
author: "Arthur Wolf"
---
<b>*We need your input as a user, help us !*</b> ( doesn't require any other skill than knowing what a Smoothie is :) )



We want to make labels clearer on the v2 series of boards. 

Right now the main power input is called "VBB", and the mosfet power inputs barely even have a name ...



We want to change that main power input to one of the following : 

* POWER INPUT

* 12-24V INPUT

* MAIN INPUT

* VMOT INPUT

What do you think ? Do you have other ideas ?



Now, on the v2-mini things are easy, we only have two connectors to name : 

* A 12-24V input that powers motors and mosfets ( hotend, fan )

* A 5V input that powers logic



But on the v2 and v2-pro things get more complicated, we have : 

* A 12-24V input that powers the motors only

* A 12-24V input that powers the mosfets ( hotend, bed, fan )

* A 5V input that powers logic



So that's 5 possible options to name. What are your ideas on naming all of these so that they are easy to understand for newcomers looking at their board, but also reasonably short so they fit on the boards ?



Thanks y'all ! :p





**"Arthur Wolf"**

---
---
**Thomas Herrmann** *December 14, 2016 21:38*

Voltage plus mot, mos, log ? Eg 12VMos


---
**Brian W.H. Phillips** *December 14, 2016 21:41*

12/24 power input 

12/24  m. drive input 




---
**Maxime Favre** *December 14, 2016 21:51*

Will check that tomorrow


---
**Stephen Baird** *December 14, 2016 22:12*

I'd favor 12-24v Input as a starting point. It's clear and doesn't make you flip back to reference material if you want to know the acceptable input ranges. 



For the v2-pro, I think 12-24 Motor-In and 12-24 Aux-In would be descriptive yet still short enough to fit. Aux-In could be a little ambiguous, but clearly differentiates from the motor input and remains descriptive. 



The logic power could be 5v Logic-In or just 5v In, keeping the same naming structure as the higher voltage input while remaining short and descriptive. 


---
**Dont Miyashita** *December 15, 2016 02:33*

V2-mini

12-24V IN

5V IN



V2 pro

12-24V MOTOR IN

12-24V HEATER IN

5V IN


---
**juergen pflug** *December 15, 2016 05:01*

V2-mini

12-24V IN

5V IN



V2 pro

12-24V Stepper or Motor IN

12-24V IN or 12-24 AUX IN

5V IN


---
**Thomas “Balu” Walter** *December 15, 2016 06:59*

Just adding something to the brainstorming pool. 

12-24 Vin

5 Vin



12-24 Vin MOT

12-24 Vin AUX

5 Vin


---
**Bouni** *December 15, 2016 08:58*

**+Arthur Wolf** Is there a reason why you put a separate 5V Input on the board? I would consider using a VIN -> 5V LDO on the board. They are available with a wide input range an only require a small cap on the input and output side and that's it.


---
**Arthur Wolf** *December 15, 2016 09:56*

**+Bouni** On the v2-mini there is a spot for a LDO but it's not populated by default ( cost reduction ). on the v2 and v2-pro there is one on there by default, but there is still a 5v input also.



Everybody else : Thanks it helps a lot, keep them coming :)


---
**Chris Brent** *December 15, 2016 19:32*

I like using STEP or MOTOR on the first input as that applies to most use cases. AUX or MOS seems like a good idea for the second set as you could be running a hotend, bed, spindle, laser, etc, etc, etc. Smoothie is so flexible I don't think you'll ever get it quite right for every use case, but that's fine too.


---
**Marc Miller** *December 17, 2016 18:00*

I agree with **+Thomas Walter**​'s labeling. 


---
**Jérémie Tarot** *December 20, 2016 08:40*

I too second **+Thomas Walter**​ schemes, just favouring MOS over AUX as it seems more exact and educative... Hmm, but not so sure on a second thought


---
**Mark Ingle** *January 15, 2017 22:27*

For V2 and V2-Pro go with MOTORS and AUX respectively




---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/HyXv5PsNyqF) &mdash; content and formatting may not be reliable*
