---
layout: post
title: "I need some advice for my Smartcontroller panel: I want to add some \"quick access\" hardware-buttons and assign panel-functionality to it, for instance Button1->reset motors, Button2->repeat the last job and so on"
date: December 15, 2016 14:08
category: "General discussion"
author: "java lang"
---
I need some advice for my Smartcontroller  panel: I want to add some "quick access" hardware-buttons and assign panel-functionality to it, for instance Button1->reset motors, Button2->repeat the last job and so on. What would be the best method? My first attempt is to use some free pins and do the rest in the firmware, but this is not so funny. Or does smoothieware support such external buttons in any way?

Thanks in advance, Johann.





**"java lang"**

---
---
**Wolfmanjm** *December 15, 2016 22:45*

read the wiki... [http://smoothieware.org/switch](http://smoothieware.org/switch)


---
**java lang** *December 15, 2016 22:48*

**+Wolfmanjm** OMG how easy. I red the wiki-switch often but I've overseen the output_on_command line. Thank you very much.


---
*Imported from [Google+](https://plus.google.com/111347667569437072694/posts/FVac1NuXoVx) &mdash; content and formatting may not be reliable*
