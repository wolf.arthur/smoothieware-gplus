---
layout: post
title: "Top and bottom PCB for the current version of Smoothieboard v2-mini-alpha As you can see we tried to put as much information as possible on the silk layer, and to make it as easy to understand as possible"
date: December 16, 2016 23:33
category: "General discussion"
author: "Arthur Wolf"
---
Top and bottom PCB for the current version of Smoothieboard v2-mini-alpha

As you can see we tried to put as much information as possible on the silk layer, and to make it as easy to understand as possible.

If you have ideas on how to improve it, or if you can spot errors, please comment here !



![missing image](https://lh3.googleusercontent.com/-vYNqWOh22Sc/WFR53B2_0-I/AAAAAAAAOjM/6ytah7Tar3IE-rTqQtAXRPDedEAMSOA6ACJoC/s0/Smoothie2Mini-pre20-3d-top.png)
![missing image](https://lh3.googleusercontent.com/-gwFhac_YE50/WFR53BDYssI/AAAAAAAAOjM/KvalOZ62r_oRANLFZX1VzqkYr7Vnk7OGQCJoC/s0/Smoothie2Mini-pre20-3d-back-bottom.png)

**"Arthur Wolf"**

---
---
**Alex Krause** *December 17, 2016 03:09*

Is the W in the OSHW going to be copper or tinned/soldered like **+Peter van der Walt**​ did on his OpenBuilds board?


---
**Dont Miyashita** *December 17, 2016 05:10*

Sorry I didn't follow the project closely so I want to ask what USB-A and USB-B do? One should be connect to computer? But what does the other do, USB thrumb drive storage or does it also support Wifi dongle?



Edit : Okay I found other image of the board it is USB storage then.


---
**Arthur Wolf** *December 17, 2016 10:00*

**+Alex Krause** I believe it's going to be copper.

**+Dont Miyashita** USB-B goes to the computer, USB-A goes to a thumb drive, yes.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/VE3zWyTc2Ac) &mdash; content and formatting may not be reliable*
