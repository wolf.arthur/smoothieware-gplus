---
layout: post
title: "I'm wanting to build a plasma cnc run by smoothie"
date: December 21, 2016 01:21
category: "General discussion"
author: "giavonni palombo"
---
I'm wanting to build a plasma cnc run by smoothie. The reason for smoothie is I want to can over mt router as well and would like the same hardware. Just to make it easier on me plus all the features. I have a few questions, I tend to confuse myself the more I read.

I would like my work flow Vcare or Corel to Sheetcam. Sheetcam can do, pierce height, piercing delay time, lead in and lead out, kerf width, cut order. I know it capable of what a plasma needs to cut correctly. 



What Post processor would work with smootie? Something like UGS? Or do I need to go Mach3 and a Mach3 BOB?



Advice is welcome.



thank you in advance





**"giavonni palombo"**

---
---
**giavonni palombo** *December 29, 2016 14:59*

Nobody? Can I use Mach3 with smoothie?


---
**Aaron Pence** *April 09, 2017 15:47*

no mach3 support 



[smoothieware.org - cnc-mill-guide [Smoothieware]](http://smoothieware.org/cnc-mill-guide#cnc)



Sheetcam g code yes don't know on post processor

sorry


---
**Aaron Pence** *February 24, 2018 23:31*

router plasma 

![missing image](https://lh3.googleusercontent.com/EVspLSl4VhfSNR_A1e5h6Px_-nNv5P2Xj5oAiduqyXbm5gPOHNrPg_InGdbeh3hnoWkRFVfPuagQ5vtzP7pSMgJsQUa6eTpcDCI=s0)


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/AJmf47MSEwy) &mdash; content and formatting may not be reliable*
