---
layout: post
title: "Hello. I have a early Smoothieboard x5 from the kickstarter and it came without Ethernet contact"
date: December 22, 2016 09:57
category: "General discussion"
author: "Terje Moe"
---
Hello. 

I have a early Smoothieboard x5 from the kickstarter and it came without Ethernet contact. Now the Hanrun Ethernet connector seems to be obsolete so how do I connect my board to the network /Internet? Can I use Waveshere lan board: [http://www.waveshare.com/lan8720-eth-board.htm](http://www.waveshare.com/lan8720-eth-board.htm).



If so how and where would I connect it?



Terje 





**"Terje Moe"**

---
---
**Arthur Wolf** *December 22, 2016 10:05*

You would have to rdesolder the USB connector and solder it on the Smoothieboard.

The connector is not obsolete, it's just rare enough that right now you can't purchase it in single quantities anywhere, but we are still able to have them soldered on smoothieboards at our factory for example.


---
**Terje Moe** *December 22, 2016 20:42*

Thanks Arthur. 

Well I did contact Uberclock, and asked if they could source one and maybe solder it on, that with the Rekon 5V DC/DC converter, but they could not help at this point. I would be very happy if I can send it in with the Rekon DC/DC converter and get both soldered on. Would that be possible? I will use a 24V  power supply.



Thanks 



Terje 


---
**Arthur Wolf** *December 22, 2016 20:44*

**+Terje Moe** The problem is they don't have any Ethernet connectors alone, since they have been pre-soldered to the Smoothieboards for so long.

If you email me at wolf.arthur@gmail.com, maybe I can find you a board that is completely dead, then you can de-solder the Ethernet connector, and re-solder it on your board, would that work ?


---
**Jeff DeMaagd** *December 23, 2016 07:54*

There's a lot of the HR911105A connectors on eBay. I'm pretty sure the HanRun connectors with sticker labels are knock-offs. And I wouldn't be surprised if the ones that show the proper etched housing will ship ones with the sticker as a bait and switch. They might still do the job well though, the one I tore apart appeared to have the proper magnetics in it.


---
**Terje Moe** *December 24, 2016 12:10*

So Santa has a suprice for me this year, I e-mailed Hanrun and they will send me some samples. I will forward them to you Arthur so you can put them to use where they are most needed.



Happy Christmas


---
**Terje Moe** *December 27, 2016 12:07*

Wow, I most say, thank you Hanrun! My samples arrived today with DHL.  Now to find someone that can solder it in for me, this and th 5v DC /DC converter. 

![missing image](https://lh3.googleusercontent.com/lJgFQheuRqYvoJu025fOb4O_lDgmBXbFagD0NKCZsItkGySUTq-Ei0x_TnX8w_7yZCjqBWV5J7nmgA=s0)


---
*Imported from [Google+](https://plus.google.com/+TerjeMoe_Temo/posts/ZSTTxXf92Xr) &mdash; content and formatting may not be reliable*
