---
layout: post
title: "At some point in the past did Smoothieware treat % as a line comment?"
date: December 24, 2016 02:00
category: "General discussion"
author: "Steve Prior"
---
At some point in the past did Smoothieware treat % as a line comment?  I ran across a Fusion 360 CAM post processor which I assume worked at some point, but now fails over lines which just contain a %.  I'm hacking the previous post processor to get it to work with my K40 with Smoothieboard so I can use Fusion 360 with it.





**"Steve Prior"**

---
---
**Wolfmanjm** *December 24, 2016 02:11*

% has never been used as a comment in smoothie.


---
**Steve Prior** *December 24, 2016 02:15*

Freaky, the post processor I started from claims to have even been updated to support the laser CAM operations, but it has two lines with %'s.  Can't imagine what the story is behind that.


---
*Imported from [Google+](https://plus.google.com/+StevePrior/posts/DmPGkBKfMFa) &mdash; content and formatting may not be reliable*
