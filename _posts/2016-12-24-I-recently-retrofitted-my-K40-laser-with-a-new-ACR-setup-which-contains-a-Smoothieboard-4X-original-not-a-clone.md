---
layout: post
title: "I recently retrofitted my K40 laser with a new ACR setup which contains a Smoothieboard 4X (original, not a clone)"
date: December 24, 2016 02:05
category: "General discussion"
author: "Steve Prior"
---
I recently retrofitted my K40 laser with a new ACR setup which contains a Smoothieboard 4X (original, not a clone).  It came running a firmware from April 2016, but when I flashed it with Edge firmware from Github built in December the board seems to crash after I send a G28 to the point where the COM port disappears from my Windows 8.1 machine.  But when I flashed with Smoothieware's Master branch using the exact same Config file it seems to work fine.  Has anyone run into something similar?



Also, I don't have an LCD on the controller, should I be using the CNC flavor of Smoothieware or the regular one for the K40 laser conversion?





**"Steve Prior"**

---
---
**Wolfmanjm** *December 24, 2016 02:09*

you should use the cnc firmware. and note that G28 is not home in the cnc firmware it is $H same as in grbl. G28 will move to park position. also read the upgrade notes on github.


---
**Steve Prior** *December 24, 2016 02:50*

In both cases above I was not using the CNC version.  I used the latest config file as the base config and modified it appropriately.  Any idea why the Edge version would be crashing and the Master would be OK?


---
**Steve Prior** *December 24, 2016 02:51*

The readme talks about an LCD being required for the CNC version - is this real?  Or do I just need to have the LCD module enabled in the config even if I don't have one?


---
*Imported from [Google+](https://plus.google.com/+StevePrior/posts/V8xXshKMzPc) &mdash; content and formatting may not be reliable*
