---
layout: post
title: "Just stumbled on this board whereas I can't remember seeing it mentioned in this community !?"
date: December 26, 2016 17:38
category: "General discussion"
author: "J\u00e9r\u00e9mie Tarot"
---
Just stumbled on this board whereas I can't remember seeing it mentioned in this community !?



Is it a known clone ? Is it a fair one ?





**"J\u00e9r\u00e9mie Tarot"**

---
---
**Jérémie Tarot** *December 26, 2016 17:40*

Same here from same vendor, for english speaking crowd



[reprap-3d-printer.com - eMotronic board &#x7c; Reprap 3D Printer](https://www.reprap-3d-printer.com/product/1234568620-notranslation)


---
**Arthur Wolf** *December 26, 2016 17:53*

**+Jérémie Tarot** They say it is GPL, but they don't publish the source on their github, and only provide the schematic as a PDF.



Also, I really don't like that company, it has for years abused it's "reprap france" name by confusing newbies into thinking they had some official connection with the Reprap project ( we see quite a few of those confused users on the french reprap irc channel ). They take advantage of this not only via their domain name but also in their adword ads.

When confronted on the issue they essentially politely tell people they'll think about it but then never do anything.

They are essentially profiting off reprap's work and know-how, but the very first printer they designed themselves, they didn't release as Open-Source.

I ( and quite a few others in the the french reprap community ) have been upset at that company for a long time before they released this board, so it has nothing to do with the board itself.


---
**Jérémie Tarot** *December 26, 2016 19:45*

**+Arthur Wolf** damn, another rogue one :(

Thanks for clarification 


---
**Arthur Wolf** *December 26, 2016 19:49*

**+Jérémie Tarot** Well with Smoothie being so popular and so awesome, it's to be expected. I just hope the Open-Source community will be able to fight to keep the people who actually contribute new and cool things around.


---
**Jérémie Tarot** *December 26, 2016 20:04*

**+Arthur Wolf** been fighting for FOSS for 17 years already and extended the battlefield to FOSHW for a handful and it raised new challenges like the copycat plague... Anyway, let's keep on advocating and educating the people ! 


---
*Imported from [Google+](https://plus.google.com/+JérémieTarot/posts/18hMimRNcXf) &mdash; content and formatting may not be reliable*
