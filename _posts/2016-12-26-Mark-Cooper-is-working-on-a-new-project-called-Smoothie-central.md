---
layout: post
title: "Mark Cooper is working on a new project called \"Smoothie-central\""
date: December 26, 2016 22:05
category: "General discussion"
author: "Arthur Wolf"
---
**+Mark Cooper** is working on a new project called "Smoothie-central".

It's essentially a board with large logic connectors all around, no power, and some neat "large machine"-oriented features, for large lasers, CNCs and 3D printers that only use "external" stepper drivers and other peripherals.



You can see a basic connector layout in the attached picture.



And the draft of the specification at [https://docs.google.com/document/d/1PKwoEB4zZQWxT4tJm48W-1_lBEOAGzk7noKMVmRszs4/edit?usp=sharing](https://docs.google.com/document/d/1PKwoEB4zZQWxT4tJm48W-1_lBEOAGzk7noKMVmRszs4/edit?usp=sharing)



It's still a very young project, but we are pretty exited about it. 

If you have ideas, comments, improvements, or you think it sucks, comments are welcome.



Cheers :)

![missing image](https://lh3.googleusercontent.com/-D4EGo7HPWNI/WGGUFEroVrI/AAAAAAAAOmo/j-36T9HrhD0GdnfermzA4BbZTOwxpHA5gCJoC/s0/SmoothieCentral-pre0.png)



**"Arthur Wolf"**

---
---
**Stephanie A** *December 27, 2016 01:25*

5mm connectors for low current is absolutely unnecessary, and quite wasteful. The only reason to have larger connectors it to handle more current.


---
**Erik Cederberg** *December 27, 2016 02:37*

**+Stephanie S**: I disagree, 5mm connectors is very handy when it comes to wire stuff neatly in a large electrical cabinet. Preferably also the pluggable terminal block type.



For example, the control box for the machine i am working on right now is about 1400x900x300mm (yes, the control box is larger than most peoples machines) since i have a bunch of large servo drivers and power supplies in there, 3 or 5mm pitch of the connectors really make no difference to the size, but it makes it much easier to wire it neatly.


---
**Ray Kholodovsky (Cohesion3D)** *December 27, 2016 04:41*

**+Erik Cederberg** looking forward to seeing it :)


---
**Arthur Wolf** *December 27, 2016 11:18*

**+Stephanie S** It's easier to manipulate/use larger connectors. It's in particular something that is neat for new users, but it's also a common practice in the industry in general. In fact I've transformed a lot of routers and laser cutters to Smoothieboard, and no matter if they are chinese or from the west, it's very rare the controller I replace has something smaller than 5mm for connectors.

For having spent hundreds of hours my head in electrical cabinets for routers and lasers, I really really would hate it if the connectors were smaller.


---
**Stephane Buisson** *December 27, 2016 11:35*

When you are getting old, and your sight is going down, you will really appreciated. 

stronger connectors is also a +


---
**Mike Creuzer** *December 28, 2016 02:46*

Coolant monitoring?  Interface to the existing display panel the lasers come with? 


---
**Arthur Wolf** *December 28, 2016 10:05*

**+Mike Creuzer** The "Safety" input is for door/PSU/coolant monitoring.

We don't plan to support any of the display panels the lasers come with, those are very very closed, don't have any kind of standard interface, so it'd pretty much be less work to do our own ( which we plan to do ), than trying to talk to them.


---
**Mike Creuzer** *December 28, 2016 14:42*

I've had my laser for 2 weeks now. One of the blue 50w from China units.  



The laser "on" light is tucked away in the side where you can't see it. I am going to add a "laser on" light. 



I need to add an ameter as these are easy to over drive the laser. 



I have little kids so I also want to add a lid switch that I can enable/disable somehow.  I physically unplug it when I am done with it right now. 


---
**Arthur Wolf** *December 28, 2016 14:43*

**+Mike Creuzer** you definitely want a door switch on laser cutters


---
**Mike Creuzer** *December 28, 2016 14:53*

I also have a Chinese CNC router. I've had it maybe 5 months but only have about 10 hours using it thus far. 220v liquid cooled spindle. 



The frequency drive looks like it has a bunch of control inputs for speed, etc that the current controller doesn't interface to. Is there enough I/O for this board to connect to these inputs? 



The frequency drive overheats easily.  Partly poor design of the case and partly I am not using it well yet. 



I really like the "trace" button on the laser.  I haven't found something like that on the router. 


---
**Arthur Wolf** *December 28, 2016 14:55*

**+Mike Creuzer** On routers, we'll have a RS485 interface which is the best way to talk to them ( most immune to noise ), but yes you could just use pins to control everything.


---
**Stephane Buisson** *December 28, 2016 15:03*

**+Mike Creuzer**

[smoothieware.org - Spindle Module - Smoothie Project](http://smoothieware.org/spindle-module)

also google "smoothieboard  bouni vfd"


---
**Boyan Silyavski** *December 31, 2016 01:37*

Hi, a  short friendly advice from the point of view of retrofitting or building new CNC, without anything personal, just trying to help you here. Forget for a sec about the 3d printer guys and see what we need here for CNCs:

24vdc all around wherever possible , end stops, etc. 5v? - Mehh



-That would be the deal breaker, the 24VDC or 5VDC. Me and other people would not even consider it if its less than 12VDC

-differential signal possibility via jumper or whatever 

-All inputs and outputs selectable Active High or Low via software or jumpers

-dont get ideas from crappy board but see CSMIO and do the same

-Analogue 0-10VDC for spindle. Even better 2x. Even better selectable and controlable from software 0-5 and 0-10VDC. Even better adjustable  0.1VDC and so on. I have 150Euro chinese offline controller that does that perfectly

-Relay out puts or a way to interface with relay board / 24VDc again



If board has not that what i am suggesting,  nobody will ever hear about it on CNC forums and i personally will be the first one to recommend against it. There is very serious competition and standards are much higher than an year back.  On forums where i am participating actively, me and some people are even actively starting to Investigate and recommend Chinese offline boards which are very evolved by the way and in an year or 2  will take the rest of the market from Mach3, 4 and similar. believe me or not.  I have not looked what max frequency will output your new board per axis, but FIY my machine is running now on 400MHz per axis controller and i would not consider anything less.  I am happy to provide more info on why this or that,  etc.

Again CSMIO is the golden standard there


---
**Arthur Wolf** *January 04, 2017 09:30*

**+Boyan Silyavski** 

* The board is 12-48VDC, so you can use 24V if you want yes. Several inputs are 48VDC-tolerant so you can use them the way you want.

* Differential signaling will be available via an extension board on the v2 version. We are not making it standard for cost reduction reasons, but you can get it if you want.

* AH/AL is something Smoothie already supports in firmware, has for a long time

* We won't have a 0-10VDC signal for spindles, but we'll have a RS485 output, which is differential and tends to work even better. There will also be a 0-5VDC output that can be used if you really want it.

* We'll have relay boards as extension boards, yes.



I'm not sure what you mean by «400Mhz per axis», that sounds like a weird number here.


---
**Boyan Silyavski** *January 04, 2017 10:40*

Great then. It seems you are on the right track.  By the xxxHz I mean that each axis should be able to output enough frequency to move at least 2048ppr-2500ppr encoder motor at 3000rpm and that coupled  1:1  1:2  1:3 to xx05 xx10 screw to give a final result resolution better than 0.01mm . I meant kHz not mhz, sorry


---
**Arthur Wolf** *January 04, 2017 10:44*

**+Boyan Silyavski** Current Smoothieboard v1 can do 100khz ( we are very rarely asked for more, even on CNC mills/routers, most drivers don't even accept more than 250khz ). v2 will be able to do 500khz, and v2-pro will do 2-5Mhz.

Really this board is based on a lot of actual feedback from users telling us their needs for large CNC router or lasercutter conversions, so I think it's going to be useful to a lot of people, even if making a board that is useful to <b>everyone</b> is pretty much impossible.


---
**Boyan Silyavski** *January 04, 2017 22:05*

I still wonder why only 4 motor drives on main board?  Especially having in mind its a printer oriented first and foremost. I would have done it 6 empty slots without drives at lower price or 6 drives and 2 empty slots.  For example i am looking into this board for driving my custom printer. 2 motors on  Y, 2 on Z, 1 on X, plus 1 extruder at least, better 2. Thats 6+ drives needed.  Is not this board aimed at people doing expensive custom builds? I know i can add motor drives, but at what cost would be one additional drive? Total cost seems too much then. Plus all will  become a mess  of cables. Not to speak of that i expect of a board thats near 200$ to be fully shielded and industrially looking.  Why would i buy the Smoothie for a CNC if there is the

UC300ETH-5LPT , UC400ETH ethernet motion controller plus software for that price? And the Pokeys57CNC. All of them for around 120euro each and capable of 8 axis control?  And much more mature and community accepted. I am not judging or provocating, just trying to point  you in the right direction.  Just thoughts. 






---
**Arthur Wolf** *January 04, 2017 23:42*

**+Boyan Silyavski** Are you talking about smoothie-central or another board ?

If you are talking about this board ( smoothie-central ), it has connectors for 5 external drivers, not 4. Machines that need more are extremely rare in our current and planned userbase, and there are enough auxilliary outputs to allow for a 6th if you really need it.

Also, this board is not primarily for CNC routers, and laser cutters, with the option of using it for large 3D printers.



About double Z, if we are talking external drivers ( like on Smoothie-central ), that's easy to wire in parallel.

If you are talking Smoothieboard v1 or v2, for Z axis you can just wire the stepper motors in series or parallel, it's what most users do and it's very largely enough. So you still only need one driver for Z. If you really want two drivers for Z, wiring an external driver is trivial ( but as I said it's very rare ).



About shielding, Smoothie-central will come with a folded-metal box.



About "why would I buy it", I think you need to look more into what it does and how it does it, there are some very good reasons Smoothie is getting this popular.

Just the fact it's Open-Source is enough for lots of people, but that's not all that is great about it. It's easy to use, very easy to extend, easy to setup, and well documented. And it's the best option around if you are going to need to add any functionality to it that doesn't exist anywhere around.

Smoothie is <b>way</b> smarter than a UC400ETH, it's a completely different product really.



I'm sure it's possible in your specific case a UC400ETH is the best option. If it is, you are definitely not our target audience, and we are not going to be doing anything to try to convince you :)


---
**Boyan Silyavski** *January 05, 2017 00:36*

Thanks for the answer. On the contrary to what  you may think, you had me convinced.  especially because its an open source. Which is the real deal for me.  I really believe Open Source is the future. I  just bought all elements save the boards, for 2 prototype printers iam working on, I will be waiting to receive all and hopefully bythe time i am done the V2 board will be ready? When you are expecting to have it  done?


---
**Arthur Wolf** *January 05, 2017 09:43*

Because v2 is made by volunteer contributors, we can't plan when it will be ready, so there is no timetable.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/cqzPL4xJTP1) &mdash; content and formatting may not be reliable*
