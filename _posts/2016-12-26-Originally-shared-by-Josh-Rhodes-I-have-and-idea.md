---
layout: post
title: "Originally shared by Josh Rhodes I have and idea"
date: December 26, 2016 06:38
category: "General discussion"
author: "Josh Rhodes"
---
<b>Originally shared by Josh Rhodes</b>



I have and idea. I'm submitting it here to you capable folks because I do not feel that I have the resources to take advantage of it. 



It's and idea to build a simpler rotary 4th axis for cnc, with a fraction of the parts. 



It works kind of like an automatic tool changer, you would move the carriage to a certain place then the z axis would actually operate the A (rotary) axis position. There would need to be some kind of linkage between the carriage and the A axis. 



I'm not sure precisely what that would look like. Maybe a star disk (5 or 6 lobe). The Z would go down to the bottom then have to come back again (several times) if you wanted to do 360deg. 



You would need a few more pieces though.



The A axis would need to stay in precisely the position it is placed, for that it needs some sort of brake. I would immediately think a disk brake from a bike would be a good way to go, but there are many ways to skin that cat. There would need to be an actuation for this brake, such as a solenoid or you could use a stepper or servo (giving up the lower complexity). An encoder wouldn't be a bad idea either. 



Then there would be the software. 

I will post this over to Smoothieware and CNCWeb communities as well to see if anyone wants to give it a whirl. 













**"Josh Rhodes"**

---
---
**Arthur Wolf** *December 26, 2016 07:45*

This would work. However, what makes a A axis expensive is the metal and bearings you need to make it strong enough for machining. Your solution only saves the stepper motor itself, which for a A axis is $10-$20.

I think this is a lot of effort for very little benefits ( and at the same time risking to loose a lot of precision, depending on exactly how it is implemented )


---
**Josh Rhodes** *December 26, 2016 07:51*

**+Arthur Wolf**​​ you forgot about all the electronics and wiring involved. Specifically the driver. 



Perhaps you're right.. 



I'm surprised that in all the diy 4th axis setups I've looked at online. I've only seen one with a brake, so that the stepper doesn't have to do that job by itself..


---
**Arthur Wolf** *December 26, 2016 07:56*

**+Josh Rhodes** My $10-$20 price point included that ( though it could be more on larger machines )



Steppers tend to be the least expensive solution to any problem. Including braking, where using a larger stepper is less expensive than adding a brake.


---
*Imported from [Google+](https://plus.google.com/102698747825507444610/posts/Tq21oUtSaxf) &mdash; content and formatting may not be reliable*
