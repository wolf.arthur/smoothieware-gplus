---
layout: post
title: "Salut  tous, Hope yall had a very nice xmas and your head and stomac aren't making that day one of the hardest of the year !"
date: December 26, 2016 14:01
category: "General discussion"
author: "J\u00e9r\u00e9mie Tarot"
---
Salut à tous,



Hope yall had a very nice xmas and your head and stomac aren't making that  day one of the hardest of the year !



Now for the question, I was wondering if there could be any interest in [smoothieware.org](http://smoothieware.org) french translation ? If there does:

   * I'd gladly contribute the [smoothieware.fr](http://smoothieware.fr) domain for the first years if desired

   * Wikidot Handbook has an extended and documented technique to handle multilingual wikidot sites explained here [http://multi-lingual.wikidot.com/](http://multi-lingual.wikidot.com/), as well as [http://handbook.wikidot.com/en:magic-language-navigation](http://handbook.wikidot.com/en:magic-language-navigation), [http://handbook.wikidot.com/en:translations](http://handbook.wikidot.com/en:translations), and [http://handbook.wikidot.com/en:add-another-language.](http://handbook.wikidot.com/en:add-another-language.).. Should we walk in their steps ?

   * I'd translate pages according to my documentation study









**"J\u00e9r\u00e9mie Tarot"**

---
---
**Maxime Favre** *December 26, 2016 14:03*

I can help :)


---
**Arthur Wolf** *December 26, 2016 15:38*

Hey.



We have a group of students working on translating the 3D-printer guide. 

If you are ready to help them, please email me at wolf.arthur@gmail.com and I will put you in contact.



I don't think translating the complete site would be a good idea, it would be impossible to maintain up to date, so it would cause more trouble than good. But maintaining the 3D printer guide would be manageable.



Cheers.


---
**Christian Lossendiere** *December 26, 2016 17:06*

Bonjour



I can help for CNC Mill guide translation


---
**Arthur Wolf** *December 26, 2016 17:20*

**+Christian Lossendiere** **+Jérémie Tarot** **+Maxime Favre** Please contact me about this at wolf.arthur@gmail.com


---
**Jérémie Tarot** *December 26, 2016 17:35*

**+Maxime Favre**, **+Christian Lossendiere** happy to see canditates for onboarding :)



**+Arthur Wolf** great to know there are already forces engaged on a part of this endeavour :) Browsing the site for a good part of the day, I can only agree that translating the whole content is a significant amount of work. The only way I can estimate content volatility is looking at pages history, but this doesn't give clues wether it's up to date or not. Maybe there are parts more subject to changes than others ? Maybe some contents are more important to translate than others, and the guides are surely ones of top priority.



Better than an all or nothing project, I'd rather discuss the order of importance of the pages for french speaking people to get to know smoothie firmware and smoothieboard, then get their hands on it, and make them work for their application (guides).



There still are so much places and peoples in the country that could dump the smoothie option just because they can't or suffer to much reading english. For example, I'd have all pains to evangelize my local FabLab without french documentation.



Main question is, do we risk to make more aim than good with partial translation ? Then, is the documentation changing so much so often ? Finally, would you accept these contributions anyway, or would you rather have them on a brother site ?



cheers






---
**Arthur Wolf** *December 26, 2016 17:39*

**+Jérémie Tarot** I think we should start with translating the guides ( 3D printing first ), try to maintain that, and see where we go from there. It's already a huge project, no reason to look past that at the moment.

The translated guides would be on the wiki, in a special section.


---
**Jérémie Tarot** *December 26, 2016 18:14*

OK, what about the "fr:" category as a "special section", following wikidot handbook methodology ?


---
**Arthur Wolf** *December 26, 2016 18:32*

That works


---
*Imported from [Google+](https://plus.google.com/+JérémieTarot/posts/L7Qcjhk9j4c) &mdash; content and formatting may not be reliable*
