---
layout: post
title: "Also wondering how this is supported by our beloved firmware and the pros and cons of using such drivers with Smoothieware ?"
date: December 30, 2016 10:15
category: "General discussion"
author: "J\u00e9r\u00e9mie Tarot"
---
Also wondering how this is supported by our beloved firmware and the pros and cons of using such drivers with Smoothieware ? 



<b>Originally shared by Jérémie Tarot</b>



Any CNC masta to educate me about how these should compare with the high power drivers usual suspects (DQ542MA, Gecko, Leadshine...) ? 





**"J\u00e9r\u00e9mie Tarot"**

---
---
**Arthur Wolf** *December 30, 2016 11:34*

That's a step/dir driver, it should just work out of the box. Any pros and cons are strictly related to the driver itself and would have nothing to do with Smoothie.


---
**Ray Kholodovsky (Cohesion3D)** *December 30, 2016 12:57*

I run the nema 17 version of his boards with my smoothie-powered Remix board . It all works - just takes the step and dir signals like Arthur said. 

What that is, is an encoder board - the nema 17 is a heavily improved mechaduino based design and this new Nema 23 one is from scratch awesomeness. You put a little magnet on the shaft of your motor and then the board has an encoder to read the magnet and compensate/ catch any missed steps. 


---
**Steve Anken** *December 30, 2016 16:09*

I've used Leadshine and Geckos but those have no encoders. This is a new kind of "smart" (encoder) implementation using the magnet trick but I don't know how it compares to servos like ClearPath servos. Servos typically do some super fancy tuning of x y an z with load to map the force vectors and ClearPath does a real bang up test using hard stops to get machine limits. There are also issues of getting the shakes due to hunting overshooting and "ringing" that this approach has to deal with so it all comes down to how well they implement the feedback and the control it surfaces. 



The servos I've worked with have very fine optical encoder discs AND a 3 hall sensor array with some pretty amazing software to tune them. once you go to encoders servos become a LOT more attractive and they are so nice and quiet.



If anybody gets one please post your first impressions. It's a step in the direction of closing the loop, the evolution of automation.


---
**Stephane Buisson** *December 30, 2016 18:02*

nema 17 version


{% include youtubePlayer.html id="fq8NBgqmkKY" %}
[youtube.com - 12RPM Nano Zero Stepper](https://youtu.be/fq8NBgqmkKY)


---
*Imported from [Google+](https://plus.google.com/+JérémieTarot/posts/Dj5rPYYyJGr) &mdash; content and formatting may not be reliable*
