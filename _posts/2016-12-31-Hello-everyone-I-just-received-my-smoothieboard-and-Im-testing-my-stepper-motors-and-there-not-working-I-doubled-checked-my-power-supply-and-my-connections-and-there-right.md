---
layout: post
title: "Hello everyone I just received my smoothieboard and I'm testing my stepper motors and there not working I doubled checked my power supply and my connections and there right"
date: December 31, 2016 03:23
category: "General discussion"
author: "jacob keller"
---
Hello everyone



I just received my smoothieboard and I'm testing my stepper motors and there not working I doubled checked my power supply and my connections and there right. And I just noticed the the little  4 LED light was not on which means that the smoothieboard is not reading the SD card. is there away I can fix this I tried reformatting the SD card tried a different SD card and its still not working. am I'm doing something wrong here. I'm trying to rebuild a cubepro.



all the help is most appreciated 



jake       





**"jacob keller"**

---
---
**Douglas Pearless** *December 31, 2016 04:30*

My experiences have been that the driver for the SD Card can be a bit picky with the card itself.  What brand and type of card (speed, etc.) are you using?



Cheers

Douglas


---
**jacob keller** *December 31, 2016 04:33*

I'm using the SD card that the board came with. Which is the SanDisk 4GB Micro SD card.



Can you recommend a different SD card for me.


---
**Douglas Pearless** *December 31, 2016 04:42*

I am using both Kingston and Strontium 8GB cards at the moment.

Also which Smoothieboard are you using?


---
**jacob keller** *December 31, 2016 04:53*

Ok so I got the smoothieboard to read the SD card And it updated the firmware. but its still not turning to motors when I tell it to? do I have to to something else do the end stop have to be plugged in?






---
**Douglas Pearless** *December 31, 2016 05:06*

Do you have a "config" file on the SD card which Smoothie uses to configure itself?  Refer to [smoothieware.org - Configuring Smoothie - Smoothie Project](http://smoothieware.org/configuring-smoothie) for details of the line options in the config file.


---
**Steve Hogg** *December 31, 2016 13:11*

I had issues with USB at first since I was using an extension cable. Half the time it wouldn't even connect properly. No issues at all since switching to a short cable. Is there a terminal in the control software you are using. Do you see the commands you are sending? 


---
**Drake Smith** *December 31, 2016 16:06*

Find out if the end stops are active, or for example the extruder won't move if it thinks it's not hot. Safety features.




---
**jacob keller** *December 31, 2016 17:20*

Thanks for the responses everyone. I got the motors to move. But they get really hot. Does anyone know why they get hot.


---
**Douglas Pearless** *December 31, 2016 21:49*

It means the current settings for the motors are too high.  Adjust the current setting to the lowest worked value .


---
**jacob keller** *January 02, 2017 19:44*

Hey everyone so everything is working. I got the hotend to heat up the endstops work. But just one thing the extruder motor won't turn it just clicks and it looks like it's trying to turn forward and backward at the same time. I put the extruder motor in to the x port and it turns but when I put it in the M4 connection it does the same thing. Is there away I can fix this. 


---
**jacob keller** *January 02, 2017 20:06*

**+Douglas Pearless** **+Drake Smith** **+Steve Hogg** Hey everyone so everything is working. I got the hotend to heat up the endstops work. But just one thing the extruder motor won't turn it just clicks and it looks like it's trying to turn forward and backward at the same time. I put the extruder motor in to the x port and it turns but when I put it in the M4 connection it does the same thing. Is there away I can fix this. 




---
**Arthur Wolf** *January 02, 2017 20:37*

Maybe you are trying to turn it too fast ? Acceleration too high ?


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/92KTFjJJaRa) &mdash; content and formatting may not be reliable*
