---
layout: post
title: "I've had an issue crop up recently that I thought was solved - I keep getting a \"firmware unresponsive\" message midway through prints"
date: December 31, 2016 23:23
category: "General discussion"
author: "Dean Rock"
---
I've had an issue crop up recently that I thought was solved - I keep getting a "firmware unresponsive" message midway through prints. I'm using Simplify 3d but it had been working fine prior to installing an older Smoothie I had on hand when the other one died.  I'm not sure in what direction to look. Ideas? TIA





**"Dean Rock"**

---
---
**Douglas Pearless** *December 31, 2016 23:54*

I would suggest that you try the very latest Smoothie, noting the config file might have changed a bit and you should carefully check it too.


---
**Dean Rock** *January 01, 2017 00:31*

Ah. Meaning that newer boards don't have this conflict? I am using the newer config.  Hmmm, I have a re-Arm on order...perhaps I'll wait to try that out. Or wait even longer for Smoothie 2. Or go wild with a Duet. If I need to replace the board, again...


---
**Douglas Pearless** *January 01, 2017 01:16*

Which board do you have as it should work with the latest version 


---
**Dean Rock** *January 01, 2017 12:25*

It's an x5 and it does work up to some unknown point when it becomes unresponsive, ceases to proceed despite force continue attempts on its own or from a S3d command.


---
**Douglas Pearless** *January 01, 2017 19:50*

Can you explain in more detail what is unresponsive, is it the host software used to print or Smoothie itself.



Can you communicate with Smoothie when this occurs, for example issuing a M999 to reset Smoothie?



Are you able to print anything else's?



How big or complex is the file you are trying to print and does it always stop in the same place?



Could you try a different slicer?



I had a similar problem but it was the host machine running out of memory ( I only had 2GB of ram and there was no more room on the disk for  swap file to expand).; do you have enough host resources available?






---
**Wolfmanjm** *January 01, 2017 20:46*

FWIW S3D as a host is not supported, and AFAIK it does not support smoothie either, try pronterface or octoprint.


---
**Dean Rock** *January 01, 2017 21:23*

Thanks for clarifying, Arthur. I knew there was a, uh, conflict in that regard but didn't know if it had been resolved. I have been using S3d without issues for well over a year with my former Smoothie board. When that seemed to die, I installed another one that I had from early on and now have problems as described. **+Douglas Pearless** The Smoothie will not reset once it becomes unresponsive. As to whether it is host software or board, remains unknown. I will try Pronterface. Incidentally, not complex prints and plenty of memory. Also, the problem is intermittent. Sometimes it makes it all the way but this is not dependable and has resulted in too many losses of partially completed prints.


---
**Dean Rock** *January 01, 2017 21:24*

Thanks for your thoughts, all. Pronterface will likely do the trick.


---
**Wolfmanjm** *January 01, 2017 21:40*

it is the host that is probably the problem not the slicer. also make sure you have upgraded to the latest version of smoothie as the old ones did have a problem with the crappy gcode s3d produces. (that was fixed)


---
*Imported from [Google+](https://plus.google.com/116087024084558080588/posts/AKmX4w44S5X) &mdash; content and formatting may not be reliable*
