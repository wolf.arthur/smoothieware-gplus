---
layout: post
title: "If I want to use a rrd graphics display, and a thermocouple, would this be possible?"
date: January 02, 2017 20:07
category: "General discussion"
author: "Rien Stouten"
---
If I want to use a rrd graphics display, and a thermocouple, would this be possible?





**"Rien Stouten"**

---
---
**Arthur Wolf** *January 02, 2017 20:21*

Unfortunately no, that is a limit of the RRD GCD hardware itself, it doesn't respect the SPI's "select" pin, so you can't do it.


---
**jacob keller** *January 02, 2017 20:32*

**+Arthur Wolf** can you take a look at my post. And what's your thoughts on the extruder problem.



Thanks


---
**Arthur Wolf** *January 02, 2017 20:32*

**+jacob keller** What post ? I have the flu I'm only now slowly catching up on things.


---
**jacob keller** *January 02, 2017 20:35*

[plus.google.com - Hello everyone I just received my smoothieboard and I'm testing my stepper…](https://plus.google.com/115098820415162360458/posts/92KTFjJJaRa)


---
**Rien Stouten** *January 02, 2017 20:37*

**+Arthur Wolf** Why use SPI at all?

Why couldn't one use a spare ADC pin?


---
**Arthur Wolf** *January 02, 2017 20:38*

**+Rien Stouten** I have no idea how you'd go about reading a thermocouple with an ADC, we only support doing that via a SPI port at the moment. If you wanted to add that, it'd require some code ( like was added recently for PT100 sensors )


---
**Rien Stouten** *January 02, 2017 21:16*

**+Arthur Wolf**

Well, not directly, no, but there are thermocouple to voltage converters out there.

At the moment I'm using a AD597 with a ramps board, which is not compatible with 3.3V, but there are others who will work fine with 3.3V.

Found one. [https://www.adafruit.com/product/1778](https://www.adafruit.com/product/1778)




---
**Arthur Wolf** *January 02, 2017 21:53*

Oh, we actually support AD8495, it's just not documented ... [https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/tools/temperaturecontrol/AD8495.cpp](https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/tools/temperaturecontrol/AD8495.cpp)
I'm going to try adding that to the documentation right now.


---
**Rien Stouten** *January 02, 2017 22:06*

**+Arthur Wolf**  So it wouldn't be any problem?

Just fill in AD8495 and the correct pin number and it would work? That would be great.


---
**Arthur Wolf** *January 02, 2017 22:07*

**+Rien Stouten** I haven't ever tested it, but the code is there yes. I added the config options to the documentation, but I'm not sure exactly how to wire it.


---
**Rien Stouten** *January 02, 2017 22:11*

**+Arthur Wolf** Well, that can't be difficult.

You just made my day.


---
*Imported from [Google+](https://plus.google.com/+RienStouten/posts/PvfDeGbim9h) &mdash; content and formatting may not be reliable*
