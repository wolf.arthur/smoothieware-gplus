---
layout: post
title: "I'm having a problem with homing on my X axis"
date: January 05, 2017 15:53
category: "General discussion"
author: "Blake Dunham"
---
I'm having a problem with homing on my X axis. When I send a M119 when the endstop isnt being depressed, it registers a 0, and when I press is down, it registers a 1. However, when I go to home my X axis, the print head moves a little and then stops. I am using simplify 3D to control the printer. I'm also running the latest release of Edge. 





**"Blake Dunham"**

---
---
**Arthur Wolf** *January 05, 2017 16:07*

Maybe it's trying to move too fast ? Or you don't have max_travel set ? Or it's noisy and you need to enable debounce ?


---
**Blake Dunham** *January 05, 2017 16:14*

I'll go ahead and try all of those and report back with results. My guess is debounce or noise. I have max travel set.


---
**Wolfmanjm** *January 05, 2017 22:02*

FWIW S3D is not supported as a host, so you may want to use pronterface or octoprint to control it.


---
**Blake Dunham** *January 05, 2017 23:03*

I got it working. Got caught up in trying things out and forgot to update earlier. I set the endstop_debounce_count to 100, not much changed. I then changed the max_travel and that seemed to fix some of the problem. I then added the endstop_debounce_ms and kept adding 1 until I got to 5 and that fixed it.


---
*Imported from [Google+](https://plus.google.com/107526587910712362690/posts/YV7HVkvDJbo) &mdash; content and formatting may not be reliable*
