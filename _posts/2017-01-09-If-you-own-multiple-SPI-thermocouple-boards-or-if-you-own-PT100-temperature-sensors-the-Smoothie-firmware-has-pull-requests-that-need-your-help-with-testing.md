---
layout: post
title: "If you own multiple SPI thermocouple boards, or if you own PT100 temperature sensors, the Smoothie firmware has pull requests that need your help with testing :"
date: January 09, 2017 11:41
category: "General discussion"
author: "Arthur Wolf"
---
If you own multiple SPI thermocouple boards, or if you own PT100 temperature sensors, the Smoothie firmware has pull requests that need your help with testing : [https://github.com/Smoothieware/Smoothieware/pulls](https://github.com/Smoothieware/Smoothieware/pulls)





**"Arthur Wolf"**

---
---
**Sally Scott** *January 12, 2017 09:57*

Hi, I'm currently upgrading my Arduino based printer to Re-ARM for RAMPS. My E3D hot end is fitted with a PT100 and amplifier. I have a question about the 'temperature_control.hotend.thermistor_pin' (0.23). Does example config take into account any pullup attached to the thermistor pin? Or would it be better to use the RAMPS pin A3 which equates to P0.27 on the Re-ARM?


---
**Luke Kuhner** *April 22, 2018 16:28*

**+Sally Scott** I am looking into installing the E3D PT100 amplifier on my Re-ARM board. I'm still uncertain on what pins to use for powering the amplifier board, as well as what input pin to use. Have you gotten it to work and could you share with me your setup? 


---
**Sally Scott** *April 23, 2018 08:20*

**+Luke Kuhner** I gave up using the PT100 as I had 3 failures in a month and a half. I went back to using a thermistor and I've had no failures since then. I've also changed to using Marlin-bugfix-2.0.x as I.m more familiar with the way Marlin works and the fact that Re-ARM was the first board to be supported with the 32 bit version.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/6W8Vf85Ktzq) &mdash; content and formatting may not be reliable*
