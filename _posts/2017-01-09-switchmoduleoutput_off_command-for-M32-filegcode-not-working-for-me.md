---
layout: post
title: "switch.module.output_off_command for M32 file.gcode not working for me"
date: January 09, 2017 16:57
category: "General discussion"
author: "java lang"
---
switch.module.output_off_command for M32 file.gcode not working for me.

I want to config a button to play a gcode-file, I'm sure there is a solution but don't know how.

First test to check if pin is right: 'switch.playbutton.output_off_command M106' -> OK works



Attempts for playing file:

with space (command works well if controlled from repetier host):

switch.playbutton.output_off_command M32 test.gcode -> No response

without space:

switch.playbutton.output_off_command M32test.gcode -> No response

filename in parenthesis:

switch.playbutton.output_off_command M32"vc.gcode" -> No response

whole command in parenthesis

switch.playbutton.output_off_command "M32 vc.gcode" -> No response

How is the correct syntax for sending filenames in config file?

Thank you in advance









**"java lang"**

---
---
**java lang** *January 09, 2017 17:25*

Problem solved using an underscore: switch.playbutton.output_off_command	M32_test.gcode


---
*Imported from [Google+](https://plus.google.com/111347667569437072694/posts/YeTuacWbGBv) &mdash; content and formatting may not be reliable*
