---
layout: post
title: "Does anyone now why my extruder is not pushing the filament into the hotend"
date: January 10, 2017 02:38
category: "General discussion"
author: "jacob keller"
---
Does anyone now why my extruder is not pushing the filament into the hotend. If I help push the filament when the motor is Turning it works. but if I let go it starts to click and and it doesn't push the filament anymore?



I'm using the E3D V6 hotend 



I'm using a smoothieboard 



I checked the wires and there correct. So it's not the wires 



I think it's something to do with the configuration.



I'm a little confused with the configuration can someone help me with the extruder configuration that would be most appreciated. There's a copy of my extruder configuration in the picture.



I'm using a Nema 17 stepper motor



I tried wiring the extruder to the x axis and it turns



Any help is Most appreciated 

Jake

![missing image](https://lh3.googleusercontent.com/-z5zJ2q0z8DI/WHRJKsHy8CI/AAAAAAAAAbc/3DVTiyk-Y8MxFcMSuzYXvSGqH7hqxuC5ACJoC/s0/IMG_20170109_203551249.jpg)



**"jacob keller"**

---
---
**Stephen Baird** *January 10, 2017 02:53*

It sounds like the current setting is low. The variable you want is in your screenshot, delta_current. 



If the x-axis pin works nicely try finding what alpha_current is set to and use that value for delta_current. 


---
**jacob keller** *January 10, 2017 03:05*

Thanks for the response.



I tried bringing the current up to 1.9 

Didn't work.



Tried 1.8

          1.7

          1.6

          1.5

          1.4

          1.3

          1.2

          1.1

          0.5



I tried all of them.

And it still not working.



I think max is 2.0 for the current on the smoothieboard. 



I don't think it's the current.


---
**jacob keller** *January 10, 2017 03:05*

**+Stephen Baird**


---
**jacob keller** *January 10, 2017 03:07*

**+Stephen Baird**



Plus I changed the steps per mm for the extruder from 140 to 100 and that didn't do anything.


---
**Don Moss** *January 10, 2017 03:35*

If you are using a 5X board, try swapping the extruder stepper to the 2nd extruder output and modifying the config to use that.

Or try plugging in a different stepper motor to that output to test it.

My 5XC board had a faulty stepper driver out of the box and I have been using the 2nd extruder output since. (Too much of a pain to send the board halfway around the world and wait for a replacement.)

Fortunately I am only using 1 extruder.


---
**Stephen Baird** *January 10, 2017 04:11*

I would agree, it sounds like there may be a problem with that driver chip. If it's brand new I'd get in touch with the vendor you bought it from.


---
**jacob keller** *January 10, 2017 04:47*

Thanks I'll check with them.



I have 4x smoothieboard


---
**Arthur Wolf** *January 10, 2017 08:39*

It's possible this is a wiring problem, please try another stepper motor on that stepper driver's port.


---
**Douglas Pearless** *January 10, 2017 09:47*

your extruder default feed rate seems very high, try 600


---
**Greg Nutt** *January 10, 2017 12:36*

What kind of extruder are you using?  A poorly configured or weak extruder could be just as likely a culprit. 


---
**jacob keller** *January 10, 2017 13:20*

Thanks everyone for responding



I will try a different extruder. And bring my default feed rate down to 600.


---
**Alex Krause** *January 10, 2017 16:40*

**+jacob keller**​ when you made changes to the firmware, did you restart the smoothie board (power it off then back on)?


---
**jacob keller** *January 10, 2017 16:46*

**+Alex Krause** yes


---
**jacob keller** *January 10, 2017 21:50*

OK so I changed the motor and the extruder is now working. Thanks for the help everyone.


---
**Douglas Pearless** *January 10, 2017 21:57*

Great, so it was a faulty motor? Or a co dig issue?


---
**Douglas Pearless** *January 10, 2017 22:31*

"co dig" == config


---
**jacob keller** *January 10, 2017 23:33*

**+Douglas Pearless** I think its the extruder bad design. Here's a link to the video. 



[dropbox.com - VID_20170110_171718633.mp4](https://www.dropbox.com/s/x97l1v7tblwf87m/VID_20170110_171718633.mp4?dl=0)



It worked for about an hour then started to do everything that it was doing before.



I have now idea what to do now. Maybe I should buy different extruder design.



I have like 5 of this extruders in my shop doing nothing.






---
**Greg Nutt** *January 10, 2017 23:59*

What's your hotend temperature?


---
**jacob keller** *January 11, 2017 00:02*

220 PLA


---
**jacob keller** *January 11, 2017 00:03*

Tried going up to 260 and 270. Plus I checked to see if there was a clog no clog


---
**Greg Nutt** *January 11, 2017 01:14*

260/270 with PLA??  That's way too hot!  200 should be the normal area.


---
**jacob keller** *January 11, 2017 01:18*

I thought there was clog but there wasn't. So I brought it up to 260/270 think that would get what ever there was in the hot end out.


---
**Douglas Pearless** *January 11, 2017 01:18*

I think the idea is to totally melt the PLA and force out anything that may be blocking the internal path i the hot-end, rather than try to print at that temperature.


---
**Greg Nutt** *January 11, 2017 01:20*

At that temperature, I'd suspect you're more likely to start burning the plastic and cause the blockage.


---
**Douglas Pearless** *January 11, 2017 01:23*

It might be getting close to the time where a different hot-end should be tested..


---
**jacob keller** *January 11, 2017 01:26*

+Douglas Pearless **+Greg Nutt**  I print PLA at 220.  And I wasn't printing at 260/270  just trying to purge the print head. But do you think I should buy a new extruder or keep trying with the cubepro extruders


---
**jacob keller** *January 11, 2017 01:27*

What hotends would you recommend that have a length of 75mm


---
**Greg Nutt** *January 11, 2017 01:30*

Personally, I think 220 is still too hot for PLA (unless you're printing at >150mm/s or it's some special formula requiring higher heat).  I like and use the E3D Titan extruder myself (geared extruder).  Not familiar with the Cubepro one you're referring to.  I used E3D v6 hotend.  You're still able to manually push filament through?


---
**jacob keller** *January 11, 2017 01:42*

**+Greg Nutt** Thanks for the hot end recommendations. What temperature do you print PLA at with your E3D V6 hot end? Yes I can manually push the filament in the hot end with cubepro extruders. 


---
**Don Moss** *January 11, 2017 01:44*

The extruder is skipping as the hotend is partially blocked.

There are a couple of possible causes.

1. Temp too high. Most PLA's print fine with temps from 160-180. Too high a temperature will 'cook' the PLA and block the hotend.

2. Temp too low. You stated that you are running 220C. Are you using the correct thermistor type in the config? An incorrect thermistor type can report incorrect temperature.

3. Buildup of foreign matter in hotend. Run the filament through small block of sponge with a slit cut in it to wipe dust etc from the filament before it goes into the extruder. Use a small tool to clear the nozzle in the hotend or disassemble the hotend and thoroughly clean it.

4. Poor filament quality. Some filaments are better than others. Use a known good quality filament.



Your hotend looks like an E3D or clone. I am currently using an E3D V6 and have had no issues with blockages printing PLA, ABS and PETG.


---
**Greg Nutt** *January 11, 2017 02:20*

I print my PLA at a base temperature between 195 and 200.  I go up to 210 to 215 if I'm printing faster (150mm/s to 200mm/s) but for the average 45 to 60mm/s I usually do about 200.



I'm not keen on clones.  I only tried one once and had instant problems with it and went to a genuine E3D v6.  Worked out of the box first try.  (your mileage may vary of course).  I have also used the Hexagon and have to say I was pretty happy with it too.


---
**jacob keller** *January 11, 2017 03:28*

I'll try again tomorrow with the specs you gave me. I bought my hot end from e3d. It's not a clone from eBay. 


---
**Venelin Efremov** *January 12, 2017 14:02*

E3D v6 would clogged if you have too long retraction. They recommend retraction less than 1mm.


---
**jacob keller** *January 14, 2017 21:23*

**+Greg Nutt****+Douglas Pearless** **+Arthur Wolf** 

So I got the printer to print. But when the first layer is done it retracts all the filament out of the extruder and then it starts the second layer and it can extrude the plastic because it retracted the filament. Any ideas why it does this.



Here's my start g-code.



G21; metric value's

G90; absolute positioning

M107; start with fan off

G28 X0 Y0 ; home XY

G28 Z0 ; home z

G1 Z15.0 F2000; move the platform down 15mm

G92 E0; zero the extruder

G1 E30 F90 ; extrude 30mm

G92 E0 ; zero extruder





All help is Most appreciated


---
**Arthur Wolf** *January 14, 2017 21:26*

That sounds like a problem with your gcode file


---
**jacob keller** *January 14, 2017 21:39*

@ arthurwolf yeah I now it something to do with my gcode. but I'm trying to figure what part of the gcode is causing the problem.


---
**Arthur Wolf** *January 14, 2017 21:53*

It's either not resetting the E axis when it's done with the layer, or resetting it but producing gcode as if it didn't. What slicing program are you using ?


---
**jacob keller** *January 14, 2017 21:58*

**+Arthur Wolf**​ slic3r


---
**Arthur Wolf** *January 15, 2017 00:47*

It should work, this is very strange


---
**jacob keller** *January 15, 2017 00:51*

I sliced the file again and it works now. Thanks for your response.


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/FY7YBtR1a5J) &mdash; content and formatting may not be reliable*
