---
layout: post
title: "I am researching the best way to rapidly turn off the motors in the case of a crash on my K40-S There are times when running Gcode tests that the carriage gets driven into the stops and I want to release the motors quickly"
date: January 14, 2017 18:46
category: "General discussion"
author: "Don Kleinschnitz Jr."
---
I am researching the best way to rapidly turn off the motors in the case of a crash on my K40-S



There are times when running Gcode tests that the carriage gets driven into the stops and I want to release the motors quickly. I suspect there are other times when I want to shut down due to unplanned interference's in the work area. 



I know that I can turn off the motors from my GLCD panel but it takes to much time to traverse the menu items after a crash. This is the approach that I currently use. I also know that I could do this by issuing a Mcode from LW.



I need this button to be remote from the smoothieboard because in my setup it is not accessible.



I am hoping for a button that when pushed stops/pauses the current operation, releases the motors and then with another action will go back to an operational state.

.................

So I am researching the use of the kill switch for this function. 

Which has generated some questions numbered below.

............... 

From this link: [http://smoothieware.org/killbutton#toc0](http://smoothieware.org/killbutton#toc0)



"When in the halted state (after kill button is pressed) the play led will flash rapidly. Kill can be cleared by holding down the kill button for 2 seconds or more"



1. By "clear" does this mean the machine will resume normal operation (exit the halt state) or is a reset necessary?

..............

"If the kill button is pressed ..... enters the Halt state, in this state the play led flashes, and the state can be cleared by issuing M999 or holding the flashing kill button for 2 seconds (it can also be cleared from the LCD panel)."



2. I could not find a reference as to how it is cleared from the LCD panel.

..............

From this link: [http://smoothieware.org/panel#toc5](http://smoothieware.org/panel#toc5)



"The button on the glcd and VIki2 can be wired as a Kill button by following This guide. In that case the panel.back_button_pin should be commented out."



3. This reference took me to this link: [http://smoothieware.org/pausebutton](http://smoothieware.org/pausebutton) which currently has no content.



4. What button on the GLCD is this referring to? 

...................

My plan is to: 

A: connect a 4 wire harness to a Stop LED and Stop PB on my K40 control panel.

    A1: LED to JP24

    A2: Momentary PB switch to JP23 



B: change the configuration file as such:

...kill_button_enable                        true             # set to true to enable a kill button

...kill_button_pin                           2.12             # kill button pin. default is same as pause button 2.12 (Add ^ for external buttons)

5. If I am wiring the PB to JP23 I will not need the ^ in the config?



I expect the operation to be:

To stop: Push the stop button and the machine goes into halt. The LED will pulse rapidly. Will this turn off the motor drive?

To resume: Push and hold the stop button for 2 sec. and machine will resume i.e. exit the halt state.



I would appreciate any advice and help from the "experts". 

I will document and publish the end results for all.









**"Don Kleinschnitz Jr."**

---
---
**Arthur Wolf** *January 14, 2017 18:59*

1. Yes, but position is lost so you should reset anyway.

2. Don't know

3. Pausebutton doesn't exist anymore. See [smoothieware.org - Stopping Smoothie - Smoothie Project](http://smoothieware.org/stopping-smoothie) and [http://smoothieware.org/switch](http://smoothieware.org/switch)

4.Dunno

5. Same.




---
**Don Kleinschnitz Jr.** *January 14, 2017 19:24*

**+Arthur Wolf** do I have to do a reset or will a "home" restore the position and axis. 

Is it advisable to remote the "reset" via JP25 through a PB to gnd. 



Is using "reset" a better approach to achieve the function above ? 

or 

Both remote reset and stop?


---
**Arthur Wolf** *January 14, 2017 19:31*

**+Don Kleinschnitz** Home should be sufficent.

You can remote the reset button yes. Make sure you twist the wires.


---
**Don Kleinschnitz Jr.** *January 14, 2017 19:40*

**+Arthur Wolf** thanks for the guidance moving forward to implement.


---
**Wolfmanjm** *January 15, 2017 02:08*

2. When it is in halt the Panel has a Clear Halt menu entry on the main menu.

4. it depends on the GLCD, the viki2 has a button as does the RRD GLCD, this can be used as an exrernal kill button, and will require a pullup. NOTE that genuine smoothieboards have  an outline for a kill button with integrated LED on the PCB.

5. You will need the pullup ^



Also after clearing halt it is advisable to home, but is not always necessary as it tries to set the current position based on the last actuator position, but if the  head is moved at all then you will lose the absolute position and homing is required. Also on some drivers microstepping is reset when the motors are disabled.


---
**Don Kleinschnitz Jr.** *January 15, 2017 02:40*

**+Peter van der Walt** I agree on the notion of hardware off control for emergencies and many times I have been using the main power switch to stop when I could not get to the panel. 

BTW I do not see a difference between the inline cord in the video and the K40 main switch?




---
**Don Kleinschnitz Jr.** *January 15, 2017 02:45*

**+Wolfmanjm** thanks can you point me to anything that tells me how to use the button on the panel as kill? I have a RRD GLDC. 


---
**Wolfmanjm** *January 15, 2017 02:47*

just hook the button to the kill pin, simple as that, treat it as an external kill button. comment out the glcd entries for pause/back/kill buttons if they are there.


---
**Wolfmanjm** *January 15, 2017 03:43*

3. Fixed the link in the wiki


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/TWLbyzKabhQ) &mdash; content and formatting may not be reliable*
