---
layout: post
title: "Any recommendations for pins on the 1769 viable for chip select spi pins?"
date: January 16, 2017 02:44
category: "General discussion"
author: "Griffin Paquette"
---
Any recommendations for pins on the 1769 viable for chip select spi pins? Wanting to run more components via the spi bus but wanted to get some feedback as to which are preferable to use. Not using the ethernet pins so if those work that would be excellent. 



Thanks in advance!

Griffin





**"Griffin Paquette"**

---
---
**Arthur Wolf** *January 16, 2017 10:54*

You can't use the Ethernet pins for anything else. Aside from that any pin works for SSEL


---
*Imported from [Google+](https://plus.google.com/111302122377301084540/posts/SC8RCMYYfC3) &mdash; content and formatting may not be reliable*
