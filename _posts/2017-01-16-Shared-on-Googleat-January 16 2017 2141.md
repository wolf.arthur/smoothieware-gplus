---
layout: post
title: "Shared on January 16, 2017 21:41...\n"
date: January 16, 2017 21:41
category: "General discussion"
author: "Arthur Wolf"
---
[http://smoothieware.org/g10](http://smoothieware.org/g10)





**"Arthur Wolf"**

---
---
**Marc Miller** *January 17, 2017 09:35*

I love that particular XKCD.  


---
**Steve Anken** *January 18, 2017 03:35*

"The nice thing about standards is that you have so many to choose from; furthermore, if you do not like any of them, you can just wait for next year's model."



[wiki.c2.com - wiki.c2.com/?AndrewTanenbaum](http://wiki.c2.com/?AndrewTanenbaum)


---
**John Herrington** *January 22, 2017 04:37*

Just quit blindly following EMC on the arbitrary ignoring of absolute mode when using g10 L2 and you software will be better for it.  I'm just saying.


---
**Arthur Wolf** *January 22, 2017 11:48*

**+John Herrington** We generally follow the NIST format and the LinuxCNC documentation. Do you have more details on exactly what you mean ?


---
**John Herrington** *January 27, 2017 03:15*

Sure.  And I don't know where EMC/LinuxCNC picked up this ignoring G91 when using G10 L2, it's not from EIA or the NIST RS274-*.  It's not from ISO 6983-1:2009.  It's just arbitrary, and less functional than respecting the distance mode.   I posted the following in the LinuxCNC's forum:

[forum.linuxcnc.org - G10 L2 - LinuxCNC](https://forum.linuxcnc.org/forum/20-g-code/31657-g10-l2#81317)


---
**Arthur Wolf** *January 27, 2017 09:48*

**+John Herrington** Thanks.


---
**Arthur Wolf** *January 29, 2017 23:16*

**+John Herrington** Actually, I've just been informed the problem you mention is fixed in Smoothie. 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/TBewwVyBAXj) &mdash; content and formatting may not be reliable*
