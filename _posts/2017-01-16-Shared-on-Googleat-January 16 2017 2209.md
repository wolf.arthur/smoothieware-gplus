---
layout: post
title: "Shared on January 16, 2017 22:09...\n"
date: January 16, 2017 22:09
category: "General discussion"
author: "Arthur Wolf"
---
[http://smoothieware.org/g28](http://smoothieware.org/g28)





**"Arthur Wolf"**

---
---
**Douglas Pearless** *January 16, 2017 22:23*

Great stuff, perhaps you would also like to describe how G28 can be different for cartesian vs (linear or rotary) delta, i.e. home one axis at a time vs home all axis at once?


---
**John Herrington** *January 17, 2017 02:45*

G28 is even more convoluted in real CNC world.  It doesn't mean go to the home position, it means return to the home position from a intermediated point.  So basically if you use G28 in absolute you can crash the piss out of a machine.



G90 G28 Z0;  means go to Z0 then go home. That's the spindle nose to the table with no other offsets present.



the proper way to use G28 is:



G91 G28 Z0;  which means move nothing, then go home on z.



just use the machine coordinates.



G53 Z0;



Which works fine in absolute.


---
**Arthur Wolf** *January 17, 2017 09:36*

**+John Herrington** We know about the intermediary point feature, it's on the todo list.

**+Douglas Pearless** I'll add that, thanks.


---
**Chris Brent** *January 20, 2017 21:26*

Want me to write an M408 page :P


---
**Arthur Wolf** *January 20, 2017 21:28*

**+Chris Brent** Sure ! Just add a header saying it's not supported yet. Where are we at with that by the way ?


---
**John Herrington** *January 22, 2017 04:32*

That's funny.  It never was a feature, it is just how it was implemented well over 30 years ago.  The null incremental movement is just a work around of the intermediate point requirement.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/VnLmXqF4NPM) &mdash; content and formatting may not be reliable*
