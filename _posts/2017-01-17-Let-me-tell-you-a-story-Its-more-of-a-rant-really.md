---
layout: post
title: "Let me tell you a story. It's more of a rant really"
date: January 17, 2017 17:43
category: "General discussion"
author: "Arthur Wolf"
---
Let me tell you a story.

It's more of a rant really.



In the Smoothie project, we have many ways users can get in touch with each other, or the devs, to get help.

IRC, forum, mailing lists, Google+, facebook ...

All those channels are very active ( except facebook, not sure why ), and users get good quality answers there, relatively quickly ( IRC being the fastest in general, if you ask at the hours both the US and Europe are awak ).



And on github, we also have another communication channel, something called "issues" : [https://github.com/Smoothieware/Smoothieware/issues](https://github.com/Smoothieware/Smoothieware/issues)



We try to reserve those issues to actually reporting bugs. This is a way to be better organized : that way we know that all the bugs are there, and only bugs are there. Because of this, we forbid users from asking "general" questions there. It's not a problem since there are many other places to ask questions.



It's simple : bugs go to github issues, general questions go everywhere else.



From time to time, somebody asks a "general" question there anyway. When that happens, we politely tell them it's not the right place to do it, and we point them at all the other ( many ) places where they can do so.

They are going to get help, just not there.



Despite this, very rarely, one of them gets upset. It's not justified, it's unfair, but it still happens. I'm not sure why.



Months ago, this happened with a user. He had gotten help from us many times before ( just search his name on the smoothieware-support mailing list ), but he asked there, he was asked ( politely ) to ask elsewhere, and got upset ( via private email, calling us arrogant and other things ).



This is what got him upset : [https://github.com/Smoothieware/Smoothieware/issues/940](https://github.com/Smoothieware/Smoothieware/issues/940) . If you can understand how that justifies getting upset, please explain it to me.



They emailed us, said they went on IRC, waited a bit and got no answer ( dozens of users get help on IRC every day, have for years, but sometimes you have to wait because people are sleeping ). 

They complained the documentation didn't contain things it actually contains ( like information on how to compile the firmware : [http://smoothieware.org/compiling-smoothie](http://smoothieware.org/compiling-smoothie) . It's linked to on the projects homepage ... ). 

They thought that meant the Smoothie project didn't have adequate help channels ( and therefore asking in the issues should be ok ... I guess ).



I wrote them an extremely long, polite and diplomatic explanation of where to get help, why things are the way they are, very nicely trying to help them, answering the questions they hadn't had answers to yet. 

I didn't get an answer from them.



Now months later, I realize that person is now writing MKS SBASE guides on Instructables, and dissing the Smoothie project in their guides' comments. 



They know the MKS boards hurt the Smoothie project, they know the volunteers who gave them the Smoothie project don't want MKS to do what they do, and despite all this they promote the MKS stuff.

They essentially are holding a grudge against the Smoothie project, and are actively helping destroy it.



I just wrote them very angry messages ( I'm like that, sorry. Injustice makes my head boil ), and I'm posting this here to vent ( sorry again, you didn't do anything wrong ).

I really don't understand some people.



[http://www.instructables.com/id/SbaseSmoothieware-Use-a-Free-PWM-Pin-and-Power-Exp/#comment-list](http://www.instructables.com/id/SbaseSmoothieware-Use-a-Free-PWM-Pin-and-Power-Exp/#comment-list)



And just to be clear : the MKS guide in the link was posted <b>the day after</b> he got upset about the "issues" thing. 

So it's very much related. 

He took his grudge, and his way of dealing with it wasn't to try to figure out if maybe we hadn't done anything wrong ( turns out we hadn't, and he'd know that if he had talked to us. We tried communicating ), but to actively try to hurt the project instead.



What a nice guy.

Or am I wrong ? What do you think ?

I think I'm giving up on explaining to him why what he is doing is nasty, but if you want to have a go at it don't hesitate.









**"Arthur Wolf"**

---
---
**Stephen Baird** *January 17, 2017 19:15*

Apparently, based on his instructables comments, he felt "attacked" when he was asked to keep bugs to the github and to ask general questions in another avenue. Which is weird, and an unreasonable take on what was said. 



It also seems stupidly petty to go around promoting the MKS boards... But you're bound to run into someone like that eventually no matter what you do. All you can do is try not to let them get under your skin and let them work out their childishness alone. 


---
**Arthur Wolf** *January 17, 2017 19:22*

**+Stephen Baird** I guess you are right. The thing is I always have the feeling if I keep talking to them and explaining to them why they are doing something wrong, they'll eventually realize it and stop. I know since they are doing it in the first place it's probably not going to happen, but I still have hope in people for some reason.




---
**Samer Najia** *January 17, 2017 21:24*

You know there's always going to be someone who just can't let go of whatever imagined slight...IMO you should use the tools and products that suit you best for the mission.  Rather than diss one product over another, be constructive and offer feedback  (not you, Arthur, the other person).  I have Smoothie, RAMPS, MKS, Azteeg, C3D, Sainsmart and a host of other stuff.  For some things Smoothie and C3D are my 'premium' easier to adjust configs (my opinion only), some projects are fixed with RAMPS and so on.  Customers can then make informed decisions.  But hey that's just me.


---
**Arthur Wolf** *January 17, 2017 21:37*

**+Samer Najia** thanks for the support :)


---
**Samer Najia** *January 17, 2017 21:41*

Your product is an excellent quality product.  One misguided customer should not get to tarnish that.


---
**Guy Bailey** *January 18, 2017 03:24*

You have far more supporters than haters. Don't let the vocal minority get to you. 


---
**Michael Stanich** *January 18, 2017 04:23*

It happens.  Its easiest to encounter discussing "religiously held beliefs" and also with the type of person best described as "irrational".  Many times, they have decided on the outcome before even opening the discussion.  Being logical, being correct, being polite, these make no headway because they want what they want regardless of your position, objectve reality, etc.  "Please don't feed the troll" is usually the least stressful way to handle it, and there are many strategies that can help.  I favor "treat them like the age they're acting" and realize that you're not going to win most of the arguments, there's truth in movies, sometimes the only winning move is not to play.


---
**Basile Laderchi** *January 18, 2017 07:33*

Why do I get the feeling that he was asking for help to complete his guide about the MKS board?


---
**Hakan Evirgen** *January 18, 2017 11:16*

I think some people are also just negative because they think that the Smoothieboard costs too much in comparison with others. I had already several discussions trying to explain why you should not buy too cheap and that you got more for your money when you buy quality products.



Someone in one of these discussions said to me: the people behind Smoothieboard are just ripping off people so I will not support them and buy from china.  - I heard similar statements more than once. And these people are hostile to the Smoothie project from the beginning.



Some people will never understand and rather choose a product which is a fire hazard. And they will bark negative about quality products.


---
**Arthur Wolf** *January 18, 2017 23:18*

The guy answered, and it was so full of blatant lies I had to put the super long and documented answer in a separate document : [goo.gl/TYLslk](http://goo.gl/TYLslk)


---
**Arthur Wolf** *January 18, 2017 23:28*

From his answers I must now suspect ( at least a little ) that he has mental health issues.

I guess that is something that happens, and that you have to run into if you talk to enough people.

Not sure what the right way to deal with that is, but if it's actually the case I guess I'll just try to leave him be, even if he is hurting the project.


---
**Samer Najia** *January 18, 2017 23:34*

**+Arthur Wolf** had something like this happen to me years ago too.  In the end that's what it was and I let it alone and in time my tormentor moved on to easier prey.


---
**Arthur Wolf** *January 18, 2017 23:49*

**+Samer Najia** We had the exact same case happen last year, guy with mental issues, goes crazy for no good reason. I guess it comes with the territory.


---
**Samer Najia** *January 18, 2017 23:51*

Sadly, yes.  Sorry to hear all this.


---
**Willem Aandewiel** *February 11, 2017 13:22*

**+Arthur Wolf** just do what your good at. This is a great product and a great project!


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/3r7kLimP2LU) &mdash; content and formatting may not be reliable*
