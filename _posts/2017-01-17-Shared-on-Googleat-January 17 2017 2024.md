---
layout: post
title: "Shared on January 17, 2017 20:24...\n"
date: January 17, 2017 20:24
category: "General discussion"
author: "Arthur Wolf"
---
[http://smoothieware.org/6axis](http://smoothieware.org/6axis)





**"Arthur Wolf"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 22:34*

Very helpful. Cuz you know, rotary. 


---
**Antonio Hernández** *September 18, 2017 03:31*

Hello everyone ! . I saw that some members of the community have achieved the working of five or six axes. In some cases, they were talking about "mini" board (I suppose "cohesion3d motion controllers".  Regardless of the fact that nowadays there is a guide for (4-5-6) axis configurations, and without considering the design of the mechanical parts for these axis, is it possible to flash this new firmware over older smoothieboards ?. I have 1.0 version. I don't know if it's possible to use them with this new firmware (if not, what version of smoothieboard could fit the requirements?). Is it possible to know (another source) more details about the problems that were found along the configuration process ? (some tips, maybe or any kind of suggestion that could help to understand the most basic problems that were found along the configuration process). Any comments are going to be appreciated. Thanks !!. (At the moment, I have "the maths"for "steps by grade" (delta_steps_per_mm) parameter, according to my physical design (it's in progress) related with 4th axis).


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 03:44*

I don't see why not, all smoothie's run the same chip (lpc1768/69. I've compiled for up to 7 axis (running a kraken off the Cohesion3D ReMix with the 7th extruder via an external stepstick) 



I haven't run jobs on it yet but I do have all 4 extruders turning. 


---
**Antonio Hernández** *September 18, 2017 04:41*

That sounds great !!!. Then, in your case, what problems did you have running smoothieboard with four axis ?. Could you share just a little bit about your experience with that configuration ?. You said "7" axis. Is that possible ?.


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 04:46*

You clone github, set # of actuators to 6 or 7 or whatever you want, and compile. You have your new firmware. Flash it onto Smoothie. You will eventually run out of memory. I don't know at what point (how many max motors can be supported or what the eventual limit will be). 


---
**Antonio Hernández** *September 18, 2017 04:57*

Ok. Uhm... In your testings, how long have you been able to notice that the board is working out of memory ?. It depends on the "complexity" of the piece ?. Did your tests use four axis or more ?. Is there an alternative to "increase" the memory of boards that have this issue ?. I suppose 1.0b or v2 are out of this situation ?.


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 05:06*

I don't have any testing. I compiled for 7 axis, wired the Remix, and manually jogged 4 stepper motors.  Yay. 

Do not confuse axis with extruder. Axis is total # of motors. So XYZ + 4 extruder = 7 axis. 

The next printer for this testing is not ready yet, I am working on a prusa style quad Y splitter and I have a kraken to test the quad extruders on Smoothie. 

I do not make the smoothieboard, I make Cohesion3D, but as far as I know all Smoothie use the same chipset. 



You will always hit a limit, whether it is speed, or something else. 



The alternative is to wait for smoothie2 :) 


---
**Antonio Hernández** *September 18, 2017 05:22*

I hope that will be possible to know more details about "n-axis setup" (n > 3) were 4th a 5th axis could have a rotatory configuration. I understand your current configuration is focused in the use of 4-7 motors as extruders. Yes, as you mentioned before, it'll be great to wait for the second version of smoothieboard, and more details about "how to" avoid that memory consumptions in older versions (if it's possible), and, also, it's difficult to establish the most common situations were memory issues are present. Thank you very much Ray !,  for your attention and for your comments about my questions related with this situation.


---
**Arthur Wolf** *September 18, 2017 09:38*

For using Smoothie for n>3, see [http://smoothieware.org/6axis](http://smoothieware.org/6axis).

Rotary axes are the same as linear axes, just using degrees instead of millimeters, it makes no difference to Smoothie at all.

There is no way to increase the amount of RAM available on Smoothie-based boards. If you run out of RAM by adding axes, you can try removing modules ( disable network, or remove switch modules ), or reducing the planner queue size from 32 to 16. I expect on a CNC setup though, you can run up to 6 axes without having to worry about RAM, so maybe only try solving this problem when you actually run into it.

[smoothieware.org - 6axis [Smoothieware]](http://smoothieware.org/6axis)


---
**Antonio Hernández** *September 18, 2017 10:46*

Ok !. The comments are very clear. Let's make it happen with our machines... Arthur, Ray, thank you very much for your help !.


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 12:11*

**+Arthur Wolf** can you advise how to disable modules? Disabling network would be a good start. 


---
**Arthur Wolf** *September 18, 2017 12:15*

I meant in the config file. You can go even further by disabling them in the Makefile too.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/SWHn7SdVW3Y) &mdash; content and formatting may not be reliable*
