---
layout: post
title: "Shared on January 17, 2017 20:25...\n"
date: January 17, 2017 20:25
category: "General discussion"
author: "Arthur Wolf"
---
[http://smoothieware.org/grbl-mode](http://smoothieware.org/grbl-mode)





**"Arthur Wolf"**

---
---
**Don Kleinschnitz Jr.** *January 18, 2017 16:57*

**+Arthur Wolf** so what does this mean to me in my application of smoothie to laser cutting/engraving and in use with LW?


---
**Arthur Wolf** *January 19, 2017 00:22*

For a laser, you can use either grbl_mode, or not use it, it depends on your taste on what the Gcode mean.

If you are going to use visicut, don't use it. For bcnc, use it. For pronterface, don't use it. For laserweb, don't use it.


---
**Don Kleinschnitz Jr.** *January 19, 2017 00:48*

**+Arthur Wolf** got it thanks.


---
**Wolfmanjm** *January 19, 2017 07:48*

actually for laserweb you need to use cnc /grbl-mode AFAIK


---
**Arthur Wolf** *January 19, 2017 10:29*

**+Wolfmanjm** so they changed the way they do homing etc ?


---
**Don Kleinschnitz Jr.** *January 19, 2017 13:25*

**+Arthur Wolf** **+Wolfmanjm** am I supposed to do anything special to get LW and smoothie to be on the same page regarding G-code mode and interpretation? i.e cnc/grbl-mode. Would the driving software (LW) set the GRBL mode?


---
**Wolfmanjm** *January 19, 2017 19:16*

you should be using the firmware-cnc build and that is grbl-mode by default so you don't need to do anything


---
**Don Kleinschnitz Jr.** *January 19, 2017 23:55*

**+Wolfmanjm** great, I am so am all set.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/4HznY6H8g7X) &mdash; content and formatting may not be reliable*
