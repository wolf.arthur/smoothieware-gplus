---
layout: post
title: "Just thinking do Smoothieware include possible to use two extruders, but only one hotend"
date: January 18, 2017 06:48
category: "General discussion"
author: "LassiVVV"
---
Just thinking do Smoothieware include possible to use two extruders, but only one hotend. Or is that planned to get work that smoothieware firmware.



I mean same kind of system with this: 

[http://www.thingiverse.com/thing:330757/#files](http://www.thingiverse.com/thing:330757/#files)

or Prusa i3 mk2 new dual/quad hotend extruder kind of system.



Thinking to use other extruder print all support material and other normal filament.





**"LassiVVV"**

---
---
**Griffin Paquette** *January 18, 2017 15:50*

It's more of a slicer thing but to answer your question yes. Smoothieware does work with this. 


---
**Wolfmanjm** *January 19, 2017 07:45*

Yes just define two extruders and one temperature control in the config.


---
**LassiVVV** *January 19, 2017 10:10*

Thanks for comments and links. Good to hear just changing Arduino based system --> Re-Arm Smoothieware system. Happy to hear that kind of two extruders works. 



If  i thinking right need to do some extra settings on "Tool change G-code" field on slic3r before that kind of two extruders work?


---
*Imported from [Google+](https://plus.google.com/112209274773346118200/posts/7oWK9buQTkV) &mdash; content and formatting may not be reliable*
