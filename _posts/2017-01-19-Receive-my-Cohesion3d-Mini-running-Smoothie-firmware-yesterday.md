---
layout: post
title: "Receive my Cohesion3d Mini running Smoothie firmware yesterday"
date: January 19, 2017 16:04
category: "General discussion"
author: "Jim Fong"
---

{% include youtubePlayer.html id="TTSkw-9NvW4" %}
[https://youtu.be/TTSkw-9NvW4](https://youtu.be/TTSkw-9NvW4)



Receive my Cohesion3d Mini running Smoothie firmware yesterday.  Bench testing with LCD panel and Ethernet web access.  No problems getting the board running at all. 



Applied Motion STM-17S-3AN high performance stepper motor with built in driver was used for speed testing.  10microstep (2000 steps per rev) 20volt supply. Using Cohesion3d external stepper adapter board. 



Step pulse output was 99,663khz.  This was the fastest I could get the firmware to reliability output a pulse train so far. Nice square wave output captured by Tek Scope and frequency counted by Fluke Multi-Counter 



Rpm meter says the stepper motor is spinning at 2990rpm



Verification 

99,663khz / 2000 x 60 = 2989.89rpm















**"Jim Fong"**

---
---
**Triffid Hunter** *January 21, 2017 06:30*

Yes the default maximum is 100kHz. There's a config setting if you want to experiment with higher rates, however we don't support changing it - ie reset it to default if anything weird happens before lodging bug reports.


---
**Jim Fong** *January 21, 2017 14:08*

**+Triffid Hunter**  thanks, Arthur confirmed that 100khz was max default. I had assumed it was higher but I can work within that limitation.  


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/RF1s77C5wh1) &mdash; content and formatting may not be reliable*
