---
layout: post
title: "So, I'm doing my usual \"let's improve the smoothie wiki\" thing, and I notice : doesn't have an Extruder section !!"
date: January 19, 2017 00:32
category: "General discussion"
author: "Arthur Wolf"
---
So, I'm doing my usual "let's improve the smoothie wiki" thing, and I notice : 



[http://smoothieware.org/3d-printer-guide](http://smoothieware.org/3d-printer-guide) doesn't have an Extruder section !!



How have people been setting up their extruders all these years ? What is going on ?!?



It's weird I forgot, but even weirder nobody pointed it out ... Is that thing about "nobody reads the documentation" really true ? Am I doing it all for nothing ? What is the meaning of life !?!





**"Arthur Wolf"**

---
---
**Alex Hayden** *January 19, 2017 00:50*

I read through the stuff I don't know. Like the bed level stuff. Or how Delta arms measurements are entered. I have read more of it buts thats just a few. Its good content and nice to have it localized on your page rather then a wiki somewhere else.


---
**Arthur Wolf** *January 19, 2017 00:52*

**+Alex Hayden** That makes sense. Note you really should read all of the guide, there is information in there that will help you make sure you do not destroy your board or your machine :)


---
**Electra Flarefire** *January 19, 2017 00:54*

We don't use extrudes.

You just cable tie on a wizard's wand to the carriage and positionally aware magic spells forms the final result. <b>*nods*</b>


---
**Arthur Wolf** *January 19, 2017 01:03*

**+Electra Flarefire** Finally somebody figured out how to 3D print metal ! Magic can do anything, so it can print metal. Problem solved ! You are a genius


---
**Jim Christiansen** *January 19, 2017 01:53*

You know **+Arthur Wolf**​, even with everything here...  I'm gonna have a few questions...  Grrr. Great wiki work though.  Thank you.


---
**Alex Hayden** *January 19, 2017 01:54*

**+Arthur Wolf**​ I did almost, but that was my lack of attention. Wired one switch vcc to ground. Almost fried the board. I killed the power as soon as I saw the magic smoke. Everything still works.


---
**Jeff DeMaagd** *January 19, 2017 03:31*

I think once you get to the extruder part in the configuration file, you've learned enough to know how to enter those numbers?


---
**Johan Jakobsson** *January 19, 2017 07:34*

I spent hours reading the wiki when I set up my first printer. For the extruder and specifically calibration if it I turned to the reprap wiki though.


---
**Arthur Wolf** *January 19, 2017 10:29*

**+Jeff DeMaagd** It'd make sense that config is enough. Well, added an extruder section anyway ...


---
**Thomas “Balu” Walter** *January 19, 2017 13:50*

42


---
**Arthur Wolf** *January 19, 2017 13:50*

**+Thomas Walter** ^^


---
**Willem Aandewiel** *February 11, 2017 13:00*

**+Arthur Wolf** .. there is so much to read and learn ... and sometimes it's easier to look at someone else his config file




---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/NWU8HEpVAEi) &mdash; content and formatting may not be reliable*
