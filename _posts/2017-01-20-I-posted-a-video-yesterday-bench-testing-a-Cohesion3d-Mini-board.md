---
layout: post
title: "I posted a video yesterday bench testing a Cohesion3d Mini board"
date: January 20, 2017 18:55
category: "General discussion"
author: "Jim Fong"
---
I posted a video yesterday bench testing a Cohesion3d Mini board.  Not sure why it didn't show up.  Anyway, I'm trying to test maximum step pulse rate and the fastest so far was 99khz. 



Increasing  x_axis_max_speed doesn't help



I set steps per mm to 200 to increase pulse rate, any more the acceleration isn't smooth.   



Is 99khz the limit or I'm missing a parameter somewhere. 



I was planning on using the controller for a future laser build with brushless servo motors. The motors have 10,000ppr encoders.  (10,000 steps pulses per revolution). 



99khz will only give me about 600rpm with these motors.   Not as much speed as I wanted.  



Just wanted to know if anyone is using their smoothieboard at a higher step pulse rate.  



Thanks. 









**"Jim Fong"**

---
---
**Arthur Wolf** *January 20, 2017 19:53*

Yes, the maximum theoretical limit is 100khz.

Note that a lot of stepper and servo drivers have limits of 100 to 250khz on their input signal. 

It doesn't matter that your encoder is 10kppr, the driver itself can receive the pulse orders at a lower rate than that ( on most drivers ) and still rotate at full speed.


---
**Jim Fong** *January 20, 2017 20:12*

**+Arthur Wolf**  ok just wanted to be sure.  These are Copley industrial drivers with max 2mhz input step pulse rate.  They do have programmable step pulse input multiplier which I will have to enable then. I'm just used to using motion control systems that are capable of several MHz pulse rates.   Thanks. 



These servos are capable of 10k rpm but I only need about 2000. 


---
**Arthur Wolf** *January 20, 2017 20:15*

**+Jim Fong** Yes, 100khz is the max v1 can do, it could theoretically do more, but we choose to have better acceleration and step generation rather than higher pulse rates because that's what is important to the vast majority of users. Smoothieboard v2 will be capable of 250-500khz, and v2-pro will be capable of going into the several-mhz.


---
**Jim Fong** *January 20, 2017 20:24*

**+Arthur Wolf** Thats great.  Most of the higher end motion boards use a fpga to do MHz speeds.  



Anyway the video I posted, which is probably in the spam folder, shows a stepper smoothly spinning at 99khz.  Atleast you know v1 can do that just fine.  



This is my first time using smoothie firmware so I am learning as I go.  Thanks.  


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 20:34*

Yeah it got caught in the spam filter of the c3d group as well. Thanks g+


---
**Arthur Wolf** *January 20, 2017 20:39*

**+Jim Fong** I believe at 99khz, doing straight motion at constant speed works fine, but doing complex shapes with multiple axes at those speeds can cause small problems, this has been tested on a regular basis when we do updates to the motion control system.


---
**Jim Fong** *January 20, 2017 20:47*

**+Arthur Wolf**  at what max step pulse range is it reliable at for multi axis movement so I know not to go above?


---
**Arthur Wolf** *January 20, 2017 20:56*

**+Jim Fong** I believe 90khz is fine.


---
**Jim Fong** *January 20, 2017 21:34*

**+Arthur Wolf** ok.good, I'm glad I did some testing before.ordering motor pulleys.  Thanks. 


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/4tMQFcyZuVx) &mdash; content and formatting may not be reliable*
