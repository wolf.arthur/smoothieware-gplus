---
layout: post
title: "Shared on January 20, 2017 22:30...\n"
date: January 20, 2017 22:30
category: "General discussion"
author: "Arthur Wolf"
---
[http://smoothieware.org/filament-detector](http://smoothieware.org/filament-detector)





**"Arthur Wolf"**

---
---
**Alex Hayden** *January 20, 2017 22:48*

Would it be possible to use an IR optical switch to trigger pause, lift and shut off heater when the switch is closed (light detected by emitter)? Then hitting play will resume printing after you have loaded new filament.


---
**Arthur Wolf** *January 20, 2017 22:50*

**+Alex Hayden** If it is just a switch, you can use the switch module : [http://smoothieware.org/switch](http://smoothieware.org/switch) to do what you want. It will trigger a pause and a custom set of G-codes when the filament out is detected, and then when you resume, it can also trigger a custom set of G-codes. It's all on the wiki.


---
**Alex Hayden** *January 20, 2017 23:01*

**+Arthur Wolf**​ thanks. Just what I wanted to see. 


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 23:01*

I have a plain roller switch with a ptfe tube and printed part doing something similar, Alex.  As Arthur said its in the switch docs, well described, and you can have an extra endstops as the resume button, but I have had some quirky behavior with that one, and I prefer to use the "resume" button that will show up at supported hosts like Pronterface and Octoprint. ![missing image](https://lh3.googleusercontent.com/6EZdp_WSWWdgZed7RwMkheP-4Q5l_MDk6_fvzP05i-rnDGbgNs62BDTiYYDsa6GzRbeBxJcTAZG-3Tc=s0)


---
**Alex Hayden** *January 20, 2017 23:18*

**+Ray Kholodovsky**​ thanks for the picture. I will probably try it both ways.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/Ksg49qYF5ij) &mdash; content and formatting may not be reliable*
