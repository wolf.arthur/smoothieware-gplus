---
layout: post
title: "This is for complete newcomers, an introduction to what Smoothie is and what it does"
date: January 20, 2017 22:51
category: "General discussion"
author: "Arthur Wolf"
---
[http://smoothieware.org/basics](http://smoothieware.org/basics)



This is for complete newcomers, an introduction to what Smoothie is and what it does.

What do you think we should add or change ?





**"Arthur Wolf"**

---
---
**Steve Anken** *January 20, 2017 23:16*

No mention of Laserweb? I have never even tried the software you mention.


---
**Arthur Wolf** *January 20, 2017 23:17*

The page has no section about laser cutting to keep it simple ( the goal is to expain the basic process, not to be exhaustive ), but our software page : [smoothieware.org/software](http://smoothieware.org/software) has laserweb


---
**Steve Anken** *January 21, 2017 02:13*

I guess because I came at it from the laser side I think of it that way. Weird how all this stuff is converging and specializing at the same time. So much work just to keep up.


---
**Arthur Wolf** *January 21, 2017 09:32*

**+Steve Anken** Eh :) Yeah, lots of converging, and lots of specializing happening. Very difficult to get the code to keep up too.


---
**Don Kleinschnitz Jr.** *January 21, 2017 14:42*

**+Arthur Wolf** tough question because you have a range of personas that consider smoothie. 



I have thought about this in the context of my blog. I have found that most people are impatient and confused by to much information to quick. 



Thinking out loud ..............



At one end of the spectrum .....

is the no expertise hobbyist that just wants a CNC/Laser/3D printer at a cheap price and they want a drop in solution to one of these ....



At the other end of the spectrum ....

is the engineer that wants a high functioning CNC/Laser/3D printer but wants to know how the guts work before making a commitment. A drop in solution is desired but not sufficient as they will want to modify it.



Uggh! Its not a one-size-fits all ....



Maybe.. instead of focus on smoothie, focus on what you can do with it. 

Example: "How to Build a Laser Cutter Using Smoothie" etc. These guides would have a tree of information that increases in complexity and become subsystem focused as one traverses downward. At the bottom of the tree is the more specific information like "smoothie schematics". At the top of the tree is more application oriented.



<s>------------------</s>

Looking back on my K40 conversion it really wasnt that bad. However getting started and learning all the pieces I needed to conclude smoothie was the right choice took a daunting amount of research and engineering expertise. Now it looks simple to me. 



Things as simple as getting the right configuration for specific applications can be daunting to some.



Still thinking ....


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/4NV24JZTUkT) &mdash; content and formatting may not be reliable*
