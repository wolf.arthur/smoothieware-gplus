---
layout: post
title: "What information do you think is missing from the Smoothie wiki ?"
date: January 20, 2017 15:52
category: "General discussion"
author: "Arthur Wolf"
---
What information do you think is missing from the Smoothie wiki ? What should we expand on ? What should be made clearer ?

Taking a minute to leave us a comment can help us make it more helpful to a lot of users. 

Thanks !





**"Arthur Wolf"**

---
---
**Maxime Favre** *January 20, 2017 16:39*

How to wire ? ;)


---
**Arthur Wolf** *January 20, 2017 16:40*

**+Maxime Favre** aha :) It's not finished yet, apparently :)

For those who don't know about it, he's doing this : [http://smoothieware.org/how-to-wire](http://smoothieware.org/how-to-wire) it's very cool.


---
**Sven Eric Nielsen** *January 20, 2017 17:24*

**+Arthur Wolf**​

More precise informations about the spare pins. What can they do and what not. (adc i. e.) 




---
**Arthur Wolf** *January 20, 2017 17:36*

+Sven [http://smoothieware.org/lpc1769-pin-usage](http://smoothieware.org/lpc1769-pin-usage) has that information, and [http://smoothieware.org/pinout](http://smoothieware.org/pinout) points at some of the capabilities too.

What do you think is missing and/or where do you think this information should be linked from ?


---
**Sven Eric Nielsen** *January 20, 2017 19:47*

Thanks but I know these already ;) 

But for me it's a little bit confusing to be honest. I simply can't find spare adc pins. So maybe there no or I'm just too blind :D



In general I would say your documentation is already quite good. But I just started with the schmoothiware topic. So obviously I'm best reference here if you ask "what could be better" 


---
**Arthur Wolf** *January 20, 2017 19:51*

**+Sven Eric Nielsen** This page : [http://smoothieware.org/lpc1769-pin-usage](http://smoothieware.org/lpc1769-pin-usage) has a list of all 8 ADC pins. And the pinout diagram helps you find where they are located. Am I missing something ?

If I understand what's missing I can add it :)



Thanks for the comment anyway.


---
**Sven Eric Nielsen** *January 20, 2017 19:59*

So as far as I understand it p0.2, p0.3, p1.30 and p1.31 are free adc pins. At least if I don't want to program the bootloader? 



But anyway, I need some more time to work with the whole topic a bit more. Too many ideas and a lot of possibilities are a quite time consuming combination :D


---
**Arthur Wolf** *January 20, 2017 20:00*

**+Sven Eric Nielsen** Yes that is correct. You don't need to change the bootloader, Smoothie configures the ADC pins on the fly as requested.

Anything else you need or think is missing just say !


---
**Sven Eric Nielsen** *January 20, 2017 20:02*

At least this I know for sure, I'll be back :D


---
**Clint** *January 21, 2017 01:28*

For me it would need some guide for dummies on Grid Compensation for cartesian printers i just cant get it to work 


---
**Arthur Wolf** *January 21, 2017 09:42*

**+Jan Tonnvik** Can you open a topic on the forum or the mailing lists about your problem ? Then we can work on getting it to work with you ( and the rest of the community ), and once we have your solution, we can use it to make the wiki better. How does that sound ?


---
**Zane Baird** *January 23, 2017 22:00*

**+Arthur Wolf** It would be nice to have a comprehensive list of all the config-override commands that can be issued. It gets a bit tiresome finding each command on a separate documentation page. Not critical, but it would be helpful to have.


---
**Arthur Wolf** *January 23, 2017 22:07*

**+Zane Baird**  They are at [http://smoothieware.org/supported-g-codes](http://smoothieware.org/supported-g-codes), do you think they need their own documentation page ?


---
**Zane Baird** *January 24, 2017 02:13*

**+Arthur Wolf**  I've noticed a couple missing from there but the only one I can see missing now (via the M503 command) is M143. Not sure if i've somehow cached a depricated page, but if I come across another that isn't included on that page I'll make a note of it. Also, 374.1 and 375.1 aren't on that page, but I suspect there is reason behind that


---
**Duane Miles** *January 30, 2017 18:56*

I could sure use some information on connecting and configuring a BL Touch. Or a simple statement that it's not supported.




---
**Arthur Wolf** *January 30, 2017 22:43*

**+Duane Miles** It's definitely supported, we put quite a lot of work with the BLtouch team into that. 

What do you feel is missing in [http://smoothieware.org/zprobe#bltouch-or-servo-retractable-touch-probe](http://smoothieware.org/zprobe#bltouch-or-servo-retractable-touch-probe) ? Tell me and I'll add it.


---
**Duane Miles** *January 31, 2017 21:43*

+Arthur Wolf, thank you for that information. I spent quite a bit of time searching, but had been unable to find it. I will follow the instructions, and report back if I have any further issues.


---
**Arthur Wolf** *January 31, 2017 21:45*

**+Duane Miles** The wiki has a search function, i think it would have worked in this case.


---
**Duane Miles** *February 01, 2017 02:52*

So I can't tell from the information provided;

Does the BL Touch also serve as the Z endstop? I see the BL Touch is connected to z-min, but you do not show the endstop portion of the configuration.

When I send a M280 S3.0, the pin drops, but when I follow it with a G31, I get "probe is not triggered".



Thanks for the help!


---
**Duane Miles** *February 01, 2017 22:44*

Just checking to see if you received my previous post from yesterday. I've been trying to get this configured for several days, and am about to return the board and go back to my RAMPS board.


---
**Duane Miles** *February 03, 2017 12:00*

Does anyone have the smoothieboard working with a BL Touch? If so, does it also serve as your Z endstop? What series of commands do you issue to perform leveling. I have not been able to get it to work with the information provided, and I have not been able to find anyone who has.


---
**Tony Sobczak** *February 10, 2017 16:12*

Following 


---
**Duane Miles** *February 10, 2017 21:19*

I've given up on it as it obviously doesn't work. Had another issue where prints would pause for no reason, and then start again. Was supposed to have been fixed in firmware, but I loaded the latest firmware, and it's still doing it. Will be pulling this board out soon.


---
**Willem Aandewiel** *February 11, 2017 12:55*

I'm new to Smoothieboard .. but I love it.



What I have not found is which configuration parameters are mandatory and which are optional. I'm using the SB for a K40 conversion and all the stuff about heating plates and what not are not used in a Laser Cutter setup. But I have found that leaving things out will prevent SB from booting.



Also: is there a way to config SB so it will alway's start the telnet or SFTP servers so, even if the config gone bad, you will be able to access the SB via the network to change or replace the config file without puling out the tiny SD card ... and maybe some logging info about whats wrong with the config.


---
**Arthur Wolf** *February 12, 2017 16:24*

**+Duane Miles** The Bltouch is just a switch like any other switch. It can be used as a probe, or as an endstop, or as both depending on how you setup your configuration file.

Hundreds of users use it as either. 

Note it's very common for Bltouches to be broken at reception or to have problems communicating over long wires, lots of reports of that, so sometimes it's not a config issue.



If you get errors with your probe, debug it's status with the M119 command, it's possible it's not read correctly.



**+Willem Aandewiel** The general rule is : modify the configuration file only as little as you need. Don't do anything more than is necessary. If you are deleting things you are probably going too far.



About config, you can download the source, edit the "config.default" file to include network configuration, compile smoothie, flash it to the board, and if config is absent, that "compiled" config file will be used.






---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/WDknMKp3j4T) &mdash; content and formatting may not be reliable*
