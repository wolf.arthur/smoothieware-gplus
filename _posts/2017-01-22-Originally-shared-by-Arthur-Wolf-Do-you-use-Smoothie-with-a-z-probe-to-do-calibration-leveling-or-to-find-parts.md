---
layout: post
title: "Originally shared by Arthur Wolf Do you use Smoothie with a z-probe to do calibration, leveling, or to find parts ?"
date: January 22, 2017 13:10
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



Do you use Smoothie with a z-probe to do calibration, leveling, or to find parts ?



You must have read [http://smoothieware.org/zprobe](http://smoothieware.org/zprobe) at some point. 

What didn't you like about it ? What did you feel was missing from it ? How do you think it could be improved ?



Any comment would help us a lot !





**"Arthur Wolf"**

---
---
**Griffin Paquette** *January 22, 2017 13:20*

Was just working with this the other day. I think it would be great if we could have a start Gcode example for each of the autolevel if strategies so new users understand how they should set up their printer. Same way you did it before where one is 3 point, one is... and so on and so forth. 


---
**Arthur Wolf** *January 22, 2017 13:36*

**+Griffin Paquette** Just added what you said, however I am not 100% sure I have it right. If users reading this can check the examples, compare it to their process, and even better post their process here, that would be awesome.


---
**Griffin Paquette** *January 22, 2017 13:39*

I'm out of the house right now but I'll send you mine later. Using 3 points strategy. 


---
**Arthur Wolf** *January 22, 2017 13:41*

**+Griffin Paquette** Thanks !


---
**Christian Lossendiere** *January 22, 2017 14:30*

Can use zprobe in smoothieboard for curve surface and not only for compensation for flat surface with 3 point ?

Like in this video 
{% include youtubePlayer.html id="ApBfHW7AhsM" %}
[youtube.com - CNC surface scanning / auto leveling](https://www.youtube.com/watch?v=ApBfHW7AhsM)


---
**Arthur Wolf** *January 22, 2017 14:35*

**+Christian Lossendiere** Yes of course, it's a core feature of Smoothie, and it is documented at the page this post is all about : [smoothieware.org/zprobe](http://smoothieware.org/zprobe)


---
**Christian Lossendiere** *January 22, 2017 17:41*

Ah thanks Arthur, now i see is Grid Compensation


---
**Dushyant Ahuja** *January 22, 2017 18:59*

On grid leveling for Cartesian printers / the guide mentions that you need to have home at 0,0. However most cartesians have 0,0 at lower left. The guide mentions use of user space to overcome this. An example of how that can be done would be great. I don't have a CNC background and simply could not understand how to use user spaces  


---
**Arthur Wolf** *January 22, 2017 19:05*

**+Dushyant Ahuja** It's explained, in the cartesian grid levelling ( [http://smoothieware.org/zprobe#toc26](http://smoothieware.org/zprobe#toc26) ), at the "centering the bed" section. Do you think it needs improvement ?


---
**Dushyant Ahuja** *January 22, 2017 19:32*

Yes please. If you give example commands. That would be great. 


---
**Arthur Wolf** *January 22, 2017 19:33*

**+Dushyant Ahuja** It has example commands. Have you looked at it recently ?


---
**Dushyant Ahuja** *January 22, 2017 19:35*

Specifically. How to set the coordinates temporarily and after getting the grid setting it back to "normal"


---
**Arthur Wolf** *January 22, 2017 19:36*

**+Dushyant Ahuja** You need to have the coordinates at the center of the bed permanently for grid leveling to work.


---
**Dushyant Ahuja** *January 22, 2017 19:39*

**+Arthur Wolf** hmmmm I thought you could set it back to lower left. Not fun. Would have to change slicer settings. 


---
**Arthur Wolf** *January 22, 2017 19:41*

**+Dushyant Ahuja** Yep exactly.


---
**Wolfmanjm** *January 22, 2017 20:53*

no you don't need to change slicer settings. machine coordinates need to be 0,0 center, but you can permanently set the workspace coordinates to 0,0 front left using G92 as explained in the wiki.


---
**Dushyant Ahuja** *January 22, 2017 23:45*

**+Wolfmanjm** I guess that is the step I never understood. Still don't :-(


---
**Wolfmanjm** *January 23, 2017 00:22*

set up homing so that 0,0 is center of bed when it homes (in a cartesian this will be something like setting min endstops to -120 or something). Then jog to where you want 0,0 to be when you print and issue G92 X0 Y0, now When you do G1 X0 Y0 it will go to that new spot. then print as normal. by setting the config variable save_g92 true you can then save that with M500 and not have to do it every time you power on. (By convention by default g92 is not saved).


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/9fGeFukvgkU) &mdash; content and formatting may not be reliable*
