---
layout: post
title: "what would cause the green wire on stepper driver 5 to melt on two seperate cables at .4 amps?"
date: January 25, 2017 08:37
category: "General discussion"
author: "Nick Lindenmuth"
---
what would cause the green wire on stepper driver 5 to melt on two seperate cables at .4 amps?





**"Nick Lindenmuth"**

---
---
**Anthony Bolgar** *January 25, 2017 08:39*

Short circuit?


---
**Nick Lindenmuth** *January 25, 2017 08:47*

must be, but i have no idea how to diagnose it further. ive tried two different motors, two different cables which are wired the same as my primary extruder which works fine. i tested it before i ziptied everything back up and it worked.  but now it doesn't 


---
**Nick Lindenmuth** *January 25, 2017 08:50*

is 26awg wired too small?


---
**Anthony Bolgar** *January 25, 2017 08:55*

I would use 20 or 22awg


---
**Maxime Favre** *January 25, 2017 09:19*

26AWG is rated at 3.5A (found different values on the web). Plus you'all have voltage drops. Upgrade your cables




---
**Basile Laderchi** *January 25, 2017 11:47*

**+Maxime Favre** according to this chart ([http://www.powerstream.com/Wire_Size.htm](http://www.powerstream.com/Wire_Size.htm)) the maximum amps for power transmittion for 26awg is 0.361A and not 3.5A.

I would agree with the sizes that **+Anthony Bolgar** suggests.

[powerstream.com - American Wire Gauge table and AWG Electrical Current Load Limits
		with skin depth frequencies and wire breaking strength](http://www.powerstream.com/Wire_Size.htm)


---
**Maxime Favre** *January 25, 2017 11:53*

**+Basile Laderchi**​ whoops typo. Thanks for correction


---
**Nick Lindenmuth** *January 25, 2017 12:22*

just ordered some 20awg 4 conductor tv satellite wire on ebay


---
**Triffid Hunter** *January 26, 2017 09:12*

Another possibility is a damaged driver. 0.4A motors are rather poorly matched to smoothie's drivers and, when spun manually, can deliver some destructively high voltages to the drivers and damage them


---
*Imported from [Google+](https://plus.google.com/+NickLindenmuth13/posts/Z3gs15Gqcjq) &mdash; content and formatting may not be reliable*
