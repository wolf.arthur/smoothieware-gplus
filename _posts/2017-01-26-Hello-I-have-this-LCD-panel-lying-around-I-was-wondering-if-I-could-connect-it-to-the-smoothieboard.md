---
layout: post
title: "Hello I have this LCD panel lying around I was wondering if I could connect it to the smoothieboard"
date: January 26, 2017 22:51
category: "General discussion"
author: "jacob keller"
---
Hello



I have this LCD panel lying around I was wondering if I could connect it to the smoothieboard.



How would one do this?



It came off one of my old printers.



Thanks



![missing image](https://lh3.googleusercontent.com/-SfEiwPQLNcg/WIp9WfcX6lI/AAAAAAAAAmI/bHCxQlKrEjg4-jEsDZZkFghlbFpjPUPuACJoC/s0/IMG_20170126_164111989.jpg)



**"jacob keller"**

---
---
**Brent Crosby** *January 27, 2017 05:03*

If you knew what controller it has, there might be a chance of bringing it up. Then there would be the job of integrating that particular controller into the software - - a considerable amount of work. In short, not very practical. 


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/SxM3dWj2orq) &mdash; content and formatting may not be reliable*
