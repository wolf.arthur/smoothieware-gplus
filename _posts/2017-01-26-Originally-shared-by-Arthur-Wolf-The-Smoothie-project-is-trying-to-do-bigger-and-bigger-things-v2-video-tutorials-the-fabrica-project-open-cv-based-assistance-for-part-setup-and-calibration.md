---
layout: post
title: "Originally shared by Arthur Wolf The Smoothie project is trying to do bigger and bigger things ( v2, video tutorials, the fabrica project, open-cv based assistance for part setup and calibration )"
date: January 26, 2017 15:18
category: "General discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



The Smoothie project is trying to do bigger and bigger things ( v2, video tutorials, the fabrica project, open-cv based assistance for part setup and calibration ). The bigger the projects get, the easier it becomes to actually pay people to do things, but the harder it is to find the money ( clones stealing revenues from the companies that usually finance this stuff, isn't helping ).



I'm thinking about setting up a Patreon. It'd be in my name ( I think Patreon requires this ) but I'd mostly use any money it generates to pay others ( have to check if Patreon allows this ).

What do you folks think ? Would it even work ?





**"Arthur Wolf"**

---
---
**Sébastien Mischler (skarab)** *January 26, 2017 15:43*

It is an idea to discuss / develop


---
**Steve Anken** *January 26, 2017 16:20*

This is a good idea. Funding is a problem and the community needs to be more creative about how they get funded.


---
**Anton Fosselius** *January 26, 2017 21:19*

sounds like a good idea :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/Hyp3t8gbu5e) &mdash; content and formatting may not be reliable*
