---
layout: post
title: "Hello, So I did another test using a Marvin This is what I got I looks like the print got stretched when it was printing?"
date: January 29, 2017 03:28
category: "General discussion"
author: "jacob keller"
---
Hello,



So I did another test using a Marvin



This is what I got



I looks like the print got stretched when it was printing?



Any ideas why it did this



![missing image](https://lh3.googleusercontent.com/-5Ah1UAm8AmE/WI1hTXb1a3I/AAAAAAAAAnQ/lunmwnY24_YC_mPVUrTj6P8F-hetNNpNwCJoC/s0/IMG_20170128_212420765.jpg)
![missing image](https://lh3.googleusercontent.com/-t5kasvTxiEc/WI1hTTtIzQI/AAAAAAAAAnQ/vAwPW3kTViIL_vSDS1-Wr5cmZLFMDqXJQCJoC/s0/IMG_20170128_212457003.jpg)

**"jacob keller"**

---
---
**jerryflyguy** *January 29, 2017 03:39*

I'd guess steps per inch is your issue? Either Z, or both x/y (assuming its proportional in both axis?)


---
**jacob keller** *January 29, 2017 03:43*

**+jerryflyguy** Does starting g-code have to do with something with this? I think I have my X steps per mm at 80 and my Y is at 120. 


---
**jerryflyguy** *January 29, 2017 04:02*

Have you made any calibration cubes or items you could measure ? I'd start there, that way you could measure the final part and figure if it steps or what. 



Not sure about starting gcode. There are M codes for scaling various axis in milling but no idea if it is used in printing ? 


---
**jacob keller** *January 29, 2017 04:03*

Will do.



Thanks for your time


---
**jerryflyguy** *January 29, 2017 04:04*

**+jacob keller** anytime! Not sure I solved anything but hope it helps? something to check at least?


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/Qo9SbJKDoLN) &mdash; content and formatting may not be reliable*
