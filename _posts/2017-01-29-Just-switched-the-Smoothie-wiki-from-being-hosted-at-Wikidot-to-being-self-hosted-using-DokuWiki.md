---
layout: post
title: "Just switched the Smoothie wiki from being hosted at Wikidot, to being self-hosted using DokuWiki"
date: January 29, 2017 14:25
category: "General discussion"
author: "Arthur Wolf"
---
Just switched the Smoothie wiki from being hosted at Wikidot, to being self-hosted using DokuWiki.

The URL doesn't change : [http://smoothieware.org/](http://smoothieware.org/) and in theory, not much else should change.

It was a lot of work and translation between wiki formats. If you find something that is broken, don't hesitate to tell me, or to edit it yourself.

One good thing that comes from this, is the wiki no longer requires creating an account to edit, you can just edit it instantly, right now.





**"Arthur Wolf"**

---
---
**Michael Andresen** *January 29, 2017 16:41*

Found a broken url to a image [smoothieware.org - delta [Smoothieware]](http://smoothieware.org/delta) under "Delta kinematics basic".



And "The two most important parameters are as follow :Â" and "to be 120Â° apart" a bit below that


---
**Michael Andresen** *January 29, 2017 16:43*

Oh, the blog is also a bit odd

![missing image](https://lh3.googleusercontent.com/YTN8FpAPCybuEjyhf-eIlOaqoJASI-atlwdO2NojZRAqJgqeNv7H0nDDAky1kaf-gmuzP_lGsF8X-f7FTa8gbeB95IrFvhsW5kU5=s0)


---
**Michael Andresen** *January 29, 2017 16:43*

And gallery


---
**Philipp Tessenow** *January 29, 2017 19:10*

Great to be able to add a revision to an article without setting up an account...



I only got a problem accessing via mobile see picture. I can't get the navigation to hide. 



![missing image](https://lh3.googleusercontent.com/i9Y4v5j1UpaU3dwSp9125u5_GbFQtWNvEBj0m72acEHZ7xuVIoJLtpjtzYFcVXx5hGaiUoG-ilOpJg=s0)


---
**Stephane Buisson** *January 29, 2017 19:33*

[smoothieware.org - blue-box-guide [Smoothieware]](http://smoothieware.org/blue-box-guide)

lost it's imges


---
**Arthur Wolf** *January 29, 2017 23:20*

**+Stephane Buisson** Noted, thanks. Seems a lot of pages have, will go over all of them /o\


---
**Glenn Beer** *February 09, 2017 20:50*

broken link to delta.png fixed


---
**Arthur Wolf** *February 12, 2017 17:13*

**+Glenn Beer** Thanks ! Don't hesitate to do some more of that :p


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/dd6CZpp3WkJ) &mdash; content and formatting may not be reliable*
