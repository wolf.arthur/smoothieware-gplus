---
layout: post
title: "New to Smoothieware here. I just received a Re-Arm board and am building a D-Bot"
date: January 29, 2017 17:55
category: "General discussion"
author: "Geoffrey Forest"
---
New to Smoothieware here. I just received a Re-Arm board and am building a D-Bot. I'm having trouble with homing. The D-Bot homes Y to max, which is what seems to be giving me trouble. I've got Alpha, Beta, and Gamma endstops working correctly (checked with M119). X homes correctly (to min). Z homes correctly (to min). I can then move manually away from X and Z endstops with host software. Y homes to max fine, but then Repetier says printer Y axis is at position 0 (instead of 300, which is the defined max in config). If I try to move away from Y max endstop, the print head moves all the way to the actual Y 0 (all the way across the print bed, and I can't stop it), then I can move it normally in manual mode. Is this normal?





**"Geoffrey Forest"**

---
---
**Arthur Wolf** *January 29, 2017 18:18*

Can we see your config file ( on [pastebin.com - Pastebin.com - #1 paste tool since 2002!](http://pastebin.com) ) ?


---
**Geoffrey Forest** *January 29, 2017 19:09*

Sure thing. First time trying pastebin, hope I did it right:

[http://pastebin.com/jEMVyEV5](http://pastebin.com/jEMVyEV5)

FYI, I've only got through the movement parts so far. I haven't tested heaters yet. This is the sample config that was issued with the Re-Arm board that I tweaked for my build.

Thanks for helping!


---
**Wolfmanjm** *January 31, 2017 07:54*

try using pronterface or octoprint, basically repetier host is closed source and I believe does not support smoothie properly. If when using proneterface you still have the problem let us know.


---
**Geoffrey Forest** *January 31, 2017 13:05*

Okay, I'll try pronterface. You're right, repetier does seem a little glitchy with smoothie. I couldn't get repetier-host to connect at all via serial connection, however repetier-server connected fine. But I thought once connected, all it did was pass gcode commands, so I didn't think it was the host. I'll try another.


---
**Arthur Wolf** *February 01, 2017 17:23*

Your config looks fine, this just shouldn't be happening. Can you try updating your firmware to the very latest version please ?


---
**Geoffrey Forest** *February 07, 2017 17:53*

Just following up, using Octoprint to manually control printer movements results in proper function. All axes home and then move correctly afterward. Something screwy is happening with Repetier-server controlling the printer. I'll post this over in the Repetier forums and if any progress comes of it, I'll report back. Thanks for the help!


---
**Geoffrey Forest** *February 09, 2017 03:26*

Ok, the developer for Repetier set me straight. There is a setting in Repetier Host and Server to define the position after homing. Just had to change the defined Y home position in Repetier's printer settings. Seems to work like normal now.


---
**Arthur Wolf** *February 12, 2017 17:42*

**+Geoffrey Forest** Would you mind giving me as complete a description of what you had to do to get it all to work, so I can add the info to the documentation ?


---
**Geoffrey Forest** *February 16, 2017 02:50*

**+Arthur Wolf** Sure thing. But to be clear, it was just my setting in Repetier that needed to be adjusted. The Smoothie config was fine.

In general, after homing, Repetier assumes the home position of the printer based on your settings. 



In Repetier-Host, open Printer Settings, and in the tab "Printer Shape" you can choose Max, Min, or 0 as the defined position after homing. I needed to change my Y position to Max. You also set the general printer shape and size here as well.



In Repetier-Server, open Printer Settings, and in the tab "General", go down to the "Manual Control" section, and there you define the X-Home Position, Y-Home Position, and Z-Home Position as actual numbers (I set my Y-Home to 300). You also set the printer size here to define the limits for manual control. There is also a "Printer Shape" tab where you define the bed shape and size. A little weird that you set this in two places. I assume it serves two different purposes.



Hope this is helpful to someone. I know you're not crazy about Repetier because its closed source, but it has some nice features, is being actively developed, and has pretty good support on their forums. I do like Octoprint as well. Repetier-Server makes it easy to run two printers and cameras, so I'm using that for now (not savvy enough with Linux to get two instances of Octoprint running).


---
**Arthur Wolf** *February 16, 2017 09:43*

Thanks, I'll point users with similar problems at this post, thanks a lot for the information.


---
*Imported from [Google+](https://plus.google.com/+GeoffreyForest/posts/Fx3AjTQ9t8x) &mdash; content and formatting may not be reliable*
