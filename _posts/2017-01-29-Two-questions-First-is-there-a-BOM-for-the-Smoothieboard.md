---
layout: post
title: "Two questions: First, is there a BOM for the Smoothieboard?"
date: January 29, 2017 14:29
category: "General discussion"
author: "Guy Bailey"
---
Two questions:



First, is there a BOM for the Smoothieboard? I burned up two of the small MOSFETs and cant find the part numbers. 



Next, what are the max values for the PID settings? 



Thanks!





**"Guy Bailey"**

---
---
**Arthur Wolf** *January 29, 2017 14:42*

You can find the link to the BOM at the bottom of the Smoothieboard page : [smoothieware.org - smoothieboard-v1 [Smoothieware]](http://smoothieware.org/smoothieboard-v1)

About PID factors, they are stored as floats, so the maximum values are extremely big : [https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/tools/temperaturecontrol/TemperatureControl.h](https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/tools/temperaturecontrol/TemperatureControl.h) ( see [https://en.wikipedia.org/wiki/Single-precision_floating-point_format](https://en.wikipedia.org/wiki/Single-precision_floating-point_format) )


---
**Guy Bailey** *January 29, 2017 14:43*

Thanks Arthur!




---
*Imported from [Google+](https://plus.google.com/+GuyBailey/posts/9pPgD55y3oc) &mdash; content and formatting may not be reliable*
