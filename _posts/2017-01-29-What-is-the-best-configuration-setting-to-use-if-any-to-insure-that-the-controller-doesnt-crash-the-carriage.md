---
layout: post
title: "What is the best configuration setting to use (if any) to insure that the controller doesn't crash the carriage"
date: January 29, 2017 18:05
category: "General discussion"
author: "Don Kleinschnitz Jr."
---
What is the best configuration setting to use (if any) to insure that the controller doesn't crash the carriage.

I am looking at all the end-stop settings trying to figure out which one(s) to set to so the controller does not overrun the working space on an error or if I send it stupid G-codes that are outside the workspace.

Can I do this with configuration settings or do I have to add 2 more end-stops?





**"Don Kleinschnitz Jr."**

---
---
**Arthur Wolf** *January 29, 2017 18:19*

That's called "soft endstops", it's the last "classic" feature smoothie doesn't have, because Smoothie's other features make it complex to add "properly". We are working on it.

in the meantime, you need to add endstops and use the "limit switches" feature.


---
**Arthur Wolf** *November 12, 2017 15:01*

Actually, there is now a candidate implementation of soft endstops, if you could test it and report how well it works it'd be very helpful.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/Dw2z6rxms7b) &mdash; content and formatting may not be reliable*
