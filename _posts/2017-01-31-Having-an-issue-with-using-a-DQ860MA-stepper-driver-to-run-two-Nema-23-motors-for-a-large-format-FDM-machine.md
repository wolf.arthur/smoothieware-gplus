---
layout: post
title: "Having an issue with using a DQ860MA stepper driver to run two Nema 23 motors for a large format FDM machine"
date: January 31, 2017 14:28
category: "General discussion"
author: "Tom Mertens"
---
Having an issue with using a DQ860MA stepper driver to run two Nema 23 motors for a large format FDM machine. I have it wired up via the drivers diagram and by smoothiewares diagram, using a pin from the X min endstop to bump the signal to the driver to 5v. 



The motors don't show any sign of engaging and using a multi-meter shows that there isn't current getting to the motors.



The motors work independently when using them with the board. 



It might be that the pins aren't giving the right signal (or any) but I wouldn't be able to tell because I'm not sure what they should be reading. 



![missing image](https://lh3.googleusercontent.com/-HPiPobPFc3s/WJCfDJP_B3I/AAAAAAAAO3o/G96CGGuC8eQok3BIJ17TkEa8Pw3Q1RjggCJoC/s0/IMG_20170130_115102.jpg)
![missing image](https://lh3.googleusercontent.com/-hwM19yTsJ8Q/WJCfDIjJkEI/AAAAAAAAO3o/AtcV8ZuinGAh5YiOiNrd8GYbbbaQ_EgbACJoC/s0/IMG_20170130_154247.jpg)
![missing image](https://lh3.googleusercontent.com/-g8WKi5GEf1Y/WJCfDFXwRQI/AAAAAAAAO3o/ugw76KrfRJAPlAlYcGdY9v1GVCBBtYFQgCJoC/s0/IMG_20170130_160509.jpg)

**"Tom Mertens"**

---
---
**Maxime Favre** *January 31, 2017 15:03*

Driver leds showing something ?

As you use common anode (not sure why), have you added the o in the config ? ( alpha_step_pin   2.0o)


---
**Tom Mertens** *January 31, 2017 15:16*

The LED on the driver is solid green. I've also added the o to the config. 



I'm using common anode as the driver requires a 5V signal that the 4 pins along can't, only 3.3V. 


---
**Maxime Favre** *January 31, 2017 15:22*

Strange. If you enable/disable (M17/M18) motors the leds go from green to red ? I use D542MA with a smoothiebrainz (which provide 3.3V) with common ground.


---
**Tom Mertens** *January 31, 2017 15:34*

I don't get a light change no. Using a multimeter on the first two pins (going to ENBL - and DIR -) reads 3.3v when the motors aren't enabled and 0.0v once they are. 


---
**Maxime Favre** *January 31, 2017 16:13*

Looks legit. +5V ok ? If yes I don't know. The driver state should change if you have 3.3v or 0v on the pin. Maybe try common ground wiring. The driver use optocoupler so 3.3v should works.




---
**Tom Mertens** *January 31, 2017 16:19*

I'll give it a go. Thanks for your time. 


---
**Morghan Jolly** *February 04, 2017 15:31*

The board might not output enough mA to run the driver logic properly? I'm currently planning on using these so that I have enough logic current.

[pololu.com - Pololu - Logic Level Shifter, 4-Channel, Bidirectional](https://www.pololu.com/product/2595)


---
*Imported from [Google+](https://plus.google.com/106645344682318646186/posts/KpKkkGsGBRz) &mdash; content and formatting may not be reliable*
