---
layout: post
title: "Need some assistance....... Originally shared by Anthony Bolgar I am in need of a Laser power meter that can measure at least 50W from a CO2 laser tube"
date: January 31, 2017 00:28
category: "General discussion"
author: "Anthony Bolgar"
---
Need some assistance.......



<b>Originally shared by Anthony Bolgar</b>



I am in need of a Laser power meter that can measure at least 50W from a CO2 laser tube. I was hoping the community had it in the to rally and raise $125 USD to get a power meter for the following reason:



I posted the other day that I will be running some controlled testing for determining average tube life in 2 scenarios: 



1. Being conservative and never running the tube past 18Ma

2. Treating the tube as a consumable and running it at max as much as possible to decrease the number of hours actually being put on it because it can run faster mm/s than at low power. 



Still some details to iron out to make the testing as accurate and fair as possible, but one item has vaulted to the forefront. To be able to accurately measure tube degradation, I will need to first measure the tube output power before being used as a baseline, and then measure the output power of both tubes after each job has been run (will run identical jobs on each machine, only difference will be the consumable machine will run at max power with a much higher travel rate.)  



That is why I need is a laser power meter capable of measuring 50W from a Co2 laser tube. This will be a long term study (months, possibly up to 2 years) so borrowing one is really out of the question. I was hoping the usual band of suspects could rally to my aid to get a power meter to use for this testing. Once  I have completed the testing I would be willing to share it out to people for short term use to measure their own machines output (really helpful to determine if you need to align the optics or is or a tube or PSU issue)





**"Anthony Bolgar"**

---


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/LF7WYc4ur8f) &mdash; content and formatting may not be reliable*
