---
layout: post
title: "Setting up a Smoothieboard for the first time"
date: January 31, 2017 23:42
category: "General discussion"
author: "Greg Nutt"
---
Setting up a Smoothieboard for the first time.  Almost got everything working.  However, the extruder is not extruding appropriately.



I have a 200 step motor, 16 microstep stepper and a E3D Titan extruder.  All these together I understand would be a 418.5 steps/mm.



Here is my extruder configuration:



## Extruder module configuration

extruder.hotend.enable                          true             # Whether to activate the extruder module at all. All configuration is ignored if false

extruder.hotend.steps_per_mm                    418.5              # Steps per mm for extruder stepper

extruder.hotend.default_feed_rate               600              # Default rate ( mm/minute ) for moves where only the extruder moves

extruder.hotend.acceleration                    500              # Acceleration for the stepper motor mm/sec²

extruder.hotend.max_speed                       50               # mm/s



When I tell the extruder to manually feed 100mm of filament with this configuration, I get about 35mm.  I tried tripling the steps_per_mm in the configuration (to 1245.5) and tried to feed 100mm of filament again.  I got the EXACT same result of about 35mm (yes, I reset the Smoothieboard by powering it off and back on again).  So, what does this mean???







**"Greg Nutt"**

---
---
**Greg Nutt** *February 01, 2017 01:13*

In fact, it appears to be exactly the same issue as previously reported here:  [webcache.googleusercontent.com - extruder/print problem - Smoothie Project](http://webcache.googleusercontent.com/search?q=cache:http://smoothieware.org/forum/t-1315217/extruder-print-problem&usg=AFQjCNFQwYgHyzf7n3ORrHheLjN-yaP_7Q&sig2=pTcuamv18W6TyxBufJRayw)



Doesn't look like a solution was reported here for it.



I will mention that using the exact same slicing settings for Marlin didn't produce this problem.


---
**Greg Nutt** *February 01, 2017 01:32*

One step further which I believe is the answer.  The default extruder.hotend.steps_per_mm  for Smoothieware is 140.  This is almost EXACTLY 1/3 of mine and the fellow in the above link.  And my extruder is extruding 1/3 of the amount of filament it is supposed to be.  Which must mean that Smoothieware is still using the default value of 140 for extruding in my case instead of the value I have placed in it of 418.5.



The question now (again) is why?


---
**Jeff DeMaagd** *February 01, 2017 04:26*

Are you factoring in that the Titan has a 3:1 gear ratio? Or is your slicer set to 2.85mm filament when you are using 1.75mm?


---
**Wolfmanjm** *February 01, 2017 05:53*

do you have an override file? if you do then changing the config will not do anything, check with M503, if it is active then either use M92 Exxx to set steps/mm or do M502 to delete the override file. BTW this is explained on the wiki in pretty much the first section on config.


---
**Greg Nutt** *February 01, 2017 13:42*

**+Wolfmanjm** Thanks for the reminder.  I'll check that again.  Definitely different from how Marlin works (in a good way, but requires a change in perspective).


---
**Greg Nutt** *February 01, 2017 14:00*

In a quick search on the [smoothieware.org - configuring-smoothie [Smoothieware]](http://smoothieware.org/configuring-smoothie) page, config overrides, although mentioned in the table of contents, are not referred to at all until the end of the page btw.



I'd almost recommend having a section labeled For Marlin users to draw attention to specific differences these users would need to look for specifically.


---
*Imported from [Google+](https://plus.google.com/+GregNuttTech/posts/SRi3DwhzgHU) &mdash; content and formatting may not be reliable*
