---
layout: post
title: "Hi Guys, i need some help. Im trying to setup a laser diode on a router, all controlled by an x5 mini (v1)"
date: February 01, 2017 13:19
category: "General discussion"
author: "Nicolas Arias"
---
Hi Guys, i need some help.



Im trying to setup a laser diode on a router, all controlled by an x5 mini (v1). I can move x/y/z ok. 



Im failling miserably trying to control the laser. Im using a chinese (bangpod) diode with its driver. If i use a cncshield with grbl it works ok, but when i try to use it on the x5 mini, i fail.



from my config.h:

extruder.hotend.enable                          false  

laser_module_enable                          true 

#laser_module_pin                             2.4  



2.4 is the extruder (on the x5 mini). If i connect the pwm signal cables the psu gets shorted. Same happens if i connect it to the fan pins.



any hints?



thanks







**"Nicolas Arias"**

---
---
**Don Kleinschnitz Jr.** *February 01, 2017 14:03*

Would need to know what the interface to the laser diode is. Schematics?


---
**Nicolas Arias** *February 01, 2017 14:11*

The white connector is power, the blue pwm

![missing image](https://lh3.googleusercontent.com/JiLtE4JJnsAUsXTSC459udPlJubrUXRcuxvlySwj3EAMVqQvaNLLVkfKbAx3Ea1GpHd_GTUBWi95__5aTp00pC5yMrXCusl5_TJt=s0)


---
**Don Kleinschnitz Jr.** *February 01, 2017 14:28*

**+Nicolas Arias** I would need to know what circuit it is driving, not just the wiring, that's why I was hope-ing for a schematic of the driver.

Can you point me to the place you got it?


---
**Nicolas Arias** *February 01, 2017 14:35*

**+Don Kleinschnitz**, got it from banggood:

[http://www.banggood.com/450nm-3500mW-3_5W-Blue-Laser-Module-With-TTL-Modulation-for-DIY-Laser-Cutter-Engraver-p-1103261.html?rmmds=myorder](http://www.banggood.com/450nm-3500mW-3_5W-Blue-Laser-Module-With-TTL-Modulation-for-DIY-Laser-Cutter-Engraver-p-1103261.html?rmmds=myorder)


---
**Don Kleinschnitz Jr.** *February 01, 2017 16:05*

**+Nicolas Arias**​ here is what I would try but I am guessing :).



Use P2.5 and I think inverting.

If diode stays on try noninverting.



Assumes: 

...PWM high (5V) turns it on

...PWM Low(gnd) turns it off





![missing image](https://lh3.googleusercontent.com/Ykh2l-oFUGo34AxV9VVVqJOkHuYTR-tzb-0AKQAPGioFbBeB_NykLmkJMzPp4g1Yy3VLyqSJNkFMCl9LYmC6ExvMTe3Zh5NhFYpl=s0)


---
**Don Kleinschnitz Jr.** *February 01, 2017 16:05*

**+Nicolas Arias**  schematic version showing entire circuit.

![missing image](https://lh3.googleusercontent.com/qQ011tcVfLpN42aif-E0F4qZnCTu0xEPtawC8EIQzVVKxqwzURA_iwhqTgdKLdB8iP5Jro2eNK-qmFIPJrppw0A32i_KM-IOkY73=s0)


---
*Imported from [Google+](https://plus.google.com/+NicolasArias/posts/iDEffdx1DgH) &mdash; content and formatting may not be reliable*
