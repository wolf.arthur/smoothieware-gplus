---
layout: post
title: "Lil Help? First time Smoothie user using the Re-Arm"
date: February 01, 2017 18:19
category: "General discussion"
author: "Ax Smith-Laffin"
---
Lil Help? First time Smoothie user using the Re-Arm. I have 2 questions, the first I'm not sure if it's something I've done configuration-wise or something else.



Config @ Pastebin -[http://pastebin.com/vGueTw5b](http://pastebin.com/vGueTw5b)



Basically I have an SSR to control power On/Off by M80/M81. Now when I go to switch the printer on via M80 the first time it fails. Checking the terminal I get "Kill button pressed - reset or M999 to continue"  Issuing another M80 gives "UnKill button pressed Halt cleared" and the printer switches on normally. I've not used the PS_ON pin 2.12 for anything else so I'm not sure exactly what's going on. 



Secondly, I have a guided Levelling script I've written, that uses M0, wait for input, which I used to run off of the SD Card on Marlin. I've added this to the custom menu, however, after reading up, M0 isn't supported, is there anything within Smoothie GCode-wise that will set wait for user input?





**"Ax Smith-Laffin"**

---


---
*Imported from [Google+](https://plus.google.com/+AxSmithLaffin118/posts/YAfpAnqBy12) &mdash; content and formatting may not be reliable*
