---
layout: post
title: "Openbuilds to the rescue! Originally shared by Anthony Bolgar Once again, Mark Carew and the Openbuilds Fairshare program have stepped up to the plate and are covering the cost of the Laser Power Meter to enable the testing"
date: February 03, 2017 13:47
category: "General discussion"
author: "Anthony Bolgar"
---
Openbuilds to the rescue!



<b>Originally shared by Anthony Bolgar</b>



Once again, **+Mark Carew** and the Openbuilds Fairshare program have stepped up to the plate and are covering the cost of the Laser Power Meter to enable the testing of mA vs. life expectancy of laser tubes.



I have almost completed designing the testing procedure to determine if running a CO2 laser tube at low mA actually prolongs the life of the tube vs running at high mA. The hypothesis is that while running at high mA, the shortened tube life is made up by the increased mm/s higher mA would allow. I have 2 identical machines to run the experiment on, so the data collected should be fairly accurate. This will be a long term project as the tube life may be in the hundreds if not thousands of hours of run time. But I will provide weekly power outputs for both variants, and hopefully we can see an emerging trend quickly.



Once again, thanks to Mark and the Openbuilds team!





**"Anthony Bolgar"**

---


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/KepJvwGDwTg) &mdash; content and formatting may not be reliable*
