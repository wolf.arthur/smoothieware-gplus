---
layout: post
title: "First Smoothieboard arrived, v1.1 5xC. Upgrading the controller on my WeldingRodBot, used to be a Mega2560 DRV8825 on Repetier, at 12VDC (it will move to my 1344mm^2 OpenBuilds 4W blue diode laser cutter, hopefully on GRBL"
date: February 04, 2017 23:05
category: "General discussion"
author: "Jason Frazier"
---
First Smoothieboard arrived, v1.1 5xC.  Upgrading the controller on my WeldingRodBot, used to be a Mega2560 DRV8825 on Repetier, at 12VDC (it will move to my 1344mm^2 OpenBuilds 4W blue diode laser cutter, hopefully on GRBL 1.1).  Few questions:



1) The step drivers appear to be heatsinked to Smoothieboard.  Does Smoothieboard require cooling for five ~80oz NEMA17 steppers @12V 1.5A?  If so, fan orientation?



2) Smoothieware site says "regular" 5VDC regulators are subpar when installed on the Smoothieboard, especially if a GLCD is attached.  I am using STMicro L7805CV, which is rated to 1A, or 1.5A if supplemental heatsinking is provided.  Can I expect problems?



3) I had considered buying TL-Smoother to help reduce noise and skipped microsteps on my DRV8825's, is this still relevant on A5984 for Smoothieboard 1.1?

[https://www.aliexpress.com/store/product/Trianglelab-4-pieces-pack-TL-Smoother-new-kit-addon-module-for-3D-pinter-motor-drivers-free/1654223_32787567456.html](https://www.aliexpress.com/store/product/Trianglelab-4-pieces-pack-TL-Smoother-new-kit-addon-module-for-3D-pinter-motor-drivers-free/1654223_32787567456.html)







**"Jason Frazier"**

---
---
**Jeff DeMaagd** *February 05, 2017 17:41*

The TL Smoother is only to fix a certain problem with TI's drivers, the Allegro drivers don't have that particular problem.


---
*Imported from [Google+](https://plus.google.com/109582687148983964093/posts/Nj4mWszYhpo) &mdash; content and formatting may not be reliable*
