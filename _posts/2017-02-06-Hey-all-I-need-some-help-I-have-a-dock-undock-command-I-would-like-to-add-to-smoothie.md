---
layout: post
title: "Hey all. I need some help I have a dock/undock command I would like to add to smoothie"
date: February 06, 2017 02:42
category: "General discussion"
author: "Christopher Hemmings"
---
Hey all. I need some help



I have a dock/undock command I would like to add to smoothie. So if for instance I send a gcode M876 T0, i would like smoothie to play a file on the sd card to dock Tool 0. and if i send M876 T1, i would like it to play another file that docks tool 1. 



together I have 4 files



Dock T0

Undock T0

Dock T1

Undock T1



One each dock i want to have a microswitch/endstop that I can plug into the unused ports Xmax for T0 and Ymin for T1 (i use ymax for homing)



If for instance the xmax enstop is triggered, that means that there is already a tool docked in T0 Dock, and if i then tell it to dock T0, it will not run that file. 



The same would go for T1,



If the xmax endstop is not triggered, that means that a tool must be in the head, and if i try to undock T0 while that endstop is not triggered, it will not play the file.



I hope you understand what im after, I have tried to look at the CPP files in the github build, although its a little over my head at the moment without some knowledge of it.



Id love to get some help on this idea.

Thanks.



Also, see youtube video below for what i am starting to develop.


{% include youtubePlayer.html id="is5lhVlLW2o" %}
[https://youtu.be/is5lhVlLW2o](https://youtu.be/is5lhVlLW2o)





**"Christopher Hemmings"**

---
---
**Daniel Kruger** *February 06, 2017 05:09*

I hope you get help with this. Swapping heads would allow for clean multi-material prints. You may be able to do it all in the slicer if you don't use switches and rely on the slicer/printer to do what it is supposed to do. 


---
**Christopher Hemmings** *February 06, 2017 09:19*

I will be doing it in the slicer to send a gcode command. Although I want smoothie to read that gcode command. Check if a endstop input is active or not and then decide whether to play a file or show an error on the screen. 


---
**Wolfmanjm** *February 07, 2017 01:02*

you can't use T as a parameter as it is reserved as a command for tool change. you can use many  other letters, P is a good choice.


---
**Wolfmanjm** *February 07, 2017 01:04*

you will need to write a new module to do this I think as nothing built in can do this


---
**Christopher Hemmings** *February 07, 2017 11:11*

Hey Wolfmanjm, Thanks for the replies. Are you able to give me a very quick rundown on the different parameters I could use to do this. Is this do you think, a simple thing to do. I assume it would handle it very similar to say the G28 command, except it would have a switch as to whether it runs, or it doesn't




---
**Wolfmanjm** *February 07, 2017 19:54*

Unfortunately it is not simple enough that I can explain here in a few paragraphs. Also there is no way currently to query endstops from another module. So either this would need to go in the Endstop.cpp module or a public access query will need to be added to query the endstop status from the Endstop module. If this is a one off I would go the quick and dirty route and just hack it into the Endstop.cpp where all the info you need to query an endstop already resides. However that kind of hack would not be accepted into main code, but may serve as an example and proof of concept.


---
**Christopher Hemmings** *February 08, 2017 04:30*

Thanks mate. Unfortunately, I am not proficient in the programming and wouldn't even know where to start.



I have had a friend help me write this for marlin, that will have to do for now i think.






---
**Arthur Wolf** *February 09, 2017 13:00*

This looks <b>a lot</b> like the planned/more general "tool change" module a few people are working on. If you want to join the effort, don't hesitate to email me at wolf.arthur@gmail.com.


---
**Christopher Hemmings** *February 09, 2017 23:45*

**+Arthur Wolf** I have sent you an email.Thanks mate




---
**Christopher Hemmings** *February 11, 2017 12:39*

So i have gotten this working on my old Rumba board, and marlin, Works great, I have some switches plugged into unused endstop locations to determine if something is docked/undocked and to only run the dock command if nothing is already docked, and only run the undock command if something is in the dock ready to be undocked. 



Ill wait for Arthur to get back to me, im keen to get this built into smoothie so we can have the safety function.

cheers




---
*Imported from [Google+](https://plus.google.com/102520470412313018729/posts/U4Qi3uLwiqn) &mdash; content and formatting may not be reliable*
