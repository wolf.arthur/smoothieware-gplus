---
layout: post
title: "Question? Does anyone now why my printer does this?"
date: February 07, 2017 01:32
category: "General discussion"
author: "jacob keller"
---
Question?



Does anyone now why my printer does this?



When I press print it homes and then it does a purge line like how I have it in my printer G-code. And then it goes back home And shuts the HotEnd off and the print aborts but it never started.



Start gcode

G28 X0 Y0;HOME XY

G28 Z0;HOME Z

M104 S195

G1 X255 F1000;MOVE 5MM OUT OF PRINT AREA

M109 S195

G4 P1000

G1 Y100 E12.5; PURGE PRINT HEAD







Here's a video of the printer.

 

[https://www.dropbox.com/s/p77ayrw9j4z0iiz/VID_20170206_191404755.mp4?dl=0](https://www.dropbox.com/s/p77ayrw9j4z0iiz/VID_20170206_191404755.mp4?dl=0)



config file

[https://www.dropbox.com/s/aqh30bd349ok2hg/config?dl=0](https://www.dropbox.com/s/aqh30bd349ok2hg/config?dl=0)



GCode File From 2-7-17[https://www.dropbox.com/s/fy3xmupekj1hjjo/TEST3-MARIC-2-7-17-AT-445PM.gcode?dl=0](https://www.dropbox.com/s/fy3xmupekj1hjjo/TEST3-MARIC-2-7-17-AT-445PM.gcode?dl=0)





**"jacob keller"**

---
---
**Michael Hackney** *February 07, 2017 01:45*

Posting your gcode would be more helpful. 


---
**jacob keller** *February 07, 2017 01:57*

**+Michael Hackney**



oops sorry about that

start Gcode

G28 X0 Y0;HOME XY

G28 Z0;HOME Z

M104 S195

G1 X255 F1000;MOVE 5MM OUT OF PRINT AREA

M109 S195

G4 P1000

G1 Y100 E12.5; PURGE PRINT HEAD








---
**Michael Hackney** *February 07, 2017 02:54*

I mean post the entire gcode file or at least the first 100 lines. You posted lines up to but not including what I wanted to see!


---
**jacob keller** *February 07, 2017 03:04*

**+Michael Hackney**

M109 S195.000000

G28 X0 Y0;HOME XY

G28 Z0;HOME Z

M104 S195

G1 X255 F1000;MOVE 5MM OUT OF PRINT AREA

M109 S195

G4 P1000

G1 Y100 E12.5; PURGE PRINT HEAD



;TweakAtZ instances: 5

;Layer count: 508

;LAYER:0

M107

G0 F3000 X0.000 Y6.888 Z0.100

G0 X0.136 Y7.249

;TYPE:SKIRT

G1 F1500 X0.000 Y6.888 E0.01543

G1 X-0.139 Y7.257 E0.03120

G1 X-0.471 Y7.992 E0.06346

G1 X-0.572 Y8.214 E0.07322

G1 X-0.982 Y8.983 E0.10808

G1 X-1.220 Y9.392 E0.12701

G1 X-1.730 Y10.167 E0.16412

G1 X-1.899 Y10.387 E0.17521

G1 X-2.326 Y10.940 E0.20316

G1 X-2.719 Y11.373 E0.22655

G1 X-2.983 Y11.664 E0.24227

G1 X-3.703 Y12.330 E0.28150

G1 X-3.893 Y12.477 E0.29111

G1 X-4.450 Y12.908 E0.31928

G1 X-5.242 Y13.412 E0.35683

G1 X-5.822 Y13.713 E0.38297

G1 X-6.566 Y14.050 E0.41564

G1 X-6.918 Y14.166 E0.43046

G1 X-7.474 Y14.352 E0.45391

G1 X-7.929 Y14.455 E0.47257

G1 X-8.405 Y14.563 E0.49210

G1 X-9.144 Y14.653 E0.52188

G1 X-9.715 Y14.685 E0.54475

G1 X-9.929 Y14.706 E0.55335

G1 X-10.862 Y14.654 E0.59073

G1 X-11.056 Y14.636 E0.59853

G1 X-11.906 Y14.501 E0.63295

G1 X-12.202 Y14.426 E0.64517

G1 X-12.813 Y14.266 E0.67043

G1 X-13.269 Y14.106 E0.68976

G1 X-14.040 Y13.787 E0.72314

G1 X-14.328 Y13.635 E0.73616

G1 X-14.908 Y13.325 E0.76247

G1 X-15.277 Y13.079 E0.78021

G1 X-15.721 Y12.785 E0.80151

G1 X-16.190 Y12.403 E0.82570

G1 X-16.611 Y12.053 E0.84760

G1 X-17.058 Y11.606 E0.87289

G1 X-17.330 Y11.333 E0.88830

G1 X-17.906 Y10.647 E0.92413

G1 X-18.006 Y10.519 E0.93063

G1 X-18.442 Y9.919 E0.96030

G1 X-18.731 Y9.451 E0.98230

G1 X-19.022 Y8.988 E1.00417

G1 X-19.539 Y7.982 E1.04942

G1 X-19.621 Y7.792 E1.05769

G1 X-19.989 Y6.934 E1.09504

G1 X-20.209 Y6.308 E1.12158

G1 X-20.362 Y5.872 E1.14006

G1 X-20.508 Y5.387 E1.16032

G1 X-20.707 Y4.604 E1.19264

G1 X-20.708 Y4.604 E1.19268

G1 X-20.813 Y4.197 E1.20949

G1 X-21.049 Y2.947 E1.26037

G1 X-21.094 Y2.604 E1.27421

G1 X-21.214 Y1.682 E1.31140

G1 X-21.290 Y0.634 E1.35343

G1 X-21.328 Y-0.849 E1.41277

G1 X-21.313 Y-1.409 E1.43518

G1 X-21.306 Y-1.670 E1.44562

G1 X-21.227 Y-2.799 E1.49089

G1 X-21.145 Y-3.490 E1.51873

G1 X-21.093 Y-3.927 E1.53633

G1 X-20.902 Y-5.030 E1.58111

G1 X-20.781 Y-5.562 E1.60293

G1 X-20.656 Y-6.114 E1.62557

G1 X-20.347 Y-7.184 E1.67012

G1 X-20.217 Y-7.552 E1.68573

G1 X-19.982 Y-8.214 E1.71383

G1 X-19.656 Y-8.997 E1.74776

G1 X-19.551 Y-9.209 E1.75722

G1 X-19.221 Y-9.884 E1.78727

G1 X-18.726 Y-10.746 E1.82703

G1 X-18.164 Y-11.576 E1.86713

G1 X-17.870 Y-11.951 E1.88619

G1 X-17.635 Y-12.250 E1.90140

G1 X-16.994 Y-12.951 E1.93940

G1 X-16.840 Y-13.091 E1.94772

G1 X-16.306 Y-13.587 E1.97687

G1 X-15.910 Y-13.894 E1.99692

G1 X-15.556 Y-14.167 E2.01480

G1 X-14.906 Y-14.590 E2.04582

G1 X-14.090 Y-15.028 E2.08286

G1 X-13.211 Y-15.399 E2.12103

G1 X-12.790 Y-15.527 E2.13863

G1 X-12.313 Y-15.673 E2.15858

G1 X-11.724 Y-15.796 E2.18265

G1 X-10.966 Y-15.917 E2.21335

G1 X-10.529 Y-15.937 E2.23085

G1 X-10.001 Y-15.965 E2.25200

G1 X-9.476 Y-15.938 E2.27303

G1 X-9.038 Y-15.916 E2.29057

G1 X-8.418 Y-15.819 E2.31567

G1 X-8.089 Y-15.769 E2.32898

G1 X-7.190 Y-15.531 E2.36618

G1 X-6.247 Y-15.185 E2.40636

G1 X-5.999 Y-15.061 E2.41745

G1 X-5.374 Y-14.753 E2.44532

G1 X-5.053 Y-14.553 E2.46045

G1 X-4.444 Y-14.169 E2.48925

G1 X-4.113 Y-13.912 E2.50601

G1 X-3.636 Y-13.536 E2.53031

G1 X-3.190 Y-13.118 E2.55476

G1 X-2.902 Y-12.846 E2.57060

G1 X-2.352 Y-12.224 E2.60382

G1 X-2.167 Y-12.010 E2.61513

G1 X-1.519 Y-11.128 E2.65891

G1 X-1.416 Y-10.958 E2.66686

G1 X-0.954 Y-10.209 E2.70206

G1 X-0.694 Y-9.702 E2.72485

G1 X-0.369 Y-9.039 E2.75439

G1 X-0.005 Y-8.189 E2.79137

G1 X0.003 Y-8.166 E2.79235

G1 X0.327 Y-8.943 E2.82602

G1 X0.455 Y-9.236 E2.83881

G1 X0.713 Y-9.737 E2.86135

G1 X0.951 Y-10.205 E2.88235

G1 X1.413 Y-10.953 E2.91752

G1 X1.521 Y-11.131 E2.92585

G1 X2.176 Y-12.024 E2.97015

G1 X2.362 Y-12.234 E2.98137

G1 X2.900 Y-12.844 E3.01390

G1 X3.189 Y-13.117 E3.02980

G1 X3.640 Y-13.540 E3.05454

G1 X4.134 Y-13.927 E3.07964

G1 X4.554 Y-14.245 E3.10071

G1 X5.177 Y-14.630 E3.13001

G1 X5.964 Y-15.044 E3.16558

G1 X6.241 Y-15.182 E3.17795

G1 X7.125 Y-15.510 E3.21567

G1 X7.315 Y-15.564 E3.22357

G1 X8.073 Y-15.765 E3.25494

G1 X8.398 Y-15.817 E3.26810

G1 X9.044 Y-15.916 E3.29425

G1 X10.002 Y-15.965 E3.33262

G1 X10.515 Y-15.938 E3.35316

G1 X10.972 Y-15.915 E3.37147

G1 X11.718 Y-15.797 E3.40168

G1 X12.315 Y-15.674 E3.42606

G1 X12.790 Y-15.527 E3.44595

G1 X13.211 Y-15.399 E3.46355

G1 X13.798 Y-15.151 E3.48904

G1 X14.087 Y-15.030 E3.50157

G1 X14.899 Y-14.594 E3.53844

G1 X15.555 Y-14.168 E3.56973

G1 X15.909 Y-13.895 E3.58761

G1 X16.306 Y-13.587 E3.60771

G1 X16.840 Y-13.091 E3.63686

G1 X16.992 Y-12.952 E3.64510

G1 X17.637 Y-12.248 E3.68329

G1 X17.869 Y-11.952 E3.69833

G1 X18.164 Y-11.577 E3.71742

G1 X18.719 Y-10.757 E3.75702

G1 X19.221 Y-9.887 E3.79720

G1 X19.592 Y-9.125 E3.83110

G1 X19.976 Y-8.232 E3.86999

G1 X20.245 Y-7.474 E3.90216

G1 X20.650 Y-6.140 E3.95792

G1 X20.902 Y-5.028 E4.00353

G1 X21.092 Y-3.928 E4.04818

G1 X21.227 Y-2.802 E4.09354

G1 X21.306 Y-1.669 E4.13898

G1 X21.313 Y-1.409 E4.14938

G1 X21.328 Y-0.865 E4.17115

G1 X21.306 Y0.393 E4.22147

G1 X21.284 Y0.709 E4.23415

G1 X21.214 Y1.676 E4.27293

G1 X21.094 Y2.603 E4.31032

G1 X21.048 Y2.949 E4.32428

G1 X20.812 Y4.195 E4.37500

G1 X20.711 Y4.589 E4.39127

G1 X20.508 Y5.394 E4.42448

G1 X20.360 Y5.879 E4.44476

G1 X20.209 Y6.308 E4.46296

G1 X19.991 Y6.931 E4.48936

G1 X19.623 Y7.788 E4.52666

G1 X19.544 Y7.973 E4.53471

G1 X19.023 Y8.983 E4.58017

G1 X18.740 Y9.437 E4.60157

G1 X18.441 Y9.919 E4.62426

G1 X18.008 Y10.517 E4.65379

G1 X17.907 Y10.646 E4.66034

G1 X17.331 Y11.332 E4.69617

G1 X17.058 Y11.606 E4.71164

G1 X16.603 Y12.061 E4.73738


---
**Michael Hackney** *February 07, 2017 03:09*

Ok so now we have ruled out a gcode issue. In the video you show the monitor with the message that the laptop is out of battery. Perhaps the firmware is shutting down the print. Has this happened more than once? Do you have your laptop plugged in now?


---
**jacob keller** *February 07, 2017 03:14*

**+Michael Hackney**​

Yes it's happened ever time. Tried different slicer's to. And I keep my laptop pluged in. I just grabbed my laptop to show everyone what it was doing. For the video. And it was doing this with the laptop charging


---
**Michael Hackney** *February 07, 2017 03:17*

And can you print other models? Or does this happen with everything? Is this a new printer or has it performed well in the past?


---
**jacob keller** *February 07, 2017 03:20*

**+Michael Hackney**​

Does this with any model. Had the printer for 2 years and now I'm changing controllers from stock motherboard to smoothieboard. Do you think it has something to do with the configuration?


---
**jacob keller** *February 07, 2017 03:30*

**+Michael Hackney**



updated the post with my config file


---
**Arthur Wolf** *February 07, 2017 09:33*

**+jacob keller** Can you please try printing the exact same file with Pronterface ? If Smoothie is outputting errors, Pronterface will show them while Cura hides them.


---
**Christopher Hemmings** *February 07, 2017 11:16*

You have Min and Max limit switches set to true, is it halting because its hitting the X limit switch, and actually going right into the negative numbers.




---
**jacob keller** *February 07, 2017 13:43*

Hey **+Arthur Wolf**​ I'll try pronterface.

**+Christopher Hemmings**​ I'll turn the max end stops off.


---
**jacob keller** *February 07, 2017 23:06*

**+Arthur Wolf** **+Christopher Hemmings****+Michael Hackney** it works with pronterface. And I update my printer Gcode to this. fix my config file with the end stops



Start Gcode

G21

G90

M82

M107

G28 X0 Y0;HOME XY

G28 Z0;HOME Z

M104 S195

G1 X255 F1000;MOVE 5MM OUT OF PRINT AREA

M109 S195

G4 P1000

G92 E0

G1 Y100 E12.5; PURGE PRINT HEAD

G92 E0



It used to do this. it world do all the gcode. print 3-4 layers and then retract all the filament out of the extruder. so I thought by updating my gcode with G92 E0 world fix the problem but it still retracts all the filament out of the extruder? any ideas why G92 E0 world not work.



thanks


---
**jacob keller** *February 07, 2017 23:20*

Updated the post with my Gcode file so you can download it. if you want to take a look at it.



thanks


---
**jacob keller** *February 09, 2017 21:13*

**+Arthur Wolf** **+Michael Hackney** **+Christopher Hemmings** still not working.

any suggestions


---
**Arthur Wolf** *February 12, 2017 17:08*

**+jacob keller** I read the comments here several times, but I'm not clear on exactly what doesn't work now.

You said it works with Pronterface. Does it work with pronterface but not with some other software ? Or is something <b>else</b> broken in Pronterface.

Either way, exactly what is it doing wrong ?


---
**jacob keller** *February 12, 2017 18:31*

**+Arthur Wolf** sorry for the confusion. When my printer starts to print it prints for 3 to 4 layers and it stops and then it reverses the extruder and all the filament comes out. and then it resumes the print. But there's no filament in the hot end so it just air prints. It does this with cura,pronterface and slic3r. If you look at my gcode file I have G92 E0 to zero the extruder but it looks like it's not working.



Am I'm doing something wrong with the configuration or the gcode?



Plus in pronterface when I press print it doesn't home itself. But in cura it does home itself?


---
**Arthur Wolf** *February 12, 2017 18:33*

This is very strange, G92 should work. Do you have volumetric extrusion configured ? Did you configure your slicing program to do relative extrusion instead of absolute extrusion ?


---
**jacob keller** *February 12, 2017 18:35*

**+Arthur Wolf**​ I'll check my configuration and my slicer.


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/eTKwp7owMjA) &mdash; content and formatting may not be reliable*
