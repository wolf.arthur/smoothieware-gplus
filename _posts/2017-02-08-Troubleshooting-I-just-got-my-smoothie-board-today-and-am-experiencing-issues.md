---
layout: post
title: "Troubleshooting: I just got my smoothie board today and am experiencing issues"
date: February 08, 2017 15:38
category: "General discussion"
author: "Jamison Go"
---
Troubleshooting: I just got my smoothie board today and am experiencing issues. When I plug the board into USB (and nothing else), my computer cant find the SD card. Four LEDS are on solid: 1-4 and 3.3V. 



Any ideas on how to fix this? I think normal operation my computer is supposed to find the device and allow me access to the SD card.



![missing image](https://lh3.googleusercontent.com/-Da2B4YLgGQA/WJs7iObTLdI/AAAAAAAAEPI/kbKzHfOG3-E9cZ4kM_uljGw7PK6eFxiPgCJoC/s0/DSC_2199.JPG)
![missing image](https://lh3.googleusercontent.com/-CrtDZKo5Zec/WJs7iNSew9I/AAAAAAAAEPI/iJrjO3aMkqsfIlFK70DDgqVYAet-qCmawCJoC/s0/DSC_2198.JPG)

**"Jamison Go"**

---
---
**Michael Andresen** *February 08, 2017 16:01*

I received one not long ago, just tried to plug it in for the first time. I started upgrading firmware, then slow blinking while the driver was installed. And then the sd popped up as a drive.


---
**Jamison Go** *February 08, 2017 16:03*

Hi Michael. So do you think it may just be an SD card issue? If I remove the card, the board exhibits the same behavior. The contents of the card seem fairly typical.


---
**Maxime Favre** *February 08, 2017 16:04*

Driver installed ?


---
**Jamison Go** *February 08, 2017 16:16*

Tried both. The guide said it shouldnt need it for win 10.



Would the LEDs be able to provide some clue as to the issue? the Troubleshooting guide online seems to have broken links to the LED codes


---
**Jared Roy** *February 08, 2017 18:13*

Just a quick check. I had same issue the other day. Ended up being my sd card was not fully inserted....


---
**Jamison Go** *February 08, 2017 18:56*

Jared, just doubled checked and the SD card is fully inserted. 



Should I clear the SD card? Put the firmware.bin on it and hope for the best?




---
**Jamison Go** *February 08, 2017 19:06*

Hi all I think I fixed it. Seems like the firmware on the SD card was bad. I cleared all the files and downloaded a fresh .bin from github. Now the board connects and lights blink. Thanks!


---
**Arthur Wolf** *February 12, 2017 17:52*

**+Jamison Go** When a firmware.bin file is present on the SD card, the board flashes it automatically. We ship the boards with a firmware.bin file on the SD card, which means the very first time you plug it in, it flashes that file. If you unplug it too early the first time you plug it in, that process can get messed up, which looks like what happened to you.

Happy you got it to work.


---
*Imported from [Google+](https://plus.google.com/115719281858291857353/posts/9LaSRGN7Q7u) &mdash; content and formatting may not be reliable*
