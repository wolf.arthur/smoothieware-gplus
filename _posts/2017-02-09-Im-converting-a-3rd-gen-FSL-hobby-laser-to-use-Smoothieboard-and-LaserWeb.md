---
layout: post
title: "I'm converting a 3rd gen FSL hobby laser to use Smoothieboard and LaserWeb"
date: February 09, 2017 19:07
category: "General discussion"
author: "Peter Vieth"
---
I'm converting a 3rd gen FSL hobby laser to use Smoothieboard and LaserWeb.  I can't identify the stepper motors so not sure what to set steps per mm to.  Any suggestions for a starting point? I figure with a reasonable number, I can move the axes around and tune it until it's right.





**"Peter Vieth"**

---
---
**Mike Mills** *February 09, 2017 20:40*

Haven't converted yet, but I seem to remember that the hobby laser (in Retina engrave) has s/mm listed in the firmware settings on the right side of the screen. If you don't have the answer by Sunday, I have to fire mine up and I'll see what it says for mine. 


---
**Peter Vieth** *February 09, 2017 20:51*

Thanks Mike.  I think the version of RetinaEngrave I was using is simply too old-- there was no such information (and no way worthwhile upgrade, hence why I'm moving to SmoothieBoard).  But I'll go look for other RetinaEngrave screenshots on Google.


---
**Peter Vieth** *February 09, 2017 23:16*

So apparently JK42HM34-1334 works with the FSL.  Specs say Step Angle(degrees):0.9 degree but obviously the distance traveled depends on the other mechanical bits of the laser.  Hmm


---
**cory brown** *February 12, 2017 06:38*

 157.48031 is what i'm using for my fourth gen FSL


---
**Arthur Wolf** *February 12, 2017 17:16*

You just need to ask it to move 1mm, see how much it moves, then based on that ask it to move a relatively long distance, measure how long it <b>actually</b> moved, and based on this you can calculate the right value for steps per mm.


---
**Peter Vieth** *July 11, 2017 05:03*

Anyone have tips on how to get the laser to actually fire?  I've connected pin 2.4 to L on the jymydy PSU but it never fires.  X and Y work fine.  Test fire works via physical control panel works.  Unfortunately Smoothieweb's "Test laser" button just spits out  You must setup "LaserTest Power" and "LaserTest Duratuion" first! (and Google returns only one lonely plea for help with this error). Have spent the entire evening trying to figure this one out...


---
**Peter Vieth** *July 11, 2017 05:27*

Just to clarify, L is the only wire that was hooked up to the original controller that sounds like it ought to be the laser itself.  I hooked up 4 wires for each motor, have 4 dangling that are labeled limit, 24v, 5v, gnd, and L.  I connected L to pin 2.4 and set the config file to use 2.4 and set laser enabled to true, but no go.  Motion is fine, but laser doesn't fire unless I hold down the two laser test buttons on the control panel.


---
**Peter Vieth** *July 11, 2017 20:23*

I've figured out that the L means the laser is active when this wire is pulled low.  Now to figure out how to configure Smoothie to use active low PWM...  EDIT:  the H line is, I think, wired to the laser test button on the control panel.  This pushing the buttons sends 5v to H, which fires the laser.  


---
**Peter Vieth** *July 15, 2017 23:27*

Got it working, S must be followed by a number 0-1.0... too used to spindle speeds in the thousands


---
*Imported from [Google+](https://plus.google.com/+PeterVieth/posts/WQ4xHeb1aMv) &mdash; content and formatting may not be reliable*
