---
layout: post
title: "Does anyone know if there is a firmware and instructions for rotary a-axis support?"
date: February 12, 2017 01:50
category: "General discussion"
author: "Carl Fisher"
---
Does anyone know if there is a firmware and instructions for rotary a-axis support? 



Trying to get this working for both a laser and a CNC using a C3D mini which runs the Smoothie firmware.



Thanks.





**"Carl Fisher"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 12, 2017 02:26*

Answered here: [plus.google.com - I'm finally trying to setup my A-Axis but I'm not finding any settings in the...](https://plus.google.com/105504973000570609199/posts/LyWUQqGRBFr)


---
**Don Kleinschnitz Jr.** *February 12, 2017 02:56*

I assumed:

The rotary axis must have the same resolution as the Y stepper 



You disconnect the Y stepper drive and connect it to the rotary adapters drive

 No ????


---
**Ray Kholodovsky (Cohesion3D)** *February 12, 2017 03:04*

He can keep Y wired in and set up the 4th socket as A using the instructions in my link. Then it's just a matter of getting gCode in X+A format - feel free to ask when LW4 might have that ready.


---
**Carl Fisher** *February 12, 2017 03:11*

This is so I can do testing of LW4 rotary support


---
**Ray Kholodovsky (Cohesion3D)** *February 12, 2017 03:12*

That's the best answer.


---
**Arthur Wolf** *February 12, 2017 12:40*

Also [smoothieware.org - 6axis [Smoothieware]](http://smoothieware.org/6axis)


---
**Carl Fisher** *February 12, 2017 13:00*

**+Arthur Wolf** That was the missing piece. Combined with what **+Ray Kholodovsky**  sent me earlier I have something I can work with :) 



I'm not 100% clear on the PAXIS thing, but I'm assuming that I want to add PAXIS=4? If I'm reading right without that would not give me position information on the rotary?


---
**Arthur Wolf** *February 12, 2017 16:14*

Paxis is about how many axes are included in the dlistance calculations for acceleration/junction_deviation. On most machines and I believe in your setup, you want to keep that at 3.


---
**Don Kleinschnitz Jr.** *February 12, 2017 16:55*

Will the swap I suggested above not work? It would not require a recompile? 

For rotary engraving you arent adding another axis to control you are just swapping a rotary for a linear. 

The rotary needs to be geared such that that one step in rotary distance =  normal linear distance. Mmmm... this will require different ratio for different diameter target vessels? Were is that set?

If you don't use the motor swap approach and add an axis, are there control implications from the gcode such as needing to reference a third axis. 




---
**Don Kleinschnitz Jr.** *February 12, 2017 17:00*

**+Ray Kholodovsky** can we set up a way to enable one driver and disable the other without g-code? A rotary enable switch of sorts that just enables the rotary driver and disables the other. 


---
**Carl Fisher** *February 12, 2017 17:33*

**+Don Kleinschnitz**  in my case, no. The motor and gearing are different and will need unique setup that would not be directly compatible with the way Y is configured.


---
**Don Kleinschnitz Jr.** *February 12, 2017 18:32*

**+Carl Fisher** 

Sorry in advance for more questions than answers but as you can tell I just started thinking about how to do this. 

These  is me "thinking out loud"......

................

So does this mean;



..................when you do a rotary job...................

1.0 Each time you have to load a different firmware from when you are not running a rotary?  

1.1 You have to change the config settings file to account for different stepper setting when using a rotary vs normal operation?

OR 

1.2 Does this mean that you have to recompile and set up one time for rotary support and after that smoothie has 3 axis avail to control and the setting for each are defined in the config file?

...........................

................. LaserWeb control of rotary .................

2.0 Does the CAM program need to know about the Y==A substitution and change in step resolution?

2.1 How do you change the move settings for different diameter vessels?

2.2 Is the support for X,Y,Z,A in LW?

2.3 Does LW need to know that a rotary is installed?

................................... 

It seems that no mater what the firmware or gearing settings are, the engraving software needs to know that and need to account for various diameter vessels?






---
**Carl Fisher** *February 12, 2017 19:14*

The plan is one firmware that is compiled to recognize XYZA all as independent axis as eventually there will be a powered Z bed as well. The design goal is to drop the work table and put the rotary in and just use Z to set the focus height based on the piece installed on the rotary.



LW4 has rotary support, but it's still very much Alpha in the development lifecycle. As far as I understand it should not need to translate Y to A as it would just write A directly to the gcode. LW4 has an on/off slider for rotary on each off the operation configurations.


---
**Chuck Comito** *March 28, 2017 22:14*

**+Carl Fisher** Any update on this? I'd like to experiment with this as well. Did you have to compile the firmware to support ABC axis' as well? I'm assuming you did but before I take the plunge I thought I'd ask how you are fairing with it.. Thanks.


---
**Carl Fisher** *March 29, 2017 01:36*

My shop is winding down while we get ready to pack up and move so unfortunately the rotary has been put on the back burner for a while.




---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/Th7xUw3Gces) &mdash; content and formatting may not be reliable*
