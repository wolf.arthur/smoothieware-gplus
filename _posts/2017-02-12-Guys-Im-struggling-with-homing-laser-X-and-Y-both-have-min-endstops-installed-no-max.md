---
layout: post
title: "Guys, I'm struggling with homing laser. X and Y both have min endstops installed (no max)"
date: February 12, 2017 17:07
category: "General discussion"
author: "Chuck Comito"
---
Guys, I'm struggling with homing laser. X and Y both have min endstops installed (no max). Click home is laserweb, Repetier, Ponterface, etc, and X moves to the endstop while Y moves away from it. I can't seem to figure out what seems like it would be so simple...

Here is a snippet of my config for the beta:

beta_min_endstop                             1.26^            # Pin to read min endstop, add a ! to invert if endstop is NO connected to ground

#beta_max_endstop                            1.27^            # Pin to read max endstop, uncomment this and comment the above if using max endstops

beta_homing_direction                        home_to_min      # Or set to home_to_max and set alpha_max and uncomment the alpha_max_endstop

beta_min                                     0                # This gets loaded as the current position after homing when home_to_min is set

beta_max                                     200              # This gets loaded as the current position after homing when home_to_max is set



Maybe I'm not looking in the right location? Any help is appreciated!!





**"Chuck Comito"**

---
---
**Jason Frazier** *February 12, 2017 17:16*

Consider whether you need to invert the beta_dir_pin output, this is actually what controls the direction of the Y axis motor.


---
**Arthur Wolf** *February 12, 2017 17:17*

Yep, you probably need to invert the direction of your Y axis. Or if the direction is correct, it means your endstop is actually at max, not min.


---
**Chuck Comito** *February 12, 2017 18:54*

I will try both of those suggestions. Thanks guys. 


---
**Arthur Wolf** *February 12, 2017 18:54*

You need to find which it is, and do only one of the two. You mustn't do both.


---
**Chuck Comito** *February 12, 2017 18:56*

Well at the moment I'm guessing (hate to say that) but I've been digging a pretty deep hole with this laser project. Sad part is, this and a pwm to fire my laser is what's holding me back right now and I can't believe I've gotten as far as I have lol. 


---
**Wolfmanjm** *February 12, 2017 21:50*

also if you are using the firmware-cnc then homing is NOT G28 it is $H. so clicking home in a 3d based host like pronterface will park heads not home them.


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/XZWTjfSUJfy) &mdash; content and formatting may not be reliable*
