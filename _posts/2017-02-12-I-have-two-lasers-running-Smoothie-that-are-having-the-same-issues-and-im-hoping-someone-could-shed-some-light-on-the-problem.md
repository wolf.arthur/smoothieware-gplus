---
layout: post
title: "I have two lasers running Smoothie that are having the same issues and i'm hoping someone could shed some light on the problem"
date: February 12, 2017 07:11
category: "General discussion"
author: "cory brown"
---
I have two lasers running Smoothie that are having the same issues and i'm hoping someone could shed some light on the problem. Here is the gcode that is giving my lasers a hard time:

G0 X280 Y89 F7500

G1 X281.127200 Y89.204800 S0.000000

G1 X281.178000 Y89.204800 S0.400000

G1 X289.153600 Y89.204800 S0.000000

G0 X206.502000 Y89.255600 F15000

G1 X198.526400 Y89.255600 F15000

G1 X184.708800 Y89.255600 S0.400000

G1 X156.565600 Y89.255600 S0.000000

G0 X280 Y89 F7500



and here is the config I am running on the latest edge and master branch :



# Basic motion configuration

default_feed_rate                            8000             # Default speed (mm/minute) for G1/G2/G3 moves

default_seek_rate                            8000             # Default speed (mm/minute) for G0 moves

mm_per_arc_segment                           0.1              # Fixed length for line segments that divide arcs, 0 to disable

#mm_per_line_segment                         5                # Cut lines into segments this size

mm_max_arc_error                             0.01             # The maximum error for line segments that divide arcs 0 to disable

                                                              # note it is invalid for both the above be 0

                                                              # if both are used, will use largest segment length based on radius



# Arm solution configuration : Cartesian robot. Translates mm positions into stepper positions

# See 

alpha_steps_per_mm                           157.4802               # Steps per mm for alpha ( X ) stepper

beta_steps_per_mm                            157.4802               # Steps per mm for beta ( Y ) stepper

gamma_steps_per_mm                           159.5             # Steps per mm for gamma ( Z ) stepper



# Planner module configuration : Look-ahead and acceleration configuration

# See

acceleration                                 3000             # Acceleration in mm/second/second.

#z_acceleration                              500              # Acceleration for Z only moves in mm/s^2, 0 uses acceleration which is the default. DO NOT SET ON A DELTA

junction_deviation                           0.05             # See #junction-deviation

#z_junction_deviation                        0.0              # For Z only moves, -1 uses junction_deviation, zero disables junction_deviation on z moves DO NOT SET ON A DELTA



# Cartesian axis speed limits

x_axis_max_speed                             30000            # Maximum speed in mm/min

y_axis_max_speed                             8000            # Maximum speed in mm/min

z_axis_max_speed                             300              # Maximum speed in mm/min



# Stepper module configuration 

# Pins are defined as  ports, and pin numbers, appending "!" to the number will invert a pin

# See 

alpha_step_pin                               2.0              # Pin for alpha stepper step signal

alpha_dir_pin                                0.5              # Pin for alpha stepper direction, add '!' to reverse direction

alpha_en_pin                                 0.4              # Pin for alpha enable pin

alpha_current                                0.6              # X stepper motor current

alpha_max_rate                               30000.0          # Maximum rate in mm/min



beta_step_pin                                2.1              # Pin for beta stepper step signal

beta_dir_pin                                 0.11             # Pin for beta stepper direction, add '!' to reverse direction

beta_en_pin                                  0.10             # Pin for beta enable

beta_current                                 0.6              # Y stepper motor current

beta_max_rate                                8000.0          # Maxmimum rate in mm/min



I think this might be a bug in Smoothieware because I can run all kinds of speed, acceleration, and jerk tests at higher speed without issue. This bit of gcode will cause the x axis to skip. If i turn down the acceleration and/or junction_deviation the problem only gets worse... but if i turn up the junction_deviation to .1 and the acceleration to 4000 the problem goes away. That's counter intuitive. Turning up the acceleration makes all my arc wavy and zig-zagy... so not a great workaround. 

Any help would be greatly welcome. also if someone could reproduce the issue that would help prove it's not mechanical problem. I have a K40 and a converted Full Spectrum laser cutter both running Smoothie with the issue and I have done all of the belt, wheels etc adjustments I can think of.﻿







**"cory brown"**

---
---
**Arthur Wolf** *February 12, 2017 12:46*

Using the very very latest edge firmware and matching example configuration file ?


---
**cory brown** *February 12, 2017 17:06*

The latest as of a 5 days ago... I can try whatever version though. I did just recreate my config file as soon as I started getting this problem. What's the best place to get a example config file for the edge branch?


---
**Arthur Wolf** *February 12, 2017 17:09*

**+cory brown** Get it here : [http://smoothieware.org/configuring-smoothie](http://smoothieware.org/configuring-smoothie)


---
**cory brown** *February 12, 2017 17:12*

Yeah that's what I have been going off of.


---
**cory brown** *April 12, 2017 20:20*

It's been quite a few weeks and after trying new smoothie board, new SD cards,  any version of the newer edge firmware and new config files i'm still seeing this problem... I wondering if @jerryflyguy is running into the same problem. Would someone mind running the Gcode I posted and see if the problem occurs? I really think there is a bug in smoothie somewhere... 




---
**Arthur Wolf** *April 12, 2017 20:21*

Can you try with [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-latest.bin) ?




---
*Imported from [Google+](https://plus.google.com/102151339533372861675/posts/hE9Uh8QKFDp) &mdash; content and formatting may not be reliable*
