---
layout: post
title: "running latest version of smoothieware My config file has this in it # Optional order in which axis will home, default is they all home at the same time, # If this is set it will force each axis to home one at a time in the"
date: February 12, 2017 12:00
category: "General discussion"
author: "Christopher Hemmings"
---
running latest version of smoothieware



My config file has this in it





# Optional order in which axis will home, default is they all home at the same time,

# If this is set it will force each axis to home one at a time in the specified order

Homing_order                                  YXZ             # Y axis followed by X then Z last

#move_to_origin_after_home                    false           # Move XY to 0,0 after homing

#endstop_debounce_count                       100             # Uncomment if you get noise on your endstops, default is 100

#endstop_debounce_ms                          1               # Uncomment if you get noise on your endstops, default is 1 millisecond debounce

#home_z_first                                 true            # Uncomment and set to true to home the Z first, otherwise Z homes after XY



I am trying to set that to home Y then X then Z to minimise some crashes that could occur on my machine, But having that line uncommented, and changing it to YXZ does not make any difference.





**"Christopher Hemmings"**

---
---
**Arthur Wolf** *February 12, 2017 12:42*

Why "Homing" instead of "homing" ? try changing that see if it helps


---
**Christopher Hemmings** *February 22, 2017 23:57*

Thanks mate ill try that tonight. Im pretty sure that is how it was stock




---
**Arthur Wolf** *February 23, 2017 10:22*

**+Christopher Hemmings** Just checked and it was never a H in the stock example. Maybe it's a mistake while uncommenting the line ( removing the # ) ?


---
**Christopher Hemmings** *February 25, 2017 05:54*

could be my mistake. putting it lower fixes it.

Thanks




---
*Imported from [Google+](https://plus.google.com/102520470412313018729/posts/Si4L3VzM3Q5) &mdash; content and formatting may not be reliable*
