---
layout: post
title: "Can someone please direct me to some example config files for fans, so I can get my fans going"
date: February 14, 2017 03:40
category: "General discussion"
author: "Erik E"
---
Can someone please direct me to some example config files for fans, so I can get my fans going.  Switchs have so many options/versatility in the documentation, a get confused. I just want them ON whenever there is a print in progress. Thank You





**"Erik E"**

---
---
**Douglas Pearless** *February 14, 2017 06:57*

Have a look at [smoothieware.org - switch [Smoothieware]](http://smoothieware.org/switch) 


---
**Maxime Favre** *February 14, 2017 07:29*

All these options are what allow you to configure the switch you want.



You have 2 options :

- add a ON gcode at the start of your gcode and OFF at the end (slicer settings) and run a simple switch: [http://smoothieware.org/switch](http://smoothieware.org/switch)



- or you can turn your fan when the hotend or heatbed temp is greater than a threshold and thus, printing : [http://smoothieware.org/temperatureswitch](http://smoothieware.org/temperatureswitch)


---
**Erik E** *February 14, 2017 09:54*

OK, Thank you. Many edits and then resets later, I think maybe the lines should look like this but the fans (switchs) do not yet turn on during the print (I hope this is not bad etiquette to dump some lines of code here, sorry) : Switch module for fan control

switch.fan1.enable                            true             #

switch.fan1.input_on_command                  M106 S255            #

switch.fan1.input_off_command                 M107             #

switch.fan1.output_pin                        2.6              #

switch.fan1.output_type                       pwm              # pwm output settable with S parameter in the input_on_comand

switch.fan1.max_pwm                           255              # set max pwm for the pin default is 255

switch.fan1.startup_state                    true

switch.fan1.startup_value                     127

switch.fan1.input_pin_behavior		     toggle	        #If  set to momentary when the input pin becomes high the switch changes to the ON state,

                                                              and when it becomes low the switch changes to the OFF state. If set to toggle the input pin

                                                              toggles the switch's state between ON and OFF.


---
**Douglas Pearless** *February 14, 2017 10:04*

The following should allow M106 and M107 to stop and start the fan, you can set up the fan on and off during print in your host software, e.g. repetier.





# Switch module for fan control

switch.fan.enable                            true             #

switch.fan.input_on_command                  M106             #

switch.fan.input_off_command                 M107             #

switch.fan.output_pin                        2.6              #

switch.fan.output_type                       pwm              # pwm output settable with S parameter in the input_on_comand




---
**Arthur Wolf** *February 15, 2017 10:16*

In the example configuration file, there is an example fan switch module. If you do not modify it at all, it will do what you want.


---
**Erik E** *February 16, 2017 20:18*

Thank you all. Based on your comments I though the problem might be my version of Printrun (last revision was Feb '15). So I am exploring using LaserWeb (building a Laser etcher, not a printer). I AM encouraged to see amber LED lights beside the two small mosfets, never seen those before. Probably because I added: switch.fan1.startup_state                    true


---
*Imported from [Google+](https://plus.google.com/101592194436034031274/posts/iASK4fmSqF7) &mdash; content and formatting may not be reliable*
