---
layout: post
title: "Hi, i've got a problem with my AZSMZ Mini board"
date: February 14, 2017 10:56
category: "General discussion"
author: "Lukas M\u00fcller"
---
Hi, i've got a problem with my AZSMZ Mini board. It doesnt boot anymore ... The Screen turns on the backlight but stays blank. Ich can Access the SD Card via USB link and connect the Board in simplify3d machine control, but its Not responding to any commands I send to it. Also ich I try to flash a new Firmware the LEDs do their Thing and after a couple of seconds the 2 outer ones stay on and the 2 inner ones flash rapidly but the Firmware in the SD Card stays as Firmware.bin file, so I suppose its not flashing anything ...

I also tried different firmware versions and SD cards ( also formating them on different devices) with no succes. But the SD cards are flashing on a second Board I have and that Board Shows the same LED behaviour. 

The Problem also accured after the Board has been functioning just fine for several months. 

I now the Board ist not really Open source but would bei nice anyway if someone knows what I'm missing or what I could try to make it work ... 

Thanks in advance ;)





**"Lukas M\u00fcller"**

---
---
**Griffin Paquette** *February 14, 2017 13:55*

First I would say buy open source;-) but in that case I would see if you can reflash the bootloader on it. Starting from scratch there seems like the best option. Not sure if they have the serial broken out though.


---
**Arthur Wolf** *February 14, 2017 17:51*

Hey. Sorry to hear about your broken board. Because boards like the AZSMZ do not respect the smoothie project or the work of the open-source project, we respectfully request that before asking the community for help, you first ask your seller for help. Please understand these sorts of boards are considered toxic to the efforts of the volunteers, therefore there are good reasons to be unwilling to help.

If the seller can not help you, please feel free to come here again for help, we just request you go <b>first</b> to the seller.

Cheers.


---
**Lukas Müller** *February 14, 2017 18:53*

Ok no worries ... I will do that from now on.

The board is now working again. I tried a third SD Card and that did the trick. Dont know why the other 2 cards didnt want to flash though.

 I purchased both boards before the Cohesion3D Boards were released ... so there wasnt much of an alternative.

But thanks anyway ;)


---
*Imported from [Google+](https://plus.google.com/101462068007380494279/posts/jhD6bruhQgh) &mdash; content and formatting may not be reliable*
