---
layout: post
title: "I have recently built a Polygon delta style 3d printer"
date: February 14, 2017 20:41
category: "General discussion"
author: "Jared Roy"
---
I have recently built a Polygon delta style 3d printer. After building I used smoothie auto calibration and it succeeded.  I then used the probe with G30 to find the bed.  The problem is, I get consistent results running G30 commands back to back, it’s when I home (G28) between G30 commands that I am seeing inconsistent results.  This is my first delta and noticed the homing had an effect on my G30 output so, I lowered my homing speed down to 20mm/s and I am still getting inconsistent G30 results if I home in between readings.  I then tried to run the calibration again and it failed because the final result was outside the tolerance.  I noticed that when it is running the calibration routine (G32) it homes after probing each tower, well homing is the one thing that is causing my inconsistent G30 results, so this must also be affecting my calibration. Is this a common issue and how can I work with it?  I was able to print a 15mm cube for reference and it was ~15.3mm in x and y, could this be related to my calibration issue?





**"Jared Roy"**

---
---
**Michael Andresen** *February 14, 2017 20:51*

Make sure the motor mounts are tight enough. I made some aluminium spacers instead of the nylon ones, because I had problems with them deforming when I tightened the screws enough so they would not slide on the rail.

![missing image](https://lh3.googleusercontent.com/raPzCG-4blgnGCWMwdIHFiGr-Uslp5PL2rOHcAd1oQ8SDqWMM08VKOSDoQRC5trgBDtSIpoFI4VP39-K5ZwSNu9HWZEzIPfMPbvN=s0)


---
**Jared Roy** *February 14, 2017 21:03*

**+Michael Andresen** I also had this issue and like your fix.  I will come up with something to allow me to tighten motors better to see if this helps.  Didn't think many people had this printer, nice to see another owner.  Do you also see variation in G30 results with a G28 prior to each G30?




---
**Arthur Wolf** *February 14, 2017 21:32*

This is either a problem with your machine's tolerances, or a problem with homing too fast ( 20mm/s can be too fast if the machine isn't sturdy or precise enough ).


---
**Michael Andresen** *February 14, 2017 21:33*

**+Jared Roy** I actually never used mine yet. I received it, put it together, then didn't like how I had done the mounting of electronics and wires. Pulled most of it apart, got hit by stress from work, and then the project has been standing idle for quite a while. Only one week ago I started fixing it again.

![missing image](https://lh3.googleusercontent.com/vviyEgKura_4TAN70JBw6z8flmThnLe3cnQf0EIKrg8DLnXTgjo_zskEk647mFcsUUd1zYSgCRluHwVt7PFNZmRybcRfqoue_j3r=s0)


---
**Ross Hendrickson** *February 15, 2017 01:50*

Have you posted a question in the official google group? Billy is super responsive.


---
**Jared Roy** *February 15, 2017 13:39*

**+Arthur Wolf** This machine seems very rigid but this is my first delta so maybe I think its good but it might not be. After running further tests last night, I am suspecting the z probe is causing the issues.  I am seeing a 0.05mm variation in readings.  I plan on removing metal lever from switch to hopefully increase accuracy.  I will post up my data findings but right now I think it's the probe and endstop switch accuracy that is the problem.  I also have BLtouch probe to try once I can print a mount and that leads me to.... **+Michael Andresen** I just saw your mount on the Google group.  Thanks for sharing. Of course my print failed.    I am going to mount mine on the original location under nozzle tonight  and repeat my tests if I have some time. **+Ross Hendrickson**  No not yet as I thought this was more related to the electronic setup and came here first.  Was going to post there as well, thanks for the heads up.  I think I'm finally on to it though.  I never liked those micro switches with the metal levers, going to run without the levers and see if things improve. 


---
**Michael Andresen** *February 15, 2017 17:56*

**+Jared Roy** cool, please let me know if it needs any adjustments to work. I have only mounted mine to what looks right to me.


---
**Jared Roy** *February 17, 2017 21:53*

**+Michael Andresen** So far, I have inverted the mount for the z probe switch and removed the metal lever.  I am now not seeing any variation when probing G30 alone and 0.02mm variation if I home in between G30 attempts.  I will now modify all axis end stop switches and see if I can remove most if not all of the variation I am seeing.  I plan to test the BL touch to compare to micro-switch probe next. 


---
**Michael Andresen** *February 18, 2017 11:02*

**+Jared Roy** I tested micro switches some years ago, and they were surprisingly accurate. With the lever on, they had variations up to 0.08mm (+/- 0.04 from the nominal reading), without they were at 0.01 or so close to zero my linear dial indicator could not measure it.



I compared that to the hall sensor end stop i made, and that was within 0.03, so still plenty fine for a printer.



Going to be interesting to see how the bltouch compare. I expect it to be better than the end stop I made, because I actually used the hall switch wrong, where in the bltouch they use it correctly.


---
*Imported from [Google+](https://plus.google.com/105970952711765020046/posts/fy2NQsnmeWQ) &mdash; content and formatting may not be reliable*
