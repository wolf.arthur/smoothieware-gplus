---
layout: post
title: "Crazy homing idea: Considering with the original smoothieboard we've got software configurable configurable stepper current, for axes that we don't need much accuracy on like x/y on a cartesian machine, couldn't we just omit"
date: February 15, 2017 03:34
category: "General discussion"
author: "Chris Chatelain"
---
Crazy homing idea:



Considering with the original smoothieboard we've got software configurable configurable stepper current, for axes that we don't need much accuracy on like x/y on a cartesian machine, couldn't we just omit the endstop, turn down the stepper current and run the axis into the hard stop and set the zero point?

For a machine that's 200x200 for example:



M906 A300 B300 ; turn stepper current down to 0.3A, barely able to move mine



G0 X-220 Y-220 ; run it a little more than the maximum axis distance, and let it click away

M18 ; Disable motors

M906 A2000 B2000 ; turn stepper current back up to normal, for me it's 2A

M17 ; Enable motors. This should make it jump to the nearest full step.

G92 X0 Y0 ; Set X and Y to zero, or -5, or whatever you like



G28 Z0 ; home the Z axis as normal



Obviously wouldn't work on a delta, but 2 less endstops, pairs of wires, 2 less opportunities for crosstalk, opportunities for wires to come off, etc...





**"Chris Chatelain"**

---
---
**Chris Chatelain** *February 15, 2017 03:44*

actually, if you've got a Z probe, you don't even need to home Z, just probe before each print.


---
**Maxim Mluhov** *February 15, 2017 06:30*

I have tried few months ago this method using different electronics and firmware on my delta with z-probe. Printer is very loud hitting corners and initial homing isn't very accurate, but this method working on deltas too.


---
**Arthur Wolf** *February 15, 2017 10:04*

This should work, it's just not very accurate.


---
*Imported from [Google+](https://plus.google.com/116517474949143363665/posts/UMyqiMURpVp) &mdash; content and formatting may not be reliable*
