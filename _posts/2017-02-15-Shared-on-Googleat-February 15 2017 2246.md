---
layout: post
title: "Shared on February 15, 2017 22:46...\n"
date: February 15, 2017 22:46
category: "General discussion"
author: "Arthur Wolf"
---
[https://github.com/Smoothieware/Smoothieware/blob/c61f7282e86fee100dacdb7f8d4067dd5d5439c5/CODE_OF_CONDUCT.md](https://github.com/Smoothieware/Smoothieware/blob/c61f7282e86fee100dacdb7f8d4067dd5d5439c5/CODE_OF_CONDUCT.md)





**"Arthur Wolf"**

---
---
**Eric Lien** *February 15, 2017 23:15*

I for one am very happy to see this being promoted. The activity of the community is it's greatest strength, and today's newbies are tomorrow's power users who can in tern give back to the next new member.


---
**Ray Kholodovsky (Cohesion3D)** *February 15, 2017 23:55*

Exponential growth of support!


---
**Marc Miller** *February 16, 2017 05:52*

This is great Arthur.


---
**Joe Spanier** *February 16, 2017 13:54*

Excellent


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/b8bT9JimsH2) &mdash; content and formatting may not be reliable*
