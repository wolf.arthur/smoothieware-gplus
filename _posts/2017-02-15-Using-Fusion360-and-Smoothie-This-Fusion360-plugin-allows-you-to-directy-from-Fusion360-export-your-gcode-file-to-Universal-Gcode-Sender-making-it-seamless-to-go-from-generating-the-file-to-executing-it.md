---
layout: post
title: "Using Fusion360 and Smoothie ? This Fusion360 plugin : allows you to directy, from Fusion360, export your gcode file to Universal Gcode Sender, making it seamless to go from generating the file, to executing it"
date: February 15, 2017 20:22
category: "General discussion"
author: "Arthur Wolf"
---
Using Fusion360 and Smoothie ?

This Fusion360 plugin : [https://github.com/tapnair/UGS_Fusion](https://github.com/tapnair/UGS_Fusion) allows you to directy, from Fusion360, export your gcode file to Universal Gcode Sender, making it seamless to go from generating the file, to executing it.



Neat.





**"Arthur Wolf"**

---
---
**Chris Brent** *February 16, 2017 21:19*

Wow!


---
**Thomas “Balu” Walter** *February 17, 2017 08:00*

I was like, "huh? Since when does Fusion360 have a slicer that generates gcode" before I realized you are not talking about printers... =)


---
**Arthur Wolf** *February 17, 2017 08:23*

**+Thomas Walter** actually , I believe it has a basic slicer at this point, I never tried it though


---
**Thomas “Balu” Walter** *February 17, 2017 08:26*

**+Arthur Wolf** It does slice objects into layers, but it doesn't do paths for 3d printers afaik. They just did a quick tip video on it. Interesting yes, but not for 3d printing :)


{% include youtubePlayer.html id="4fFH_TcI-9U" %}
[youtube.com - QUICK TIP: Slicer for Fusion 360](https://www.youtube.com/watch?v=4fFH_TcI-9U)


---
**Arthur Wolf** *February 17, 2017 09:09*

Eh right sorry sometimes I confuse things I know they are working on with things they have released already


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/N1p4yoCvNMq) &mdash; content and formatting may not be reliable*
