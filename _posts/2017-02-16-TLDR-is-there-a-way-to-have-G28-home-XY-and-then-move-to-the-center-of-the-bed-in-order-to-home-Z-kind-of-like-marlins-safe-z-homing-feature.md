---
layout: post
title: "TL;DR - is there a way to have G28 home X&Y and then move to the center of the bed in order to home Z (kind of like marlin's \"safe z homing\" feature)?"
date: February 16, 2017 15:02
category: "General discussion"
author: "Tom Keidar"
---
TL;DR - is there a way to have G28 home X&Y and then move to the center of the bed in order to home Z (kind of like marlin's "safe z homing" feature)?



Hi, first time smoothieware user.

I received my Re-ARM board yesterday, and spent most of the day connecting, testing and configuring.

since i'm using EPCOS G550 (4803 100K) thermistors, which are slightly different to the standard ones defined - i had to find the spec sheet again and have smoothie calculate their curves (would you consider adding them to smoothie's table?). and... i have to hand it to smoothie - it's extremely easy and friendly to do, and the wiki is a great resource. had some help as well from a more experienced smoothie user (thank you **+Ax Smith-Laffin** ).



basically, everything is now configured and ready to print... well, almost...

my printer is a cartesian one (moving bed) of my own design and build, built and re-built over the course of ~2yrs. one of it's features is an easily swappable hot bed (different sizes, heating capacities, and materials) and hot end (hotend is going through a re-design currently).



the printer's z endstop is a proximity sensor. for a few reasons...

mainly - setting a physical endstop is not practical for this type of a setup, because if the bed or the xz axes get displaced out of sync (my kids routinely play with the machine when it's idle - so xz axis is moved out of sync a LOT) it's easiest to just probe 2 sides of the bed and then re-align the xz axis since the bed is perfectly level and will be very hard to misalign. using a physical endstop would just result in 1 side being too close and the other too far - with a proximity sensor - it's always triggered at the same distance. 



having a swappable bed means that for about 80% of my prints i use the smaller beds (~50% 25x25cm and ~30% 10x10cm) and since it's too much of a hassle and a potential failure i don't relocate my endstops each time. instead - in marlin, what i used to do is their "safe-z-homing" where it basically homes x, then y and then homes z at the center of the bed (since those are always the same coordinates regardless of bed size).

i can't seem to find a comparable option, or way in smoothie to achieve the same effect.

and since the endstops are located where they can work with the 30x30cm bed - if i'm printing with the 25cm bed and i home (G28) it means that 0,0 is ~2.5cm to the left of the bed and the proximity sensor will not work.

bottom line - is there any way to have this setup working as is?



apologies for the long post - i wanted to explain my situation and reasoning, since i read the wiki's z-probe and endstops articles a few times, and felt the need to explain why my setup has those requirements. 





**"Tom Keidar"**

---
---
**Arthur Wolf** *February 16, 2017 15:11*

you can do this with multiple gcode commands, but not modify G28 to do this. it's simply not what it's definition says it should do, and we must follow definitions or it's going to be chaos

you can also use the switch module to define a new m-code that will run your specific series of G28/G1 codes, that would work, it just can't "replace" g28


---
**Tom Keidar** *February 16, 2017 15:35*

i can definitely understand wanting to conform to standards and your comment on chaos, especially in the case of G28.



i thought about the option of using a custom menu command, but my main concern is for a G28 command to be sent accidentally (or not) to the printer and the "head bashing" that would ensue... is there any configuration option to define G28 to only home X & Y and not Z - for safety reasons?


---
**Arthur Wolf** *February 16, 2017 15:40*

we don't have that, though I guess it could be nice to have.

one thing you could do right now ( which is a hack ) is to not have any z endstop configured, and instead configure your z endstop as a proble, then you use G28 to home XY, and G30 to home Z


---
**Tom Keidar** *February 17, 2017 00:19*

i'll try that first thing tomorrow morning. besides not being able to G28 Z0, would there be any other "side-effects" to using this solution?



also - i wouldn't mind implementing that safety masure. one of the reasons i wanted a board that could run smoothie is to be able to contribute, and this seems like a simple enough thing to start with. would that be ok?


---
**Wolfmanjm** *February 17, 2017 06:35*

i don't think this would be a generally useful feature, so please feel free to implement it in your own fork, but i don't think it is something that would be accepted into the mainline code.


---
**Wolfmanjm** *February 17, 2017 06:37*

the correct way to do it is as arthur says G28 X0 Y0 followed by G0 Xn Yn followed by G28 Z0


---
**Arthur Wolf** *February 17, 2017 09:11*

**+Wolfmanjm** If we had more ram ( like in v2 ), I think allowing users to specify a custom G28 routine ( that gets executed when G28 gets no parameters ) would be acceptable. Can't do it in v1 though.


---
*Imported from [Google+](https://plus.google.com/109773963965431418709/posts/RphWXSKpKJX) &mdash; content and formatting may not be reliable*
