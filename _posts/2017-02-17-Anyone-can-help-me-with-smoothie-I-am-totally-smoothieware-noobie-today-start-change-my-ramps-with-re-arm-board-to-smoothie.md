---
layout: post
title: "Anyone can help me with smoothie. I am totally smoothieware noobie, today start change my ramps with re-arm board to smoothie"
date: February 17, 2017 12:05
category: "General discussion"
author: "LassiVVV"
---
Anyone can help me with smoothie. I am totally smoothieware noobie, today start change my ramps with re-arm board to smoothie. Now many things works ok.



Example:

Hotend starts heat ok and right sensor.

Hotbed start heat ok and right sensor.

x y and z movement (with octoprint goes right ways and right axles)

graphic display works ok.



Problems.

1. When i send M80 command to start my ATX power, console give feedback: "Recv: Kill button pressed - reset or M999 to continue"

And if i write again M80 power start up normally and work ok. M81 command works ok.



2. Maybe bigger problem is  zprobe (z endstop?)



Can i use inductive sensor (plugged min enstop pin) to use that z endstop and same time to zprobe because to offset and autobed leveling?



Now i not get any signal changes to zprobe display (M119) when i trigger inductive sensor. Inductive sensor give 3.3volts mainboard and 0.2volts when not triggered.



M119 give everytime same Probe: 0 back to me. Is that something to smoothiewiki tells probe not can be used z endstop? Is that true or can i do some settings to use same inductive sensor probe z and endstop same time?



M119 command give this kind feedback:

Recv: X_min:1 Y_min:1 Z_min:0 pins- (X)P1.24:1 (Y)P1.26:1  Probe: 0







So many thanks if someone can help me with this.





**"LassiVVV"**

---
---
**Arthur Wolf** *February 17, 2017 14:35*

The M80 problem sounds like a problem with the kill button ( see for example [smoothieware.org - stopping-smoothie [Smoothieware]](http://smoothieware.org/stopping-smoothie) )



On the Zprobe problem : first, it's not recommended to use inductive probes, they detect surfaces not points, that makes them innacurate and inadequate.

You can use a zprobe both as a zprobe and as a zmin endstop, just configure them as both. It's not officially supported, but users have reported it works.



About the inductive sensor not working, i'm not sure what's going on, maybe somebody else has an idea.


---
*Imported from [Google+](https://plus.google.com/112209274773346118200/posts/iNmBr9HXKNn) &mdash; content and formatting may not be reliable*
