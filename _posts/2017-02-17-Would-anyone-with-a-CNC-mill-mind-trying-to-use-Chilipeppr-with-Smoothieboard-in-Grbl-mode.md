---
layout: post
title: "Would anyone with a CNC mill mind trying to use Chilipeppr with Smoothieboard in Grbl-mode ?"
date: February 17, 2017 19:51
category: "General discussion"
author: "Arthur Wolf"
---
Would anyone with a CNC mill mind trying to use Chilipeppr with Smoothieboard in Grbl-mode ? ( [http://smoothieware.org/grbl-mode](http://smoothieware.org/grbl-mode) )

They never added support for Smoothie, but now that we are very close to looking exactly like grbl, it's possible it will work with Smoothie if you tell it it's talking to grbl.

If somebody could test this I would be very grateful, and you could find out something that would be useful to a lot of people.



Thanks !





**"Arthur Wolf"**

---
---
**Anthony Bolgar** *February 17, 2017 20:14*

I will fire up the Openbuilds OX and see how it handles Chilipeper...always willing to help out the Smoothie world.


---
**Ray Kholodovsky (Cohesion3D)** *February 17, 2017 20:16*

Ohhhh


---
**Arthur Wolf** *February 17, 2017 20:19*

**+Anthony Bolgar** Thanks !!


---
**Anthony Bolgar** *February 17, 2017 20:26*

No problem, gives me an excuse to swap the grbl board back to the Smoothieboard, been meaning to do it for a while now. I should have some results for you by Monday. I will also try it with Estlcam, since it is so close to grbl.


---
**Ray Kholodovsky (Cohesion3D)** *February 17, 2017 20:28*

While you're at it, feel like milling that smoothie carrier for the STAMP, that you promised some 3-6 months ago? :)


---
**Anthony Bolgar** *February 17, 2017 20:29*

Sure thing **+Ray Kholodovsky**  , forgot all about that. Remind me where the files are?


---
**Ray Kholodovsky (Cohesion3D)** *February 17, 2017 20:30*

[https://github.com/raykholo/Single-Sided-Smoothie?files=1](https://github.com/raykholo/Single-Sided-Smoothie?files=1)





The "attempt 3" folder[github.com - Single-Sided-Smoothie](https://github.com/raykholo/Single-Sided-Smoothie/tree/master/Attempt%203)


---
**Anthony Bolgar** *February 17, 2017 20:32*

OK, got them.


---
**Eric Lien** *February 17, 2017 20:44*

I will give it a test this weekend too. Since switching to Smoothieware on my CNC I have just been sending my code via pronterface. I tried bcnc, but wasn't a big fan of the interface. Pronterface macros did 95% of what I needed anyway, so I just went with it. Plus I do like that pronterface runs as a native app, and it avoided any overhead of the browser on my little NUC PC streaming the code.



But Chilipeppr has that nice mesh level feature which is handy, so more options is always better :)


---
**Arthur Wolf** *February 17, 2017 20:48*

**+Eric Lien** bCNC has mesh too, and if you learn to use it it's actualy very very good. definitely great if you use the machine <b>a lot</b>.


---
**Steve Anken** *February 17, 2017 21:24*

**+Ray Kholodovsky**, me too on that stamp. Sorry, I need to try again with the new stuff but I am currently having two problems on two different machine, sigh. Rain does not help... 


---
**Виктор Большаков** *February 17, 2017 22:54*

I try almost everything while experimenting with PCB isolation milling. bCNC give best results. Chilipeppr hang several times on serial comminications.


---
**Wolfmanjm** *February 18, 2017 00:35*

I suspect streaming from chili pepper will notwork if it thinks it is grbl as it will be trying to use the grbl serial buffering which will not work with smoothie. only ping pong works


---
**Anthony Bolgar** *February 18, 2017 00:40*

Thanks for the heads up, I guess I will soon find out though ;)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/br1LXbEcjjM) &mdash; content and formatting may not be reliable*
