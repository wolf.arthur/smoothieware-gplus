---
layout: post
title: "A lot of new users have been asking for this, so I finally wrote : For instructions on migrating from Marlin to Smoothie"
date: February 20, 2017 15:56
category: "General discussion"
author: "Arthur Wolf"
---
A lot of new users have been asking for this, so I finally wrote : 



[http://smoothieware.org/from-marlin](http://smoothieware.org/from-marlin)



For instructions on migrating from Marlin to Smoothie.



Please fix any errors you find, this is a free-to-edit wiki



If somebody would like to write a similar guide for Repetier, Teacup or GRBL, that would be extremely welcome. It's not <b>that</b> much work, it took me only two hours doing this one.







**"Arthur Wolf"**

---
---
**Len Trigg** *February 21, 2017 23:57*

FYI, the mobile site is completely unusable (on Android under both chrome and Firefox). The black top menu thing is expanded to take up the entire visible screen with no scrolling possible. (If I request the desktop version it is OK)


---
**Arthur Wolf** *February 21, 2017 23:58*

**+Len Trigg** That has been reported yes but it sounds very difficult to fix. It's on my todo list though.


---
**Jarek Szczepański** *February 24, 2017 13:46*

it's relatively easy to fix. where I should send my fix?


---
**Arthur Wolf** *February 24, 2017 14:09*

**+Jarek Szczepański** You can just email me


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/cd7nABFPEUt) &mdash; content and formatting may not be reliable*
