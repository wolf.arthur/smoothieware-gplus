---
layout: post
title: "Maybe stupid question but try to my z axis height adjusted right and not get that working :S Problem is G32 i think (leveling 3point)"
date: February 20, 2017 10:29
category: "General discussion"
author: "LassiVVV"
---
Maybe stupid question but try to my z axis height adjusted right and not get that working :S



Problem is G32 i think (leveling 3point). I think that someway overwrite my M306 Z0 value and do own values before printing.



Example:

After that i use Smoothiewiki instruction:

G28

G0 Z0

Then i jog nozzle right height and -->

M306 Z0

M500



Now nozzle is perfect lift, but..



If i now write G28 command. Printer goes all axis home otherwise ok, but nozzle stay too high. If i write M114 check axis that tell me this: X:0.0000 Y:0.0000 Z:2.5106 E:10.255



And when printing starting normally 

(my start gcode is this kind: 

M80 ; 

G1 Z10 ;

G32 ;)



That height is about that same 2.5mm, but graphic display says Z is 0.2mm. 



If i write command G0 Z0 nozzle goes ALWAYS right height of bed.



Hope you can give me advice with this @Arthur Wolf or someone else.





**"LassiVVV"**

---
---
**LassiVVV** *February 20, 2017 11:26*

Try this kind system too, but not working. 



M80 ; virtalähde päälle

G28 ; Home all axis 

G1 Z5 ; Lift Z axis 5mm 

G4 P2000 ; odota 2s

G32 ; Perform calibration 

G30 Z0.8 ; set Z height after probing 



After all this nozzle stays 0.6mm on bed surface. Perfect.. but after this command .. normal print start smoothie just drive center of print bed and not drive z axis at all. 



If i read that smoothiewiki and G30 instructions right that command put Z height 0.8mm and that printer do. Put when printing starts Z height just swapped value of 0.2mm and start printing.



After that G30 Z0.8command. Printer need to drive Z axis that 0.2mm value, not overwrite my 0.8mm Z axis spot.


---
**Arthur Wolf** *February 20, 2017 13:51*

Try doing : G92 Z0.8 after the G30


---
**LassiVVV** *February 20, 2017 15:36*

That seems works ok, but next prints and if manual control printer nozzle try to get inside to bed. 



Any tutorials or instructions how do right way that kind of cartesian system. I think many guys use this kind of system, but dont find any instructions on google or smoothiewiki. 



If you use probe to take z height and G32 auto leveling. Different questions and thinkings founded, but not any good working system. Marlin just put offset your nozzle -0.8 and enable auto leveling and that it everything works ok.



I believe smoothie have easy systen here too, but not find any instructions or tutorial how that do, that way what manufactor thinks.




---
*Imported from [Google+](https://plus.google.com/112209274773346118200/posts/WDygBLKaP6j) &mdash; content and formatting may not be reliable*
