---
layout: post
title: "For the GRBL users out there : A guide explaining what GRBL values correspond to what Smoothie values, to help with migrating"
date: February 21, 2017 11:20
category: "General discussion"
author: "Arthur Wolf"
---
For the GRBL users out there : 



[http://smoothieware.org/from-grbl](http://smoothieware.org/from-grbl)



A guide explaining what GRBL values correspond to what Smoothie values, to help with migrating.



A lot of users have been asking for this recently, so here it is.



If you find errors, please fix them, it is a free to edit wiki.





**"Arthur Wolf"**

---
---
**Dushyant Ahuja** *February 21, 2017 12:26*

**+Arthur Wolf** can you check why the mobile version of the site is not working correctly. I click on the link in the Google + app and all I get is the menu. 


---
**Arthur Wolf** *February 21, 2017 12:28*

**+Dushyant Ahuja** It sounds quite difficult to fix but I will look at it at some point


---
**Anthony Bolgar** *February 21, 2017 16:56*

BTW **+Arthur Wolf**, I had no success with chilipepper


---
**Arthur Wolf** *February 21, 2017 18:00*

**+Anthony Bolgar** Thats sort of expected, however it should be close to qworking, it's just about changing afew things about how serial coms work. I hope somebody will open an issue with CP to ask them to add support for this. Thanks a lot for testing !


---
**Anthony Bolgar** *February 21, 2017 21:16*

I will start an issue at CP for you, least I can do for all the support you have given me with hardware and setup info.


---
**Arthur Wolf** *February 21, 2017 21:17*

**+Anthony Bolgar** Awesome, Don't hesitate to tag me there, or to give me the link


---
**Anthony Bolgar** *February 21, 2017 21:25*

[github.com - tinyg](https://github.com/chilipeppr/tinyg/issues/43) **+Arthur Wolf**


---
**Arthur Wolf** *February 21, 2017 21:25*

Thanks !


---
**John Lauer** *February 23, 2017 03:59*

Hey guys, I saw the ticket on Github for this. I responded to that, but it would be great to get Smoothie support on CP. I do think it will take some folks to help out on the code if there are any volunteers.



The 1st part would just be getting a Smoothie buffer working in SPJS. It could make the most sense to use the TinyG buffer as the start point since that buffer is 100% perfect. That buffer does character counting and looks for an r:{} response to know the line got processed. I know Grbl sends an OK response instead, so guessing that's what you send back when in "grbl-mode". I know not a ton of folks know Go, but it really is so close to C it's pretty easy to take up.



For the 2nd part, it could make sense to fork the Grbl workspace (or TinyG workspace) and then modify as needed and then deploy at  [chilipeppr.com/smoothie](http://chilipeppr.com/smoothie) as the new standard workspace location.



I could help on direction/thoughts/approaches, but don't have time to write code.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/5bV5Zy8okHG) &mdash; content and formatting may not be reliable*
