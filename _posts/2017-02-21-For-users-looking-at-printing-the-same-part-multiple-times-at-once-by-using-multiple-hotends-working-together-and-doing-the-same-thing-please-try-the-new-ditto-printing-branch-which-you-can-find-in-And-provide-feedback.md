---
layout: post
title: "For users looking at printing the same part multiple times at once, by using multiple hotends working together and doing the same thing, please try the new \"ditto printing\" branch, which you can find in : And provide feedback"
date: February 21, 2017 14:11
category: "General discussion"
author: "Arthur Wolf"
---
For users looking at printing the same part multiple times at once, by using multiple hotends working together and doing the same thing, please try the new "ditto printing" branch, which you can find in : 

[http://smoothieware.org/third-party-branches](http://smoothieware.org/third-party-branches)



And provide feedback about it in Smoothie's github. Your testing would be very much appreciated.





**"Arthur Wolf"**

---
---
**Jason Frazier** *February 22, 2017 17:47*

Page is unreadable on Chrome for Android, the 8 bit icons menu is permanently displayed over 90% of the screen. Have to use desktop view to get half the screen for the page content.


---
**Arthur Wolf** *February 22, 2017 17:53*

**+Jason Frazier** That has been reported several times, but I'm not sure I'm capable of fixing it. I'll try though.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/NT54d7vYNp2) &mdash; content and formatting may not be reliable*
