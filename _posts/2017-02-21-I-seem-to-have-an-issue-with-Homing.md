---
layout: post
title: "I seem to have an issue with Homing"
date: February 21, 2017 04:23
category: "General discussion"
author: "Ax Smith-Laffin"
---
I seem to have an issue with Homing. It's a new Delta machine running an Azteeg X5 Mini with optical endstops. 



The issue is that after issuing a G28 generally only 2 of the pylons home correctly with the the 3rd ending up short, however if the Z pylon gets to the endstop first the other 2 end up short, sometimes by a great distance, you can imagine what happens when trying to run auto-calibration via G32.  



Wiring has been checked, voltage for the enstops has been changed via the jumper. Delta arm solution and Delta Homing is set. Current stable firmware has been put on the card and uploaded. Manually triggering the endstops shows they are working. 



I'm a little stumped.





**"Ax Smith-Laffin"**

---
---
**Douglas Pearless** *February 21, 2017 04:51*

Can you post your config file


---
**Eric Lien** *February 21, 2017 05:09*

Isn't there a max distance to home before it gives up value in there somewhere... Maybe it's set to short on that tower and stops?


---
**Wolfmanjm** *February 21, 2017 06:53*

yes your max_distance is not set correctly. check the wiki section on endstops and/or delta


---
**Ax Smith-Laffin** *February 21, 2017 09:13*

**+Wolfmanjm** - I'm assuming you mean gamma_max? I've had a look on the pages you suggest and via the wiki search but can't see a max_distance setting.


---
**Wolfmanjm** *February 21, 2017 09:23*

NO I mean alpha_max_travel (and beta and gamma)l


---
**Ax Smith-Laffin** *February 21, 2017 09:24*

Ah, sorry, Just woken up and am uncaffeinated ;)


---
*Imported from [Google+](https://plus.google.com/+AxSmithLaffin118/posts/HMv3ieRMheM) &mdash; content and formatting may not be reliable*
