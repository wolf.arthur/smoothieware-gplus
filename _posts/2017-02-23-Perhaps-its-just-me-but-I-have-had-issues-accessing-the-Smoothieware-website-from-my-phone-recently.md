---
layout: post
title: "Perhaps it's just me but I have had issues accessing the Smoothieware website from my phone recently"
date: February 23, 2017 12:27
category: "General discussion"
author: "Griffin Paquette"
---
Perhaps it's just me but I have had issues accessing the Smoothieware website from my phone recently. The page loads, but the menu bar is perpetually fixed taking up the entirety of the screen. Just wanted to bring this up just in case other people have the same issue. 





**"Griffin Paquette"**

---
---
**Arthur Wolf** *February 23, 2017 12:37*

Yes it sounds very difficult to fix, if someone with CSS magic skills can look at it and tell me how I would be eternally grateful.


---
**Steve Hogg** *February 23, 2017 12:41*

**+Griffin Paquette**​ It's not just you. Happens for me on mobile as well in Chrome and Firefox. Been like that for a while I think. Looks ok if you use the "request desktop site" option, but shouldn't need to.


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2017 14:58*

Have been experiencing this on my iPhone as well, the request desktop site or anything else does not help. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 24, 2017 05:45*

Just checked it out using Chrome Dev tools (to mimic other devices) & sure enough I see the same thing.



A quick fix would be to take the ".nav" or ".navbar-nav" classes & put "visibility: hidden;" into the CSS (based on @media query breakpoints for resolutions below a certain point).



Then, a solution to have the menu actually work would be to attach a JQuery event onclick for the hamburger menu (the three lines thing) that sets "visibility: visible" (or the appropriate JQuery command) or "visibility: hidden" (depending on its current state). This would effectively turn the menu on/off.



edit: Note, it seems the current breakpoint for the menu change is @ width <768px. So alternatively, a restyling of the menu for width <768px may prove simpler than adding JQuery into the mix.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 24, 2017 05:56*

Actually, after further investigation, the whole structure of that menu seems odd to me. You have <LI> tags around the image icon </LI> then you have <LI> tags around the name, e.g. SmoothieBoard </LI>.



It seems to me that the structure would be better suited to be something like <LI>SmoothieBoard</LI> & have the image set as a :before in the CSS.



E.g. <LI class='navitem smoothieboard'>SmoothieBoard</LI>



then in CSS



LI.navitem:before {

//the required code to place the icon to the left of the LI

//you won't set the URL for the image here, except as a "default" icon

}

LI.navitem {

//add some code to allow space to the left of the text for the icon, e.g.

padding-left: 50px;

}



LI.navitem.smoothieboard:before {

//set the specific URL for the smoothieboard icon. all other settings would be done by the main LI.navitem:before rule

}


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 24, 2017 06:01*

Another thought appears, which may be even simpler again. All the items in the UL (unordered list) could stay unaltered.



You could just change the relevent LI css to make each item "float", or "inline-block".



So, you could try changing this:



.nav>li {

    position: relative;

    display: block;

}

which is at bootstrap.min.css:5 (I assume that's maybe line 5?, not too sure).



Change to:



.nav>li {

    position: relative;

    float: left;

    display: block;

}



Tested just now in Chrome Dev Tools & it puts all the list items in a line (left to right) which means the menu takes up far less space vertically.



Not 100% perfect (i.e. icons may end up of different lines to the text that matches them) but it will make it functional at least.


---
*Imported from [Google+](https://plus.google.com/111302122377301084540/posts/Q21FXQNMJjZ) &mdash; content and formatting may not be reliable*
