---
layout: post
title: "Not a question or anything, I just wanted to show the home for my smoothieboard :D Going to use 16AWG wire (10AWG in the picture, the 16AWG is still in the mail)"
date: February 27, 2017 17:21
category: "General discussion"
author: "Michael Andresen"
---
Not a question or anything, I just wanted to show the home for my smoothieboard :D Going to use 16AWG wire (10AWG in the picture, the 16AWG is still in the mail). With 16AWG silicone wire, it should be plenty easy for them to bend and go into the connectors, without breaking anything or causing excessive twisting on the connectors.



The entire plate is a new electronics plate for my Polygon delta printer. I was very worried if the shape would be correct, but it was, now I just need to find a way to secure it, but I am sure I will find a way. :)



There are also slots in front of the power supply so I can use zip ties to hold the cables in place, and they go up all the side of the power supply too, so wires for the smoothieboard can be held in place too.

![missing image](https://lh3.googleusercontent.com/-_4wS3nCF_54/WLRgCIfVdwI/AAAAAAAAb-A/hgjyKGZRs783zNAJvmrwI-65KBQ2Lg8bQCJoC/s0/IMG_20170227_180414.jpg)



**"Michael Andresen"**

---
---
**Michael Andresen** *February 28, 2017 18:40*

And with some wires connected. :)

![missing image](https://lh3.googleusercontent.com/P4757FJHBx-3jcHDzogz3E_XA9FfPkUJ9UB4P6r0TconG-EMQFHt03uEym5bB98rT9VstS7_VPskPbfJINWgEwJbFoqxiK2bqvz8=s0)


---
*Imported from [Google+](https://plus.google.com/+MichaelAndresen/posts/5FPowS9c6C4) &mdash; content and formatting may not be reliable*
