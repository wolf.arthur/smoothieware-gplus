---
layout: post
title: "this is weird? Why does Cura Start printing the part before the skirt?"
date: February 27, 2017 18:24
category: "General discussion"
author: "jacob keller"
---
this is weird? Why does Cura Start printing the part before the skirt? tried different setting can't seem to get any results.



Any idea what is going on.



gcode file



[https://www.dropbox.com/s/ysmccpkdjxwx5ut/thing-115200.gcode?dl=0](https://www.dropbox.com/s/ysmccpkdjxwx5ut/thing-115200.gcode?dl=0)









**"jacob keller"**

---
---
**Glenn Beer** *February 28, 2017 16:15*

Doesn't seem like it would make any difference to the purpose of the skirt. 


---
**jacob keller** *February 28, 2017 21:28*

**+Glenn Beer** **+Arthur Wolf** 



Here's a video of my printer printing today. middle of the print it stops and retracts for about 2 minutes. and then it starts back up  but theres no filament in the extruder to print with.



I now it not my start gcode



Here's my start gcode.



;Sliced at: {day} {date} {time}

;Basic settings: Layer height: {layer_height} Walls: {wall_thickness} Fill: {fill_density}

;Print time: {print_time}

;Filament used: {filament_amount}m {filament_weight}g

;Filament cost: {filament_cost}

;M190 S{print_bed_temperature} ;Uncomment to add your own bed temperature line

;M109 S{print_temperature} ;Uncomment to add your own temperature line

G21        ;metric values

G90        ;absolute positioning

M82        ;set extruder to absolute mode

M107       ;start with the fan off

G28 X0 Y0  ;move X/Y to min endstops

G28 Z0     ;move Z to min endstops

M208 S-0.05 F180

M207 S1 F1800

G1 Z15

G92 E0

G1 E12.5 F100

G92 E0



and Here's the full gcode file.

[dropbox.com - Spool_holder3.gcode](https://www.dropbox.com/s/dbizosmg99lwbo1/Spool_holder3.gcode?dl=0)



Here's the video of me trying to push the filament back in but its still retracting.

[https://www.dropbox.com/s/diu8n4asy0a4l8a/VID_20170228_140637215.mp4?dl=0](https://www.dropbox.com/s/diu8n4asy0a4l8a/VID_20170228_140637215.mp4?dl=0)


---
**jacob keller** *March 01, 2017 01:06*

**+Glenn Beer** **+Arthur Wolf**

So I was looking at my gcode again.



and I find this. would this cause a problem. like with the filament coming out of the extruder?



G1 F1200 X147.880 Y112.334 E10044.40612

G0 F4800 X145.559 Y111.618

G1 F1200 X144.699 Y110.758 E10044.47196

G1 F150000 E10043.47196

G1 Z30.100

G0 F4800 X126.462 Y115.148

G1 Z29.100

G1 F150000 E10044.47196

G92 E0 

G1 F1200 X125.996 Y114.682 E0.03568

G0 F4800 X120.927 Y120.683

G0 X118.818 Y124.100

G0 X116.418 Y127.731

G1 F1200 X116.946 Y128.259 E0.07610

G0 F4800 X116.159 Y134.460

G0 X116.551 Y139.336

G0 X117.316 Y142.898

G1 F1200 X116.792 Y143.422 E0.11621


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/h9RU82dvqU9) &mdash; content and formatting may not be reliable*
