---
layout: post
title: "Hey guys, I havent done much. with Smoothieboard so far but I plan to use it noe for a small diode laser setup"
date: March 01, 2017 01:15
category: "General discussion"
author: "Frank \u201cHelmi\u201d Helmschrott"
---
Hey guys,



I havent done much. with Smoothieboard so far but I plan to use it noe for a small diode laser setup. I plan to power with 12V for the steppers and will put the voltage regulator on for the 5V smoothie power. 



As far as I understamd the laser module the pin defined there can be directly take from the respective headers (e.g. 2.5 on JP33) to directly take the PWM from the board and not through the mosfet. 



And here's my questions: My laser driver ([https://lasertack.com/Media/Uploaded/Datasheets_software/SHS-2500-EN.pdf](https://lasertack.com/Media/Uploaded/Datasheets_software/SHS-2500-EN.pdf)) wants to see between 0.2-0.3V (low/idle power) and 5V modulation voltage. Does the smoothieboard deliver 5V on the PWM ports or 3.3V? I alread found different information there. 



Is the config for max_power and tickle_power changing this voltage? I wonder if this will work for my driver and want to be clear instead of breaking things.



Any help is appreciated. thanks.





**"Frank \u201cHelmi\u201d Helmschrott"**

---
---
**Anthony Bolgar** *March 01, 2017 01:43*

I run the smoothie pin through a level shifter to get the 5V range


---
**Arthur Wolf** *March 01, 2017 10:21*

Smoothie outputs 3.3v on all pins ( including the pwm pins ), if you saw otherwise somewhere, I need to know exactly where to fix it.

You can "simulate" a 5V output with the open-drain configuration, see the documentation on using external drivers for an example.


---
**Frank “Helmi” Helmschrott** *March 01, 2017 11:12*

Thanks Arthur. I'm sure it wasn't on the official site but I will definitely let you know if I see something. I'll look into open drain. So but basically I'm right that these power config vars are there to regulate the output voltage? Then i could definitely do it like I thought with either open drain or a level shifter?




---
**Arthur Wolf** *March 01, 2017 11:17*

"these power config vars" ?

open-drain essentially means the "switch" in the pin "switches" to ground instead of switching to 3.3v, which means that if you then connect the peripheral to that ( which switches to ground ) and to 5v, it's the same as connecting it to that ( which was 3.3v ) and to ground, in the "non-open-drain" system.


---
**Frank “Helmi” Helmschrott** *March 01, 2017 11:25*

yeah sorry for beeing the noob here - I'm just a dumb person when it comes to electronics, that's why my questions or wording may sound dumb too. 



I was more referring to my initial question that was if the vars in the laser module config that change max_power and tickle_power influence this output voltage (percentage wise). In the documentation this only referred to as "power" and I was unsure if this effectively changes the voltage and is what the driver expects to get. 


---
**Arthur Wolf** *March 01, 2017 11:26*

It changes the pwm output, that is "how often" it is on. See [https://en.wikipedia.org/wiki/Pulse-width_modulation](https://en.wikipedia.org/wiki/Pulse-width_modulation)


---
**Don Kleinschnitz Jr.** *March 01, 2017 16:59*

**+Arthur Wolf** I think that the confusion about 3.3 v or not might stem (it did for me) that the P(x) values really reference the chip signals not necessarily the output pin depending on what pin you are using. For the internal pins they are the same but for example the drivers they are not. 

Some diagrams label the output with the P number.

[smoothieware.org - pinout [Smoothieware]](http://smoothieware.org/pinout)



When in fact that P is the input to the drive not the output and whether you invert it or not depends on if you are using internal pins or the driver and what way you are using the driver i.e open drain or pulled up. Of course the voltage on the output also depends on how the driver is wired.


---
**Don Kleinschnitz Jr.** *March 01, 2017 17:15*

**+Frank Helmschrott** glancing at your Laser Diodes spec sheet it looks like its PWM input is a TTL like signal, so it wants 0-.8 for low and 2-5v for high. 



A way to get this control input is to use one of the smoothies MOSFET outputs connected to a pull-up to the laser drivers supply (Vin) as the laser driver. The output of the smoohties MOSFET is connected to the MOD input on the laser diode driver.



Insure that you choose a MOSFET who's P(x.x) input is PWM capable and configure it correctly in the config file. 



It may be prudent to put a filter across the Vin supply at the driver to eliminate noise.



![missing image](https://lh3.googleusercontent.com/emD43KC5OPv-ay_QKTd8vlCYXWgRc2P-DYkvUsy3IdEzVUvUEsxwpBQ943jr6l2uDQaKOpp7c8QjLCv0ucrLZ6YzsXm1SW4y1zsC=s0)


---
**Brian Mitchell** *March 05, 2017 13:51*

**+Arthur Wolf** Hi Arthur. I stumbled upon your response here in my vain search for answers to why I can't get my DQ524MA driver to work after following the external driver docs you referred to.  I've wired everything as recommended and checked all of my wires.  After much troubleshooting I've gotten to the point where I can get a green light on the driver and torque on the stepper   As soon issue a move command I get an alarm on the driver and lose torque.   I checked the voltage on the end stop pins and it's only 4.8.  Could this be my issue? Is USB power not adequate for this and do I need a separate 5v when using external drivers?  I have an old brick that does 5.5v could smoothieboard use this?  Thanks!




---
**Arthur Wolf** *March 06, 2017 11:26*

No, 4.8V should be way more than enough. And if you were lacking power it wouldn't go into alarm mode anyway.

If it goes into alarm mode I would suspect either bad wiring, a bad driver, a bad motor or a bad power supply ...


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/aQmEVfdB4AR) &mdash; content and formatting may not be reliable*
