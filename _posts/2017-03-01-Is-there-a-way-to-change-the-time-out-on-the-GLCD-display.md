---
layout: post
title: "Is there a way to change the time out on the GLCD display?"
date: March 01, 2017 19:59
category: "General discussion"
author: "Tony Sobczak"
---
Is there a way to change the time out on the GLCD display?  E.G. when I want to do several test fires on the laser, the test fire screen disappears and goes back to the home screen.  I'd like that changed to something on the order of 20 seconds.



TIA>





**"Tony Sobczak"**

---
---
**Wolfmanjm** *March 02, 2017 05:32*

no it is hard coded to 10 seconds unless you select the Keep on, then it will stay on for about 10 minutes. You could edit the source code and change the dms->set_timeout(10) to dms->set_timeout(20) in Laserscreen.cpp line 79


---
**Tony Sobczak** *March 15, 2017 05:57*

I'll try that, thank you.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/bQVTJqFmENC) &mdash; content and formatting may not be reliable*
