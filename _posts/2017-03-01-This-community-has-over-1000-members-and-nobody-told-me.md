---
layout: post
title: "This community has over 1000 members and nobody told me !"
date: March 01, 2017 15:47
category: "General discussion"
author: "Arthur Wolf"
---
This community has over 1000 members and nobody told me ! Congrats little #Smoothieware !





**"Arthur Wolf"**

---
---
**Maxime Favre** *March 01, 2017 20:04*

Cheers !




---
**Marc Miller** *March 02, 2017 11:52*

Huzzah!


---
**Stephane Buisson** *March 02, 2017 12:28*

C3D nearly 250

the OX group on it's way to 500

Smoothieware >1000

Laserweb on it's way to 2000

K40 >3000



Good dynamic all together ;-))



**+Arthur Wolf** **+Peter van der Walt** **+Ray Kholodovsky**



Ps: Arthur tu devrai créer des categories (in edit community), c'est vraiment utile pour faciliter les utilisateurs dans leurs recherches.


---
**Arthur Wolf** *March 02, 2017 14:06*

**+Stephane Buisson** Can't find where to do that ...


---
**Stephane Buisson** *March 02, 2017 14:10*

**+Arthur Wolf** very simple , you need to login with owner account.

the 3 little white dots more option menu (hidden in your mobo graphic next to 5v) and choose edit community menu) it's all there.


---
**Stephane Buisson** *March 02, 2017 14:21*

**+Arthur Wolf** it seem you found it I can see the categories appearing ;-))



look c3d for exemple:

[https://plus.google.com/u/0/communities/116261877707124667493](https://plus.google.com/u/0/communities/116261877707124667493)


---
**Arthur Wolf** *March 02, 2017 14:21*

**+Stephane Buisson** Yeah thanks !


---
**Matt Wils** *March 03, 2017 07:53*

Congrats. 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/UvAq5U6thsm) &mdash; content and formatting may not be reliable*
