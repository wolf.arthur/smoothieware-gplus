---
layout: post
title: "So I can get absolute extrusion to work with my printer maybe absolute isn't made for my printer"
date: March 03, 2017 03:54
category: "General discussion"
author: "jacob keller"
---
So I can get absolute extrusion to work with my printer maybe absolute isn't made for my printer.



so I think I might try relative extrusion. never tried relative extrusion.



if anyone has any thoughts on relative gcode. please help me



like start gcode for a build area of 280mm x 270mm x 230mm



thanks      





**"jacob keller"**

---
---
**Triffid Hunter** *March 03, 2017 06:02*

Relative extrusion causes your printer to push the wrong amount of plastic due to accumulation of floating point error.



Absolute extrusion averages errors to zero between consecutive moves, but requires the gcode generator to periodically emit G92 E0 to keep the floating point value within it's high precision range.



Stick with absolute, there's no good reason to bother with relative. Relative is only supported because some gcode generators are stupid.


---
**jacob keller** *March 03, 2017 06:06*

**+Triffid Hunter**  absolute doesn't work with my printer. Cura,slic3r keeps putting  at G92 E0 in the middle of the gcode leaving me with a failed print every time. When it gets to the middle of the print it retracts the filament for a bought 2 minutes making the filament come out of the extruder.


---
**Triffid Hunter** *March 03, 2017 07:54*

Then you've done something wrong, almost everyone using smoothie for 3d printing is using absolute mode and you're the only one reporting this issue.



I've personally never even tried relative mode and I used slic3r exclusively with no problems and many, many beautiful prints.


---
**Hakan Evirgen** *March 03, 2017 07:58*

**+jacob keller** there is something else wrong. G92 E0 should just reset the counter but not make any movement. 


---
**jacob keller** *March 03, 2017 13:32*

**+Triffid Hunter****+Hakan Evirgen** this is what it looks like where the extruder retracts the filament. Tried this print many times with different slicer's.





G1 F1200 X147.880 Y112.334 E10044.40612

G0 F4800 X145.559 Y111.618

G1 F1200 X144.699 Y110.758 E10044.47196

G1 F150000 E10043.47196

G1 Z30.100

G0 F4800 X126.462 Y115.148

G1 Z29.100

G1 F150000 E10044.47196

G92 E0 

G1 F1200 X125.996 Y114.682 E0.03568

G0 F4800 X120.927 Y120.683

G0 X118.818 Y124.100

G0 X116.418 Y127.731

G1 F1200 X116.946 Y128.259 E0.07610

G0 F4800 X116.159 Y134.460

G0 X116.551 Y139.336

G0 X117.316 Y142.898

G1 F1200 X116.792 Y143.422 E0.11621


---
**Hakan Evirgen** *March 03, 2017 14:05*

This code looks ok for me. I think that there is something wrong with your printer or its settings. Check also firmware. Which firmware are you using anyway?


---
**jacob keller** *March 03, 2017 14:12*

**+Hakan Evirgen** The latest version of smoothie firmware. What could it be in the slicer or the firmware?


---
**Hakan Evirgen** *March 03, 2017 16:34*

Can you please check on your sd card if there are other files except for the config file? There may be a file which puts additional config parameters. Remove all but the config file.


---
**jacob keller** *March 03, 2017 18:18*

I'll check. And I'll make a new configuration file


---
**jacob keller** *March 03, 2017 23:50*

**+Hakan Evirgen** I checked my SD card and there's the configuration file and the firmware file


---
**Hakan Evirgen** *March 04, 2017 09:44*

That is really strange. Does it do this also when you manually extrude 20cm, then issue g92 e0 and then extrude 5 cm more?


---
**jacob keller** *March 04, 2017 17:09*

**+Hakan Evirgen**​

Only when I print


---
**jacob keller** *March 04, 2017 19:34*

**+Hakan Evirgen** here's a link to my config file. im not sure what im looking for. maybe someone on here can.



thanks

[https://www.dropbox.com/s/aqh30bd349ok2hg/config?dl=0](https://www.dropbox.com/s/aqh30bd349ok2hg/config?dl=0)


---
**Hakan Evirgen** *March 05, 2017 01:44*

I see nothing out of the ordinary. Maybe something in your slicer configuration...


---
**jacob keller** *March 05, 2017 02:28*

**+Hakan Evirgen** made a new configuration file. Testing the printer with a test file. Will see in an hour.


---
**jacob keller** *March 05, 2017 04:30*

**+Hakan Evirgen** looks like it's working. Will see tomorrow when I print something bigger.


---
**jacob keller** *March 06, 2017 02:56*

**+Hakan Evirgen** new configuration file. After 5 hours a Successful print. No big retracts and everything is working. Thanks for the troubleshooting.


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/XuDyeKhBsRs) &mdash; content and formatting may not be reliable*
