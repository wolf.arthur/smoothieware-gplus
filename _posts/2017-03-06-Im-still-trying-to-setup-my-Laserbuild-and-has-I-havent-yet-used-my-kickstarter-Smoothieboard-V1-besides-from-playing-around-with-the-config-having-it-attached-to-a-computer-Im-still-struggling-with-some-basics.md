---
layout: post
title: "I'm still trying to setup my Laserbuild and has I haven't yet used my kickstarter Smoothieboard V1 besides from playing around with the config having it attached to a computer I'm still struggling with some basics"
date: March 06, 2017 17:54
category: "General discussion"
author: "Frank \u201cHelmi\u201d Helmschrott"
---
I'm still trying to setup my Laserbuild and has I haven't yet used my kickstarter Smoothieboard V1 besides from playing around with the config having it attached to a computer I'm still struggling with some basics.



I've bought a Recom R-78E5.0-0.5 and soldered it in. My plan was to power it with 12V from a power supply and having everything powered with that. That didn't work. After unsoldering the Recom again i tried powering it through the 5V pins with an external step down that I had installed for a Raspberry Pi that will also be running in the setup. This one was a DSWY2596 with 2A(3A max) which should be good to go. After trying that out that worked first but quickly stopped working again. I found out that the DSWY suddenly only has 1.5V on the output side instead of 5.1V before (it's adjustable). I tried readjusting and it looks like it's broken - i can only adjust it up to 6.1V now instead of 12V before. I continued looking at the smoothieboard and tried to find out why the Recom didn't work. Interestingly I could measure +12V between the negative side on the 12V input and all 3 pins of the Voltage regulator. This looks wrong to me!?



I really don't have any deeper knowledge in electronics so bear with me if I did something wrong but to me this looks weird.



I'm sure I double checked wiring anytime before switching anything on so I can quite surely eliminate any wiring issues - at least as I knew everything right :)



Thanks for any help





**"Frank \u201cHelmi\u201d Helmschrott"**

---
---
**Arthur Wolf** *March 06, 2017 19:22*

Why didn't the Recom work ? It works for everybody else, that's not normal ... <b>How</b> didn't it work ?


---
**Frank “Helmi” Helmschrott** *March 06, 2017 19:30*

Well looks like I was just tricked by the reversed order of the pads behind the plug for the main power. Looks like plugged them wrong way :/ definitely my fault but definitely confusing :/




---
**Arthur Wolf** *March 06, 2017 19:31*

**+Frank Helmschrott** Oh the documentation warns about that, I believe in several places, yes.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/ALtzVAWRZ1g) &mdash; content and formatting may not be reliable*
