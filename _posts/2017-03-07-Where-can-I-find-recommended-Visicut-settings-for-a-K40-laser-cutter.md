---
layout: post
title: "Where can I find recommended Visicut settings for a K40 laser cutter?"
date: March 07, 2017 05:52
category: "General discussion"
author: "Mark Bowie"
---
Where can I find recommended Visicut settings for a K40 laser cutter?





**"Mark Bowie"**

---
---
**Alex Krause** *March 07, 2017 06:05*

Have no clue about visicut... But have you tried out laserweb? It's compatible with a smoothieboard as well


---
**Arthur Wolf** *March 07, 2017 09:57*

I don't think there is much to modify, just change the width and height, and enter the IP adress of your smoothieboard and it should work.


---
*Imported from [Google+](https://plus.google.com/106672726939466041484/posts/5uBWwwAfVrg) &mdash; content and formatting may not be reliable*
