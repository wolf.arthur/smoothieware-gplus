---
layout: post
title: "Hey, i'm quite happy with my progress on the small laser cutter"
date: March 08, 2017 11:12
category: "General discussion"
author: "Frank \u201cHelmi\u201d Helmschrott"
---
Hey, i'm quite happy with my progress on the small laser cutter. It's a CoreXY machine built on Openbuilds. I have the smoothie powered via PSU 12V and currently via USB (will have an external 5V source later).



Motors are hooked up, endstops are too. I already had everything configured right, motors ran on 0.5A current pretty well and Endstops worked. Homing worked too with one of the endstop beeing min, the other max.



I have only changed the steps/mm from 60 to 75 for both axis after measuring the travel and now face the problem that every time I do homing via Pronterface the Smoothieboard seems to hang itself.



Here's my config - probably any weird error/typo i'm not seeing? [http://pastebin.com/h7knBEKu](http://pastebin.com/h7knBEKu)



Sidenote: The whole laser part isn't configured at all yet. It's all just about movement currently.





**"Frank \u201cHelmi\u201d Helmschrott"**

---
---
**Frank “Helmi” Helmschrott** *March 08, 2017 11:16*

additional note: It doesn't matter if I home only one of the two axis or both. The same problems happens no matter what. Axis movement works well until the homing, then the board hangs up and doesn't respond to any gcode.



One more thing that may point to a problem: On every move everything looks great apart from the fact that a small amount of slow travel is added to the end. for example if i move X by 100 it does another 10 or so in really slow. I don't yet know where this comes from but may point out some problems?


---
**Frank “Helmi” Helmschrott** *March 08, 2017 11:26*

I also checked if the endstop do work and it looks like they do (untriggered and then triggered)



SENDING:M119

X_min:0 Y_max:0 Z_min:0 pins- (X)P1.24:0 (X)P1.25:0 (Y)P1.26:0 (Z)P1.28:0 (Z)P1.28:0

>>>M119

SENDING:M119

X_min:1 Y_max:1 Z_min:0 pins- (X)P1.24:1 (X)P1.25:0 (Y)P1.26:1 (Z)P1.28:0 (Z)P1.28:0




---
**Frank “Helmi” Helmschrott** *March 08, 2017 15:03*

This is really strange and i'm not sure if there's something broken on the hardwareside now. I'm getting very randomd feedrates on manual moves. Homging actions still lead to the board hanging itself up completely. Some travel moves on the x-axis (seems to be the x axis only while y moves normally but homing problem affects both) get slowed down to about 1 step per second.


---
**Frank “Helmi” Helmschrott** *March 08, 2017 15:53*

Looks like this in fact does also affect the Y axis but it seems to be happening less often. I've litterally changed everything in the config back and forth but can't seem to get rid of this problem. This is weird.


---
**Frank “Helmi” Helmschrott** *March 08, 2017 20:13*

After starting from a sample config again I found out that removing the extruder.hotend.enable and all of its config was a bad idea. As soon as no extruder config is present in the config these whole weirdness starts. Still digging the config for any confirmation of that but currently it looks like I even need an extruder config for the laser setup.


---
**Wolfmanjm** *March 09, 2017 05:30*

extruder is not even loaded on the cnc build which is what you need to be using. so I suspect you have a corrupt config.


---
**Frank “Helmi” Helmschrott** *March 09, 2017 07:29*

I found out that i need to use the cnc build for the laser but the problem persists. As soon as i take the extruder.hotend. section out or set it to false the errors reappear.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/BoAwnNkyCXP) &mdash; content and formatting may not be reliable*
