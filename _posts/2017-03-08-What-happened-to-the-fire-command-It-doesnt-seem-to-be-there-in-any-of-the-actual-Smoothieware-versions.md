---
layout: post
title: "What happened to the fire command? It doesn't seem to be there in any of the actual Smoothieware versions"
date: March 08, 2017 23:59
category: "General discussion"
author: "Frank \u201cHelmi\u201d Helmschrott"
---
What happened to the fire command? It doesn't seem to be there in any of the actual Smoothieware versions. How are we expected to turn on the laser manually on a given amount of power for testing/tuning? We tried the CNC version of the firmware and the normal edge version. 



Also I can't seem to get the open drain method to work. We're using 2.5 as the PWM pin and set it to 2.5o and also tried inverting it. We get either nearly 5V by default (2.5o) or around 1.5V (2.5o!) which seems completely weird. Everything seems to work fine in non-drain mode against 3.3V then of course.



We can still use a level shifter but the open drain method seemed so comfortable. Any ideas?





**"Frank \u201cHelmi\u201d Helmschrott"**

---
---
**Arthur Wolf** *March 09, 2017 00:10*

I don't believe the fire command has been removed, and if it were broken we'd have heard about it. What are you using to talk to the board ?

About open-drain, it makes the pin switch to ground instead of switching to 3.3v, so you need to measure things accordingly.


---
**Frank “Helmi” Helmschrott** *March 09, 2017 00:14*

I'm using Pronterface and Laserweb. Pronterface doesn't like the @ syntax but M1000 works accordingly - except from the fire command. This is from the master branch that I had currently installed. Tried the edge and edge+cnc before with same results:



>>>M1000 fire 10

SENDING:M1000 fire 10

Command not found: fire

>>>M1000 help

SENDING:M1000 help

Commands:

version

mem [-v]

ls [-s] [folder]

cd folder

pwd

cat file [limit] [-d 10]

rm file

mv file newfile

remount

play file [-v]

progress - shows progress of current play

abort - abort currently playing file

reset - reset smoothie

dfu - enter dfu boot loader

break - break into debugger

config-get [<configuration_source>] <configuration_setting>

config-set [<configuration_source>] <configuration_setting> <value>

get [pos|wcs|state|status|fk|ik]

get temp [bed|hotend]

set_temp bed|hotend 185

net

load [file] - loads a configuration override file from soecified name or config-override

save [file] - saves a configuration override file as specified filename or as config-override

upload filename - saves a stream of text to the named file

calc_thermistor [-s0] T1,R1,T2,R2,T3,R3 - calculate the Steinhart Hart coefficients for a thermistor

thermistors - print out the predefined thermistors

md5sum file - prints md5 sum of the given file







regarding open drain: I know, we took the 5V from the endstop and switch the pin to open drain like I said. Probaby this doesn't work for our solution but I don't think it's a problem of measuring as the laser driver still powers the diode by about 100% without anything beeing switched on. This is why I think it's not a measurement problem.


---
**Don Kleinschnitz Jr.** *March 09, 2017 00:31*

Is this a K40, what controller and what power supply?


---
**Frank “Helmi” Helmschrott** *March 09, 2017 00:37*

No, no K40, It's just a self built machine with a diode laser.


---
**Wolfmanjm** *March 09, 2017 05:25*

fire works perfectly well in edge. I suspect you did not flash the edge correctly. master does not support it. maybe you do not have the laser properly enabled in config?


---
**Joe Alexander** *March 09, 2017 06:02*

if your using the pin as open drain why'd you need to pull 5V from an end stop? (makes sense for the level shifter way but not on single wire method)


---
**Frank “Helmi” Helmschrott** *March 09, 2017 07:31*

**+Joe** I need a 5V-PWM signal. That's why. See this discussion [plus.google.com - Hey guys, I havent done much. with Smoothieboard so far but I plan to use it...](https://plus.google.com/u/0/+FrankHelmschrott/posts/aQmEVfdB4AR)



+Wolfmanjm The fact that only the edge build has the laser command is a first to me. I will retry the edge+cnc build and check back with the config if it continues to not work.


---
**Frank “Helmi” Helmschrott** *March 09, 2017 10:30*

Ok, I've just flash this cnc-version: [raw.githubusercontent.com - github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-cnc.bin?raw=true](https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-cnc.bin?raw=true)



>>>M1000 version

SENDING:M1000 version

Build version: edge-9399ed7, Build date: Feb  7 2017 22:14:13, MCU: LPC1768, System Clock: 100MHz

  CNC Build 3 axis



After flashing that my homing (corexy style) doesn't work, the Y axis just homes to the wrong direction and travels to 0 instead of homing to max (this worked great on the non cnc build).



Additionally when removing the whole extruder part (which shouldn't be loaded at all on the cnc build) from the config the homing changes to some cross travel (which looks like non-corexy) and the problems i described here ( ) reappear.



Here's my config [http://pastebin.com/YYJGMAkw](http://pastebin.com/YYJGMAkw) it would be great if someone could have a look at point me to what I did wrong. It's hard to find out all the little things as a beginner. It's taking a long time to collect all the info needed out of the documentation.



Thanks in advance.


---
**Frank “Helmi” Helmschrott** *March 09, 2017 10:31*

oh and by the way, the fire command isn't part of this build - or is there still a problem with my laser module config (see pastebin link above)



>>>M1000 fire

SENDING:M1000 fire

Command not found: fire


---
**Arthur Wolf** *March 09, 2017 10:34*

**+Frank Helmschrott** If upgrading to the latest edge, you should re-do your config from scratch using the example config provided on github. Also read the upgrade notes, a few things need to be taken into account.


---
**Frank “Helmi” Helmschrott** *March 09, 2017 11:18*

Thanks **+Arthur Wolf** - i had already used the sample config yesterday when rebuilding mine but unfortunately I haven't looked at the snippets. The endstops are configured differently in the main sample config and the endstops snippet. I've now reconfigured my endstops in an external file and included them but I still seem to face the same issues. As soon as removing the extruder config homing leads to the board crashing.



What am I missing?



This is my config with the latest changes [pastebin.com - Smoothieboard configuration file, see  ...](http://pastebin.com/raw/8UHkz0iM)



this is my endstops config [http://pastebin.com/raw/uNGmagQj](http://pastebin.com/raw/uNGmagQj)



The endstops seem to work well according to M119



Any help is highly appreciated.



Thanks! 


---
**Arthur Wolf** *March 09, 2017 11:33*

Why are you removing extruder ? You should be modifying the config file only strictly as little as you really need to.

In the same way, don't do the include if you don't need to, keep things as simple as possible. 


---
**Frank “Helmi” Helmschrott** *March 09, 2017 14:16*

To be honest **+Arthur Wolf** – this is hard to wrap your head around. I'm really no noob to all this cnc and printer world but this is kind of confusing. Keeping things simple for means to clean things up. Which normally means removing stuff that already is commented out and not used. Also I understood the modularity of smoothie in a way that you don't enable modules that you don't need. Why am I removing the extruder? Well it's a laser which doesn't have an extruder. The fact that the extruder configuration is key for the reliable movement of the axis didn't come to my mind. Especially after people told me the extruder module isn't even loaded in the CNC build.



Regarding the endstop include: It's pointed out that this is a new syntax for the endstops which should be used. Packing this into an include makes sense for me as the main config doesn't get too long this way. Now you say I should just keep it like it's in the sample config. Isn't that the old syntax?



Ok, I'll redo the whole config again then and see if homing will suddenly work. The reason for all that changes were that homing suddenly didn't work again after the upgrade to edge+cnc.


---
**Don Kleinschnitz Jr.** *March 09, 2017 15:00*

**+Frank Helmschrott** sorry I got lost.

Is your PWM working at all or are you just trying to get the FIRE to work from the controlling software.



If the PWM to the LD has been confirmed to work ignore the rest of this. 



........................



If its not working can you tell us exactly how you have the PWM wired  to the LD. I realize its 2.5 but is it via a board output pin or a pin internal to the board.



If you are using the driver example I provided with the external pullup you will need to invert 2.5 in the config. 



If you are using a pin internal to the board (which I do not recommend) then it needs to be configured as pulled up not open drain.


---
**Frank “Helmi” Helmschrott** *March 09, 2017 15:20*

**+Don Kleinschnitz** PWM basically works (measured between the non-drained P2.5 to GND (+3.3V) it works just fine basically but we were not able to fire that in anyway but using G1 combined with a movement as the fire command wasn't there. We still have to get the open-drain variant to work but we can also use a level shifter if this fails. This is definitely not our biggest problem. Not having any command to manually fire the laser is definitely the bigger problem



**+Arthur Wolf** in CNC mode homing seems to be way different. That's probably why we were confused by it yesterday. I tried $H instead of G28 to move to the endstops but this doesn't seem to work so good in corexy. 



** EDIT: Ignore this, my fault - just got it fixed. Accidentally only uncommented the corexy_homing but didn't set it to true. **



It looks like only one axis gets homed. Do you know what might be going wrong here? This time I reduced my modifications to the sample file to a minimum. Basically as the steps/mm were already right I only changed the arm_solution, some endstopf stuff and lenght of axis. Also I enabled the Laser module of course. So I should be quite near to "out of the box".



Here's that config: [pastebin.com - Smoothieboard configuration file, see  ...](http://pastebin.com/raw/8XJdCJ0B)



But worst of all: The fire command still isn't there.



Here's the list of commands



>>>M1000 help

SENDING:M1000 help

Commands:

version

mem [-v]

ls [-s] [folder]

cd folder

pwd

cat file [limit] [-d 10]

rm file

mv file newfile

remount

play file [-v]

progress - shows progress of current play

abort - abort currently playing file

reset - reset smoothie

dfu - enter dfu boot loader

break - break into debugger

config-get [<configuration_source>] <configuration_setting>

config-set [<configuration_source>] <configuration_setting> <value>

get [pos|wcs|state|status|fk|ik]

get temp [bed|hotend]

set_temp bed|hotend 185

net

load [file] - loads a configuration override file from soecified name or config-override

save [file] - saves a configuration override file as specified filename or as config-override

upload filename - saves a stream of text to the named file

calc_thermistor [-s0] T1,R1,T2,R2,T3,R3 - calculate the Steinhart Hart coefficients for a thermistor

thermistors - print out the predefined thermistors

md5sum file - prints md5 sum of the given file

>>>M1000 fire

SENDING:M1000 fire

Command not found: fire



And this is my version:



SENDING:M1000 version

Build version: edge-9399ed7, Build date: Feb  7 2017 22:14:13, MCU: LPC1768, System Clock: 100MHz

  CNC Build 3 axis






---
**Don Kleinschnitz Jr.** *March 09, 2017 15:38*

**+Frank Helmschrott** in regard to your FIRE problem, at this point not sure how (or if you need) help because I am unclear on how you are actually driving the LD. 



I am sure that you realize that if the LD PWM drive is not working neither will the FIRE :).



Note: Based on the information you posted on the LD interface, I am relatively certain that a level shifter is not needed and will not work reliably as a driver.


---
**Frank “Helmi” Helmschrott** *March 09, 2017 15:41*

**+Don Kleinschnitz** well the fire command will be needed anyways - no matter what way we find to get the Laser driven.



I'm not planning to use the Level shifter as a driver. We have a driver and this needs a 5V PWM signal. We already discussed that here with also some information about our Laserdriver there in the thread [plus.google.com - Hey guys, I havent done much. with Smoothieboard so far but I plan to use it...](https://plus.google.com/+FrankHelmschrott/posts/aQmEVfdB4AR) 



That's where **+Arthur Wolf** suggested the open drain alternative to the level shifter. 



EDIT: Sorry, just realized you were in this discussion too but leave it there as a reference nonetheless.


---
**Wolfmanjm** *March 09, 2017 19:03*

the fire command never shows up in help. do not expect it to as it is laser specific. Just try the command. It is there and has been for a long time. The only reason it would report it is not there is if you were using an old firmware version. Just type fire it should return help for the fire command


---
**Wolfmanjm** *March 09, 2017 19:05*

what board are you using? this is not a smoothieboard is it?




---
**Frank “Helmi” Helmschrott** *March 09, 2017 19:05*

welll, that's what I did several times already



>>>M1000 fire 10

SENDING:M1000 fire 10

Command not found: fire


---
**Frank “Helmi” Helmschrott** *March 09, 2017 19:07*

and here's the version: 



SENDING:M1000 version

Build version: edge-9399ed7, Build date: Feb  7 2017 22:14:13, MCU: LPC1768, System Clock: 100MHz

  CNC Build 3 axis




---
**Arthur Wolf** *March 09, 2017 19:09*

**+Frank Helmschrott** Can you please tell me : 

* Exactly what board you are using ?

* What is the result of sending just "fire" via the web interface ?


---
**Frank “Helmi” Helmschrott** *March 09, 2017 19:11*

**+Arthur Wolf** Smoothieboard 1.0 (Kickstarter Version). I don't use Networking or the WebInterface. My Board came without an Ethernet Jack,. I got one but didn't get it working until now but don't have any priority on it so I kept it off for now.



EDIT: oh, and it's the 5-axis-version.


---
**Arthur Wolf** *March 09, 2017 19:12*

We need you to talk to the board using something that doesn't eat commands, can you use cutecom ? 


---
**Wolfmanjm** *March 09, 2017 19:14*

Yea M1000 does not work with the fire command just use a newer version of pronterface and send  @fire SOrry I forgot tha M1000 only handles certain commands.


---
**Frank “Helmi” Helmschrott** *March 09, 2017 19:18*

**+Arthur Wolf** never used it but looks like a serial terminal - should be able to do so.



**+Wolfmanjm**that's actually interesting information. I was always wondering why the @ commands didn't work on Pronterface. Maybe the macOS Version I have is too old.


---
**Frank “Helmi” Helmschrott** *March 09, 2017 19:49*

Ok, I got iTerm2 to connect via USB/Serial to the board and fire works this way. Thanks for pointing this out guys. Looks like pronterface isn't available as a newer version for mac but that doesn't matter too much. I will get calibration done via serial interface.



Does anyone know how Laserweb does it's Laser test thing? I'm a bit unsure about the Units there in the settings. Couldn't get anything on the PWM side when using their test button so far: Have set max S value to 1 like suggested for Smoothieboard and tried the test power setting in 0-1 notation and in 1-100 but no matter what I can not measure any outcome on the pwm port in the 10 seconds (set it to 10000ms) during the laser test.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/3v2zJbWwsoW) &mdash; content and formatting may not be reliable*
