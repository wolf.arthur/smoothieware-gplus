---
layout: post
title: "Hello All, First time using a shiny new 1.1 smoothieboard and i'm having a problem with DC42's mini IR sensor"
date: March 10, 2017 23:06
category: "General discussion"
author: "Robin Evans"
---
Hello All, First time using a shiny new 1.1 smoothieboard and i'm having a problem with DC42's mini IR sensor.

I've connected it with the recommended 470ohm pulldown resistor and when m119 it always set in one state. Its attached to the z- min endstop input and i've checked it all with a multimeter.



Sensor flashes twice, as expected, and picks up fine the light comes on when in proximity to bed or paper etc.



Config for probe:

## Z-probe

# See [http://smoothieware.org/zprobe](http://smoothieware.org/zprobe)

zprobe.enable                                true            # Set to true to enable a zprobe

zprobe.probe_pin                             1.28^          # Pin probe is attached to, if NC remove the !

zprobe.slow_feedrate                         5               # Mm/sec probe feed rate

#zprobe.debounce_count                       100             # Set if noisy

zprobe.fast_feedrate                         100             # Move feedrate mm/sec

zprobe.probe_height                          5               # How much above bed to start probe

#gamma_min_endstop                           nc              # Normally 1.28. Change to nc to prevent conflict,



Anyone got any ideas?





**"Robin Evans"**

---
---
**Robin Evans** *March 11, 2017 12:43*

So turns out on the 1.1 smoothie board you no longer need the 470 resistor, undocumented change I guess!


---
*Imported from [Google+](https://plus.google.com/100179153620147877525/posts/5cZU3dfCxvT) &mdash; content and formatting may not be reliable*
