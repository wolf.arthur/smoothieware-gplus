---
layout: post
title: "Question I purchased a reprap discount glcd"
date: March 10, 2017 01:07
category: "General discussion"
author: "jacob keller"
---
Question



I purchased a reprap discount glcd. But it doesn't turn on.



I connected all the new pins to the smoothieboard as you can see in the picture.



I have a 5v regulator 1 amp on the smoothieboard.



Here's the configuration file for the panel.

[https://www.dropbox.com/s/m07qc40bhb8s3e6/config1?dl=0](https://www.dropbox.com/s/m07qc40bhb8s3e6/config1?dl=0)



Did I miss something.



On the configuration file ignore the file name "config1" I made it that way so I don't get confused with the real configuration file. It's just a copy



PS. I have the red glcd from Amazon

And the red adaptor for the smoothieboard.





**"jacob keller"**

---
---
**jacob keller** *March 10, 2017 01:10*

![missing image](https://lh3.googleusercontent.com/koZA6plSaeJUGMZ8v63kdaupZanGXyWANf5gbPU0256GoPV_fRBt8fn7fjIrV7opmv7hVU1q9nkk9AhYkQwSn3sYAn5M5ydUjEg=s0)


---
**jacob keller** *March 10, 2017 02:36*

![missing image](https://lh3.googleusercontent.com/Wt-4XP-J1ACa3h-bANFZ7GrZbsSkHnmiUUN0PGcojKLoyYE6gEPLZsbwX86los_xmRbV8ZGqi6ZRZRy43n385P964pnJJaOreAg=s0)


---
**jacob keller** *March 10, 2017 03:06*

OK so I took another shot at it. And I got the screen to come on. But it's just a blank blue screen. And now the number four green LED light is not on. Meaning that there's a SD card problem. Is that because there's an external SD card slot on the glcd? Please help


---
**Joe Alexander** *March 10, 2017 08:28*

on mine I had to reverse the connections to the adapter board. Easy way is to nip off the keyed section and insert backwards. It is a common issue for the non-genuine boards and took me a while to find it myself.


---
**jacob keller** *March 10, 2017 13:42*

**+Joe**​ I'll try that. Thanks


---
**jacob keller** *March 11, 2017 00:45*

**+Joe** it works it's much brighter. But there's no display still? Like everyone else has it saying smoothie ready. How do I get it to do this.



Thanks


---
**jacob keller** *March 11, 2017 02:19*

**+Joe** this is what it looks like.

![missing image](https://lh3.googleusercontent.com/UwqgTcU4pp-shNQRGQyGqPjH5oxOJz8yf_3uRW1RX2GqqmBkbYdJ5M6uYc7jj7EL3UowFLNQE37R0i7cfMFKK7bhqgzRkQHE58U=s0)


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/P9EceUyMhrS) &mdash; content and formatting may not be reliable*
