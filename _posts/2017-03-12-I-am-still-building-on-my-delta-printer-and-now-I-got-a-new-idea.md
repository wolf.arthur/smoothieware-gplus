---
layout: post
title: "I am still building on my delta printer, and now I got a new idea"
date: March 12, 2017 19:14
category: "General discussion"
author: "Michael Andresen"
---
I am still building on my delta printer, and now I got a new idea. I want to have a LED ring as light in t he top of it. The light will be four of these [https://www.adafruit.com/products/2874](https://www.adafruit.com/products/2874) to make a ring. The leds can be controlled individually, and I would like to use them to show a few states of the printer.

To control the leds, I am going to just use an atmega running arduino, but I somehow need to tell that what to do.

My plan right now is to use some of the unused pins on the smoothieboard, and control them with some custom codes using just the switch module in smoothieware. But are there a smarter way to do it?



My plan is when I just power on the printer, the leds will be white. When starting to heat, it will start to fade between white and red. When it starts to print, it will then again just return to white. If it enters HALT condition, it will flash rapid red, and if a print finishes it will just fade between white and green.



But, if there is a smarter way to do it, maybe where it is possible to do more, that would be pretty cool too. :)





**"Michael Andresen"**

---
---
**Wolfmanjm** *March 12, 2017 20:49*

There is a led ring module here.... [https://github.com/wolfmanjm/Smoothie/tree/feature/led-ring-module/src/modules/utils/led_ring](https://github.com/wolfmanjm/Smoothie/tree/feature/led-ring-module/src/modules/utils/led_ring)


---
**Michael Andresen** *March 12, 2017 20:57*

But that is only for normal rgb leds? What I am looking at doing, is to control only some of them, which is why I got that type of leds.


---
**Jarek Szczepański** *March 13, 2017 08:15*

if these leds are based on WS2812 and you plan to use Arduino to control them you just need to create some kind of communication protocol between Smoothie and Arduino (using I2C or SPI - I don't know exactly)


---
**Marc Miller** *March 14, 2017 17:22*

**+Michael Andresen**  Depending on how many lighting states you want to display, and if the smoothieboard pins you are going to use have PWM capability, here are a few other ideas.



A super simple solution could use two digital pins that are just set high or low that the Mega directly reads.  That would give you four lighting states.



The next simple solution would be to use a smoothie pin that has PWM capability.  That would provide 256 values for the Mega to receive.  That should give you plenty of lighting states!  :)



However if I was going to use PWM, and for example wanted 8 lighting states, I would not have my PWM output range be 0-7.  Instead I would use 0, 32, 64, etc., and then with the Mega check for values in a range around those points.  So when reading the value maybe check for something in the range of 0-6, 28-38, 58-70, etc.  This creates a safety factor when reading the value so if there happened to be some odd tiny fluctuation in the signal the lighting state wouldn't suddenly jump back and forth between two adjacent states.



Once the mega knows what state you want to display then you can go to town with the Adafruit Neopixel library or FastLED library for wonderful addressable LED goodness.  Make sure you have about 4 Amps of power available for those 60 LEDs if you make them go full bright white. That will be a fun add on to your printer!


---
**Michael Andresen** *March 17, 2017 06:05*

The PWM idea is a pretty good idea! Maybe I could modify the module **+Wolfmanjm** linked to, to just do that.


---
**Matthew Kelch** *March 17, 2017 17:02*

**+Michael Andresen**, I'm also going to be adding a NeoPixel to my printer. I'm using a much smaller array, but the same concept applies.



In my case I'm hoping to directly control the NeoPixel from Smoothieware rather than using a Arduino as an intermediary. If I am not mistaken you should only need to use a single PWM pin (5V, ground, signal).


---
**Marc Miller** *March 17, 2017 20:09*

This link might interest you **+Matthew Kelch**.  I was under the impression that Neopixels (WS2812) required very precise timings, but perhaps not.  I'd be interested to hear more when you get it going.



[wp.josh.com - NeoPixels Revealed: How to (not need to) generate precisely timed signals](https://wp.josh.com/2014/05/13/ws2812-neopixels-are-not-so-finicky-once-you-get-to-know-them/)




---
**Alec Richter** *August 11, 2017 05:20*

Did you have any luck figuring this out? I'm in the same boat right now.


---
**Matthew Kelch** *August 11, 2017 15:59*

Honestly never made it to that point. I'd probably just use an Arduino to make it easy.


---
**Alec Richter** *August 11, 2017 17:29*

Thank you, that's progress. I have a Trinket, how would you connect things? I'm thinking 5V and GND from the ring to the Smoothieboard, the Data Input to one of the Trinket pins, but how should I wire/program it so the Smoothie tells the Trinket what to do?


---
**Marc Miller** *August 12, 2017 08:58*

All grounds would need to be connected, so the ground of the Arduino needs to be connected to the ground of the LEDs and also the Smoothieboard's ground.



Depending on how many pixels you're going to light up you may or may not want to connect it to the Smoothieboard 5V because lighting up a lot of LEDs actually starts drawing a decent amount of Amps.   Make sure the power source can safely provide those Amps!



You can estimate about .06A per pixel when it's lit up full white (that's 20mA per R, G, and B color).  So if you had a 60 pixel setup displaying full bright white:

60 pixels x .06A/pixel = ~3.6 Amps!

If you only run limited colors, and/or limit your brightness to half of maximum, and/or only light up some of the pixel at a time then of course you can use a smaller power supply.



**+Alec Richter** do you have the 5V or 3.3V version of the Trinket?  Either one should be fine being powered by 5V since it has an on board regulator for power, but the 3.3V one will be damaged if you try to read a 5V signal on an input pin only designed for reading 3.3V.



Do the Smoothieboard pins you plan to monitor/read with your Arduino output 5V or 3.3V?




---
**Marc Miller** *August 14, 2017 20:51*

I created this example of how I think I would go about this.  Colors or color patterns could be as simple or crazy as you desire.  Any questions or suggestions for improvements are welcome. (Sorry for weird video quality. Bright pixels are hard to record, looks much better in person.)


{% include youtubePlayer.html id="aNB4iK3_Y_s" %}
[youtube.com - State Indicator example](https://youtu.be/aNB4iK3_Y_s)



Code available here:

[https://gist.github.com/marmilicious/f5ba0027a5188805a9a37b0ca70c6136](https://gist.github.com/marmilicious/f5ba0027a5188805a9a37b0ca70c6136)




---
*Imported from [Google+](https://plus.google.com/+MichaelAndresen/posts/1xVDuFPsxUc) &mdash; content and formatting may not be reliable*
