---
layout: post
title: "Can someone help me with the LCD screen"
date: March 16, 2017 21:11
category: "General discussion"
author: "jacob keller"
---
Can someone help me with the LCD screen.



I got a new screen I had a reprap discount glcd full LCD screen and it didn't work. So now I bought new one I got the XL one reprap discount glcd full LCD.



And when I turn it on it has these white block's. I tried​ switching the cables and now it just rings at a high pitch noise. So I switched the cables back.



Is there any reason why the LCD screen is doing this?



And I did config the panel in the configuration file.



Did I do something wrong.



here my config file. ignore the number 10

[https://www.dropbox.com/s/iqeip1m76c1fb8b/config10?dl=0](https://www.dropbox.com/s/iqeip1m76c1fb8b/config10?dl=0)



![missing image](https://lh3.googleusercontent.com/-nKfhpHBmsZw/WMr_ZOS_G1I/AAAAAAAAArw/KS36ZA-L3Fw7RgMK1b6q3GPYSyFVBPjDACJoC/s0/IMG_20170316_160135568.jpg)
![missing image](https://lh3.googleusercontent.com/-F0JbZHrbYe4/WMr_ZKJumNI/AAAAAAAAArw/soEasZ487esOcPfiMr7-HHx2RmskAZ6DACJoC/s0/IMG_20170316_160125290.jpg)

**"jacob keller"**

---
---
**Arthur Wolf** *March 16, 2017 21:32*

Where did you read that smoothie supports this panel ? I have never been informed that it does.


---
**jacob keller** *March 16, 2017 21:40*

**+Arthur Wolf**​ this screen is still the Reprap discount full graphics lcd. It just has a bigger screen. I tried the smaller reprap discount glcd full graphics lcd. But it wasn't working. I tried everything from the cables to the configuration file.


---
**jacob keller** *March 16, 2017 21:42*

Smoothie supports​ the reprap discount glcd full graphics lcd screen. It says right in the panel page **+Arthur Wolf**​


---
**Arthur Wolf** *March 16, 2017 21:42*

It actually looks like a large version of the "smart" panel, not the "glcd" panel. Smoothie doesn't support the "smart" panel ( well, not without using an arduino nano as an interface, and that's not very much documented ).


---
**jacob keller** *March 16, 2017 21:45*

**+Arthur Wolf**​ will can you help me with the glcd panel that I bought. It works but there's no display there's just a blue screen.


---
**Arthur Wolf** *March 16, 2017 21:58*

**+jacob keller** As I said this isn't a panel that is supported by smoothie ...


---
**Griffin Paquette** *March 16, 2017 22:02*

You could probably use this one using the universal panel adapter found here: [github.com - wolfmanjm/universal-panel-adapter](https://github.com/wolfmanjm/universal-panel-adapter) . Can't say for sure though.



Smoothieware supports the GLCD with its SPI interface, but that doesn't mean that yours is. It's entirely possible that the screen is actually an I2C display. I would read up on your lcd and see if the universal panel adapter is a viable option for you.


---
**jacob keller** *March 16, 2017 22:03*

**+Arthur Wolf**​ I wasn't talking about the XL smart controller. I'm talking about the the Reprap discount full graphics smart controller. The one that is listed on the smoothie website.


---
**jacob keller** *March 16, 2017 22:39*

**+Griffin Paquette** I have a glcd but it just displays​ a blue screen. Tried using the little light brighter knob to adjust the light and still nothing. 



Everything that I do with this board has been nothing but headaches. I'm about to give up on the smoothieboard. And switch to ramps.


---
**Arthur Wolf** *March 16, 2017 22:39*

**+jacob keller** Ah, yes then it should work. Is your panel an original or a clone. Is the adapter an adapter for original panels or for clone panels ?


---
**Arthur Wolf** *March 16, 2017 22:40*

**+jacob keller** Smoothieboard, and the RRD GLCD, work perfectly fine for thousands of users. You probably just have a weird little problem with connections, or compatibility of clones or adapters. No reason at all to give up ...


---
**jacob keller** *March 16, 2017 22:45*

**+Arthur Wolf**​ I have a clone of the glcd and not sure about the adapter. I didn't now there was a clone of the adapter. I have the red one V1.



I'm trying not to give up on the smoothieboard. It's just I spend money on things and I can get them to work right.


---
**Arthur Wolf** *March 16, 2017 22:52*

**+jacob keller** Original RRD GLCDs and clone RRD GLCDs have inverted ( turned 180 degrees ) connectors compared to each other.

This isn't something we can do anything about, it's not a problem with smoothie but with the clones panels.



So there are two versions of the adapter : one for the original, and one for the clones. You need to know which one you have. If you don't know, it probably means you bought the adapter for the original instead of the adapter for the clones.



Again, this isn't a problem with Smoothie at all, just a problem with the things you bought ...



You really need to be more patient, Smoothie is used by thousands of very happy users, it's considered to be the easiest to use system around. There is absolutely no reason why it wouldn't work for you.



One thing you can try is this : use a tool to remove the plastic black box on the connectors, rotate them 180 degrees, and replace them around the pins. Then try using the panel again.






---
**jacob keller** *March 16, 2017 23:12*

**+Arthur Wolf** it didn't work. 



I'm just going to buy the original adaptor.



is this the original?

 [robotseed.com - GLCD Panel Premium](http://robotseed.com/index.php?id_product=23&controller=product&id_lang=2#/gcld_acc-pcb_shield_adapter_soldered/glcd_panel-shield_pcb)



thanks for the help 



jake


---
**Arthur Wolf** *March 16, 2017 23:26*

If you have a clone panel, you want an adapter <b>for clones</b>, not an adapter <b>for originals</b>.

Go here [robotseed.com - Shield GLCD panel](http://robotseed.com/index.php?id_product=97&controller=product&id_lang=2#/gcld_acc-pcb_shield_adapter_soldered/glcd_adapter_sdcard_reader_extension-sd_reader_on_glcd/glcd_adapter_vr-no_voltage_reg/glcd_genuine_option-glcd_clone) and choose <b>GLCD Clone</b>


---
**jacob keller** *March 16, 2017 23:33*

**+Arthur Wolf** thanks for the help


---
**Arthur Wolf** *March 16, 2017 23:37*

Reverting the connector's black plastic box is the same as buying the other adapter, though. It should work exactly the same.


---
**Guy Bailey** *March 17, 2017 03:12*

I have a clone glcd and before I rotated the connectors it did the same thing. Powered on but only displayed squares, just like your picture. I didn't remove the connectors though. I simply cut a slot to allow the cables to be rotated. That way there was no risk of me damaging the wires. Have you checked the wires in the ribbon for continuity?


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/EG87o8cARez) &mdash; content and formatting may not be reliable*
