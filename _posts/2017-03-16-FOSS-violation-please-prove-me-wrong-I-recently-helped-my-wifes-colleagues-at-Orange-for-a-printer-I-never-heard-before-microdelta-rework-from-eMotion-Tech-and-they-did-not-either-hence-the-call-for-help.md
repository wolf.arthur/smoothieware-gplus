---
layout: post
title: "FOSS violation (please prove me wrong!)? I recently helped my wife's colleagues at Orange for a printer I never heard before (microdelta rework from eMotion Tech) -- and they did not either, hence the call for help &"
date: March 16, 2017 07:40
category: "General discussion"
author: "Jeremie Francois"
---
FOSS violation (please prove me wrong!)? I recently helped my wife's colleagues at Orange for a printer I never heard before (microdelta rework from eMotion Tech) -- and they did not either, hence the call for help & calibration (actually there was a bad connector and loose joint screws, had no time to check the hexagon extruder). It is pretty sturdy 500€ Delta printer. I was quite glad to see it run Smoothieware + Repetier. But ....



They brag about their 100% French board & design but I saw no credit nor proper attribution to any pre-existing hardware nor software (**+Arthur Wolf** you will like it...). They say it is open source but I only could find two PDF files for the design, a prebuilt binary firmware, and they even stripped any mention off Smoothieware config.txt". Not cool.



I managed to identify the board as an "eMotronic board": [https://www.reprap-3d-printer.com/product/1234568620-emotronic-board](https://www.reprap-3d-printer.com/product/1234568620-emotronic-board)





**"Jeremie Francois"**

---
---
**Johan Jakobsson** *March 16, 2017 08:13*

I'm sure (i hope) someone will come along and correct me if i'm wrong.

They did include the the drawings, as you mention. That is, afaik, the source - and it's open since you can download them. 

There are a few links to [smoothieware.org](http://smoothieware.org) in the config.txt

On the description page it says "Firmware : Smoothie-compatible" and just below that there's even a link to [smoothieware.org](http://smoothieware.org). 

So I'd say they are very open about it. 


---
**Arthur Wolf** *March 16, 2017 08:35*

**+Johan Jakobsson** « Can read the source » and « Open Source » are two very different things. Open-Source requires, for example, that you be able to modify the files ( and be legally allowed to by the license ), that you be legally allowed to sell the thing, that you be allowed to share the design and your modifications, etc ...

See for example : [oshwa.org - Definition (English)](http://www.oshwa.org/definition/)



Open-Source is all about sharing and building communities, it's not about "documenting the thing by showing uneditable schematics".



**+Jeremie Francois** EmotionTech contacted me about this board when they released it, asking me if I wanted to test it. I refused to have anything to do with them because of problems I already had with them, in particular the fact they mislead newcomers to the french Reprap community with their "Reprap France" website and adword ads, which lead many newcomers to think they are in some way an official organ of the Reprap project.

Furthermore, they have violated licenses before, and the first printer they ever released themselves ( a derivative of the Open-Source mini kossel ), they made closed source, thus showing clearly they aren't an Open-Source or community friendly community.

They also sold extremely poor quality printers in their first year, which was damaging to the Reprap project.


---
**Thomas “Balu” Walter** *March 16, 2017 08:44*

So in a way it's good they don't mention smoothie anywhere? ;)


---
**Arthur Wolf** *March 16, 2017 08:45*

**+Thomas Walter** Nope they should still give attribution ( I don't want anything to do with them, but I'm not the Smoothie project ). They did nearly nothing and wouldn't be selling this product without the hundreds of volunteers from the project.


---
**Jeremie Francois** *March 16, 2017 08:49*

**+Johan Jakobsson** you are right about the "mention" that it is  "Smoothie-compatible" on the webpage that describe the board, but this is no http link nor a proper attribution (they have to tell it just to help selling the board I guess).

And there seems to be just no reference at all on the main page for the printer itself at [https://www.reprap-france.com/produit/1234568619-imprimante-3d-microdelta-rework](https://www.reprap-france.com/produit/1234568619-imprimante-3d-microdelta-rework)



Finally, the only links to smoothieware that can be found in the files to download at [https://data.emotion-tech.com/ftp/Datasheets_et_sources/Sources_electronique/eMotronic_brd_1.0.4.zip](https://data.emotion-tech.com/ftp/Datasheets_et_sources/Sources_electronique/eMotronic_brd_1.0.4.zip) are to explain some of the options, and not, again to give proper attribution (i.e. they only benefit from the huge work on documentation by the project). They do not let the user rebuild the firmware either, so if ever they did a change in the code source, the user is bound to this company and the community does not benefit from them.



Making money off Open-Source project is possible, and others do so. What really sucks is when people do it without telling so. Not only they already benefit from the work of many, but they even would steal the credits? Both unfair, misleading and illegal.


---
**Thomas “Balu” Walter** *March 16, 2017 09:27*

**+Arthur Wolf** I was just joking about their hardware being so bad that mentioning the project might hurt it like they did with Reprap. Of course they should.


---
**Lars Thalheim** *March 21, 2017 22:50*

**+Arthur Wolf** "Open-Source is all about sharing [...], it's not about "documenting the thing by showing uneditable schematics"."



What does "editable" mean. Would a schematics be "documentation" only, just because the schematics is published in a format, not everybody has access to? For example: if I develop a schematics with KiCAD and publish the file... would they be open source? What if I used Altium instead... would it be closed source, because not everybody has access to Altium?



Open-source is certainly more, than plain publishing documentation. But I'm not convinced, that publishing a "paperwork only" design, is not eligible to be attributed "open-source".


---
**Arthur Wolf** *March 21, 2017 23:07*

You guys know I didn't invent Open-Source right ? I'm not an authority or anything, you can google this stuff :)



**+Lars Thalheim** Difficult to edit still is legally open-source, but it's not in the spirit of the thing. It's pretty much cheating, which is pretty common : doing just enough to legally be able to call yourself open-source, but not doing enough that the community as a whole profits from your work very much.



To go into a bit more detail, the definition of open-source hardware says you should publish the files in the format "preferred" for edition. It means if you can export your work in multiple formats, you should export it in ( at least, but not limiting yourself to ) the format that is easiest for others to edit.



I quote : 

« The documentation must include design files in the preferred format for making changes, for example the native file format of a CAD program. Deliberately obfuscated design files are not allowed. Intermediate forms analogous to compiled computer code — such as printer-ready copper artwork from a CAD program — are not allowed as substitutes. The license may require that the design files are provided in fully-documented, open format(s). »



To answer your question : «  if I develop a schematics with KiCAD and publish the file... would they be open source? » 

Yes, if you also release it under an Open-Source license, of course.



«  What if I used Altium instead... would it be closed source, because not everybody has access to Altium? »

It'd still be open-source, if you choose an open-source license that allows for this sort of bad behavior, which not all do, but it's extremely unlikely to be re-used by the community and is likely to be frowned upon.

It's essentially bad open-source, it's not in the spirit.

It's still open-source, the same way donating shit-smeared pennies is still donating ...






---
**Jeremie Francois** *March 22, 2017 07:40*

**+Arthur Wolf** and **+Lars Thalheim** may I add something?



It boils down to the same issue as with closed-source project: you would have to revert engineer the read-only data you are only allowed to see in order to improve or to re-use the project.



IMHO, I could use software X to engineer and design something, but I cannot only release an "intermediate/compiled" version of it. Technically, using the most expensive closed-source X software may still be OK even though it is not "nice": you could argue that X is better, or that you know X better, which is fine with me (even though I obviously would favor companies that use Y FOSS software for their design, which is accessible to everyone by definition).



But Open source is incompatible with the idea of not providing the data that make embedding/reusing/modifing the work. Hiding between intermediate/compiled format is explicitly mentioned in the licence.



What options are left with them? We would have to read thoroughly and re-write the project, back into a <b>design format</b> (and not a byproduct). This is highlighted by the fact that it would still be the case even though we would use the same software X (what a ridiculous waste of time)! Additionally here, they do not even provide the intermediate Gerber files, while routing is probably the major part of the work  for a smoothieware compatible board.



All in all: people and clients barely cannot improve their design without some counterproductive reverse engineering: not open source, period.



Moreover, they do not give credit as required by the licence of the open source project they based their own work on (i.e. Smoothieware). They deliver binaries only and they even have removed the licence and reference/links to the original open source project. This is undoubtedly illegal, and it "proves" the point.



(eventually, I should write a more structured post by collecting all the pieces -- with attribution. This way I could point to it each time!)


---
**Lars Thalheim** *March 22, 2017 08:27*

I absolutely agree with your point (Arthur and Jeremie), that the questioned product violates the license terms of smoothieboard.



I also acknowlegde, that if there is only "paperwork" available one would have to bring this format in some editable form by means of "reengineering", which is not in the spirit of open-SOURCE (!). Full ack on that too.



Nevertheless there is a difference of reengineering something without the licensed permission to modify, alter, edit, change and reengineering something because your take your granted right (by license) into account.



Providing something in an "open format" is not defined. It is more a "desireable" state; a "nice habbit". Let's stick with the example of an electronic component.



What would be an acceptable "open format" to store a schematics and the accompanying PCB layout. Is it KiCAD, because the original creator used KiCAD initially and it is widely available? Is it Eagle, because it is widely available to? It is Mentor, because the used features in the schematic require it to be saved as native Mentor file? Or wouldn't a printed PDF file not also provide all required information to edit, modify and derive designs.



Just because it is less convinient to first recapture a schematic from a printed file, then edit it and save it in a different format doesn't  violate the "open-sourcedness" of a thing (in my understanding). It is open source, as long as all necessary informations are available and permission is granted to create a derived work from it.



However. I'm not an expert on that. Thank you, for taking this into discussion.


---
**Arthur Wolf** *March 22, 2017 09:09*

**+Lars Thalheim** It's very simple : 

You must provide the easiest to modify format you can provide.

Kicad is preferable to Eagle, and Eagle is preferable to Altium, etc ...

Some licenses allow using expensive proprietary software for the sources, some do not. I'd have to check but I'm nearly certain smoothie's license doesn't allow such shenanigans.



Providing a printed file, with the only option for users being to recapture/re-draw it, isn't open-source, because there was an easier-to-modify option : providing the actual source file you worked on.


---
*Imported from [Google+](https://plus.google.com/+JeremieFrancois/posts/U7ySbjXqpVv) &mdash; content and formatting may not be reliable*
