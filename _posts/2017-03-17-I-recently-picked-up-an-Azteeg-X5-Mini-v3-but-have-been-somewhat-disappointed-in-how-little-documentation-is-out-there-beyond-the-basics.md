---
layout: post
title: "I recently picked up an Azteeg X5 Mini v3, but have been somewhat disappointed in how little documentation is out there beyond the basics"
date: March 17, 2017 14:27
category: "General discussion"
author: "Matthew Kelch"
---
I recently picked up an Azteeg X5 Mini v3, but have been somewhat disappointed in how little documentation is out there beyond the basics. To start with, where can I find the capabilities of each of the expansion pins? Is there some sort of scheme to how the pins are named (IE: 0.15, 2.4, etc...)?

![missing image](https://lh3.googleusercontent.com/-JEs3SOz-UVk/WMvyPiGxUQI/AAAAAAAAhsU/vFHcmB3RcIoy2-ZS4ZRfXe4JDx5cEVKTgCJoC/s0/Capture.JPG)



**"Matthew Kelch"**

---
---
**Zane Baird** *March 17, 2017 14:38*

[smoothieware.org - pinout [Smoothieware]](http://smoothieware.org/pinout)



While the location of the pins is different, the capabilites and numbering should be the same as the smoothieboard in the link.


---
**Matthew Kelch** *March 17, 2017 15:08*

**+Zane Baird**, I've looked at this previously but it doesn't seem to match up with the pinout on the X5 Mini.



As an example, 1.25 and 0.26 are both PWM fan/led outputs on the X5 Mini V3, but these are listed as 'alpha_max_endstop' and 'thermistor3'. Even though the uses don't map 1:1 do the underlying capabilities remain the same?


---
**Arthur Wolf** *March 17, 2017 15:17*

Also [smoothieware.org - lpc1769-pin-usage [Smoothieware]](http://smoothieware.org/lpc1769-pin-usage)


---
**Zane Baird** *March 17, 2017 15:44*

**+Matthew Kelch** The numbers are assigned functionality based on the config file. The numbers themselves correspond to pins on the microcontroller chip that is the heart of the controller. For instance, you could swap X and Y endstops by only changing the firmware pin assignments in the config file. 


---
**Matthew Kelch** *March 17, 2017 15:50*

**+Zane Baird**, got it! Thanks for the confirmation.


---
*Imported from [Google+](https://plus.google.com/+MatthewKelch/posts/Zxy8sLpS4hN) &mdash; content and formatting may not be reliable*
