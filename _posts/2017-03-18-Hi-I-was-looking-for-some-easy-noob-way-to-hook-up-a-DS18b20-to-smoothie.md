---
layout: post
title: "Hi, I was looking for some easy noob way to hook up a DS18b20 to smoothie"
date: March 18, 2017 08:28
category: "Development"
author: "Jorge Robles"
---
Hi,



I was looking for some easy noob way to hook up a DS18b20 to smoothie. I've read I2C is not an option, so, where can I find info about hooking up thru nano or Attiny as SPI? Where can I find a guide of using SPI with Smoothie?



Thanks.





**"Jorge Robles"**

---
---
**Don Kleinschnitz Jr.** *March 18, 2017 12:27*

I have considered doing the same and backed off because:



1. Potential to need firmware changes to get display and alarms like I wanted them.

2. The potential for controller hangs to miss water problems. I don't want anything but hardware in my coolant control or my laser enable path.



So I wimped out and just got a temp monitor from amazon. It monitors and alarms within temp ranges and you could control a temp device like heater in the winter. It runs on 12vdc and has a nice display and bezel. 



[amazon.com - Inkbird Dual Stage 12V Digital Temperature Controller Fahrenheit Thermostat: Amazon.com: Industrial & Scientific](https://www.amazon.com/dp/B019I3YCFS?psc=1)


---
**Jorge Robles** *March 18, 2017 12:52*

I've got one similar but uses 10k thermistor instead ds. Now I'm improving an attiny with ds18b20, (offtopic: or maybe a nano to track laser hours too). But i feeled the smoothie board underused and miss some documentation on smoothieware site about spi interfacing


---
**Don Kleinschnitz Jr.** *March 18, 2017 12:58*

**+Jorge Robles** Yes, I agree it seems wasteful for that board not to do such things, like temp, interlocks, hrs etc. But I decided not to get into the smoothie FW business.

I have many time thought about an arduino-based control panel that does all the external accessories as an add in. I know there are a few out there already .....

 I decided to spend my time on other K40 problems :) 


---
**Jorge Robles** *March 18, 2017 13:04*

Cool :)


---
**Don Kleinschnitz Jr.** *March 18, 2017 13:08*

**+Peter van der Walt** ya, that kind of thing..... its not about what cool thing can you do its were do you want to spend your "cool maker time" :).




---
**Arthur Wolf** *March 19, 2017 12:11*

This would require adding some new code to Smoothie, however not too much.

Smoothie already abstracts away temperature sensors, so you would need to add a new temperature sensor, and teach that new class to read the sensor.

This isn't too much work, depending on your exact skillset of course.



I'm pretty sure in the very wide family of thermistor around, there are some with enough accuracy in the 20-40°C range, and that would be usable without modifying Smoothie's code, so I think that would be a preferable alternative, but if you really want to use the 1-wire sensor, there is nothing preventing you of doing so, you just need to do the code.


---
**Jorge Robles** *March 19, 2017 12:33*

Ok thanks **+Arthur Wolf**​, I was thinking a generic way to hook the nano emulating the thermocouple avoiding touch smoothie code.


---
**Arthur Wolf** *March 19, 2017 12:40*

**+Jorge Robles** That would work too.


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/Va3sdvXmnea) &mdash; content and formatting may not be reliable*
