---
layout: post
title: "Dear Smoothie. Jim needs your help, I quote : <wolfmanjm> I'd like to encourage people to test this branch..."
date: March 20, 2017 21:27
category: "General discussion"
author: "Arthur Wolf"
---
Dear Smoothie.



Jim needs your help, I quote : 



<wolfmanjm> I'd like to encourage people to test this branch...   [https://github.com/Smoothieware/Smoothieware/blob/stepticker/use-64bit-fixed-point/FirmwareBin/firmware-hires.bin](https://github.com/Smoothieware/Smoothieware/blob/stepticker/use-64bit-fixed-point/FirmwareBin/firmware-hires.bin) or for cnc users [https://github.com/Smoothieware/Smoothieware/blob/stepticker/use-64bit-fixed-point/FirmwareBin/firmware-cnc-hires.bin](https://github.com/Smoothieware/Smoothieware/blob/stepticker/use-64bit-fixed-point/FirmwareBin/firmware-cnc-hires.bin)



It's a new branch that moves to 64bit for some counters, after a few people with odd setups ran into weird issues. We need to know if this branch works fine for you or if you run into issues with it. If you could use it instead of edge for a while, and report problems, we'd very much appreciate it.





**"Arthur Wolf"**

---
---
**Anthony Bolgar** *March 20, 2017 22:32*

More than happy to test this for Jim. Will run for a few days and get back to you.


---
**Douglas Pearless** *March 20, 2017 23:41*

I am in the process of rebuilding one of my printers and can probably try this bin file later in the week.  :-)


---
**Douglas Pearless** *April 14, 2017 09:18*

Make that several weeks as I am testing some other code and work :-)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/Eon8S16ZW22) &mdash; content and formatting may not be reliable*
