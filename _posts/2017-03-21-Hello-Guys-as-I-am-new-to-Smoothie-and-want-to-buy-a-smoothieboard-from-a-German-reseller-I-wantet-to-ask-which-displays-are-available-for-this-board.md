---
layout: post
title: "Hello Guys, as I am new to Smoothie and want to buy a smoothieboard from a German reseller I wantet to ask which displays are available for this board?"
date: March 21, 2017 18:07
category: "Help"
author: "Gabriel Doblinger"
---
Hello Guys,

as I am new to Smoothie and want to buy a smoothieboard from a German reseller I wantet to ask which displays are available for this board? Is there any 7" touch screen usable with smoothie and Laserweb?



Thank you very much for your help.

Gabriel





**"Gabriel Doblinger"**

---
---
**Gabriel Doblinger** *March 21, 2017 19:03*

Yes I know I need a PC. But for homing and zeroing etc.. And with laser web there is no support for rendered gcode files for the SD card? Thanks in advance


---
**Maxime Favre** *March 21, 2017 19:35*

Honestly LW does all that ;)


---
**Gabriel Doblinger** *March 21, 2017 19:37*

OK, thanks guys! I'll try without LCD


---
**Chris Brent** *March 21, 2017 21:49*

**+Alex Skoruppa** I helped a little with the Panel Due support. It kind of works, but we need some updates to the Panel Due firmware for full support. It's also tempting to get a 10" tablet for the same price and run octoprint.

EDIT: Don't run Laserweb on a tablet, Peter says it won't work ;)




---
*Imported from [Google+](https://plus.google.com/+GabrielDoblinger/posts/azsbVi72NZ6) &mdash; content and formatting may not be reliable*
