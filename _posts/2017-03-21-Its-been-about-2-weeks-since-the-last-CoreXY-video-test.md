---
layout: post
title: "It's been about 2 weeks since the last CoreXY video test"
date: March 21, 2017 18:16
category: "Help"
author: "Jim Fong"
---
It's been about 2 weeks since the last CoreXY video test. The "Great Lightening Strike of 2017" killed the laptop, Arduino, Applied Motion STM17 stepper motors, power supply and just about all the electronics in the house.  Starting over with a old laptop, new stepper motors and drivers.  



Grbl1.1f

45,000mm/min (750mm/sec) speed

10,000mm/sec^2 acceleration 

Automation Direct nema17 76oz-in stepper motors

10 microstep

Parker E-DC stepper drivers

36volt power supply



This is the practical speed limit for grbl using 10microstep setting.  I just received my 32bit re-arm board so i need to install it and learn how to configure it.  Does anyone have a smoothie corexy re-arm config.txt they can share?????




{% include youtubePlayer.html id="AC-p3n0o_IQ" %}
[https://youtu.be/AC-p3n0o_IQ](https://youtu.be/AC-p3n0o_IQ)





I mounted a steel 123 block using a clamp to the linear bearing.  The 123 block and clamp weighs 635grams, much more than a e3d hot end that I will be using.  The axis is moving at 750mm/sec and repeats several times before returning back to zero on the brown&sharpe .001" dial indicator.



The stepper motors have no problem moving the heavy 123 block at high acceleration and speed so it should perform well enough with the lighter weight hot end.  




{% include youtubePlayer.html id="12HwXar-8Yw" %}
[https://youtu.be/12HwXar-8Yw](https://youtu.be/12HwXar-8Yw)















**"Jim Fong"**

---
---
**Sébastien Plante** *March 22, 2017 00:46*

going to build a CNC?


---
**Jim Fong** *March 22, 2017 00:55*

**+Sébastien Plante** large format 3d printer.  


---
**Sébastien Plante** *March 22, 2017 11:17*

**+Jim Fong** that's a lot of speed for a 3D Printer, can't wait to see the results! 


---
**Jeremiah Coley** *March 30, 2017 14:54*

**+Jim Fong** after watching the video, I am wondering about your belt paths, the belt moves in and out at the x pulley... Not a criticism, but I am curious dose it have accurate movement? The reason I'm asking is I have a diy corexy and have had a hard time finding what could be called "the right belt and pulley setup", I have seen many other builds and it seams that pulleys and belt paths are all over the place...  Any I'm out is appreciated. Thnx


---
**Jim Fong** *March 30, 2017 15:50*

**+Jeremiah Coley**(null) when I milled the mounting plates, I measured the position of the bearing wrong.  They are off by about 8mm.  I need to remove the plates and re-drill new holes.  It's a easy fix but I haven't done it yet.  This will straighten out the belt path.  


---
**Jeremiah Coley** *March 30, 2017 18:06*

+Jim Fong. I was wondering as I have seen belts run like this on other printers, but from what I gather(and that is limited) is the path must be kept at a 90° angles to the pulleys. I like the design you have and would like to see more as you build it.


---
**Jim Fong** *March 30, 2017 20:07*

**+Jeremiah Coley**(null) I'm waiting to get replacement computers and servo drives for my CNC machines. They all died in a lightning strike/power surge.  Once they are running again, work on the corexy printer can resume. I'll post more when that happens.  


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/1wtm5WyKXRy) &mdash; content and formatting may not be reliable*
