---
layout: post
title: "The latest update to the Path ( CNC ) Workbench in FreeCAD ( which is awesome btw ) includes a Smoothieboard post-processor !"
date: March 21, 2017 21:44
category: "General discussion"
author: "Arthur Wolf"
---
The latest update to the Path ( CNC ) Workbench in FreeCAD ( which is awesome btw ) includes a Smoothieboard post-processor !



I pushed for this so I'm very happy it happened :p




{% include youtubePlayer.html id="HqI1Mnh6NIc" %}
[https://www.youtube.com/watch?v=HqI1Mnh6NIc&feature=youtu.be](https://www.youtube.com/watch?v=HqI1Mnh6NIc&feature=youtu.be) ( end of video )





**"Arthur Wolf"**

---
---
**Anthony Bolgar** *March 23, 2017 12:01*

Any word on Chilipepper?


---
**Arthur Wolf** *March 23, 2017 12:05*

Nope unfortunately


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/6q8hAU7qM45) &mdash; content and formatting may not be reliable*
