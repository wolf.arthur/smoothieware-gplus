---
layout: post
title: "Hi all. Can some one help me with problem to start heating/pid values?"
date: March 22, 2017 10:52
category: "Help"
author: "LassiVVV"
---
Hi all. Can some one help me with problem to start heating/pid values?



I do normally auto pid tuning. Everything goes ok, i save them with M500 command.



After that i start normal printing (slic3r gcode file (240C hotend temp)) hotend start rising but leave 1-2degrees under setpoint and "never" reach that setpoint. I wait many minutes but dont reach that setpoint.



After that i shutdown and put manually on octoprint 240C to hotend and everything work perfectly. 



Can anyone tell me what is problem. Under you see screenshot same situation what i explain here.

![missing image](https://lh3.googleusercontent.com/-Jd9O4zoYjdc/WNJXZg_SWLI/AAAAAAAAHMM/DOrgwnqOFAQinFvt3VL69Eh0PZ83QWMxACJoC/s0/tallanne%252Bjuttu.png)



**"LassiVVV"**

---
---
**Arthur Wolf** *March 22, 2017 11:03*

This sounds like you need to tune your PID settings manually a little bit. there are hundreds of guides on doing that on the internet


---
**LassiVVV** *March 22, 2017 11:15*

Try to allready do that. But i dont understand why pid settings works perfectly if i manualle put 240C setpoint. And when gcode file give that same 240C setpoint printer newer reach that temp.



Not can be pid problem i think, because then that manually setpoint not reach that setpoint eather




---
**Triffid Hunter** *March 22, 2017 11:22*

your gcode starts from a lower temperature than your "working" runs. Increase P by 10% and see what happens. Hypothetically, the I term should make it get there eventually but with such a low delta it WILL take ages. The PID autotuner seems to really like choosing numbers that cause this to happen, changing them for a little overshoot is vastly better than the "perfect" asymptote that autotune goes for.



Also try latest edge, I believe some hysteresis stuff was added at some point. If not, it should be added - something like if temp is within target +/- 3°C for 10 consecutive seconds, call it good and continue


---
**LassiVVV** *March 22, 2017 11:49*

Hi Triffid, thanks for tips.



not understand why gcode get wrong temps. because printer shows right temparatures on printer screen (if i put that setpoint manually or gcode put that, same 240C seems on printer screen setpoint, but gcode situation that not be reach).



I try to but 10% and more P. I but very much more P, but not help. Control start oscillate what is normal when put too much P. but after all that i see that setpoint newer reach and pid control not even try to reach that, that only oscillate both sides that 238C. Seems that somewhere printer think that setpoint need to 238C and not 240C (printer own screen tells that right 240C setpoint, but newer reach that.).



That last point is very good, nowadays this firmwares look too exact temp before they start printing that +/- 3°C or 2 is okay.






---
**Jeff DeMaagd** *March 22, 2017 12:45*

Autotune doesn't use PID. G code does. Your PID tune is still off, that's all there is to it. Increase I, not P.



[reprap.org - PID Tuning - RepRapWiki](http://reprap.org/wiki/PID_Tuning)



"For manual adjustments:

if it overshoots a lot and oscillates, either the integral gain needs to be increased or all gains should be reduced

Too much overshoot? Increase D, decrease P.

Response too damped? Increase P.

Ramps up quickly to a value below target temperature (0-160 fast) and then slows down as it approaches target (160-170 slow, 170-180 really slow, etc) temperature? Try increasing the I constant."


---
**LassiVVV** *March 22, 2017 12:55*

I know autotune not use PID, but manually set setpoint use. And that work perfect, but gcode setpoint not.



I can try to increase I, but that seems to nothing pid tunes helps, because control try to keep up that 238 not 240C what is asked.



And when you put manually 240C that goes there just right and keep there. But when gcode but that same temp that always leave that 238C (2degrees under setpoint).






---
**Jeff DeMaagd** *March 22, 2017 13:03*

Have you tried to increase I before? I suggest a 50% increase and see how it goes.


---
**Jeff DeMaagd** *March 22, 2017 13:06*

Do you have your part cooling fan on in the sliced g code?


---
**LassiVVV** *March 22, 2017 13:16*

Allready try to increase I before (2.1 something is auto pid I) i try 8 and 10 values on I. Not helps stop same way 2degrees before setpoint.



Part cooling fan is same rpm always when power supply is on.


---
**Jeff DeMaagd** *March 22, 2017 14:25*

Do you have a config override file on your SD card? Any parameters in configuration override file will clobber your config file settings.


---
**LassiVVV** *March 22, 2017 14:31*

yeah i try different pid settings just with override file. Just use M500 command to write new pid to override file.




---
**LassiVVV** *July 10, 2017 18:07*

Same problem is still standing. Again try to heat my printer and have same problems with smoothie. Everytime when i use gcode printer leave 2degrees under setpoint. If i put octoprint manually 250degrees that works perfectly. Like that works perfectly if i put manually my printer display 250degrees. but when gcode goes, not matter what gcode i put that always leave 2degrees below setpoint and go HALT if i wait longer time.


---
**LassiVVV** *July 10, 2017 18:57*

Here we see that same problem again. Now i drive autotune again.



First period is normal printing what have 255degrees printing temp like see in diagram. Temp not go on setpoint ever that goes direct line if wait more and more.



Second period is right after ALL SAME SETTINGS. I put manually on octoprint user panel 255degrees to setpoint and again magic happens. Temps goes setpoint just fine.

![missing image](https://lh3.googleusercontent.com/EQcn5AcoMQ7yq1GZtyDuDe_secni9Vpe6l4wK-w6zq5ZCS46UW0juosSLA_aqV7JMuxvWjq3iW4-4EuYq29uB968q_zcclkRcG8=s0)


---
*Imported from [Google+](https://plus.google.com/112209274773346118200/posts/cS1GK2VXkUS) &mdash; content and formatting may not be reliable*
