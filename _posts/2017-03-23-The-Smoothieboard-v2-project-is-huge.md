---
layout: post
title: "The Smoothieboard v2 project is huge ."
date: March 23, 2017 10:28
category: "General discussion"
author: "Arthur Wolf"
---
The Smoothieboard v2 project is <b>huge</b>. Pretty much everything we are doing hasn't been done before : Building on top of a RTOS, having an actual bus to add extra extruders, a complete ecosystem of plug-and-play extension boards, MTP file access, a board version that is inexpensive but not china-crappy, and a ton of features nobody has seen before.



This is taking a lot of time. Recently we have sped things up a bit by paying professional devs to work on some things. We already put nearly $15k into this, and we are running out of money.



We are thinking about doing a kickstarter to help. Not a kickstarter for the Smoothieboard v2 ( that would come later ), but a kickstarter where you would support the project, and get a nice T-Shirt in return.

We'd then use the money to speed up the v2 development.



What do you think ? Would that work ? Would you get a T-shirt ?



Cheers !!



[please reshare :p]





**"Arthur Wolf"**

---
---
**Tiago Vale** *March 23, 2017 10:30*

I want mine!!!




---
**Phil Aldrich** *March 23, 2017 10:39*

I'm in


---
**Jack Colletta** *March 23, 2017 10:44*

I can't wait 


---
**Maxime Favre** *March 23, 2017 10:47*

I need a smoothie t-shirt ;)


---
**Roger Kolasinski** *March 23, 2017 10:50*

Honestly, Kickstarter or Indigogo, etc. are really product focused. If yours was for v2, then I would say yes.  For what you are proposing - asking for your loyal group of followers for donations - setting up a Patreon account or just a PayPal account to accept donations might me a better route. 



I don't know the fees Patreon charges, but PayPal is less than 3%.  Kickstarter is something like 10%  



Save kickstarter for the actual v2, and you have a winner, IMO.  


---
**Arthur Wolf** *March 23, 2017 10:53*

**+Roger Kolasinski** We already have a Paypal, it's on the homepage ( and I've made calls on social media for donations ) and nobody donates ( Patreon is something we are considering but I'm skeptical how well it'd work ).

Here people would have a chance to get a T-shirt. Don't underestimate the power of the T-shirt :)


---
**Griffin Paquette** *March 23, 2017 10:58*

It's not necessarily what KS is designed for but I would be down for it!


---
**Don Kleinschnitz Jr.** *March 23, 2017 11:34*

Sure ....... how much money are you trying to raise?


---
**Anthony Bolgar** *March 23, 2017 11:53*

I would love a Tshirt..start the campaign and I will donate :)


---
**Arthur Wolf** *March 23, 2017 11:56*

**+Don Kleinschnitz** We need at least 10k, but more would speed things up.


---
**Anthony Bolgar** *March 23, 2017 11:58*

Check your paypal, donation sent ;) See, some people use it......


---
**Arthur Wolf** *March 23, 2017 12:05*

**+Arthur Wolf** Jim, the lead dev, handles the paypal account, so he knows how much you sent. Thanks a ton however you sent !!! It definitely helps.


---
**Anthony Bolgar** *March 23, 2017 12:13*

A worthwhile cause in my opinion. And I can't wait to start testing V2 when it is available.


---
**Matt Miller** *March 23, 2017 12:16*

+1 for patreon.  I toss Gina and Tom money every month, and it's worth every penny.  I'm not sure of the overhead on your end though.  


---
**Eric Lien** *March 23, 2017 12:48*

Perhaps Patreon or Kickstarter, but based on donation level you can request a feature. Seems like a good way to have people put their money where their mouth is if they want it to do something.


---
**Artem Grunichev** *March 23, 2017 12:57*

+2 for patreon


---
**Kirk Yarina** *March 23, 2017 13:16*

The shirts, while cool, just siphon off money for them and the shipping.  Time to go looking for that PayPal link


---
**Kirk Yarina** *March 23, 2017 13:21*

You should include links in your signature.  I'm on slow hotel wireless and your home page is appearing at a very slow crawl, gonna take a very long time to find that PayPal link


---
**Arthur Wolf** *March 23, 2017 13:33*

**+Kirk Yarina** Thanks a lot for the donation :)


---
**Kirk Yarina** *March 23, 2017 15:11*

Not yet, haven't been able to get into [smoothieware.org - start [Smoothieware]](http://smoothieware.org).



Finally found a laptop, site not working with a mobile device


---
**Ross Hendrickson** *March 23, 2017 17:38*

Do you have a prioritized backlog of features/issues somewhere? I would be curious to see what is left before you can launch the v2. What software pieces are missing (we spoke in the past about opencv related things). That might help the funding story as well.


---
**Arthur Wolf** *March 23, 2017 19:21*

[smoothieware.org - todo [Smoothieware]](http://smoothieware.org/todo) shows some of it, but a lot of it is quickly evolving, and documenting it well would be a huge time consumer.


---
**Anton Fosselius** *March 23, 2017 20:09*

what RTOS did you go with? smoothieware homepage is just black (with pixely icons)  on my phone.


---
**Arthur Wolf** *March 23, 2017 20:14*

**+Anton Fosselius** We are going with NuttX. We actually already financed C++ support in NuttX, as well as the port of NuttX to the LPC43XX, both of which have been merged with mainstream nuttx and are available to the community.


---
**Anton Fosselius** *March 23, 2017 20:18*

Interesting, never heard of it before. What is the benefits over FreeRTOS?


---
**Arthur Wolf** *March 23, 2017 20:20*

It's a bit of a mini-OS in itself, like a linux, and it comes with lots of neat tools you can adapt like a web server, a terminal. But it fits in much less RAM than a linux would.


---
**Wolfmanjm** *March 23, 2017 22:38*

I'd prefer a separate paypal/patreon for V2. The current link for paypal donations on the smoothie wiki is really for current smoothieware support, and it is easier to manage the two separately, not knowing where the money is to go. As Arthur is paying people from his pocket for V2 development the money needs to go to him for V2.


---
**Matej Rozman** *March 23, 2017 22:41*

**+Arthur Wolf**​ set some easy way of contributing money, I've just tried to look after link to PayPal from my phone and not successful. Do direct link, patron, just don't do kickstart ect., their reputation isn't worth a dime these days. 

I'll put in as much as I can even though I don't use smoothie or anything related to the project, just know how much effort and money projects like this can consume.


---
**Mike Mills** *March 23, 2017 23:38*

I'm in. 


---
**ekaggrat singh kalsi** *March 24, 2017 00:02*

Tshirts and keyrings would work better than a pateron or donation.


---
**Christopher Seward** *March 24, 2017 17:03*

I'm in for a few bucks. 


---
**Andrew Wade** *March 24, 2017 19:54*

I will happily support the good cause 


---
**Ross Hendrickson** *March 25, 2017 01:48*

**+Arthur Wolf** that list is nice. I find that having something like that helps me see things that I could do to help a project that also fall in the sweet spot of something I'm personally interested in learning/doing/have knowledge of.  I also use Patreon (though I hate their fees and find them excessive, even though I like their founder, a little love/hate thing going on there). 



I wouldn't mind chatting your needs for a web server at some point. You have my email already.


---
**Javier Prieto** *March 25, 2017 16:08*

Kickstarter for v2 is a good idea!!! I would do the campaign now and speed up with that ;)


---
**Stephane Buisson** *March 25, 2017 17:28*

Use kickstater for a very low goal , it's more important to have a good exposure (promo) and finish with a success. you could always add extended goal (stickers) as you raised  more money. a success will also be important for your next campain. **+Arthur Wolf**


---
**MakerGal** *March 25, 2017 21:13*

I'm in also!!! I want my tshirt 😂😂


---
**Charles Steinkuehler** *March 25, 2017 22:55*

Take a look at the HAL layer of Machinekit (or LinuxCNC), which can sit on top of just about any RTOS and provides a configurable real-time framework that turns "spice-like" netlist files (HAL files, basically a text netlist) into working real-time systems.  Note this is totally separate from the gcode interpreter and motion planner (which mostly run in userspace and not in hard real-time).



We're already working on breaking out the HAL layer of Machinekit so it can stand on it's own, so as long as your RTOS supports shared libraries (or you are willing to add that support) it would be very easy to craft a dynamic hard real-time foundation.  It might save you some time (and developer $$$).


---
**Rob Mitchell** *March 29, 2017 10:54*

Interested in supporting. How much money is needed to finish the project and get v2 out?


---
**Mark Ingle** *April 25, 2017 02:55*

**+Arthur Wolf** I will buy a T-shirt just let me know where and when.  But to be honest it may not by worth your time if you are looking to make a profit to support dev cost.  I was with Open Pilot for a few years before the project ended.  The store offered t-shirts, mugs, etc.. but the margin was thin on these products.  Most $ was made from the hardware.  You might get better cash flow with making a patch of v1 boards to sell....but there is more to consider on your end I am sure. 


---
**Rob Mitchell** *April 25, 2017 14:01*

Start with a kickstarter with hardware as goal 1. From the previous posts it appears you are thinking of two separate kickstarters before hardware release. This is way too long for most of us to get behind. 



Launch 1 kickstarter, include hardware as release 1. You will get more support from the community and be able to plan better for delivery while managing expectations.



Launch today, don't delay! :-)






---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/6hTXaVw4TsH) &mdash; content and formatting may not be reliable*
