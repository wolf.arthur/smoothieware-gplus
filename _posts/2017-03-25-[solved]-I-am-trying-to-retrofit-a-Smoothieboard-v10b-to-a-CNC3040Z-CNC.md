---
layout: post
title: "[solved] I am trying to retrofit a Smoothieboard v1.0b to a CNC3040Z CNC"
date: March 25, 2017 21:19
category: "Help"
author: "Jeremie Francois"
---
[solved] I am trying to retrofit a Smoothieboard v1.0b to a CNC3040Z CNC. I kept the TB6560 drivers and just made a cable for the dir/step/spindle/probe/estop signals. It plugs onto a 2x13 pin header that I soldered on the existing board, which simply match the signals of the parallel port (I did check more than once of course).



But I am having hard times with the config despite the nice doc.



The web server was up and OK with my old coreXY config & firmware. But I had no step signals (checked on my scope). I thought it could be due to a misconfiguration that triggered a protection meant for 3D printers (e.g. thermal protections etc). I then made lots of modifications (disabling all temps, extruders and heaters just in case), but probably screwed the config (fast blink on led 2 & 3, the other stay on).



I am using the latest "cnc" firmware. The web server fails to open, though I see my usual IP in the LAN. 



Now... a cool option for me would be to get a default "brainless" config file which is pre-tailored for a generic mill. Say, the default config is really nice to start with and customize a 3D printer, but there are significant differences for milling, and I just do not know exactly what goes on below.



I am not sure about the firmware, first. Quoting from github at [https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin](https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin): <i>"the firmware-cnc.bin is a build for CNC using a new pendant style LCD display, _</i><b>requires a graphic LCD</b>__ it also excludes modules required for 3D printing like temperature control and extruders etc. It also uses $H to home as G28 is used to park in real GCode."_



...well... does it mean I <b>need</b> an LCD to run it? :/ Is this still compatible with "old" smoothieboards?﻿

![missing image](https://lh3.googleusercontent.com/-SqXeF0akw0A/WNbe73TODMI/AAAAAAAAP5M/rQXpyE5dtjw_vJ5KSE5VrUBrSmF84cVMgCJoC/s0/smoothieboard_cable_adapter_to_cnc.jpg)



**"Jeremie Francois"**

---
---
**Arthur Wolf** *March 25, 2017 21:43*

I believe on a 3040 you can use Smoothie's on-board drivers ( at 2A ). I do on mine and it works fine.



You want to use the latest cnc firmware and the latest example config. Change only as little as you really need. 

On mine ( 3040 ) I only needed to change steps per mm, acceleration, and enable network. That's it ! Works perfect, the example config was studied for this. Don't change any more than you need.



In your case you may need to change the pins for step/dir, that's it.




---
**Jeremie Francois** *March 25, 2017 22:07*

**+Arthur Wolf** thanks, I did not dare because the tb6560 were rated for 3A iirc. Anyhow, so your config file has no spindle nor GRBL keyword?


---
**Arthur Wolf** *March 25, 2017 22:09*

**+Jeremie Francois** Spindle is manual right now ( I'll move to smoothie control later ). Same thing for grbl-mode, I plan it for later. The machine works great this way though.


---
**Jeremie Francois** *March 25, 2017 22:22*

**+Arthur Wolf** good to know. I can deal with the lack of PWM (or rely on an external control). The real deal is the quality of the movement compared to linuxCNC. I will check when I can and tell you, thanks again :)


---
**Jeremie Francois** *March 26, 2017 12:37*

update: there are hex inverters on the existing board so I just had to invert the signals (which is plain trivial with smoothieware), and it worked :)

I used the last build from source.


---
**Jérémie Tarot** *April 01, 2017 09:09*

**+Jeremie Francois** do you have issues with LinuxCNC so you are looking for an alternative, or do you want to see if Smoothie performs as well as LinuxCNC ? What characteristics of the movement do you want to compare ? 


---
**Jeremie Francois** *April 01, 2017 13:46*

**+Jérémie Tarot** my only major issue with linuxCNC is the need for a dedicated outdated  PC, because of the RTOS and of a parallel port: lots of wasted space, lots of cables, super sluggish system, really cumbersome physical setup. I even had linux issues because of the old hardware. I already have a decent desktop & laptops so I would not want to buy anything in addition.



So it was a real pain to get it ready for milling, and it took the entire table in the veranda ;) Moreover, I wanted to be able to hack my CNC, and both the parallel port and linuxCNC will not give this freedom easily.



Now about the performance: I cannot tell yet but I had zero complaints with linuxCNC, it did its job very well and reliably. But the jog dial was barely usable because of the ugly+sticky UI (priority to the signals = even slower interface -- at least on this PC).



I just hope Smoothieware+board will perform as nicely, and I would be glad if it does better of course (I would be a bit surprised though -- the guys behind linuxCNC really know their business).



Since I did not remove the old driving board, and I plugged the smoothieboard directly into the existing parallel port signals, I know I could go back. But so far, after switching to smoothie:



* the jog is day and night. I mean it does what I ask when I ask, and it stops when I stop. At last! :)



* It takes only a little box now, so I hope I will use my CNC more often. 



* I have lots of GPIOs to hack and add sensors :)



* I was not able to control my spindle from gcode yet, but I think I just need to fix the config. I need this for automated work. At worst I will bypass the existing parallel-port driver.



* I absolutely need to re-wire the emergency stop, or fix the config. It does nothing yet. The signal I routed on the smoothieboard did not tell whether it is input or output, and I did not want to "unplug" anything from the original board so I took the outgoing signal directly from the parallel port.



* I hope I will be able to run the fourth axis, even though I never made use of it (I tried YZA, but I never did XYZA anyhow). So I could live without for now.



* I need to find a good gcode sender. I first checked with the old, good and reliable pronterface (with serial-over-USB... meh, it does not seem as reliable as a parallel port -- but adding an ethernet router and/or having cables across the room is simply NO option to me). I really would like to use the full featured chilipeppr (as it has some serious gcode debugging options, but the json serial driver really refused to connect to the smoothieboard). I tried laserweb (found no big advantage compared to pronterface, but that was a really quick test). Now I need to give bCNC an in-depth testing (the UI is harsh, but everyone seems happy with it).



Regarding the last point: my super annoying issue is that I want to drive the CNC with a nice little Pipo X8 that I bought years ago for the task. It runs either Android... or Windows 10! There seems to be very few people that re-installed it under debian, so for now I tried to play the game and I kept windows. And it is a super huge pain in the ass for me to try and deploy all these nice open source software on a windows :) NodeJS, python, modules & so are just so trivial on Linux. I crashed W10 10 times and I hate it. And having browser / online requirements on windows just to run a CNC looks like unreliable to me. I would certainly favor an off-line non-browser application. 



So: bCNC is probably the key here. I mean, it even runs fine and consistently on a Raspberry! [github.com - vlachoudis/bCNC](https://github.com/vlachoudis/bCNC)


---
**Jérémie Tarot** *April 03, 2017 08:02*

**+Jeremie Francois** wow man, thanks a lot for this detailed answer ! 👍☺️



Did you consider using a BeagleBone Black with a CNC cape to run LinuxCNC/MaxhineKit instead of an old PC ? IIUC, it has a real time processing unit that may well make a difference, plus it has a decent form factor ;), runs a decent OS, and is real OSHW. Chances are you could even get the added I/Os you plan on needing. 



I LOVE the concept of a setup that could be switched form LinuxCNC, to ChiliPeppr, or LaserWeb with just a plug swap !



LaserWeb is a fast evolving project and seeing what happened in a year, I'd hardly tell how far it will get before Xmas !!! Version 0.4 is again a new beast, with app packaging to ease deployment, and should be usable on some kind of kiosk Linux setup... Feature wise, you're comparison with Pronterface surprises me, but without hands on experience, I won't comment further but invite you to give LW 0.4 a shot... Plus it has an amazing community ☺️



Anyway, bCNC seem to be a super tool one shouldn't miss in it's CNC toolbox, so you won't loose time getting handy with it... 


---
**Jeremie Francois** *April 03, 2017 09:45*

**+Jérémie Tarot** Yes, I was an early backer of the BBB + replicape v2. This combo is a killer, as the BBB is a real industrial-grade SBC imho and its PRUs make it a super obvious choice (way superior to the RPi + externally USB-driven controller). But the BBB is a tad slow; it has only one USB port (used by a wifi key for "preliminary" connection, updates and and uploads), and still no screen. What is really nice is you get a single, tightly coupled  package (control + linux), and a better layout of the connectors compared to the funky Smoothieboard v1 that scattered them all everywhere on the PCB (the v2 seems much much better). You made me want to give machinekit a try now, as I still did not fully settled on where to install my various boards! :D



Anyhow, I wanted a real UI so I chose a small PC with a real GUI (like the Pipo X8), which drives a dedicated controller. Now, I am uncomfortable running an app in a browser through a serial link bridge. Also in my case, dealing with Windows 10 is just freakingly painful (it is probably my fault but it seems like I am incompatible at the hardware level with windows: crashed it a lot, failed to find/install the proper version and so -- huh).



I always ran my 3D prints directly from a board and mostly never trusted USB links, but CNC need more control & jog work so an interface becomes a must.



If I really was to run a milling app in a browser, I think I would still favor Chilipeppr because of the gcode-level debugging and detailed 3D view (e.g. finding the gcode line to restart the job at), and historical straight & extended work dedicated to milling. But 1) it failed with me and the javascript bridge on W10 and smoothie board 1 (I have to check again) and 2) I feel even more uncomfortable because it requires me to stay online (there is no offline installation, contrary to Laserweb), and third party tests showed it may be somehow more unstable than other solutions :/



Once again, my priority is rock-solid communication: bCNC or even pronterface win this point. But the latter is barely better than letting the smoothieboard do it all on its own. Now its own UI is so minimalist that I would be more at home with a replicape, bash scripts and command line debugging instead ;)



Back to Laserweb: I love the project, and I really  like the people who make it :) I advertised it a lot when it started and even sponsored Peter at the time! I think it is a huge win for laser owners compared to the former crap that was delivered with chinese machines.

So yep, I will keep an eye on it because it certainly can work as a gcode sender for milling. But if it I only use it as a g-code sender, I think it is a bit overkill for me (I really do not expect to mill bitmaps a lot).



BTW we still need to meet around a CNC and a beer! No way in the next few weeks though, I will be far from home :)


---
**Wolfmanjm** *April 03, 2017 22:25*

laserweb can generate gcode pretty well, but the developers seem reluctant to fix the streaming to smoothie, so much so that they deleted my reply to someone reporting problems about streaming to smoothie from lw4 when I tried to correct the erroneous info the devs were giving. Contrary to what they say it is NOT a planner issue, it is a host streaming issue. To prove the point I wrote [fast-stream.py](http://fast-stream.py) which is on the smoothie github , [https://github.com/Smoothieware/Smoothieware/blob/edge/fast-stream.py](https://github.com/Smoothieware/Smoothieware/blob/edge/fast-stream.py) this will stream at full blast and all tested rasters have worked with it. So if you use lw4 you can generate the gcode then stream it with the fast-stream.py.


---
**Arthur Wolf** *April 03, 2017 22:27*

Grbml ... censorship definitely isn't ok, and I've had other Smoothie contributors get this sort of treatment recently, it's not ok and if it keeps up we'll have to go public about it ( which I don't have the time or courage to do right now. why can't people just do cool stuff and be nice ... )


---
**Wolfmanjm** *April 03, 2017 22:28*

I use bCNC for all my CNC needs. It has a little issue with the stop/alarm stuff sometimes, but it mostly works fine. Make sure you use the cnc build of smoothie and set the correct flavor of communications in bCNC (select smoothie instead of grbl)


---
**Jeremie Francois** *April 04, 2017 23:16*

**+Wolfmanjm** oh thanks for the counter intuitive hint about grbl vs smoothie. I was already using the grbl build but I have no clear picture regarding the diff between bCNC talking grbl or smoothie to the board :)

NTW, would your version of [fast-stream.py](http://fast-stream.py) help for chilipeppr driving a smoothie board?


---
**Wolfmanjm** *April 04, 2017 23:34*

I know nothing about chilipepper so I can't answer that question sorry :)


---
**Matt Wils** *April 06, 2017 17:49*

Where can I find this cape you speak of?


---
**Bonne Wilce** *April 09, 2017 06:46*

I think there is an issue, perhaps on both sides, but removing laserweb from being the host speeds and comparing streaming via other application,  pronterface for example performs about the same. Running directly from the smoothieboards SD card is slightly faster, however it tends to crash mid job.



The stream fast is still pretty slow, 100mm/s is not a solution, from what i understood the fast-stream also removes the ability to abort the current job.


---
**Arthur Wolf** *April 09, 2017 09:08*

**+Peter van der Walt** After I've recently had two people I respect and trust come to me with their story of how they got banned by you for frivolous reasons, assuming this was an act of censorship isn't laughable, it's a reasonable assumption.

I've just been removed as a member from the Laserweb github, the issue where we were trying to determine the cause(s) of transfer slowness was closed and on top of that I was asked to refrain from discussing the issue <b>at all</b>.

And this isn't all, there were other incidents on top of this, all adding up in my opinion to demonstrating you don't understand how to behave to make the overall open-source digital fabrication community welcoming and tolerant.

If you don't do some solid thinking on this issue, people are going to keep getting hurt and that's not acceptable.



People should be able to disagree with you as long as they stay civil. Handling people who agree with you is easy, how you react when they don't is what really determines if you are building a friendly community or not.

I know handling Smoothie for the past 5 years doesn't automatically mean I'm right on anything, <b>but</b> : we have been extremely careful how we handle disagreement and any other kind of conflict over the past few years in the Smoothie project ( often against the basic instinct of some contributors to just ban and forget about it ), and it's a big part of what has made it's success : it's a friendly place where you know you'll be treated fairly. If we didn't have that, it's likely it wouldn't be where it is now.



What I'm saying is : your behavior hurts the laserweb project's reputation, I'm already hearing people I had no contact with previously ( so I didn't influence them ) who have a vision of the project as being unwelcoming. This is super sad especially considering that technically it's a project that's not too difficult to start contributing to, so it's very much a waste.



It's only going to get worse if you don't learn to calm down and handle disagreement or any other kind of conflict with the kind of detachment and fairness you see in other projects around you.



Past experience shows you are likely to only see this as an attack and react the same way you have in the past, but I really hope you don't and instead realize there is something to fix, and you fix it. It's not that hard, and we all have had to adjust on this front in one degree or another, it's not just you it's very common.












---
**Arthur Wolf** *April 09, 2017 09:40*

If you really aren't the lead on LW anymore, this problem I think you have will not have much impact in the future, so none of this is relevant and I'm not going to waste time on it.



I'm just going to give the new LW leads the free trust I give to anyone I  don't know, and see what they do with it. Hopefully they do the same sort of scientific fact-finding we did on our side, and come back to us later on for a reasonable discution on the subject.


---
**Jeremie Francois** *April 10, 2017 02:09*

Glad to see you cooled down a little, guys! I would better see you collaborate than fight. This also is the spirit of openess (which is even more important than open source imho).


---
**Jérémie Tarot** *April 10, 2017 18:55*

Well, passed the sadness of seeing two MVPs of our beloved open source fabrication community having such a discussion... I'm too glad things are cooling down and hope a fruitful and open collaboration will be our everyday enjoyment for the days to come and the benefit of both projects and the community as a whole... 



PS: **+Jeremie Francois**​ overloaded for the next weeks, hope we'll finaly have that beer before summer... 


---
**Jeremie Francois** *April 13, 2017 23:50*

**+Jérémie Tarot**​ Not right now in anyway... I am actually in san pedro de atacama (where you can order a beer only if you eat something!!) :D


---
*Imported from [Google+](https://plus.google.com/+JeremieFrancois/posts/TqPsdK3Jk4T) &mdash; content and formatting may not be reliable*
