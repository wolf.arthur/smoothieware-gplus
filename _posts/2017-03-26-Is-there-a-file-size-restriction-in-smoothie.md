---
layout: post
title: "Is there a file size restriction in smoothie?"
date: March 26, 2017 02:40
category: "General discussion"
author: "jerryflyguy"
---
Is there a file size restriction in smoothie? I'm having lockup issues on longer prints. The last time it was 91% into a 27hr print. ~35mb file w/ 1.2M lines of code, ~270 layers. Am I locking up because I'm hitting some 'limit'? 





**"jerryflyguy"**

---
---
**Arthur Wolf** *March 26, 2017 09:00*

There is no file size limit, you could print 4GB files no problem.

What happens exactly when it locks up ? Does the board still answer to commands ? Do the LEDs still blink, and how ? Is temperature regulation still active ?

Can you open the files you are sending and check if maybe they are truncated ? ( this happens )

What host software are you using ? What slicer ? Are you printing over serial, network, or via the SD card ?


---
**jerryflyguy** *March 26, 2017 09:38*

Thanks **+Arthur Wolf** I've had two styles of lockup. The first is running off the SD card. The system locks, no control inputs work(no response to button pushes etc) the heating will stop on this variety... system is still 'alive' but nobody is home so to speak. That's the most common one I've had. Usually happens 15-20hrs into a print.



I tried printing via Pronterface and have had one lockup or failure. But this time the system didn't die, it just stopped printing, kinda paused? Heaters still on and would respond to button pushes etc.



Using S3D to slice, haven't checked the files. 



It's on an Azteeg X5 mini


---
**Arthur Wolf** *March 26, 2017 09:46*

Ok let's concentrate on the first style of lockup for now ( printing from SD ).

Can you please print from SD, but <b>still with Pronterface connected to the machine</b>, then when it crashes I want to know : 

* Any error messages in pronterface's logs ?

* What do the 6 LEDs do <b>exactly</b> ?


---
**jerryflyguy** *March 26, 2017 17:07*

**+Arthur Wolf** will do! I'll report back when it happens(will be a day or so I'd think)


---
**Rustin Guerin** *March 27, 2017 14:47*

I had a similar issue that I chased for a while. Ended up being a microswitch stuck. Try M119 command and check the status of all your switches, assuming you're using them. 


---
**jerryflyguy** *March 27, 2017 14:52*

**+Rustin Guerin** I can try that, it homes normally and with repeated accuracy so it'd seem unlikely but worth checking none the less


---
**Rustin Guerin** *March 27, 2017 21:04*

Mine homed correctly as well, it was actually my X-axis Max Endstop, which is not used for homing. Worth checking out.


---
**Wolfmanjm** *March 27, 2017 22:57*

a bad sdcard can also cause this. bad blocks can casue the sdcard read to hang then the watchdog kicks in.


---
**jerryflyguy** *March 28, 2017 03:45*

Ok. A new SD is a cheap enough fix if that's the solution. Will give that ago this week


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/Q9koFmNzdnU) &mdash; content and formatting may not be reliable*
