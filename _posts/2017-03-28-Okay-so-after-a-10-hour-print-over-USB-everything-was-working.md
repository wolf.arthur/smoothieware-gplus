---
layout: post
title: "Okay so after a 10 hour print over USB everything was working"
date: March 28, 2017 01:21
category: "Help"
author: "jacob keller"
---
Okay so after a 10 hour print over USB everything was working.



But when I got back from work to start a new print I turned​ the machine on and just the red VBB was on so I turned the power supply off. Plugged the USB in the smoothieboard and all the LEDs didn't turn on? And my computer siad that this device is drawing to much power so it disabled the port.



So I was like ok maybe if I unplug all the wires leaving the smoothieboard naked with just the USB connected that it would change something but still no LED lights?



So then I rebooted my computer with the USB desconnected. Then plugged the USB back in and still no LED lights?



So then I took my smoothieboard to my desktop computer plugged the smoothieboard into the USB but still no LED lights?



After all that in the end. I think my board went bad. Somehow



Any ideas what I could do. I don't want to buy another board.



Thanks

Jacob





**"jacob keller"**

---
---
**Joe Alexander** *March 28, 2017 01:24*

did you try removing the sd card, formatting and reinstall firmware on pc then put back in smootheie?


---
**jacob keller** *March 28, 2017 01:27*

Good question. Didn't think to do that. Thanks I'll try that.


---
**Chris Chatelain** *March 28, 2017 01:32*

I've had cards go bad myself as well


---
**jacob keller** *March 28, 2017 02:02*

**+Joe**​ **+Chris Chatelain**​ didn't work! Just tried plugging the 12v power supply in and now the VBB light doesn't turn on now. And the power supply is making a ticking sound. But when I unplug the smoothieboard from the power supply the ticking sound goes away?



Plus I just tried using a brand-new SD card right from the package.






---
**Joe Alexander** *March 28, 2017 03:19*

hmm in my machine my VBB is 24v not 12, typo? And im not sure why your PSU would be ticking, fan bearing maybe?


---
**jacob keller** *March 28, 2017 03:54*

**+Joe** I have a 12v power supply. The power supply ticks only when it's connected to the smoothieboard almost like it's going to start on fire I checked the positive and negative and it's right. 



And the smoothieboard gets hot. And the LED lights still don't turn on.



But when I desconnect the power supply from the smoothieboard and then turn the power supply back on it doesn't tick. 


---
**Joe Alexander** *March 28, 2017 05:02*

very odd, I would definitely not connect them until you figure out why its heating so bad. I tinker a lot but am no expert by any measure but that sends off bells in my head :)


---
**Arthur Wolf** *March 28, 2017 10:39*

This sounds like either a short in the wiring, or ESD, killed your smoothieboard unfortunately. This is very rare but it can happen ... It's possible replacing the microcontroller would fix the issue, but it's not guaranteed. It <b>does</b> most times people have tried.


---
**jacob keller** *March 28, 2017 20:36*

**+Arthur Wolf** here's a video of me turning it on.



[dropbox.com - VID_20170328_152806341.mp4](https://www.dropbox.com/s/xlmsj8s87pbsa2g/VID_20170328_152806341.mp4?dl=0)



Do you have a link to where I could buy the microcontroller for the smoothieboard?



Do you now if anything else would have fried so I can replace those parts as will.



Thanks


---
**jacob keller** *March 28, 2017 20:42*

**+Arthur Wolf** And here's a video of the power supply not connected to the smoothieboard.



[dropbox.com - VID_20170328_153845197.mp4](https://www.dropbox.com/s/zubzwqslnu3r1wi/VID_20170328_153845197.mp4?dl=0)




---
**Arthur Wolf** *March 28, 2017 20:48*

It's a LPC1769, you can find it for example on digikey or farnell


---
**jacob keller** *March 28, 2017 20:53*

**+Arthur Wolf**​ thanks! Any thoughts on the video?


---
**jacob keller** *March 30, 2017 21:35*

**+Arthur Wolf** check my wiring and there was short. So the smoothieboard is back up and running. My LCD screen came in today with the adapter. And the screen is still just blue with the original reprap discount glcd and the original black adapter. And anything else I need to do to get the glcd to desplay smoothie on the front? Is the blank blue screen have smothing to do with contrast?


---
**Arthur Wolf** *March 30, 2017 21:38*

Clone GLCDs and original GLCDs have inverted ( rotated 180 degrees ) connectors. Maybe you have the wrong adapter for your panel type.

You can try cutting an additional "slot" in the connector sockets, and rotating your connectors 180 degrees, and see if that then works.



Cheers.


---
**jacob keller** *March 30, 2017 21:46*

**+Arthur Wolf** I don't have clones​. I bought them as a kit. Tried turning them 180 degrees still nothing?


---
**Arthur Wolf** *March 30, 2017 21:47*

Then it's either contrast, or one of the pins isn't making contacts with the adapter.


---
**jacob keller** *March 30, 2017 22:32*

 +Arthur Wolf okay I made sure that the pins where touching and they are. Turned the cables 180 degrees. Turn the little contrast knob and still nothing. Maybe it's my configuration can you check it for me. [dropbox.com - config](https://www.dropbox.com/s/aqh30bd349ok2hg/config?dl=0)



Thanks


---
**Arthur Wolf** *March 30, 2017 22:41*

Config looks fine, I'm a bit lost here. Do you have cables you could use to wire the panel directly to the board without going through the adapter ?


---
**jacob keller** *March 30, 2017 22:47*

**+Arthur Wolf**​ I don't have any wires that I could use. Do you have a place that you recommend. Do you guys have a chart that I could use as a diagram so I now where to put the wires on the glcd and the board. 


---
**Arthur Wolf** *March 30, 2017 22:48*

It think the panel page has most of the info you need. for the cables, you pretty much want the rainbow cables that are used for breadboards. very common on aliexpress, but not sure where to get them close to you.


---
**jacob keller** *April 01, 2017 20:46*

**+Arthur Wolf** tried doing the rainbow cables still didn't work. Do you have a picture of yours​ that I can look at to compare to mine. So after spending roughly around $115 Dollars on just supply's​ to get this LCD to work. It still doesn't work. I have to be doing something wrong here. 


---
**jacob keller** *April 03, 2017 15:59*

**+Arthur Wolf** do you have a picture of your glcd connected ​to your smoothieboard? So I can compare mine to yours.



I did the manual wiring and it still didn't work.



Thanks


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/Mbai7DRzFSY) &mdash; content and formatting may not be reliable*
