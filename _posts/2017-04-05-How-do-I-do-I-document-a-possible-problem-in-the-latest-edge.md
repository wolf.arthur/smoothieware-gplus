---
layout: post
title: "How do I do I document a possible problem in the latest edge?"
date: April 05, 2017 19:46
category: "General discussion"
author: "Michael S"
---
How do I do I document a possible problem in the latest edge?

I attempted to do a G32 on a Rostock mini pro using the JhonSL fsr board and 3 FSR's under the bed.



There was no override file present on the SD card at the time of the initial run.



The board is an Azteeg x5 v2

Commands were issued via USB serial from a Windows ten machine



The command would run through several iterations then become unresponsive, the log froze with a program exception and thread ended error. Then disconnected. This happened repeatedly.



I then tried:

G32 E0 and it was successful 

M500

G32 R0 and it was successful 

M500



After this G32 doing full radius and endstop calibration will iterate and succeed without crashing. 



I have my config and terminal logs of the sessions on the machine in question









**"Michael S"**

---
---
**Arthur Wolf** *April 05, 2017 19:55*

It's possible you were very close to running out of ram, and because latest edge uses a tiny bit more RAM, you are now out of ram. can you try disabling a switch module and changing your queue from 32 to 16 ( just for testing ), and try to reproduce the error ?


---
**Michael S** *April 05, 2017 20:05*

Sure, my switch module is only configured for fan though. Is that enough? Just # it our or set to false?



Do you want me to delete the override file and try it that way or leave it in place?



As I said it did become successful after the overrides file was in place having calibrated endstops and radius separately. Btw the radius is not large on this machine. (Testing 60mm R)



Not sure if that's a factor. 



I can test the Rostock today if you like.  Just want to know how to best document it for your analysis.



I also have a bunch of other boards I could try it on on different bots, but that would have to wait for me to finish taxes (ugg, being an adult sucks lol) I have pretty much all the Azteeg versions including the latest with the Bigfoot drivers. And a lot of different machine geometries. I may be a good resource in the future. Converting everything to smoothie.  


---
**Arthur Wolf** *April 05, 2017 20:06*

Just set the switch to false, don't change anything else ( in particular don't change the override or anything, we want to change as little as possible to isolate the issue ).


---
**Michael S** *April 05, 2017 20:09*

Yup, one variable at a time.. I can test with switch false, then true, and queue 16 then both if you like. 


---
**Arthur Wolf** *April 05, 2017 20:10*

Just try one, and if it still crashed, try both together.


---
**Michael S** *April 05, 2017 20:16*

Failed with switch disabled and no other changes

Now going to change queue


---
**Michael S** *April 05, 2017 20:24*

Success with queue @16 and switch off

M500 save



Now turning switch for fan to true. 


---
**Michael S** *April 05, 2017 20:28*

Failed with switch set to true and queue set to 16


---
**Michael S** *April 05, 2017 20:30*

Going to set queue back to 32. And run G32 E0 and G32 R0 


---
**Michael S** *April 05, 2017 20:37*

Nope. So just set switch to false and plan ahead queue to 16 when I need to calibrate. Then back to 32 and true to run normally in the meantime? And do you need the config and the logs or this is enough for now? If it matters I am running the Azteeg with stepping set to max I believe. 


---
**Michael S** *April 05, 2017 20:40*

I can also evaluate any other builds if/when they are available. 

Running mar 23 22:12:46


---
**Wolfmanjm** *April 05, 2017 23:57*

what did the leds do? what host program are you using? the error is from the host not smoothie. FWIW I have never actually run out of memory on a delta with the standard config and a few switches set. issue the mem command at a prompt and see what you get I'll bet you are nowhere near running out of memory.


---
**Blake Dunham** *April 05, 2017 23:58*

I've been having some issues along the same line with my X5 V2 using Simplify3D. I upgraded to the latest edge build and now the printer seems to lag. It will print a few lines of code, freeze for a short while, and then resume. Would this be due to a ram shortage as well? I don't run an LCD. 


---
**Wolfmanjm** *April 06, 2017 00:08*

S3D cannot be used as a host to smoothjie it does not support the protocol smoothie uses, try pronterface or octoprint or a plain temnial. Any proiblems with S3D host are their problems not ours :)




---
**Wolfmanjm** *April 06, 2017 00:09*

and no it would have nothing to do with RAM as I said there is usually plenty of RAM once it boots. to check issue the mem command if you have more than 2k free memory you are ok.


---
**Michael S** *April 06, 2017 05:57*

Had the same issue with pronterface, and no terminal at all with a custom.menu set up to do G32.

However I was able to get G32 E0, M500, G32 R0, M500 to work on both. 


---
**Michael S** *April 06, 2017 05:59*

I don't have visual on the leds, they are under the delta, but I can put it up on books and try again if that is a critical piece of data.


---
**Blake Dunham** *April 06, 2017 05:59*

I managed to fix my problem. I disabled retract while wiping and that seems to have fixed my intermittent pausing I was getting. I don't know why you can't use smoothie as a host, I've been using it for a few years now.


---
**Wolfmanjm** *April 06, 2017 06:36*

It is odd that runni g with E and R works as it does exactly the same thing. Did yo u monitor the log for any error messages? did you set initial height correctly as per the wiki instructions?



If you use FSRs they tend to be overly sensitive and can sometimes trigger prematurely which would cause the calibration to cancel. I had that just happen to me with my FSR setup. I also run with far less memory than you would have available and routinely run G32 with NO issues whatsoever. So I can pretty much guarantee you are not running out of memory.

My best bet is you need to increase the initial_height setting.


---
**Wolfmanjm** *April 06, 2017 19:52*

we really need more information than "it hangs" does it crash? does it just stop? are there messages telling you why it stopped? FWIW I run this everyday and have zero problems!


---
**Wolfmanjm** *April 06, 2017 19:53*

and FWIW there have been NO changes to the code since master that would affect G32.




---
**Michael S** *April 06, 2017 21:27*

No problem, I am not trying to provide anything other than what you need to investigate the problem further.  So here are some more data points:



1. it's a new smoothie build, so I am not saying there is anything wrong with edge vs. an older build.



2. I don't want to say crash or hang because I am not really sure what's going on and I don't want to give the wrong impression 



3 the machine is reasonably well built given the physical limits of the Rostock mini pro design (not as rigid as a Kossel etc.)



When running the command, when the "issue" occurs, The effector will stop moving.  The mini viki 2 display does not show any odd symptoms, it says "Smoothie Printing" as usual. The log stops getting ok's status messages etc. then windows, will lose the USB smoothieboard device and try to reaquire it. It will come up as device not recognized after a bit. Heaters fans etc seem to stay on.



Only resetting the controller returns things to normal. Then windows recognizes the device again...  no driver remove/reinstall just the typical events you would see when you plug in any smoothieboard or compatible.



The measurements of the arms bed and radius are very good if not excellent. And the physical adjustments of the endstops are the same. When the cal is successful, and it has been under some circumstances, the suggested soft trim "set trim to X:0.000000 Y:-0.066913 Z:-0.059882" that seems to be not a lot to me, but I could be wrong.  



It may have something to do with the numbers I am choosing for the leveling strategy.



I chose 7 as a starting level, because for some odd reason 10 wasn't working, kicking off the error that I needed to check my starting values for the leveling strategy ( I know I am paraphrasing the error code here, sorry, I can dig into the older logs if I need to be more precise).



I chose a radius for calibration of 60 because I didn't want to run into bed retention with the heater block. The actual glass plate is the typical 170mm radius/85mm diameter



The probe points tap dead center in the FSR's, I can see them through the bed.



I am probing at pretty slow speeds, the Rostock design as stated above is less rigid, as stated above, if the frame flexes too much dur to inertia and slamming the effector around, FSR's under the bed will give off a bunch of pre-mature positives, (technically not false ones, the sensors are being triggered by the plates inertia relative to the frame or by the frame flexing against the plate squeezing the FSR)



The JhonSL fsr controller further minimizes false readings, by actively compensating for bed weight, print weight etc. only a tap or other sudden change really sets off the output of the fsr controller board to the Azteeg probe pin. 



I know you and are aware of most of this if not all, I am just trying to provide the details for someone who's doesent that may read this later and be in a similar situation.





Since delta radius is really just a number that controls where the Kinematics and inverse Kinematics are equal as I understand delta math, I try not to get too hung up on measurements that are NASA accurate. I did for a long, time and couldn't wrap my head around why the most precisely measured and built delta didn't just simply need no calibrating out of the gate. I know the results of what the tramming system reports are more important than falling in love with your precisely measured actual radius. Flat math within your build envelope is what you are shooting for regardless of what you "know" delta physical measurement are.

 

I don't yet have data on what the controller play leds are doing when the issue occurs.



Here is a log of the Successful G32 E0|M500|G32 R0|M500 (Success) followed by the G32 fail, from the terminal perspective, it's not very informative to me, but you might see a clue and let my know of anything else I could try.



To be clear, I think smoothie is a great project.  I am committed to converting everything I can to smoothieware, just puzzled by this one and wondered if I am doing it wrong or missed something obvious.





Connecting...

Printer is now online.

>>> G32 E0

SENDING:G32 E0

Calibrating Endstops: target 0.030000mm, radius 60.000000mm

set trim to X:0.000000 Y:0.000000 Z:0.000000

initial Bed ht is 165.367523 mm

center probe: 0.0081

T1-0 Z:7.1100

T2-0 Z:7.1634

T3-0 Z:7.1578

set trim to X:0.000000 Y:-0.066913 Z:-0.059882

T1-1 Z:7.1578

T2-1 Z:7.1831

T3-1 Z:7.1662

trim set to within required parameters: delta 0.025299

Calibration complete, save settings with M500

>>> M500

SENDING:M500

Settings Stored to /sd/config-override

>>> G28

SENDING:G28

>>> G32 R0

SENDING:G32 R0

Calibrating delta radius: target 0.030000, radius 60.000000

initial Bed ht is 165.353455 mm

center probe: -0.0284

CT Z:7.014

T1-1 Z:7.177

T2-1 Z:7.132

T3-1 Z:7.172

C-1 Z-ave:7.1606 delta: -0.146

Setting delta radius to: 85.2844

T1-2 Z:7.045

T2-2 Z:7.065

T3-2 Z:7.096

C-2 Z-ave:7.0688 delta: -0.054

Setting delta radius to: 85.1484

T1-3 Z:7.009

T2-3 Z:6.989

T3-3 Z:7.054

C-3 Z-ave:7.0172 delta: -0.003

Calibration complete, save settings with M500

>>> M500

SENDING:M500

Settings Stored to /sd/config-override

>>> G28

SENDING:G28

>>> G32

SENDING:G32

Calibrating Endstops: target 0.030000mm, radius 60.000000mm

set trim to X:0.000000 Y:0.000000 Z:0.000000

initial Bed ht is 165.519394 mm

center probe: 0.0194

T1-0 Z:6.8428

T2-0 Z:6.9075

T3-0 Z:6.9187

set trim to X:0.000000 Y:-0.080995 Z:-0.095077

T1-1 Z:6.8653

T2-1 Z:6.9356

T3-1 Z:6.9159

set trim to X:0.000000 Y:-0.169040 Z:-0.158474

T1-2 Z:6.9272

T2-2 Z:6.8822

T3-2 Z:6.8625

set trim to X:-0.080995 Y:-0.193688 Z:-0.158474

T1-3 Z:6.8372

T2-3 Z:6.9019

T3-3 Z:6.9272

set trim to X:-0.080995 Y:-0.274683 Z:-0.271186

T1-4 Z:6.8794

T2-4 Z:6.8484

T3-4 Z:6.8766

set trim to X:-0.119725 Y:-0.274683 Z:-0.306401

T1-5 Z:6.9075

T2-5 Z:6.8541

T3-5 Z:6.8850

set trim to X:-0.186638 Y:-0.274683 Z:-0.345150

T1-6 Z:6.8597

T2-6 Z:6.8737

T3-6 Z:6.8766

trim set to within required parameters: delta 0.016876

Calibrating delta radius: target 0.030000, radius 60.000000

initial Bed ht is 165.516586 mm

center probe: -0.0059

CT Z:7.006

T1-1 Z:6.851

T2-1 Z:6.907

T3-1 Z:6.908

C-1 Z-ave:6.8887 delta: 0.117

Setting delta radius to: 85.4414

>>> ;ITS HANGING NOW

<b>*</b> Unknown syntax: ;ITS HANGING NOW

[ERROR] Can't read from printer (disconnected?) (SerialException): call to ClearCommError failed

[ERROR] Can't write to printer (disconnected?) (SerialException): WriteFile failed ([Error 22] The device does not recognize the command.)



 


---
**Wolfmanjm** *April 06, 2017 21:42*

what host are you using? the * Unknown syntax is very unusual and makes me think the host is disconnecting for some reason because of the feedback it is getting.

It seems smoothie is running fine and not crashing you are just getting a host disconnecting which is usually the hosts fault. but we need some more info.

Without seeing what smoothie error is being sent it is hard to diagnose. I suspect it is a premature FSR trigger.


---
**Wolfmanjm** *April 06, 2017 21:44*

IF you can run G32 from a plain terminal or from printerface, and paste the entire console log to [pastebin.com - Pastebin.com - #1 paste tool since 2002!](http://pastebin.com) and give us the link. I need to see what smoothie is reporting and not have the host disconnect




---
**Michael S** *April 07, 2017 00:02*

That was from pronterface. The unknown syntax error is from me typing "its crashing now" at the console when smoothie stopped.

I will see what I can come up with on the plain terminal side. I assume that telnet to a network interface will provide less data, not more correct? If that's a better route than the USB serial, I can install a nic on the Azteeg if that'd help.

Also remember that it did the same thing from the custom menu panel command.  I THINK we can eliminate the host as a cause.

I know you are just looking for anything that will keep reporting status messages.  



Are there any debug or verbose commands I should set first?



Also I have been running the USB serial at 250000 I assume this is correct.



 I haven't seen anything in the config to set the baud rate on the USB side UART, only config for the hardware UART for the second serial port. 



 I can connect an Esp8266 there and Bluetooth in if that might provide a better chance of catching the error. 



Again, just trying to take the route you would prefer, and I have the options on hand. 




---
**Wolfmanjm** *April 07, 2017 00:07*

what we really need to know is if it crashed or not and for that you need to see the leds as asked before. Then to debug further you need to read how to provide a crashreport on the wiki, under troubleshooting, you will need an ftdi on the uart.

USB serial has no bauidrate it is ignored.

network will not work at all.

if it crashed then the crash dump is essential to go any further as I cannot duplicate this issue at all.


---
**Michael S** *April 07, 2017 00:08*

Oh and for what it's worth, I miss the old 9-pin serial ports that didn't disconnect, USB serial is great and convenient, but hardwareserial was better for diagnosing this kind of thing




---
**Wolfmanjm** *April 07, 2017 00:14*

there s h/w serial it is on the uart port. read about it on the iki.


---
**Wolfmanjm** *April 07, 2017 00:41*

do you have an aluminum or glass bed?


---
**Wolfmanjm** *April 07, 2017 00:46*

[smoothieware.org - mri-debugging [Smoothieware]](http://smoothieware.org/mri-debugging)




---
**Wolfmanjm** *April 08, 2017 05:26*

try the latest firmware-hires.bin on github in the Firmware folder. I reduced the stack usage for some operations and that may be what you are running into given how odd the failure mode is. It worked for someone else having G32 issues.


---
**Wolfmanjm** *April 08, 2017 20:15*

it is just the latest version 


---
**Michael S** *April 09, 2017 01:18*

Performance of the level function seems much improved. It completes, the console still reports some semaphore errors. I posted the logs publicly to pastebin as you requested earlier. Username Mshievitz just like here. 


---
*Imported from [Google+](https://plus.google.com/103972936973249035300/posts/jnhf6VkgKJw) &mdash; content and formatting may not be reliable*
