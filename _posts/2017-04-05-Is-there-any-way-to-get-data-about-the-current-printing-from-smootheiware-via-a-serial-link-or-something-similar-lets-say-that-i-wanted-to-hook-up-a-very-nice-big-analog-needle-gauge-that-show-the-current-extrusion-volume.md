---
layout: post
title: "Is there any way to get data about the current printing from smootheiware via a serial link or something similar, lets say that i wanted to hook up a very nice big analog needle gauge that show the current extrusion volume"
date: April 05, 2017 19:04
category: "General discussion"
author: "Erik Cederberg"
---
Is there any way to get data about the current printing from smootheiware via a serial link or something similar, lets say that i wanted to hook up a very nice big analog needle gauge that show the current extrusion volume per second etc via a arduino connected by serial to the smoothieboard? ;)





**"Erik Cederberg"**

---
---
**Arthur Wolf** *April 05, 2017 19:18*

You can get print progress via the "progress" command, but it won't show volume per second we don't have a way to get that sort of info. The host program would be able to analyze the gcode it's sending and control the gauge so that's maybe a possibility.


---
**Erik Cederberg** *April 05, 2017 22:45*

OK, we just had this crazy idea of making a steampunk-looking printer with lots of analogue gauges to show everything from extrusion volume per second to temperatures to progress, but it might not be that simple then :-)


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2017 02:24*

Do it as an octoprint plugin. That's always the answer. 


---
*Imported from [Google+](https://plus.google.com/114505261653802471003/posts/88L5k5A6eYd) &mdash; content and formatting may not be reliable*
