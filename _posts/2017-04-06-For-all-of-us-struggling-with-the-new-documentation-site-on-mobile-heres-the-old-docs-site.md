---
layout: post
title: "For all of us struggling with the new documentation site on mobile, here's the old docs site"
date: April 06, 2017 19:03
category: "General discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
For all of us struggling with the new documentation site on mobile, here's the old docs site. I have this on the home screen of my phone, you should too. 



[http://smoothieware.github.io/Webif-pack/documentation/web/html/index.html](http://smoothieware.github.io/Webif-pack/documentation/web/html/index.html)





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Jeremiah Coley** *April 06, 2017 22:27*

Thanks, ya not being able to look at it on my phone is killing me, some of the time I just want to look up something on the fly.


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2017 22:31*

I do my customer support (most of which is on the Cohesion3D G+ community) primarily on my phone. So the smoothie docs not being visible was unpleasant to say the least. 


---
**Griffin Paquette** *April 06, 2017 23:21*

I think we really need to just fix the issue wit the smoothie website. **+Arthur Wolf** any way we can mess with that dropdown bar? Between all the guys on this community we can hopefully fix the code.


---
**John Pickens** *April 08, 2017 02:08*

In the interim, "Request Desktop Site" in Chrome Settings works for me.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/WEmjQPPtjSH) &mdash; content and formatting may not be reliable*
