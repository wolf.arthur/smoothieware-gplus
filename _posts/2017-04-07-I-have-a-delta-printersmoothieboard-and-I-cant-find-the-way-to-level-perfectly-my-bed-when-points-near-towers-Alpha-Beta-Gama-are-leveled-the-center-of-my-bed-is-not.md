---
layout: post
title: "I have a delta printer+smoothieboard and I can't find the way to level perfectly my bed, when points near towers Alpha/Beta/Gama are leveled the center of my bed is not"
date: April 07, 2017 19:09
category: "Help"
author: "Michel DA SILVA"
---
I have a delta printer+smoothieboard and I can't find the way to level perfectly my bed, when  points near towers Alpha/Beta/Gama are leveled the center of my bed is not. At the center I must lift my hotend (0.2 mm) so I not success to print big part. My bed is Ø400mm. My bed is a mirror (4mm) so I think it's perfectly flat.

I was thinking about the tower angles, I check it, it's seems very good but i not have the tools to have an accurate reading.

May someone explain me how delta_tower_offset / angle work?

I not understand how to use it 

delta_tower1= delta_tower Alpha?...

What value have we have to write? Diference between 120° and the value?

Thanks





**"Michel DA SILVA"**

---
---
**Arthur Wolf** *April 07, 2017 19:19*

To use these values, you need to use an automated calculator that uses readings of heights at different points on your bed. There is no way to finetune these yourself by hand.

See [escher3d.com - Escher 3D](http://www.escher3d.com/pages/wizards/wizarddelta.php)


---
**Michel DA SILVA** *April 07, 2017 19:22*

**+Arthur Wolf** Merci


---
**Wolfmanjm** *April 07, 2017 20:04*

use delta grid DO NOT USE the tower angles they do not work very well if at all.


---
**Chris Chatelain** *April 07, 2017 20:04*

This sounds like a delta_radius problem to me.


---
**Wolfmanjm** *April 07, 2017 20:11*

Yes he needs to run G32 first then G31 as described in the wiki [smoothieware.org - zprobe [Smoothieware]](http://smoothieware.org/zprobe#delta-calibration)


---
**Michel DA SILVA** *April 07, 2017 20:20*

I will try tomorrow, i will make a z-probe.

Thank you


---
**John Pickens** *April 08, 2017 02:05*

I agree with Chris Chatelain, from your description, your print head is closer to the bed away from center, and higher when centered. This is corrected by increasing your delta radius value.  Here is the best guide I use for calibration:

[minow.blogspot.com - Myths About Building A Delta](http://minow.blogspot.com)


---
*Imported from [Google+](https://plus.google.com/115671562824621394958/posts/LGZVwNG5Jcf) &mdash; content and formatting may not be reliable*
