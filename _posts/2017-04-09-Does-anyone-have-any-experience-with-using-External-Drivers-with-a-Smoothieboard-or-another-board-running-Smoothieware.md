---
layout: post
title: "Does anyone have any experience with using External Drivers with a Smoothieboard or another board running Smoothieware?"
date: April 09, 2017 16:27
category: "General discussion"
author: "Kelly Burns"
---
Does anyone have any experience with using External Drivers with a Smoothieboard or another board running Smoothieware?  I fried the M1 Driver  on my Smothieboard X3.  While not completely dead,  it also seems to have affected the M2 Driver output.  This all worked until I made a careless mistake in the final testing.  The long story below, but basically I now have BETA (Y) Axis hooked to M3 output and working fine.  I am attempting to get ALPHA (X) working on an External 8825 Driver using the M4 Pre-Driver Pin connecdtions on the Smoothieboard.  It moves, but its not reliable or consistent.  





Long story here: 

I'm finishing up a physical and controller converson on my K40 Laser.  With the helpof LaserWeb community, I had everything working great.  Homing and CArraige Movement were perfect and the Laser was firing when running The G-Code. 



After spending 5 hours aligning the laser with my upgraded (220x500) carriage. I got in a hurry to run a Test engrave.  When I hit the connect button in Laserweb, I heard a pop and immediately smelled burnt electronics.  I had fried the M1 Driver Chip.  All I could see is that my unmounted middle-man board was dangling close to the carriage and it likely created a short.  The chip was visibly damaged and the X axis was dead.  Furthermore, the Y Axis moved, but seemed to lack the power necessary.  



The M3 Output seemed to work fine, so I switched the Y-Axis to that and made the changes in the config to use those pins instead of the default BETA pin definitions. It working fine and seems to be moving correctly.



For the X-Axis, I am attempting to use one the 8825 drivers I have and connect it to the  M4 Pre-Driver Output pins. I'm pretty familiar with these drivers with many hobby projects, I thought I had it working, but it doesn't move correctly.  I struggles to move and when it does, it goes in a random direction.  I either have the micro stepping wrong or something else is not correct.  I have never used the 1/32 stepping with these and looking a that Datasheet, I have them configured correctly.   If I connect Switch the X and Wire axis,  the X works fine and the Y does as describe above.   



Sorry for the long history. I figured it was important to rule out any of Physical things.   This thing was very close to the finish line am I very frustrated.  











**"Kelly Burns"**

---
---
**Arthur Wolf** *April 09, 2017 16:37*

Did you read [smoothieware.org - general-appendixes [Smoothieware]](http://smoothieware.org/general-appendixes#external-drivers) ?

About the 8825, you need to make sure the right pins are tied to 5V and GND, I think pololu's product page has a diagram.


---
**Kelly Burns** *April 09, 2017 16:41*

**+Arthur Wolf**   Thanks.  I did read that and used polus page fur them.  I'm pretty familiar with these drivers in Arduino world, but I was building mOving props and used default full stepping config.  



I'm less frustrated today so I'll re-read the smoothieware page.  Thanks again 


---
**Kelly Burns** *April 19, 2017 17:47*

Wanted to report back...   I wasn't insane and knew what I was doing.  It giving up on the external drivers, I purchased a different controller to run Smoothie on.  I had the same issues.  As it turns out, I pretty much fried everything when I blew the X Axis Driver.  The stepper motor was damaged as were the optical end stops.  I replaced the X-Axis motor and swapped optical for mechanical end-stops and everything is perfect.   






---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/FTsk94q8QWz) &mdash; content and formatting may not be reliable*
