---
layout: post
title: "I got a re-arm board from the kick starter"
date: April 09, 2017 11:04
category: "Help"
author: "Nigel Proctor"
---
I got a re-arm board from the kick starter. I've swapped the ramps shield on and installed the drivers. When I connect to the board my laptop sees the board as an external drive. I'm able to see the config file.

But I can't get repetier or pronterface to connect.

What have I missed?





**"Nigel Proctor"**

---
---
**Jeremy Parton** *April 09, 2017 11:13*

Serial drivers? 


---
**Arthur Wolf** *April 09, 2017 11:32*

What windows version do you have, what driver did you install ?


---
**Nigel Proctor** *April 09, 2017 12:38*

I used the smoothieware-usb-driver-v1.1.exe, it's a windows 7 machine.


---
**Triffid Hunter** *April 10, 2017 07:13*

Smoothie self-reports as a usb disk <b>and</b> one or two standard usb serial ports <b>and</b> a DFU device.



For some reason, Windows comes with a usb serial driver but won't use it unless you tell it to  associate it's built-in driver with that specific device.



Other OSes have no such problem, and simply go "oh ok you say you're a usb serial, no worries, here you go".



So have a wrestle with pointing Windows at smoothie's usb serial driver which, frankly, is just a magic text file that tells windows to use it's own built-in driver.



Beware that if you change the option that makes smoothie provide two usb serial ports or you use disable-MSD for some reason, windows may have a conniption and require further driver wrestling because it mostly ignores USB devices' self-description and gets confused if the description changes.


---
*Imported from [Google+](https://plus.google.com/111757889480474793582/posts/h9DM42iqa8Y) &mdash; content and formatting may not be reliable*
