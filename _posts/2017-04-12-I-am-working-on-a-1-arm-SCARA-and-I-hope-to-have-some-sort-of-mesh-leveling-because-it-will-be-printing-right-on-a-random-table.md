---
layout: post
title: "I am working on a 1-arm SCARA and I hope to have some sort of mesh leveling because it will be printing right on a random table"
date: April 12, 2017 00:56
category: "Development"
author: "Nicholas Seward"
---
I am working on a 1-arm SCARA and I hope to have some sort of mesh leveling because it will be printing right on a random table.   The probing pattern would need to cover a donut shape.   I was digging in the source and it looks like I can probably modify DeltaGridStrategy to do what I want.  Do I have the right idea?





**"Nicholas Seward"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 02:28*

You might want to talk to **+Douglas Pearless**. He's into this stuff. 


---
**Douglas Pearless** *April 12, 2017 04:17*

Can you publish more details on your SCARA?


---
**Nicholas Seward** *April 12, 2017 04:19*

**+Douglas Pearless** [plus.google.com - 1 Arm SCARA](https://plus.google.com/collection/IFEMGE) 

[http://forums.reprap.org/read.php?185,760198](http://forums.reprap.org/read.php?185,760198)


---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 04:20*

[plus.google.com - 1 Arm SCARA](https://plus.google.com/collection/IFEMGE)


---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 04:20*

Ahh You beat me to it!


---
**Arthur Wolf** *April 12, 2017 09:51*

There is a special branch of smoothie made by the guys at [morgan3dp.com - Morgan 3D Printers - Robotic 3D prototypers manufactured in South Africa](http://www.morgan3dp.com/) that has a lot of morgan-specific stuff in it that's not in the mainstream smoothie.


---
**Nicholas Seward** *April 12, 2017 12:37*

**+Arthur Wolf**​ That is my starting point.  However, machine coordinate based probing isn't something they needed.  I went through last night and figured out a rough approach that does some extra coordinate transformations.  Ideally, once a position is converted once to machine coordinated then it will be compensated for.


---
*Imported from [Google+](https://plus.google.com/+NicholasSeward/posts/Am2xuNmK2Dd) &mdash; content and formatting may not be reliable*
