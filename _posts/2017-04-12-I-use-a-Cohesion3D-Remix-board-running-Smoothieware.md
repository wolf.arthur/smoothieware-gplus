---
layout: post
title: "I use a Cohesion3D Remix board running Smoothieware"
date: April 12, 2017 22:57
category: "Help"
author: "Kraft King"
---
I use a Cohesion3D Remix board running Smoothieware. I have recently added a zprobe (3DTouch). I have modified the config file and have the servo and sensor working. The problem, however, is that when I send the G29/G31 command to initiate bed leveling compensation, the printer claims it cannot find a strategy to handle the command. What may be the problem? Thanks for your time! Here's my config file: [https://docs.google.com/document/d/1JYxwOkTsbclfdeAyua6BFDfzUFRxhdeONCvs9cF_z-Y/edit?usp=sharing](https://docs.google.com/document/d/1JYxwOkTsbclfdeAyua6BFDfzUFRxhdeONCvs9cF_z-Y/edit?usp=sharing) 





**"Kraft King"**

---
---
**Eric Lien** *April 13, 2017 01:59*

Do you have the latest edge build [firmware.bin](http://firmware.bin) loaded to be able to use rectangular grid?


---
**Kraft King** *April 13, 2017 03:30*

**+Eric Lien** I am not sure. I am using the firmware.bin that came with my board. Do you know where I could get the latest one or how I could modify my own?




---
**Wolfmanjm** *April 13, 2017 03:47*

You could try reading the copious amounts of documentation [smoothieware.org - start [Smoothieware]](http://smoothieware.org/)


---
**Eric Lien** *April 13, 2017 04:25*

**+Kraft King** [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware.bin)



But as **+Wolfmanjm**​ mentioned I would recommend reading through the details on how to upgrade on the website. 


---
**Arthur Wolf** *April 13, 2017 08:10*

**+Kraft King** You shouldn't be using the board without reading the documentation first. This will help make sure you don't destroy your board, and it would answer questions such as the one you are asking now.


---
**Griffin Paquette** *April 13, 2017 11:01*

I would take a good read over the documentation before you get started with the board like the other guys said. It's possible that you put it in the config incorrectly. Also feel free to check out the Cohesion3D G+ group here: [https://plus.google.com/communities/116261877707124667493?iem=1More](https://plus.google.com/communities/116261877707124667493?iem=1More) people with your exact board. 


---
**Kraft King** *April 13, 2017 11:28*

There is a FIRMWARE.cursor file in my SD card for the board. Do I replace this file with the .bin file? Sorry but I don't know much about these things.




---
**Arthur Wolf** *April 13, 2017 11:30*

**+Kraft King** This is explained in <b>a lot</b> of detail in the documentation. You want to read the documentation before asking questions, Smoothie has <b>extremely complete</b> documentation, and it's very important you read it before using your board or you will : 

* Possibly damage your board

* Ask questions that are already answered in full in the documentation, therefore taking support time/effort away from people with questions that are not answered in the documentation



Please go to [smoothieware.org - start [Smoothieware]](http://smoothieware.org) and go through the documentation.


---
**Kraft King** *April 13, 2017 11:52*

It worked! I only have one more question. The probe is offsetted from the nozzle so when I tell the machine to probe the probe will not hit the bed. I have defined the correct offsets in the config file. Why is it not reading the offsets?




---
**Arthur Wolf** *April 13, 2017 11:56*

Do you mean offset in XY or in Z ?


---
**Kraft King** *April 13, 2017 12:00*

XY


---
**Eric Lien** *April 13, 2017 12:14*

**+Arthur Wolf** when I did my probe a while back, It was unclear if offset should be possible or negative depending on the probes position in relation to the nozzle. Any chance in the documentation you could make that clearer?


---
**Arthur Wolf** *April 13, 2017 12:39*

**+Eric Lien** It's an offset relative to the nozzle, if it's positive it's positive, if it's negative it's negative. It's not inverted if that's your question, I'm not sure why it would be.



**+Kraft King** The XY offsets only tell the probing system what the offset is between your probe and your nozzle. If you want to make sure the probe will not go outside the bed, you need to change your homing offset ( the 0,0 point of your bed ), by modifying parameters like alpha_min etc.


---
**Kraft King** *April 14, 2017 21:56*

It worked! Thanks for all your help!


---
*Imported from [Google+](https://plus.google.com/114938032105833823625/posts/ZBKgFYiZMhk) &mdash; content and formatting may not be reliable*
