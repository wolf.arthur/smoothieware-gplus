---
layout: post
title: "The PT100 thermistor page mentions connecting to AVCC and AGND"
date: April 12, 2017 01:20
category: "General discussion"
author: "Chris Chatelain"
---
The PT100 thermistor page mentions connecting to AVCC and AGND. I've found AGND on the pinout diagram, but where would AVCC be if my board had it?





**"Chris Chatelain"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 02:01*

AVCC is the output of 3.3v through a 39nH inductor. 

The way I read the pt100 is to power the amp board with 3.3v. NOT 5V. 

Should wait for someone else to weigh in. 


---
**Arthur Wolf** *April 12, 2017 09:48*

AVCC is only available on the most recent versions of the smoothieboard ( v1.1 ), and is right next to AGND.

If you use 3.3v instead of AVCC ( because you don't have it ) it will work, but be less accurate.


---
**Chris Chatelain** *April 12, 2017 16:39*

is this the most recent pinout diagram? I'm not seeing it in the diagram, though I could just be blind.



[smoothieware.org - pinout [Smoothieware]](http://smoothieware.org/pinout)


---
**Chris Chatelain** *April 25, 2017 03:00*

Well, got it working just fine on a 1.0b. I kept the wires to the amp board very short, and noise doesn't seem to be a problem yet. Sure beats my old NTC thermistor for accuracy compared to Fluke thermocouple, especially in the upper temp range. less than 1C difference with a room temperature flutter of about 0.1C. pin 2.6 

 (on the headers before the mosfet) didn't work, 1.30 in the instructions did. 


---
*Imported from [Google+](https://plus.google.com/116517474949143363665/posts/J6mmbUMBxk9) &mdash; content and formatting may not be reliable*
