---
layout: post
title: "Fans don't turn on when config-override is on the SD card?"
date: April 13, 2017 22:18
category: "Help"
author: "jacob keller"
---
Fans don't turn on when config-override is on the SD card?



But will turn on when config-override is not on the SD card.



But I need the config-override on the SD card so the Z max is correct.



Is there away to fix this?



Thanks





**"jacob keller"**

---
---
**Wolfmanjm** *April 14, 2017 07:07*

config-override has nothing to do with fans, so there must be some other issue, maybe a bad sdcard? try a fresh new sdcard. or reformat the current one. and start with a new config.


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/6D6sJKT8oTG) &mdash; content and formatting may not be reliable*
