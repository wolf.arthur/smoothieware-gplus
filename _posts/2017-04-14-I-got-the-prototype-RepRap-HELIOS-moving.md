---
layout: post
title: "I got the prototype RepRap HELIOS moving!"
date: April 14, 2017 06:01
category: "Development"
author: "Nicholas Seward"
---
I got the prototype RepRap HELIOS moving!  I even got the inverse and forward kinematics plugged in.  I know they work to some extent.  I was able to do Cartesian jogging at a couple of points.  The problem is I can't find an elegant way to set arm position at the beginning of a print.  I don't have any limit switches which keeps me from using G28 solutions.  I have tried using G92 but have read in a few places that it doesn't work for transformed coordinate system.  Oh internet...please share your wisdom.  I know how to hack together a solution but I am hoping not to have to hack up the code.





**"Nicholas Seward"**

---
---
**Nicholas Seward** *April 14, 2017 07:25*

The problem is that it thinks the x and y position is at 0,0 at boot time which is right over the central pivot.  I have been able to give it a safe move to a position I can reach, turn of the motors, and move the arm to the correct position.



I tried a semiautomated approach where I put the arm in the right position, turn the current down to almost nothing, and tell the arm to move to the position that it is already at.  (I don't have to tell you this is a horrendous solution.)


---
**Arthur Wolf** *April 14, 2017 09:03*

You <b>really</b> want to have homing on this sort of machine. It's the same problem as on a delta really, it's going to be useless without homing ( and some calibration )


---
**Douglas Pearless** *April 14, 2017 09:10*

Hmm, my post didn't turn up :-( So lets see if the one does.  I have just finished writing (an hour ago) the ARM solution for this SCARA [thingiverse.com - FirePick SCARA by Snuffles](http://www.thingiverse.com/thing:1656504) which is using a similar principle.  I won't have a change to test the code until late next week and as Arthur points out you out have end-stops in order to home to a known position.


---
**Nicholas Seward** *April 14, 2017 13:39*

I have hard stops which are very easy to push the machine against which have insane repeatability.  Eventually I will use a board that will let me do current limited homing.  Until then it is faster and just as accurate to push the machine to the start position.  The question I have is how would I go about setting the arm position.


---
**Arthur Wolf** *April 14, 2017 14:32*

Smoothie essentially requires endstops, it won't be able to correctly do complex arm solutions without them, it's just not designed to work without them.


---
**Nicholas Seward** *April 14, 2017 14:36*

**+Arthur Wolf**​ Does it not seem reasonable to be able to set machine coordinates.  There is nothing magical about endstops that will make this printer work better or worse during a print.



I will hack something together.  I was just hoping that there was a feature I was overlooking.


---
**Arthur Wolf** *April 14, 2017 14:40*

**+Nicholas Seward** Allowing users to use gcode instead of homing to set machine position is opening the door to a ton of users completely mis-using the system, and making support people's life hell. This is why it's actually fairly common for systems to refuse to have ways to manually set machine coordinates, this isn't just a smoothie thing.

Endstops are just a few cents, and are trivial to install. 

If you really really don't want to install them, just <b>pretend</b> you did : configure them so they are always triggered, move the axes to the right position, issue G28 anyway and it'll think it has homed ...


---
**Nicholas Seward** *April 14, 2017 16:52*

**+Arthur Wolf** Just because something is easy and cheap isn't a reason for doing or not doing it.  Not including endstops was a deliberate decision.   There is a laundry list of reasons not to include them.  The number one reason is that they are an extra point of failure and replicate what can be done with missed step detection with fancier stepper drivers.  (Interesting side note: the absolute position of the arms only needs to be +/- a few degrees.  The only thing that needs to be bang on is the relative angle between the two arms.)



There are quite a few use cases for needing to switch back and forth from machine coordinates to other coordinate systems.  I don't see how that would make support for machines any worse than say allowing the switching between inches and mm.  I was trying codes like G53 but they don't seem to be fleshed out yet.  (At least the G92 didn't successfully set the coordinates in the selected coordinate system.)



That said...you are right.  I can just pretend I have limit switches.  That is very helpful.  Thank you!


---
**Arthur Wolf** *April 14, 2017 17:07*

As for switching between WCS and MCS, that is fully supported, we do have G53 etc, it's widely used ( as are it's other CS-related friends ). 

G92 doesn't set coordinates, it sets offsets, there is a lot going on there with a lot of possibilities, you probably want to read the related linuxcnc docs on the subject.

The only thing we don't support ( without a bit of trickery ) is <b>editing</b> MCS


---
**Nicholas Seward** *April 14, 2017 17:24*

**+Arthur Wolf**​. Ahhhh.  Everything is snapping into focus.  Thanks.



Last question:  what makes setting the MCS tricky?  I understand the reasons one would not want to allow this.  I am more interested in the technical hurdles.


---
**Arthur Wolf** *April 14, 2017 17:25*

Not sure there are that many technical hurdles, look at how Endstops.cpp does it and that's probably the right way.


---
**Wolfmanjm** *May 01, 2017 07:56*

Actually there is an undocumented  G28.3 X0 Y0 Z0 that manually homes the axis, effectively setting the MCS. But I agree with arthur, that is why it is undocumented.


---
**Dieter Kedrowitsch** *October 11, 2017 19:11*

Just an FYI but there were some recent rumblings on the Duet3d Forums in regards to implementing switch-less homing via Trinamic's "Stallguard" feature on the Duet controllers.  I'd keep an eye out for switch-less homing to appear in a beta soon... because it sounds like this would be just the ticket for your HELIOS.


---
**Arthur Wolf** *October 11, 2017 19:12*

The prusa i3 mk3 supports this, and I expect somebody will code this into Smoothie v2 soon after it comes out.


---
**Dieter Kedrowitsch** *October 11, 2017 19:15*

Yep, Trinamic's SPI configured drivers really open up a number of unique opportunities.  


---
**Wolfmanjm** *October 11, 2017 19:54*

I tested it on the advanced driver in smoothie, it is very inaccurate, never gets the same setting twice. if you use the Azteeg GT with the bigfoot drivers the pin is connected to the endstop I think already.


---
*Imported from [Google+](https://plus.google.com/+NicholasSeward/posts/WSq4pHNn5yb) &mdash; content and formatting may not be reliable*
