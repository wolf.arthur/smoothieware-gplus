---
layout: post
title: "I have recently got my BLTouch probe working but I would like to go one step further in automating the bed leveling compensation process"
date: April 14, 2017 23:33
category: "Help"
author: "Kraft King"
---
I have recently got my BLTouch probe working but I would like to go one step further in automating the bed leveling compensation process. I have set up a macro that will probe the bed and home, but I would like it to probe the bed, home, and then set the nozzle height. Basically, what I would like to do is do a probe at the current XY position and use the distance from the bed to set the nozzle height. I know I can tell the printer to probe at the current XY position and report back the distance but how can I take that reported distance and set the nozzle height automatically? Thanks for your time. 





**"Kraft King"**

---
---
**Chris Chatelain** *April 15, 2017 01:49*

G30 z2 to set the zero height 2mm lower than the probe activation point after probing for the bed. Note that it will also set the z offset so use the g92 gcode to clear offset before probing. If that's not clear enough, I'll look up my script later.


---
**Kraft King** *April 15, 2017 02:26*

**+Chris Chatelain** When I use the G30 command it reports the distance from the bed. If I add the Z parameter will it save the correct z zero height? 


---
**Arthur Wolf** *April 15, 2017 09:19*

You can also use G92 Z2 after the G30


---
**Griffin Paquette** *April 15, 2017 10:47*

Agreed with the other guys. Been using G92 offset for a while. 


---
**Kraft King** *April 15, 2017 21:26*

G30 with the Z parameter worked. Thanks for all your help and happy printing!


---
*Imported from [Google+](https://plus.google.com/114938032105833823625/posts/Yu6hPKoFoPY) &mdash; content and formatting may not be reliable*
