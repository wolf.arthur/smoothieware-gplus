---
layout: post
title: "I thought I should post an update since I have been asking you guys dumb questions"
date: April 15, 2017 00:01
category: "Development"
author: "Nicholas Seward"
---
I thought I should post an update since I have been asking you guys dumb questions.



<b>Originally shared by Nicholas Seward</b>





**"Nicholas Seward"**

---
---
**Arthur Wolf** *April 15, 2017 08:58*

Really really nice. Would love to see that documented on the smoothie wiki at some point.


---
**James Rivera** *April 15, 2017 19:38*

Damn. Great discussion of your design decisions. Adding more weight to the end to improve the print quality is counterintuitive, but makes sense when you describe it. I'd love to hear more about the sensing method for first layer auto-calibration. Great ratio of machine volume to build volume, too. Also, kudos for <i>continuing</i> to think outside of the box--I <b>love</b> your designs! Simpson, Morgan, Helios--wow!  I'm looking forward to seeing your updated version of this!


---
**Nicholas Seward** *April 15, 2017 19:40*

**+James Rivera** Wish I could take credit for Morgan but that was all the amazing **+Quentin Harley**.  If it wasn't for him then this design wouldn't have been a slam dunk.


---
**James Rivera** *April 15, 2017 20:08*

**+Nicholas Seward** Oops. I wondered if I might be misstating that when I hit post. Sorry **+Quentin Harley**, no slight intended! If anything, just the opposite!


---
*Imported from [Google+](https://plus.google.com/+NicholasSeward/posts/XZbrP6qf8as) &mdash; content and formatting may not be reliable*
