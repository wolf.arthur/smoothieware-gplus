---
layout: post
title: "Does the latest edge firmware-cnc.bin includes ABC axis support?"
date: April 17, 2017 20:51
category: "General discussion"
author: "Ariel Yahni (UniKpty)"
---
Does the latest edge firmware-cnc.bin includes ABC axis support?





**"Ariel Yahni (UniKpty)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2017 21:28*

Unless there is something specific about the latest build...



 You need to compile for 6 axis:

[smoothieware.org - 6axis [Smoothieware]](http://smoothieware.org/6axis)


---
**Ariel Yahni (UniKpty)** *April 17, 2017 21:49*

But this was merge , why the compile?


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2017 21:56*

The compile is because smoothieware .bin is compiled for 5 axis. So you need to compile for 6 axis if you want C. 


---
**Ariel Yahni (UniKpty)** *April 17, 2017 22:08*

I just want A


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2017 22:19*

Remove/ disable the extruders from your config and try to define an A axis per the docs linked above. 


---
**Ariel Yahni (UniKpty)** *April 17, 2017 22:26*

Yeah I just wanted to make sure it's there. Have a user wanting to test but it did not for him hence the question to confirm


---
**Jim Fong** *April 18, 2017 02:41*

**+Ray Kholodovsky** hi Ray. I'm the one trying to get rotary A axis working the the mini. 



I defined the A axis (delta) per the docs listed above and disabled the extruders.  Flashed the latest edge firmware cnc. Made sure the A axis pin assignments are correct in config.txt as follows...



Step 2.3

Dir 0.22

EN 0.21



I can get the motor to spin if I plug them into the Z axis so I know the driver/motor/wiring is ok.  If I move the driver/motor plug to the A axis. Can't get it to spin. The stepper motor never gets enabled, motor shaft doesn't lock. 



Before I lug down the Oscilloscope to check the board out, has anyone verified the latest firmware to actually work for A axis. 



The firmware works fine for x,y,z movements. No problem running the laser but can't get  A axis to respond. 






---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 03:01*

Derp. Quick way to rule out the board is redefine the Z pins to the A socket (and un-define A in config in the process). 


---
**Jim Fong** *April 18, 2017 03:23*

**+Ray Kholodovsky** why didn't I think of that. Duh.  Ok I reverted to a earlier config.txt without the ABC axis mods. Changed the Z pins to the A socket and got the motor to spin. So physically the board is fine, all 4 stepper sockets are good. 



So something is either wrong with my modified config.txt or latest firmware is broken.  


---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 15:10*

Yep, we test for this sort of thing before shipping boards :)



Going to need some help from the Wolfs on this one, please post a config file. 


---
**Jim Fong** *April 18, 2017 15:51*

The config,txt  I'm trying to get working for rotary A axis. Works fine XY lasering.  Using latest firmware-cnc.bin that is posted about 6 days ago.



[https://pastebin.com/dEpTAPGz](https://pastebin.com/dEpTAPGz)






---
**Jim Fong** *April 19, 2017 03:46*

I decided to download the source and rebuild for multiaxis.  It WORKS!!!  Rotary A axis moves when running gcode.  


---
**Ray Kholodovsky (Cohesion3D)** *April 19, 2017 03:52*

Ok so we do have to recompile.  Did you do make AXIS=4 CNC=1 ?


---
**Jim Fong** *April 19, 2017 03:57*

**+Ray Kholodovsky** make AXIS=4 CNC=1 is what I used. Didn't need the 5&6th. 



Only did a quick A axis gcode test to see the rotary move.  More testing is needed to see if any issues. 


---
**Ray Kholodovsky (Cohesion3D)** *April 19, 2017 03:58*

Yeah that's what I originally thought needed to be done. 


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 03:27*

**+Jim Fong** that's awesome!  FYI the video freezes around 30 seconds in, but still, the point was made and made well!  


---
**Jim Fong** *April 20, 2017 03:44*

**+Ray Kholodovsky** YouTube messed up video. Re-uploading again. 





Updated video



Laserweb4 rotary on a k40 laser running Smoothieware






{% include youtubePlayer.html id="UoGlDKjPGSE" %}
[https://youtu.be/UoGlDKjPGSE](https://youtu.be/UoGlDKjPGSE)



[https://imgur.com/a/HdMd7](https://imgur.com/a/HdMd7)


---
**Antonio Hernández** *March 13, 2018 19:28*

**+Jim Fong**  , Could be possible to know which postprocessor, or CAM software, did you use to generate gcode for 4th axis operation ?




---
**Jim Fong** *March 14, 2018 00:22*

**+Antonio Hernández** at the time, I used Laserweb4 to create the gcode. 


---
**Antonio Hernández** *March 14, 2018 01:26*

Ok ! great !!. It's good to know that. Did you try to move 4th axis using smoothie as cnc ?


---
**Jim Fong** *March 14, 2018 02:53*

**+Antonio Hernández** I only have Smoothie hooked up to a laser and not a CNC machine. Rotary should still work the same.  



You might also want to try out fusion360.  Light years a head of Laserweb4 for regular cnc control. It also does rotary. 


---
**Antonio Hernández** *March 14, 2018 04:33*

Ok, very good. I'll try Fusion360. Thanks for your info and time !.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/Gt6p7CwPNsm) &mdash; content and formatting may not be reliable*
