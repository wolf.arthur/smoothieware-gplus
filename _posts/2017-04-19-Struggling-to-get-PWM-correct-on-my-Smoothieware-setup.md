---
layout: post
title: "Struggling to get PWM correct on my Smoothieware setup"
date: April 19, 2017 17:08
category: "General discussion"
author: "Kelly Burns"
---
Struggling to get PWM correct on my Smoothieware setup.  Laser fires, but does not vary based on power.   I thought it was LaserWeb gcode, but that is not the case.  I can see the correct S Values in the G-Code and when I manually send G-Code to the machine, the power does not vary.   It is always whatever the Maximum setting is on the POT.   Maybe I simply have  block on how it should behave, but I don't think so. 



I noticed someone else posted with a PWM Freq setting of 400.  I am using the value of 20 that seems to be common and recommended.  



Thanks





**"Kelly Burns"**

---
---
**Kelly Burns** *April 19, 2017 17:53*

**+Peter van der Walt** Thanks. I may be a little confused on this. It is wired to from pin 2.5 to L, but not "inverted" that  unless done accidentally in ignorance.  




---
**Kelly Burns** *April 19, 2017 17:57*

I did linger there, but I don't like to post there since I didn't end up buying Ray's board.  Tried to, but I'm impatient ;)  I'll see what I can find out.  Thanks for the info. Don't believe I ever had the need to invert a variable output from any Processor/Controller.  




---
**Ray Kholodovsky (Cohesion3D)** *April 19, 2017 18:17*

All are welcome. 

Just hook up L to some mosfet, I use 2.5. With mosfet, invert ( ! ) not necessary. 


---
**Ray Kholodovsky (Cohesion3D)** *April 19, 2017 18:19*

Now I'm going to be beating myself up more over the inventory management and back order situation. Sad ray. 


---
**Kelly Burns** *April 19, 2017 19:13*

Thanks +Ray Kholodovsky (Cohesion3D)   I just try hard to only be a passive leach.  I have learned a lot from your Cohesion Community.  


---
**Kelly Burns** *April 19, 2017 19:28*

I couldn't resist trying so I ran to the basement to test.  I have a MKS Sbase v1.3 board that I had purchased for a future 3D Printer build.  Hell, I didn't even know it was a Smootheiware compatible controller until I fried the Smoothieboard and started looking through my past and future project storage when I couldn't get another board quickly.



When I inverted the output, it fired the laser as soon as I rebooted the controller. I changed it back to the way it was and it worked as before.  It turns out that I have it wired to PIN 2.4, but on the (-) post of that jumper.  It was firing the laser, but just not with PWM.  I left it that way and then changed the PWM frequency to 400, rebooted and tested with the LCD Panel Menu and its definitely working now.



THANKS.   


---
**Kelly Burns** *April 19, 2017 20:54*

**+Peter van der Walt** Sorry, but remember, it was a smoothie config question, not a wiring one.  So I didn't think pics were required.  Also, I didn't want anyone to have to support this Chinese controller ;)



It is wired the sane way it has been since I first installed the controller.  Going to the (-) of pin 2.4 output.  It's always possible there's something strange with controller.  



As I said, it seems to be working correctly with the Freq Change.  I'm about to perform all kinds of Laseweb tests.  








---
**Claudio Prezzi** *April 19, 2017 23:57*

It's absolutly correct to use the minus pin of 2.5 or 2.4 connector, because those are the pins connected to the mosfet. The plus pins are directly connected to the power source input. This way, no inversion is needed, because the mosfet pulls to ground when output is on.


---
**Claudio Prezzi** *April 20, 2017 00:03*

You can play a bit with the PWM period. On my K40, 400ms works good for grayscale images, but 50ms works better for depth map cutting (deeper with less burning).


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 00:05*

**+Claudio Prezzi** maybe you can use a macro and the smoothie config-set commands to easily switch pwm periods :)


---
**Don Kleinschnitz Jr.** *April 20, 2017 04:23*

This is all explained here. Although your controller may be different the control of the LPS is the same.



 [donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)



[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)




---
**Kelly Burns** *April 20, 2017 04:32*

**+Don Kleinschnitz** I thought I had read everything on your site, but may have missed the first link.   Thanks for detailed info.  Couldn't have gotten this far without you sharing that.  


---
**Claudio Prezzi** *April 20, 2017 07:09*

**+Don Kleinschnitz** Your documentation is very usefull, thank you for writing it down! 

You could probably write explicitly, that the L Pin has to be connected to the minus Pin of the P2.4 or 2.5 Output, so users of smoothie compatible boards also can find the right pin.


---
**Don Kleinschnitz Jr.** *April 20, 2017 12:29*

**+Claudio Prezzi** your right, that labeling was initially confusing to me before I read the schematics. They are sometimes also labeled VBB and grnd which makes it more confusing. I added new pictures and notes to the post. Thanks for pointing it out.

[donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)




---
**Claudio Prezzi** *April 20, 2017 12:37*

**+Don Kleinschnitz** Cool, your text makes it clear now.


---
**Kelly Burns** *April 20, 2017 13:21*

You guys are awesome.  I pay $160k/year for support of the system I own at work and I don't get this kind of response from them.  


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 14:40*

Yeah, I'd happily take an 80k job... Peter and I can split the 160 and subcontract out to Claudio out of our pockets :) 


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 14:57*

I already have that.  At least could be making a living wage doing it. 


---
**Kelly Burns** *April 20, 2017 14:57*

**+Peter van der Walt** not in healthcare IT. Vendors seem to do what they want and customer (me) is never right! 


---
**Kelly Burns** *April 20, 2017 14:59*

**+Peter van der Walt** speaking of that, where can I find donation links.  


---
**Don Kleinschnitz Jr.** *April 20, 2017 16:01*

**+Kelly Burns** mine is on my blog site lol! But do them first!


---
**Don Kleinschnitz Jr.** *April 20, 2017 16:03*

**+Ray Kholodovsky** , split 80K, wth so you three suck up all the funds .... lol. 


---
**Kelly Burns** *April 20, 2017 16:38*

**+Don Kleinschnitz** in due time I always support the contributors.  I believe in have donated to a couple of LW4 devs.  I have a few bookmarks.  When this project is over, I will pay my dues.  


---
**Don Kleinschnitz Jr.** *April 20, 2017 17:12*

**+Kelly Burns** Np just loosely place donation humor on my part!


---
**Kelly Burns** *April 20, 2017 18:52*

**+Don Kleinschnitz** it's funny.  I love that you put it out there on every page.  I want you get as many as possible.  It ensures the great info continues.  Content creators are joy and saving grace of my Maker existence. 


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/NZfXK2YCU5J) &mdash; content and formatting may not be reliable*
