---
layout: post
title: "I have incremental bed leveling mostly working in Smoothieware"
date: April 23, 2017 07:56
category: "General discussion"
author: "Nicholas Seward"
---
I have incremental bed leveling mostly working in Smoothieware.



<b>Originally shared by Nicholas Seward</b>





**"Nicholas Seward"**

---
---
**Arthur Wolf** *April 23, 2017 13:18*

Really nice :)


---
**Douglas Pearless** *April 23, 2017 20:53*

Excellent!!!


---
**Douglas Pearless** *April 23, 2017 20:53*

I look forward to reading the code updates you made to Smoothie, when might you be posting it to GitHub?


---
**Nicholas Seward** *April 23, 2017 20:55*

I will publish in this next week.  I will need someone that actually knows what they are doing to get ready for the main branch.


---
**Douglas Pearless** *April 23, 2017 21:13*

When you make your GitHub available I will have a (quick) look at it :-)


---
**Wolfmanjm** *May 01, 2017 07:44*

To be honest I really do not think we need yet another bed leveling method, so i recommend keeping it in a fork and we can list it in the third party branches. We do not have unlimited flash space unfortunately.


---
**Wolfmanjm** *May 01, 2017 07:47*

looks pretty much like the grid leveling that was recently added that allows you to set arbitrary rectangles to level.


---
**Nicholas Seward** *May 01, 2017 21:27*

**+Wolfmanjm** I don't really care if it is included or not but for the record it is a much more generic approach than arbitrary rectangle probing.  It uses about the same or less code to accomplish a more generic result.



You could do spiral probing which would be great for polar/delta printers.  You could do rectangle probing which is good for most printers.  You could do incremental probing for any odd shaped beds to avoid clips and other things.  My point is that probing every point in a grid is a waste of time and limits you to a rectangular probing area.  You could even do strange/useful things such as chain the probing of multiple rectangles and circles together to define a shape useful to you.  (You could have an option to turn off re-probing within a certain distance of an existing probe point.)



I would be glad to work with you guys to provide a generic (as backwards compatible as makes sense) leveling module.  I think making one module handle arbitrary rectangles, circles, and incremental probing would be worthwhile.  There is really no reason to have >3 point based leveling reside in multiple files (CartGridStrategy.cpp and DeltaGridStrategy.cpp and Morgan has yet another one). I am also happy just doing my own thing and scratching my own itch.


---
**Nicholas Seward** *May 01, 2017 21:49*

Here is some really gross code. [github.com - proto_RepRap_HELIOS](https://github.com/NicholasSeward/proto_RepRap_HELIOS/blob/master/Smootieware/src/modules/tools/zprobe/IncrementalStrategy.cpp)



It is only a proof of concept.  It currently, uses all the points for calculating your adjusted Z for every little move.  I have it provisioned to fill in a grid after every new probe but I have some little bug somewhere.  This way you could do quick bilinear interpolation.



Let me know if there is any interest in including this and I will correctly fork the repo and create some more thoughtful code.


---
*Imported from [Google+](https://plus.google.com/+NicholasSeward/posts/GMmb6AJZpxY) &mdash; content and formatting may not be reliable*
