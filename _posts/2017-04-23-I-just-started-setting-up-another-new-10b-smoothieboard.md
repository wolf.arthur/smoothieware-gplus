---
layout: post
title: "I just started setting up another new 1.0b smoothieboard"
date: April 23, 2017 04:46
category: "Help"
author: "Chris Chatelain"
---
I just started setting up another new 1.0b smoothieboard. With nothing plugged into it other than usb and my sd card, I managed to get the firmware to flash. It renamed the firmware file on my card to FIRMWARE.CUR so I'm pretty sure that's all good. When I power it on, I get LEDs 1 and 4 solid, and 2 and 3 flash for a few seconds, then either go solid on, or solid off, and the board doesn't respond. I thought it might be my config so I tried a fresh, unmodified config file from the example config, with no change. I do have a 5v regulator soldered on board, but I don't have my 24v power supply plugged in yet. I've also tried 3 different, formatted sd cards with the same result. I just tried flashing firmware again, and this time it didn't make it through the flashing light flashing process before locking up.



Any ideas what could be wrong?





**"Chris Chatelain"**

---
---
**Arthur Wolf** *April 23, 2017 13:22*

Where did you buy the board from ?


---
**Chris Chatelain** *April 23, 2017 16:35*

Uberclock, about June 2016. It's been in storage waiting for a project since.


---
**Arthur Wolf** *April 23, 2017 16:43*

Contact me at wolf.arthur@gmail.com, if everything you mentionned is correct, it's likely a bad board.


---
**Chris Chatelain** *April 23, 2017 19:35*

Actually, sorry to bother you. I figured it out. Silly me, I didn't check all of the single points of failure. My usb card reader was corrupting every sd card I tried to format with it. I formatted the card with a different card reader, and all is well.


---
**Arthur Wolf** *April 23, 2017 19:36*

awesome




---
*Imported from [Google+](https://plus.google.com/116517474949143363665/posts/BDArHnbhEVF) &mdash; content and formatting may not be reliable*
