---
layout: post
title: "VH-80: The World's First Bilateral Laser Distance Measurer Arthur Wolf We just need three for the hang printer and some smoothieboard running it of course ..."
date: April 25, 2017 12:47
category: "General discussion"
author: "David Bassetti"
---
VH-80: The World's First Bilateral Laser Distance Measurer

**+Arthur Wolf** 

We just need three for the hang printer  and some smoothieboard running it of course ... as this laser would really shorten the Calibration time.



Please can someone help us write the marlin to smoothie firmware we need to use the hangprinter without Ramps ?

Already Arthur has offered a smoothieboard 5xc to the person who can do it...  I can help as well just contact me 





**"David Bassetti"**

---
---
**Arthur Wolf** *April 25, 2017 12:51*

I don't think 3mm accuracy for the price it costs is a good fit for our uses. But this is good news for the future, at some point we'll have hardware cheap enough and accurate enough to use.

About the hangprinter you should do a separate post just for that :p


---
**Steve Anken** *April 25, 2017 18:36*

I wonder how good these boards are?

[aliexpress.com - 1PCS Smart car Obstacle avoidance Infrared module Reflective photoelectric](https://www.aliexpress.com/item/1PCS-Smart-car-Obstacle-avoidance-Infrared-module-Reflective-photoelectric/32327477526.html?spm=2114.10010108.1000010.4.W8ITa2&scm=1007.13438.37934.0&pvid=c8546cac-e3e9-4d7e-91a7-abb392f077be&tpp=1)


---
**Arthur Wolf** *April 25, 2017 19:22*

These are well known for having terrible resolution and repeatability. Useless for what we do.


---
**David Bassetti** *April 27, 2017 08:02*

Firstly we need smoothieboards to run the hangprinter firmeare :)




---
**CescoAiel** *May 03, 2017 19:06*

There was another (failed) kickstarter that would've given the desired resolution... I'll see if I can find it again... Maybe they've got other, similar products shipping?




---
*Imported from [Google+](https://plus.google.com/+DavidBassetti/posts/f2n8AKuGeLV) &mdash; content and formatting may not be reliable*
