---
layout: post
title: "Is there any gcode command to test the z-probe repeatability in the latest edge branch?"
date: April 27, 2017 10:14
category: "General discussion"
author: "Mircea Russu"
---
Is there any gcode command to test the z-probe repeatability in the latest edge branch? 

I would really like to trust my BLTouch but it seems even with calibration and deltagrid compensation I still can't get constant first layer height.

If there isn't such features do you know of any branch that has it?

Thank you.





**"Mircea Russu"**

---
---
**Chris Chatelain** *April 27, 2017 11:19*

I just made a gcode file with repeated g30 commands, moved the bed to about 5mm below the probe, and printed it. Then I got the output from the terminal and took the average.



My bltouch shipped with a bad magnet, I had terrible repeatability even though it's rigidly mounted (important!). After contacting support with my test numbers they replaced the probe and it works perfectly now. Their support is great, don't hesitate to email them.



I also noticed that it needs a lot of power, I couldn't just power it off of the endstop power pins and USB 5v. I needed to provide another 5v power into the 5v power jack and then run 5v to the bltouch right off that jack. 


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/1cY35qzEzKz) &mdash; content and formatting may not be reliable*
