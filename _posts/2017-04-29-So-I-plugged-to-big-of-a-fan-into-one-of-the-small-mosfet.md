---
layout: post
title: "So I plugged to big of a fan into one of the small mosfet"
date: April 29, 2017 02:13
category: "General discussion"
author: "jacob keller"
---
So I plugged to big of a fan into one of the small mosfet. Not sure what it is but it blew one of these​ thing's on the board. You can see in the picture I have it drawn in red. Does anyone now where I can get a replacement.



Thanks

![missing image](https://lh3.googleusercontent.com/-bgjebUkIviU/WQP2rtN1-3I/AAAAAAAAAuY/4DSTm1IVdzAUgyHbRZWucStVus-WlCBXgCJoC/s0/IMG_20170428_210642.jpg)



**"jacob keller"**

---
---
**Chris Chatelain** *April 29, 2017 02:43*

It is one of the small MOSFET themselves. My guess is you need to install a diode to protect them from the spike that comes frpm shutting the fan off


---
**Chris Chatelain** *April 29, 2017 02:44*

Should be able to just order one directly off of digikey, the model number is on the BOM on github. See the documentation on fans to see the diode you need to install.


---
**jacob keller** *April 29, 2017 02:54*

**+Chris Chatelain**​ thanks


---
**Arthur Wolf** *April 29, 2017 08:12*

You want [https://docs.google.com/spreadsheets/d/1vshB97WWt6Lceo59aggbdhRuXxTEhSL9lkotScR0oYE/edit#gid=0](https://docs.google.com/spreadsheets/d/1vshB97WWt6Lceo59aggbdhRuXxTEhSL9lkotScR0oYE/edit#gid=0)

[smoothieware.org - troubleshooting [Smoothieware]](http://smoothieware.org/troubleshooting#somebody-refused-to-help-me-because-my-board-is-a-mks-what-s-that-all-about)


---
**jacob keller** *April 29, 2017 15:13*

**+Arthur Wolf** thanks


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/gUVf9K2k3mj) &mdash; content and formatting may not be reliable*
