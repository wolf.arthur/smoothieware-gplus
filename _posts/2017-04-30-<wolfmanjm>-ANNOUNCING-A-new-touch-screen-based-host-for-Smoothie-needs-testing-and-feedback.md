---
layout: post
title: "<wolfmanjm> ANNOUNCING:- A new touch screen based host for Smoothie, needs testing and feedback"
date: April 30, 2017 20:39
category: "General discussion"
author: "Arthur Wolf"
---
<wolfmanjm> ANNOUNCING:-  A new touch screen based host for Smoothie, needs testing and feedback. Mostly usable but still a lot to do. Command input, jogging and extruder control currently works ok'ish 

[https://github.com/wolfmanjm/kivy-smoothie-host](https://github.com/wolfmanjm/kivy-smoothie-host)



Please help test !





**"Arthur Wolf"**

---
---
**Sébastien Plante** *April 30, 2017 23:52*

What if I want to use this with a CNC?


---
**Whosa whatsis** *May 01, 2017 00:05*

Pics or it didn't happen! (Seriously, I'd like to know what the interface looks like.)


---
**Wolfmanjm** *May 01, 2017 05:23*

load it onto your linux desktop and see :)


---
**Wolfmanjm** *May 01, 2017 07:37*

**+Whosa whatsis** I added some screenshots to the README, it is a work in progress, there will be a macro button page and a DRO page too. The setting of temps needs to be redone too.


---
**Wolfmanjm** *May 01, 2017 08:02*

**+Sébastien Plante** I will be adding a CNC specific version at some point. However this is targeted at an RPI with the official 7" touch screen. It is supposed to be a step up from the LCD panels we currently support, but maybe not as full featured as octoprint or pronterface. In my experience to use with CNC you need a much more sophisticated host like bCNC. So CNC will not be my primary target for this, as in my case I will always use bCNC as the host for my CNC (and laser) machines.


---
**Sébastien Plante** *May 01, 2017 11:09*

**+Wolfmanjm**​​ Make sense, thanks! :)


---
**Andrew Wade** *May 01, 2017 19:50*

I would be interested in helping test and design code for the touch screen


---
**Wolfmanjm** *May 01, 2017 23:54*

**+Andrew Wade** your help would be welcome, fork the github and submit pull requests if you feel inclined. I am not much of a UI designer, so I would also welcome any comments on improving the UI design. You really need to try it on a touch screen though to get a feeling for how it would work.


---
**Jérémie Tarot** *May 02, 2017 13:58*

Great work ! 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/ffWxCDHCWJV) &mdash; content and formatting may not be reliable*
