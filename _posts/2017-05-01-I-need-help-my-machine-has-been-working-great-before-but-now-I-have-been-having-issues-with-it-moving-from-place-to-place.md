---
layout: post
title: "I need help! my machine has been working great before but now I have been having issues with it moving from place to place"
date: May 01, 2017 04:36
category: "Help"
author: "Glen Scott"
---
I need help! my machine has been working great before but now I have been having issues with it moving from place to place. It seems to either stop or move extremely slowly and this happens randomly through both gcode and jogging. I notice it does it on any line or command and once it finishes it, albeit very slowly, it moves on normally.  I have tried it on Laserweb 4 and Visicut and have had the same thing happen in both. Any help on this would be greatly appreciated!





**"Glen Scott"**

---
---
**Douglas Pearless** *May 01, 2017 04:45*

Are the stepper driver chips getting too hot (thermal shutdown)?


---
**Chris Chatelain** *May 01, 2017 04:50*

Double check the wires and connectors for the motors. I had a similar thing g happen when one pin worked it's way loose from a connector block and another time when a wire started to come loose from the connector crimp.


---
**Glen Scott** *May 01, 2017 05:19*

I thought they might be getting hot too they get a little warm but cool enough to touch I do also have a fan blowing on it. I checked my connections and they're all tight as well.


---
**Glen Scott** *May 01, 2017 23:31*

I also noticed that the second LED Light stops flashing and goes out but all other lights are normal.


---
**Douglas Pearless** *May 01, 2017 23:33*

Have a look at this [http://smoothieware.org/troubleshooting](http://smoothieware.org/troubleshooting) <[http://smoothieware.org/troubleshooting](http://smoothieware.org/troubleshooting)>


---
**Glen Scott** *May 01, 2017 23:41*

I've seen that before and it did help but now its just the number 2 LED that goes out 3 flashes normally. Would that mean its something with just my config? or the firmware as well?


---
**Douglas Pearless** *May 01, 2017 23:53*

That is strange as LED 2 is the heartbeat LED and is turned on and off and on (etc) in the mainline so if it is not flashed, the Smoothie has got stuck somewhere deep in the code.



Does the pattern match the two “crashed” LED patterns?



Can you still communicate to Smoothie via you host?


---
**Glen Scott** *May 02, 2017 00:05*

After the machine stopped only the number 2 stopped flashing while the number 3 still flashed. the only command that gets through is the abort from laserweb and when I do that it then crashes the firmware. Its strangely inconsistent though because I can run a file with no problems then I can run the same file immediately after and it stops.


---
**Douglas Pearless** *May 02, 2017 00:17*

That is extremely odd.



The LED patterns indicate Smoothie is stuck somewhere in its firmware.



LED 2 is the main loop heartbeat (which calls the main_loop and on_idle “processes") and LED 3 is the Slowticker which is used for monitoring temperature, end-stops, etc and that is alive.



If could be you have discovered a weird G-Code combination that crashes Smoothie, or perhaps the firmware is corrupted, or even there has been a spike on the power supply and caused a brown out.



Can you reflash to the latest Smoothie firmware from github?



Does it reliably crash at a particular place in the same job?



Do you have the Watch dog timer enabled in your config?  This is ‘reset’ during the “on_idle” process so if you have it on, then smoothie should “crash” given your scenario and go into debug mode where you can follow the instructions [http://smoothieware.org/mri-debugging](http://smoothieware.org/mri-debugging) <[http://smoothieware.org/mri-debugging](http://smoothieware.org/mri-debugging)> and upload a dump and someone on the Smoothie core team can try to work out why the firmware crashed.



Cheers

Douglas


---
**Glen Scott** *May 02, 2017 01:49*

I have reflashed the firmware several times but nothing seemed to have changed. There are a few spots it hangs up on some times but more often than not it happens randomly on any line even when jogging it too. And the Watch dog timer was at 10 and i changed it to 0 but there was no difference.


---
**Douglas Pearless** *May 02, 2017 02:03*

I am beginning to suspect and electrical issue that is upsetting the hardware.  



Can you post a wiring diagram, e.g. do you have any items (laser etc) that use a lot of power, or large motors and what is the capacity of the power supply?


---
**Glen Scott** *May 02, 2017 02:19*

My system is fairly simple I have 3 nema 17 motors 1.7 amps each plugged into the smoothie board with a 12-24V 4.5A power supply. The laser is on its own separate power supply


---
**Douglas Pearless** *May 02, 2017 02:27*

Is it possible to try it without using the laser and it's power supply?



Sent from my iPhone


---
**Glen Scott** *May 02, 2017 02:30*

I have tested it with the laser power supply on and off with the same results.


---
**Glen Scott** *May 03, 2017 04:48*

I've read that one thing that caused smoothie board to freeze was long ribbon cables going to a display [http://forum.smoothieware.org/forum/t-1692885/smoothieboard-freezes-after-1-hour-of-printing](http://forum.smoothieware.org/forum/t-1692885/smoothieboard-freezes-after-1-hour-of-printing). I don't have a display but could my problem be similar? i.e. too long power supply wires or usb cable?

[forum.smoothieware.org - Smoothieboard freezes after 1 hour of printing - Smoothie Project](http://forum.smoothieware.org/forum/t-1692885/smoothieboard-freezes-after-1-hour-of-printing)


---
**Douglas Pearless** *May 03, 2017 04:53*

That is a good point, I have heard of and seen issues with cheap USB cables that were not properly shielded; do you have a good quality cable you can try?


---
**Glen Scott** *May 03, 2017 05:03*

I have been using one from a printer that was a bit old so that could be it but also what is the likelihood of the wires crossing each other being the cause?


---
**Douglas Pearless** *May 03, 2017 05:07*

Noise and RFI can affect lines, I would start with a new quality USB cable, also separate and route power and high current wires away from data lines.


---
**Douglas Pearless** *May 03, 2017 05:22*

And lastly look for ground loops, I.e where something is earthed or grounded in more than one place, ideally they should have a "star" topology where they all connect to a single place, or failing that a "tree" so there are no loops of that makes sense?


---
**Glen Scott** *May 06, 2017 22:43*

I used a new shielded USB cable and I have all the cables as far away from each other as I can get them and yet it still hangs up at random areas. I'm absolutely puzzled.


---
**Douglas Pearless** *May 06, 2017 23:20*

Ok, can you completely disconnect the LCD board and only run across the USB so that can be eliminated 


---
**Glen Scott** *May 06, 2017 23:27*

Oh I'm not running an LCD just the usb to the computer.


---
**Douglas Pearless** *May 07, 2017 00:42*

Hmm, might be time to go back to first principles.  Can you remove all wiring except the USB cable so that the Smoothie is powered from just the USB cable. You may need to leave the endstop wiring if you need to simulate a homing sequence.  Then can you try a dry run if your jobs.  This will reduce the variables so we can isolate the cause.  If that works OK, then add just the power supply and retry, again if that works and then one stepper motor at a time.


---
**Glen Scott** *May 10, 2017 03:25*

Alright I finally was able to test it and it still happens without anything else plugged into it. I've tried it with one motor, no motors, and then no power apart from the usb to power the board. The rate of movement displayed on laserweb was the same as when they were all plugged in.


---
*Imported from [Google+](https://plus.google.com/100338077477204259797/posts/gJWpkUaSUKE) &mdash; content and formatting may not be reliable*
