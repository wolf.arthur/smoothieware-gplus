---
layout: post
title: "Weird question, but I cannot seem to find the answer (either it's not there, or my searches are just not returning what I am looking for) to 2 simple questions..."
date: May 03, 2017 18:43
category: "General discussion"
author: "CescoAiel"
---
Weird question, but I cannot seem to find the answer (either it's not there, or my searches are just not returning what I am looking for) to 2 simple questions...



Can I drive multiple pins in a single configuration block? (Say I want to drive 2 separate power supplies with an M80/M81 command pair, can I assign 2 pins to it)

If the above is not possible, can I use the same command in 2 configuration blocks to have both trigger at the same command?



Assuming a block similar to this:

# Switch module for reset control

switch.reset.enable                            true             # Enable this module

switch.reset.input_on_command                  M80              # This switch is turned on when M80 is sent

switch.reset.input_off_command                 M81              # This switch is turned off when M81 is sent

switch.reset.output_pin                        0.25!^           # This pin is turned on when this switch is turned on, and vice-versa

switch.reset.output_type                       digital          # PWM output settable with S parameter in the input_on_comand



Could I add a 2nd ping to the output_pin line, or add a 2nd block identical to this, with a different output_pin value and have both trigger with the M80/M81 commands?





**"CescoAiel"**

---
---
**Jorge Robles** *May 03, 2017 18:54*

What about a 50cts relay? :)


---
**CescoAiel** *May 03, 2017 19:00*

**+Jorge Robles**, of course you'd be the first one to respond to my question!  <VBG>



Yes, that'd be possible, but then I'd need to place another relay somewhere... And where's the fun in that?

I have plenty pins left on my smoothie, so if neither is possible, I already have a feasible workaround to send two commands... 

It'd just be more elegant if either of the two above methods would be possible...


---
**Jorge Robles** *May 03, 2017 19:08*

Hmm have no spare smoothie to test. I use a relay for non-atx psu, so was no so stray shot :)


---
**CescoAiel** *May 03, 2017 19:09*

I'm not ragging on you... Just want to know... If noone has an answer, I'll probably end up testing this on my FABkit smoothie on some spare pins with a random command...  ;)




---
**Jeremiah Coley** *May 03, 2017 19:14*

Great question, I'm a smoothie no-one so no answers here but defenatly interested in the answer.


---
**CescoAiel** *May 03, 2017 22:00*

Of course ill share the results! ;)


---
**Douglas Pearless** *May 03, 2017 22:01*

In ready through the source code for Smoothie I note that it does not appear to support more than one pin definition, so, no to your first question.  I have not had time to read the g-code interpreter to see if the second one will work; suggest you try it an see :-)


---
**Chris Chatelain** *May 03, 2017 22:09*

That's the kind of thing I use one or more SSR's for, driven off the same pin.



Or just define an additional switch with an additional unused mcode, and just run both from your gcode script


---
**Wolfmanjm** *May 05, 2017 04:59*

You can have two switch commands driving two different pins but with the same M code. So M80 would turn on two different pins at the same time. you Cannot have one pin used twice in two different switches, or specify two pins in one switch. Although if you were to define one pin twice in config it may work for now but is not guaranteed to work in the future.


---
*Imported from [Google+](https://plus.google.com/111350720796378027645/posts/TCokn5e5dh2) &mdash; content and formatting may not be reliable*
