---
layout: post
title: "When suspending a print with gCode \"M600\" or \"suspend\" command for filament change, the standard-setting turns off the heaters"
date: May 05, 2017 19:58
category: "Help"
author: "Ren\u00e9 Jurack"
---
When suspending a print with gCode "M600" or "suspend" command for filament change, the standard-setting turns off the heaters.

Documentation for suspend-command says "Heaters: Turned off if option enabled (default)"

But I didn't find any information about keeping the heaters on. What do I have to do to let the heaters continue to heat during this pause-modus?





**"Ren\u00e9 Jurack"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2017 22:58*

There's an option for heaters on during disable. 


---
**René Jurack** *May 06, 2017 06:18*

Yeah, that is what I already wrote... 


---
**David Martin** *May 06, 2017 07:13*

leave_heaters_on_suspend is a config option


---
**René Jurack** *May 06, 2017 07:38*

Thx for the info


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/jLDXpF6jGCi) &mdash; content and formatting may not be reliable*
