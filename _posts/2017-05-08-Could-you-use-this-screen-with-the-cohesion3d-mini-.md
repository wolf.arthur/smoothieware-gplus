---
layout: post
title: "Could you use this screen with the cohesion3d mini? "
date: May 08, 2017 08:41
category: "General discussion"
author: "Colin Rowe"
---
Could you use this screen with the cohesion3d mini?

[http://www.3dpmav.com/product/3dprinter3-2-inch-full-color-touch-screenmks-tft32/](http://www.3dpmav.com/product/3dprinter3-2-inch-full-color-touch-screenmks-tft32/)﻿







**"Colin Rowe"**

---
---
**Eric Lien** *May 08, 2017 10:36*

Yes. It has it's own processor and firmware. It just sends codes and commands over serial between the Printer Controller Board  and the LCD. Just connect Tx, Rx, 5V, and Gnd.


---
**Eric Lien** *May 08, 2017 12:31*

And you can slightly modify the interface with custom codes, and custom button images:



[https://github.com/makerbase-mks/MKS-TFT](https://github.com/makerbase-mks/MKS-TFT)



[http://forum.smoothieware.org/forum/t-1975072/new-gui-touchscreen-for-smoothieboard](http://forum.smoothieware.org/forum/t-1975072/new-gui-touchscreen-for-smoothieboard)


---
**Christian Lossendiere** *May 16, 2017 20:53*

So for connect MKS TFT32 to original smoothieboard is like this picture ? right



Tx of display to Rx of smoothieboard

Rx of display to Tx of smoothieboard

5V to 5V and Grnd to Grnd 



Someone can confirm ?



![missing image](https://lh3.googleusercontent.com/Myj_ub5dn05IC52Vq2_WNQBGPLGcEMO46e3qiX8JIQN4RMEW3yePCyutMBV4Sjel3mZ1n91AaNJrF348yNZtI5l0wIWuSeyVHBY=s0)


---
**Christian Lossendiere** *May 16, 2017 20:55*

Need 4 pin in bottom of Aux-1

Tx Rx Gnv 5V



But why Aux-1 have 8pin

The 4 pin in top is for what ?

No need to use ?

![missing image](https://lh3.googleusercontent.com/5vdSW-lPPz9VLQjXigA68wHtIiNj-H7mDFpKAuWkf5-nNG-LdOtXoFv84uU2Rpb90vTWyXvpHBGk7YsQTABL8K2xN1hmrNH_ZGM=s0)


---
**Eric Lien** *May 16, 2017 22:22*

Correct, you don't need to use anything else except TX RX 5V GND for smoothie.


---
**Christian Lossendiere** *May 17, 2017 05:17*

Thanks Eric




---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/55eZaE3kujW) &mdash; content and formatting may not be reliable*
