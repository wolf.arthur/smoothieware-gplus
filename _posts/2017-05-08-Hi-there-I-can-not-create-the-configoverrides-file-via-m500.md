---
layout: post
title: "Hi there, I can not create the config.overrides file via m500"
date: May 08, 2017 17:45
category: "General discussion"
author: "\u00c5bs\u00f8lem"
---
Hi there,

I can not create the config.overrides file via m500.

After reading it appears that you have to eject the device.

But this one persists in remaining in the tree.

Do you have a solution so that once ejected, it goes one time for all to be able to use the command M500?

I am under the latest version of Windows 10.





**"\u00c5bs\u00f8lem"**

---
---
**Arthur Wolf** *May 09, 2017 07:27*

you must eject, or the card may become corrupted, however M500 should provide an override even without ejecting.

Please try formatting your sd card


---
**Åbsølem** *May 11, 2017 08:57*

Hi **+Arthur Wolf** ,

Why do not I get a G32, I have this message: No strategy found to handle G32.

I nevertheless followed the doc scrupulously.


---
**Åbsølem** *May 11, 2017 11:54*

**+Arthur Wolf**  and my card is not corrupted because into Octoprint the M500 command works and the file config.override is well created.

So the fault comes back to Windows 10




---
**Greg Nutt** *May 11, 2017 15:20*

**+Absolem** Here are my relevant entries in my startup script.  You will obviously need to adapt it for your own printer. 



M280 S3.0  # Drop BLTouch pin

G31              #  Auto bed level rectangular grid

G28             # Home X/Y

G30             #  Home Z

G92 X0 Y0  #  Set X/Y absolute position

G30 Z1.48  #  Set Z home position with BLTouch offset

M280 S7.5 # Retract BLTouch pin

G92 Z1.48 #  Set Z absolute position with BLTouch offset




---
**Åbsølem** *May 11, 2017 15:31*

Thank you very much, I had already made it work with the delta grid at the time, but this rectangular does not suit me because we can not specify the delta grid, it does not probe it or I want it does not take into consideration the offsets In short for me the cup is full, I stop wanting to run this probe with smoothie, it will go on my Rumba board when I would go up on it. Brief smoothie there is still work to compete with Marlin I now fully convinced. For the time being I will return to a classic use and in the best case my card turning under smoothie will be resold.




---
**Greg Nutt** *May 11, 2017 15:55*

Smoothieboard is a compliant G-CODE controller board and as such requires specific G-CODE programming.  Marlin has been highly customized for the easier use of the masses but that introduces its own issues.  The Smoothieboard is a very powerfully customizable board with a fantastic firmware interface, and just requires taking time to become familiar with the interface.  It is far superior to any Arduino/RAMPS configuration.


---
**Åbsølem** *May 11, 2017 16:43*

I am well aware of the benefits produced by smoothie, but for my part it does not satisfy me. I just wanted to use my bltouch, arthur says it's the best card and firmware in the world, I do not agree. In any case it is not so for printing3d. It's a real hassle. I prefer to go back by the compilation arduino and to pass of the 32 bits that to make me shit to find a real solution to be able to make my self-leveling with my bltouch.

easier use of the masses It is especially when you sell your product as the best.


---
**Greg Nutt** *May 11, 2017 17:37*

Again, I emphasize you are only speaking from a lack of familiarity with how Smoothieboard works.  It is a superior product.  It just so happens that your superior experience is with Marlin and is limited with Smoothieboard.  That does not make Smoothieboard an inferior product.


---
**Wolfmanjm** *May 12, 2017 05:33*

Yea basically you  misunderstand how to use smoothie, and based on your lack of knowledge you say Marlin is better, i think thousands of users who do understand smoothie would disagree with you. I may be biased but IMO smoothie is far superior to Marlin in virtually every function. You could easily get this to work if you really understood what you were doing with smoothie.


---
*Imported from [Google+](https://plus.google.com/111776692046871835463/posts/HwBcjWjdCh2) &mdash; content and formatting may not be reliable*
