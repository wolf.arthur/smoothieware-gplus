---
layout: post
title: "When running a job straight from the SD card, there's an elapsed time displayed on the LCD, however as soon as the job ends, that time gets reset and disappears"
date: May 08, 2017 04:50
category: "General discussion"
author: "Ashley M. Kirchner [Norym]"
---
When running a job straight from the SD card, there's an elapsed time displayed on the LCD, however as soon as the job ends, that time gets reset and disappears. I like to keep track of how long a job takes however I'm not always at the machine when the job ends. Is there a way to keep that time displayed and only reset it when the next job starts, and/or till I trigger a homing command?





**"Ashley M. Kirchner [Norym]"**

---
---
**Wolfmanjm** *May 09, 2017 04:33*

no sorry. if you can program  you could try to change it :)


---
**Ashley M. Kirchner [Norym]** *May 09, 2017 05:08*

Hah! Ok. Won't promise anything as I already have several other projects going on, but I'll grab the source and start poking.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/LusjLaWnXYj) &mdash; content and formatting may not be reliable*
