---
layout: post
title: "Originally shared by Donny Hilsenrath Just installed the Cohesion board in the k40"
date: May 12, 2017 10:51
category: "Help"
author: "Donny Hilsenrath"
---
<b>Originally shared by Donny Hilsenrath</b>



Just installed the Cohesion board in the k40. Works great! Only issue I'm having is I'm getting a very loud noise on the X axis while engraving. While jogging around and manually moving there is no noise and it glides smoothly. Belt tension is tight as well. I'm assuming it's a setting in LaserWeb?

!![images/6e72d1c15368ed920a21959f5e2ba365.jpeg](images/6e72d1c15368ed920a21959f5e2ba365.jpeg)



**"Donny Hilsenrath"**

---


---
*Imported from [Google+](https://plus.google.com/116233537121721630298/posts/2vqNCvQB9Es) &mdash; content and formatting may not be reliable*
