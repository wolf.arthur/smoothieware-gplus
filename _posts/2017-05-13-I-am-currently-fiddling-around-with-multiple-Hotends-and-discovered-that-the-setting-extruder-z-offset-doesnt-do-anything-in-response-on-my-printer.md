---
layout: post
title: "I am currently fiddling around with multiple Hotends and discovered, that the setting \"extruder z-offset\" doesn't do anything in response on my printer"
date: May 13, 2017 12:16
category: "Help"
author: "Ren\u00e9 Jurack"
---
I am currently fiddling around with multiple Hotends and discovered, that the setting "extruder z-offset" doesn't do anything in response on my printer.



For example, neither:

extruder.EXT1.z_offset 1

nor:

extruder.EXT1.z_offset 50



does anything to my printjob, it just prints as if the setting isn't even there.

Do I have to take something additional into account for using extruder z-offsets?









**"Ren\u00e9 Jurack"**

---
---
**Arthur Wolf** *May 13, 2017 12:33*

Extruder height isn't set via z_offset but relative to the axis' end, see [smoothieware.org - gamma_max [Smoothieware]](http://smoothieware.org/gamma_max) for several methods for doing so.


---
**René Jurack** *May 13, 2017 12:40*

But I don't want to set a home-position, I want to set an <b>offset</b> to it. I already have my first extruder (EXT0) set perfectly in height. But EXT1 has a different height at home-position than EXT0, lets say EXT1 is 1mm higher than EXT0. Now I have to tell smoothieware, that EXT1 needs to get 1mm closer to the bed than EXT0. "extruder.EXT1.z_offset -1" looks like the way to go, but it doesn't do anything in response.


---
**René Jurack** *May 13, 2017 13:23*

I renamed the extruder modules to T0/T1 etc to maybe match the tool identifier. Didn't help. A bit strange: I can't find a single result with google that ANYONE is even using the extruder z_offset setting at all. Does it even work in general? Or do I have to blame my google-skills?


---
**René Jurack** *May 13, 2017 14:02*

Though I am not that good at coding, I took a look into smoothie-source and it seems, that the z_offset is at least programmed in at every place where x&y offsets are mentioned. But I am not even close to sure, that I checked this correctly.

Additionally, I tried using the gCode-command "G10" (tool offset), because I saw it mentioned in the source. Didn't work either :(




---
**René Jurack** *May 13, 2017 14:13*

I changed X&Y offsets of each of mine extruders like I did with the Z-offset before. They all work. I tried both, positive and negative values. Every time I get a different position, according to my set offsets. Only Z-offset doesn't work. Because of working X&Y-offsets, I can probably rule out user-error. Shall I open an issue thread​ at GitHub about this, **+Arthur Wolf**?


---
**Arthur Wolf** *May 13, 2017 15:06*

z_offset means nothing to smoothie, it's fully ignored. you are pretty much the first user in 5 years to have hotends that are not at the same z height ( that I know of ) if that's really what's going on here. is it ?


---
**René Jurack** *May 13, 2017 15:23*

But I am definitively not the first having problems with this. But for smoothiware "fully ignoring" this, the command z_offset is mentioned everywhere with X&Y offset in the source-code ;) It should be easy to activate this if it is already written in, or not?


---
**Arthur Wolf** *May 13, 2017 15:25*

**+René Jurack** So far users who mention z_offset are actually users trying to do what is described on the gamma_max page ( which is why I pointed you to it initially ), I really don't know I've seen a user with different z positions for different hotends before, you are at the bleeding edge here.

I don't think anything except the setting is written in the code for this.


---
**Ray Kholodovsky (Cohesion3D)** *May 13, 2017 15:37*

Will G10 do what you want? 



[forums.reprap.org - Dual Extrusion Offset](http://forums.reprap.org/read.php?263,268033)


---
**René Jurack** *May 13, 2017 15:40*

I already tried G10, doesn't do anything for Z, too.


---
**René Jurack** *May 13, 2017 15:42*

I just mentioned a bounty at github, maybe someone gets motivated by it. And if not, it fixes a bug for a good firmware that I use daily. win win :D


---
**Ray Kholodovsky (Cohesion3D)** *May 13, 2017 15:42*

How about M218?


---
**Ray Kholodovsky (Cohesion3D)** *May 13, 2017 15:43*

Might be XY only though. 


---
**René Jurack** *May 13, 2017 15:45*

I looked through the gcode/mcodes, and tried G10 and M206. Both didn't work with Z on smoothie like I want them to.


---
**René Jurack** *May 13, 2017 15:47*

The thing is, if you look for "x_offset" or "y_offset" in the sourcecode, everytime the parameter "z_offset" is mentioned, too. With code, math etc... I think, it is just left out in one of the last calculations...


---
**Chris Chatelain** *May 13, 2017 17:12*

Just thought I'd mention that if the nozzles are fixed in relation to each other, when the higher nozzle is used, the lower nozzle will drag through the print and mess it up. For dual extrusion I've always had to set them the same. 


---
**Chris Chatelain** *May 13, 2017 17:14*

Also you might be able to get around it by using a tool change script that puts the machine into relative mode, use g92 to shift the offset, and put it back into absolute mode 


---
**René Jurack** *May 13, 2017 17:22*

No, sadly not. Fixing this through a toolchange script does only work for the layer where the toolchange happened.. The following layer has an absolute Z-height and gets squished or stretched. 

Different heights of the toolheads is no problem with my setup.


---
**Greg Nutt** *May 14, 2017 12:24*

Obviously you would need to do the tool change script to shift the offset back using G92 every time you also switch back to the first head too.


---
**René Jurack** *May 14, 2017 12:26*

**+Greg Nutt** I already opened an issue on github, bc it is a little bit more complicated than that ;)


---
**Wolfmanjm** *May 17, 2017 05:40*

FWIW this does in fact work but the toolchange height for Z does not take effect until you actually move the z axis. This is a side effect of treating the toolchange height the same as workspace coodinate changes.  SO in the end it works it just won;t show the height change if you just move X or Y. This may or may not be the correct way to do it. But as extruders are not covered by the NIST gcode spec who is to say what is right or wrong :)

To make this height change immediately on T1 would violate the NIST spec as M6 is what is supposed to do the actual toolchange. so a compromise needs to be found. The OP found a workaround by adding a G0 Znnn in the toolchange script, where nnn is the current z height..


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/WtrLwUKnQGt) &mdash; content and formatting may not be reliable*
