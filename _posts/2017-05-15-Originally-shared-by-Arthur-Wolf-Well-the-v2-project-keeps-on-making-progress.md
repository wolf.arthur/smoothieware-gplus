---
layout: post
title: "Originally shared by Arthur Wolf Well the v2 project keeps on making progress"
date: May 15, 2017 20:57
category: "Development"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



Well the v2 project keeps on making progress. 



The firmware is very close to having the right base to be built upon ( as in : NuttX has finally mostly been beat up into the shape we need ), 



and the first ( <b>PRE-ALPHA</b> ) v2-mini protos have just been produced, they'll be used mostly only to confirm the basic design decisions for the series, and are not expected to print/cut anything for several months still.



Exciting times :)



![missing image](https://lh3.googleusercontent.com/-jtkZaOlL9xE/WRoWOCR4dlI/AAAAAAAAPB4/HbQIk1KghmUMQUeSPdmgt9NHEOX31ObcgCJoC/s0/0511_2.jpg)
![missing image](https://lh3.googleusercontent.com/-v4nbnh5IaTI/WRoWOBSo_bI/AAAAAAAAPB4/uQexVFPrsso631iQ_84Q6KlcLojUAlnagCJoC/s0/0511_3.jpg)
![missing image](https://lh3.googleusercontent.com/-cvcFYPoopSA/WRoWOGJ_XXI/AAAAAAAAPB4/_cyo-EpolBYvmKc9m1ymkqZieatgw8WGwCJoC/s0/0511_1.jpg)

**"Arthur Wolf"**

---
---
**Stephen Baird** *May 15, 2017 21:08*

It's so cute.


---
**Arthur Wolf** *May 15, 2017 21:11*

**+Stephen Baird** It <b>is</b> ! :) And if we do everything we want to do succesfully, it'll probably be at a price that will shut up the "I'd buy Open-Source buy China is less expensive" people.


---
**Stephen Baird** *May 15, 2017 21:15*

Seriously exciting times ahead, then. I look forward to seeing it completed.


---
**Anthony Bolgar** *May 15, 2017 21:22*

Looking forward to the new version. Keep up the great work!


---
**Don Kleinschnitz Jr.** *May 15, 2017 21:27*

Are there any new firmware/hardware laser features like analog power control, variable pwm period, temp probe, interlocks etc.?


---
**Arthur Wolf** *May 15, 2017 21:29*

There will be a ton of new features, but they will be rolled out progressively. v2 is all about having the room and power to implement those. Right now we are concentrating on getting v2 to the point v1 is at now.


---
**Don Kleinschnitz Jr.** *May 15, 2017 22:22*

**+Arthur Wolf** I'm mostly thinking of analog output for power control. 


---
**Arthur Wolf** *May 15, 2017 22:41*

**+Don Kleinschnitz** Actually, v1 already has this, it's just commented out because nobody ever tested it. It's been there for two years, it's possible the line was deleted since in a clean-up, but it's really a 3-lines change. It's just not activated because nobody ever tested it and we nearly never get requests for it.


---
**Douglas Pearless** *May 16, 2017 00:00*

Looking great!!!!


---
**Artem Grunichev** *May 16, 2017 06:20*

Thank you for news, so we don't need to ping for it ;)


---
**Don Kleinschnitz Jr.** *May 16, 2017 11:47*

**+Arthur Wolf** whats the best way to get more info this analog power control and how to use it?

+Wolfmanjm  would this be a better approach than the PWM to analog conversion circuit we are working on???




---
**Arthur Wolf** *May 16, 2017 11:48*

**+Don Kleinschnitz** I just looked and it's not in there anymore, so not really sure.


---
**Don Kleinschnitz Jr.** *May 16, 2017 11:59*

**+Wolfmanjm** would this be a better approach than the PWM to analog conversion circuit we are working on???



G+ at it again not sure if the +Wolfmanjm  link is active?


---
**Thomas Herrmann** *May 16, 2017 13:01*

That one huge elko is smoothing out the stepperdriver supply?


---
**Arthur Wolf** *May 16, 2017 13:06*

**+Thomas Herrmann** think it's smoothing all of it.


---
**Thomas Herrmann** *May 16, 2017 13:35*

**+Arthur Wolf** Smooth, solidcap was unpractical? Should improve productlife #noexpert :P


---
**Arthur Wolf** *May 16, 2017 13:36*

**+Thomas Herrmann** No idea :) Will ask.


---
**Wolfmanjm** *May 16, 2017 17:41*

**+Don Kleinschnitz** i am not sure exactly what arthur is referring to. the only analogue i am aware  of is the vfd adapter.


---
**Arthur Wolf** *May 16, 2017 17:49*

I'm saying changing the laser module from pwm to dac is only a few lines changes ( the pwm and dac objects are very similar ) and years ago somebody did just that ( it was in the code for a while commented ... or possibly in a branch ) but nobody ever actually tested it.


---
**Wolfmanjm** *May 16, 2017 18:00*

I think there is an external circuit still required for DAC to get 0-5v




---
**Don Kleinschnitz Jr.** *May 16, 2017 19:11*

**+Wolfmanjm** is the processor dac not run whatever reference voltage you set up (travelling so I didn't look up the processor)? 

I was encouraging the add of a 0-5 dac or pwm-analog circuit on V2. 


---
**Wolfmanjm** *May 16, 2017 19:20*

the azteeg GT Pro has a vfd on board, so maybe one wil be added to smoothie v2. As for the LPC 1769 I think if the DAC works it will be 0-3.3v, it would need some circuit to boost it to 0-5v




---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/i47Efo58SHk) &mdash; content and formatting may not be reliable*
