---
layout: post
title: "Weird issue, I tried activating Z lift, and suddenly my prints looked underextruded and were stretched vertically"
date: May 15, 2017 21:47
category: "Help"
author: "Whosa whatsis"
---
Weird issue, I tried activating Z lift, and suddenly my prints looked underextruded and were stretched vertically. It looks like it is lifting on a G10, but not dropping back down on a G11. Anyone seen this before?





**"Whosa whatsis"**

---
---
**Miguel Sánchez** *May 16, 2017 06:46*

it seems as if you are missing steps on Z axis. Maybe too much acceleration or too high Z speed? (not going down enough). I have not used z-lift on smoothie though, so I do not know if it could be firmware related.


---
**Whosa whatsis** *May 16, 2017 07:41*

It's possible, but I didn't hear any skipping, and this would mean that it's skipping going down, but going up. The opposite seems more likely.


---
**jacob keller** *May 16, 2017 23:47*

**+Whosa whatsis** did you calibrate the z axis? I had the same problem with my printer where the z axis would go down to far on each layer making it stretch on the vertical plane. And when you calibrate the z axis I would do X and Y at the same time.  I recommend this file on thingiverse and I would following his instructions for the calibration.



[thingiverse.com - STEP Calibration Piece (X, Y and Z axis) by voltivo](http://www.thingiverse.com/thing:195604)


---
**Whosa whatsis** *May 17, 2017 01:40*

Yes, I correctly calibrated my Z axis. This problem only happened when I activated Z lift on retraction.


---
**Wolfmanjm** *May 17, 2017 05:44*

FWIW I've used zlift for years and never had an issue. Send me the gcode file so I can check it isn't doing something bad between the G10 and G11, like slic3r used to.  BTW slic3r now has a smoothie flavor which fixes that.


---
*Imported from [Google+](https://plus.google.com/+Whosawhatsis/posts/QBwaGuUri71) &mdash; content and formatting may not be reliable*
