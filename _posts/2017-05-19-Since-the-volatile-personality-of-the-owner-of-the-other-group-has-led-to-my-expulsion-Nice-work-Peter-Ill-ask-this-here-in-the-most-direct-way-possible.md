---
layout: post
title: "Since the volatile personality of the owner of the other group has led to my expulsion (Nice work Peter!), I'll ask this here in the most direct way possible"
date: May 19, 2017 16:57
category: "General discussion"
author: "Kelly Burns"
---
Since the volatile personality of the owner of the other group has led to my expulsion (Nice work Peter!), I'll ask this here in the most direct way possible.  To the folks that are using Laserweb with Smoothieware what are you doing with the Raster Engrave overall and variable speed issue.  I was hoping for a magic bullet that I missed, but maybe its just a matter of an acceptable workaround or settings that are good enough.  Any input and advice is appreciated.  So long as it isn't and "switch to GRBL"  









**"Kelly Burns"**

---
---
**Roberto Fernandez** *May 19, 2017 17:06*

Try this firmware of grbl 1.1 forma smoothieboard.

[github.com - gnea/grbl-LPC](https://github.com/gnea/grbl-LPC)


---
**Arthur Wolf** *May 19, 2017 17:13*

**+Kelly Burns** Sorry to hear you've been lw-banned, that happens much more than we'd like, and I'm not sure what to do about it.



About engraving, setups vary very very much, however I'll try to summarize the issue as seen by most users who have given us feedback : 

* Playing raster gcode from LW to Smoothie is slow, hovewer, playing from the SD card is much faster ( seems to depends on the SD card how much ), and playing the gcode using this script ( [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/edge/fast-stream.py) ) is even faster.

* To us, this seems to point to a problem with the way LW sends Gcode to Smoothie ( and/or possibly a problem with the Windows serial drivers when used in this specific way ).



Hopefully, there are signs that the LW community is going at some point to tackle this issue ( they have shown signs of renewed good will recently ), and possibly ( I can't guarantee anything I'm not a Laserweb dev ), get LW to talk to Smoothie as fast as the fast-stream script is capable of.



Until they do this, temporary solutions include playing from SD, using the fast-stream script, or using GRBL. Really sorry about not having more options for you, but from the Smoothie side of things we really don't have much control over the situation. 


---
**Kelly Burns** *May 19, 2017 17:28*

Thanks for the response.  I must admit, I sit here pretty dumbfounded by this.  Even knowing Peter's propensity for this type of reaction, I was caught off-guard. It is what it is. I'll let it sit a while. I'm bit too old to put up with this for what is essentially an enjoyable hobby. 



You guys are some intense dudes.  Maybe it comes with being that smart.  I will tell you that the Dev Groups need to work together.  I haven't read everything on this Raster/GCODE feed problem and I stumbled into by accident while search.  As an outside overserver/consumer of both projects, its pretty disappointing.


---
**Kelly Burns** *May 19, 2017 17:39*

**+Roberto Fernandez**  Good Lord, its sad to see such Smart people act like children.  I have done nothing to warrant the treatment I have received today by a community that I have contributed to both physically and financially.  While I'm pretty disappointed, I'll get over it and ultimately, it will save me time and money.  


---
**Jim Fong** *May 19, 2017 17:50*

I've seen Smoothie/laserweb serial stutter using feed rates as low as 25mm/sec.  Both on raster and vector cutting.   I just copy the gcode to the sdcard and run from there.   I prefer it that way since I can use the computer to do something else while watching the laser run. (Less chance of a PC hiccup messing up the engraving)    Most of my raster engravings are done at 200mm/sec or less so it runs stutter free from the sdcard.  


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 18:09*

Welcome to the club **+Kelly Burns**. I too was kicked out for calling him out for his responses to others. He didn't like being called out and I got banned as well. Then he tried picking a fight again on github when I opened a ticket and I kept pointing out his mistake for not reading what I was referring to, while one of the other developers knew exactly what I was talking about. The result of that was that he quickly deleted the whole exchange between him and I. All I can think there is that he has a large ego to satisfy and can't stand ever being wrong. It's his way or go fuck off. No one else would see that he was wrong. It's a shame really. I stood up for him in the past when people were being pedantic, but his behavior is well out of hand. In my 30+ years of being in IT I've had to deal with a lot of crap thrown my way, but I never, ever, respond to anyone the way he does. Even if I know I'm correct and the other person is wrong, even if I think the other person is a complete moron, or hasn't bothered to read the instructions, it doesn't matter. Learning how to talk to people is a skill one has to learn. Learning to get off of your high horse and down to the other person's level of stupidity, that's yet another skill to learn.



I will keep using LW as it develops and help in whatever way I can, but I have zero desire to go back to that community if I'm ever invited/allowed back in. At least not while he's still running it and spewing his anger and complete communication incompetence at everyone.



Now, as for your Raster Engrave question, I run all my raster tasks straight from the SD card. It gets a bit annoying when I have multiple tasks within the same job but for the moment I'll deal. There's the grbl-lpc release that has addressed the streaming issue however in my case I can't use it (at least I don't think so) because I no longer have a potentiometer on my machine and the grbl-lpc page specifically states it's for machines that (still) have that. My PWM signal is on pin 2.4, inverted (as opposed to their pin 2.5.) And it also doesn't pass the SD card to the host computer, making it difficult to put files on it if I have a large job. So for the moment I will deal with the extremely slow transfer rate from host -> SD card, because that's still better than watching the machine stutter as LW tries to send data to it.



I do believe it's a Smoothie USB driver issue. Somewhere on the Smoothieware forums I saw a post that talks about it and the result was that they're aware of an issue but they can't figure out why or what exactly it is. Such is life in the fast lane. :)


---
**Kelly Burns** *May 19, 2017 18:13*

**+Jim Fong** Thanks for the response.  Seems like a common approach.  I have gotten a few private responses from folks that saw the post and the SD thing or GCODE feeder is what others are doing. This is why I posted.   I certainly knew that switching to GCODE was an option, but certain people take everything personally.  I'll play around with the SD method until I figure out what I'm going to do.  



I'm sorta all-in at this point and can't go back to the original controller, but I may have to find a non Laserweb solution.  If only for my own sanity.    



btw... you have done some great stuff with these tools.  Much of it seems to be similar to what I do.   








---
**Arthur Wolf** *May 19, 2017 18:16*

**+Ashley M. Kirchner** About "it's a Smoothie USB driver issue". Smoothie doesn't have a USB driver. What we tell our users is a "driver" they need to "install" really is just a short text file that tells Windows "hey dummy, you have a driver for this already, use it with this board".

Linux and Mac are smart enough to figure this out, Windows you have to handhold.

The point is : we don't have any control over the serial driver, it's hardcoded into Windows and it sucks.



There are however ways to get serial to go fast even with the crappy driver, but the problem is Laserweb has not yet at this point implemented these better ways. It sounds like they are working on it though.




---
**Kelly Burns** *May 19, 2017 18:16*

**+Ashley M. Kirchner** 

Thanks for the response.  It may very well be a Smoothie issue, but I certainly wasn't blaming Laserweb either.   My question was to inquire what other people were doing since it seemed as though there were workarounds.   



It might end up being a SW issue, but when people aren't willing to work together, its difficult to resolve.




---
**Arthur Wolf** *May 19, 2017 18:19*

**+Kelly Burns** « but when people aren't willing to work together, its difficult to resolve. » 

It's not a problem on Smoothie's side, so we can't work on resolving it. We have offered to help and have helped the LW people with any info we have on the best ways to fix things on their side, but that has often lead to very bad reactions. Recently at least one of the devs though has had a positive reaction and seems to agree/understand how to fix things, so maybe things will make progress on the LW side now.


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 18:29*

**+Arthur Wolf**, I was referring to when I was using the Smoothie USB drivers on a Win7 machine. The v1.1 release never worked and I dropped to the manual installation of v1.0 which did. However transfer rates from host to the SD card were abysmal. Then I switched to a Win10 machine since I knew I didn't need to install any specific driver anymore (as it just recognized the built-in one) and much to my surprise the problem persisted. LW wasn't in the picture, it's straight host computer -> the Smoothie's SD card.


---
**Chris Cecil (Robosprout)** *May 19, 2017 18:31*

Transfer rates to the SD card are limited by the SPI speed not the USB speed...from my understanding.


---
**Arthur Wolf** *May 19, 2017 18:31*

**+Ashley M. Kirchner** Yep, you never actually installed a driver, you just "activated" the one that was already in your Windows, and we have no control over what that is and how that works


---
**Wolfmanjm** *May 19, 2017 18:37*

**+Ashley M. Kirchner** FWIW it is NOT a smoothieware issue at all. And in fact I spent a huge amount of time debugging the issue and found out exactly the cause. It is outlined in this issue, plus the required streamer technique required for mac and windows (Linux does not suffer from the issue). The issue IS the operating system drivers on macos and windows. It is not a bug per se in  those drivers, but the way they delay the reads from USB/ACM/CDC devices. This causes a lag between sending gcode and getting the ok which smoothie sends almost immediately. Any attempt I made to explain this in the LW forums was deleted :)

Here is the issue that the solution was eventually found... [github.com - Smoothie problem with streaming raster gcode over USB · Issue #1096 · Smoothieware/Smoothieware](https://github.com/Smoothieware/Smoothieware/issues/1096)

As you can see this was solved in January.


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 18:52*

**+Wolfmanjm** I'm understanding when you say it's not Smoothieware's issue, when run on a Windows 10 system which has built-in drivers. But, is that still true for the USB drivers that is provided for those who DO NOT run Windows 10? Specifically, these: [smoothieware.org - windows-drivers [Smoothieware]](http://smoothieware.org/windows-drivers)



As I mentioned, I <b>used</b> to run a Win7 machine so I had to install those drivers (1.1 didn't work, had to manually install 1.0). And I did NOT install those same drivers on Win10, it's all built-in.


---
**Kelly Burns** *May 19, 2017 18:55*

I'm getting some nasty messages, so let me explain...  Nowhere in my posts on either community did I place blame for the problem.  Mainly, that's because I don't know and I'm not smart enough  to figure it out ,but also because that wasn't the point.  I wanted to know what others were doing to deal with the problem.  



I will assign blame and call a person out for blocking me and attacking me.  And I did that on purpose in my post above.  I don't apologize for it.  I have done nothing wrong.  


---
**Wolfmanjm** *May 19, 2017 18:56*

as **+Arthur Wolf** said these are still builtin drivers and probably the same driver. The ini file simply maps the device to the builtin driver. smoothie does not have ANY o/s drivers at all. Those scripts do not install anything they are simply .ini files.


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 18:59*

If you're getting nasty messages, tell them to grow up. You haven't done anything wrong here. If anything, I'm the one who placed blame on Smoothie and am now corrected and I'm happy to see that.


---
**Wolfmanjm** *May 19, 2017 19:02*

**+Kelly Burns** I feel your pain :) I too was censored and yelled at... it comes with the territory I am afraid, try to not take it personally. We all have our 'volatile' times :)




---
**Ashley M. Kirchner [Norym]** *May 19, 2017 19:02*

**+Chris Cecil**, correct, it would be. However you can drive SPI in the MHz range and get high speed transfers, I've done it many times. This delay that we're seeing doesn't come anywhere close to that speed. And since **+Wolfmanjm** said that the OK msg from Smoothie comes back almost instantly, that tells me it is writing/reading at high speed already, so it wouldn't be that. It sits in the communication between the host and Smoothie, not on Smoothie itself.


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 19:05*

**+Wolfmanjm**, gotcha. I made the wrong assumption that those drivers provided by Smoothie, were custom Smoothie drivers, and not generic built-in stuff. Mental note made and saved. :)


---
**Chris Cecil (Robosprout)** *May 19, 2017 19:06*

**+Ashley M. Kirchner** The point I was stating was USB>SD transfer rates and the streaming rates on the USB are not the same thing.  The transfer rates to the SD card are slow right now...over USB or ethernet.



There was some testing done on the speeds on different cards and such.  V2 won't have this issue from my understanding.


---
**Wolfmanjm** *May 19, 2017 19:17*

When playing from sdcard there is no OK, it just reads from sdcard as it needs to keep the planner queue full. The speed limit on sdcards is actually the sdcard itself, in SPI mode they tend to be very very slow especially the new 'fast' ones which have huge write buffers. class 4 are actually faster in smoothie than class 10. there was an effort to write new SPI sdcard drivers, and they were a lot faster, but they were not used as it would have lost the MSD feature (which IMO was no loss)


---
**Steve Anken** *May 19, 2017 21:36*

How sad. I too am in the club for pointing out that Peter is not a people person. Peter does not see the pattern is him and not all the people he has attacked when he got miffed. He needs to talk to someone who knows people and that he respects. His tech chops make it hard to tell him that his empathy IQ is near zero. Unfortunately if you push someone like that too much they never learn to be humble enough to apologize and admit it was them and they have a lot to learn, even from folks with no tech chops.



If anybody gets slammed by this guy, it's not you, it's him.


---
**Anthony Bolgar** *May 19, 2017 22:07*

Is it time to start a support group for us survivors? I just repeat to myself "Free at last!" as I do not need to deal with his personality quirks anymore. :)


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 23:05*

LOL! You have the power to start a group. :)


---
**Kelly Burns** *May 20, 2017 00:15*

 While I appreciate the support I do not want this to be about anything with Peter.  While I certainly think he's wrong it's his group and laser web is his baby.  I simply want to be able to discuss this issue and find some workarounds for now until something is done.   


---
**Ashley M. Kirchner [Norym]** *May 20, 2017 00:21*

I think we've all pretty much do the same thing, save the file on the internal SD and run it from there. When I have jobs with multiple tasks, it would depend where in the process the raster part is. If it's the first thing, then I tend to only save that task, then run the other cutting or fill path ones from LW as that works just fine.


---
**Todd Fleming** *May 20, 2017 01:24*

We (the remaining LW developers) are currently looking into the ban issue. There are too many good people on the ban list (many posted here) and we aim to fix it.


---
**Todd Fleming** *May 20, 2017 01:59*

We released the bans, hope to not repeat them, and hope all of you who are willing rejoin us. Our statement is at [https://plus.google.com/101442607030198502072/posts/Natbf9W2ohi](https://plus.google.com/101442607030198502072/posts/Natbf9W2ohi)


---
**Joe Alexander** *May 21, 2017 02:31*

Thanks **+Todd Fleming** for your response to this :)


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/DY84cbLu49V) &mdash; content and formatting may not be reliable*
