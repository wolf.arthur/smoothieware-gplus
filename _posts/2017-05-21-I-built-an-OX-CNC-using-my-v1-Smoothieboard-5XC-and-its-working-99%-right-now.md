---
layout: post
title: "I built an OX CNC using my v1 Smoothieboard 5XC and it's working 99% right now"
date: May 21, 2017 00:55
category: "Help"
author: "Michael Forte"
---
I built an OX CNC using my v1 Smoothieboard 5XC and it's working 99% right now. I just ran a test file and everything is moving in the correct direction with the correct distances. 



Now that it works, it's time to tune it up. The problem I'm having is with the Y stepper motors. In the attached photo Y is left to right. Those 3 squares should all be in a straight line, not staggered. One Y motor (the one with the longer wires) seems to be lagging the other Y motor in starting and stopping. At times the lagging (or lack of movement) is causing the opposite (leading motor) side, to misstep. I am at a loss of what I should try first.  Are there known 'safe' configurations for steps, current, etc. that I should try in the config file. I doubt the belts are too tight since I can lift one end and the whole gantry will slide down to the other end. There are so many variables in this that I'm not sure what I should troubleshoot first. 



SETUP: 

My motors are MOTECH 2.8A 1.8deg/step motors and both sides are identical except for their opposite rotation.



I am taking the signal from the Beta Step, Enable, and Dir pins and running those in parallel to the negative inputs on 2 separate motor controllers on TB6600 motor controllers. The + on the TB6600s are going to the smoothie endstop ground pins. I originally tried to connect the motors in serial; from the smoothie to TB6600 (+Y) to TB6600 (-Y) with the same results.



The first question should probably be is my wiring OK? Can I tee off the commands and go to 2 motors? If not, can I go to 1 motor and then jumper the motors? 



Once that is answered then are my config file settings OK? After that, I don't know what to check. Any help is greatly appreciated!



Config file settings:

# Robot module configurations : general handling of movement G-codes and slicing into moves

default_feed_rate                            3000             # Default rate ( mm/minute ) for G1/G2/G3 moves

default_seek_rate                            3000             # Default rate ( mm/minute ) for G0 moves

mm_per_arc_segment                           0.5              # Arcs are cut into segments ( lines ), this is the length for

                                                              # these segments.  Smaller values mean more resolution,

                                                              # higher values mean faster computation

mm_per_line_segment                          5                # Lines can be cut into segments ( not usefull with cartesian

                                                              # coordinates robots ).



# Arm solution configuration : Cartesian robot. Translates mm positions into stepper positions

alpha_steps_per_mm                           53.33            # Steps per mm for alpha stepper. 53.33 GT3 belt with 20T pulley

beta_steps_per_mm                            53.33            # Steps per mm for beta stepper

gamma_steps_per_mm                           400             # Steps per mm for gamma stepper



# Planner module configuration : Look-ahead and acceleration configuration

planner_queue_size                           32               # DO NOT CHANGE THIS UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING

acceleration                                 3000             # Acceleration in mm/second/second. was 3000

z_acceleration                              500              # Acceleration for Z only moves in mm/s^2, 0 uses acceleration which is the default. DO NOT SET ON A DELTA

acceleration_ticks_per_second                1000             # Number of times per second the speed is updated

junction_deviation                           0.05             # Similar to the old "max_jerk", in millimeters,

                                                              # see [https://github.com/grbl/grbl/blob/master/planner.c](https://github.com/grbl/grbl/blob/master/planner.c)

                                                              # and [https://github.com/grbl/grbl/wiki/Configuring-Grbl-v0.8](https://github.com/grbl/grbl/wiki/Configuring-Grbl-v0.8)

                                                              # Lower values mean being more careful, higher values means being

                                                              # faster and have more jerk

#z_junction_deviation                        0.0              # for Z only moves, -1 uses junction_deviation, zero disables junction_deviation on z moves DO NOT SET ON A DELTA

#minimum_planner_speed                       0.0              # sets the minimum planner speed in mm/sec



# Stepper module configuration

microseconds_per_step_pulse                  1                # Duration of step pulses to stepper drivers, in microseconds

base_stepping_frequency                      100000           # Base frequency for stepping, higher gives smoother movement



# Cartesian axis speed limits

x_axis_max_speed                             30000            # mm/min

y_axis_max_speed                             30000            # mm/min

z_axis_max_speed                             300              # mm/min



# Stepper module pins ( ports, and pin numbers, appending "!" to the number will invert a pin )

alpha_step_pin                              2.0o             # Pin for alpha stepper step signal

alpha_dir_pin                               0.5o            # Pin for alpha stepper direction

alpha_en_pin                                0.4o!           # Pin for alpha enable pin

alpha_current                               2.0              # X stepper motor current

alpha_max_rate                              30000.0          # mm/min



beta_step_pin                                2.1o             # Pin for beta stepper step signal

beta_dir_pin                                 0.11o!            # Pin for beta stepper direction

beta_en_pin                                  0.10o!           # Pin for beta enable

beta_current                                 2.0              # Y stepper motor current

beta_max_rate                               30000.0          # mm/min



gamma_step_pin                               2.2o             # Pin for gamma stepper step signal

gamma_dir_pin                                0.20o            # Pin for gamma stepper direction

gamma_en_pin                                 0.19o!           # Pin for gamma enable

gamma_current                                1.68             # Z stepper motor current

gamma_max_rate                               300.0            # mm/min







![missing image](https://lh3.googleusercontent.com/-mPIwBR4wWKM/WSDlZmpOnwI/AAAAAAAAA-A/QF-bU94MhkU8rA15X2xOlROGwZfGs-E2gCJoC/s0/IMG_3990.JPG)
![missing image](https://lh3.googleusercontent.com/-zFwwxGkEeZ0/WSDlZoG5OvI/AAAAAAAAA-A/efoNtnDooAg1qEfxrDC2pyty4R1apo4QQCJoC/s0/IMG_2987.JPG)

**"Michael Forte"**

---
---
**Scott Lewis** *May 21, 2017 01:28*

Bad stepper wire connection? Wire nuts seem like a bad idea to me.  Also other possibilities are bad motor or physical binding on y axis movement, or slipping belt.



Are the OX belts a loop with idler/tensioner or a one sided thing where the extruder moves with the gantry?


---
**Michael Forte** *May 21, 2017 01:46*

I don't think it's a bad stepper wire connection since it works well when I tell it to step 100mm. It's when I get to smaller increments where the motor just doesn't seem to keep up. In every case, however, the one side starts and stops moving slightly after like there's a time delay. I doubt the wire length could do that, could it?



The wire nuts in the picture was a temporary splicing to see if a parallel setup would work any different than a serial setup. It's not a final solution.



The motor seems to work OK. You give me an idea though. I put in quick disconnects between the motor controllers and the motors. Let me try swapping the y motors. If it still binds the same way then it has to be mechanical. If the lags happens on the opposite side, then it would have to be electrical in nature.



The extruders are mounted on either side of the gantry. The belt connection is pretty slick. The belt is tight but it's loose enough where lifting one end of the frame allows the entire gantry to slide to the other end just by gravity.  See the attached photo.

![missing image](https://lh3.googleusercontent.com/sIp4irYCCZvH9jl41Afb9rCc582MxUH2jJjtXMd-69tA7p8ObGzW1q4fUKnbOr01SOz4JMZxwFasNAUkOCwSgpmhQwPmEw4ELQ=s0)


---
**Jim Fong** *May 21, 2017 02:22*

If it isn't a mechanical problem, My guess it is loosing steps on the acceleration. Try lowering the acceleration by half and re-run the gcode.  



Tb6600 likes to use a higher voltage PS for better performance. When I used a tb6600, my PS was around 30volts.  


---
**Arthur Wolf** *May 21, 2017 09:24*

It's possibly not related to your issue, but your step/dir wiring is wrong and you should fix it anyway. Please see [smoothieware.org - general-appendixes [Smoothieware]](http://smoothieware.org/general-appendixes#external-drivers) and ideally use the "Open Drain" setup described there.



Also, if you are using external drivers, then the current setting on the Smoothieboard is meaningless and you need to correctly adjust the current setting on the drivers themselves.



Also, your acceleration seems very high for a router.


---
**Stephane Buisson** *May 21, 2017 09:44*

if mecanic, just a clue from here could maybe help :

Installing dual belts on OX CNC - X-Axis


{% include youtubePlayer.html id="f4VcLQ1cfIs" %}
[youtube.com - Installing dual belts on Ooznest OX CNC - X-Axis](https://www.youtube.com/watch?v=f4VcLQ1cfIs)


---
**Michael Forte** *May 21, 2017 23:28*

**+Arthur Wolf**

My wiring is setup according to the open drain external controller description on the web site.  The only difference is I want to send that one step command to 2 motor controllers (+ and - Y) simultaneously so I jumpered the EN, STEP, and DIR pins between the motor controllers. The picture is showing the connection in parallel. I also tried it in serial but it didn't seem to make a difference. If there is a preferred way to set that up please let me know. On the web site there is an example of jumpering the pins together but, if I'm not mistaken, that is only if you are using the built in drivers, not external drivers.



**+Jim Fong** I will try reducing the acceleration since both of you are saying it's too high.



I will report back once I have adjusted acceleration.




---
**Michael Forte** *May 21, 2017 23:38*

**+Stephane Buisson**

Interesting setup! I'll probably try it after I get this primary issue fixed.




---
**Michael Forte** *May 22, 2017 02:38*

SUCCESS! 

First, I reduced acceleration to 1500 from 3000. I then went back to the serially wired open drain setup since it's much cleaner wiring and then decided to remove the belts off the Y motor gears so I could watch them rotate individually. Well the lag side of the gantry was lagging because that motor was not working at all. That side of the gantry was getting dragged back and forth once the force was high enough to push it.

 

So my troubleshooting showed that one of my wire splices had come loose. Once I fixed that wire it mostly worked but still had a slight lag. That's when I noticed the gear on the motor shaft wasn't tightened enough so there was some play in it. I tightened that down, respliced that motor with quick disconnects, and ran a test file. It ran flawlessly.



Here is a picture of the completed test with the OX inside an enclosure I built out of Vslot, noise cancelling foam, plexiglas, and other odds and ends.

![missing image](https://lh3.googleusercontent.com/Kveen4VLuQovvu_EdhCIlUTTZsN0Y8BnLgE_rUnvB_dl9p6ESjB_7ur2sc_azSGDFCV7uaWJCzy9NFLoAtD9FrdBc0aVjlGovw=s0)


---
**Jim Fong** *May 22, 2017 03:27*

Great!!!! I use loctite blue on all pulley set screws. The stepper vibration will eventually loosen them up again if you don't. 


---
**Michael Forte** *May 22, 2017 14:13*

**+Jim Fong** It never crossed my mind that the set screws would come loose on the gear. If you put it on can you remove it at a later date if needed?




---
**Jim Fong** *May 22, 2017 15:18*

**+Michael Forte** loctite blue #242 thread locker is removable.  Only takes a little bit of extra torque to break the bond. Don't use red version since that is semi permanent and requires heat to loosen up again. 






---
**Stephane Buisson** *May 22, 2017 15:41*

Nail varnish (ask your girl friend) work very well too.


---
**Matthew Kelch** *May 26, 2017 17:56*

Even with the red loctite, a soldering iron works great to heat up the grub screw and break down the adhesive.


---
*Imported from [Google+](https://plus.google.com/+MichaelForte66/posts/LtDs9hB3xaW) &mdash; content and formatting may not be reliable*
