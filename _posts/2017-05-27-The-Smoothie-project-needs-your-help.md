---
layout: post
title: "The Smoothie project needs your help !"
date: May 27, 2017 09:35
category: "General discussion"
author: "Arthur Wolf"
---
The Smoothie project needs your help ! ( what ? again ? )



A few months back, we migrated the wiki ( documentation, at [smoothieware.org](http://smoothieware.org) ) to being a self-hosted wikidot.



Unfortunately, a few things were lost in the process. In particular, a lot of pictures didn't properly transition between the two wiki systems, and aren't displayed in the pages anymore.



We could improve the wiki a lot if a few of you went over the pages of the wiki, found the images that are not displaying, and fixed the syntax. It would be very helpful to a lot of people, and is not <b>that</b> much work.



For example, I just went to the [http://smoothieware.org/windows-drivers](http://smoothieware.org/windows-drivers) page, clicked "edit" on the right, then in the page's source found this ( by doing Ctrl+F "image" ) : 



<image src='findthedriver.jpg' width='620px'><br/>



And to fix the image, I just need to change it to : 



<image src='/_media/windows-drivers/findthedriver.jpg' width='620px'><br/>



If you could go over the wiki and fix broken images like this, it would be extremely appreciated and welcome, and would help the project a lot.



Cheers :)







**"Arthur Wolf"**

---
---
**Ray Tice** *May 27, 2017 10:57*

There's an app for that!  Lots of them, at least for finding broken links. For example, Google is willing to give the site owner a list of broken links that its web crawlers have found. If you stick that list on a wiki page, people could correct those and cross them off, instead of people randomly searching.  (disclaimer: this is all based on a quick web search, not personal experience) [support.google.com - Crawl Errors report (websites) - Search Console Help](https://support.google.com/webmasters/answer/35120?hl=en)  


---
**Arthur Wolf** *May 27, 2017 10:59*

**+Ray Tice** The problem here is that this is not about broken link, it's about broken wiki syntax. The images don't appear at all ( not even in broken form ) on the pages, because the wiki syntax is wrong. And even if you fix the wiki syntax, you then stil need to also fix the path to the picture.


---
**Sébastien Plante** *May 27, 2017 12:24*

woohoo, finally something I can do ! I'll help you :)


---
**Arthur Wolf** *May 27, 2017 12:27*

**+Sébastien Plante** Awesom, thanks !

Btw, if you want to help the Smoothie project, we have a procudure where you email me at wolf.arthur@gmail.com, we talk about your skills and tastes, I take a note of them, I tell you about some things you can do to help that match what we talked about, and I keep all the info aside so I can contact you if something comes up that matches you.


---
**Kirk Yarina** *May 27, 2017 13:30*

(g)awk, or your favorite pattern matching language?


---
**Arthur Wolf** *May 27, 2017 14:19*

**+Kirk Yarina** I already did a script that fixed the easy ones, but there are a lot of "special cases" that need to be fixed by humans.


---
**Ray Tice** *May 27, 2017 14:55*

**+Arthur Wolf** Ah, so the broken links on the windows-drivers page are a completely different issue then.


---
**Arthur Wolf** *May 27, 2017 14:56*

**+Ray Tice** Yep lots of problems that need fixing.


---
**Michael Andresen** *May 30, 2017 04:57*

I went over the gallery some time ago, that was a huge mess 😂


---
**Arthur Wolf** *May 31, 2017 09:12*

**+Michael Andresen** Thanks a ton, that was a huge one !


---
**Sébastien Plante** *May 31, 2017 12:41*

**+Arthur Wolf** Will I see a "broken link" image ? Or it will be left as a blank space ? Cause I've checked a lot of pages the others day and I didnt see any broken image! :(




---
**Sébastien Plante** *May 31, 2017 12:47*

**+Arthur Wolf** For example, I see a lot of link to [chibidibidiwah.wdfiles.com](http://chibidibidiwah.wdfiles.com) instant of [smoothieware.org](http://smoothieware.org), are those okay ?



I sended you an email, if you want to provide me more details, I'm pretty sure I'll be able to help.


---
**Arthur Wolf** *May 31, 2017 15:29*

**+Sébastien Plante** Broken images simply don't display, so you actually need to go look at the source of each page to find them, unfortunately. The images are hosted in various places, it doesn't matter exactly how you do it, the objective is just that the users see them :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/A2spzFVQKMM) &mdash; content and formatting may not be reliable*
