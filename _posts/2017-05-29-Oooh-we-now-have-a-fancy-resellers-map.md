---
layout: post
title: "Oooh, we now have a fancy resellers map !"
date: May 29, 2017 10:46
category: "General discussion"
author: "Arthur Wolf"
---
Oooh, we now have a fancy resellers map !



[http://smoothieware.org/getting-smoothieboard](http://smoothieware.org/getting-smoothieboard)



Btw, still looking for more resellers, if anyone's interested ! 



And we are in discussions to have new resellers in Australia, South Africa, Japan, China and several South America countries.





**"Arthur Wolf"**

---
---
**Kelly Burns** *May 30, 2017 18:46*

Definitely need to make it easier to obtain one.  The 3 week turnaround I was given just isn't acceptable today.  


---
**Arthur Wolf** *May 30, 2017 21:09*

**+Kelly Burns** We are small companies and most of the profits go into improving the firmware and hardware. We work very hard on trying to improve the delivery delays, and often nowadays it's "as it should be", but recently a worldwide shortage of the stepper motor driver we use, caused huge production delays.


---
**Kelly Burns** *May 30, 2017 21:12*

I know. It's a tough balance.  Us maker types need immediate gratification ;). 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/Wk46XdW9mub) &mdash; content and formatting may not be reliable*
