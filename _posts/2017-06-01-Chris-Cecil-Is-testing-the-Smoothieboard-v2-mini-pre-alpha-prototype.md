---
layout: post
title: "Chris Cecil Is testing the Smoothieboard v2-mini pre-alpha prototype"
date: June 01, 2017 19:51
category: "General discussion"
author: "Arthur Wolf"
---
**+Chris Cecil** Is testing the Smoothieboard v2-mini pre-alpha prototype. He's getting ready to flash the first firmware on it, do you think it'll work ? I sure hope it does.

This is a huge step in a colossal project that has involved nearly a hundred volunteers and more means than we have ever put together before, over more than two years now.

This is going to be such a mind blowing project, nobody really realizes how much we are doing, can't wait for everyone to go nuts over this :)

![missing image](https://lh3.googleusercontent.com/-Fs_0CzgHdTU/WTBwPlu_dSI/AAAAAAAAPGo/GVkY8klMDSweNHyTeuNXcWy7Wu4CwfUVwCJoC/s0/20170601_124441.jpg)



**"Arthur Wolf"**

---
---
**Steve Anken** *June 01, 2017 21:19*

Congratulations on staying with it and making it work. I sure hope the software is as good and supports things like 4th and 5th axis CNC work.


---
**Arthur Wolf** *June 01, 2017 21:32*

**+Steve Anken** The current v1 firmware for the v1 board supports 4/5/6 axis work.


---
**Steve Anken** *June 01, 2017 21:51*

Well I hope it shows up in software before V3. At one point I saw some 4th axis stuff posted with one rotation axis but as it is now the fact that it's in firmware does not help end users. 3d is where CNC really stands out. The robot arms are doing a 7th axis for tool attachments. They do not have the precision or power of a CNC for milling but they are great at loading and unloading parts. Tool path planning gets very weird because there are so many solutions and it's hard to find optimal paths. Anyway, thank for all the hard work. It does mean a lot to people who you may never meet. The machines I converted are working to help some folks who are struggling to keep their shops open.


---
**Arthur Wolf** *June 01, 2017 22:07*

**+Steve Anken** I'm not sure what the problem is, Smoothie <b>fully</b> supports 4, 5 and 6 axis machining, and you can use software like Fusion360 to generate the Gcode for those setups ...


---
**Steve Anken** *June 01, 2017 23:32*

Yes, but you cannot see what is going on while milling . Have you done this or has anybody done 5 axis 3d CNC with smoothie?


---
**Arthur Wolf** *June 02, 2017 02:17*

I and others have done 5 axis, but yes there isn't much software to visualize. Hasn't been a big block though.


---
**Steve Anken** *June 02, 2017 03:22*

Interesting. would love to see a video of how you did it. It's is not trivial to hold the material and I have no idea how you set up the 5 axis machine since there seems to be different ways to do it.


---
**Arthur Wolf** *June 02, 2017 03:30*

Will do a video at  some point


---
**jerryflyguy** *June 02, 2017 18:25*

**+Arthur Wolf** are you saying smoothie will do the realtime 5 axis tool tip offsets (G97? IIRC) from generated g-code? For those of us who have run a mainstream Cnc machine (or mach3 for that matter) it's hard to visualize how the workflow would be on a smoothie system. There's lots of work offsets and other machining function that (at least on first glance) appears to be missing. It'd be great if it (smoothie) could do the heavy lifting for a front end like mach4 or EMC. 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/fwNDaWqYAtq) &mdash; content and formatting may not be reliable*
