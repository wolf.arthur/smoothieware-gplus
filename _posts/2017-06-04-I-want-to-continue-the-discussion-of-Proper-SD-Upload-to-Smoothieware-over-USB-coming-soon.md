---
layout: post
title: "I want to continue the discussion of \"Proper SD Upload to Smoothieware, over USB coming soon!\""
date: June 04, 2017 08:36
category: "Development"
author: "Yale Zhang"
---
I want to continue the discussion of "Proper SD Upload to Smoothieware, over USB coming soon!" from November 21, 2016, but can't access that thread any more.



I'm getting very slow SD reads/writes when uploading over USB mass storage. Only 124KiB/s sequential read speed!



That thread said the bottleneck was the 1 bit SPI interface.  I kind of doubt that because I worked on a project that read from SD using the SDIO interface (not SPI), in both 1bit and 4bit mode. I recall at low speeds, a 1 bit interface is just as fast as 4bit. Throughput depended more on the read/write block size. A 1 bit interface @25 MHz should be capable of ~3MiB/s. What's the SPI speed on SmoothieBoard?



It seems the problem is the USB mass storage implementation?  Did Adam Green ever upload his improvements?



Also, what about replacing USB mass storage with MTP? That was on the todo list for V2. What's the status? If it's ready, I was thinking of porting it back to V1 because right now, I have to unplug the USB cable after uploading gcode because I can't figure out how to unmount in Windows.







**"Yale Zhang"**

---
---
**Jeff DeMaagd** *June 04, 2017 18:20*

Smoothie uses the 12Mbps USB. But SD is the same speed over Ethernet too.


---
**Yale Zhang** *June 05, 2017 20:09*

Right, it's slow for both Ethernet & USB because they both have to go through 1 bit SPI to reach the SD card. But what's the SPI speed?



I'm thinking of increasing the USB mass storage block size to see if that improves throughput.



I said in an earlier STM32F7 + microSD project, I found write speed heavily depends on the write block size:



Sequential writes. Speed measured in MiB/s. Card to MCU interface is 4 bits wide @ 25 or 42 MHz.



  write size   Kingston class4(25Mhz?)   Sony UHS (42MhZ)

    1KiB:                         0.47                                     0.70

    2KiB:                         0.84                                     1.3

    4KiB:                         1.4                                        2.2

    8KiB:                         2.1                                        3.7

    16KiB:                       3.1                                        6.0

    32KiB:                       3.6                                        7.0




---
**Marcos Gomes** *June 11, 2017 18:58*

We can not directly compare SPI and SDIO protocols, even if we use the 1-bit mode.



SDIO uses a higher frequency and uses different connections for commands and data transfer.



Using SPI before transmitting data, we need to exchange a couple of commands between the mcu and the seller card.



In the last couple of years I have been working with the same lpc17xx family of mcu's and had exactly the same problem.



Like smoothie, I use the fatfs library and experience the same slow SD read/write speeds.



Smoothie v2 mcu features SDIO peripheral, so I believe it will achieve at least a couple of MBytes/s






---
*Imported from [Google+](https://plus.google.com/110289521769967338399/posts/dUYDU4eVDA1) &mdash; content and formatting may not be reliable*
