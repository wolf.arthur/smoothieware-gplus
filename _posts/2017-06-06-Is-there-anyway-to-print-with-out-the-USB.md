---
layout: post
title: "Is there anyway to print with out the USB"
date: June 06, 2017 22:15
category: "Help"
author: "jacob keller"
---
Is there anyway​ to print with out the USB. Because on the smoothieboard website said you can print from the SD card but it's​ not working. When I press print over USB to the SD card it starts perfect but right when I unplug the USB the printer stops and jerks like it not receiving the gcode correct.



I have the gcode file on the SD card and I press SD upload in pronterface. But that still didn't work.



Please help



Jake





**"jacob keller"**

---
---
**Chris Chatelain** *June 06, 2017 23:12*

Do you have an LCD screen? That's how I trigger and card prints without a computer attached. 


---
**jacob keller** *June 07, 2017 00:01*

**+Chris Chatelain**​ I have two LCD screens. They are the discount glcd screens that are compatible with the smoothieboard. Tried using the adapter didn't work tried doing manual wire the screen to the board didn't work. And the adapter and the screen are original. And I flipped the cables 180 degrees as recommended. Still didn't work and I had Arthur Wolf look at my config and he siad that it was correct. Do you have a picture I can see of your board with LCD screen plugged in.



Thanks for the reply


---
**Sébastien Plante** *June 07, 2017 00:09*

you need to place the gcode on the smoothie, then start the print from the LCD menu (or web interface).


---
**Sébastien Plante** *June 07, 2017 00:10*

[http://smoothieware.org/printing-from-sd-card](http://smoothieware.org/printing-from-sd-card)


---
**jacob keller** *June 07, 2017 00:21*

**+Sébastien Plante**​ that's what I did I put the Gcode file on the smoothieboard then  I pressed SD print from pronterface. 


---
**Sébastien Plante** *June 07, 2017 00:51*

no, use the play/print on the LCD, not pronteface


---
**jacob keller** *June 07, 2017 00:53*

**+Sébastien Plante**​ I don't have a LCD it doesn't work.


---
**jacob keller** *June 07, 2017 00:55*

**+Sébastien Plante**​ for some reason I use to be able to print from my computer and then unplug the USB and walk away and the printer world work just fine.


---
**Wolfmanjm** *June 07, 2017 02:44*

in pronterface send the command 

@play /sd/filename then disconnect pronterface and you can pull usb out


---
**jacob keller** *June 07, 2017 03:07*

I'll try that **+Wolfmanjm**​ thanks for the response


---
**Sébastien Plante** *June 07, 2017 03:37*

**+jacob keller** Oh sorry, I was sure you had an LCD. The "play" from Pronterface won't work, send a console commands as said by **+Wolfmanjm**​. Get an GLCD working, it will really help :)


---
**Hakan Evirgen** *June 09, 2017 13:24*

**+jacob keller** does your printer work when you do not connect the USB cable? Do you have a 1 ampere voltage regulator soldered and it is working fine? If you had a 0.5 amp regulator then it may be broken as LCD need more power.


---
**Parth Panchal** *June 24, 2017 10:43*

If you print using the play button from pronterface(with gcode file loaded in pronterface) it will only work as long as the USB cable is connected.

You will have to play the gcode from the lcd(e.g. GLCD) 


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/gWSbS14WRUt) &mdash; content and formatting may not be reliable*
