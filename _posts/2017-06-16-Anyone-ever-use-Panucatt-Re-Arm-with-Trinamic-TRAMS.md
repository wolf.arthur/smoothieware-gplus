---
layout: post
title: "Anyone ever use Panucatt Re-Arm with Trinamic TRAMS?"
date: June 16, 2017 12:30
category: "General discussion"
author: "Dont Miyashita"
---
Anyone ever use Panucatt Re-Arm with Trinamic TRAMS?

TRAMS do use digipot to adjust the current on stepper motors but is it compatible with Re-Arm and Smoothie?

[https://www.trinamic.com/support/eval-kits/details/trams/](https://www.trinamic.com/support/eval-kits/details/trams/)





**"Dont Miyashita"**

---
---
**Dont Miyashita** *June 16, 2017 15:03*

I find that TRAMS use PWM pin to control each drivers current seperately, but Smoothieboard use different way to control it, maybe we need to assign spare PWM pins to control TRAMS?


---
**Jeremiah Coley** *June 16, 2017 19:56*

I am in the process of doing just that, I think hardware wise it will work. As far as, smoothieware, there is tmc26xx support and I have seen tmc2100s being used, but it is unclear to me if the tmc2130s are supported with all their functions in smoothie.

I just purchased another mega/ramps to first test it out in marlin, then I will move it over to the re-arm and begin testing the smoothieware.

Anyone else have them working?


---
**Jeff DeMaagd** *June 17, 2017 14:44*

It looks to me like unless you rework the firmware, you're bypassing the motion control feature of the chips, largely negating the reason to use those chips vs. a regular Trinamic driver.


---
*Imported from [Google+](https://plus.google.com/+DontMiyashita/posts/4VPjDXKDvco) &mdash; content and formatting may not be reliable*
