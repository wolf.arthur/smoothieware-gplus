---
layout: post
title: "Have you ever had a problem with your Smoothieboard ?"
date: June 16, 2017 10:14
category: "General discussion"
author: "Arthur Wolf"
---
Have you ever had a problem with your Smoothieboard ? It'd be great if you could share the solution you found, with the rest of the community ! You have the power to help a lot of people !



Just go to [http://smoothieware.org/troubleshooting](http://smoothieware.org/troubleshooting), and add your problem and the matching solution, it's easy and lots of folks will be very thankful !



Thanks :)





**"Arthur Wolf"**

---
---
**Joe Alexander** *June 16, 2017 10:29*

Lol I love the PEBKAC reference(problem exists between keyboard and chair) :)


---
**Anton Fosselius** *June 16, 2017 16:27*

In swedish we say a SBS (skit bakom spakarna) problem with translates to "shit behind the steering wheel"


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/KHuVLQmB7Nf) &mdash; content and formatting may not be reliable*
