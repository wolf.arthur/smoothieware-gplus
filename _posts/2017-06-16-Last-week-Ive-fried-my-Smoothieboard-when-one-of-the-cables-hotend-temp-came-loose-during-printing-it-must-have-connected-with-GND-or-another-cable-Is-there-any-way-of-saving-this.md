---
layout: post
title: "Last week I've fried my Smoothieboard when one of the cables (hotend temp) came loose during printing (it must have connected with GND or another cable) :( Is there any way of saving this?"
date: June 16, 2017 21:30
category: "General discussion"
author: "Oliver Seiler"
---
Last week I've fried my Smoothieboard when one of the cables  (hotend temp) came loose during printing (it must have connected with GND or another cable) :(

Is there any way of saving this? I have some SMD soldering experience, but never attempted to swap out anything with that many pins.

![missing image](https://lh3.googleusercontent.com/-pexjAorkyN0/WURN6lN5HmI/AAAAAAABCSc/UpcZ62_t15E5v1vbSt_RkVnPFw9YKOiWACJoC/s0/IMG_20170609_151909.jpg)



**"Oliver Seiler"**

---
---
**Steve Prior** *June 16, 2017 21:42*

I see residue off to the right - did something else blow there or is that from the chip in the center?


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 21:44*

If you want to even attempt that, I recommend using a hot air rework station with the proper (square IC shape) attachment. I've done stuff like that more than I want to admit to, it's a pain. 


---
**Oliver Seiler** *June 16, 2017 21:47*

**+Steve Prior** Not exactly sure what you mean by residue, maybe I should get a better photo. Here's another one I took further from the right. The rest of the board looks ok but I know there might be other parts affected. Just wondering if it's worth trying to swap the mcu and see if that brings it back to live. Looking at my config I believe it would have been Pin  0.23.



![missing image](https://lh3.googleusercontent.com/C16QwUtiAGqOGAFKmne3b07T-VMSWyz-o5wDxWiEtovuhGuV9QahI8lNsLmzxF6jw0V4rfWI86thuaA=s0)


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 21:48*

The other thing too is, there is no way to know if anything else got damaged. You may end up spending a lot of time chasing problems. It might be a fun exercise to learn more about electronics but you'll be without a working machine while you do that. 


---
**Arthur Wolf** *June 16, 2017 21:52*

Several users with the same kind of problem have fixed it by replacing the microcontroller and voltage regulator.


---
**Oliver Seiler** *June 16, 2017 21:58*

Good to hear **+Arthur Wolf**.

What would I have to do to get the mcu into the correct bootstrapping for Smoothie? Can I flash whatever is required after soldering it onto the board?

And lastly would this one work as replacemnet? 

[http://nz.rs-online.com/web/p/microcontrollers/7257742/](http://nz.rs-online.com/web/p/microcontrollers/7257742/)


---
**Stephanie A** *June 16, 2017 22:00*

Hot plate + hot air + low temp solder. I've done hundreds of devices like this. 


---
**Arthur Wolf** *June 16, 2017 22:00*

**+Oliver Seiler** After replacement you just do [smoothieware.org - flashing-the-bootloader [Smoothieware]](http://smoothieware.org/flashing-the-bootloader)

And yes, that's the right chip.




---
**Griffin Paquette** *June 16, 2017 22:51*

Should be a $10 fix in parts. Future electronics has the LPC's cheap:



Flash the bootloader, firmware, and use the same config. Just soldered one today with a pen iron. Hot air is better but if you have solder wick and are good at soldering either way will work. [futureelectronics.com - LPC1769FBD100,551 &#x7c; LPC1769 Series 512 kB Flash 64 kB RAM 32-Bit SMT Microcontroller - LQFP-100 &#x7c; NXP  
 - Future Electronics](http://www.futureelectronics.com/en/technologies/semiconductors/microcontrollers/32-bit/Pages/3715687-LPC1769FBD100,551.aspx?IM=0)


---
**Steve Prior** *June 17, 2017 02:34*

The area I circled in red looks like something bad happened.

![missing image](https://lh3.googleusercontent.com/sFiaV-nv9yqz9fPnUzoA9r0tUms15rMkLI7JmP8-AHkEyhQD-B4VcOqovNqCfiqf-tjIn89C857VntZsmkYiIBN-Cpn3l_07VJKB=s0)


---
**Oliver Seiler** *June 17, 2017 04:19*

Thanks for pointing that out **+Steve Prior**​ it's just a reflection or so, the board is perfectly fine in that area. Sorry for the the crappy photo. 


---
**Oliver Seiler** *June 17, 2017 07:50*

It seems that the 3.3V rail is also screwed. Anyone know where I can find the regulator (IC10)? I can't see it


---
**Ashley M. Kirchner [Norym]** *June 17, 2017 15:11*

It may not be, I've heard of LPCs failing and causing a dead short on the 3v3 rail. Remove the chip and test the rail again. 


---
**Oliver Seiler** *July 01, 2017 04:16*

Ok, I've managed to replace the MCU and uploaded (+verify) the bootlader. But when I boot it up with firmware.bin and config file on the SD card it doesn't seem to do anything (LED  1-4 dark, 3.3V working fine again).

I have checked the connections (SD_CS, SCK, MISO, MOSI) from the mcu pins to the sd card and they're all fine.

I'm wondering if one the other mcu pins didn't connect to the pad? Any pointers as to which would be the most crucial ones?




---
**Douglas Pearless** *August 04, 2017 08:37*

Do you have access to a Segger Link unit?


---
**Oliver Seiler** *August 04, 2017 09:02*

**+Douglas Pearless** not really :(




---
**Arthur Wolf** *August 05, 2017 09:22*

**+Oliver Seiler** If it won't work, you need to check all the pins, and another possibility is the oscillators for the mcu having a soldering problem ( try hot air on them ). Having the bootloader but not the firmware work, would point at a oscillator problem.


---
**Oliver Seiler** *August 05, 2017 11:42*

Thanks **+Arthur Wolf**, I'll check the oscillators. The pins seem all good, I've double and triple checked them.


---
**Oliver Seiler** *August 06, 2017 03:20*

There was a problem with the oscillator indeed and resoldering fixed it, thank you so much **+Arthur Wolf**!

The firmware is booting and I can talk to the board via the serial port. There're still some issues with USB and LCD, but they might also be driver/config related.


---
**Griffin Paquette** *August 08, 2017 16:35*

What turned out to be the issue specifically?!

Just ran into this same problem. 


---
**Oliver Seiler** *August 08, 2017 19:30*

**+Griffin Paquette** I had to re-solder the oscillator (silver rectangle top right of MCU in the picture above). It must have moved slightly when I replaced the MCU.


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/gVuxaFbST5B) &mdash; content and formatting may not be reliable*
