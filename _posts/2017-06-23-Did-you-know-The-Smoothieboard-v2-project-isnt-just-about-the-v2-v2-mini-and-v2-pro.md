---
layout: post
title: "Did you know ? The Smoothieboard v2 project isn't just about the v2, v2-mini and v2-pro"
date: June 23, 2017 09:31
category: "General discussion"
author: "Arthur Wolf"
---
Did you know ? 



The Smoothieboard v2 project isn't just about the v2, v2-mini and v2-pro. All of those boards will have a set of standard "gadgeteer" sockets on them, and the community has been designing a full family of extension boards for those sockets.

Want to add a stepper driver ? A thermocouple ? Plug in a raspi ? A parralel port ? A mosfet ? Easily connect to a laser PSU ? There will be boards for pretty much everything you can think of wanting to connect your Smoothieboard to. No more soldering, and all pretty much plug-and-play.



If you are curious, you can take a look at the boards list in the spec here : [https://docs.google.com/document/d/144EbmhN6z-J2V_Zw7GfJpZrfD-B0dC3cuea9oWPgxNM/edit#heading=h.8xx27lexvf4h](https://docs.google.com/document/d/144EbmhN6z-J2V_Zw7GfJpZrfD-B0dC3cuea9oWPgxNM/edit#heading=h.8xx27lexvf4h)



Oh and by the way : if you know kicad, we are looking for help designing the remaining boards, please don't hesitate to ping me at wolf.arthur@gmail.com

And re-share to your kicadian friends :p





**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/AWB3FRueZEZ) &mdash; content and formatting may not be reliable*
