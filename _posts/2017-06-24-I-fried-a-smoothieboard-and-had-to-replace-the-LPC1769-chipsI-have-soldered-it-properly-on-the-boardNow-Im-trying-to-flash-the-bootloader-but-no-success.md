---
layout: post
title: "I fried a smoothieboard, and had to replace the LPC1769 chips.I have soldered it properly on the board.Now I'm trying to flash the bootloader but no success"
date: June 24, 2017 09:03
category: "General discussion"
author: "Parth Panchal"
---
I fried a smoothieboard, and had to replace the LPC1769 chips.I have soldered it properly on the board.Now I'm trying to flash the bootloader but no success.

I'm trying to use Arduino as USB/TTL converter.

I've connected the reset pin on arduino to the gnd.

TX(smoothie) -> RX(arduino)

RX(smoothie)->TX(arduino)

GND(smoothie)->GND(arduino)

5V(smoothie)-> +V(arduino)

I've tried it with Arduino Mega 2560 and Uno but no luck.

Getting the same error 'Operation Failed. Failed to autobaud - step1'



Also,Is there any indication that the smoothie has entered the bootloader mode?







**"Parth Panchal"**

---
---
**Arthur Wolf** *June 24, 2017 14:45*

There are lots of guides on using the arduino as a ttl converter, I recommend you follow one of those, it should just work. If it doesn't I'm not sure what is going on.

You'll know the bootloader works if it flashes firmwares when you try to do so.


---
**Parth Panchal** *June 25, 2017 13:01*

Thanks. I figured out the problem.

I used an old Arduino with an FTDI chip.The new ones have ATmega16u2 which were giving problems.

The board works now.But won't read any temperature.

READ: ok T0:inf /0.0 @0 B:inf /0.0 @0 

I tried changing the input from pins 0.23 along 0.26, but still the same.I also verified the C6 capacitor and RN1 Resistor for 3.3V and they are getting proper voltage.



The LPC1769 had fried cause of a short circuit, so i believe something in the circuit might be damaged.

Are there any other test points that I should

 look for? 


---
**Parth Panchal** *June 26, 2017 08:17*

Fixed the problem.Just in case someone finds a similar problem, I am mentioning the solution.

The short circuit fried the LPC1769, but along with it, one of the printed tracks connecting the AGND or GND to C21 through C24 had opened up. A quick fix is to put a jumper from and of the four GND pins of thermistor inputs to the GND at the main 5V input(the 4pin molex connector used for VBB.


---
*Imported from [Google+](https://plus.google.com/105671120468789536944/posts/UiXWeXUfeUx) &mdash; content and formatting may not be reliable*
