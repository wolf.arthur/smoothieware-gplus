---
layout: post
title: "Shared on June 24, 2017 19:31...\n"
date: June 24, 2017 19:31
category: "Machine showcase"
author: "Arthur Wolf"
---






**"Arthur Wolf"**

---
---
**Andrew Hague (Old English Workshop)** *June 27, 2017 11:49*

Looks good. I am upgrading from my Shapeoko to a Gatton CNC. [davegatton.com - David Gatton](http://www.davegatton.com/building-a-gatton-cnc-.html)



I am thinking of using an Azteeg X5 GT with the Bigfoot drivers and nema 23 motors. 



Is there any benefit with the V2 for CNC routing?


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/JrBvcoYdF22) &mdash; content and formatting may not be reliable*
