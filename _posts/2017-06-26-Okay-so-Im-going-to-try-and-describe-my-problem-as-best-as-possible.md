---
layout: post
title: "Okay so I'm going to try and describe my problem as best as possible"
date: June 26, 2017 20:03
category: "Help"
author: "Blake Dunham"
---
Okay so I'm going to try and describe my problem as best as possible. 



Im using Bigfoot SPI drivers on my X5 GT and the second extruder refuses to move. What I think may have happened is that particular channel has been fried but im not sure how to tell. 



I've tried changing the pin's in the config file and it still wont move. Im trying to see if its a wiring issue now but in the meantime, does anyone have any ideas? 





**"Blake Dunham"**

---
---
**Joe Alexander** *June 26, 2017 20:55*

try wiring the motor that doesn't run to one of the drivers you know is working to test if its fried or not. If it works then chances are something is, if it fails then it must be a bigger issue(bad motor, wiring, config, etc.) first the easy test then worry if its bigger than that :)


---
**Blake Dunham** *June 26, 2017 21:00*

Tried that, the motor does turn...unfortunately. It doesn't seem to be a wiring issue either


---
**Joe Alexander** *June 26, 2017 21:09*

well next best thing to do would be to post pics of wiring, your config file settings, and maybe something will stand out. Also screenshot your LW settings, helps to know.


---
**Ray Kholodovsky (Cohesion3D)** *June 26, 2017 22:23*

Joe has been doing a little too much Laser support these days. Blake has a 3D printer, a really nice one I'm told, that's running this. 


---
**Blake Dunham** *June 26, 2017 22:30*

No worries. I've been looking into building a laser engraver/cutter. I'll get those setting posted here in a bit. **+Ray Kholodovsky**​ I'll get you pictures of those driver's soon. 


---
**Joe Alexander** *June 26, 2017 22:51*

indeed I have, was meant to be SW settings but defaulted to lasers.


---
**jerryflyguy** *June 27, 2017 17:17*

I'd post the config to get a second set of eyes, I know there was a tiny error on mine that **+Roy Cortes**  caught and once corrected it worked.


---
*Imported from [Google+](https://plus.google.com/107526587910712362690/posts/FaMgkXyvULe) &mdash; content and formatting may not be reliable*
