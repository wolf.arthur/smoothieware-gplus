---
layout: post
title: "All, I have seen references to a change in wiring for the FSLaser Gen4 hobby laser, but when I try to dig deeper, nothing pans out"
date: June 27, 2017 22:36
category: "General discussion"
author: "Mike Mills"
---
All, 

I have seen references to a change in wiring for the FSLaser Gen4 hobby laser, but when I try to dig deeper, nothing pans out. 

My question is: Has anyone installed a smoothie board into a Gen4? I just want to make sure I don't smoke anything.... 

TIA





**"Mike Mills"**

---
---
**Ben Delarre** *June 28, 2017 02:11*

I installed a cohesion3d Mini in mine. Went flawlessly. I just chopped up and repinned the existing connectors to fit.


---
**Mike Mills** *June 28, 2017 07:58*

OK thank you! 


---
*Imported from [Google+](https://plus.google.com/115297742224179054625/posts/Hjhj9Zi86j4) &mdash; content and formatting may not be reliable*
