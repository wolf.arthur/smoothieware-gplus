---
layout: post
title: "Robotseed manufactures, distributes Smoothieboard, contributes to and supports the Smoothie community"
date: June 29, 2017 13:17
category: "General discussion"
author: "Arthur Wolf"
---
Robotseed manufactures, distributes Smoothieboard,  contributes to and supports the Smoothie community.

( Warning, kinda ad, promise I won't do too much of these, but it's relevant ).

These past few months, many Fablabs/Hackerspaces in Europe have also bought from us laser cutters and CNC routers pre-transformed to use Smoothieboards, and improved in other ways ( safety, better interface most notably ).

We want to get out of just getting sales from word of mouth, so we put the machines up on our website : [http://robotseed.com/index.php](http://robotseed.com/index.php)

If you are interested in a digital fabrication machine, we do lasers, routers, cnc mills, vinyl cutters, giant 3D printers, and we can look into other types too.

If you know people who might be interested, don't hesitate to point them at us too. 

What's great about this, is that the real-life experience of installing Smoothie into these machines has been a great driving force in the development of new features and boards ( some specs of the v2 series but not only ). It also gives us more means to push the Smoothie project forward and support the community.

Noticeably, counting all costs ( transport and instruction ), our machines are only slightly above what it'd cost you to buy a non-Smoothieboard Chinese machine from an importer ( with all the well known user experience this entails ), and much less expensive than buying a US/EU native machine. But the improvements we add to the machine also make them very significantly better than a Chinese machine.

We are also working very hard on actually making the design of the machines themselves Open-Source. We can't do it for the machines we sell now, but we are working on designing our own models, and when that's done, we'll be able to sell the first fully Open-Source, non-kit, professional laser cutters and CNC routers.



Don't hesitate to re-share, it'd help a lot getting more people to know about our offer, one thing we've noticed is we talk regularly to labs or small businesses who learn of our offer and are pretty upset they didn't know about it sooner since they bought a more closed machine ( often at higher prices, or with a poorer UX ) in the meantime. So help you machine-searching friends out :p





**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/TuGweiEh2T2) &mdash; content and formatting may not be reliable*
