---
layout: post
title: "For my smoothieboard 0X, i solder Voltage regulator R78-E same arrangement of my other smoothieboard like wiki documentation But when i plug 24v VBB no led start, no led red VBB and other green led Just works after when plug"
date: July 01, 2017 19:39
category: "General discussion"
author: "Christian Lossendiere"
---
For my smoothieboard 0X, i solder Voltage regulator R78-E same arrangement of my other smoothieboard like wiki documentation



But when i plug 24v VBB  no led start, no led red VBB and other green led

Just works after when plug USB

But if unplug USB after play gcode the CNC stop



So can use Voltage regulator with smoothieboard 0x or not ?

Have something need to do for this to regulator can work ?



Or need have other power supply 5V ?







**"Christian Lossendiere"**

---
---
**Douglas Pearless** *July 02, 2017 03:18*

You also need to add Diode D6 which is an MBRA210LT3 which supplies VBB to the regulator and C99 0.33uF and C76 0.1uF otherwise it won't work! (refer to the Smoothie BOM on the git hub site for more details on these parts.


---
**Christian Lossendiere** *July 02, 2017 09:12*

Thanks Douglas




---
**Douglas Pearless** *July 02, 2017 09:13*

:-)


---
*Imported from [Google+](https://plus.google.com/113177179325470084866/posts/K4zquCtKcRY) &mdash; content and formatting may not be reliable*
