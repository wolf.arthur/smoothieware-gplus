---
layout: post
title: "Does smoothieboard 5xc support 8Gb uSD Cards?"
date: July 05, 2017 12:58
category: "General discussion"
author: "Parth Panchal"
---
Does smoothieboard 5xc support 8Gb uSD Cards?





**"Parth Panchal"**

---
---
**Arthur Wolf** *July 05, 2017 13:18*

Sure


---
**Chris Chatelain** *July 05, 2017 14:05*

I've got a 16gb in mine right now


---
**Parth Panchal** *July 06, 2017 06:58*

I am using a 8Gb card in mine but for some reason whenever i connect the USB cable to my PC from my smoothieboard, the controller stops responding.

Only LED 2 and 4 remain stable out of the 4 LEDs. I tried changing the uSD card and flashing the latest firmware. No luck.


---
**Parth Panchal** *July 06, 2017 13:11*

also my external SD card was working all this time.I've had more than 200 hrs of printing using only the external SD card. But suddenly now along with this issue the external SD also stopped working.

It detects the SD card, it show ext/ in the 'Play' menu, but once I click on it, its appears to be empty.

Really, problematic.Have 2 smoothies and no way to print. I actually have to remove the onboard sd card and put the files in from my PC and start the printer every time i want to print.




---
**Arthur Wolf** *July 16, 2017 12:00*

Hey.



SD cards are <b>not</b> designed to be used over wires. Smoothie contains the code for external SD cards, but it is so common that users have problems with it ( because it is not the way SD cards are electrically meant to be used ) that we do <b>not</b> support officially using external SD cards. Users can do it if they want, it works for <b>some</b> people, but we can <b>not</b> support it officially.

If it worked before and stopped working now, it is possible some very small change in your EMI environment is preventing it from being reliable or working at all. As I said, this isn't how sd card are supposed to be used, and it is quite frequent for users to have these sorts of problems.

Smoothie offers many other methods : USB, Ethernet, UART, on-board SD card. All those are supported. Only external SD is not supported, for reasons we have absolutely no control over, and can do absolutely nothing to improve.


---
**Parth Panchal** *July 17, 2017 09:30*

For the same reason, I am trying to connect over USB, its does work well sometimes, but quite a few times it has happened that once the I connect using the USB, the 4 LEDs on the board stop in the position the were in and after a few seconds, one LED 2 and 4 remain on and the printer hangs up completely, until I restart the board.


---
**Parth Panchal** *July 24, 2017 10:25*

**+Arthur Wolf** What does Led 2 and LED 4 ON indicate?

Can't find it on the website




---
**Arthur Wolf** *August 08, 2017 15:48*

Led 2 blinks when things are going well, LED4 is off if there is a SD card problem.


---
*Imported from [Google+](https://plus.google.com/105671120468789536944/posts/5gLBtMCiiQT) &mdash; content and formatting may not be reliable*
