---
layout: post
title: "Is there anyone in this group and in the states familiar with replacing MOSFETs and willing to replace a few on my board?"
date: July 06, 2017 02:30
category: "General discussion"
author: "Mark Ingle"
---
Is there anyone in this group and in the states familiar with replacing MOSFETs and willing to replace a few on my board?  I am going to attempt to do it myself but I need a backup plan incase things do not go well...







**"Mark Ingle"**

---
---
**Alex Krause** *July 06, 2017 03:23*

**+Ray Kholodovsky**​?


---
**Douglas Pearless** *July 06, 2017 03:24*

Why has happened and which MOSFETs (etc) are the problem, or are you trying to increase the current for the MOSFETs (in which case it is better to externalise these or perhaps us SSRs for this purpose)


---
**Mark Ingle** *July 06, 2017 03:52*

I burned up the fet on P2.4 using a regulator to drop the voltage to 12v for a high speed fan. I have a 24v PSU. I confirmed it's shorted with a meter


---
**Douglas Pearless** *July 06, 2017 04:04*

Interesting, is that an early Smoothie board (or are you using a C3D) as the later revisions have a protection diode soldered to the board.  Easiest way is to use a hot-air soldering iron with a very focussed nozzle to heat up the part and then to pull off with tweezers, then use solder braid and a soldering iron to remove (wick up) the solder from the pads and then hand solder in the new part.  When you have removed the MOSFET, also check the protection diode, D7.


---
**Douglas Pearless** *July 06, 2017 04:05*

Also, get a 24V fan!!!


---
**Mark Ingle** *July 06, 2017 10:04*

Yes it has the protection diode. How do I test the diode? Resistance?  I found a high speed fan from dig-key that is 24v. 


---
**Douglas Pearless** *July 06, 2017 10:10*

If you have a multimeter, the continuity tester function is usually marked with a diode symbol indicating it is also a diode test function too.  Basically the test function should allow current one way and if you swap the probes around it won't allow voltage to pass, does that make sense?


---
**Douglas Pearless** *July 06, 2017 10:26*

![missing image](https://lh3.googleusercontent.com/b3Alh9-fxza0p_YeOOUnBR7mh69vHNAOJP_eEUwABF_crPu71_jW2ZSTfYH-gymntVGlVyN5SQ_lB6_ytiIfxJSss41PWMRX4Wo=s0)


---
**Douglas Pearless** *July 06, 2017 10:26*

 It's the diode test function


---
**Mark Ingle** *July 06, 2017 10:43*

My multimeter does not have a diode test function. Just volts ohms and amps 


---
**Douglas Pearless** *July 06, 2017 10:59*

Ok, measure the resistance ( ohms ) across the diode. One way should give you a low reading and the opposite way should give an extremely high or even off the scale reading.


---
**Griffin Paquette** *July 06, 2017 11:50*

I have a hot air station if you want them reflowed. I don't have FETS on hand but if you send me a couple through digikey and the boards I'll definitely replace them for you


---
**Mark Ingle** *July 06, 2017 12:12*

**+Griffin Paquette** Thanks for responding!  The MOSFETs I ordered should arrive Monday.  Ill remove the MOSFET tonight and post pictures here as I work through the process.  Any and all feedback is welcomed.  If things dont go well I will send you a PM.  I am in Fort Mill SC.


---
**Griffin Paquette** *July 06, 2017 12:14*

Sounds good! I've done that size with a normal iron and they weren't too much trouble. Let us know if you need any help/advice


---
**Ray Kholodovsky (Cohesion3D)** *July 06, 2017 13:33*

Cut legs, clean pad, tack solder new one on. 


---
**Mark Ingle** *July 06, 2017 14:57*

Thanks guys!  I found this in the troubleshooting sections - [http://www.smoothieware.org/troubleshooting#i-burnt-one-of-my-mosfets...posting](http://www.smoothieware.org/troubleshooting#i-burnt-one-of-my-mosfets...posting) here for the next newbie!  :)

[smoothieware.org - troubleshooting [Smoothieware]](http://www.smoothieware.org/troubleshooting#i-burnt-one-of-my-mosfets)


---
**Mark Ingle** *July 06, 2017 22:37*

**+Griffin Paquette** testing the diode shows that is is bad...I get same reading in both directions....compared to the reading of P2.6!  Which is working!  I do have some through hole diodes (Same used on my 5x smoothieboard).  Can I install the through-hole diode on the P2.4 circuit?  If I can should I remove the SMC diode on P2.4?  Thanks in advance for the help!




---
**Douglas Pearless** *July 06, 2017 22:46*

Yes, remove D7 and immediately behind the X7 connector (where you have plugged the fan into) are two pins designed for a through hole diode.  Check out [smoothieware.org - spindle-mosfet-control [Smoothieware]](http://smoothieware.org/spindle-mosfet-control?s%5B%5D=diode) and more importantly here [http://smoothieware.org/3d-printer-guide?s[]=diode#fans](http://smoothieware.org/3d-printer-guide?s%5B%5D=diode#fans) which shows you how to install the diode and its correct orientation.


---
**Griffin Paquette** *July 06, 2017 22:57*

Agreed with douglas.


---
**Mark Ingle** *July 06, 2017 23:53*

![missing image](https://lh3.googleusercontent.com/Zlihc7yuR1GOuhjWhC4nqR3NY4BoruxJggWnRRosfLycVZk3kLF5qYFJ9rD7gl6MlJ31gcxh7eS8DhkT29mmPaC_1UO6oHBXT_k=s0)


---
**Mark Ingle** *July 06, 2017 23:54*

I'll need to use hot air. What temp?


---
**Griffin Paquette** *July 07, 2017 00:36*

I say start 330C and work up. 4 layer is a pain to solder. Be careful with any connectors near it. 


---
**Mark Ingle** *July 07, 2017 01:12*

It was brutal!!!  My board has a few scars and I lost an LED but I think everything is okay. The FETs were easy. But I didn't give the diode enough thought and forgot to trim the leads causing cold joints etc. Any I got it on.![missing image](https://lh3.googleusercontent.com/i3Rcgnno6hr89IMcPIrmhMaRNiburrq9XgIQOkl2LLJ9qzC8G-muja8wPUTGNFvwMj6fPkW43yTfTNBhMcknqa4eQ1uLQu46FHk=s0)


---
**Mark Ingle** *July 07, 2017 01:12*

I'm concerned about back ![missing image](https://lh3.googleusercontent.com/q1p6qZIga4ApYDobwm1eIP8fCEPpjWJRUrE3bPZFlMVevQTQt_kiEEuD6DznoofWEhM3kUU9i_fPpOt28UPg2ffrg_ET7EO5S-I=s0)


---
**Douglas Pearless** *July 07, 2017 01:15*

Suggest you resolder the Diode, and check for shorts etc before you power up the board.


---
**Mark Ingle** *July 07, 2017 15:04*

Prilimary power test was successful. I'll add the fets Monday and test again. Fingers crossed!!!


---
**Douglas Pearless** *July 07, 2017 22:25*

Good to hear 😄


---
**Mark Ingle** *July 11, 2017 02:09*

Well looks like I ruined a perfect new 4x...this is really depressing!  both mosfets fail the diode test.....the VBB led actually comes on! (barely).  Heres my solder job...feedback welcomed!  Gotta figure out what do do next....

![missing image](https://lh3.googleusercontent.com/e3U56txlcjUFq_29orOBW4pwc-4QCkRTiPmoK8C-zIKiG9KvmXwT7NuZCHd4P5Sx_P0wrlbXtPLFCFHkjlaHHRXQ7Sq-AGwDFDs=s0)


---
**Douglas Pearless** *July 11, 2017 02:13*

Can you highlight the components you added and can you take a photo sideways showing the orientation of the diode you added behind the green connector


---
**Douglas Pearless** *July 11, 2017 02:14*

[c2.staticflickr.com](https://c2.staticflickr.com/8/7284/15838800004_d94bd040f9_b.jpg)


---
**Douglas Pearless** *July 11, 2017 02:15*

Is the through hole diode the correct orientation so the white band is closest to the "+" on the connector?


---
**Mark Ingle** *July 11, 2017 02:24*

![missing image](https://lh3.googleusercontent.com/TlNYtIuVeDEG5q21g0HWAFuTTSxbgfDzZBT5VGunmB-UVozb4BFl5j00YaeXYjYx26b74-TNXbZOlSByQWTxbv_Pbe5qHfqrXtY=s0)


---
**Douglas Pearless** *July 11, 2017 02:28*

They look OK, if you put a multimeter set across the terminals (green ones) who happens when you turn the MOSFET ON/OFF via g-code commands? (does it go from 0V to 24V and back to 0V?

Also, did you use the MOSFETs per the Smoothie BOM, which Diodes did you use?


---
**Mark Ingle** *July 11, 2017 02:36*

I tested each fet with my meter.  Source and drain stay connected.  I cannot get the gate to open/close.  Meter shows 24v as soon as I turn on the PSU... I ordered from the BOM.  I will post a picture of my digikey invoice






---
**Mark Ingle** *July 11, 2017 02:37*

![missing image](https://lh3.googleusercontent.com/T75OI2ipyAkNWf4zIb8ggf_UEKg-lJA8onsYTZSYyiw-Hm5UB_FWHrbPLuT42X8_k6VSSdAMvtgnxPLPylqBOb6NtdJ5Fizvy68=s0)


---
**Douglas Pearless** *July 11, 2017 02:41*

![missing image](https://lh3.googleusercontent.com/g76TN6_BoI9gtb8EO95vOeikAshqTdpyp02XTG2zLK5wWtz41uIMcE2E963rta-bkm_YgUwF2mCwBOqgdEHkh8aPWAo1a0cWJj8=s0)


---
**Douglas Pearless** *July 11, 2017 02:44*

The bright red trace shows the pin P2.4 is available at, that should nominally be LOW so the FET is off, can you check that, also note the two red pads which route this via R73 (22R) to the Gate, can you check both sides of that resistor and make sure it is not shorted to a nearby trace, same with R74 (10k) which holds the Gate low (to GND).  You should be able to use a 1k resistor and short that pin to 3V3 and the MOSFET should turn on (1k to limit the current).




---
**Douglas Pearless** *July 11, 2017 02:51*

(Yep the MOSFET part looks correct)




---
**Douglas Pearless** *July 11, 2017 02:53*

![missing image](https://lh3.googleusercontent.com/o2gQblE2-igrQCtQ3-8s1c6lZyy5qqD6QiSRK4RM2W2NsNXK8YOJZstO7ZgX2aWQzlm52PD6FKDj75qPaKpB3L444z-1kxuQwF8=s0)


---
**Douglas Pearless** *July 11, 2017 02:53*

This is the part of the schematic we are talking about




---
**Mark Ingle** *July 11, 2017 03:25*

**+Douglas Pearless** Iam not completely sure how to check with the meter (I tried).  I do understand that when a pin is LOW it is connected to GND.  Just not sure how to check that with a meter.  I will research it though....I really appreciate your help!!!!  Big time!!!


---
**Douglas Pearless** *July 11, 2017 03:28*

![missing image](https://lh3.googleusercontent.com/geniOQefvmA23yBsx9WxS8lw7hIx8XGkwsBInkTETBEH9NEoYnDyhL_6_Ex6K1qvawL3VxVcyi9XZHER72jgKueZvg1_XofzQE8=s0)


---
**Douglas Pearless** *July 11, 2017 03:30*

Measure the voltage between the bright green pin (P2.4) and the nearby pin (GND) and it should be close to 0V I have highlighted them on the picture




---
**Douglas Pearless** *July 11, 2017 03:31*

If not, then something is stuck "on" and we need to figure out what and why, can you post your entire config file as well as it may be something in there.




---
**Mark Ingle** *July 11, 2017 03:49*

Okay yeah I figure out how to measure.  It makes sense too.  I have voltage on 2.4 and 2.6.  I compared to 2.5 and 2.7 which are only .001

![missing image](https://video-downloads.googleusercontent.com/ABzBgw2TuvI7xSObhcQIVnrGfx7jSednlkQZuCwvQYT7Bv37a75dZK5d98uUQbH0uNlAnv8Ycfd7lWBC5b-Wgc-Ljadixdz8lel_hEZ_EZ7DoW96v8Zua7Upg_cBqrjhA6Oo-63J1-m86pz42YADGEn0c5q4Uc9NRX6-B7IPaHRwFpqpbPb06DoPgWxabtXDFTOhQiI8luttNoqW9ZwHl8fmZ8Nx-VZ8CekRV3DaKzZp4GUcDz-JcJI4NIyn4eYzHgu3vkE64ASrd0myCgjTniBkmoDIDaRnNuXs007ll9gmK7_IO9JTaEb0HVnPJiaoI0y1sbhfiVUcH98ljnnxNI0ZWm0xlXFX37AzfTKdAhFP2jAD-jD7KsBInCQ6Zx8__pn2hqyo6e9pKXTwm2shD1Wv8G9-wjPJ_Z7ijdXLiiY45Wa47JpyH-VsDIKyrQxO4RKoRYGEDKYRmE8pJOSRrEYW48P2GXEShj1E_b2cGWImhqX5TYSrK8CTrZATjpWOFyKVTvNNZQaUu7ygytrzEBp4DLKLspX2t754a8cllHpPfM3oZtyGIFaOTUqqiyZX5IbxdX3Q6Iq8WzRGNgQKPkolqm2QaKQsJGqK3mao0A5OQAAMCV0CAsbNZZDT8cd_cOhCPQJR-5yr3bXeS06YmihXjX6ty-X8pbfwTkyDXrTBhTXKeqboilk90A3OADZQSmY3WZnEfnGwPMBTQ_IlLHJsiRxpcA4tleZJAItmQOSw9ShbqxFY07h-jtTyjWgfJENDEug1cinDjGvb5cgF1Ze85tR0w0TcwZjijZn4MmTab3ahpoeL6QE)


---
**Mark Ingle** *July 11, 2017 03:49*

![missing image](https://video-downloads.googleusercontent.com/ABzBgw3QmKRKqKTPbn92Zz8KkBouSEQtwMvd6ItiqFyTxzIGKWZC1I3koKTUNsssYRihWrVIqZ5dxyPKP0KPKD3uA4rAfGUPaHW6KBrKtO6XtWkV2DyMP1fR7h2IwJiFRBADF7JdO96HPO-IfBuS205Ehh8JDabI42Nyn2ywGg9X4FoHA5XYwbePy5YPQ_ZllmoqGe9ukys9E00Ay7uNkw4AGh-U_w7eg14M_zQfCWa5lq78HddRk0KWR2WFlQKoZJMzQXX0JPWj9ByJPs3ytip4BbEN9an8fdf567MG4UR2Rw2J376I2jF7m_5YoNzVRX7N3t_vLFkNzKCkfNIWMzvKGOeTT8Mc-TLJE6_h5vK4iTkp02UbMRr6PlaiA1ehaU8_eFnxv1EXeyLiwpO3nsDaHqq7TLmNAgLgQ8zIKRpSOmQEhMnegDZPLPmgZtQg29fdhzVOgUlIZfz4NFV7RzvjMQhIgi4icR4ql5Uf-QtSaxyee0duBLeySpf9zQXaoQz3ZYx983o25_HOeMOsZDo7oTuyT7dYbdlc7CipUGqu1NruIYLLpB0ZfN3JvEz308k4alV_meVWK9VQGixGIOfXODJb0HJwr7PmwkInZOqqOmT629Ib3NL7UYZpwoT0sEulllMa_RuGQsOZtV62GHDqLHmNaz_wE0UPRY0cySuGr4XnLc_QeLqJWLSJPF9mC01E8sEtPJNtsQOsLrAl42DSPhUxfcX52Hxb-afpOCI000wFmHDE1ZmoeoM7YjUlsGyuU4ulax0G25_CVVmtI61uNpe8dCC4iQloFfXJAeALQUeOV41NR2Q)


---
**Mark Ingle** *July 11, 2017 03:52*

Note - I don't have the USB connected so the firmware is not booted.  So something is stuck on....I will download the brd and sch files and review during lunch.  




---
**Douglas Pearless** *July 11, 2017 05:53*

OK, sounds like it is most probably and issue with the soldering and something is shorted. Most likely candidate is the 22R resistor, of the MOSFET itself.  If you carefully remove the 22R resistor and measure the voltage between the gate and GND is not really close to 0V, then it is most likely the MOSFET soldering that is the issue, you can confirm that by then also removing the 10k resistor so the MOSFET is isolated and re-run the measurement.  If the issue goes away with the 22R resistor, then check the PIN at the header I showed (check the PIN against where the 22R resistor was) is also at GND, if it is not, then it is further up the circuit; let me know how you get one.




---
**Mark Ingle** *July 12, 2017 00:54*

I am gonna remove the mosfet first.  it looks like I have excess flux under the mosfet.  May have used too much.


---
**Mark Ingle** *July 12, 2017 01:48*

I removed the two mosfets.  The 22R resistor would not lift.  maybe its a different solder used.  Anyway I tested with the mosfets off.  2.6 was .001 and 2.4 was .005.  I am not sure what that means but they both should be consistent.  Also the black silkscreen is starting to lift.....I think I am going to install an old MKS board to get my printer going.  Then work to install components and connection for X9 (bottom of picture).  I wish I had thought of this early on.....  I can use 2.4 to power my cooling fans for the hot end and control board fan, they run all the time anyway.  Then get X9 working to cool the print and enable bridging

.....of course I need to review the wiki to find out what controls X9 but it should be possible.  Big thanks to **+Douglas Pearless** for his help...I couldn't pull it off but I learned a lot!  Thank you very much!



![missing image](https://lh3.googleusercontent.com/K0weM0sgEDmtBUpHJE03lb59DtaeADUWC9M0sLUrrRy4vL0HRFV11rmXR7POXT1g3rLn2cYmdPg11-3RiEuB5YLahzsWPQr8PLs=s0)


---
**Douglas Pearless** *July 12, 2017 03:09*

OK it looks like the problem is the soldering around the GATE of the MOSFET as the copper around that pin is actually the 24V VBB which any solder bridge will have the MOSFET turned on (and may even destroy the MOSFET, but hopefully not), the first picture shows the PWM0 trace from the LPC1769:



![missing image](https://lh3.googleusercontent.com/SAehkDwXPOBhlp9VCmEubJkmwMkCRdpVoYShxF08FLbBxHs4HjV7b6-kXD6BBSvKii0YTQy8QGlWaVkZlg40UC3CAEpk1NHIqmc=s0)


---
**Douglas Pearless** *July 12, 2017 03:09*

The next picture shows the trace going to the GATE of the MOSFET:



![missing image](https://lh3.googleusercontent.com/tRjZLTGKJn88RslOBPKEtP0Pk3zcc22bFaZcMjxThbcZNEk4R_X9lU3H5rxSKlVgKus-gui6HLzBP7rJAneJ8hsQEA99Scqg7vk=s0)


---
**Douglas Pearless** *July 12, 2017 03:10*

The final picture shows the 24V "plane":



![missing image](https://lh3.googleusercontent.com/rerIjDMQ1mShzK0oXA5LxgluZx_JeH5rn8N8JtIp0S--BxhxZAUwM_ioAMgBexEMIzxikF8kfuk_R6tDH6kM0maeYjMVzJWcDKc=s0)


---
**Douglas Pearless** *July 12, 2017 03:11*

so you can see any excess solder can very easily create a bridge to 24V and I think that is exactly what happened when you replaced the dead MOSFET(s) :-)

So, resolder CARFEULLY and use a desoldering braid to wick up any excess solder and check for shorts in this area, good luck! 


---
**Mark Ingle** *July 12, 2017 03:14*

Thanks **+Douglas Pearless**!  That makes sense.  I have some really good copper braid that soaks up well.


---
**Douglas Pearless** *July 12, 2017 03:15*

Great, let me know how you progress, would be good to get you running properly :-)




---
**Mark Ingle** *July 12, 2017 22:21*

Cleaned with contact cleaner. Getting ready to clean up pads. The Via under drain is not exposed although it looks that way in the picture. There is a spot by the gate but I cannot tell what it is![missing image](https://lh3.googleusercontent.com/5Wv5sitwwbHQ206XRBMnMFE_9PB03wcvU2cZujOma5iPAKXZlO3pzgu9-G80GBOWhhcFH2PGeiTUrhjuIXcTnO8Qj_h65MJRLAk=s0)


---
**Mark Ingle** *July 12, 2017 22:23*

The drain via is exposed on q9. Loose the black silkscreen too. Gonna clean up the pads and reflow withmuch less solder. Fingers crossed. ![missing image](https://lh3.googleusercontent.com/k2R-2DV_V4sOxmbxNIUcfHraYZivVvbP02axtCD6KjFE4lUHObtSPcBHsW-EFtfsZEpkwgfJPgTbEgtPB0Lt8a6OgASpC2AMHoI=s0)


---
**Douglas Pearless** *July 12, 2017 22:27*

That may simply be some silk screen to mark the orientation of the part.  Suggest your reduce the excess solder on the pads and hand solder the tab first, then continuity check looking for shorts and then proceed with each pin checking for shorts after eacd solder; user the finest solder you have (0.56mm diameter would be ideal).


---
**Mark Ingle** *July 12, 2017 23:39*

I only have 1mm solder.  If I get shorts on the first one i'll order some .5


---
**Mark Ingle** *July 13, 2017 03:24*

Thought I would test the fet pads before going any further tonight and found something interesting (maybe).  With no power source on VBB and using the diode test setting the VBB led lights ups when source and drain pads are connected using diode test.  Fan LED lights up when gate and drain are connected.....this doesn't seem right...does this mean there is a short somewhere?  See video  The fet I re-soldered does the same thing....

![missing image](https://video-downloads.googleusercontent.com/ABzBgw3nPIh9OynWioxRcy9wMCGm6xIpj98JSh1PGgBhDCUcOwVGnq3yeTSdrsGWYw2_kHceMz5ZF2Z6D4XCt9cTIwB0JVFAOqLPxiSNR_weVnrswgYZcH0AOemllDZwe_ipY1NMJ4Xu9b_r7CWQ1zm29Xk7H0nmDWq9Xuys9RzU4HIX-wgecojxzZt7ZNE-vnZHlI-853pPlWOD6vPFKLDpEPQsO35MjAiQt4TsXC9KrYaVNa6W8Dl34n7Nvod3V2-52h3xQgiHvlMjlt-OB2B0MRgV3cpzlY8FdcdrdaJeodDZmU4hCOxZoh2KzlhwoevavIEi51bNth9uvTbvtIuhwIRvIyUsdAaeC2cvWKSXiYyRa7NEd3r7l8K2nzTHs_aN4o_Zbeov1gOQT93ZKe6D6HsxSpVMZg6BdXKRfklROTBbViYjFKDbHKOcJiIo5MnH9QFsEUBS3e_UmfRsSrnPN2KGDgtCtCegt0foCfTSnZF9bRArsm47T0K8w770malUrAozFPn8WulEIzPGkU-5YbUUb2pp_l4wbx-OhvQmDBkGMA0Hkpc0-Q5riS3lgG2-4cfYSBLl1SGluOjbVlfsz20t8jMIjAdG7Q8wUW9fDAOop-4X9ZHZb4kMS9OjYdsrmwB2iSOMxa0menE11mn_Dnc5Yh7LQe4fABAc4VfMQGf0orONLoEwQ2GP7DfErh3C-SHs1FCjYCByu34EVwRq2omT7l2Kv01kcRCxqgi0IbZobJyPEQerQAOyPr4domK_St-8ehxTdlpiNGvocJWr8MijA1hf01g473dOENdfqk2_Zs6tyD8)


---
**Douglas Pearless** *July 13, 2017 07:10*

Um, that is what I was expecting as there is usually just enough voltage in the continuity test to power an LED.  Unless I am short on coffee, you should be good to go (fingers crossed).




---
**Mark Ingle** *July 13, 2017 22:44*

This looks promising!!!! :)

![missing image](https://video-downloads.googleusercontent.com/ABzBgw244kDRxkOe1Daungm7B2MXd-MOdqctd1n5BhUDg0tOAvQrQPtCFJx_IJDA9DcyRHeyBmYO8xmw0eFpjuEvtgIsLiLi_Lkg4pFgUqtdIHJuqns9JrlA-T6GO1UGfVZJRo_jetTvcTI8lEfPFLPAzpHiFjbqCKFiF07rYTrzeUFvMUTPHgW3jIVqxwRc2HRsd52ZxZClRbxDaut361ZIxRd48wM6ZVtnSIIPqSnYF0pVwWbsSZ-WixHQjJwzSnzD4CqUB2eODx0WZ_Dkci0PXmHoFX3fiGABaAySWVmE5fEjh5410XhHoHtffqDtXEWX5IbCwZLwa0SeDoUk14iAqStpEzeWVlEth5wMcTm98VIsJjVZpRVLKuN8FZf_hFkGp_xnEdo4vgMIx0OhEv-JVfvhQnnyd339ClG9iFtkDfrMkYa0xy4klnkF4I7ig3ElZKlCqGv6MDDsn2T9FIW40oz6omBuZWNfkE5CKon3DIzJTUpOQ_E5299su40px_DdXsY7IdsmQmQ3vCDbeMsYR-xC-QRJQ7QXUAP9iaGsywJ6s02yhHvleVPyBYS6AEP2rRyMbN1oam0dlBS1r_CKM8uMQt_WnhEV_EBFkAMRR35PaAtBBMylr5X49S_c3PDF8N6soFCbHLbVnNGMQIa98lJGinMnDEK8PLMN3cZCcmwpmKZT-QEAmzzaa0PuvjXvsn6zMUfubF36BnVhWbaT0CxGR8jcWaP6obE-MSqckesIHET82Vq4QNObmcF7xkebloKoYFJbPU3a9obwTWUaOkZ_CuRWKP9IztQo38rMrD1I37eW4cw)


---
**Mark Ingle** *July 13, 2017 22:52*

**+Douglas Pearless** Thank you very much!  I cannot express how much I appreciate the help!  Ill solder up 2.6 and get everything connected up for a burn it test

![missing image](https://video-downloads.googleusercontent.com/ABzBgw1fK_D62O2saFCwtgY-aK7xpUEruzQCXzulfVA65G1AnzLB4N2ZmKG_VnPTBDYSTiQg_jVM4S8vSeG0-ZkmSglKeOZohwqLdTCiIHWTJ2lr3oFyDBF6lCImXDGyBA9pSQh1gfj1aveaDSvk_R8R_6FAdvkigwJqH1QDR8IzntcqEaY7j9O92W8kB4Qir25HqXRqQzfHtIQt_sVBEDAQnHCkQ7wDnTpX6pVh71yqYIcfdFWumWVLipep_CTh7wLHETV-ttQNwJQE81zg0Q28mxjgnRQwBXsAWesmzO956n9GmLIazsMz1Rfd54WFRAOUGhQm6X92Qw8ZN4-2-QIAJUTwtOu_YWdg-vhNznPHFcAhDQcOVwN14LGMu9sYOkZs6HyHnXnbLRMFLOBMz1DjgRR5jYs-ccuLK3uoX5hYv5utT2gF3H2eiifCbXry-u4t75fEbrjON-827zSv6qYUTVewTQdI5F3GzSA-Zij7fCHBxWfBCbOhNOPhM3B-ioZdZM2Tv_OCXMnzj69SulWczpxwlGALrnxh_ToKgW2BoY49NpYyciqRlXfz_8rO5pgea2mjbifvY4Np4zgKBJO_dMf1evjxoWnOjDc1Tu6l54ouTkVfHd6y2dYA4DVDYxF7y1stG1coXi4S_hDFrMpS2Mt5ie6qZywwcS9GnDcQo3uPkVSGz-cQxCfDdc32nquvnMnvdADSOsuJr9965ZqZwYtLnNgAGdfPDcXu9JHnEP4-Ri8LNVD7Bhyg2OqYqnynXz73Z-jGj86dv1Qc4z-alLtVg972MpL5HM6qbMARfcJTwgbiO-Y)


---
**Douglas Pearless** *July 13, 2017 23:00*

That looks great.  Make sure you have the protection diodes on all the MOSFETs going forward :-)




---
**Mark Ingle** *July 13, 2017 23:35*

This board is the latest version with the smc diodes for small mosfets. Leave to me to find a way to break it!  I have through-hole diodes on my 5x though 


---
*Imported from [Google+](https://plus.google.com/102654723337396117182/posts/4eXyLEneprC) &mdash; content and formatting may not be reliable*
