---
layout: post
title: "Does anyone now why slic3r is flipping my object when printing?"
date: July 08, 2017 23:24
category: "General discussion"
author: "jacob keller"
---
Does anyone now why slic3r is flipping my object when printing? If you look at the gcode view it's correct but when it's printing it prints it flipped? 

![missing image](https://lh3.googleusercontent.com/-QXSPbzcyF18/WWFppfhbsyI/AAAAAAAACco/RAE98ZakIekxE2S50BCY5cBAotdym6alwCJoC/s0/IMG_20170708_181833840.jpg)



**"jacob keller"**

---
---
**MakerGal** *July 08, 2017 23:28*

You got one endstop in the wrong side


---
**Griffin Paquette** *July 08, 2017 23:31*

Direction is wrong. Flip the end stop homing direction and flip your motor wire 180 deg. 


---
**jacob keller** *July 08, 2017 23:45*

**+MakerGal**​ **+Griffin Paquette**​ it only does it with this part.


---
**Douglas Pearless** *July 09, 2017 00:01*

Are you printing on a Cartesian or linear Delta printer?


---
**jacob keller** *July 09, 2017 00:04*

**+Douglas Pearless**​ its a Cartesian machine if you're facing the printer all the endstops are to the left it's a cubepro that I open sourced.


---
**MakerGal** *July 09, 2017 00:12*

left and back?? or left and front??


---
**jacob keller** *July 09, 2017 00:14*

**+MakerGal** left back


---
**MakerGal** *July 09, 2017 00:19*

everything its correct. You try different slicing softwares??


---
**Whosa whatsis** *July 09, 2017 00:20*

Your origin should be in the front left corner of the platform. Sounds like your Y axis is reversed.


---
**MakerGal** *July 09, 2017 00:23*

Its there already, he has the end stops in the left and the back. aso it get the origin in the front-left corner


---
**Douglas Pearless** *July 09, 2017 00:34*

Ok that means you typically have the wiring for the X axis stepper motor reversed, ( or the Y axis depending on the orientation of your print job 😄) simply unplug the motor and rotate the connector 180 degrees reversed.  Make sure you change of the endstop as well otherwise it will crash into then end when you HOME as it will now go in the opposite direct.


---
**jacob keller** *July 09, 2017 00:34*

**+Whosa whatsis**​Yes the origin is in the left front corner.


---
**MakerGal** *July 09, 2017 00:36*

And you say **+jacob keller**​ that only mirror that piece?? are you shure?? You print more pieces with that machine?


---
**Whosa whatsis** *July 09, 2017 00:36*

**+MakerGal** The cubepro uses the old BFB-3000 frame with a different plastic case over it. The important fact to know about this machine for this purpose is that it moves the extruder in X and Y. Mendel-style machines have their Y endstop in the back because they move the platform in Y instead of the head. All motion should be considered motion of the tool relative to the platform, even if the platform is moving, so moving the platform to the back is better thought of as moving the tool forward. When the platform is moving, the endstop needs to be in the back because that's where the platform will be when the head is at the front, but if the head is moving in Y, the Y endstop needs to be in the front.


---
**MakerGal** *July 09, 2017 00:38*

Ooooh yes yes **+Whosa whatsis**​, it similar to a Core XY construction, yes the endstops should be in the front and left


---
**jacob keller** *July 09, 2017 00:40*

**+MakerGal**​ **+Whosa whatsis**​ **+Douglas Pearless**​ this is what I have. It came from the factory like this.


---
**jacob keller** *July 09, 2017 00:41*

![missing image](https://lh3.googleusercontent.com/pshVLfpCkrPWRqBO9Gvh9QxEGyueAqyYYNQEKPLjgoosru4gANKDW9FlWZ0rz1CutMkFPzMBysZQI2AeCiD0GUE-G5Drfh6wcIA=s0)


---
**MakerGal** *July 09, 2017 00:45*

But like that, you got Y axis Inverted, as **+Whosa whatsis**​​​ said, with that constrution your endstop should make that the nozzle when homing stops in the left-front coner. As you got your nozzle stops in the left-back corner, so the machine will mirror your pieces around Y axis


---
**jacob keller** *July 09, 2017 00:47*

**+MakerGal**​ I'll change the y Axis then. Thanks for the help.


---
**MakerGal** *July 09, 2017 00:47*

You should move Y Endstop, to the same side of X Endstop, and turn arround the Y axis motor conector in your motherboard. That should fix the problem


---
**MakerGal** *July 09, 2017 00:48*

Your welcome!!


---
**Whosa whatsis** *July 09, 2017 00:48*

It's possible that the machine, by default, homes Y to its maximum instead of minimum. You would still have to invert the direction of the motor either electrically or in software, but you could leave the switch where it is.


---
**MakerGal** *July 09, 2017 00:52*

If he do that **+Whosa whatsis**​ need to change in the slicer software also?? or its not needed?? I mean in repetier host, in the machine settings, you got to change Homing direction?? X to min and Y to Max, or its not really needed 🤔🤔


---
**jacob keller** *July 09, 2017 00:54*

**+MakerGal**​ **+Whosa whatsis**​ now I'm just getting confused.


---
**MakerGal** *July 09, 2017 00:56*

You got 2 choiches, or either you change the endstops to the other side, or just change in Marlin the homing direction, you can say that your Y axis home to Max instead or Min


---
**jacob keller** *July 09, 2017 00:56*

**+MakerGal**​ **+Whosa whatsis**​ so I took a look in slic3r and they have a flip option so you can flip your part on the x y and z axis. Not to confuse with routine.


---
**MakerGal** *July 09, 2017 00:57*

That's also another option, if it works fine, do that, easier and faster


---
**jacob keller** *July 09, 2017 00:59*

**+MakerGal**​ **+Whosa whatsis**​ again thanks for the help.


---
**Gary Tolley - Grogyan** *July 09, 2017 01:07*

Stepper and endstop wired and mounted wrong. Normally you can fix this in firmware, quicker to bodge it methinks 


---
**Whosa whatsis** *July 09, 2017 02:07*

Btw, I have to say that you have an impressive array of 3D-printing-related software in your dock. Do you still get much use out of pleasant3d?


---
**jacob keller** *July 09, 2017 03:17*

**+Whosa whatsis**​ I don't use it really anymore. I just used it to check my gcode. It's pretty cool still.


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/1kTz1SejWeD) &mdash; content and formatting may not be reliable*
