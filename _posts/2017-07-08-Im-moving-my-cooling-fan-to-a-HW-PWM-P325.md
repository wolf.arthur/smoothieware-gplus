---
layout: post
title: "I'm moving my cooling fan to a HW PWM P3.25"
date: July 08, 2017 16:14
category: "General discussion"
author: "Mark Ingle"
---
I'm moving my cooling fan to a HW PWM P3.25. Should I add a diode?  If so can I add it to the leads going to the fan?

![missing image](https://lh3.googleusercontent.com/-40puRXjXuDw/WWEE3z3y2uI/AAAAAAAAB7s/gVNprtYrvLsitJzS50SgddMwkYRvzH_LgCJoC/s0/b2132985-73ff-469e-8623-2f766f6149aa.jpeg)



**"Mark Ingle"**

---
---
**Griffin Paquette** *July 08, 2017 20:16*

You're talking about a fly back diode yes?


---
**Mark Ingle** *July 08, 2017 20:30*

Well P3.25 only has 3volts. Need a pin with 12v


---
**Jeff DeMaagd** *July 08, 2017 20:52*

If the pin doesn't offer the voltage to run a fan, why are you moving the fan there?


---
**Mark Ingle** *July 08, 2017 21:23*

**+Jeff DeMaagd** I am not moving the fan now....should have check voltage before posting question.  Its still on P2.6




---
**Griffin Paquette** *July 08, 2017 22:17*

I mean you could move it if you use an external mosfet. Seems like a lot for a fan though. 


---
**Douglas Pearless** *July 08, 2017 22:26*

STOP: Do not connect a fan directly to 3.26 you WILL destroy the LPC1769 processor.  The best option is here [smoothieware.org - 3d-printer-guide [Smoothieware]](http://smoothieware.org/3d-printer-guide?s%5B%5D=diode) which is hardware PWM capable (check that your Smoothie already has the protection diode installed  per that page).


---
**Douglas Pearless** *July 08, 2017 22:33*

If you <b>really</b> want to use 3.26, then a SSR is the easier option as it only involves some wiring and not have to put in place a MOSFET and protection diodes, etc. (refer further down on the link I posted above).


---
**Mark Ingle** *July 08, 2017 23:06*

I need to control the air flow of a high speed 12v DC fan. It pushes about 25CFM. P2.6 does not support HW PWM and P3.25 is only 3volts. This design is need to get the same bridging quality that's possible on my RAMPS board on a delta. About 2-3 inches of plastic printed in mid-air!!


---
**Douglas Pearless** *July 08, 2017 23:13*

P2.4 (connector X6) supports hardware PWM (PWM0) (Small MOSFET) and should have the protection diode (D7) already installed, unless you have an early Smoothie board (double check!!), if it has the diode, simply wire the FAN (check polarity) and you are goo to go (update you config...)


---
**Mark Ingle** *July 08, 2017 23:22*

Thanks **+Douglas Pearless** I have 5x 1.0


---
**Jeff DeMaagd** *July 08, 2017 23:50*

Smoothieboard didn't have that flyback diode installed until about 6-7 months ago. It's relatively easy to add your own though.


---
*Imported from [Google+](https://plus.google.com/102654723337396117182/posts/V4diJNY2ETi) &mdash; content and formatting may not be reliable*
