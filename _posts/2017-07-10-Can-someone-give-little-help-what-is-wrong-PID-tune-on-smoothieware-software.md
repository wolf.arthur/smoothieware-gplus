---
layout: post
title: "Can someone give little help what is wrong PID tune on smoothieware software"
date: July 10, 2017 17:57
category: "Help"
author: "LassiVVV"
---
Can someone give little help what is wrong PID tune on smoothieware software. Before that marlin and mega2560 works brilliant, but now i dont get any good values auto pid to my printer.



I try different temps etc, but not help. Auto-tune gives me this kind of pid tunes:

e3d v6 hotend with 12volts.



Recv: \x09Kp:  38.7

Recv: \x09Ki: 2.573

Recv: \x09Kd:   145



Always temperature goes ok but "braking" too much and leave 2-3degrees under setpoint and ever reach that setpoint. If wait long printer goes HALT state. 



Any tips or are smoothieware some known bug on autotune procedure?





**"LassiVVV"**

---
---
**Arthur Wolf** *July 10, 2017 18:08*

Auto-pid isn't always perfect, sometimes you need to adjust values a bit. Google has litterally hundreds of tutorials on manually tuning PID values.


---
**Stephanie A** *July 10, 2017 18:12*

I always pid tune to worst case scenario, cooling fan on max, highest operating temp.


---
**LassiVVV** *July 10, 2017 20:17*

**+Arthur Wolf** hi Arthur. Can you tell is some temperature tolerance on hotend when printing is going. Now i get my temp nearly right that goes 253.5-254.5degrees whole time on print. Last time goes 252-253degrees.



Is there some 2degrees tolerance etc if printer is out that tolerance example 15min printer goes HALT state?


---
**Arthur Wolf** *July 10, 2017 20:23*

**+LassiVVV** For that, you want to read [smoothieware.org - temperaturecontrol [Smoothieware]](http://smoothieware.org/temperaturecontrol#safety)


---
**Geoffrey Forest** *July 17, 2017 04:08*

**+LassiVVV** This post explains PID tuning pretty well without getting too technical:

[forum.seemecnc.com - How-to: Quickly hand-tune your PID values - Welcome to the SeeMeCNC Forum](http://forum.seemecnc.com/viewtopic.php?t=11440)

I'm having similar problems as you getting printer HALTs when the hotend can't maintain temperature. Look for thermal runaway settings in the link that Arthur posted above. 


---
*Imported from [Google+](https://plus.google.com/112209274773346118200/posts/cERq9Joxxyn) &mdash; content and formatting may not be reliable*
