---
layout: post
title: "[ERROR] Cant read from printer(disconnected?)(SerialException):WriteFaied. Any help anyone It does this every time I try to run it"
date: July 10, 2017 18:54
category: "General discussion"
author: "Hector escarzaga"
---
[ERROR] Cant read from printer(disconnected?)(SerialException):WriteFaied.  Any help anyone   It does this every time I try to run it





**"Hector escarzaga"**

---
---
**Hector escarzaga** *July 10, 2017 19:04*

I've tried different computers and USB cables, it's the same. It was working fine for the last year, and I make sure not to let any smoke out of any parts. 


---
**Jack Colletta** *July 10, 2017 19:07*

Hector could you provide more details such as computer os, interface you are using, board you are connecting to, config file, firmware version...




---
**Jack Colletta** *July 10, 2017 19:10*

Well it could be a corrupted sd card if no smoke was let out.  Did you try reformatting the card and putting the firmware and config file back on?




---
**Arthur Wolf** *July 10, 2017 19:13*

Following, waiting for the info.


---
**Hector escarzaga** *July 11, 2017 10:58*

Running windows 10 on a surface pro with latest confit and bin files ,    Yes tried re flashing sad card .  


---
**Hector escarzaga** *July 11, 2017 11:07*

Tried moving all other electrical Rf producing equipment away, shorten the USB cable,even tried putting a ferrite choke on the cable. The LED 's inside are all flashing correctly. Red ,orange,green,green,green,and green with the two middle greens flashing.  Now they stop flashing on a crash and just stay green


---
**Jack Colletta** *July 11, 2017 14:47*

What board are you using.




---
**Hector escarzaga** *July 11, 2017 15:36*

Smoothie 4x v1


---
**Jack Colletta** *July 11, 2017 16:44*

The last 2 things I can think of is since you tried an another computer (surface books are new to me and I have heard of firmware updates making them flaky.)



1.  Try a different SD card.  I have seen this helping others.



2.  Re verify the config file and maybe post it.



**+Arthur Wolf**, any thoughts.












---
*Imported from [Google+](https://plus.google.com/103747247685514847068/posts/3HhMLCtFpoN) &mdash; content and formatting may not be reliable*
