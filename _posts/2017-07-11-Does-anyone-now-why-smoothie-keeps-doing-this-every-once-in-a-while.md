---
layout: post
title: "Does anyone now why smoothie keeps doing this every once in a while"
date: July 11, 2017 23:05
category: "General discussion"
author: "jacob keller"
---
Does anyone now why smoothie keeps doing this every once in a while. It just stops like 20 layers before the print is done. But try the file again and it works. And it keeps all the heaters on and Motors. I'm printing over USB and the computer doesn't shut off.





**"jacob keller"**

---
---
**Douglas Pearless** *July 12, 2017 00:08*

Try unmount(ing) (or Ejecting if you use Windows) the Smoothie USB MSD device (i.e. it appears as a USB attached storage device like a USB memory stick under Windows, OSX and Linux), the serial connection will continue to work (from Repetier, Pronterface or whatever you are using to "print") but sometimes the host computer can cause issues.  An alternative is try copying the GCODE of the print to the SD card on the Smoothie, unplug the USB cable and print using the LCD panel on your printer and this will prove if it is the host mucking about with the USB connection, or not.


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/dB7HcAgGD2n) &mdash; content and formatting may not be reliable*
