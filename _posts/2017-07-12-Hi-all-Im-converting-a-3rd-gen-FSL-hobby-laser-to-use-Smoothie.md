---
layout: post
title: "Hi all, I'm converting a 3rd gen FSL hobby laser to use Smoothie"
date: July 12, 2017 01:43
category: "General discussion"
author: "Peter Vieth"
---
Hi all, I'm converting a 3rd gen FSL hobby laser to use Smoothie.  Motion is working fine (well, I haven't hooked up the limit switches yet) but I can't get the laser to fire from software.  The jymydy PSU has a L (active low, I think) wire which the original controller utilized (by process of elimination it must be this wire); I've hooked it up to the + pin on 2.4.  laser_module_enable is true and I've tried laser_module_pwm_pin 2.4 and 2.4!^ ... regardless, with a DMM attached to pin 2.4 and a cutting job running, there's no action on pin 2.4.   The laser fires using the test button, and I can switch back to the original controller and it works, so it sounds like a configuration issue.  Any ideas? 





**"Peter Vieth"**

---
---
**Don Kleinschnitz Jr.** *July 12, 2017 02:07*

You might find out what you need here, I think that the K40 and FSL have some similarities.



Post a picture of the LPS connectors. I do not know if its the same as a K40.



BTW I don't think you want the +pin on 2.4 (if you are using the green connector)  I think that is not the MOSFETS drain, its the DC supply. If I recall the drain is on the - pin. 



Try and grnd the LP:S's L and see if that fires the laser if so it is grnd true. If its the same as the K40 supply it needs an open drain connection.



If I recall you also don't need the !^ using the output connector.



Here is how I did it....

[http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)





[donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Ray Kholodovsky (Cohesion3D)** *July 12, 2017 02:22*

**+Ben Delarre** did a conversion. I'm told it was similar enough to the k40, but there were differences. 


---
**Peter Vieth** *July 12, 2017 17:28*

Don, When I touch L to ground, the laser fires.  I wired L now  2.5's minus/gnd connector.  With the config file's pwm output pin set to 2.5!^, the laser fires continuously and never stops.  With it set to just 2.5, the laser never fires.  It sure doesn't seem like there is a PWM output on either of these pins.  @#(<b>$(@#</b>  This is via the laser test button and trying to cut a question mark SVG I found via Google.


---
**Don Kleinschnitz Jr.** *July 12, 2017 19:36*

**+Peter Vieth** why are you wired to 2.5, did you check the post I linked? You should be using the 2.4??

Please post a picture of how your are wired...


---
**Peter Vieth** *July 12, 2017 20:23*

Hi Don, I've tried both 2.4 and 2.5.  Just wanted to make sure in case I'd blown up the FET on 2.4 or something.  Thanks for your assistance!

![missing image](https://lh3.googleusercontent.com/kr2vRELrFOJt1gMpPQ9PBpQKWz2nqEL2SnX21QwGJ9ww3llFVFkQgSDwqPJZmuHyEsmmFg3Zf4WtupNd4ThBNHdyJumBEr7PCL15=s0)


---
**Peter Vieth** *July 12, 2017 20:26*

Are there any settings in the Smoothie web UI that might might make the laser not fire?  The laser cutter guide doesn't really explain those and I just went through them quickly.




---
**Kelly Burns** *July 12, 2017 20:55*

Just to rule out any Laserweb issues...  do you have an LCD panel attached to your SW board?  If so, do a Test Fire from the LCD Menus


---
**Peter Vieth** *July 12, 2017 22:34*

Kelly, I do not have an LCD panel attached.  I wouldn't be opposed to getting one though.  I have a LulzBot Taz 5 and the panel is pretty useful.  Any recommendations?


---
**Don Kleinschnitz Jr.** *July 12, 2017 23:01*

**+Peter Vieth** lets look at the configuration file can you post it. Also can you post a photo of the actual  wiring coming from the smoothie to LPS?


---
**Peter Vieth** *July 13, 2017 00:03*

Configuration file as requested:  [pastebin.com - [PHP] FSL Smoothie Config File - Pastebin.com](https://pastebin.com/3nDpBpS6)


---
**Peter Vieth** *July 13, 2017 01:07*

...and photos [onedrive.live.com - Microsoft OneDrive - Access files anywhere. Create docs with free Office Online.](https://1drv.ms/f/s!ArD0suVV0QrRirx2T1zk57W1uHFe6A)


---
**Don Kleinschnitz Jr.** *July 13, 2017 03:48*

**+Peter Vieth** disable all modules in your config file that you are not using for laser like temp, fan etc. I don't see a conflict so that may not be a problem but just to be safe.






---
**Peter Vieth** *July 13, 2017 16:15*

Ok, commented out everything about fans, extruders, etc.  Still no go.  I tried another Mac, this time with LaserWeb4.  Also no go.  Is there some debug log on the Smoothie itself?  Additionally, in LaserWeb, in the control tab, when I jog there is a prompt that the machine has been jogged but when I click LaserTest, the UI doesn't echo back anything.  Not sure if that's normal or not.  Firmware version ok?  LaserWeb reports edge-9399ed7 

[smoothieware.org - currentcontrol [Smoothieware]](http://smoothieware.org/currentcontrol)


---
**Don Kleinschnitz Jr.** *July 13, 2017 16:26*

**+Peter Vieth** Many have this working so there is something we are missing.



1.) Do you have a scope? If so can you look at the "L" signal and verify then post what is there?



2.) Do you have a DVM? Put the meter on a scale that will read 0-5V and connect from "L" to gnd. Then send the machine Gcodes with various S values in 10% increments and you should see the average voltage change on "L", such as.

100% = 5 vdc

50% = 2.5

10% = .5



These values won't be exact but they should be in the ball park.


---
**Peter Vieth** *July 13, 2017 17:01*

Thanks again Don.  I have a scope and DVM.  Will try this when I get home tonight.


---
**Don Kleinschnitz Jr.** *July 13, 2017 17:06*

**+Peter Vieth** ok with the scope you can just send Gcodes and monitor "L" for change in DF.


---
**Peter Vieth** *July 14, 2017 00:12*

When I turn the machine on, L goes high.  It then stays high no matter what I do.


---
**Don Kleinschnitz Jr.** *July 14, 2017 04:50*

**+Peter Vieth** what Gcodes did you send and how?


---
**Peter Vieth** *July 14, 2017 15:08*

I used Pronterface.  g0 x0 y0, then g1 x100 y0 s100, so on.  Also verified the laser module is enabled and the pin settings are right by sending @config-get laser_module_pwm_pin etc


---
**Peter Vieth** *July 14, 2017 16:58*

I tried downloading the latest config file, chopping out all extras, and setting up config again. Still no signs of life on 2.4.  Tried 2.5 as well again, just in case...  Setting the pin as 2.4^ or 2.4o, it also just stays high.  If I use 2.4!, the pin stays low (and the laser fires continuously).  Attempting to fire the laser in this case from Pronterface or LaserWeb3 does not cause any change visible on the scope on this pin either.  I suppose this is good as it confirms the pin is being configured upon startup, but why setting the laser power via sXXX does not produce any change on 2.4 or 2.5 is beyond me.  Based on [smoothieware.org - laser [Smoothieware]](http://smoothieware.org/laser) it looks like s might be in a range 0.0-1.0, so I can try that, and it also says i can try @fire 100  in pronterface.  I will have to try those tonight, after that the last thought I have is to flash the firmware again and pray.  


---
**Peter Vieth** *July 14, 2017 18:14*

Don, there shouldn't be any need to connect additional ground pins on the Smoothie to the PSU other than Vbb-, would there?  The laser cutter guide suggests connecting a GND from JP10 to the LPSU ground... seems redundant if Vbb minus is already connected.


---
**Peter Vieth** *July 15, 2017 22:49*

So, the value after s must be a number between 0.0 and 1.0.  Otherwise, the pin just doesn't do anything.  I'm surprised this isn't in the documentation.  I'm used to CNC milling and the number after S must be an integer, so it didn't occur to me to try a decimal!


---
**Peter Vieth** *July 16, 2017 00:03*

Well, now I noticed the x and y travel distances are incorrect.  Although the configuration "takes" (verified in pronterface), no matter what I set alpha_steps_per_mm, for example, a 100mm move in the X produces a 50mm actual move.  EDIT: it appears the board needs to be power cycled for settings on steps per mm to take effect.


---
**Don Kleinschnitz Jr.** *July 16, 2017 13:28*

**+Peter Vieth** it is good grounding practice to insure that signal grounds and their current return to their source rather than some other route. Leaving off the ground will probably <b>work</b> but you could have noise problems.




---
**Don Kleinschnitz Jr.** *July 16, 2017 13:38*

**+Peter Vieth** it is in the docs somewhere I don't recall where :) Its also here: [donsthings.blogspot.com - Gcode: Getting Started](http://donsthings.blogspot.com/2016/12/gcode-getting-started.html)



<b>Does this mean that your PWM is working?</b> 


---
**Peter Vieth** *July 17, 2017 16:57*

For the record, 4th gen FSL steps per mm is 157.48031 but 3rd gen is exactly  double that.  


---
**Don Kleinschnitz Jr.** *July 17, 2017 17:28*

**+Peter Vieth** did you get your PWM fixed, did i miss that :)?


---
**Peter Vieth** *July 17, 2017 17:50*

Hi Don, yes, it's working now if I use S0.0 to 1.0 (well, not exactly-- it doesn't fire when a value less than 0.2 is used... but works well enough for now!).  Thanks for all your help!


---
**Don Kleinschnitz Jr.** *July 17, 2017 18:58*

**+Peter Vieth** lasers don't discharge well at low currents I think 4ma is the lowest.


---
*Imported from [Google+](https://plus.google.com/+PeterVieth/posts/gjZDUMpDpM7) &mdash; content and formatting may not be reliable*
