---
layout: post
title: "First time ever posting on G+ so sorry if I do something wrong or take a while to reply"
date: July 13, 2017 17:00
category: "Help"
author: "Marcus Hawkins"
---
First time ever posting on G+ so sorry if I do something wrong or take a while to reply.



Earlier this year I bought the Smoothieboard x4 and installed it into modified K40 laser (new gantry) followed a few guides online and got it installed and software setup and started cutting, after a few minutes the control board would crash the leds would show VBB (on) 3v3 (on) 4 (on) 3 (off) 2 (on) 1 (off) none of them flashing. Hit the reset on the control board, restart job and if I am lucky I could get it done without it crashing again (70% chance of it crashing)



Recently had enough and contacted RobotSeed and they sent me a replacement and upgraded it to the 5x board, just installed the board, sorted out the config and start to cut and.... crashed again.



I can leave the control board on for hours and 0 crashes, start cutting and it crashes, seems to only crash when the laser is firing so its either a level shifter issue, power supply or the tube.



Does anyone use a level shifter? I am using a 3.3v <> 5v Arduino one, seen a few guides without one, and 2 with one. So not sure if I should be using it or not.





Not sure what to do next. 



Anyone have any ideas?﻿





**"Marcus Hawkins"**

---
---
**Ron Hunn** *July 13, 2017 18:06*

Do you have a good power supply? Sounds like a voltage drop causing issues with the board. 


---
**Joe Alexander** *July 14, 2017 05:37*

try this and see how it works for you, its minus the level shifter.

![missing image](https://lh3.googleusercontent.com/njf4G-Ro8ZD7Nity3Ub_L4ko2cGdAIHVnN8duFa2geMFxnOwhoCeA1-hWT0HQ1vrpcjYLcuchn1-VJllcah_iOdIRonD6LfvSRg=s0)


---
**Arthur Wolf** *July 14, 2017 09:45*

This sounds like there is something very very wrong with your wiring or your psu ...


---
**Mike Mills** *August 01, 2017 12:07*

Just finished wiring in my second smoothie into a FSL 9.5x14 hobby laser. Still dialing in, but problems I've seen before that are similar to yours...  Are you using USB to drive the smoothie? If so, is the cable near any source of power, INCLUDING a cell phone? Any AC power source can induce OV conditions in your USB cable, causing your board to shut down. Worth a look...


---
*Imported from [Google+](https://plus.google.com/109498476392134538148/posts/YGdrpabGBKW) &mdash; content and formatting may not be reliable*
