---
layout: post
title: "I have disabled hotend thermal runaway (by setting the allowable temp variation to 0) because I kept getting nuisance HALT errors when the temp would drop below the setpoint and not catch up in time (working on manual PID"
date: July 16, 2017 04:45
category: "Help"
author: "Geoffrey Forest"
---
I have disabled hotend thermal runaway (by setting the allowable temp variation to 0) because I kept getting nuisance HALT errors when the temp would drop below the setpoint and not catch up in time (working on manual PID tuning). But I STILL get a HALT if the temp is below the set point for a few minutes. I have the time set to 2000 and the runaway should be disabled. Did I miss something?





**"Geoffrey Forest"**

---
---
**Arthur Wolf** *July 16, 2017 09:19*

very latest firmware ( edge branch ) and very latest config example file ?


---
**Geoffrey Forest** *July 16, 2017 13:15*

**+Arthur Wolf** Config file is here: [https://pastebin.com/kidYV4Dy](https://pastebin.com/kidYV4Dy)

and my current firmware file was downloaded on 2/3/2017. I'm not near the printer right now, so I can't run the version command. I can do that if necessary later tonight. Thanks!


---
**yos gu** *July 16, 2017 14:16*

**+Arthur Wolf**​​ also getting the same halt in my delta. Running latest smoothieware + latest delta config file 


---
**Geoffrey Forest** *July 18, 2017 00:47*

This is getting frustrating. I updated firmware today to the currently available firmware-latest.bin. I tried a print and watched the temperature. The temp was hovering within a degree or two of the set temp, and about 10 minutes into the print, the printer stopped and the display screen went blank. I watched the temperature start to dive. Once the temp dropped a ways, I got a HALT. The error said temperature runaway on T (delta temp -76.404....). So something is stopping the printer, and then after the temperature dives, the HALT errors occurs.


---
**Arthur Wolf** *August 08, 2017 16:52*

How are you communicating with the board, how are you starting the files you play ?


---
**Geoffrey Forest** *August 09, 2017 22:42*

I use Repetier-Server. The problem surfaced when I switched to a high flow fan for cooling the hotend. I ran a PID autotune, but I guess it was not able to maintain a steady temp? Swapping back to the normal hotend fan has so far avoided the problem from showing up. 



The frustrating part is I would like to completely disable the watchdog (I'll watch the printer) and let the printer continue a job even if the temp is below the setpoint. But I can't seem to do that, it will still HALT on a thermal runaway.



Would running a print from an SD in the display be something to try for troubleshooting?


---
**Arthur Wolf** *August 10, 2017 09:57*

repetier-server is very very well known for not being compatible with smoothie. even if it's not the source of this problem, you <b>will</b> have other problems. it's pretty much the only host that doesn't do what's needed to work with smoothie, all the others work.


---
**Geoffrey Forest** *August 11, 2017 05:10*

Thanks for the heads up. I typically don't have trouble running prints, just every so often this thermal runaway happens. I use it because it will serve two printers rather easily. I have an image of Octoprint I have used to troubleshoot things, but I am not Linux literate enough to get it to run two printers. I might try it for a while running just the smoothie printer.


---
*Imported from [Google+](https://plus.google.com/+GeoffreyForest/posts/ZKDFDVEAg9G) &mdash; content and formatting may not be reliable*
