---
layout: post
title: "Hello everyone!. I received my azteeg mini x5 v3.0 + miniviki lcd"
date: July 22, 2017 22:24
category: "General discussion"
author: "Pedro Rocca"
---
Hello everyone!. I received my azteeg mini x5 v3.0 + miniviki lcd. Connect everything but I'm having trouble setting the screen, just turn on the light, something I'm doing wrong is the firmware, could someone help me ?. thank you very much.





**"Pedro Rocca"**

---
---
**Eric Lien** *July 22, 2017 22:28*

Start by posting your config file. We can proceed from there.


---
**Eric Lien** *July 22, 2017 22:28*

Also a picture of how you have it wired/connected.


---
**Pedro Rocca** *July 23, 2017 12:03*

![missing image](https://lh3.googleusercontent.com/zDxewqyNqATamvTtvXsaye5UFoY2lzsY1dgB7IUc88pMx27R-8Eusu26qwrpo1LzMHecnWD3sj0P1iawm-E9TncACB0xnjzq9SHv=s0)


---
**Pedro Rocca** *July 23, 2017 12:03*

![missing image](https://lh3.googleusercontent.com/gkNriHe_zRscN6ZjIv30YYXAWEtqrx9N0S1hEq7VGknv9wwhy9YSWOZyMPad1_7XDz9TqzKXBDc2mhFCf30r7BNdlzb4yd9B7ES_=s0)


---
**Pedro Rocca** *July 23, 2017 12:03*

![missing image](https://lh3.googleusercontent.com/YAhl2ZcaaBMiLHV8ZIusQM0_IPjTcT5jFRz4jgFOIUCuRPe_NOvkNibj26dVb-_xy37E5k-gzLcc1pfz-i3TlzOCoqj3dOMo8Rhm=s0)


---
**Pedro Rocca** *July 23, 2017 12:04*

![missing image](https://lh3.googleusercontent.com/eRsbv5PevzWCtUBhzfLuHmL10m_HTsnH2UMXMLrAhZOHIPRtTouu4W2gsSfXb9dDZyNdqYyBz7UfHM5uq3q0ep7ttV4Qw-7zDqiA=s0)


---
**Pedro Rocca** *July 24, 2017 00:37*

someone? please




---
**Eric Lien** *July 24, 2017 00:59*

Here is my config for a Viki_Mini for one of Rays boards.

![missing image](https://lh3.googleusercontent.com/abiOupbYVlUbQAYMgDtAZFmbQgOnZE_COLm7zA2sGlW--h2CFsQJ2-aKJIEDz2wSlOmXSqSIvomGEZMNr3bVgjHoIzLiusp9Z0a7=s0)


---
**Pedro Rocca** *July 24, 2017 17:08*



Excellent, I'll try with that configuration, thank you!


---
*Imported from [Google+](https://plus.google.com/111835693371657010648/posts/BEMKdyWeLuq) &mdash; content and formatting may not be reliable*
