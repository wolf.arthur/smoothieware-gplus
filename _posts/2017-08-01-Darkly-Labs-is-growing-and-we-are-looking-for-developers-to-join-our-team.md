---
layout: post
title: "Darkly Labs is growing and we are looking for developers to join our team"
date: August 01, 2017 23:43
category: "General discussion"
author: "Domenic Di Giorgio"
---
Darkly Labs is growing and we are looking for developers to join our team.



Do you have experience with javascript, nodejs, react/redux and webpack?

Do you like low-level programming, or have micro-controller experience?

Do you want to work on some cool and challenging projects?



Get in contact with us by emailing your resume to info@darklylabs.com







**"Domenic Di Giorgio"**

---
---
**Arthur Wolf** *August 02, 2017 15:06*

Robotseed does consulting on smoothie stuff/can find contributors ready to do consulting too, if you have specific tasks. You have my email.


---
*Imported from [Google+](https://plus.google.com/117537980354391774283/posts/QFv75qn2RGS) &mdash; content and formatting may not be reliable*
