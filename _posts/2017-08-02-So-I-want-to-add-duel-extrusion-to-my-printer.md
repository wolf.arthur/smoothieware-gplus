---
layout: post
title: "So I want to add duel extrusion to my printer"
date: August 02, 2017 00:17
category: "General discussion"
author: "jacob keller"
---
So I want to add duel extrusion to my printer. but I bought the 4x Smoothieboard last year. 



what do I need to upgrade to get to a 5x Smoothieboard?

And How? 





**"jacob keller"**

---
---
**Douglas Pearless** *August 02, 2017 02:17*

You simply need to populate the parts for the fifth stepper, check out the BOM here [docs.google.com - Smoothieboard BOM](https://docs.google.com/spreadsheets/d/1vshB97WWt6Lceo59aggbdhRuXxTEhSL9lkotScR0oYE/edit#gid=0) and look for "Stepper motor driver 5"


---
**jacob keller** *August 02, 2017 02:23*

**+Douglas Pearless**​ Thanks!


---
**Griffin Paquette** *August 02, 2017 03:20*

Doug is spot on. Hope you're ready to do some reflowin!


---
**Chris Chatelain** *August 04, 2017 11:43*

It can be simpler than that if you want, check out the docs for adding external drivers. Only need to wire in 4 pins instead of doing smd work


---
**Douglas Pearless** *August 04, 2017 12:26*

Yep, that is also true!


---
**jacob keller** *August 04, 2017 12:35*

+Chris Chatelain +Douglas Pearless Thanks for the comments. I will look into external drives.


---
**Arthur Wolf** *August 05, 2017 13:46*

**+jacob keller** you only need to add one external stepper driver, for example a TB6600


---
**jacob keller** *August 06, 2017 01:28*

Thanks **+Arthur Wolf**


---
*Imported from [Google+](https://plus.google.com/115098820415162360458/posts/ZFJELt7icS9) &mdash; content and formatting may not be reliable*
