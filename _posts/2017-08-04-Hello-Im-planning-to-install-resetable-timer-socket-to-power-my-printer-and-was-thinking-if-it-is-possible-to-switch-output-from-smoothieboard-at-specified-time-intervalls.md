---
layout: post
title: "Hello! I'm planning to install resetable timer socket to power my printer and was thinking if it is possible to switch output from smoothieboard at specified time intervalls"
date: August 04, 2017 07:37
category: "General discussion"
author: "Topias Korpi"
---
Hello! I'm planning to install resetable timer socket to power my printer and was thinking if it is possible to switch output from smoothieboard at specified time intervalls. The output pin would "push" a button at the timer, so the socket timer would switch the power off if there is no signal coming.



So, can I switch an outputpin on and off at specified time intervalls?





**"Topias Korpi"**

---
---
**Claudio Prezzi** *August 04, 2017 08:14*

Do I understand you correct that you just want to switch the power off when the print job is finished?



You could probably use the switch module (see: [http://smoothieware.org/switch](http://smoothieware.org/switch))

[smoothieware.org - switch [Smoothieware]](http://smoothieware.org/switch)


---
**Topias Korpi** *August 04, 2017 09:47*

No, I want to set up a timer socket to switch power off, lets say 30 seconds after button is pressed. I'd like to set up smoothieboard to "press" that button like every 10 second.


---
**Jeff DeMaagd** *August 04, 2017 11:41*

That sounds a lot like a watchdog circuit. There's something like that built into the microcontroller and firmware. I think it's discussed in the safety section. But the watchdog is totally internal to the microcontroller, it doesn't use an external circuit.


---
**Topias Korpi** *August 04, 2017 12:45*

**+Jeff DeMaagd** Thanks for pointing that out. Reading the safety section was extremely informative(and important)! And this is way more convinient than what I was planning:

[smoothieware.org - temperaturecontrol [Smoothieware]](http://smoothieware.org/temperaturecontrol#solid-state-relay-controlled-safety-cutoff)


---
*Imported from [Google+](https://plus.google.com/+TopiasKorpiTK/posts/PvAY8L6481d) &mdash; content and formatting may not be reliable*
