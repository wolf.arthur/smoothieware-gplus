---
layout: post
title: "I've replaced the MCU on my Smoothieboard a few weeks back, but I can't get it to load the firmware"
date: August 04, 2017 07:44
category: "General discussion"
author: "Oliver Seiler"
---
I've replaced the MCU on my Smoothieboard a few weeks back, but I can't get it to load the firmware. I've uploaded and verified the bootloader via the serial port, put the firmware onto an SD card. I've also checked the traces to the SD card and they are fine.

I'm stuck and any suggestions of what I can do to bring it back to life would be great. 

[https://plus.google.com/+OliverSeiler/posts/gVuxaFbST5B](https://plus.google.com/+OliverSeiler/posts/gVuxaFbST5B)







**"Oliver Seiler"**

---


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/NHKMQP933hy) &mdash; content and formatting may not be reliable*
