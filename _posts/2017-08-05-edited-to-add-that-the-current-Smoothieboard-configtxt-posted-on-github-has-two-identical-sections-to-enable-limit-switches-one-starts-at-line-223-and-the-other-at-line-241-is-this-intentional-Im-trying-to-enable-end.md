---
layout: post
title: "(edited to add that the current Smoothieboard config.txt posted on github has two identical sections to enable limit switches, one starts at line 223 and the other at line 241 - is this intentional?) I'm trying to enable end"
date: August 05, 2017 05:17
category: "General discussion"
author: "Ashley M. Kirchner [Norym]"
---
(edited to add that the current Smoothieboard config.txt posted on github has two identical sections to enable limit switches, one starts at line 223 and the other at line 241 - is this intentional?)



I'm trying to enable end stops for a Z-table and running into some unexplained issues. The board is running the stock firmware-cnc firmware as well as a stock (Smoothieboard) config.txt from smoothieware's github (with gamma_max_pin enabled.) In config.txt, I set gamma_limit_enable to true (and removed the # at the start of the line.) With a switch connected, pronterface reports P1.29 as 0 (zero) or 1 when I manually depress the switch. Great, it sees it. Now here's what happens:



Boot up the board and try to jog Z (either positive or negative) and it instantly halts. Pronterface responds with: ALARM: Hard limit +Z



I can issue a reset (M999) and try to jog again and now it <b>does</b> jog, no alarm, however manually triggering the switch does nothing.



At this point I thought, ok maybe it's something with the switch itself, so I removed it completely, yet left them enabled in config.txt. Reboot the board, connect to pronterface, try to jog Z and it instantly halts, same alarm code. Once I issue a reset, I can now jog Z just fine.



Tried a different approach: boot up the board, I can jog X and Y no problem, tried jogging Z and it halts again. Issue a reset, and now I can jog all three axis.



Now for the 4th attempt, where I reconnected the switch. Boot up the board, jog X and Y successfully, tried jogging Z and it halts. Issue a reset, now I can jog all three, however the switch does not appear to have any effect what so ever.



Anyone have any suggestions of what to try to debug this?





**"Ashley M. Kirchner [Norym]"**

---
---
**Ashley M. Kirchner [Norym]** *August 05, 2017 06:45*

Continuing on this, on a whim I tried Z-min, lo and behold that works as expected. With the switch normally closed and M119 reports a value of 1. I can jog Z and manually trigger the switch and it halts. Great. However, doing the same thing on Z-max and nothing happens when I trigger the switch.



Also, it still instantly halts when it first boots up and I try to jog Z. But after a reset it works fine, and the end stop triggers for Z-min only.



config.txt shows

gamma_min_endstop 1.28^

gamma_max_endstop 1.29^



gamma_limit_enable true


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/g3nrJNpsTzJ) &mdash; content and formatting may not be reliable*
