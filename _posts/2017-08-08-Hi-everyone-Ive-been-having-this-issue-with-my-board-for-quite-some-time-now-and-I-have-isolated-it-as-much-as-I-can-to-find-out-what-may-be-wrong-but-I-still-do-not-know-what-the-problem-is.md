---
layout: post
title: "Hi everyone! I've been having this issue with my board for quite some time now and I have isolated it as much as I can to find out what may be wrong but I still do not know what the problem is"
date: August 08, 2017 23:51
category: "Help"
author: "Glen Scott"
---
Hi everyone! I've been having this issue with my board for quite some time now and I have isolated it as much as I can to find out what may be wrong but I still do not know what the problem is. My board will randomly slow down to around 1mm/s at random areas while cutting no matter what the file cut rate is. It is very inconsistent as I can do an entire file and it'll work fine then do the same file 

again and it will slow to that speed at random inconsistent areas. Visually it looks like its freezing up but the position readout on laserweb show it is still moving but its really slow. While it is happening the board becomes unresponsive (only reset and abort work) and LED 2 either stays ON or Off for its duration but does not flash. It appears to do this only on lines of code e.g. once the line is done it resumes normal speed. But this will also happen while jogging it as well. This is my second board (same issue with the first as well) and I have tried different computers, power supplies and cords. Even with nothing else plugged in, only the usb from the computer to the board, this problem happens. Any help will be appreciated.





**"Glen Scott"**

---
---
**Sébastien Plante** *August 09, 2017 01:21*

Did you try another Power Supply?  Or maybe SDCard? 


---
**Douglas Pearless** *August 09, 2017 01:48*

Can you run a job of the SD Card with the USB disconnected?


---
**Glen Scott** *August 09, 2017 01:48*

**+Sébastien Plante**  yes I have tried two different power supplies both meanwells as well as two SD cards but it appears to have the same problem "moving" even with the supplies not plugged in


---
**Glen Scott** *August 09, 2017 01:53*

**+Douglas Pearless**  I haven't tried running off the SD card because I'm not sure how but I use the usb to power the board as well


---
**Douglas Pearless** *August 09, 2017 01:59*

Hmm, what host system are you using (OSX, Linux, Windows, etc) and can you eject the SD card from the host so you are only using the USB to provide power and a serial connection to your host software?


---
**Glen Scott** *August 09, 2017 02:01*

I'm using windows and yes I can eject the SD card


---
**Sébastien Plante** *August 09, 2017 12:48*

When plugged, just copy the file into the SD Card of the Smoothieboard, then use the LCD to play or the Terminal with the command "play" ([smoothieware.org - player [Smoothieware]](http://smoothieware.org/player%29)


---
**Glen Scott** *August 09, 2017 23:47*

**+Sébastien Plante** oh I don't have an LCD for it


---
**Douglas Pearless** *August 09, 2017 23:59*

OK



I am interested in eliminating the MSD part of the USB connection (i.e. the on board SD Card is mounted and can be seen on the host). 



So a simple eject of that “device" from your host will probably suffice; if still have an issue then you will need a LCD panel to perform the local USB isolated test.



Obvious question, you are using a good quality USB cable as some if the cheap ones are not very well made and do cause issues.



Cheers

Douglas


---
**Glen Scott** *August 10, 2017 22:36*

So could there be power interruption through the usb? Or at least the usb port? The cable I'm using is much better than the older one I had


---
*Imported from [Google+](https://plus.google.com/100338077477204259797/posts/DBXiRZspsCJ) &mdash; content and formatting may not be reliable*
