---
layout: post
title: "Wanted to share a quick post about my custom Smoothieware compatible board"
date: August 08, 2017 23:32
category: "General discussion"
author: "Griffin Paquette"
---
Wanted to share a quick post about my custom Smoothieware compatible board. Files will be released once the board is out of dev state. 



<b>Originally shared by Griffin Paquette</b>



I am extremely excited to share a project that has been in the works for some time: this is the Stealthboard powered by Smoothieware



The board has four integrated Trinamic 2208 stepper drivers with full digital current control. There are also two high power mosfets and a number of other peripherals. Tried to keep it as compact as possible at 100x60mm. 



This is still a work in progress and as you can see, has been beat on!



Would love to know what everyone thinks about it! More info to come soon.



Fellow mods if you feel this is not appropriate just let me know and I will remove the post. 



-Griffin Paquette



!![images/5b15de668e4cdd00c580419566b31fd9.jpeg](images/5b15de668e4cdd00c580419566b31fd9.jpeg)
!![images/dbd83be425572c53478ef790b637bf86.jpeg](images/dbd83be425572c53478ef790b637bf86.jpeg)
![missing image](https://lh3.googleusercontent.com/-zVnaS5I0uRg/WYpJmPUQiPI/AAAAAAAAhE0/C-6U6KqOcBoAvPvz68KQubKI_NnNxrlqACJoC/s0/001.jpeg)
!![images/775e9b1dcf200cedbff01288dc441490.jpeg](images/775e9b1dcf200cedbff01288dc441490.jpeg)

**"Griffin Paquette"**

---
---
**Marcos Gomes** *August 10, 2017 07:52*

Hi, the board seems to be OK, but there is little information for me to express a better opinion. There are a couple of things I would like to know, for example, number of layers, copper thickness, expansion support (LCD, Ethernet, ...). Is there any price estimate?






---
**Griffin Paquette** *August 10, 2017 12:30*

**+Marcos Gomes** you're completely right. The original post has more of that covered but I should mirror it here. 



The board is 4 layer with 2oz copper on all 4 layers. It has Ethernet expansion support (female header) and Viki LCD support (male header). I also have a tiny GLCD adapter that can be plugged in to use the full glcd. I'll add a photo of that soon. 


---
**Marcos Gomes** *August 11, 2017 08:08*

At a first glance it looks like a cohesion3D with soldered Trinamic drivers. Some may see the soldered drivers as a disadvantage. I personally don't care, as long as the board is designed to allow a proper heat dissipation.



Opposed to the C3D yours looks to be oriented to the 3D printer community. Having one or two drivers would benefit users who want a dual extrusion system or those who need more than one motor for an axis.



Other than that I believe it appears to be a good board, which distinguishes it self by using 256 microstepping trinamic drivers.


---
**Griffin Paquette** *August 11, 2017 13:54*

**+Marcos Gomes** it does indeed share some features with the C3D. The big difference is that the Cohesion boards are 2 layer 1oz boards so the thermal properties as well as pours are not as favorable as the 4 layer 2oz. Trinamic drivers tend to run quite hot especially when on the stepsticks which is why they are much better off being soldered on. 



You are 100% correct on the orientation of the board. Ray's Mini is an excellent board for K40's as it offers all the proper terminals and connectors to do so. It's FET terminals are not rated near to the same current as these. There are just a few things that make it stand out.



A 5 driver version is already in the works and promises to have all expandable points broken out! Like others on the community I like having some expandability and cool features and that's what the bigger version will be. This was a board designed as an inexpensive upgrade for the lower cost printers on the market offering quiet motion and properly rated terminals (all are 20A rated). 


---
**Jérémie Tarot** *August 11, 2017 15:59*

Another board is always good news as long as the creator plays fair with the Open Source project and the community. Publishing design files is the first step, contributing back is the path... 

Congrats for this nice piece of hardware anyway :) 


---
**Griffin Paquette** *August 11, 2017 16:09*

**+Jérémie Tarot** completely agreed. I'm 100% about open source and that won't change here! I want to ensure that the files are without fault before I release them. Really appreciate the kind words!


---
*Imported from [Google+](https://plus.google.com/111302122377301084540/posts/jdqztkge5Cw) &mdash; content and formatting may not be reliable*
