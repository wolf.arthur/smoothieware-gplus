---
layout: post
title: "Hi! We are replacing the control box on a large 1700x2500mm chinese laser with a smoothieboard"
date: August 11, 2017 06:51
category: "Development"
author: "Mats-Ola Str\u00f6m"
---
Hi!

We are replacing the control box on a large 1700x2500mm chinese laser with a smoothieboard. Everything works great so far.

The laser has a conveyor belt. We would really like to be able to move the belt (U axis, stepper driver) with our Smoothieboard. I found the jogger and joystick module in the docs, but I cannot seem to find them in the source. Does anyone know how I can compile those in to the firmware to get the jogger/joystick functionality? 





**"Mats-Ola Str\u00f6m"**

---
---
**Arthur Wolf** *August 11, 2017 09:41*

It's in a separate branch, it's untested, and not officially released. See [smoothieware.org - third-party-branches [Smoothieware]](http://smoothieware.org/third-party-branches)


---
**Arthur Wolf** *August 11, 2017 09:41*

Also, I want video of that machine working, thanks :)


---
**Mats-Ola Ström** *August 14, 2017 11:57*

Thanks! I will try to compile it into the latest edge somehow. Will post a video soon!


---
*Imported from [Google+](https://plus.google.com/109093233803703858186/posts/g1YTjXfPRKc) &mdash; content and formatting may not be reliable*
