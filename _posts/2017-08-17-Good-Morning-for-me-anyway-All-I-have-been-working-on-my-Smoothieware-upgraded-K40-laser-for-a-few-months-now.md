---
layout: post
title: "Good Morning (for me anyway) All, I have been working on my Smoothieware upgraded K40 laser for a few months now"
date: August 17, 2017 15:37
category: "General discussion"
author: "Kelly Burns"
---
Good Morning (for me anyway) All,  I have been working on my Smoothieware upgraded K40 laser for a few months now.  The basics all work great.  Thanks to the team for that.  I'm having issues with some quality and I'm beginning to suspect that a fubar'd some settings along the way.



1.  I can't seem to get the speed above 100 mm/sec when doing rasters.

2.  With all left to right engraving functions, I get Dark sparts on the left and right edges. 

3.  Same Laserweb G-Code generator produces crisp image on another GRBL-based Laser.



Any thoughts on some quick items to look at.  Maybe best to start with fresh config.?







 





**"Kelly Burns"**

---
---
**Arthur Wolf** *August 17, 2017 20:18*

This is a well known issue with Laserweb/Windows USB drivers/USB packet sizes ( grbl doesn't have it because it uses a different driver and USB/serial system. it's technically impossible for Smoothie to do the same, and we have no way to modify the Windows drivers. I believe it's possible modifying the way the Gcode is sent by LW could solve the issue but it hasn't been demonstrated it's possible with the nodejs stuff they are using ). I'm not going to go into details, there are other posts where this is explained. This has caused a lot of trouble with the LW folks saying this was a Smoothie problem, us Smoothie folks thinking we have proven it isn't ( I'm not going to be affirmative we have because this is super touchy and I don't want to start an argument again ) with rigorous experimentation, and everybody getting very upset at each other to the point we don't speak much anymore. Last time I checked ( and I might be wrong I haven't followed things closely lately ), the situation was that the LW devs didn't deny this was a USB/driver/packet sizes issue, but didn't think there was enough evidence to say it was either, and their position was that they plan to do experiments to figure out exactly what is going on, but they won't do them right away because they are essentially emotionally exhausted by the dispute at the moment ( my understanding, they might put it differently ).



<b>Please</b> do not tag in any LW people, I really don't want to start an argument again, I'm just explaining this so you are not left in the dark. <b>Please</b> if you bring this issue up with the LW people ( which I don't recommend, I believe they now have a policy to delete anything related to this until they have done the further experiments I mentionned, to protect everyone from further disputes ), do not mention I said this, we really want to start having a productive relationship with the LW people again ( I think it's starting to slowly happen, which is very good, but has taken a lot of time ), and starting this dispute again would ruin that perspective.



You might try playing the file from the SD card, depending on the SD card and the gcode resolution, it might solve the issue.


---
**Kelly Burns** *August 18, 2017 04:57*

Thanks for the detailed response.   I actually have tried to run it from the SD card. With the same results.  Same with using GCode sender.  I certainly understand the issues with the LW and raster engraving on Smoothie.  I'm fairly certain this is not it.  I definitely will not step on that landmine again.  



I was hoping there were some settings I messed up in my testing and playing over the past few months.  I'm beginning to think there is something physically wrong with my machine.  



After my upcoming busy weeks are finished, I'll tear it down and start from the beginning utilizing everything I have learned from everyone in these great communities.  



Thanks again and thanks for the great Firmware. 










---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/NoYpo1jCmmD) &mdash; content and formatting may not be reliable*
