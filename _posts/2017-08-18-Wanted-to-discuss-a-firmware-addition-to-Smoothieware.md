---
layout: post
title: "Wanted to discuss a firmware addition to Smoothieware"
date: August 18, 2017 16:46
category: "Development"
author: "Griffin Paquette"
---
Wanted to discuss a firmware addition to Smoothieware. After reading through the GitHub discussion about using ESP3D and Smoothieware, it became increasingly apparent that the two don't want to play nice through a gcode upload using the serial port on the LPC.



I have checked out how Duet3D is able to use a single SD card for both file upload and configuration, and it turns out that a flip-flop is used to control which device (the main chip or the esp) has control of chip select for the SPI bus. I was wondering if this is something that could be added so that wifi could be an onboard option.



In theory, the esp tells the LPC that it needs bus control so the LPC flips over the CS line and idles in terms of SD access until the file is uploaded. Just figured I would throw the idea out here in case it is a feature that could be easily implemented.





**"Griffin Paquette"**

---
---
**Arthur Wolf** *August 18, 2017 19:37*

I'm not sure how difficult this would be. You'd need to look at the code, see where the SD card is implemented, and see if that part can ignore/throw an error if a global "don't touch the SD" flag is set. 

Obviously this will only ever work for the non-MSD version of the firmware.


---
*Imported from [Google+](https://plus.google.com/111302122377301084540/posts/RiFWrVdKfED) &mdash; content and formatting may not be reliable*
