---
layout: post
title: "So i am making a Gatton CNC and i decided to use a Panucatt Azteeg X5 GT Smoothieboard"
date: August 20, 2017 21:43
category: "General discussion"
author: "Andrew Hague (Old English Workshop)"
---
So i am making a Gatton CNC and i decided to use a Panucatt Azteeg X5 GT Smoothieboard. I chose the Bigfoot 109A stepper drivers, and as i have 2 Y motors i would like to connect 1 to the Y on the board and the other E1 and enslave the other Y master. What is the easiest way to do this?? Anyone's help is appreciated. I am using the **+Mark Carew** high torque Nema 23 motors.





**"Andrew Hague (Old English Workshop)"**

---
---
**Andrew Hague (Old English Workshop)** *August 23, 2017 18:57*

[plus.google.com - Almost ready for prime time. Soon. Azteeg X5 GT with goodies.](https://plus.google.com/u/1/+EricLiensMind/posts/FHqHt5R6VsJ?fscid=z13izniixp2oi3hup23qs3cw1mnkhr5dz.1503285824702631)




---
*Imported from [Google+](https://plus.google.com/+OldEnglishWorkshop/posts/Svke9MwGSrw) &mdash; content and formatting may not be reliable*
