---
layout: post
title: "Is there someone that can explain the 132 char limit per line in the config file?"
date: August 24, 2017 03:14
category: "General discussion"
author: "Mark Ingle"
---
Is there someone that can explain the 132 char limit per line in the config file?  Is it a hard limit?...meaning 132 char period regardless of the # for making comments





**"Mark Ingle"**

---
---
**Douglas Pearless** *August 24, 2017 03:25*

it is a limit in the code:



bool FileConfigSource::readLine(string& line, int lineno, FILE <b>fp)</b>

<b>{</b>

<b>    char buf[132];</b>

<b>    char *l= fgets(buf, sizeof(buf)-1, fp);</b>

<b>    if(l != NULL) {</b>

<b>        if(buf[strlen(l)-1] != '\n') {</b>

<b>            // truncate long lines</b>

<b>            if(lineno != 0) {</b>

<b>                // report if it is not truncating a comment</b>

<b>                if(strchr(buf, '#') == NULL)</b>

<b>                    printf("Truncated long line %d in: %s\n", lineno, config_file.c_str());</b>

<b>            }</b>

<b>            // read until the next \n or eof</b>

<b>            int c;</b>

<b>            while((c=fgetc(fp)) != '\n' && c != EOF) /</b> discard */;

        }

        line.assign(buf);

        return true;

    }



    return false;

}




---
**Mark Ingle** *August 24, 2017 10:52*

Thank you!


---
**Douglas Pearless** *August 24, 2017 10:53*

Your welcome 


---
*Imported from [Google+](https://plus.google.com/102654723337396117182/posts/fKBZZ2RvXwg) &mdash; content and formatting may not be reliable*
