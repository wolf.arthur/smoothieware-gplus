---
layout: post
title: "Who here is running a Smoothie with a rotary 4th-axis?"
date: August 25, 2017 01:08
category: "General discussion"
author: "Reverend Eric Ha"
---
Who here is running a Smoothie with a rotary 4th-axis? How did you compile the firmware? The command I used was 



make AXIS=4 PAXIS=4 CNC=1



but after reading I'm not sure that was correct if I wanted the 4th axis to be rotary. 





**"Reverend Eric Ha"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2017 01:38*

Don't touch paxis


---
**Reverend Eric Ha** *August 25, 2017 01:52*

Ok, so just AXIS=4 should do me. Got it. 


---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2017 01:54*

Axis and CNC


---
**Reverend Eric Ha** *August 25, 2017 02:17*

oh yeah...forgot that...:)




---
**Reverend Eric Ha** *August 25, 2017 21:51*

OH, also.. It says to add some stuff to the config like the line below. Is it safe to assume that "delta_steps_per_mm" is actually steps per degree as suggested? 



# A axis

delta_steps_per_mm                    100     # may be steps per degree for example




---
*Imported from [Google+](https://plus.google.com/+ReverendEricHa/posts/dikW2knKCay) &mdash; content and formatting may not be reliable*
