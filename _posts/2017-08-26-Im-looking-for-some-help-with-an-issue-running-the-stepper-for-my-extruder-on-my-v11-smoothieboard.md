---
layout: post
title: "I'm looking for some help with an issue running the stepper for my extruder on my v1.1 smoothieboard"
date: August 26, 2017 10:34
category: "Help"
author: "Andrew Bellos"
---
I'm looking for some help with an issue running the stepper for my extruder on my v1.1 smoothieboard. When I send a command using Pronterface it will move slightly and then stop. The board then will no longer take any commands to the other axis for a duration of time. This amount of time seems to depend on how long it would take to extrude the amount of filament I ordered in the command. After this, the other axis will then move to the commands I had sent. After the extruder stepper moves slightly initially it will no longer move until I remove the usb cable that is powering the board. Simply hitting the reset button on the board the stepper won't allow that initial move. 



I know the stepper motor is wired correctly, I ve tested it, and it runs fine on the other stepper outputs on the board running the same amount of amps. I have tried using the most recent firmware and config file, and load them on a new sd card. I also tried formatting the old one and putting the new files on that too. I put ferrite beads on both ends of my usb cable and also tried a new one.  I've tried adding a 5v power supply so it would be running off the usb. 



I currently am using a fresh config file where I swapped the pin output for the extruder stepper and the x axis.  I have a spare stepper motor hooked up to M4 so there is no load on it. When initially powering up the board it will move between a 1/8 and 1/4 of a turn before shutting down. 



Here is my pastebin with my config

[https://pastebin.com/rw7whHzD](https://pastebin.com/rw7whHzD)



Any help would be much appreceited.





**"Andrew Bellos"**

---
---
**Arthur Wolf** *August 26, 2017 10:44*

Maybe it's stalling ? Try reducing the requested extruder movement speed, and if that doesn't help, the extruder acceleration.


---
**Andrew Bellos** *August 26, 2017 23:34*

I don't think it is stalling. I swapped the pin output for the x-axis and the extruder in the config file and then swapped the ports the steppers were plugged into accordingly. The x-axis that ran fine before on M1 now just turns slightly and then stops completely when hooked up to M4. It doesn't cause the delays in inputs to the other axis like when I had the extruder hooked up to M4. With the extruder hooked up to M1 it runs flawlessly. Right now I have a spare stepper motor that isn't attached to anything on M4, it runs fine on the other output for stepper using the same settings, so I don't see any reason it would be stalling. I'm still getting a small amount of movement and then it stops completely until I power cycle the board. 



I still currently have the spare stepper motor on M4 with the x-axis pin outs set to it. I went through and incrementally halved my x axis max speed down to 7500 and then my acceleration down to 750 testing it along the way, and it hasn't helped. 


---
**Arthur Wolf** *August 27, 2017 10:43*

Please do the following : 

1. Flash the latest version of the firmware [http://smoothieware.org/getting-smoothie](http://smoothieware.org/getting-smoothie)

2. Get the latest version of the config file and do not modify anything in it : [http://smoothieware.org/configuring-smoothie](http://smoothieware.org/configuring-smoothie)

3. Try moving each motor ( at low speeds ) and report results

4. Assuming M4 doesn't work, try the other motors on that port, report results

5. Try the M4 motor on each other port, report results



Thanks.


---
**Chris Chatelain** *August 27, 2017 17:44*

I made a similar thing happen by having my steps per mm be so high the stepper motor couldn't move



also try flashing the latest firmware using another sd card, I've had whacky results like that happen when one of my cards was flakey.


---
**Andrew Bellos** *August 29, 2017 11:38*

Okay, so I reflashed the firmware using the latest file and used the latest config file without making any changes, all on a new sd card.



All outputs still work fine except M4. The M4 motor runs fine on M1-M3. M1-M3 motor don't work when connected to M4. It has been the same with all the motors when connected to M4, the board will become unresponsive for the length of time it would take to extrude what I have sent to it using Pronterface. Also they all will occational make a small amount of movemnt at first when running them on M4 and then stop, I have to resest the board if I want to see that small amout of movment again. 


---
*Imported from [Google+](https://plus.google.com/101317522286252901844/posts/jDLGzz2qGha) &mdash; content and formatting may not be reliable*
