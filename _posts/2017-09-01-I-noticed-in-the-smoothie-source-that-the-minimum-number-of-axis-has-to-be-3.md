---
layout: post
title: "I noticed in the smoothie source that the minimum number of axis has to be 3"
date: September 01, 2017 05:57
category: "Development"
author: "Jason Dorie"
---
I noticed in the smoothie source that the minimum number of axis has to be 3.  For a laser, is there a reason it couldn't be 2?  Would reducing the number of actuators reduce processing time for the block queue?





**"Jason Dorie"**

---


---
*Imported from [Google+](https://plus.google.com/110748936089896575893/posts/Fe2ztQSNihc) &mdash; content and formatting may not be reliable*
