---
layout: post
title: "Alan and Jim are making progress on the Smoothieboard v2 firmware :"
date: September 04, 2017 10:54
category: "Development"
author: "Arthur Wolf"
---
Alan and Jim are making progress on the Smoothieboard v2 firmware : 

[https://acassis.wordpress.com/2017/09/03/programming-smoothieboard-v2-mini/](https://acassis.wordpress.com/2017/09/03/programming-smoothieboard-v2-mini/)







**"Arthur Wolf"**

---
---
**Sébastien Plante** *September 04, 2017 13:36*

Look so nice, I love it! 


---
**Ray Kholodovsky (Cohesion3D)** *September 04, 2017 14:26*

Wonderful. 


---
**Anthony Bolgar** *September 11, 2017 21:49*

Nice!


---
**lenne 0815** *September 22, 2017 08:14*

love the XT30 !


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/9U2J8Mo5dUM) &mdash; content and formatting may not be reliable*
