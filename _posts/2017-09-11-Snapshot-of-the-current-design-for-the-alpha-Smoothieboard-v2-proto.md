---
layout: post
title: "Snapshot of the current design for the alpha Smoothieboard v2 proto"
date: September 11, 2017 20:59
category: "Development"
author: "Arthur Wolf"
---
Snapshot of the current design for the alpha Smoothieboard v2 proto.

Logxen has been working on it for a while now, LayerOne ( of the Atom 3D printer ) are financing both the layout work and the manufacturing of the first few prototypes, so thank them for speeding up v2 a lot.

![missing image](https://lh3.googleusercontent.com/-Vys8mjAVvOM/Wbb5SauIiSI/AAAAAAAAPpE/16BIThJX6J4TG7hJMZjDFsaheaB1VLbLwCJoC/s0/Smoothieboard2Standard-pre2-3d.png)



**"Arthur Wolf"**

---
---
**Anthony Bolgar** *September 11, 2017 21:49*

Kudos to them for the support!




---
**Griffin Paquette** *September 11, 2017 23:52*

Will there be physically larger versions for the > 4 driver versions?


---
**Arthur Wolf** *September 12, 2017 09:42*

**+Griffin Paquette** We'll sell a dual extrusion kit which is a normal Smoothieboard v2 and a stepper driver extension board pre-wired.

For setups with even more extruders we have a system just for that where you can chain an infinite amount of extruder boards ( still in dev  ).


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/cF5oWearRA3) &mdash; content and formatting may not be reliable*
