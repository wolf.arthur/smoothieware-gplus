---
layout: post
title: "Hey, we need electronics help. On Smoothieboard v2 we want reverse polarity protection on the power inputs"
date: September 12, 2017 20:41
category: "Development"
author: "Arthur Wolf"
---
Hey, we need electronics help.



On Smoothieboard v2 we want reverse polarity protection on the power inputs. On v1 we have it only on the main power input, but on v2 we'd like ideally to have it also on the mosfet input ( even though errors are extremely rare ).

The problem is it's fairly expensive, we currently use a mosfet and a zener I believe ( see [https://docs.google.com/spreadsheets/d/1vshB97WWt6Lceo59aggbdhRuXxTEhSL9lkotScR0oYE/edit#gid=0](https://docs.google.com/spreadsheets/d/1vshB97WWt6Lceo59aggbdhRuXxTEhSL9lkotScR0oYE/edit#gid=0) ). but that's like >$1. If we now have two of them that's >$2 ...

That's a lot. Anyone has a trick or an idea for very cheap reverse polarity protection ? 12-24v, 0-20A.



Thanks a ton :p





**"Arthur Wolf"**

---
---
**Don Kleinschnitz Jr.** *September 12, 2017 21:30*

Can you point us to a schematic and what inputs you want protected?


---
**Arthur Wolf** *September 12, 2017 21:33*

There's no current schematic. We've got a a 12-24v, 20A input and we want the cheapest way to protect it against reverse polarity, pretty much.


---
**Ray Kholodovsky (Cohesion3D)** *September 12, 2017 22:30*

Reverse diode, blows a fuse. Then have an inline diode for the brain electronics just to be safe. 


---
**Griffin Paquette** *September 12, 2017 22:39*

^ this is an option. Could blow the diode too though if the current is too high and the fuse doesn't blow fast enough.


---
**Sébastien Plante** *September 13, 2017 00:37*

Not sur how hard it could be, but using sensor and relay, you could plug how you want and always have the right polarity? 


---
**Don Kleinschnitz Jr.** *September 13, 2017 03:31*

Have you considered these techniques. I have not reviewed this yet....



[maximintegrated.com - Reverse-Current Circuitry Protection - Application Note - Maxim](https://www.maximintegrated.com/en/app-notes/index.mvp/id/636)


---
**Griffin Paquette** *September 13, 2017 04:12*

A mosfet is awesome because it can handle the current without burning a lot of heat. Diodes usually have a decent voltage drop which results in quite a bit of heat generated. If you have it on the main input, why would you need it on the others? is there no regulator built in?


---
**Venelin Efremov** *September 13, 2017 04:15*

[google.com - www.google.com/url?sa=t&source=web&rct=j&url=http://www.ti.com/lit/an/slva139/slva139.pdf&ved=0ahUKEwjAp5WHpqHWAhXBwFQKHX48AWAQFgglMAA&usg=AFQjCNEgbgKvs3cD_Ee5HJDeIE-qO5csLQ](https://www.google.com/url?sa=t&source=web&rct=j&url=http://www.ti.com/lit/an/slva139/slva139.pdf&ved=0ahUKEwjAp5WHpqHWAhXBwFQKHX48AWAQFgglMAA&usg=AFQjCNEgbgKvs3cD_Ee5HJDeIE-qO5csLQ)


---
**Venelin Efremov** *September 13, 2017 04:18*

I would suggest 40a low resistance FET. You would still need a fuse to protect the feet from too much current in case the heated bed is short


---
**Triffid Hunter** *September 13, 2017 09:04*

Unfortunately, mosfet is basically the best way at those currents. The zener is required since most mosfets have a Vgs(max) of 20v and thus 24v would damage it. If you limit voltage to 15v or so, you can ditch the zener.


---
**Arthur Wolf** *September 13, 2017 10:35*

Hey ! Thanks for all the answers !

So, we are aware of the different options ( diode+fuse, mosfet, etc ), that wasn't really the question. 

The question was more "*do you know of specific parts ( as in : part numbers ) that are interestingly inexpensive and still do the job we need done here*". With all the talented PCB people around here I expect some of you have know parts we don't.



Thanks again :p


---
**Sean Houlihane** *September 13, 2017 10:38*

Seems like it would be best to diode protect the low-current circuit/voltage regulated circuits, and design the MOSFET output side to be tolerant of reverse connection by clamping the bias. Should be possible to avoid using any extra high-current devices, I hope.


---
**Tiago Gala** *September 14, 2017 14:53*

A relay could work. If you're going with 20A on both inputs it' harder to find something <1$, but for 10A, 24VDC: [digikey.pt - G5LE-1A DC24 Omron Electronics Inc-EMC Div &#x7c; Relays &#x7c; DigiKey](https://www.digikey.pt/product-detail/en/omron-electronics-inc-emc-div/G5LE-1A-DC24/Z3114-ND/369005) 

(0.67€ @ 500 un.)

 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/9DhCdPfwhy9) &mdash; content and formatting may not be reliable*
