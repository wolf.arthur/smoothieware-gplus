---
layout: post
title: "Win a free Smoothieboard ! Learn how here : Please re-share if like me you want to see a lot of entries :)"
date: September 20, 2017 20:50
category: "General discussion"
author: "Arthur Wolf"
---
Win a free Smoothieboard !



Learn how here : [http://arthurwolf.github.io/SmoothieContest/](http://arthurwolf.github.io/SmoothieContest/)



Please re-share if like me you want to see a lot of entries :)





**"Arthur Wolf"**

---
---
**Anthony Bolgar** *September 20, 2017 22:48*

Great idea!




---
**ThantiK** *September 21, 2017 01:46*

Are there any portions of the documentation that could actually be done if someone doesn't already own a smoothieboard? - I know someone who has their own printer, and already does videos, but she hasn't ever used any smoothie-anything.


---
**Arthur Wolf** *September 21, 2017 09:53*

You can do pretty much any part of the documentation, you don't need an actual board, just explain it. We even have a repo with illustrations : [github.com - Bouni/smoothieboard-graphics](https://github.com/Bouni/smoothieboard-graphics)


---
**Rob Mitchell** *November 03, 2017 19:02*

How can I enter the contest if I don't own a smoothieboard? I want to win one for a new CNC project build?


---
**Arthur Wolf** *November 04, 2017 00:00*

**+Rob Mitchell** The contest is for videos that teach users something about using Smoothie, mostly. You just have to learn from the documentation, then teach back in video format. That's something you can do without a board. Hope that helps.




---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/csep1Z4DmSq) &mdash; content and formatting may not be reliable*
