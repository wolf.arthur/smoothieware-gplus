---
layout: post
title: "Do you know somebody looking for a Laser Cutter or a CNC Router ?"
date: September 21, 2017 10:28
category: "General discussion"
author: "Arthur Wolf"
---
Do you know somebody looking for a Laser Cutter or a CNC Router ?

If you do, and you can convince them to buy our Open-Source-controlled machines ( see [robotseed.com](http://robotseed.com) , we can help you with the convincing ), we'll give you a substantial finder's fee !

Contact us at wolf.arthur@gmail.com

This is true in the future too, if you run into an opportunity, remember this post :)





**"Arthur Wolf"**

---
---
**Arthur Wolf** *September 22, 2017 20:37*

They ship with Smoothie in CNC mode. The machines come with Octoprint ( typical workflow is visicut -> octoprint -> smoothie ), and that doesn't have any of the gcode streaming problems Laserweb has. I'm confident laserweb will in the long run end up solving the issue, we gave them a lot of information about how to do it, including examples of code, and info about various solutions they can implement.


---
**Piotr Esden-Tempski** *September 28, 2017 21:28*

This looks awesome. Do you know if there are any programs helping out not for profit hackerspaces? Having open source and cutting edge machines based on smoothie would be nice. Alternatives like lasersaur or buildlog might be an option but I was curious what you would recommend to equip a space. (for context we are specifically interested in laser cutters at the moment)


---
**Arthur Wolf** *September 28, 2017 21:43*

**+Piotr Esden-Tempski** Don't know any such programs. We tend to give better prices to fablabs/hackerspaces as a company but that's it. You can email wolf.arthur@gmail.com for more details.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/LigzyQmSenr) &mdash; content and formatting may not be reliable*
