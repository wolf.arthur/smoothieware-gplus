---
layout: post
title: "I would like to connect an LED strip to one of the PWM pins on a Re-ARM board so I can turn the lights on and off through the printer interface"
date: September 26, 2017 03:45
category: "Help"
author: "Geoffrey Forest"
---
I would like to connect an LED strip to one of the PWM pins on a Re-ARM board so I can turn the lights on and off through the printer interface. 



I see on the Smoothieboard pinout that there are four LED pins at 1.18, 1.19, 1.20, and 1.21:

[http://smoothieware.org/pinout](http://smoothieware.org/pinout)



On the Re-ARM board, these pin numbers pass to servos 1, 2, 3, and 4 (although pin 1.21 is labeled as going to something called J5 on the Re-ARM pinout, but referring to the Mega and RAMPS pinouts, it seems to go to Servo 2):

[http://panucattdevices.freshdesk.com/helpdesk/attachments/1047536701](http://panucattdevices.freshdesk.com/helpdesk/attachments/1047536701)



I plan on turning on a certain pin number and I want to make sure I'm using an appropriate pin. Are these pins (1.18 - 1.21) good to use for turning on lights? Is there a better suited pin for this? I don't plan on using the servo pins for anything else.



Thanks





**"Geoffrey Forest"**

---
---
**Chris Chatelain** *September 26, 2017 05:05*

I just use a fan of spare heater pin for my lights. Then I don't have to mess with the board status LEDs.


---
**Wolfmanjm** *September 26, 2017 13:13*

You would need to turn of leds in the config. Then hook the pin to a mosfet, as the raw oin is just 3.3v and can only drive 5mA.


---
**Geoffrey Forest** *September 26, 2017 15:29*

Maybe its better to find another pin. Although, it looks like the Re-ARM board doesn't have LED status lights? Since it uses those pin numbers go to to the servo pins on the RAMPS? Although I had not thought about the voltage. The spare heater pin is probably best so I can get 12 volts directly out.


---
**Arthur Wolf** *September 27, 2017 10:01*

You can use any pin for anything you want ( mostly ). No need to use the LED pins, leave them alone doing their LED thing. See [smoothieware.org - switch [Smoothieware]](http://smoothieware.org/switch) to control a pin using gcode.




---
**Geoffrey Forest** *September 27, 2017 13:49*

I had not looked into the switch module before. Very versatile, thanks!


---
*Imported from [Google+](https://plus.google.com/+GeoffreyForest/posts/GKopjAqUquS) &mdash; content and formatting may not be reliable*
