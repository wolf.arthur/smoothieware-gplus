---
layout: post
title: "Smoothieboard v2-mini engraving the Open-Source Hardware logo : Picture of the result : Jim is making awesome progress on the v2 firmware"
date: September 27, 2017 20:48
category: "Development"
author: "Arthur Wolf"
---
Smoothieboard v2-mini engraving the Open-Source Hardware logo : 



[https://www.flickr.com/photos/wolfmanjm/36688764273/](https://www.flickr.com/photos/wolfmanjm/36688764273/)



Picture of the result : [https://www.flickr.com/photos/wolfmanjm/37328449462/](https://www.flickr.com/photos/wolfmanjm/37328449462/)



Jim is making awesome progress on the v2 firmware. Still a ton of features lacking, but basic machining is there, this is really cool progress.





**"Arthur Wolf"**

---
---
**Wolfmanjm** *September 27, 2017 20:57*

I think enough features are there to 3d print, Extruder, heaters, thermistors all seem to work. There is a switch module, and a laser module .


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/YuJCjeUZLUn) &mdash; content and formatting may not be reliable*
