---
layout: post
title: "1st cut on the Gatton CNC powered by a Panucatt Azteeg X5GT SMOOTHIEBOARD"
date: September 30, 2017 13:28
category: "General discussion"
author: "Andrew Hague (Old English Workshop)"
---
1st cut on the Gatton CNC powered by a Panucatt Azteeg X5GT SMOOTHIEBOARD. 





**"Andrew Hague (Old English Workshop)"**

---
---
**Andrew Hague (Old English Workshop)** *September 30, 2017 13:29*

And big thanks to **+Eric Lien** for his help on figuring out the few issues I got stuck on. 


---
**Eric Lien** *September 30, 2017 14:26*

**+Andrew Hague**​, glad to be of help. I love seeing first moves and first cuts on machines. Thanks for mentioning me so I got the notification.



Your first cut was more successful than mine. On mine I tried acrylic will all the wrong feedrates. Ended up with a ball of plastic wrapped around my only endmill :)






---
**Andrew Hague** *October 01, 2017 02:48*

So just in case anyone else wants to use the Azteeg X5 GT Smoothieboard i wrote down all the steps to get everything working. 




---
**Andrew Hague** *October 01, 2017 02:48*

[https://app.box.com/s/gctl5nbegya9xq79d8o13vamd1pzfxr2](https://app.box.com/s/gctl5nbegya9xq79d8o13vamd1pzfxr2)


---
**Don Kleinschnitz Jr.** *October 02, 2017 12:40*

#GattonCNCAztexX5GT



Well done build log thanks for sharing


---
*Imported from [Google+](https://plus.google.com/+OldEnglishWorkshop/posts/5QL1yeXuoSi) &mdash; content and formatting may not be reliable*
