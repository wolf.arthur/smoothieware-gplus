---
layout: post
title: "Recently I taught a few hours-long class at the local Fablab, basically a \"beginner's how to intro\" to Smoothieboard for 3D printing ( in French )"
date: October 02, 2017 10:05
category: "General discussion"
author: "Arthur Wolf"
---
Recently I taught a few hours-long class at the local Fablab, basically a "beginner's how to intro" to Smoothieboard for 3D printing ( in French ).



Mostly just went over the 3D printer guide with lots of extra comments.



I'm thinking I could do a similar thing in English over the Internet, as a livestream. People have been asking for something like this, and the recording of the livestream could ( if it's not too terrible ) be added to the documentation.



Would you watch ? Have ideas of how to organize it ? Have ideas of how to make sure people watch it live/how to promote it ?



Thanks :)





**"Arthur Wolf"**

---
---
**Douglas Pearless** *October 02, 2017 10:36*

Great idea, Perhaps treat it as someone who has ordered a Smoothie and go through the unbox to getting it working.  Show different 3D printer arm solutions ( Cartesian, linear Delta, coreXY) and perhaps a laser cutter?


---
**Miguel Sánchez** *October 02, 2017 11:02*

That seems like a great addition to documentation. I think walking the new user through the wiring and configuration would be great.


---
**John Harris** *October 02, 2017 13:38*

Sounds like a good idea. I've been printing for four years, but am still learning. Last year I switched from ABS to PETG, now I'm getting ready to try WOOD. On my Smoothieboard printer, I just installed a GEEETECH MK8 Direct Drive Extruder, which meant going back to basics (1) Calculated steps/mm from effective diameter, and step rate. (2) Guess at thermistor type, and test flow with different temperature settings, etc. The extruder should be a good topic for the show. How about using a Google group for live input? I will watch.




---
**Michaël Memeteau** *October 02, 2017 22:47*

I would love to see how to interface load cells to use as Z-probe (through an ADC like the HX711 for example). Probably not the typical setup, but...


---
**Douglas Pearless** *October 02, 2017 22:48*

Just a quick comment, load cells are not very reliable for z-probes 😄


---
**Michaël Memeteau** *October 02, 2017 22:50*

**+Douglas Pearless** Are talking about FSR or real load cells? Curious to hear about your experience... I guess the heating bed shouldn't help either.


---
**Douglas Pearless** *October 02, 2017 23:16*

FSR


---
**Thomas “Balu” Walter** *October 03, 2017 08:09*

You could start by adding those "extra comments" to the existing documentation? ;) Biggest problem with live streams is timezones. Promoting should be easy on here or other social medias 


---
**Hakan Evirgen** *October 03, 2017 11:46*

**+Arthur Wolf** that is great idea! Did you though about doing such thing together with one of the 3D printer youtuber Gurus? I was thinking of **+Thomas Sanladerer** as he is in my opinion one of the most objective and also best in explaining things.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/LEx6g3gxwJA) &mdash; content and formatting may not be reliable*
