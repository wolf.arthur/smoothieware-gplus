---
layout: post
title: "Hello, I'm hoping someone can answer a question for me because my searches for an answer aren't yielding anything"
date: October 03, 2017 18:58
category: "Help"
author: "Michael Escamilla"
---
Hello,



I'm hoping someone can answer a question for me because my searches for an answer aren't yielding anything.



I've recently setup a 2-in-1 hotend, specifically the Prometheus System setup. I'm using Simplify3D as my slicer and a community member has created a post processing script for handling the color changes more reliably. But I'm having issues with extruding.



I believe the issue is because the post processing script is requiring that I enable "relative extruder distances" in S3D. So when the initial gcode is created, all the extrusion distances are set to "relative" distances. The start of the gcode is:

G90

M83

and the print will start fine since positioning has been set to absolute and extruder is overwritten to relative with M83.



After running the post-process script is when I get problems. This script is adding a purge tower and the gcode for the tower switches to G91 relative positioning, and when the purge is done, it switches back to G90 absolute positioning. So the print starts off fine by printing the purge tower, then as it starts to print the part, which has absolute position for XYZ but Relative values for E, my extruder starts to freak out.



My initial thought was because the last line before starting the part is switching back to G90 Absolute, and this is also setting the Extruder to Absolute. Which is why the extruder sounds like its missing steps and clicking.



To troubleshoot I manually added a M83 after every G90 in the gcode and the print works perfectly.



After speaking with the writer of the post processing script, he mentioned to me that on marlin firmware you do not need to keep sending M83 after switching between G90/G91, and he thinks smoothieware maybe handling this different.



So my question is does switching between G90/G91 effect the extruder as well? And how does M82/M83 effect the extruder when switching between G90/G91?



Really appreciate any insight on this as I'm not too familiar on how these codes usually work on other firmwares/controllers.





**"Michael Escamilla"**

---
---
**Wolfmanjm** *October 04, 2017 09:59*

Yes G90/G91 sets absolute/relative for everything, there is no standard here, we just do things differently than marlin.. so stick a extruder relative after the G91.


---
**Michael Escamilla** *October 07, 2017 01:24*

**+Wolfmanjm** thanks for info. That's what I needed to know! 


---
*Imported from [Google+](https://plus.google.com/108371578672674936948/posts/W15Sd2LsMjb) &mdash; content and formatting may not be reliable*
