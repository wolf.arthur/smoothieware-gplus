---
layout: post
title: "Hello again !. I hope that someone could help me with this problem"
date: October 03, 2017 10:55
category: "General discussion"
author: "Antonio Hern\u00e1ndez"
---
Hello again !. I hope that someone could help me with this problem.



I want to "catch" ==> ALARM <== messages when my gcode file is running.



I suppose there are a lot of reasons to show these type of messages containing that word (for example, if endstops were not detected or if limit switches were enabled and gcode file contains coordinate values that are not allowed).



I thought that FTDI - MRI communication could help me to "speak" to the board and get these kind of messages (while gcode files are running), including the fact of sending gcode commands and get some responses. I sent some gcode commands but, honestly, I was more confused with these "debug" mode. I saw the options, just a few, but, it wasn't clear to me how could I get these "ALARM" messages if they were thrown at "playing time".



I have sent some gcode commands using FTDI breakout (without using MRI mode) and, yes, the board responds well as it normally does. (gcode command - ok response...).



I want to "speak" ALARM messages when these type of messages were thrown at any time. I want to use an arduino sketch and emic2speech to "talk" these  ALARM messages if they were detected in playing time.



Creating a simple test, I was able to send gcode commands over serial port (window given by arduino IDE) using an arduino mega (even I did a test sending play command using a file inside the board, and it worked),  but at the same time, I want to receive the response for each command that I have sent ("ok" response or whatever), and the last thing, didn't happen.



My commands are received without problems, but, for each one, I can not see the response given by the board. Mainly, I want to know, what can I do to "listen"  (like pronterface using "debug communications" option) all the responses given by each gcode command processed by the board, and send "the response flow" to arduino serial port (like a listener ?). Is it possible ?.

That situation will allow to control ALARM events, and do something after that event...



I want to speak alarm messages very loud, including the fact to show some leds "flashing" when these type of events happened. I know that "arduino" is not the main topic, but any comment willl be appreciated.



As usual, thanks a lot for your help and support.





**"Antonio Hern\u00e1ndez"**

---


---
*Imported from [Google+](https://plus.google.com/107343842763021715447/posts/Z4esLMdc2u8) &mdash; content and formatting may not be reliable*
