---
layout: post
title: "I am experiencing a problem with the temperature switch"
date: October 03, 2017 17:47
category: "General discussion"
author: "Brian Zellers"
---
I am experiencing a problem with the temperature switch. When I try to use an mcode to arm the switch, it does not work. If I comment out the temperatureswitch.module_name.arm_mcode, it works fine. I am trying to achieve the ability to press a macro (set to send M1100 S1) that turns on both a fan and heater, which heats my build chamber to a predetermined temperature, then turns off the heater and fan till the temperature drops and needs reheating. I currently have arm_mcode commented out which turns my heater and fan on at the same time the board turns on, which works, but I would like to be able to turn on and off the heated chamber based on what material I am running. I have tried changing the trigger and inverted settings to all 6 possible configurations with no success. When I have the arm_mcode command set and I send the M1100 S1, I get a response of Temperature Switch Enabled, but nothing ever happens. Any help would be appreciated, I also posted this same question on the smoothieboard forum, and have my config settings listed there. Thanks.





**"Brian Zellers"**

---
---
**Wolfmanjm** *October 03, 2017 18:36*

I think you are confusing temperature switch and switch.



Switch turns things on and off with mcodes, whereas temperature switch turns things on and off based on a temperature NOT an mcode, I think you also misunderstand what the arm code does. I hope this is all documented int the wiki so please take a look there.




---
**Brian Zellers** *October 03, 2017 18:52*

**+Wolfmanjm**  I understand the difference, the temperature switch is supposed to turn on and off based on temperature, but there is a setting to have a mcode that disables/enables the temperature switch script. If I remove the line of code:



temperatureswitch.chamber.arm_mcode          1100



The temperature switch works as it should,  my heater and fan turn on and run till my therm reaches 60C. then the fan and heater turn of till 58C when they turn back on. This cycle works fine during a print, etc. But when I add the above line of code, and then send the command M1100 S1 through Simplify3D. I get a response temperature switch enabled, but the fan and heater never turn on.



Correct me if I'm wrong but the arm code command is simply supposed to enable/disable the temperature switch script to run.


---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2017 19:00*

When you send the command manually, roughly what temperature are the devices at? 

The tempswitch itself only activates when the threshold is crossed (and it checks on boot) thus if you are already hot and arm it, it would not activate. 


---
**Brian Zellers** *October 03, 2017 19:04*

I have tried activating it when the the temp is low like 30C and I have heated the therm up, with a heat gun, to around 110C. Both produced no results.


---
**Brian Zellers** *October 03, 2017 19:21*

**+Ray Kholodovsky** **+Wolfmanjm**  After some more testing I did get the temperature switch to work. It's just not working how I intended it to work. The way it is currently working is, if I arm the temperature switch then heat the therm up past 60C, manually, with a heat gun, then it maintains that temperature (it turns on when the temp drops below 60C and off when the temp rises above 60C [within a hiesterisis of around 5C]).



So now my question is, is there a way to set it so that when I activate the arm command, the heater and fan kick on right away without needing to have the therm heated first? I'm going to play with the trigger and invert settings some to see if there is a combination that yields the result I want.


---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2017 19:29*

You could manually turn the switch itself on using a defined gcode (M42 is the example in the stock config file) as part of your macro.  

If I understand your intention, it is to kickstart the switch the first time whenever you press the macro button.  Adding the M42/ equivalent would turn on that switch. 


---
**Wolfmanjm** *October 03, 2017 19:31*

No, that i s not what the arm function is designed to do. It was designed to turn off a power supply when the temperature drops below a certain point but ONLY when the print has finished so the arm command is put at the end of the gcode to arm the switch to turn off when everything has cooled down.




---
**Wolfmanjm** *October 03, 2017 19:32*

If you want to turn the fan on immediately then use the mcode defined in the switch module not the temperature control module. The switch generally has a mcode defined to turn it on or off.


---
**Brian Zellers** *October 03, 2017 20:01*

**+Ray Kholodovsky** **+Wolfmanjm** Thank you, gentlemen. I created a macro that turns the switches on and enables the temperature switch. And the did the trick.


---
*Imported from [Google+](https://plus.google.com/110884982661099816710/posts/NHpUosBYGHM) &mdash; content and formatting may not be reliable*
