---
layout: post
title: "I have a smoothieboard 5XC running my MPCNC machine and have the drivers paired together since I have 2 motors each for X and Y"
date: October 04, 2017 15:15
category: "General discussion"
author: "Steve Hogg"
---
I have a smoothieboard 5XC running my MPCNC machine and have the drivers paired together since I have 2 motors each for X and Y. However I'm wondering if there was a way to do this without the jumpers. It'd like have an endstop on each X or Y rail and have the axis home and align itself. The reason I'm asking is I've noticed on my X/Y is not truely square and it would be nice to have the endstops set correctly and then the system could self-align itself.



I thought I could have done this with a post-process script, but then I was lost when I started looking at G2/G3 commands which do get used in my setup.







**"Steve Hogg"**

---
---
**Arthur Wolf** *November 01, 2017 22:20*

There is a way to do this, but it requires quit a good understanding of Smoothie's switch module, and of electronics.

Pretty much you can use switch modules to enable/disable specific drivers with Gcode, and use endstops wired in parralel to have one endstop per side of an axis. With that you can write a homing "gcode file" that will do what you want. 


---
*Imported from [Google+](https://plus.google.com/106157807810449032928/posts/2TTFcUMA6BM) &mdash; content and formatting may not be reliable*
