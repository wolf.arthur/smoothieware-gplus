---
layout: post
title: "From *** @ > Would you mind telling me what you are using the board for ?"
date: October 07, 2017 20:39
category: "Development"
author: "Arthur Wolf"
---
From <b>***</b> @ [nasa.gov](http://nasa.gov)



> Would you mind telling me what you are using the board for ?



[...] We are using the [smoothie]board in a 3D Refabricator (plastic printer) aboard the International Space Station.  For what it’s worth, you can tell your customers that your contribution is helping NASA make deep space exploration a possibility by developing technologies for in-situ fabrication and repair of tools and structures.





**"Arthur Wolf"**

---
---
**Anthony Bolgar** *October 07, 2017 20:54*

Awesome!!!!!


---
**Todd Fleming** *October 07, 2017 22:53*

Wow!


---
**Douglas Pearless** *October 07, 2017 23:05*

Superb!!


---
**Jack Colletta** *October 07, 2017 23:41*

Very cool, great work


---
**Kamil Steglinski** *October 08, 2017 00:11*

Cool😃


---
**Sébastien Mischler (skarab)** *October 08, 2017 05:05*

Hahaha Smoothie-On-Board !!! Bravo :)


---
**Anthony Bolgar** *October 08, 2017 06:11*

If NASA handles the travel arrangements, I am willing to offer on-site support for them for free. :)




---
**Thomas “Balu” Walter** *October 08, 2017 10:28*

We need a picture of that printer or a video of one astronaut printing a piece :)


---
**Rodrigo Bugni** *October 08, 2017 11:41*

Nice!


---
**Maxime Favre** *October 09, 2017 07:08*

"Achievement unlocked"


---
**Gerald Dachs** *October 11, 2017 17:56*

Has the lack of gravity any influence on bed adhesion? 


---
**Arthur Wolf** *October 11, 2017 17:58*

**+Gerald Dachs** I think from what I heard, 3D printing in space actually takes very little tuning from what we do here on earth. I hope they publish some details at some point though.


---
**Carl Fisher** *November 30, 2017 21:26*

Ok, that's just freaking cool. Old post but I just saw it.




---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/26Mrre3LRQp) &mdash; content and formatting may not be reliable*
