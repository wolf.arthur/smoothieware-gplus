---
layout: post
title: "Guys, I'm hoping to get some clarification on powered optical switches and how the limit switch pin-out on the smoothie-board relate (if at all)"
date: October 10, 2017 18:20
category: "Help"
author: "Chuck Comito"
---
Guys,

I'm hoping to get some clarification on powered optical switches and how the limit switch pin-out on the smoothie-board relate (if at all). My optical switches are 5v. I want to use a separate 5v power supply to power the switch because the 5v output of the limit headers don't seem to be able to handle all 6 switches. I would then connect the signal output of the switch to the S pin on the  limit switch headers for each switch. What I'm not sure of is if the input pin (S) on the smoothie is somehow referencing it's voltage (high or low) to the ground pin next to it? 

I might be totally off base here all together.. Let me know if you need clarification on anything. I'm not very good at asking technical questions. LOL









**"Chuck Comito"**

---
---
**Arthur Wolf** *October 10, 2017 21:47*

Just connect your power supply to the 5V input ( next to the VBB input ), then the 5V output on your limit switch connectors will use that.


---
**Chuck Comito** *October 10, 2017 22:14*

Thank you **+Arthur Wolf**​ I will do that. It didn't even cross my mind. Out of curiosity, would it matter if I did it the other way I mentioned? I think in my mind I figured I'd have to tie the ground from the separate psu to the smoothie but then I'd have a potential ground loop. For me it's really a matter of clarity. I don't expect a lesson in electronics but if your feel like sharing I won't complain :-)


---
**Arthur Wolf** *October 10, 2017 22:16*

Would probably have worked. Just not very tidy.


---
**Chuck Comito** *October 11, 2017 01:00*

**+Arthur Wolf**​, just wanted to say thanks and I'm up and running (limits switches anyway). I have a problem with the wiring on my y limit but I'll sort that. Thanks for the input. 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/4njK2cw2u2C) &mdash; content and formatting may not be reliable*
