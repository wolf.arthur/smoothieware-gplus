---
layout: post
title: "Do I need to remove the reset switch on the smoothieboard to wire an external reset switch?"
date: October 11, 2017 23:13
category: "Help"
author: "Chuck Comito"
---
Do I need to remove the reset switch on the smoothieboard to wire an external reset switch? I read that I can issue the reset command via a console but can I do this same function via hardware?





**"Chuck Comito"**

---
---
**Chuck Comito** *October 11, 2017 23:30*

Found it... [smoothieware.org - switch [Smoothieware]](http://smoothieware.org/switch#setting-up-a-reset-button)


---
**Arthur Wolf** *October 12, 2017 08:27*

No need to remove the reset switcgh.


---
**Chuck Comito** *October 12, 2017 10:53*

Yes **+Arthur Wolf**​. I did find the tutorial on switch functions. I think i found every function I needed and am in the process of adding the data to the config file. The only hang up I have is the play command. I'm not sure if there's a way to "push button" a run job with the job being loaded from laserweb. Looks like that function is reserved for sd card play. 


---
**Arthur Wolf** *October 12, 2017 10:57*

Smoothie has no way to tell Laserweb to do anything, or even know Laserweb is present, you probably want a button in Laserweb or something ...


---
**Ray Kholodovsky (Cohesion3D)** *October 12, 2017 13:51*

The case I see for this is "dwell until user pushes switch button on smoothieboard" which is on your to do list but not in yet. Start the job in LW but wait for input on the board so the user has time to get to the machine. 



M600 is a possible workaround, if that works in cnc mode. 


---
**Chuck Comito** *October 15, 2017 20:52*

Thanks guys. It doesn't really matter to me as my pc is right next to my laser. It would have been cool of I could get all the switches on the Fanuc controller to do what their labels say. 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/EeZ6HNAh8oQ) &mdash; content and formatting may not be reliable*
