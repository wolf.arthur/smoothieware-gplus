---
layout: post
title: "My auto-level is made on the pressure sensor and operates below the bed level"
date: October 15, 2017 14:10
category: "Help"
author: "\u0410\u043d\u0434\u0440\u0435\u0439 \u0412"
---
My auto-level is made on the pressure sensor and operates below the bed level. The printer delta. When sent, G32 issues "Calibration failed to complete, check the initial probe height and / or initial_height settings". I found out that the error is present only when calibrate_delta_endopps. How to fix?





**"\u0410\u043d\u0434\u0440\u0435\u0439 \u0412"**

---
---
**Arthur Wolf** *October 15, 2017 15:24*

Sounds like you haven't set the right values for the max_travel settings.


---
**Андрей В** *October 15, 2017 16:37*

Did not help installed max_travel 2x gamma_max. G32 E works without errors, and G32 R with an error.


---
*Imported from [Google+](https://plus.google.com/114856401284730264713/posts/9Xy3ZoLGpN3) &mdash; content and formatting may not be reliable*
