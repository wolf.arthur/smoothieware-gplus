---
layout: post
title: "I just put together an Open Builds CNC machine and I am using a Smoothie to control it"
date: October 16, 2017 23:24
category: "Help"
author: "Phil Aldrich"
---
I just put together an Open Builds CNC machine and I am using a Smoothie to control it.  I just flashed the latest version of firmware (firmware-cnc.bin).  I am running a v1.0b board.  I have a Tr8*8-2p (8mm-2mm pitch) screws.  My stepper motor is a standard 1.8 degree, 200 step device for the X, Y, and Z axis.  The current is set to 2A.  I set my alpha_steps_per_mm to 400 by using the calc:



step/mm = ((fullsteps per rotation) * (microsteps per step)) / (mil per rotation)



which should equate to:



400 = ((200 * 16) / 8)



I read that a v1 board uses 16 microsteps vs 32 for the newer boards.  This appears to be the case for me.  When I move the X axis stepper 1mm, I get .91mm as measured by a micrometer.  When I move 10mm, I get 9.97mm.  If I move several hundred mm's (say 500 as measured with a meter stick), I will be over by several mm's (approx 503 to 504mm).  Something doesn't quite add up.  Any suggestions of things to check?  Could other settings effect the scaling?  I am obviously missing something.



BTW - I have a TinyG board that I swapped in and with it's recommended settings, I get the expected measurements as per the micrometer - both at 1mm (within .01mm) and 500mm.





**"Phil Aldrich"**

---
---
**Sébastien Plante** *October 16, 2017 23:39*

Hum... with a Leadscrew TR8 with 2mm pitch running 200 step, you should be around 1600 in the setting ? 


---
**Sébastien Plante** *October 17, 2017 00:06*

Move 100mm and do this formula :



New Steps/mm = (Current steps/mm) x [100 / (measured distance traveled)]






---
**Phil Aldrich** *October 17, 2017 00:13*

**+Sébastien Plante** I moved it 300mm to make it easier to measure. The actual distance traveled (as best as I can eyeball) is 302mm. By the formula you provided, that comes out to 397.35 mm


---
**Phil Aldrich** *October 17, 2017 00:14*

**+Sébastien Plante** I replaced 100 with 300. 


---
**Sébastien Plante** *October 17, 2017 00:28*

So you did 

300/302 = 0.99338

400 x 0.99338 = 397.35



Change your config from 400 to 397.35, reset and try again :)


---
**Phil Aldrich** *October 17, 2017 00:34*

**+Sébastien Plante** will give it a shot tomorrow. Thanks for your help. 


---
**Wolfmanjm** *October 17, 2017 20:48*

If it is a true tr8x8 then there is no fudge it will be exactly 400 or 800 steps/mm depending on your microstrpes (1/16 or 1/32) the advice to use some fractional steps/mm is bogus. You need to find out why it is not moving the correct distance. It could be they are not tr8x8, or there could be some mechanical issue. fudging the [steps.mm](http://steps.mm) when they should be exact is not the solution


---
**Phil Aldrich** *October 17, 2017 23:15*

**+Phil Aldrich** I agree that "fudging" is not the best answer but using 397.35 seems to make it much more accurate when I run the X axis out to 300+ mm.  It just seems odd that when using a TinyG board, I can set it exactly to 400 and it is dead on at 1mm or 300mm.


---
**Wolfmanjm** *October 18, 2017 08:23*

I suspect you are losing steps by having acceleration too fast and/or feed rate too fast. Although I would think it would stop short if pulses were lost.




---
**Sébastien Plante** *October 18, 2017 10:18*

**+Wolfmanjm** Got something there, maybe compare/copy the jerk, acceleration and others setting from the TinyG.



And I saw I miss readed the threaded rods pitch is 8 et not 2 :)


---
**Phil Aldrich** *October 18, 2017 10:33*

I'll double check the settings against the TinyG. Thanks all for the help. 


---
*Imported from [Google+](https://plus.google.com/105080379699420524192/posts/dJL7P6okA9s) &mdash; content and formatting may not be reliable*
