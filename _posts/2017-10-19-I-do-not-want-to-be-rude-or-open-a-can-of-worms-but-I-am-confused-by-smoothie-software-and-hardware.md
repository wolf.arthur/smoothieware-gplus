---
layout: post
title: "I do not want to be rude or open a can of worms, but I am confused by smoothie software and hardware"
date: October 19, 2017 12:39
category: "General discussion"
author: "Ron Ginger"
---
I do not want to be rude or open a can of worms, but I am confused by smoothie software and hardware. Am I correct that there are several boards that all run smoothieware? Are these all 'fair use' of the open status of smoothieware? 



I bought a cohesion3d board, which I understand runs smoothie ware. I was going to install it in my K40, but I have discovered k40whisper and like it a lot, so I will not be putting the board in it.



I have an old Solidoodle 4 printer that could use an upgrade. Can I use the cohesion3d board on it? Will I use the same smoothieware?





**"Ron Ginger"**

---
---
**Arthur Wolf** *October 19, 2017 13:00*

Hey.



The Smoothie firmware is developped by a community of Open-Source  contributors around the world. The Smoothieboard hardware is developped by mostly the same crew.

Buying the Smoothieboard hardware from one of the official sellers strongly supports the development effort of both the firmware and hardware. 

Open-Source derivatives of the hardware are generally considered "ok" though we wish they'd contribute more. Closed-source derivatives of the hardware are generally frowned at and you might have a hard time finding volunteers ready to spend their time helping you with them.



Cohesion is Open-Source and runs the Smoothie firmware.


---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2017 14:08*

The Mini is definitely capable of driving a 3D Printer with a single extruder and a heated bed up to say 10-12 amps.  So for a typical 200x200mm i3 or similar delta this is perfect. 

Not 100% sure about how many amps the solidoodle bed takes. 



You might just need additional drivers to make a total of 4 and flash the original smoothie firmware and stock config file back onto the board/ memory card and take it from there :) (the laser bundle flashes the Smoothie-cnc firmware and has a k40 specific config file, so you'll want to go back to the defaults and configure from there). 


---
**Wolfmanjm** *October 20, 2017 16:09*

Buying a genuine smoothie board from Robotseed or Uberclock (or one of their distributors) is the only way of contributing to future firmware development. Although Panucatt (who makes the azteeg range) has contributed a small amount to firmware development, the reduction in sales to arthur et al is not made up for. None of the other people who make, sell and profit from smoothie firmware compatible boards have contributed anything to firmware development, even though they use (and profit) from the firmware. As I volunteer my time as the currently primary developer for smoothie firmware I do not expect to get paid, however the project will die IMO if Arthurs sales are continually nibbled away at by others who do not contribute. This is my opinion on the subject that you asked about (I speak for myself and not Arthur who may well hold differing views).


---
**Ron Ginger** *October 20, 2017 17:46*

Thanks for the replies. I understand and fully support the idea that those who benefit should also support. That is what I meant in the original note when I said 'fair use' of the firmware. My problem here was that I did not understand who all the players are, and who does and does not support the firmware. In this case I made a hasty decision on reading about the cohesion board. By the time it arrived I understood more, and regret buying it. They do not accept returns, so I will make use of it in my 3d printer. Maybe I can contribute something of my experience as I get into it.


---
*Imported from [Google+](https://plus.google.com/104419714348493626449/posts/aA9x3cbqHUe) &mdash; content and formatting may not be reliable*
