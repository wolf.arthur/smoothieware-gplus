---
layout: post
title: "Im having a problem with my bltouch"
date: October 24, 2017 20:49
category: "General discussion"
author: "C\u00e9sar Ara\u00fajo"
---
Im having a problem with my bltouch. 



The problem is as you can see in the video that when the extruder goes to the some areas of the printing bed (the right side and the back side) it rises a lot compared to the bed. 



I have done a 49 probe point calibration a minute before the video and this is what i ended up with. 



Any help?




{% include youtubePlayer.html id="tn1_pYCSfXg" %}
[https://youtu.be/tn1_pYCSfXg](https://youtu.be/tn1_pYCSfXg)



Attached is the gcode for the calibration of the bed, the start.gcode from cura/simplify and the config of my smoothieboard. Any help is apreciated, thsnks.



[https://mega.nz/#F!uJoE2Rbb!6-T0jiCbVzGjbevnPfR64Q](https://mega.nz/#F!uJoE2Rbb!6-T0jiCbVzGjbevnPfR64Q)





**"C\u00e9sar Ara\u00fajo"**

---
---
**Taylor Landry** *October 25, 2017 17:13*

Why 49 point? Seems excessive for that size bed. My guess is that you got some errant probe points.



I'd recommend trying MatterControl and configuring the leveling to take 5 samples at each point to see if there's a hardware or firmware issue. 



Change the "Lower/Deploy" value to 3.0,  and the Raise/Stow value to the correct Smoothie servo value. You'll also need to change the Z offset to match your setup. (the Z offset is distance between the probe and the nozzle when the probe is deployed)

![missing image](https://lh3.googleusercontent.com/NK5g65z-lgMjQK11TL0GMZ_jlASviRQMz6Go4JVxOUT2CF9KB1e_JqW0eMtDNofZwFQA7AiEuD_dZdyK88-0d5HmYAJyABdpq8A=s0)


---
**Arthur Wolf** *October 29, 2017 22:58*

What's the offset between your hotend and probe ?


---
**César Araújo** *October 29, 2017 23:00*

25x 30y, im going to put it on the right so it safely probes. 


---
**César Araújo** *October 29, 2017 23:01*

I'll report tomorrow 


---
**Arthur Wolf** *October 29, 2017 23:01*

That's a huge offset that's probably the problem here, you want to put the probe as close to the hotend as possible, tell smoothie config the exact value of the offset, and only move to places that have been measured by the probe. 


---
**César Araújo** *October 29, 2017 23:03*

Yes ill make that, otherwise outside the probing area i must not print. 


---
*Imported from [Google+](https://plus.google.com/111486081448945611757/posts/Ek91TuhWpe9) &mdash; content and formatting may not be reliable*
