---
layout: post
title: "Does Smoothie do accumulation of relative GCode moves in fixed precision (IE, the gcode values as large integers simply without a decimal) or float precision (as in, the gcode values after they've been converted to float)?"
date: October 27, 2017 05:52
category: "Help"
author: "Jason Dorie"
---
Does Smoothie do accumulation of relative GCode moves in fixed precision (IE, the gcode values as large integers simply without a decimal) or float precision (as in, the gcode values after they've been converted to float)?



I'm generating GCode output for an image plot as all relative moves, as multiples of 0.1mm, and the image slowly leans to the right.  If I run the same GCode through a back-plotter it comes out as a perfect rectangle.



The only thing I can think of is that 0.1 is a repeating binary decimal, so accumulating in float space over time would lose bits, whereas accumulating in integer before converting to float wouldn't have that problem.



Thoughts?  I can post the GCode if anyone wants to try it for themselves.  It's almost entirely X-relative G1 moves with S- commands for laser power.





**"Jason Dorie"**

---
---
**Triffid Hunter** *October 27, 2017 08:21*

I believe smoothie accumulates floats, however this is the exact reason that we strongly recommend using absolute positioning.



Even if smoothie handled your numbers perfectly, you would still slowly lose bits due to the limitations of merely converting the delta value to a float and representing it as a human-readable string unless your hostware had an elaborate scheme to track float error and periodically add slightly longer moves - at which point you've simply reinvented double or quad precision which is rather more complex than simply using absolute positioning in the first place.


---
**Arthur Wolf** *October 27, 2017 13:44*

Smoothie is known to be able to do bitmap raster with relative gcodes without problem, so I would suspect more of a mechanical/electronics issue than a coordinate one.


---
**Jason Dorie** *October 27, 2017 15:52*

Triffid - the software does track the delta error and adjust accordingly - it’s no more complex than error diffused dithering.  I emit relative moves because the code is significantly smaller, so it’s worth the effort.



I haven’t run into this issue with Smoothie before, but this is the first time I’ve done an engraving of this size / detail level before. My users have, so I’m pretty certain this is a settings or hardware issue with a new device, but I figured it was worth asking. 



Thanks guys.


---
*Imported from [Google+](https://plus.google.com/110748936089896575893/posts/AMy9wphnQp2) &mdash; content and formatting may not be reliable*
