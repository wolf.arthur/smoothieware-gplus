---
layout: post
title: "Hello everyone. I would like to know what is the purpose of this command ?"
date: October 29, 2017 08:24
category: "General discussion"
author: "Antonio Hern\u00e1ndez"
---
Hello everyone. I would like to know what is the purpose of this command ?

"get [fk|ik] [-m] x[,y,z]". This command appears in smoothieboard help.



I don't know if this command is related with some type of machine (cartesian, delta...) or if it does not matter and applies for all possible configurations. I'm not an expert, so, I would like to know just a little bit about that command, and the concepts related with it (if they exist). I can't see in the docs something about that.



What type of scenarios this command must be used or considered ?



Thanks.







**"Antonio Hern\u00e1ndez"**

---
---
**Arthur Wolf** *October 29, 2017 10:15*

It's mostly useful for debugging, the docs give the syntax as : 

get [pos|wcs|state|status|fk|ik]


---
**Antonio Hernández** *October 29, 2017 10:20*

Ok. Perfect. Thanks for the info, I thought this parameter must be used in particular cases.


---
*Imported from [Google+](https://plus.google.com/107343842763021715447/posts/MedFJPCCRAo) &mdash; content and formatting may not be reliable*
