---
layout: post
title: "So last night during a lightning storm my powered limits started glitching out"
date: November 05, 2017 17:54
category: "General discussion"
author: "Chuck Comito"
---
So last night during a lightning storm my powered limits started glitching out. Today after a couple power cycles and firmware updates I no longer have any limits. No power to them at all. Think it could be the 5v regulator? 





**"Chuck Comito"**

---
---
**Arthur Wolf** *November 05, 2017 18:29*

Possibly. Measure the 5V output on the endstop inputs. Also try powering your limits separately from the board and see if they work that way.


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/MSNfeKSXU3K) &mdash; content and formatting may not be reliable*
