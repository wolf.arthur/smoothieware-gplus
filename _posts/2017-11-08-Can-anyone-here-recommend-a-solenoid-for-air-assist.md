---
layout: post
title: "Can anyone here recommend a solenoid for air assist?"
date: November 08, 2017 03:44
category: "General discussion"
author: "Chuck Comito"
---
Can anyone here recommend a solenoid for air assist? How's you go about it and is a relay in order? Just looking for some info to get me started as I have no clue! Thanks. 





**"Chuck Comito"**

---
---
**Don Kleinschnitz Jr.** *November 08, 2017 13:25*

Below are ideas to get you started. There are some important design nuances and part selections that are not included below. I can help you when you decide your general approach.

..............

<b>General Approaches</b>



I would decide if 1.) I need an air valve or 2.) can I just turn off the air compressor.



<b>1.)</b> this may require;

<i>a solenoid like this:</i> 

[amazon.com - Robot Check](https://www.amazon.com/Inch-Electric-Solenoid-Valve-Water/dp/B06XN5K3RY/ref=sr_1_3?s=industrial&ie=UTF8&qid=1510146233&sr=1-3&keywords=24v+air+valve&dpID=41ggQu4I-lL&preST=_SY300_QL70_&dpSrc=srch)



I guessed at 24 vdc and 1/4"  but you have to decide based on your available power and size plumbing.



<i>a driver like this:</i>

You will need a solenoid driver some thing like this will work. I like it because it has an opto-coupler on the input isolating it from the processor. You will need to decide where to get the power from and how to filter it as solenoids can be electrically noisy.



 [https://www.amazon.com/gp/product/B01N482TF2/ref=oh_aui_detailpage_o02_s05?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01N482TF2/ref=oh_aui_detailpage_o02_s05?ie=UTF8&psc=1)

......................................... 

Alternately can use a AC solenoid and switch it with an SCR.

[https://www.amazon.com/Electric-Solenoid-120VAC-Normally-Closed/dp/B00DQ1ACQ2/ref=sr_1_3?s=industrial&ie=UTF8&qid=1510146858&sr=1-3&keywords=ac+air+solenoid&dpID=411uUU70rQL&preST=_SX342_QL70_&dpSrc=srch](https://www.amazon.com/Electric-Solenoid-120VAC-Normally-Closed/dp/B00DQ1ACQ2/ref=sr_1_3?s=industrial&ie=UTF8&qid=1510146858&sr=1-3&keywords=ac+air+solenoid&dpID=411uUU70rQL&preST=_SX342_QL70_&dpSrc=srch)



[https://www.amazon.com/SMAKN-SSR-25DA-Solid-State-24-380V/dp/B00PQ7Y8ZU/ref=sr_1_7?s=industrial&ie=UTF8&qid=1510146906&sr=1-7&keywords=ssr+relay&dpID=41dNO-vXk9L&preST=_SX300_QL70_&dpSrc=srch](https://www.amazon.com/SMAKN-SSR-25DA-Solid-State-24-380V/dp/B00PQ7Y8ZU/ref=sr_1_7?s=industrial&ie=UTF8&qid=1510146906&sr=1-7&keywords=ssr+relay&dpID=41dNO-vXk9L&preST=_SX300_QL70_&dpSrc=srch)



<b>2.)</b> Just put a relay or solid state relay in line with the air compressor. (this is what I did because my air is a separate motor). 



In all cases you will have to decide what port to use on the smoothie and how you are going to control and configure it. What port you use will impact what driver you use for the solenoid or relay.


---
**CescoAiel** *November 08, 2017 15:30*

I like using SSRs for these things as I can drive those directly from a Smoothie or similar boards!...


---
**Chuck Comito** *November 08, 2017 15:51*

I like the idea of controlling the air and leaving the compressor on. I have a rather large compressor with a tank. Ideally, Id like the spindle on (laser on) and air on to work at the same time. The  valve you pointed me to (**+Don Kleinschnitz**) seems like the ticket. The switch also seems to be a winner based on the digital trigger function.  I think I could then use the same "laser_module_pwm_pin" 2.5 and duplicate the line in the config file but define an unused pin and use that pin to enable/disable the switch??

![missing image](https://lh3.googleusercontent.com/yGcSsqdqWtpn-UtINVqcrSZ0zlc-Hqr2DzWJdzI529aRZ27nm0qu3YiyW_MJzpoWEAoBBiJoTGg1RU4Q202NXiEoLjGsMeQJY4o=s0)


---
**Don Kleinschnitz Jr.** *November 08, 2017 17:10*

**+Chuck Comito** if you can wait a few days I can design this up for you [others may like one, including me]. Got to get some committed projects completed.



You will need to add a few components to make it reliable and less noisy. Also need to look at the specs on all these parts to insure they are compatible. Looks like you will need roughly 1A@24vdc. 

Although theoretically you can power this from the same supply as your controller power I don't like to put motors and solenoids on the same supply as the processor. 

Are you running an external 24vdc for the controller or using your LPS 24V??



Something like this will work but you could also get a higher capacity supply for future expansions and make that your 'dirty" supply.



[amazon.com - Robot Check](https://www.amazon.com/Phihong-PDA024A-1A0S-R-Regulated-Switching-Adapter/dp/B00HKJES0C/ref=sr_1_26?s=industrial&ie=UTF8&qid=1510160829&sr=1-26&keywords=24vdc+1A+power+supply)



What is your budget goal :)?



.....

To drive it from the smoothie you would find another (not your PWM) OPEN DRAIN connection on the smoothie and configure it as a switch that you turn on with an M command. Does not need to be a PWM capable pin.


---
**Chuck Comito** *November 08, 2017 17:42*

Hi **+Don Kleinschnitz**, personally I have 2 independent power supplies and the LPS. The LPS is strictly for the laser. I then run another independent supply to my motor drives and have a third "dirty" supply as an expansion supply. The additional 2 supplies are both 24vdc at 10amps.  As far as the budget goes, I'm open to doing the job right.  I was worried about the noise from the solenoid. I think there will be (although my electronics experience is very limited) a spike when the coil collapses. I can wait though. No hurry here. It would be great to have a "how to" on this subject. 


---
**Don Kleinschnitz Jr.** *November 08, 2017 17:52*

**+Chuck Comito** great I will assume a 24vdc dirty power avail. There will be a spike when the coil energy collapses and we will "snub" it with a diode in the final design :).


---
**Chuck Comito** *November 08, 2017 23:10*

Can you make this pretty **+Don Kleinschnitz**​! Haha. I did go ahead and order the parts. When you get some time I can implement the idea and report back how it works. Once all the issues are ironed out maybe this is something you put on your site!

![missing image](https://lh3.googleusercontent.com/bv9oeNH5RX6P4_Wzewq3yxQzrYvTk8Q7zI9o8s33lSgqlnfblpbalKk0c2kieWbACYW6kirU7hjj9qGok5Zh4jPW9S7dK8UzXJ8=s0)


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/jE3WeGwaaEM) &mdash; content and formatting may not be reliable*
