---
layout: post
title: "Even though the M119 command shows all endstop pins as 0, when I press home the machine moves and a bit in wrong direction and stops"
date: November 16, 2017 18:16
category: "General discussion"
author: "B"
---
Even though the M119 command shows all endstop pins as 0, when I press home the machine moves and a bit in wrong direction and stops.  I can't home the device, what could be wrong?







**"B"**

---
---
**Arthur Wolf** *November 16, 2017 21:06*

Can you show us the exact output of M119 with no endstop pressed, and the same thing with all endstops pressed. Thanks.


---
**B** *November 16, 2017 21:28*

nothing pressed, X_min:0 Y_min:0 Z_min:0 pins- (X)P1.24:0 (X)P1.25:0 (Y)P1.26:0 (Y)P1.27:0 (Z)P1.28:0 (Z)P1.29:0



all pressed (except z Max since I can't send the command while pushing all on the machine lol): X_min:1 Y_min:1 Z_min:1 pins- (X)P1.24:1 (X)P1.25:0 (Y)P1.26:1 (Y)P1.27:1 (Z)P1.28:1 (Z)P1.29:0 



Also I messed with some DIP switches and now machine behaves different, all home options only move Y axis, and end stops pushing won't stop anything.  


---
**Arthur Wolf** *November 16, 2017 21:31*

DIP Switches ?


---
**B** *November 16, 2017 21:33*

Sorry for double posting I am very confused about google+ 



DIP Switches: 1,4,7,8 ON rest is OFF



Motors: NEMA 23 High Torque from open builds


---
**B** *November 16, 2017 22:40*

Sorry Xmax is also working properly even though i posted as 0.  I messed around with dip switches, changed to 3,7,8 ON and motors don't produce extreme heat anymore.  With pronterface I am still not able to set home, limit switches won't stop motors in both situations where I manually press during moving them in any direction or while it is trying to go home.  


---
**Kamil Steglinski** *November 17, 2017 01:11*

Dip switches? Are you using external stepper motor drivers. What's the recommended voltage on the input signals on the drivers. Some take 5V, and act erratically at lower voltage levels. The Smoothie board puts out 3.3V on the external driver pins. 

Also, do you have the end stops also set as the limit switches?


---
**B** *November 17, 2017 05:11*

I found the problem, in case someone else spends 2 days trying to figure it out.  First one was dip switches were all set to off, I had to set them properly , currently sitting at 1600 pulse/rev and 2.8 something peak current.  Second and most major problem was not being able to home or do anything, messed with endstops and config for 2 days straight, all of the problems were caused by cnc firmware of smoothieboard.  I don't know what went wrong or if it is my mistake somewhere but there is a possibility of something seriously wrong with the cnc specific firmware on github


---
**Wolfmanjm** *November 17, 2017 18:39*

There is absolutely NOTHING wrong with the CNC firmware on github, 100's are using it daily with NO problem. Maybe the problem is you did not read the documentation that says $H is home NOT G28 when using the CNC firmware?


---
**B** *November 17, 2017 18:41*

I used strictly pronterface, like i said base firmware fixed all the problems all is good just saying what my experience was thanks for all the hlep


---
**Wolfmanjm** *November 17, 2017 18:53*

Pronterface does not work for CNC especially in GRBL mode. Don't blame the firmware when it is your mistake Try a host that works for CNC like BCNC. or don't use grbl mode if you do not understand what it is doing or what it is for.


---
**B** *November 17, 2017 23:46*

"Don't blame the firmware if it is your mistake" Maybe you should work on understanding what you read better.



If I understood all these different things I wouldn't be coming here for help, I didn't know GRBL mode completely makes pronterface useless when the documentation suggests using pronterface for testing,  like I said if it is my mistake/ignorance but in case it wasn't, it could mean firmware or something else, worth noting right?.  I don't know why you are not reading it properly assuming I attack you or your code, I said nothing against either.  I explained my situation as a beginner that this was my experience and may help someone in the future.  I will add my contribution to the wiki, unless it is there and I missed it, that having GRBL mode on, or using cnc-firmware, will not work with software like pronterface


---
*Imported from [Google+](https://plus.google.com/113399877302676550349/posts/EniwR1Xp2zD) &mdash; content and formatting may not be reliable*
