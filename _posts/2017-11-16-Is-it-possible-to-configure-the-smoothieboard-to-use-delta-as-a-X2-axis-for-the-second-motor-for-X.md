---
layout: post
title: "Is it possible to configure the smoothieboard to use delta as a X2 axis for the second motor for X?"
date: November 16, 2017 05:21
category: "General discussion"
author: "B"
---
Is it possible to configure the smoothieboard to use delta as a X2 axis for the second motor for X? So that it can send same signal to both X axis motors?





**"B"**

---
---
**Arthur Wolf** *November 16, 2017 10:10*

[http://smoothieware.org/3d-printer-guide#doubling-stepper-motor-drivers](http://smoothieware.org/3d-printer-guide#doubling-stepper-motor-drivers)


---
**B** *November 16, 2017 14:00*

I saw that which suggests jumpering them together but my question is basically if i can assign multiple pinouts to alpha through the config. 


---
**Arthur Wolf** *November 16, 2017 15:43*

Nope, that'd be computationally extremely expensive, so it's not likely to be implemented when a perfectly functioning electrical solution exists.


---
**B** *November 16, 2017 15:46*

Thank you for your help quick help. Basically I have to run cables from one driver to another for PUL>PUL, DIR>DIR, ENB>ENB and so that only one pinout on the board sends signal, two drivers share the signal and send to two different motors all this sounds correct 


---
**Arthur Wolf** *November 16, 2017 15:48*

Yep


---
*Imported from [Google+](https://plus.google.com/113399877302676550349/posts/Ud3TViJAubN) &mdash; content and formatting may not be reliable*
