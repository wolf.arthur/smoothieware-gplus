---
layout: post
title: "Smoothieboard is under the top 10 - 3D Print App users Originally shared by Mathias Dietz Most used 3D Printer types by App users (2017)"
date: November 19, 2017 20:16
category: "Machine showcase"
author: "Mathias Dietz"
---
Smoothieboard is under the top 10 #GCodePrintr - 3D Print App users 



<b>Originally shared by Mathias Dietz</b>



Most used 3D Printer types by #GCodePrintr App users (2017). 

![missing image](https://lh3.googleusercontent.com/-YBWDzfBX-lw/WhHl-wBFqAI/AAAAAAAAT3U/BwNOHBALAmUpqpChGtSCUQXJ6mjgsrRjwCJoC/s0/GcodePrintr-users.png)



**"Mathias Dietz"**

---
---
**Jeff DeMaagd** *November 19, 2017 21:07*

This seems to be mixing controller types with machine models and mechanism types.


---
**Mathias Dietz** *November 19, 2017 21:12*

**+Jeff DeMaagd**  Yes you are right. Some of the "Prusa i3 or i2" might run with Smoothieboard too.


---
*Imported from [Google+](https://plus.google.com/+MathiasDietz/posts/dhruvoY1seB) &mdash; content and formatting may not be reliable*
