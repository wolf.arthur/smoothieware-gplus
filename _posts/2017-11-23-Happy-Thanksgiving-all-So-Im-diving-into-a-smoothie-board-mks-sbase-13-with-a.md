---
layout: post
title: "Happy Thanksgiving all! So, I'm diving into a smoothie board mks sbase 1.3 with a"
date: November 23, 2017 19:45
category: "General discussion"
author: "Dan Morningstar"
---
Happy Thanksgiving all! So, I'm diving into a smoothie board mks sbase 1.3 with a. Anycubic delta. I'm trying to zero it out, but its not taking gcode commands from pronterface. I have tried reinstalling the config and firmware. I CAN use the adjustment guide to mode all axis'. I cannot get the extruder to move or retract nor am I able to track temp in pronterface. When I send g28 I get a chirp from the strippers. Additionally I can open cura and run a print but it dkesnt zero home first. Any insight would be helpful





**"Dan Morningstar"**

---
---
**Steve Anken** *November 23, 2017 19:58*

" I get a chirp from the strippers"   ?????


---
**Dan Morningstar** *November 23, 2017 20:11*

**+Steve Anken** auto correct fail... Stepper motors


---
**Dan Morningstar** *November 23, 2017 20:13*

I'm beginning to think my end stops are incorrectly wired. I only have two wires on my end stop switches. 


---
**Eric Lien** *November 23, 2017 20:24*

You might need put an invert on the endstops in the config. Send an m119 and look at the output.



[smoothieware.org - guide-endstops [Smoothieware]](http://smoothieware.org/guide-endstops)


---
**Dan Morningstar** *November 28, 2017 12:14*

So after a weekend of pretty much not touching it I got it working correctly by starting from square one. Now I need to tweek the fan settings and get the arm radius and tgermistor set correctly


---
*Imported from [Google+](https://plus.google.com/108528324069988069507/posts/PmYrTibAsrD) &mdash; content and formatting may not be reliable*
