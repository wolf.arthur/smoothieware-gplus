---
layout: post
title: "Good night ! . Again, I'm here trying to solve some issues"
date: November 26, 2017 06:04
category: "General discussion"
author: "Antonio Hern\u00e1ndez"
---
Good night ! . Again, I'm here trying to solve some issues. I hope that someone could bring me some help or any kind of comment. I'm trying to use lcd panel as an additional tool to move stepper motors. I have two lcd panels, the first one is 16*2 and the last one is 20*4. (Both are from sparkfun). There are (20*4 ==> [https://www.sparkfun.com/products/9568](https://www.sparkfun.com/products/9568))  and (16*2 ==> [https://www.sparkfun.com/products/10862](https://www.sparkfun.com/products/10862)). I saw the guide on smoothieware site to setup the required configuration for lcd panels. 20*4 lcd has the option to be controlled by parallel interface (JP1 section). 16*2 lcd has parallel interface by default. Both require the presence of arduino board and a rotatory encoder (In case of arduino board, I'm using arduino uno, and for the encoder, I'm using one from keyestudio ==> [http://www.keyestudio.com/shop/keyestudio-rotary-encoder-module.html](http://www.keyestudio.com/shop/keyestudio-rotary-encoder-module.html)). Using 20*4 lcd and setting up the recommended wiring for universal panel adapter (also, adding the required parameters to smoothieboard config file), the lcd works, and the rotatory encoder works, but, it's only for a moment. The encoder stops working and the panel did not refresh the contents (both stop working). I have tried changing the encoder resolution and menu offset parameter values, but, the behavior is different for each test, and finally, with the same results. It appears that the lcd stucks and the encoder didn't work. If I turn the encoder, I can appreciate some kind of "refreshing" over lcd, but nothing happens. Sometimes the encoder and the panel work and I can see more shown elements  (for example, play, jog, reverse commands, and more internal options for each "click"). I have to disconnect my arduino board to see an updated content over the lcd panel. The hex file for parallel panel was uploaded to arduino uno board. The behavior is weird and it's not the same for each executed user action. With the second lcd panel (16*2) nothing happens, even I can not see any kind of text on the screen. The wiring was done two or three times, I'm sure the wiring complains with the recommended steps. I'm stuck in this part, and, reading other users with similar problems, I don't know what additional configurations can I do. Any suggestion will be appreciated. Thanks !.





**"Antonio Hern\u00e1ndez"**

---
---
**Wolfmanjm** *November 26, 2017 12:28*

16*2 is not supported. 16*4 should work but probably needs to be modified for the type of lcd you are using. see the source code


---
**Antonio Hernández** *November 27, 2017 00:57*

Thanks for your comment **+Wolfmanjm**. The number of lines was changed to 20 (instead of 16). Changes were applied over Panel.h file. I saw the full code (all files) of panel and screen directories. I think this file contains the parameters to be changed. (uint16_t screen_lines:16, uint16_t menu_current_line:16;)... I created a new firmware.bin file and flashed into smoothieboard, but..., the behavior is the same. Are there more tips or suggestions ?. If changing lcd panel could be an option, which lcd panel didn't give issues while it's working ?. Some weeks ago, I thought viki2 panel could be a good choice. Thinking to change to 16 *4 basic lcd , are there more possibilities it could work ?.


---
**Wolfmanjm** *November 27, 2017 09:16*

No you cannot change the lines it is 8. The change in code is in the arduino NOT smothieware. Your panel is NOT compatible with the arduino Universal adapter as is and will need to be modified to drive your specific panel. 


---
**Antonio Hernández** *November 27, 2017 16:50*

8 was not changed (that's another variable). The lines changed are related with the value of 16 (only the two variables mentioned before).  I will try to use an accepted panel from the list. Thanks for your help  **+Wolfmanjm**.


---
**Antonio Hernández** *November 29, 2017 21:39*

+Wolfmanjm, thinking about your comment, I understood why you said 8. When I was compiling the source code (smoothieboard) the process sent a warning about the value of changed variable. The type was changed only to see if the warning dissapear, and yes, it was, but, finally, the variable's original type was not changed and the test was done but without news. Leaving the fact that your suggestion points to change arduino code (universal adapter section and available source code), I'm going to use viki2 panel. To be honest, I don't have the enough knowledge to add another kind of panel over current source code, and, obviously, at the moment, I don't know the business logic to make the relevant modifications.  Anyway, as it has always been, thanks for your comments and support.


---
*Imported from [Google+](https://plus.google.com/107343842763021715447/posts/TDmByHnLxLJ) &mdash; content and formatting may not be reliable*
