---
layout: post
title: "Hi all, looking for some help with my board"
date: November 28, 2017 08:37
category: "General discussion"
author: "Colin Rowe"
---
Hi all, looking for some help with my board.

I have the cohesion3d board and I am looking to set it up for both Z and A motors.

I have set the Z motor up and its moving with commands from laserweb4.

First question is, What is the code to set the correct steps per mm, is it "gamma_steps_per_mm"?



second question, I also want to put in an A axis and have found the code needed to put in.

# A axis

delta_steps_per_mm    100     # may be steps per degree for example

delta_step_pin        2.3!o   # Pin for delta stepper step signal

delta_dir_pin         0.22!o  # Pin for delta stepper direction

delta_en_pin          0.21!o  # Pin for delta enable

delta_current         1.5     # Z stepper motor current

delta_max_rate        3000.0  # mm/min

delta_acceleration    500.0   # mm/sec²



It also asks for

Change the compile command from 'make clean all' into two separate ones:

make clean

make AXIS=4 CNC=1



Once the compile finishes, there should be a main.bin file in a folder titled LPC1768. Rename the file to firmware.bin and copy it onto the SD card of the Cohesion3D Mini. Open the config.txt file and make the same changes to the 'gamma' stepper module as described above.



question is, I cannot seem to work out how to compile the file.

just dont seem to understand what I need to do. is there a video for the PC that shows step by step.

I Thanks in advance





**"Colin Rowe"**

---


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/5ZToVMCcTLX) &mdash; content and formatting may not be reliable*
