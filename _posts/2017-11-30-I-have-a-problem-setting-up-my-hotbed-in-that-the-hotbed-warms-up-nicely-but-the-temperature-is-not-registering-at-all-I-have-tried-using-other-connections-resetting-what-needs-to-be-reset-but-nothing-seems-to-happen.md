---
layout: post
title: "I have a problem setting up my hotbed in that the hotbed warms up nicely but the temperature is not registering at all, I have tried using other connections resetting what needs to be reset, but nothing seems to happen"
date: November 30, 2017 21:58
category: "General discussion"
author: "Howard Cooper"
---
I have a problem setting up my hotbed in that the hotbed warms up nicely but the temperature is not registering at all, I have tried using other connections resetting what needs to be reset, but nothing seems to happen. i get reading from the hotend but not the bed, I have tested the thermister using a multimeter and heating the Bed and have seen the resistance change as the temperature rises. the hotend on T0 returns two readings but reports both as T in simplify3D, The same happens on Pronterface. The lights on the board show normal. I have checked the troubleshooting guide and followed the guide for this problem.  added to this when the T1 Connection is live and T0 is not we get inf. when T0 is connected and T1 is not, two readings appear,  does anyone have any helpful suggestions?





**"Howard Cooper"**

---
---
**Arthur Wolf** *December 01, 2017 09:36*

can we see the result of the M105 command, as well as your config file on [pastebin.com - Pastebin.com - #1 paste tool since 2002!](http://pastebin.com) ? Thanks.


---
**Howard Cooper** *December 02, 2017 21:24*

I Have sent the config file to [pastebin.com](http://pastebin.com) it includes the result of

sending the M105 instruction. I signed up under the username gismogajet



Regards


---
**Arthur Wolf** *December 03, 2017 11:36*

You need to give us the exact link to your config file, that's how pastebin is used.


---
**Howard Cooper** *December 03, 2017 20:08*

The link is [https://pastebin.com/NCAE3rNA](https://pastebin.com/NCAE3rNA)  at least thats the URL at the

top of the page, sorry I failed to send this before but I am not used to

the way this works yet!



Regards



James


---
**Arthur Wolf** *December 03, 2017 20:24*

Hey.



It says "This is a private paste. If you created this paste, please login to view it."



You need to just go to [pastebin.com](http://pastebin.com), paste your config, submit, and give the resulting link.


---
**Howard Cooper** *December 04, 2017 09:33*

I will check and re-paste but I thought I had set it for public.


---
**Howard Cooper** *December 04, 2017 09:43*

I have re-pasted the config the link is: [https://pastebin.com/s9fyVs6M](https://pastebin.com/s9fyVs6M)



Regards



James



On 4 December 2017 at 09:33, Howard Cooper <*<b>***</b>@<b>**</b>> wrote:



> I will check and re-paste but I thought I had set it for public.

>


---
**Arthur Wolf** *December 05, 2017 16:25*

" temperature_control.bed.thermister " is incorrect, it should be "thermistor".



More in general, when editing config, you need to do what the documentation says : start from the example config file, and edit only what is STRICTLY necessary to get things to work, and not anything more. Once everything works, you can save the working file, and play with the formatting, but not before.


---
**Howard Cooper** *December 06, 2017 13:59*

Sometimes men do stupid things and I am no exception, Thank you for your

help it is appreciated.

I am hoping to build my own printer at a later date and understanding

smoothieboard and smoothieware is key to that end.

I hope to use the Laser module on that printer.

My intention now is to put a copy of my config file on my computer and do

what you suggest replace it with the original example and start again not

because I have to but because I want to.


---
*Imported from [Google+](https://plus.google.com/102204126880754239275/posts/8jBCZpsJVHQ) &mdash; content and formatting may not be reliable*
