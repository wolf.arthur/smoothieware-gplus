---
layout: post
title: "are alpha_acceleration and beta_acceleration implemented? I'm working with a C3D mini board running the 10/17 firmware-cnc.bin firmware"
date: November 30, 2017 20:28
category: "General discussion"
author: "Carl Fisher"
---
are alpha_acceleration and beta_acceleration implemented?



I'm working with a C3D mini board running the 10/17 firmware-cnc.bin firmware. I'm trying to split out acceleration of X and Y axis on my laser.



I've tried to comment out the main acceleration and set only the alpha and beta values but it brings the machine to a complete crawl. I re-enabled the main acceleration thinking that alpha and beta would be treated as overrides but it only seems to obey the main acceleration setting.



I'd really like to be able to set my Y acceleration slower than X to accommodate the weight difference between the two.





**"Carl Fisher"**

---
---
**Arthur Wolf** *December 01, 2017 10:06*

I think you want both acceleration and *_acceleration uncommented.


---
**Carl Fisher** *December 01, 2017 12:51*

I did try that but it only seems to obey the acceleration no matter what I put in _acceleration. 


---
**Arthur Wolf** *December 19, 2017 13:53*

that's very weird, it really should work. are you certainly certain it's the latest firmware you have ? version command says ?

also copy/paste that part of your config here, maybe that'll help.


---
**Carl Fisher** *December 19, 2017 21:45*

# Planner module configuration : Look-ahead and acceleration configuration

planner_queue_size                           32               # DO NOT CHANGE THIS UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING

acceleration                                 4500             # Acceleration in mm/second/second.

alpha_acceleration							 5000				#Acceleration in millimetres/second/second for the alpha actuator ( X axis on cartesian )

beta_acceleration							 3000				#Acceleration in millimetres/second/second for the beta actuator ( Y axis on cartesian )

#z_acceleration                              500              # Acceleration for Z only moves in mm/s^2, 0 uses acceleration which is the default. DO NOT SET ON A 




---
**Arthur Wolf** *December 19, 2017 21:47*

you have planner queue size in there which means this is a super old config. you need to update to the very latest firmware, and to the very latest example config file, and port your values to it


---
**Carl Fisher** *December 19, 2017 21:48*

Interesting. Ok as of the time I posted this I was using the latest cnc firmware, but I've never replaced my config file as I did a lot of tweaking to get it working initially. I'll have to grab a new config and try to move all my other settings over.




---
**Joe Spanier** *December 19, 2017 21:58*

Im having the same issue. And also have planner_queue in mine running the latest cnc_firmware. Ill try the new config as well.


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/j53PMWMiCks) &mdash; content and formatting may not be reliable*
