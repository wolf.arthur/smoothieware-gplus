---
layout: post
title: "I can't seem to find this online for some reason(even Google Verbatim doesn't help)"
date: December 01, 2017 00:01
category: "General discussion"
author: "Reverend Eric Ha"
---
I can't seem to find this online for some reason(even Google Verbatim doesn't help). How do I disable auto locking my stepper motors in the Smoothie config?





**"Reverend Eric Ha"**

---
---
**Triffid Hunter** *December 01, 2017 04:57*

what is auto locking? do you understand how steppers work? See M18 in [smoothieware.org - supported-g-codes [Smoothieware]](http://smoothieware.org/supported-g-codes)


---
**Arthur Wolf** *December 01, 2017 09:30*

I'm not sure what you mean by auto-locking, can you explain ?


---
**Reverend Eric Ha** *December 02, 2017 00:32*

Oh, sorry. Some controllers will automatically "lock" the steppers upon powering up so they won't allow any accidental movement of your spindle, etc. and I was under the impression it was something in the config files, not g-code itself. 


---
**Peter Cruz** *December 02, 2017 01:33*

I think you mean m84 to turn off the motors or m99 <axis><value>



[http://reprap.org/wiki/G-code](http://reprap.org/wiki/G-code)


---
*Imported from [Google+](https://plus.google.com/+ReverendEricHa/posts/F48C8uoBT3f) &mdash; content and formatting may not be reliable*
