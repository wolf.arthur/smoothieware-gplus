---
layout: post
title: "Hi guys, i have a couple questions"
date: December 02, 2017 02:42
category: "General discussion"
author: "Chuck Comito"
---
Hi guys, i have a couple questions. Does anyone here have experience with optical endstops you can share with me? Basically, I can't seem to get my switches to ground. They seen to only go as low as 1.2vdc or so. Second question, is it normal for the 5vdc regulator to only output 4.7vdc? 







**"Chuck Comito"**

---
---
**Jeff DeMaagd** *December 02, 2017 03:33*

Is this 1.2VDC while connected to controller? Are you certain the wiring is correct?



4.7VDC will work but the regulator is probably overloaded. Tap the regulator chip very briefly to feel if it’s hot.


---
**Don Kleinschnitz Jr.** *December 02, 2017 13:55*

Are these end-stops on a K40? Otherwise what kind of end-stops are they or what does the circuit look like?

I would stay 4.7v is to low and not normal. 


---
**Wolfmanjm** *December 02, 2017 14:02*

4.7v is due to the diode used between the 5v regulator and the other 5v input pins. there is always a voltage drop across these diodes


---
**Chuck Comito** *December 02, 2017 14:02*

**+Jeff DeMaagd**, yes the 1.2vdc is while connected to the controller and I'm certain the wiring is right. The regulator is not hot and should not be overloaded. I don't have anything but the switches running off of the onboard 5vdc supply.

**+Don Kleinschnitz** these are not the stock switches. They are Omron EE-SX672's. The schematic looks like this. I've tried every possible option in the config with no joy. Notice the jumper from pin 1 to pin 2 to invert the signal from NO to NC.

I should also mention that the 1.2vcd when the switch is activated only is apparent when connected to the SVG on the smoothie. If I connect it to an external power supply and measure the voltage it drops to .003vdc which is what I would expect. 

![missing image](https://lh3.googleusercontent.com/niURyBA1cgW8KZnJ5UllRNis7KFPMqBTSATW0-424M2a0LjwAxysSIcHaaA54eha53-IazIGS0sYolwiE-_h_4dItjT92wKG0NE=s0)


---
**Don Kleinschnitz Jr.** *December 02, 2017 16:56*

**+Chuck Comito**​ to help I would need to know:

1.) Exactly how the signal and power to this device is connected to your smoothie

2.) What smoothie you are using.

3.) Is the sensor part number exactly EE-SX672 with no letters? i.e. Its a standard NPN model. ?

4.) Is the 4.7v you mention measured at the sensor?

5.) More info as to what machine this is in and what problems this is causing.

6.) What is SVG?  "....connected to the SVG on the smoothie...."

7.) When you are connected on the bench to an external supply how are you connecting the "OUT" pin? i.e. what is the load?



Note: the voltage specs for that sensor say 5-24vdc supply. I don't know how sensitive it is to the .3v drop?

If you have a variable supply you could lower the supply voltage (or add a diode) on the bench to 4.7vdc and see is it still operates correctly.


---
**Jeff DeMaagd** *December 02, 2017 20:39*

Oh I didn't realize it was a smoothie. There is a protection diode after the regulator on the board and that reduces the effective output voltage available. I don't know about the 1.2V level output.


---
**Chuck Comito** *December 03, 2017 23:19*

**+Don Kleinschnitz**​ I posted answers below your questions. Thank you!

1.) Exactly how the signal and power to this device is connected to your smoothie

I will post my schematic 

2.) What smoothie you are using.

5xc

3.) Is the sensor part number exactly EE-SX672 with no letters? i.e. Its a standard NPN model. ? Yes, exactly that.

4.) Is the 4.7v you mention measured at the sensor? Yes and at the supply pin and  at the diode

5.) More info as to what machine this is in and what problems this is causing. 

Completely custom k40 laser

6.) What is SVG?  "....connected to the SVG on the smoothie...."

Svg is signal, ground and voltage supply pins for the endstops.

7.) When you are connected on the bench to an external supply how are you connecting the "OUT" pin? i.e. what is the load?

Just used a 1/4 watt resistor. I don't remember the value. 



Note: the voltage specs for that sensor say 5-24vdc supply. I don't know how sensitive it is to the .3v drop?

If you have a variable supply you could lower the supply voltage (or add a diode) on the bench to 4.7vdc and see is it still operates correctly.

I will try this. 


---
**Chuck Comito** *December 04, 2017 01:16*

**+Don Kleinschnitz**​, this is pretty crude but it might help illustrate my setup. 

![missing image](https://lh3.googleusercontent.com/6PWFCYWMtjUwDT9YXyIQMZkS8-Yud8BwzSD62kYu6PyePuCkUL2UeOyEutagcWEBNy1iVbJpuBIRExIdFMeRg1TFmiZ31FmzFGU=s0)


---
**Chuck Comito** *December 05, 2017 01:00*

So instead of using the limit switch headers, I used open GPIO pins and assigned them accordingly in the firmware. It seems to work as expected now with the exception of false triggering. I think I might have some noise present near my wiring. It will be hard to fix. Looks like I might have to give up and go with mechanical switches that actually work! I think the reason I can't get the switches to work on the switch headers is because of the built in pull-up. I think it makes a voltage divider and I'm not able to get it ground properly (terminology is probably poor in that last statement). 


---
**Jeff DeMaagd** *December 05, 2017 04:08*

I’m pretty sure the endstop pull-ups can be turned off in the firmware config.


---
**Wolfmanjm** *December 05, 2017 10:46*

**+Jeff DeMaagd** They are hardware pullups on the endstops pins not internal




---
**Don Kleinschnitz Jr.** *December 05, 2017 14:24*

**+Chuck Comito** 



Sorry for the long and complex post..... I felt solving this may be worth getting an  optical end-stop alternative for the community :)!



I took a look at this and I don't see an obvious reason why this would not work using the normal end-stop connections and your optical sensor. I created and attached a schematic so that we can stay in sync, Please verify its correct with your setup and wiring.



<b>Voltage Divider:</b>

I don't think the internal smoothie pull-ups create a voltage divider. See schematic. 

When the sensor is interrupted it should pull the signal (point A in schematic) fully to ground. If its not then something is strange with the sensor. If its not ground then the output stage of the sensor is not fully turned on. 

Some possibilities are:

A.) the light transmitted to the sensors receiver is not bright enough [due to low voltage or transparent interpose-r] to fully turn on the output stage. This is not likely since you had is working on the bench. Are the interpose'r materials the same in the machine and the bench test? I don't know how you mounted these sensors?

B.) the output stage is not really a switch and the pull-up resistor value matters.

C.) the sensor is sensitive to supply voltages below the spec value of 5V.



<b>Noise when GPIO connected:</b>

The smoothie end-stop inputs have noise filters on them to prevent noise from getting to the processor. Attaching long wires to processor inputs can be problematic especially in electrically noise environments. Without careful wiring and additional filters using the raw GPIO pins may provide a world of intermittent problems. Noise on end-stops in the middle of jobs would create difficult to identify problems. That said the use of twisted pair or shielded wire may improve your situation.

I am convinced that these sensors will work and I would not advise direct connection to GPIO pins. 



<b>Diagnosis</b>

<i>This assumes that I got the attached wiring and circuit right.</i>



The symptoms when applied to the circuit suggest that the output stage of the sensor is not fully turned on and providing a ground. 



Since we do not really know what is inside the sensor (the specs are sketchy)



<b>and;</b> 

assuming the sensor has no optical problems (you are blocking it with a solid material which is not transparent at its wavelength)



<b>therefore;</b>

I am concluding there is something about the sensor output that having a 1000 ohm resistor pull-up is causing.



<b>and/or;</b>

The sensor is "sensitive" to lower than 5V ie: 4.7VDC



<b>Things to try</b>

I would do some bench testing using the circuit in the attached schematic as a model. Our purpose is to verify sensor operation in a circuit just like when its connected to the smoothie.



<b>1.)</b> Connect the sensor on the bench the same way the schematic shows and use the same value (1000ohms) for the pull-up and 5V for the supply. You can add the filter (5K & .1 uf) but I do not expect this to make any difference). Verify and record the voltage at point A when the sensor is interrupted and not.



<b>2.)</b> Add a diode after the 5V PS but before the 5V pin on the sensor (in series) pointed the correct direction :)! Verify the voltage at the sensor is close to 4.7 V. [a 1N4001 may provide .3-1V drop]. Verify and record the voltage at point A when the sensor is interrupted and not.



<b>3.)</b> If either of these fail to provide ground at the sensors "output" when interrupted try 2.) again but with a 100 ohm installed like the "add a resistor" tab in the schematic shows. This will sink about 40ma of current through the output stage.



BTW; I feel confident that we can find a less expensive and simpler alternative to this sensor with adequate design data :), such as this $4 part.

[http://www.ttelectronics.com/sites/default/files/download-files/OPB460-470-480-490.pdf](http://www.ttelectronics.com/sites/default/files/download-files/OPB460-470-480-490.pdf)





[digikey.com - EE-SX672 interface to Smoothie](https://www.digikey.com/schemeit/project/ee-sx672-interface-to-smoothie-4OFNLDG303D0/)




---
**Chuck Comito** *December 06, 2017 03:05*

Wow **+Don Kleinschnitz**​, that's a lot of info. I will set this up on the bench and test and report back. Hopefully I can do all of this tomorrow. I tried all night tonight and thought I had it figured out but in the end I was back where I started.. uggg. I haven't looked at the schematic yet but I'm excited to dig into it!


---
**Chuck Comito** *December 13, 2017 01:43*

So it looks like the problem is solved (sort of).  The Y axis randomly doesn't work but what that really means is that during the homing cycle it just doesn't move the Y. That said the solution was to use the "-" switch to disable the internal pull ups and to use an external power supply. The voltage was too low when source solely from the smoothie 5v out to work with my switches. As you suspected **+Don Kleinschnitz**​ the switches didn't like being a tad under 5v.. 


---
**Don Kleinschnitz Jr.** *December 13, 2017 12:28*

**+Chuck Comito** thats annoying, however the specs do say 5VDC. 



The low 5VDC from the smoothie is something to remember when Connecting I/O.

If it was me I would still use a more common optical switch :).


---
**Chuck Comito** *December 15, 2017 01:16*

**+Don Kleinschnitz**, what is the max input voltage I could potentially set the external power supply to? Right now it is perfect at 5.0. Is 5.5 ok or should it be as low as possible while still working?


---
**Don Kleinschnitz Jr.** *December 15, 2017 11:26*

**+Chuck Comito** if you mean the supply to the sensor 5.5VDC is ok. Why do you ask?


---
**Chuck Comito** *December 15, 2017 12:40*

I ask because the Y is sometimes intermittent and outbid curiosity. 


---
**Don Kleinschnitz Jr.** *December 15, 2017 13:43*

**+Chuck Comito** by intermittent you mean ...?


---
**Chuck Comito** *December 15, 2017 23:36*

Hi **+Don Kleinschnitz**, by intermittent I mean that "on occasion" the homing protocol will home the x and z and skip the y. But if I try again it will miraculously work..  I have another mystery for you as well. I wanted to private message you this but I couldn't figure out how to do it. So here it goes.. I built an electronics enclosure that houses all the power supplies, smoothie, drivers, etc. It's a pretty nice panel mount enclosure with a nice locking door (made of steel). The issue I see with it is when the door is closed, firing the laser will disconnect laserweb from the smoothie. The only way to reconnect is to reset the smoothie. This only happens when the laser is firing AND when the door is closed. If it is open, everything works fine. I don't believe this to be a smoothie issue at all but instead some sort of interference issue. Could the laser firing be emitting enough noise to cause this behavior and it is somehow "bouncing" this noise off the door?? It seems silly but I'm not sure what else it could be. I thought with your electronics background you might be able to point me in the right direction on this. I thought some type of Faraday cage around the laser power supply might be in order?


---
**Don Kleinschnitz Jr.** *December 19, 2017 05:02*

**+Chuck Comito** you can PM by simply:



[plus.google.com - ##K40PersonalMessage Personal messaging someone ... After following someone y...](https://plus.google.com/+DonKleinschnitz/posts/QnCFS2Poeqo)



I think I will need some pictures of what you are talking about :).


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/8mVeKf9q6Jc) &mdash; content and formatting may not be reliable*
