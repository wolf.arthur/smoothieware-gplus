---
layout: post
title: "Hi, since im getting slowly lost in setting up my laser engraver build, hopefully somebody here has gone through this an is willing to help ;) Im upgrading a A3 banggood laser to smoothie and a stronger 5.5w diode"
date: December 03, 2017 16:58
category: "General discussion"
author: "Christian te Kock"
---
Hi, 



since im getting slowly lost in setting up my laser engraver build, hopefully somebody here has gone through this an is willing to help ;)

I´m upgrading a A3 banggood laser to smoothie and a stronger 5.5w diode.

Everything went fine so far, except the laser diode setup.



My diode driver has no pwm input but a 1 wire TTL cable.

I checked with an lab psu. It switches on to full power at about 4v. Below that its off.

Since there seems to be a lot of misconceptions about ttl and pwm and after days of research im still not sure:



Is it possible to drive this kind of driver with smoothie? and adjust power?



as of instructions i found i did the following:



1. Assigned a pwm pin via the config: 

laser_module_enable : true

laser_module_pwm_pin: 3.25



2. Checked with a multimeter:

Pin goes high tfrom 0 to about 3.2v when "firing" the laser with cncjs laser section.



3. Wiring 1:(as of directions i found )

smoothie: pwm pin to laser: ttl-wire

smmothie: grnd pin to laser ground



This gave me a always on 3.3v signal



4. Wiring 2:

smoothie: pwm pin > laser: ttl-wire



Voltage regulation via "test laser" command works again 0-3.2v



5. Triggervoltage 5v:

The common open drain concept directions i found needs the pwm pin and a 5v pin to work.

since im only using one wire for ttl i didn´t know how to do this.



Levelshifter: i acquired a levelshifter, wired the smoothie:pwm pin (3.25) to low-input and the laser:ttl to high. powering the 3 and 5v inputs with smoothie pins ( strange enough 3,2v pins were hard to find. the most were about 2,3v ?!? )

Testing: The multimeter "approximation" now shows values between 0-4,7v.



6. Test drive: and now the bad...



The laser "on" has a delay. i.e. Circles are randomly "not closed"

 Laser switches on ( mostly, but randomly not ) when jogging! testet in cncjs and pronterface

Power adjustment works in cncjs laser test module ( fire nnn ) from 40 -100%, whilst the shift command ( S0.2 ) in cutting makes no difference...



So still after days of tinkering:



Is it basically possible to drive a "real" digital ttl diode driver with smoothie? 

How should the laser config/wiring look like ( to get it ttl pulsing )?



thanks in advance for your help!



Have a nice evening!

I won´t ;))))





cheers



ch.







**"Christian te Kock"**

---
---
**Don Kleinschnitz Jr.** *December 03, 2017 22:20*

This post from my blog may help you get unstuck. It provides more detail behind how to drive a K40 power supply but theoretically the connection from the Smoothie to the laser diode driver can be similar.



In most cases you can drive laser power supplies and diodes from one of the PWM capable Mosfet transistors.



You can drive using a pulled up or an open drain configuration. It depends on what the driver interface for you laser diode looks like.



If it is ttl a pullup at the laser end to 5V should work.

If the input to the laser driver is an opto-coupler an Open Drain may work better.



Level shifters can work but are unnecessary and unless designed properly can be problematic.



1.) Can you provide more information on the electrical interface to your laser diode & driver? Some quick searches suggested there is no TTL connection.



2.)It would also be helpful to have a diagram of exactly how you are wired.



3.) Pictures of the Laser Diode electronics.





[donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)


---
**Christian te Kock** *December 06, 2017 10:20*

Hey don,  i´m running out of time so i just ordered a new driver with "real" pwm. hopefully this will solve the guesswork.

I had a look at your blog. Thats what i call a profound collection of knowledge!

Since i will have to rebuild a K40 style co2 laser machine in the near future, this will be a source of great help! 

thanks for your support!


---
*Imported from [Google+](https://plus.google.com/117733901410544866012/posts/9LJWRtL4umX) &mdash; content and formatting may not be reliable*
