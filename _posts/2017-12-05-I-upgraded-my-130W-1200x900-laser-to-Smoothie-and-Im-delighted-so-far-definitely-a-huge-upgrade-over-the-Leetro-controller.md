---
layout: post
title: "I upgraded my 130W 1200x900 laser to Smoothie, and I'm delighted so far - definitely a huge upgrade over the Leetro controller"
date: December 05, 2017 17:55
category: "Help"
author: "Rod Fitzsimmons Frey"
---
I upgraded my 130W 1200x900 laser to Smoothie, and I'm delighted so far - definitely a huge upgrade over the Leetro controller.



I'm having trouble with laser power, however, it's very confusing. I can't seem to get the laser above 30% or so using S0.x commands.



Using a terminal to connect to Smoothie, I've been cutting lines using code like:



M3 0.2

G1 X10 F500 (I'm in G91 relative mode)



When I do this I monitor the current the laser is taking - it maxes out at about 10mA at about 30% power (S0.3).  After that, increasing the S parameter has no effect.



laser_module_maximum_power is set to 1.0



FURTHER WEIRDNESS: When I use test mode, i.e.



fire 50

G1 X10 F500



I get much more power, it increases all the way to 50mA at 95% power.



Any tips?





**"Rod Fitzsimmons Frey"**

---
---
**Don Kleinschnitz Jr.** *December 05, 2017 19:04*

How is your smoothie connected to the LPS? What LPS, what power laser?


---
**Arthur Wolf** *December 05, 2017 20:44*

Hey.



Note laser power isn't linear.

But what you describe could be due to the way it's wired, have more info on that ?


---
**Wolfmanjm** *December 05, 2017 21:16*

M3 0.2 is not a legal gcode command. The S1.0 goes on the G1 line usually.


---
*Imported from [Google+](https://plus.google.com/110568191880372951039/posts/dfZzMJDJzAU) &mdash; content and formatting may not be reliable*
