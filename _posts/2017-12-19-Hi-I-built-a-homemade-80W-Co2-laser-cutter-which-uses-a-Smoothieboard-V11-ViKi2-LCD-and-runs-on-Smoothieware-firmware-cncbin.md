---
layout: post
title: "Hi, I built a homemade 80W Co2 laser cutter which uses a Smoothieboard V1.1, ViKi2 LCD, and runs on Smoothieware firmware-cnc.bin"
date: December 19, 2017 04:00
category: "General discussion"
author: "Daniel Gliebe"
---
Hi, I built a homemade 80W Co2 laser cutter which uses a Smoothieboard V1.1, ViKi2 LCD, and runs on Smoothieware firmware-cnc.bin.  I have been using Inkscape and Visicut (with Visicut extension for Inkscape) for my software workflow.  Vector cuts are amazingly easy to do and fast to send to the laser via the LAN.  Of course when I try to do any raster engraving it is slower than molassas going uphill in the winter to get the gcode from Visicut into the Smoothieboard.  I've run some tests where it took an HOUR to get in there, then once it is finally done the laser starts doing its thing reasonoably fast.

When I research this I become increasingly confused.  Can anyone help point me to a workflow that would be best for my hardware setup?  Should I give up and use grbl-LPC firmware?  Thanks for any advice!

Here's a picture in case anyone is curious.  I took it to the San Diego Maker Faire at the beginning of October where it was adorned with a pretty blue ribbon.  I just want to be able to unlock the full potential of this beast and finally get rastering to work.  :-/

![missing image](https://lh3.googleusercontent.com/-14-mdMQu0Cw/WjiO9xswjDI/AAAAAAAAnOQ/6o4a-cmonOUnFDoJelC4y9xomen5oIxvgCJoC/s0/img_20170909_205044.jpg)



**"Daniel Gliebe"**

---
---
**Joe Alexander** *December 19, 2017 07:36*

nice build! are you planning on sharing the build info with all of us curious fellows? :P


---
**Arthur Wolf** *December 19, 2017 10:50*

have you tried the fast stream script provided with smoothie to stream a gcode file, see if that helps ?




---
**Samer Najia** *December 19, 2017 12:56*

I second the request to share the build info.  Santa wants to give me a K40 but I could convince him to buy the laser tube instead :-)


---
**Daniel Gliebe** *December 20, 2017 01:46*

**+Arthur Wolf** I will research the fast stream script idea and see if I can make it work and report back.


---
**Daniel Gliebe** *December 20, 2017 01:51*

**+Joe Alexander** **+Samer Najia** I tossed together a quick Wordpress page about it for Maker Fair goers to visit if they wanted to see more pictures, read my FAQ, and see the detailed parts list with all pricing and links included.  You can find it here. [https://lasermandan.wordpress.com](https://lasermandan.wordpress.com)

I didn't want to go the ever popular K40 route because they are really small and everyone who upgrades their tubes end up with a K40 with a massive tube tumor hanging off the side.   

[lasermandan.wordpress.com - Dan's Homemade 80W Laser Cutter](https://lasermandan.wordpress.com/)


---
**Daniel Gliebe** *December 21, 2017 03:08*

**+Arthur Wolf** Sorry, I am struggling a bit on figuring out how to use [fast-stream.py](http://fast-stream.py).  Is there a guide somewhere I am missing?  My computer is a Windows 10 laptop.  All I can seem to find is a thread that says usage is python [fast-stream.py](http://fast-stream.py) (gcode filename) /dev/ttyACM0 - q but i'm not sure what to do with that in Windows and I suspect there's some more to it than that.  



I just upgraded from upstreamedge-b6153da to edge-608b066 but that didn't seen to make any improvement.

I ran some baseline tests using a very small raster drawing:

- Visicut via "HTTP Upload Method":1min 46sec from clicking Execute to Laser begins running job which then runs as expected.

- Visicut via "IP Upload Method": Starts Instantly but job runs VERY VERY slow and stuttery. Probably would take hours to finish if I let it instead of a couple minutes.

- Visicut save local gcode file: Saves Instantly

- Windows file copy via USB of 1.2MB gcode file: 14sec (little slow for a file that size over USB but not world ending)

- Run gcode file from LCD: Starts Instantly, Laser job runs as expected.



So, basically the Smoothieboard can run the laser fine from SD card of course, the problem is being able to conveniently get to job to the laser without having to do a lot of intermediate steps or a lot of waiting.  This is especially hard on raster jobs of course due to the trial and error nature of getting the raster settings right. 


---
**Daniel Gliebe** *December 21, 2017 04:25*

Ok, I pushed through some stuff and figured out I needed to install Python 2.7, not Python 3.  (update system path of course). Then I had to find and install pyserial-2.7.win32.exe because it is not included in Python.  Now I can run the script in this format.  

python [fast-stream.py](http://fast-stream.py) Filename.gcode COM4



Results: When using fast-stream.py, yes it streams the exact same gcode I was using in my above tests immediately and without any slowness at all.  Basically perfect.



It's not an ideal workflow because I can't just hit one button from Visicut and it just runs a raster.  I'd have to save the gcode file, open a command line, run a script, etc.  Also It means I can't use the network, stuck tied to USB.



Is this how "everyone" is doing it?  I'm open for other programs, workflows, I have perhaps not considered.  Also thanks for the tips so far, they are greatly appreciated! 


---
**Anthony Bolgar** *December 21, 2017 13:36*

There is a new software package coming out on January 1st called Lightburn. It works great with smoothieware. Look for it on Facebook, they have a FB page for it, as well as a website at [lightburn.com](http://lightburn.com).




---
**Daniel Gliebe** *December 21, 2017 19:55*

**+Anthony Bolgar** That looks interesting.  Looks like the website is actually [lightburnsoftware.com](http://lightburnsoftware.com).  I will have to test the demo once available in Jan.  Normally I would try to stay on the Free/Opensource side but looks like the Gcode version is only $30 which is not bad.


---
**Anthony Bolgar** *December 21, 2017 20:23*

Sorry about the link, glad you still found it  **+Daniel Gliebe** The software is a great deal at $30, I have been beta testing it and I find it so easy to use. It is very intuitive, and crazy fast the developer used to design games so he makes everything very fast and well designed.


---
**Jason Dorie** *December 31, 2017 04:35*

The LightBurn trial is live now - They released a few days early so people could play over the weekend.


---
**Daniel Gliebe** *December 31, 2017 04:39*

**+Jason Dorie** thanks for the heads up. I'll give it try!


---
*Imported from [Google+](https://plus.google.com/+DanielGliebe/posts/4pXrqiqXCY5) &mdash; content and formatting may not be reliable*
