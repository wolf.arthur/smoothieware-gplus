---
layout: post
title: "Smoothie - Prometheus extrusion problem . I added a Prometheus dual extrusion system to a homebuilt Cartesian printer"
date: December 21, 2017 09:28
category: "General discussion"
author: "Naren N"
---
Smoothie  - Prometheus  extrusion problem .

I added a Prometheus dual extrusion system to a homebuilt Cartesian printer.

The extruders ( either one )  work perfectly when using a single filament process. When using a Dual filament process from Simplify 3D ( 4.0 ) the extruder motor does not rotate and there is no filament feed. It seems like the motor is stalled and makes strange noises. Almost like it is missing steps.

Anyone with any tips on where to look ?





**"Naren N"**

---
---
**Arthur Wolf** *December 21, 2017 10:44*

S3D is possibly trying to move it too fast ? Maybe try reducing both acceleration and max speed for both extrders ?


---
**Naren N** *December 22, 2017 12:41*

Problem solved. The Prometheus( DisTech Automation ) provided starting script had a missing M83 code after a G90 E0 line.

Adding that line to the Gcode file manually resulted in printing immediately and I now added this line to the Simplify 3D process settings.

Script quoted below - The addition is on the last Gcode line.

; START SCRIPT START

M104 S[extruder0_temperature]  T0

M140 S[bed0_temperature]

M190 S[bed0_temperature]

M109 S[extruder0_temperature]  T0

G28 ; Home extruder

G90 ; Absolute positioning

G92 E0 ; Reset extruder position

M83

; START SCRIPT END 


---
*Imported from [Google+](https://plus.google.com/115851432288218842065/posts/EfVUwU4px8e) &mdash; content and formatting may not be reliable*
