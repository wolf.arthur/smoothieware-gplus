---
layout: post
title: "i have found some threads on this but no resolution"
date: December 27, 2017 02:46
category: "General discussion"
author: "Tech Bravo (Tech BravoTN)"
---
i have found some threads on this but no resolution. i have a smoothie based laser controller (C3D Mini) and on Windows 10 its fine. Win 7 installations need the usb drivers which i got. i tried the installer and the manual install to no avail. i still get this: "This device is not configured correctly. (Code 1)A service installation section in this INF is invalid. To find a driver for this device, click Update Driver.". Any ideas?





**"Tech Bravo (Tech BravoTN)"**

---
---
**Griffin Paquette** *December 27, 2017 03:31*

Hi **+Tech Bravo** sorry you’re having this issue. There is actually a community dedicated to Cohesion3D products that you can find here:





This is probably the best place to find information specific to the Mini and Remix. [Cohesion3D](https://plus.google.com/communities/116261877707124667493?iem=1)


---
**Tech Bravo (Tech BravoTN)** *December 27, 2017 03:41*

thanks. i did post there. i found a bunch of people with this issue that were not c3d specific and since the driver is smoothie (uberclock) i thought this may be my best chance. thanks for the insight. i will continue my quest with the c3d group. :) 


---
**Ray Kholodovsky (Cohesion3D)** *December 27, 2017 04:51*

Have you tried a manual install of the driver? Uninstall first then follow the Smoothie drivers instructions for the manual install. 


---
**Eric Lien** *December 27, 2017 09:29*

Make sure to use the 1.0 driver, and do the manual Install. [http://smoothieware.org/windows-drivers](http://smoothieware.org/windows-drivers)



I also know there are a few pc chipsets that for some reason don't wanna work with smoothie driver. Not sure why.


---
**Tech Bravo (Tech BravoTN)** *December 27, 2017 13:18*

Thanks guys. Yep. Did all of the above. I will redo just to confirm. Otherwise i will just get another pc  :) 


---
**Ray Kholodovsky (Cohesion3D)** *December 27, 2017 13:34*

Also a cable question. The stock USB cable from the k40 can be problematic. Try a different one. 


---
**Tech Bravo (Tech BravoTN)** *December 27, 2017 13:40*

Roger that. Thanks


---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/fzFMAqJYFrX) &mdash; content and formatting may not be reliable*
