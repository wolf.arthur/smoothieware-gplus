---
layout: post
title: "Hello people, My name is Philippe Guilini"
date: January 03, 2018 10:06
category: "General discussion"
author: "Philippe Guilini"
---
Hello people,

My name is Philippe Guilini. I'm a member of the Eco Fab Lab Brugge, in Brugge, Belgium.

We are reviving a broken down Full Spectrum Hobby Laser 5th generation.

We are now making our first little steps and succeeded in figuring out what cables come from what part and yesterday we succeeded in controlling the stepper motors with the board and the lcd panel.

Glad to have found this community and hoping to be able to participate.

Kind regards,

Philippe.







**"Philippe Guilini"**

---
---
**Sjoerd Aarts** *January 04, 2018 14:03*

Welcome!


---
**Kelly Burns** *January 04, 2018 17:27*

I can't find it, but earlier last year, I came across another person doing a similar conversion of FSL older generation machine.  Seems like a solid hardware for Smoothie board upgrade. Google around.  It will give you a jump start.  Of course, you did the hard part ;)  GOOD LUCK




---
**Philippe Guilini** *January 05, 2018 14:27*

Thanks for accepting us !


---
*Imported from [Google+](https://plus.google.com/109415938778659747209/posts/jVwTVgPwYRp) &mdash; content and formatting may not be reliable*
