---
layout: post
title: "As mentioned before, we are reviving our FSL laser with a smoothieboard ( rev 1.0b )"
date: January 05, 2018 14:22
category: "General discussion"
author: "Philippe Guilini"
---
As mentioned before, we are reviving our FSL laser with a smoothieboard ( rev 1.0b ). Up untill now, we succeeded in controlling the stepper motors and the endpoint switches. Our laser has a separate guiding led that shows you where the focus point is. Our aim is to switch this led on whenever the laser starts a job. With a job, we also mean that the laser does not fire but simply goes around the contours  of the item to laser. Job finished should dim the led. Until now, we had no success in finding examples on how to do this. Anyone has good ideas please ? 





**"Philippe Guilini"**

---
---
**Douglas Pearless** *January 05, 2018 21:18*

If I understand your issue, I think you should start with looking at the Switch Module to control the LED and turn that on and off via the GCode you send to it.


---
**Philippe Guilini** *January 06, 2018 10:41*

**+Douglas Pearless** 

Hey Douglas, I'm not sure what you mean by "Switch <module". We found that on the Smoothieboard, there are four pins that can be used to switch and generate PWM, but we don't know how to use them.

Thanks for thinking along with us.

Philippe.




---
**Douglas Pearless** *January 06, 2018 11:51*

Do you want to simply turn on and off the LED or do you want to use PWM to control the LED?


---
**Philippe Guilini** *January 06, 2018 19:02*

Like I said in the opening post : "Our aim is to switch this led on whenever the laser starts a job. With a job, we also mean that the laser does not fire but simply goes around the contours of the item to laser. Job finished should dim the led. "

So, even if the laser is asked to run the outer contour of the object, without efectively lasering it, the led should turn on and dim afterwards.


---
**Douglas Pearless** *January 07, 2018 01:58*

Have a look at [smoothieware.org - switch [Smoothieware]](http://smoothieware.org/switch) and set up the switch to control your LED on whatever PIN you want to use.  The FAN example is a close to what you want and if you can use a HWPWM PIN, all the better.  Then in your G-Code file, use the M106 with a S parameter to control the PWM and hence the brightness of the LED from full ON to OFF (and the M107 to turn it off as well).


---
**Philippe Guilini** *January 07, 2018 10:21*

**+Douglas Pearless** 

All Right. Thanks for thinking along with us. Tuesday evening is the next gathering, so we'll try your solution then. Fingers crossed ! 

If you like to see how far we are for the moment, check our facebook page.We are on : [https://www.facebook.com/ecofablab/](https://www.facebook.com/ecofablab/).

You'll be able to see a few movies and photo's. 

 ( PS : On the opening photo at the top, I'm the one with the brown jacket to the left of the orange car.)

[facebook.com - Eco-fab-lab Brugge](https://www.facebook.com/ecofablab/)


---
**Douglas Pearless** *January 07, 2018 10:25*

Good luck!!


---
**Philippe Guilini** *January 10, 2018 18:18*

We opted for a KISS solution and installed a separate switch to ignite the guidance led. 




---
*Imported from [Google+](https://plus.google.com/109415938778659747209/posts/SKx15QjRkyx) &mdash; content and formatting may not be reliable*
