---
layout: post
title: "Hi guys, I'm a new user of Smoothieboard and Smoothieware, I come from Arduino Uno and grbl, the performance of Smoothie is just amazing!"
date: January 07, 2018 01:31
category: "Help"
author: "Octavian Bujor"
---
Hi guys, I'm a new user of Smoothieboard and Smoothieware, I come from Arduino Uno and grbl, the performance of Smoothie is just amazing!



I'm using it with my laser diode machine, usually my gcode files are pretty big (1-4 million lines) and the size can reach 80MB and more. I tried to upload one of those files through the web interface and it took a lot of time more than an hour. 



So my question is, what is a good/fast method to upload big files in the board? Can current performance be improved? I don't want to stream gcode, just upload and play from the sd card.



Thanks!





**"Octavian Bujor"**

---
---
**Douglas Pearless** *January 07, 2018 01:53*

The USB interface is the fastest of all the interfaces on Smoothie.


---
**Joe Alexander** *January 07, 2018 04:26*

alternately you could pull the sd card, save the file on your computer, then put it back


---
**Sébastien Plante** *January 07, 2018 04:45*

Did you try another browser? 


---
**Octavian Bujor** *January 07, 2018 16:39*

**+Douglas Pearless** Do you mean mounting sd card through usb or streaming? I tried to mound sd card using usb and it was quite slow as well, not like browser but slow. In your opinion can the OS be a problem? Maybe usb drivers? I'm using Ubuntu.


---
**Octavian Bujor** *January 07, 2018 16:42*

**+Joe Alexander** Yes, I'm using this method but is not very handy, currently this is the fastest method I found.


---
**Octavian Bujor** *January 07, 2018 16:43*

**+Sébastien Plante** No, I didn't, I used Chrome, I will try Firefox.


---
**Douglas Pearless** *January 07, 2018 20:27*

The speed of the interface to the SD Card will be a limiting factor; you can remove the SD Card, connect it to your computer and copy the file (gcode, etc) to the SD card, eject it and place back into Smoothie, power Smoothie on and use with the console to "play" the file, or if you have an LCD interface on your Smoothie, you can select the file form that and "play" the file.


---
**Octavian Bujor** *January 07, 2018 21:06*

**+Douglas Pearless** As I said before, I'm already doing this, but is not very handy to remove every time the SD. I don't know if the speed of the interface is a limiting factor because if I remove SD and insert in the PC to copy files it takes lets say a couple of minutes but if I connect though usb and mount SD to copy the same files it takes much more,  maybe 10 times slower.


---
**Douglas Pearless** *January 07, 2018 21:10*

**+Octavian Bujor** Ah, sorry I was not clear in my response, the SPI interface on the LPC1769 is very slow compared to inserting the card into your PC, so, unfortunately HOW you connect to Smoothie will probably not make any real difference as that is not the limiting factor, the SPI bus is.


---
**Octavian Bujor** *January 07, 2018 21:16*

**+Douglas Pearless** Ah, ok, now is clear! So this means that the speed cannot be improved with software updates, right?


---
**Douglas Pearless** *January 07, 2018 21:16*

**+Octavian Bujor** Yep, unfortunately.


---
*Imported from [Google+](https://plus.google.com/105658968640955572858/posts/XxDXVPvRs3z) &mdash; content and formatting may not be reliable*
