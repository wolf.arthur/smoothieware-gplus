---
layout: post
title: "Hi All, I've been working with the cohesion3d and wanted to use the smoothieware on a 5-axis machine"
date: January 09, 2018 05:26
category: "General discussion"
author: "Todd Mitchell"
---
Hi All,



I've been working with the cohesion3d and wanted to use the smoothieware on a 5-axis machine.  I've had no issues jogging all 5 axis simply yet wanted to confirm if smootheware vlatest supports 5-axis simultaneous movement (i.e. multi-axis motion).



I use fusion3d to generate the toolpaths and during the post process, the script provided throws an exception "multi-axis motion is not supported."



Is this due to the firmware not supporting it or the fact the script author has not (yet?) provided support. FWIW - the grbl post-processor throws the same exception.



<s>--</s>



Update: From the looks of the supported g-code list in smoothieware's docs and given the fact that the EMC, mach3, pocketnc, and a few other (I forgot which) use the G93,G94 commands to support inverse time when milling simultaneous axes, I will assume smoothieware cannot (yet?) support it.  Therefore, I'll move forward using mach3 for the short term until I can assess what it takes th update + PR the smoothie firmware to support these, and potentially other, commands.





**"Todd Mitchell"**

---
---
**Antonio Hernández** *January 09, 2018 05:58*

I'm curious about your question. I would like to know which design did you use to build 4th and 5th axis. Is it possible ?. Your question is very important, because, I don't know if the software (fusion3d) could show the multiple machine configurations (4th and 5th axis, together) and if it's possible to choose an option that could fit your machine design and obviously, the created gcode program could run and work as expected. It's just an idea about your problem... I'm only curious about your post. By the other hand, (as a general question for the community), which could be the most important comments about the building (or use) of 5th axis ?. I have seen something about forward and inverse kinematics inside "help commands" in smoothieware, and I don't know if these commands are related with the functions of 4th and 5th axis, mainly the last one. Any comment is appreciated. Thanks !.


---
**Todd Mitchell** *January 09, 2018 06:10*

For the 4th and 5th, I started with a prefab one from china to simplified matters with a mind toward building a more rigid one after I really understood what 5-axis machine is about.  Here's the one i used ontop of a xy-table configuration i rigged up from old biolab gear.



[automationtechnologiesinc.com - Stepper Motor &#x7c; Stepper Motor Driver &#x7c; CNC Router &#x7c; Laser Machine &#x7c; 3D Printers For Sale](https://www.automationtechnologiesinc.com/products-page/cnc-router/cnc-router-rotational-axis-the-4th-5th-axis/)



In retrospect, i should have started with this one (below) as it seems to be more of the norm and requires less z-height:



[https://www.ebay.com/itm/CNC-Router-Rotary-Table-Rotational-Axis-4th-5th-Axis-A-B-Axis-100MM-Chuck-3-Jaw/161679996102?hash=item25a4e0f4c6:g:0MAAAOSw-KFXeiG5](https://www.ebay.com/itm/CNC-Router-Rotary-Table-Rotational-Axis-4th-5th-Axis-A-B-Axis-100MM-Chuck-3-Jaw/161679996102?hash=item25a4e0f4c6:g:0MAAAOSw-KFXeiG5)



The issue is that it cost more and was more difficult to acquire (at the time)






---
**Antonio Hernández** *January 09, 2018 06:26*

Ok !!!, By my side, I'm trying to build both axis, together, using the most common configuration for that purpose (apparently, the most common). It's something similar shown in your images, but, the same doubt that you expose, it's the same doubt I was wondering some weeks ago... I read in some place there are multiple configurations for 4th and 5th axis (together). I would like to think it's very important the machine's design and how it's made the design of 4th and 5th axis. I would like to think that the generated gcode is related with the position of both axis over the machine, and obviously, the last one, if smoothieware could fit the function of 5th axis using the most common configuration (again, the mechanical configuration shown in your images...)


---
**Antonio Hernández** *January 10, 2018 20:25*

Somebody ?. **+Arthur Wolf** ?. Help ! . Any comment... 


---
**Todd Mitchell** *January 10, 2018 21:16*

**+Antonio Hernández** did you see my update to the post?  I'm no expert  and only basing my switch to a mach3 bob for my 5axis based on the fact multiple 5-axis post-processors in fusion3d use G93.  My understanding of that command made sense that you would need it for 5 axis.



I <3 smoothieware so I will use it for my 3 axis and 4 axis machines.


---
**Antonio Hernández** *January 10, 2018 21:37*

Yes, of course, I saw it. Also, I'm an enthusiast about this. I'll try to work my machine using 5 axis, but, at the moment, I don't know more details about this. I saw some posts (smoothie's community) it's possible to use even > 6 axis (It'll be possible only changing a parameter inside smoothieboard programming code, according to some comments). By the way, for me, it'll be enough to work with 5 axis, but, I can't see something more detailed about that (maths for rotatory axis also could not be a problem, according to smoothie docs). The gallery that exists over smoothieware page, shows it's possible to work without problems with 3 axis. At the moment, another member has a post showing the 4th axis with his machine, and it worked (laser machine, in this case). It'll be great if somebody could share (anything) her/his experience about the 5th axis and how could it work and just a little bit about the experience gained using this axis... but, it appears at the moment, could not be possible.


---
**Antonio Hernández** *January 11, 2018 00:13*

**+Todd Mitchell**, I understood... (sorry), I thought your update was about a new comment 'after' the original post. I saw the update that you mentioned before... I hope we can read more news about that 5th axis (over smoothieboard) that could help us to use it (most common mechanical configuration or the mechanical configuration recommended by smoothieboard team, maybe....). By the way, I read some comments about Mach3 (I don't know anything about the others that you mentioned before) and I believe it's possible to use the 5th axis, but, "the maths" involved about the movements are different vs smoothieboard. Maybe with the new version of smoothieboard we could have more info about the function of axis > 3 planes. Hope to hear very soon about your machine. Good luck !.


---
**Todd Mitchell** *January 11, 2018 03:30*

**+Antonio Hernández** All good.  I think it's feasible to use smoothieware for 6 axis when you're doing indexed moves so still quite-quite valuable.


---
**Antonio Hernández** *January 11, 2018 07:17*

We will be waiting for news about this **+Todd Mitchell**.


---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/PqgRtwgexVi) &mdash; content and formatting may not be reliable*
