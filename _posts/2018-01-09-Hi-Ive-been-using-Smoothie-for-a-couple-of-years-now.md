---
layout: post
title: "Hi. I've been using Smoothie for a couple of years now"
date: January 09, 2018 05:37
category: "General discussion"
author: "Eric N"
---
Hi.  I've been using Smoothie for a couple of years now.  (Very impressed with it!)  ...but I just installed a Z-probe (BLtouch) and am confused by something...

Your help would be greatly appreciated...



I am running a homemade cartesian machine that I use interchangeably for both printing and milling.  Running latest edge firmware.  When I am milling plate, I want to use the z-probe to perform auto grid leveling on the plate (not the bed).  The plate is typically much smaller than the bed -- roughly centered on the bed.  I have tried several things to properly probe it, finally winding up with some success, but I suspect there should be a better way.  Here is what I have tried...



1. I manually move the probe over the front-left corner of the plate, and send G32.  The machine moves to the starting z-height -- good.  I then expect it to  probe straight down from there, but instead it moves to X0 Y0 -- off the plate, and hits my endstops -- not what I want or expect.



2. Again I manually move the probe over the front-left corner of the plate.  However, this time I first send a G92 X0 Y0, figuring that if it wants to move to the origin, I am all set.  Only, it does the same thing -- heads for my endstops at "machine" coordinates MCS X0 Y0, ignoring WCS.



3. The only way I can seem to make this work is:

Manually move the probe over the front-left corner of the plate.  Send G28.3 X0 Y0.  Then do G32.  This "seems" to work, but is it really the correct/only way to do this through g-code?



A few questions on the subject:



a. (Repeating above)  Is resetting the machine coordinate home (G28.3) the correct/best/only way to do this (grid probing a smaller plate located somewhere inside the perimeter of a larger bed)?



b. Why doesn't G92 work?  Must leveling be performed in MCS?  I don't want my MCS origin/home just inside the corner of my plate if I can help it -- it just doesn't make much sense.



c. If the plate that I am milling is variably sized, can I use g-code to inform Smoothie of the X and Y dimensions of the grid as well as the number of points to probe within?  (It's not clear, but I think G29 might have the facility for this?  Can G32 take the same arguments?)



d. The Smoothieware site says that it can not extrapolate outside of the grid.  What does it do outside of the grid?  Since I have to probe the top of the plate, I will necessarily be probing just inside the edge of the plate (by a few mm), but may want to mill all the way out to the edges, so what should I expect to happen outside the probed grid?



Basically, I'd like to be able to manually set the "origin" of the plate.  Then have Smoothie probe/auto-level the plate, then start milling it.



Answers appreciated...



Thanks!

Eric







**"Eric N"**

---
---
**Wolfmanjm** *January 09, 2018 12:15*

probing MUST happen in MCS that is just the way it must work, so of course G92 won't affect probing.

Please read the wiki on the cartesian grid, there are options to set a sub rectangle to probe which is what you are doing. (It was designed to probe a PCB). If you use the firmware-latest.bin on github you will find that it will work better when out of the probe area, it still does not extrapolate but uses the last known heights at the edge.


---
**Wolfmanjm** *January 09, 2018 12:20*

this wiki page should help you understand better..  [smoothieware.org - pcb-milling [Smoothieware]](http://smoothieware.org/pcb-milling)


---
**Eric N** *January 10, 2018 01:30*

Thanks again, Wolfman, for being so generous with your time not only in development, but answering all the forums.  Don't know how you do it.



After I posted my questions, I remembered reading about people using Smoothie to mill PCBs (my next adventure), and thought they might have similar issues.  I totally skipped over (probably 5 times) the "leveling-strategy.rectangular-grid.only_by_two_corners" parameter in the z-probe wiki and its link to PCB milling.  Your link directed me right in.  



FWIW...I understand that the leveling offsets have to be calculated and maintained in MCS, but using G92 could have worked to temporarily move the probing grid origin for "our" purposes, and back calculated to MCS.  Anyway, forget that, the way it was ultimately implemented by adding the extra parameters to G29/31/32 is definitely the "right" way.



What do you think about adding a 'Z' parameter to, optionally, set the initial_height?



(I find myself thinking that RAM usage on the microcontroller must be terribly tight.)



Best,

Eric


---
**Eric Lien** *January 10, 2018 03:33*

**+Wolfmanjm** great to see the grid compensation has edge extrapolation now. Even if it is just mirroring the last edge points. The previous limitation of not having extrapolation was the only reason I didn't use mesh level before. Mesh works well when you have a large heated bed which can have a tendency to crown/cup when heated just from the bottom. I have an offset probe, which caused me to have too small of usable probe area. The probe would miss the bed probing one side, and can't reach the other side due to travel limitations.


---
*Imported from [Google+](https://plus.google.com/112118213035401144752/posts/K9SxB2y6Qa9) &mdash; content and formatting may not be reliable*
