---
layout: post
title: "Hello everyone, We have been reading and rereading different posts in this group, but we can't find an answer to our problem"
date: January 15, 2018 17:26
category: "General discussion"
author: "Philippe Guilini"
---
Hello everyone,

We have been reading and rereading different posts in this group, but we can't find an answer to our problem.

We have a smoothieboard revision 1.0b.  Our power suply has three green blocks for the connections. Left most izs the power supply, in the midle the one for the inputs from smoothie and on the right the power out. 

The middele one is labeled (G)(P)(L)(G)(IN)(5V). 

Somehow we can't control our laser with the PWM.

Could one of you please help us along with a simple drawing of which pins from the PSU should be connected to which pins on the smoothie ?

We read the laser guide from the smoothie wiki and wired everything like that, with the same pin numbers, but no luck.



Philippe.





**"Philippe Guilini"**

---
---
**Don Kleinschnitz Jr.** *January 16, 2018 03:52*

Try out this blog: 

[donsthings.blogspot.com - Don's Things](https://donsthings.blogspot.com/)



Look at the index for relevant subjects. My board is newer but you could figure out the equivalent on the older boards looking at the schematics of both.



This post in particular:

[http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)




---
**Joe Alexander** *January 16, 2018 10:48*

short answer is that the power level is controlled by the voltage input on the (in) pin ranging from 0-5v (usually controlled from the potentiometer). Most of us instead control the laser on/off via pwm on the (L) pin by using a mosfet as "open-drain" and pulsing it to ground(connecting L-->GND activated laser, this is what the test fire button does on the standard panel). As mentioned above by **+Don Kleinschnitz** thorough documentation is on his blog, and is an excellent source of information for these machines(and the hazards thereof).


---
**Philippe Guilini** *January 17, 2018 16:33*

Gentlemen, these were the threads ( among several others) I was referring to in my opening post. 

 Paticularly the question from Mr Lopez took our attention and we read and reread it several times, probing along. But with no success. But, next week, we'll be able to use a scope. Hopefully we can find som more then.


---
**Don Kleinschnitz Jr.** *January 17, 2018 22:45*

**+Philippe Guilini** 

Can you provide pictures of the smoothie and LPS connectors/connections that you currently using.

A copy of the smoothies configuration file is also necessary....



..........

The simple version is that you need to connect the drain of an open drain MOSFET on the smoothie to the "L" pin on the LPS (rightmost connector). 

The AC to the supply must be present and the laser must have its anode and cathode connected.

The IN pin on the LPS is connected to the center leg of the power adjustment pots three terminals, with one end to 5V and the other to gnd.

The LPS must have its enable pins (ie: P) at the correct voltage according to the manuf specs. for that LPS.



The smoothie must be configured correctly for the particular mosfet you are using and the laser features you want.






---
**Joe Alexander** *January 18, 2018 04:39*

there is the rare chance that your 2.4 mosfet somehow got fried as this happened to mine. In that case I swapped to the 2.5 pin and moved one mosfet over. The pic below was the most helpful one that I found when doing my conversion, hope it helps (ironically this image is also on dons blog, read every page he hides the good stuff :P

![missing image](https://lh3.googleusercontent.com/1wPGo0cdts5nLKAPi07uamvBVfQSPRGwMoQdUnhORUvV7nbBeYr2mUX-g85970XrLFDfGG-6dXlbasL1F3DCtS9JTR5G_WZ2vrQ=s0)


---
**Philippe Guilini** *January 18, 2018 10:30*

Thank you for your reactions. It is very much appreciated to know that there are people around willing to help you ! As I said, next week we'll have a scope at our disposal ( with a man who can operate it  :-)  ).  I promise you that i'll report back to you afterwards. 

We also used Don's picture.

  Grtz,

 Philippe.




---
*Imported from [Google+](https://plus.google.com/109415938778659747209/posts/eeAJgehY27u) &mdash; content and formatting may not be reliable*
