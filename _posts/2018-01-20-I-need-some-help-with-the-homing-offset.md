---
layout: post
title: "I need some help with the homing offset"
date: January 20, 2018 15:43
category: "Help"
author: "Chuck Comito"
---
I need some help with the homing offset. I'm trying to offset from my gamma max 5mm however when it reaches the switch it just stops and doesn't offset. I think I might have something mixed up because I'm homing to max and not min. Can anyone shed some light please? Here's what I've got so far:



#gamma_min_endstop                           1.28-          

gamma_max_endstop                            1.29-           

gamma_homing_direction                       home_to_max     

gamma_min                                    0              

gamma_max                                    200         



alpha_max_travel                             1236            

beta_max_travel                              700            

gamma_max_travel                             500            



# Optional enable limit switches, actions will stop if any enabled limit switch is triggered

#alpha_limit_enable                          false            

#beta_limit_enable                           false           

#gamma_limit_enable                          false           



# Endstops home at their fast feedrate first, then once the endstop is found they home again at their slow feedrate for accuracy

alpha_fast_homing_rate_mm_s                  100              

alpha_slow_homing_rate_mm_s                  25              

beta_fast_homing_rate_mm_s                   100             

beta_slow_homing_rate_mm_s                   25             

gamma_fast_homing_rate_mm_s                  4             

gamma_slow_homing_rate_mm_s                  2             



alpha_homing_retract_mm                      5            

beta_homing_retract_mm                       5           

gamma_homing_retract_mm                      5          





**"Chuck Comito"**

---
---
**Joe Alexander** *January 20, 2018 21:34*

weird your settings seem right to me. Maybe try un-commenting the limit switch lines and try both false and true? or remove the - from gamma max endstop? doesnt hurt to try :)


---
**Chuck Comito** *January 20, 2018 22:18*

I wonder if it should be something like gamma_max_retract 5mm or something to that effect. I have a feeling it's something to do with that. 


---
**Joe Alexander** *January 20, 2018 22:45*

doubtful as it being phrased as is allows it to apply to min or max. what made you put the pull-down "-" on the endstop btw?


---
**Chuck Comito** *January 20, 2018 23:25*

I'm using active optical switches. 


---
**Joe Alexander** *January 20, 2018 23:40*

ahh makes sense now :)


---
**Douglas Pearless** *January 21, 2018 02:05*

When you are homing to max, is it your intention to invert the plane, i.e. gamma_min is really -200 and therefore gamma_max is 5?


---
**Chuck Comito** *January 23, 2018 03:41*

**+Douglas Pearless** I'm not sure what you mean by invert the plane. I home to max which puts my Laser bed almost in focus with my lens (all the way up). Offsetting 5mm would put in perfect focus to the surface of my bed. If it offset automatically then I could move the bed to whatever distance I need for my material thickness and be right on every time. I know 5mm is pretty easy to add to my material but I was hoping to automate the process so I don't have to remember to do it each time. I've already forgotten a handful of times..


---
**Joe Alexander** *January 23, 2018 10:53*

I think Doug means that is max on your setup fully raised rather than the lowest point? Most machines I believe would have 0, 0, 0 origin at the uppermost point on z and the bottom left on x/y. This should be a simple fix by inverting your gamma step pin and change it to home to min right? I could be wrong about the orientation but it makes logical sense to me at the moment :P Seeing the motor info from the setup file couldnt hurt either.


---
**Douglas Pearless** *January 23, 2018 18:40*

That was what I was as trying to say, but I managed to be less clear 😄


---
**Joe Alexander** *January 23, 2018 21:27*

I got your back Doug :P


---
**Douglas Pearless** *January 23, 2018 21:35*

:-)


---
**Chuck Comito** *January 23, 2018 23:45*

Lol! Anyway, I'll see if I can invert and home to min then. I'll Let you know if it works. 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/BzyaBaR45nK) &mdash; content and formatting may not be reliable*
