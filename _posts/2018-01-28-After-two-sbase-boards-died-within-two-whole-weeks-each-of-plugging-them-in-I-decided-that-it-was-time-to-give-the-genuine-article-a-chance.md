---
layout: post
title: "After two sbase boards died within two whole weeks each of plugging them in I decided that it was time to give the genuine article a chance"
date: January 28, 2018 13:06
category: "General discussion"
author: "Rachel Rand"
---
After two sbase boards died within two whole weeks each of plugging them in I decided that it was time to give the genuine article a chance. I was pleased to see a QC sticker - it was nice to know someone had checked it out before I received it.



I changed a bunch of my connectors over, and wired it up. Motors ran in the right direction..the M119 command registered 0 for non-triggered and 1 for triggered endstops. Homing works correctly. The hotend (heater, fan, extruder) work correctly.



However, if I go to print using repetier or cura  on windows first the machine homes (good) and then the machine tries to go lower on z (bad bad bad). I have a switch that I hit to  prevent damage, but what could be going on? The Gcode has no such outlandish command in it.



Ideas or solutions? How does directional control on Z work and homing work and this not work?





**"Rachel Rand"**

---
---
**Rachel Rand** *January 28, 2018 13:31*

I think I found a firmware bug. I had left a G29 command in place and no leveling info was set. That should not, obviously, try to move the carriage down below the stop.



I've enabled  gamma limit stop and taken out the G29. I think that's going to fix it.


---
**Rachel Rand** *January 28, 2018 14:03*

I just realized the card was supplied with firmware over a year old - really? Really? Gosh.


---
**Rachel Rand** *January 28, 2018 14:05*

And that bug is fixed in the current firmware


---
**Arthur Wolf** *January 30, 2018 17:21*

Hey. Documentation says to update to latest firmware/config when you get the board, so we don't update the factory firmware too often ( we don't really want users to rely on that happening anyway ).

Glad you got it to work.


---
**Rachel Rand** *January 30, 2018 18:46*

Tx, I'm in fairly good shape. Not too many self inflicted wounds but some :)


---
*Imported from [Google+](https://plus.google.com/+RachelDeeRand/posts/UmfD8p4Q2oU) &mdash; content and formatting may not be reliable*
