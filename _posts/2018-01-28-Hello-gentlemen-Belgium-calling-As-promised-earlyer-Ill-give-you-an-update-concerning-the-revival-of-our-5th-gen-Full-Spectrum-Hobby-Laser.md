---
layout: post
title: "Hello gentlemen, Belgium calling ! As promised earlyer, I'll give you an update concerning the revival of our 5th gen Full Spectrum Hobby Laser"
date: January 28, 2018 16:35
category: "General discussion"
author: "Philippe Guilini"
---
Hello gentlemen,  Belgium calling !

As promised earlyer, I'll give you an update concerning the revival of our 5th gen Full Spectrum Hobby Laser.



If you read back my earlier posts, you can see that we couldn't use PWM from smoothieboard with our laser. We could only regulate the power with a potentiometer that we installed following your schema's. The help that you kindly provided us with, didn't work at all in our case. So we disconected everything and restarted from scratch. 



First thing we did was having a close look at our power suply. We opened the lid and found the markings on the pin-out. On the internet we found the manufacturer and a description of the pins ! 



Then we decided to wire everything as was described in the user manual from the PSU manufacturer and as described in the WIKI from the smoothieboard.  We also decided to omit  the potentiometer.



For testing purposes we put a multimeter in the Low Voltage returnline from the lasertube.



We installed LightBurn and ............BINGO ! Everything worked as it should ! We have full PWM control over our laser. We could see that on full power the tube took 20 mA max. So we are safe as not to blow up our tube.



So....in our case we couldn't use your directions to connect the L from the PSU to the PWM-pin from the smoothieboard and the Ground to the Ground of the smoothieboard. It simply didn't work at all in our case.



Below here, I'll put a drawing of our wiring, a copy of the config file and the webpage with the description of our PSU.



I do hope that this can prove helpful for other users of this wonderfull forum.



Kind regards from, 

Philippe.







![missing image](https://lh3.googleusercontent.com/-P_ow2-ZIO8I/Wm376I0xQnI/AAAAAAAAALQ/eBImqkq2214nyQNod7HaZeIxgrDSwuMuACJoC/s0/scan.jpeg)



**"Philippe Guilini"**

---
---
**Philippe Guilini** *January 28, 2018 16:40*

Sorry, but apparently the webpage was omitted in the mean time ?  I get a 404 error if I want to go to the page ?

" [www.cloudraylaser.com/collections/co2-power-supply/products/35-50w-co2-laser-power-supply-myjg-40og](http://www.cloudraylaser.com/collections/co2-power-supply/products/35-50w-co2-laser-power-supply-myjg-40og)" 


---
**Philippe Guilini** *January 28, 2018 16:40*

" [www.cloudraylaser.com/collections/co2-power-supply/products/35-50w-co2-laser-power-supply-myjg-40og](http://www.cloudraylaser.com/collections/co2-power-supply/products/35-50w-co2-laser-power-supply-myjg-40og) "


---
**Arthur Wolf** *January 29, 2018 11:36*

Added to the laser cutter guide page, thanks !


---
**Wolfmanjm** *January 29, 2018 11:51*

Interesting, with my Blue box standard K40, I connect PWM to L and it works perfectly, I also still have the standard potentiometer connected to IN. However the PWM period does need to be set to 200 and not the standard 20. This is described in Dons blog [donsthings.blogspot.co.uk - Don's Things](http://donsthings.blogspot.co.uk/search/label/K40%20PWM%20Control) and for wiring [http://donsthings.blogspot.co.uk/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.co.uk/2016/11/the-k40-total-conversion.html)


---
**Martin W** *February 05, 2018 18:53*

Hey, whats with the manual test fire switch wich is connected between G and L on the middle connector? Did it work?




---
**Philippe Guilini** *February 06, 2018 10:04*

**+Martin W** : Hey Martin. This bridge is necessary to make the laser fire. It is not meant as a test button ( as far as we can think ). If this bridge is not made, the laser won't fire at all. On the board from the PSU however, there is a dedicated button to test fire the laser. This is situated between the most left green connector en the next green connector. We also have a red led next to this button that lights up when the tube is active.


---
**Martin W** *February 06, 2018 16:58*

I will rewire it!




---
**Philippe Guilini** *February 08, 2018 10:11*

And..........? Any luck ? Keeping my fingers crossed over here !


---
**Martin W** *February 08, 2018 12:43*

**+Philippe Guilini**  i draw it for me in fritzing. And yes it works!!



I throw the Poti in the trash :D 



But i will test what will happen when in connect the L on the logic side to ground. Will it Fire? Maybe?

![missing image](https://lh3.googleusercontent.com/lARqXy7evGkyrLkWfZvp_JL-cGjXlIvVoUD-8fg-Tv8Nb7dLS2gTEizJ-zT9iHjiahBRhvjYLAKwRgkYAzMa_lKEbki2Ud5RZGM=s0)


---
**Philippe Guilini** *February 09, 2018 10:31*

Never tried this. But.....I saw that you didn't connect the 24 volt and its ground to the smoothieboard ! Do you use a different PSU to power your stepper motors ?   

Glad to hear that you finallly have your laser working !



Philippe.




---
**Martin W** *February 09, 2018 10:52*

**+Philippe Guilini**  I think it might be work. My laser work since last year already :P I use external stepper drivers(DM556) for more tourque. And i use two external Power Supplys 12V for The Smoothie and 24V for the Stepper motors. I didnt trust these chinese HY50 PSU with the 24V :D 










---
**Martin W** *February 11, 2018 14:43*

**+Philippe Guilini** i found a problem :/ when i close my Laser Lid the laser will fire directly with 2-3mA. What looks your smoothie config like? And i cant reach the 100% 19-21mA. Littlebit confused :O




---
**Martin W** *February 11, 2018 15:44*

Im again :) I got the Solution with Test Fire button. Leave the Pot in and make so the maximum current regulation. Works fine with PWM and POT. 

![missing image](https://lh3.googleusercontent.com/pxKrbTsckvLIkVUCeJf4cPXzdot9QFLNZeoqDBUm4cATlrZ6fMT7HITMjZUbB4K14NNBtqzmsQ2pDHIX-nfNTdo-o3_kTMw_8p0=s0)


---
**Philippe Guilini** *February 11, 2018 16:28*

Hehehe ! Something to try ! Not tomorrow, because tomorrow evening a father to be has reserved our laser to make gifts for when the baby is born . Thanks for your message mate .

Philippe.


---
**Martin W** *February 11, 2018 16:29*

Happy laser!




---
**Philippe Guilini** *February 11, 2018 16:35*

On second view of your drawing, something I don't understand. On the smoothie there is a little cluster with a row of five pins, marked : GND, P2.7, P2.5, P2.4, P2.6    From your drawing it seems that you only connect the ground of this row. Or is this a typing error and you mean connecting pins P2.4 en P2.5 ?

Philippe


---
**Martin W** *February 11, 2018 16:42*

That means Groung is connect to 2.5 and the PWM Output is from 2.4. Its always a double connector with only one wire up. Behaps a little bit confusing.


---
*Imported from [Google+](https://plus.google.com/109415938778659747209/posts/8jW5od2A96A) &mdash; content and formatting may not be reliable*
