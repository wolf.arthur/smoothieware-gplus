---
layout: post
title: "I went to print and it crashed the smoothie firmware hard"
date: January 28, 2018 17:38
category: "General discussion"
author: "Rachel Rand"
---
I went to print [https://www.thingiverse.com/thing:1278865](https://www.thingiverse.com/thing:1278865) and it crashed the smoothie firmware  hard. I had to unplug the USB cord - the leds had ceased activity. The gcode had come from cura.





**"Rachel Rand"**

---
---
**Rachel Rand** *January 29, 2018 01:04*

Looks like I can't have the gamma limit on. Grrrr. What's the use of having an endstop without it working as a limit? There should be a soft limit option that prevents motion but doesn't require a reset.

However, the print still stops part way and it still crashes the firmware.

Is this my new smoothie card or is it something else?


---
**Rachel Rand** *January 30, 2018 04:18*

Needed to turn on the handshaking per request  mode. Kinda silly, they should be able to manage this through dynamic flow control.


---
*Imported from [Google+](https://plus.google.com/+RachelDeeRand/posts/KymekuoURkG) &mdash; content and formatting may not be reliable*
