---
layout: post
title: "I'm using smoothieboard in my 3D Printer (Delta) and my MPCNC"
date: January 28, 2018 14:56
category: "General discussion"
author: "S\u00e9bastien Plante"
---
I'm using smoothieboard in my 3D Printer (Delta) and my MPCNC. 



In both case, I'm using Z-Probe. Mechanical switch on the 3D Printer and cooper plate with a lead connected to the bit on the CNC. 



On the 3D Printer, I can calibrate the bed automatically, but on the CNC I can only find the Z-Height at one point.



I was wondering if I could do auto "bed" leveling. Piece of wood are not always flat, the wasteboard is not always flat and the frame is not straight. 



Like probing to the 4 corner and maybe 1 or 2 in the middle to make smoothie compensate the g-code?





**"S\u00e9bastien Plante"**

---
---
**Johan Jakobsson** *January 28, 2018 19:20*

I think the idea with a wasteboard is that you mill it flat using the cnc router.


---
**Sébastien Plante** *January 28, 2018 20:06*

yeah, it's what I do... but it take time and you have to redo it from time to time, plus, it doesn't help if the wood on top is not straight and you don't want to surface it.  But as now, it's my only options.


---
**Jose Manuel Campos** *January 29, 2018 10:39*

**+Sébastien Plante** Use BCNC  [github.com - bCNC](https://github.com/vlachoudis/bCNC/wiki)


---
**Arthur Wolf** *January 29, 2018 11:38*

You can use BCNC, or use Gcodes to run grid levelling on a specific area.


---
*Imported from [Google+](https://plus.google.com/+excessnet/posts/ZgSYyuD46XZ) &mdash; content and formatting may not be reliable*
