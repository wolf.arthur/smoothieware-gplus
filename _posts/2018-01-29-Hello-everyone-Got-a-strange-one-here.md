---
layout: post
title: "Hello everyone. Got a strange one here....."
date: January 29, 2018 20:17
category: "General discussion"
author: "Jonathon Thrumble"
---
Hello everyone. Got a strange one here..... My extruder motor driver is stuck in one direction. When i extrude it goes clock wise when i retract it goes clock wise. I have a x5c so have just swapped to the fith driver but am wondering if there is a fix? Ive checked the motor, and even put my x motor on the driver with the same result. So im at odds to see what it is. Flashed latest firmware, even started with a clean config.txt and no luck. 





**"Jonathon Thrumble"**

---
---
**Triffid Hunter** *January 30, 2018 05:37*

Either your config has remapped the direction pin for that motor driver or you've shorted it somehow or there's a board fault. having a poke around with a multimeter might be helpful


---
**Jonathon Thrumble** *January 30, 2018 10:53*

Yeah i think the driver is dead. Config is fine. 


---
**Arthur Wolf** *January 30, 2018 17:14*

Could be the connector too, it's that sometimes.


---
**Jonathon Thrumble** *January 30, 2018 17:38*

Must admit the original owner had seriously poor soldering skills. So I'll have a check. 


---
*Imported from [Google+](https://plus.google.com/107526739491327163020/posts/EysjN9BW2Cs) &mdash; content and formatting may not be reliable*
