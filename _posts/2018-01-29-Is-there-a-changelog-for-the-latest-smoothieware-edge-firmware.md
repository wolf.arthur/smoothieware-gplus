---
layout: post
title: "Is there a changelog for the latest smoothieware edge firmware?"
date: January 29, 2018 22:18
category: "Development"
author: "Dushyant Ahuja"
---
Is there a changelog for the latest smoothieware edge firmware? I have a version from 2016 and was wondering if it makes sense to update. 





**"Dushyant Ahuja"**

---
---
**Triffid Hunter** *January 30, 2018 05:35*

[github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/commits/edge)



It always makes sense to update to edge, however note that you'll need to regenerate your config from the included example rather than muddle through the breakage that happens if you use an old config with recent firmware.


---
**Dushyant Ahuja** *January 30, 2018 07:15*

**+Triffid Hunter** Thanks a lot


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/KQLdv14NtDy) &mdash; content and formatting may not be reliable*
