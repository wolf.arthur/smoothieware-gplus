---
layout: post
title: "I have come across another issue i cant seem to find any answers to"
date: January 30, 2018 18:25
category: "General discussion"
author: "Daniel Wood"
---
I have come across another issue i cant seem to find any answers to. My heated bed seems to be turning on an off really quickly this is visible by the led flashing on and off. Is there something in the config to control this?







**"Daniel Wood"**

---
---
**Douglas Pearless** *January 30, 2018 18:50*

Can you post your config


---
**Jonathon Thrumble** *January 30, 2018 19:07*

That's how it works. 


---
**Daniel Wood** *January 30, 2018 19:31*

My config is here: [paste.ubuntu.com - Ubuntu Pastebin](https://paste.ubuntu.com/26491055/)


---
**Douglas Pearless** *January 30, 2018 19:57*

I do not see anything wrong with the config for the hotbed.



The most likely cause is Smoothie reacting to the temperature sensor in some way;



Possible things to try:

(1) Using [smoothieware.org - temperaturecontrol [Smoothieware]](http://smoothieware.org/temperaturecontrol) go through the PID tuning and add those parameters to the config

(2) Try another sensor in case that one is faulty

(3) Check the thermal contact between the sensor and the hot-bed (do you have an accurate thermometer you can check against) to minimise thermal lag (time taken for the heat from the hot-bed to get to the sensor)

(4) Increase the readings_per_second setting

(5) As a last resort, you can alter the r0 and beta parameters but at that point, it is probably easier to replace the sensor with another one.




---
**Jonathon Thrumble** *January 30, 2018 20:34*

Is it heating up correctly? 


---
**Reverend Eric Ha** *January 30, 2018 22:38*

Since I don't know anything about how these work I will just ask a quick question that might help: Do these use PID heat controllers? If so that would explain the quick cycling in all likelihood. PIDs shorten their cycles once you near your target temp. 


---
**Jeff DeMaagd** *January 30, 2018 22:39*

How quickly? What you described is not necessarily a problem in itself so it can’t be judged without more context. What is your expectation and what are you seeing? If it’s about ten times a second then you’re fine. The pulse length can seem erratic because it uses a different kind of PWM. What matters is whether the heater temperature is stable. I’ve been able to tune my bed to be reliably +/- 0.1 degrees at steady state. You can change the frequency. You can also change to bang-bang but that will result in wider temperature swings.


---
**Daniel Wood** *January 31, 2018 06:12*

**+Jonathon Thrumble**  Yes it heats up and maintains a consistent temp. Just didn't expect the light show. 


---
**Daniel Wood** *January 31, 2018 06:17*

**+Jeff DeMaagd** I will have to record a video. Using marlin once the bed was up to temp, the led came on for 10 seconds or so then off for 10 and the bed maintained temp well. With smoothie the led doesn't actually go off, it just flashes a few times a second and flickers. 


---
**Douglas Pearless** *January 31, 2018 06:24*

Please go through the PID tuning as this will optimize the way Smoothie handles the over and under shooting of the temperature as it tries to maintain the temperature set.


---
**Jeff DeMaagd** *January 31, 2018 06:48*

**+Daniel Wood** Marlin was set for bang mode then, which is essentially a thermostat cycle. It’s not as tight as PID. Smoothie is probably doing it better provided the tune is within reason. Bang mode might be fine but if there is even a slight heave to the bed, PID would avoid it. The flicker is not a problem.


---
**Triffid Hunter** *January 31, 2018 08:29*

Many years ago I documented print artifacts caused by cycling the bed heater too slowly - the heat/cool cycles caused the bottom of the bed to expand and contract which in turn made it bow up and down affecting layer height and ruining print quality.



Smoothie indeed will cycle the bed heater much faster than marlin, using PID. The higher rate and PID control eliminates thermally induced bed flex, and thus the light show is completely normal as long as your heater is maintaining good thermal regulation.



If you're curious, smoothie uses ΔΣ (delta-sigma) modulation rather than PWM, which is far more suitable for use with AC SSRs (if you set the rate to line frequency), can put less stress on DC power supplies, and furthermore is easier on the MCU when using non-PWM pins at this cycle rate.



The only downside is that ΔΣ modulation is less familiar to hobbyists, which is why I tried to document it fairly extensively in the (inappropriately named) src/libs/Pwm.cpp :P


---
**Triffid Hunter** *January 31, 2018 08:36*

You can reduce the heater temperature read rate in config (try `temperature_control.bed.readings_per_second 5` or so, example configs don't list this value so you have to add it) if the flashing annoys you, but beware that you may notice some artifacts from layer height changes depending on mechanical specifics of your printer's bed, and may need to retune your PID for the new rate. I'd suggest leaving it as-is and covering your lights if they're bright.


---
**Daniel Wood** *February 01, 2018 19:46*

![missing image](https://lh3.googleusercontent.com/dzl0tQbO13T5eCDFkSyzoYynJOmI9LXiZWUo03cziSVGbrAzKywn6LjHIJA0xrCW6DruS-2LT7J76nthLMdx4LY-qZbmWRTBWCIW=s0)


---
**Daniel Wood** *February 01, 2018 19:47*

I think I will leave the lights flashing when i'm getting layers like this!! The print quality has improved significantly since using smoothie.



That's at 0.2mm layer height. I struggled to get a photo that shows the layers they are that fine.  


---
**Triffid Hunter** *February 02, 2018 05:16*

heh 0.2mm is my normal layer height; I've gone down to 0.05mm for specific objects!

Fwiw the lower limit on layer height is mostly defined by your extruder's ability to precisely feed filament at very slow rates, although the precision of your Z axis may have a small influence too.

If you're planning to go lower, the first layer or two should be a saner height so that the high precision layers have a precise planar surface to stick to.


---
**Daniel Wood** *February 02, 2018 05:26*

There's not really any need for me to use lower layer heights. I will probably start using thicker layers soon to try and reduce print times. My main use for 3D printing is for making fixtures and Jigs for CNC milling and prototyping parts before milling so they normally end up in the bin.


---
**Daniel Wood** *February 02, 2018 05:29*

Thank you for all the support and help. I'm super impressed with smoothieware. I'm building a bigger printer this year with dual extrusion and I will certainly be using a smoothieboard. 


---
*Imported from [Google+](https://plus.google.com/+DanielWood/posts/6vF2rGdVnj9) &mdash; content and formatting may not be reliable*
