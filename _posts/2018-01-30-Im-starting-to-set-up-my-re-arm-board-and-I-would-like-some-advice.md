---
layout: post
title: "I'm starting to set up my re-arm board and I would like some advice"
date: January 30, 2018 07:27
category: "General discussion"
author: "Daniel Wood"
---
I'm starting to set up my re-arm board and I would like some advice. I am currently using Marlin and my homing routine is:



G28 XY

G0 X 100 Y100

G28 Z



This is set automatically in marlin as I am using an inductive probe to set the Z height, Marlin offers a Z probe position which I have set to 100,100 to get the probe near the middle of the bed. 



Is there a way to do this in Smoothie so i can issue a G28 and it automatically runs the homing routine? 





![missing image](https://lh3.googleusercontent.com/-couZRiniK4A/WnAeZhB5FhI/AAAAAAAAxe0/4PZIQLHoXjAM4Ajii5kATiH4DZSUnJBxwCJoC/s0/MVIMG_20180129_193835.jpg)



**"Daniel Wood"**

---
---
**Johan Jakobsson** *January 30, 2018 10:01*

You want to use the probe as endstop and home to 0 on Z-axis?

The other way to go about it would be to home to max and then probe the bed.

But if you want to use probe as endstop I guess you could set homing order to XYZ and debounce to 100/100. (but i could be wrong.=)



alpha_homing_retract_mm 100 # Distance to retract from the endstop after it is hit for alpha/X

beta_homing_retract_mm 100 # Distance to retract from the endstop after it is hit for beta/Y

gamma_homing_retract_mm 1 # Distance to retract from the endstop after it is hit for gamma/Z



gamma_min_endstop    1.28^ # Pin to read min endstop, add a ! to invert if endstop is NO connected to ground

#gamma_max_endstop   1.29^ # Pin to read max endstop, uncomment this and comment the above if using max endstops



gamma_homing_direction home_to_min



homing_order XYZ # X axis followed by Y then Z last




---
**Daniel Wood** *January 30, 2018 18:32*

What I have done for now is set my startup code to:

G28 XY; home X and Y

G0 X75 Y75 ; move over the bed

G30 Z1.75 ; set the z height. 



This seems to be working well and is giving repeatable first layers. 


---
**Daniel Wood** *January 30, 2018 18:36*

**+Johan Jakobsson** Superb!! using axis_homing_retract_mm is a great idea. Thanks!!




---
**Johan Jakobsson** *January 30, 2018 20:42*

Don't know which printer you have so not sure what the "correct" way of homing it is. 

But I do suspect homing to max and setting gamma_max to the distance from the bed, then probing to get bed surface would be a "correcter" solution.


---
**Daniel Wood** *January 31, 2018 06:20*

It's i3 clone that I made myself. It doesn't have max endstops. 


---
*Imported from [Google+](https://plus.google.com/+DanielWood/posts/1y5hnDMfr3R) &mdash; content and formatting may not be reliable*
