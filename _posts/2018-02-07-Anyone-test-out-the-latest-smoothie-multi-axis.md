---
layout: post
title: "Anyone test out the latest smoothie multi axis?"
date: February 07, 2018 16:42
category: "Development"
author: "Jim Fong"
---
Anyone test out the latest smoothie multi axis?   I compiled from source on Sunday but incorrect or no axis movements.  The firmware I compiled back in July works as expected but the newer firmware doesn’t with the same config.txt.  



The newer firmware flashed fine since “Version” matches the compile date. 



If I comment out the A,B,C axis config.txt settings and reboot,  I notice X,Y,Z axis will then move correctly.  



I compiled a 4 and 5 axis firmware and both have the same issues. 

Eg.

make AXIS=4 CNC=1





**"Jim Fong"**

---
---
**Arthur Wolf** *February 07, 2018 16:45*

maybe try the latest config too ? ( with as little edits as you can to get it to testing )


---
**Jim Fong** *February 07, 2018 16:53*

**+Arthur Wolf** I actually did that, that’s how I noticed XYZ works without ABC config settings in config.txt.  When I edited in the ABC settings and reboot then it doesn’t work. 


---
**Wolfmanjm** *February 07, 2018 17:14*

try the firmware-latest.bin it is 5 axis. remove all references to extruder in the config. 

I suspect you have not configured it correctly for  5 axis or you built it incorrectly, (wrong toolchain etc)


---
**Jim Fong** *February 07, 2018 17:41*

**+Wolfmanjm** so someone has already tested this 5axis version and confirmed that it works?    I’m at work so I’ll try this tonight.   


---
**Wolfmanjm** *February 07, 2018 17:51*

yes lots of people have been using 6-axis for quite a while now. however you do need to fully understand how it works and what it is used for.


---
**Antonio Hernández** *February 07, 2018 22:26*

**+Arthur Wolf** , **+Wolfmanjm**, do you have an example (video, maybe ?) that could show a machine working with 5th axis, and this axis working as a rotatory element  (or oscillatory, 180 degrees) ?. Just for curiosity...


---
**Arthur Wolf** *February 10, 2018 16:13*

I've seen a video of Smoothie with 4 axes, but not with 5 axis. The rarer a setup, the less likely there is to be video of it ...

I'll definitely film it when I setup 5 axes.


---
**Antonio Hernández** *February 10, 2018 16:45*

**+Arthur Wolf**, That will be fantastic !. Please, do it... we want to see that... nowadays I'm building a cnc machine with 5 axis... (4th and 5th as rotatory), but, I can't see some videos (even comments, just a few) where we can see how it works. If we could see an example.... it will be wonderful... as a consequence, more questions will appear... and that's the idea...


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/VjJok7Zcayc) &mdash; content and formatting may not be reliable*
