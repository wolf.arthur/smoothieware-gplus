---
layout: post
title: "Is there a way to have Smoothieware fire a laser without moving any axis?"
date: February 07, 2018 08:29
category: "General discussion"
author: "LightBurn Software"
---
Is there a way to have Smoothieware fire a laser <b>without</b> moving any axis? Our software supports a 'pierce' mode used for cutting thicker material, which amounts to a power-on with dwell, followed by a standard cut. The user can specify the power percentage and dwell time.



With GRBL, I would do this with:



M3

G1 F100  S xxx

G4 Px.x

M5

G0

# resume normal cutting



I don't see anything comparable in the Smoothie docs, as everything states the system must be moving to fire, with the exception of the fire command, but I'm assuming that does not go through the path planner.



Is this operation possible with a Smoothieboard?





**"LightBurn Software"**

---
---
**Douglas Pearless** *February 07, 2018 09:34*

Can you post your current config and what was the compile options you used for Smoothie?


---
**LightBurn Software** *February 07, 2018 09:37*

If this is a normal operation for Smoothie I wouldn’t expect the config to matter, and I can’t have a requirement that users recompile for this anyway. It’s CNC 4-axis generic Smoo, if I remember right.


---
**Claudio Prezzi** *February 07, 2018 10:06*

You can do that with LaserWeb ;) (see laser test settings)


---
**Wolfmanjm** *February 07, 2018 10:52*

why can't you use the fire command?


---
**Wolfmanjm** *February 07, 2018 10:52*

an alternative is to add a switch that turns the laser on/off with a m code.


---
**Claudio Prezzi** *February 07, 2018 13:00*

Jim is right. Using fire works perfect.

Not sure if G4P works but you can do the duration with a timer in LB.


---
**LightBurn Software** *February 07, 2018 15:34*

Does the fire command work with the planner? If I had a bunch of GCode mixed with fire and delay commands and pushed that through fast-stream, that would work? That’s what I’m looking for.


---
**Wolfmanjm** *February 07, 2018 16:27*

**+LightBurn Software**  You would precede it with M400 to sync it with the stream. I'm not sure fire command returns ok though so I could fix that if needed. You could also define a switch that simpl;y output the fire command on a defined M code. Sort of like a macro.




---
**LightBurn Software** *February 07, 2018 16:30*

I’m looking for something that won’t require a specific config. The lack of OK is fine - I already track whether or not to expect a response for a given line of code.  I’ll try the M400 - thanks.


---
**LightBurn Software** *February 07, 2018 16:31*

(To be clear, I don’t use fast-stream, but something a little more clever, using it as a starting point)


---
**Wolfmanjm** *February 07, 2018 17:17*

**+LightBurn Software** that is fine. really the only thing fast-stream does is decouple the wait for ok on every line.


---
**LightBurn Software** *February 07, 2018 17:56*

I will say that if you made <b>every</b> command return an ok it would make some things much easier for development. For example, writing code to stream over IP to an RPI or ESP that then communicates with Smoothieware, those instances now need to know whether to expect an ok back or not. Using the islower() on the command string is ok, but it wasn’t obvious from the start that would work.


---
**Wolfmanjm** *February 07, 2018 18:08*

Every gcode does return ok. every line returns ok, unless it is lowercase.

 commands like fire are not gcode and therefore do not return ok.


---
**LightBurn Software** *February 07, 2018 18:10*

Exactly my point - I can’t just write a dumb pass-through that waits for ok/error, it has to know whether the command will respond with ok or not.


---
**LightBurn Software** *February 07, 2018 18:10*

It’s minor - just feedback.


---
**Wolfmanjm** *February 07, 2018 18:19*

commands are not designed  to be used in gcode files. most will not work if you stream them. fire maybe an exception. commands are only supposed to be used from the console.




---
**LightBurn Software** *February 07, 2018 18:20*

Well sure, but how does a dumb pass-through know if it is feeding commands from stream or a console?


---
**LightBurn Software** *February 07, 2018 18:22*

It’s fine really. I have fix this in my own applications so I don’t have a stake in the outcome. I was suggesting a small change that would make it simpler for future developers.


---
*Imported from [Google+](https://plus.google.com/110213862985568304559/posts/ENYCDNjZwjG) &mdash; content and formatting may not be reliable*
