---
layout: post
title: "Did you know you can change your Smoothieboard's web interface ?"
date: February 11, 2018 18:41
category: "Development"
author: "Arthur Wolf"
---
Did you know you can change your Smoothieboard's web interface ?



See the wiki, or the live pages at [http://smoothieware.github.io/Webif-pack/](http://smoothieware.github.io/Webif-pack/)



The "new web interface" one is much more featureful, is under development, and is looking for testers and contributors !





**"Arthur Wolf"**

---
---
**Reverend Eric Ha** *February 12, 2018 00:07*

Oooh, nice. I'll have to get this and update. 




---
**Griffin Paquette** *February 12, 2018 13:45*

Looks good! Any chance we are gonna see the WiFi addon’s that Roy now has on Smoothie’s official firmware??


---
**Arthur Wolf** *February 12, 2018 14:16*

I have no idea how his wifi thing works, I bought one but haven't had time to look at it.


---
**brad wigton** *March 15, 2018 17:00*

thank you I learned something new today

. 


---
**Steven Kirby** *March 31, 2018 11:52*

Hey **+Arthur Wolf**, I like the look of the "New Web Interface" interface and would like to use it for my CNC router project. How do i go about setting it as the default web interface?  


---
**Arthur Wolf** *April 02, 2018 15:14*

You need to access it on the sd card ( /sd/ ), it's too big to fit in the mcu's flash like the default one.


---
**Steven Kirby** *April 02, 2018 17:42*

Cool thanks for the info, **+Arthur Wolf**. I'll give it a shot once I fathom my VFD woes!


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/GCWiVimEyni) &mdash; content and formatting may not be reliable*
