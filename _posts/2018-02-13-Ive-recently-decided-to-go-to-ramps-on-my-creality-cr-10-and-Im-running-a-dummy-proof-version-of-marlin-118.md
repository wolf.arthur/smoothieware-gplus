---
layout: post
title: "I've recently decided to go to ramps on my creality cr-10, and I'm running a dummy proof version of marlin 1.1.8"
date: February 13, 2018 22:10
category: "General discussion"
author: "cyris69"
---
I've recently decided to go to ramps on my creality cr-10, and I'm running a dummy proof version of marlin 1.1.8. However, I just purchased a RAMPs 1.4 SB premium board and a re-arm. I've never used smoothieware and not sure how to start and what marlin settings translate to smoothie. I'm just using a capacitive probe, drv8825's, nothing else really special. I know my esteps for extruder, and the X/Y/Z 160/160/400 here is my eeprom settings if anyone can help. My build volume is 300/300/400 also my probe offsets are 

#define X_PROBE_OFFSET_FROM_EXTRUDER 63

#define Y_PROBE_OFFSET_FROM_EXTRUDER 0

![missing image](https://lh3.googleusercontent.com/-5-IPXZBrX3A/WoNiTUoblVI/AAAAAAAAc0o/wIyvwPgqlbsYqurBe2f7GETt6A0Zemy4QCJoC/s0/eeprom.png)



**"cyris69"**

---
---
**Dushyant Ahuja** *February 14, 2018 07:46*

[3dprinterchat.com - Smoothiefy your 3D Printer &#x7c; 3D Printer Chat](https://3dprinterchat.com/2016/10/smoothiefy-your-3d-printer)/

Smoothiefy your 3D Printer | 3D Printer Chat

Try this


---
**Arthur Wolf** *February 14, 2018 10:12*

You want to read the 3D printer guide at [smoothieware.org - start [Smoothieware]](http://smoothieware.org) ( and possible the from-marlin page too ). you need to read it or you'll make mistakes.


---
**cyris69** *February 14, 2018 19:00*

Thanks, I'm working with a few other users who I've found are converting over to smoothie on the same machine as me. I guess my biggest question is how to convert my capacitive probe in such a way that it's no longer my z-endstop. I assume the process will be adding back a physical endstop and putting the wires from the probe elsewhere?


---
**Dushyant Ahuja** *February 15, 2018 02:03*

**+cyris69** Check out these links to convert Marlin to Smoothie:

[https://3dprinterchat.com/2016/10/smoothiefy-your-3d-printer/](https://3dprinterchat.com/2016/10/smoothiefy-your-3d-printer/)

[http://smoothieware.org/3d-printer-guide](http://smoothieware.org/3d-printer-guide)

[http://smoothieware.org/from-marlin](http://smoothieware.org/from-marlin)



You can add a physical endstop and move the probe to ZMax. See directions at: [http://forum.smoothieware.org/zprobe#toc25](http://forum.smoothieware.org/zprobe#toc25)




---
**cyris69** *February 15, 2018 02:58*

**+Dushyant Ahuja** Thanks, I will give these a thorough read. I assume I can't leave the z stop empty though? Not a big deal if I have to add back the z stop

 


---
**Dushyant Ahuja** *February 15, 2018 03:00*

**+cyris69** I've used the z probe as the min endstop as well. Though I don't think that's recommended. 


---
**cyris69** *February 15, 2018 03:58*

**+Dushyant Ahuja**, ok, so G29 should work as it has with marlin just not a recommended method for smoothie 


---
**Dushyant Ahuja** *February 15, 2018 04:00*

Not sure. The way I had done it was too use the probe as the min endstop and as a z probe. So would home using G28


---
**cyris69** *February 15, 2018 04:09*

**+Dushyant Ahuja** Would you care to share your config.txt so I can understand the method. I don't know why I'm having such a odd time getting this which it seems much simpler than marlin in general




---
**Dushyant Ahuja** *February 15, 2018 04:10*

**+cyris69** sorry, don't have that config anymore. Got rid of the probe some time back. Use manual bed leveling now


---
**cyris69** *February 15, 2018 04:11*

**+Dushyant Ahuja** The reason I ask is that since there is no offsets in smoothie like there is in marlin I don't see how you can tune the probe x.xxmm from bed and then set a negative offset to z when it decides to print.




---
**Dushyant Ahuja** *February 15, 2018 04:12*

There are some examples on the smoothie site. You save the offset using M500


---
**Dushyant Ahuja** *February 15, 2018 04:14*

Basically probe, then manually bring the bed down to correct level, tell smoothie to save that as homing position 


---
**Dushyant Ahuja** *February 15, 2018 04:15*

M500 (to save probe results) 

G28 (Home XYZ) (jog down to touch the plate) 

M306 Z0 

M500 (to save homing offset) G28




---
**cyris69** *February 15, 2018 04:19*

**+Dushyant Ahuja** Ok, so it seems I just leave it as an endstop and when probing set the offset from trigger using G30 Znnn so for me as example would be G30 Z-2.68. From there create a mesh and save it and instead of adding G29 or whatever in start script just do G28 then G30 Z-2.68 and call it a day?


---
**Dushyant Ahuja** *February 15, 2018 04:30*

**+cyris69** sounds like a plan


---
**cyris69** *February 15, 2018 04:35*

**+Dushyant Ahuja** Good deal, I'll still attempt to learn the proper way, as it is nice to have a physical endstop for protection either way. Thanks for your help, I'll post back once I get it setup.


---
**cyris69** *February 15, 2018 06:08*

Ordered a BLtouch to mess around with


---
*Imported from [Google+](https://plus.google.com/117940551724978334733/posts/HG33oVYNMdT) &mdash; content and formatting may not be reliable*
