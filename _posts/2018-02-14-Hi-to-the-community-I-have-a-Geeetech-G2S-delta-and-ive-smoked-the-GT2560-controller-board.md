---
layout: post
title: "Hi to the community. I have a Geeetech G2S delta, and i've smoked the GT2560 controller board"
date: February 14, 2018 08:23
category: "General discussion"
author: "Dario Armellin"
---
Hi to the community.

I have a Geeetech G2S delta, and i've smoked the GT2560 controller board.

I would like to moove to 32bit and smoothieware is the natural choice, i've selected for cost reason the MKS 1.3, i would like to ask few quesions:

1. Does somebody has a G2S config to start with ?

2. I know the 32 bit boards are not compatible with 2004 display, is it the any workaround for this ? 

Many thanks for any help ! 







**"Dario Armellin"**

---
---
**Wolfmanjm** *February 14, 2018 10:49*

by buying an MKS you will not get any support from the smoothie community. you will need to ask MKS for support.... 



[smoothieware.org - troubleshooting [Smoothieware]](http://smoothieware.org/troubleshooting#somebody-refused-to-help-me-because-my-board-is-a-mks-what-s-that-all-about)


---
**Dushyant Ahuja** *February 14, 2018 11:23*

Look for open source alternatives - 

[http://smoothieware.org/getting-smoothieboard](http://smoothieware.org/getting-smoothieboard)

[http://cohesion3d.com/](http://cohesion3d.com/) 

[http://www.panucatt.com/azteeg_X5_mini_reprap_3d_printer_controller_p/ax5mini.htm](http://www.panucatt.com/azteeg_X5_mini_reprap_3d_printer_controller_p/ax5mini.htm)




---
**Dario Armellin** *February 14, 2018 11:46*

Cool, i didn't know about this issue will go for an opensource version then, any help with the config ? 




---
**Griffin Paquette** *February 14, 2018 15:06*

Personally a big fan of **+Ray Kholodovsky**  and his Cohesion 3D boards.


---
**Dario Armellin** *February 14, 2018 15:33*

Thanks, will have a look to the Cohesion3D, i hope to source it in europe at a decent price


---
*Imported from [Google+](https://plus.google.com/108857839134462169948/posts/g4oLs77Fgaa) &mdash; content and formatting may not be reliable*
