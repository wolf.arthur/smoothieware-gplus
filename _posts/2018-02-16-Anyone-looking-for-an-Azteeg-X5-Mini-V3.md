---
layout: post
title: "Anyone looking for an Azteeg X5 Mini V3?"
date: February 16, 2018 04:24
category: "General discussion"
author: "Matthew Kelch"
---
Anyone looking for an Azteeg X5 Mini V3? I have one which I'd like to sell along with a set of four Panucatt SD6128 stepper drivers.



Looking for $70 shipped. 



EDIT: SOLD!





**"Matthew Kelch"**

---
---
**Matthew Kelch** *February 16, 2018 04:52*

**+Michael Johnson**, yes, should have mentioned that. I'm in the US. I can ship elsewhere but it will cost a bit extra.


---
**Gary Hangsleben** *February 16, 2018 07:44*

I can use a spare board for my buiild.  I'll buy it.


---
*Imported from [Google+](https://plus.google.com/+MatthewKelch/posts/NK73PWcG3RX) &mdash; content and formatting may not be reliable*
