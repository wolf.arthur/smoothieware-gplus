---
layout: post
title: "this landed on my desk... smintx6 smoothie compatible board"
date: February 17, 2018 00:02
category: "General discussion"
author: "Marko Novak"
---
this landed on my desk...

smintx6 smoothie compatible board.



I'm new to smoothie ecosystem, firstly i'm interested if any of you already know this board and if it is in any shape in violation of smoothiewere...



i only received basic pinout diagrams, rest of info on installation/configuration is linked to smoothieware page.



board is made in same country i live, so technical support is bit easier for me, they said the board is completely open source, so maybe they just need bit help in support department ;)



i hope i didn't step on any toes, for pasting this here :)



[https://www.azurefilm.com/product/3d-printer-parts/electronics/smintx6-5stepper-driver/](https://www.azurefilm.com/product/3d-printer-parts/electronics/smintx6-5stepper-driver/)



![missing image](https://lh3.googleusercontent.com/-uzu9kRk3Fx4/WodxLRqMx9I/AAAAAAAAEms/hfGghuFx4iQpLBr3KENYB22oaoT0-EQ_ACJoC/s0/P_20180216_102012.jpg)
![missing image](https://lh3.googleusercontent.com/-odx_aCqfrPg/WodxLpBKiOI/AAAAAAAAEms/jsovmSZ9nBgEGEuNvl4Nat5KZn15sNo6QCJoC/s0/P_20180216_101947.jpg)

**"Marko Novak"**

---
---
**Tiago Ermida** *February 17, 2018 00:17*

Which drivers?


---
**Marko Novak** *February 17, 2018 00:45*

**+Tiago Ermida** if i got it right, same ones as on original smoothieboard...



edit:reposted, because of photo compression. drivers are A5984.[https://lh3.googleusercontent.com/eeOyJ9_rggPHeCAs4EV9co7KKiQZwerM9aB-Y7ULjwQRWHdnEMr8Z5_UbVX8_yny2nHdyeZbLg](https://profiles.google.com/photos/112050613337136711262/albums/6523317991932370929/6523317989009930146)


---
**Griffin Paquette** *February 17, 2018 01:01*

I remember seeing the prototypes of this! Looks cool


---
**Cruz Monrreal (Mr. Cruz)** *February 17, 2018 01:35*

Damn, that is one clean, well layed-out board.


---
**Griffin Paquette** *February 17, 2018 02:30*

**+Cruz Monrreal** no kidding. Damn nice board from the looks of it. 


---
**Marko Novak** *February 17, 2018 02:37*

i got it without heatsink (stamped alu plate with 2 fans), i reckon i could fit heatpipe from laptop or 2 on it... but i was told that it could work passive on "lite" loads (<1A, no heated bed).



one this i still miss... pinout sequence on motor connectors.


---
**Marko Novak** *February 17, 2018 02:42*

**+Griffin Paquette** only thing i could trip over on the board... flux wasn't cleaned after soldiering connectors. 



board layout is a bit different than on the photos i got, i presume photos are from prototype.


---
**Griffin Paquette** *February 17, 2018 02:48*

**+Marko Novak** yeah that’s not good. Can cause corrosion pretty quickly. Any chance you could give me what the top of the mosfets read? Wanting to know their value. 


---
**Marko Novak** *February 17, 2018 02:50*

**+Griffin Paquette** you'll get it in few hours ;) when i get home


---
**Marko Novak** *February 17, 2018 05:58*

taking a photo is easier than reading :)

all 6 are same, could you get back to me with numbers... so i don't have to dig.[https://lh3.googleusercontent.com/E4w2Y65jwY9r03n8BCn8mRWbrlsy2lwaiRSaftRYQVbHRG9cNk80_4w9PEXAdqXDF0enGovlsw](https://profiles.google.com/photos/112050613337136711262/albums/6523398684168880257/6523398685314822722)


---
**Marko Novak** *February 17, 2018 05:59*

any other close-ups needed?


---
**Triffid Hunter** *February 17, 2018 12:57*

Yikes I hope they're driving those mosfets from at least 5v, they're extremely marginal with only 3.3v on the gate.. see Fig.8 in the datasheet (Infineon BSC067N06LS3)


---
**Griffin Paquette** *February 17, 2018 15:12*

**+Triffid Hunter** they have gate drivers that you can see from the full board view. No FETs work particularly well at 3.3V gate voltage from Smoothieware. Almost everyone uses s gate driver to step it up. 


---
**Marko Novak** *March 04, 2018 19:16*

without proper pinout diagram, i managed to get basic things going in geeetech g2s frame...

bit of discussion and photos here:

[https://plus.google.com/108857839134462169948/posts/g4fPkiBcuyg](https://plus.google.com/108857839134462169948/posts/g4fPkiBcuyg)



there is quite a bit of "coil wine" from FET when powering the 144w bed (24v).



all 5/3v3 switch points are pre soldiered for 3v3 (smd) in my case.

[plus.google.com - Hi to the community. I have a Geeetech G2S delta, and i've smoked the GT2560 ...](https://plus.google.com/108857839134462169948/posts/g4fPkiBcuyg)


---
**Marko Novak** *March 04, 2018 19:19*

looks like i will have to rotate glcd cables, since i only get quick beep and no screen.


---
**Marko Novak** *April 02, 2018 12:10*

board was tested on g2s delta, to confirm it works... 

then off to modding :)



exp2 reset pin (as it works in glc and marlin...) was not connected to anything, so i jumped reset pin from ISP connector. 



if that isn't wise, please let me know, i still have to finish other stuff on intended printer frame, so it won't be powered on any time soon :)[https://lh3.googleusercontent.com/09jMSEyX5Zl_p_LQTk7XhZ2HANDDzL9kVCi3sHvGm4LAwLU6nAho12QIS0C4zlZnY8j9XfCQRg](https://profiles.google.com/photos/112050613337136711262/albums/6539822368664038817/6539822367634671298)


---
**Marko Novak** *April 02, 2018 12:17*

added some pins...

top left, led indication pins that could be repurposed...

 and below Ethernet connector, signal pins for mosFETs, few gnd points, 3v3, 5V, and one VBB pin.[https://lh3.googleusercontent.com/8uqcnC-AjuZAMjkmVqC7cU09ACxUkaK3osH3GwSFro0jGAZlobD768smzeFQfJu9v8VqjHvtdA](https://profiles.google.com/photos/112050613337136711262/albums/6539824077123708545/6539824077962518994)


---
**Marko Novak** *April 02, 2018 12:21*

rest of the pins are doubled from exp ports and thermistors, but in different arrangements.


---
**Bray pp** *January 10, 2019 20:23*

more infos about drivers, fets, pinout, schematic,...:



[indata.si - SMINTx6](https://www.indata.si/product/smintx6/)


---
**Marko Novak** *February 02, 2019 17:38*

**+Bray pp** sorry, I forgot to reply earlier... Same problem still persist, drawings from previous version 😉. I didn't get the board from them directly, but i did note it to my seller... 



HW wise I got it figured out as much as I need it, but I can't use it on my big delta (not enough calibration options), i hit the wall trying to port marlin on it.

Electrically it held well, pushing 350W+(24V) true the board with adequate cooling isn't an issue, or running mixed voltage.



I recently "burned" board I used on my modified ender3, but it's bit too big for "elegant" swap. Without custom mount and new wiring... If I do that, I tend to do some additional things too, dragging down time.



It has potential, it needs few more links on info pages (or selling pages) and bit more revision attention. 


---
*Imported from [Google+](https://plus.google.com/112050613337136711262/posts/94wmXQX9orc) &mdash; content and formatting may not be reliable*
