---
layout: post
title: "I've finally gotten all axises and probe working on my ramps + re-arm"
date: February 20, 2018 04:43
category: "Help"
author: "cyris69"
---
I've finally gotten all axises and probe working on my ramps + re-arm. However, I can not figure out how to get the probe when doing grid probing to stay inside the boundaries. I see that with a probe that is offset from the hotend that you apparently have to mess with the bed volume x/y to get it to not start probing off the left of the bed. So I've tried changing the size to accommodate the offset as you can see in from my beautiful mspaint skills. If I try to adjust the X much further it will not understand its hit the x-max and jam up but also I can not get it to move inward on the bed for the y to probe. I don't think changing the offset of probe to a - would matter but I also don't want to affect the mesh. Then from there I'm not sure how to give it the offset to move down to for printing.  Link to my current config [https://pastebin.com/fTifnfSE](https://pastebin.com/fTifnfSE)

![missing image](https://lh3.googleusercontent.com/-KCnTDKrltbM/WounizHvtfI/AAAAAAAAdAQ/v4TS89vU9YoPMHLM4VpBsd43muHKraIlwCJoC/s0/probing.png)



**"cyris69"**

---
---
**cyris69** *February 20, 2018 05:07*

Unless I can fake it using origins?

# Extruder offset

#extruder.hotend.x_offset                        0            # X offset from origin in mm

#extruder.hotend.y_offset                        0            # Y offset from origin in mm

#extruder.hotend.z_offset                        0            # Z offset from origin in mm




---
**Jeremiah Coley** *February 20, 2018 05:31*

Wish I new, still a project I haven't gotten far in, not out of the box yet, far. 😁

Got any pointers?


---
**cyris69** *February 20, 2018 06:25*

**+Jeremiah Coley** I came from old school sailfish, then onto marlin, now trying smoothie & Repetier. While I'm not that great at the software side of things it hasn't caused me too much hair loss. While I enjoy a single word document for settings and a bin file for updating I still miss marlin. It's grid probing was easier to configure with marlin letting you pick the boundaries much more precisely and less confusing. I still really don't know how to set the offset to print at how in marlin I just set Z offset -2.65 in eeprom and good to go. I'm sure I'll get it eventually but have my rostock max v3 which has a rambo board running Repetier to repair this week so I can actually be printing while working smoothie out.  If you are just using a cartesian like I am I wouldn't even bother just stick with marlin and an 8-bit board, like ramps and pop in some decent drivers, maybe TMC2100 and call it a day. However, If you're going corexy or delta then sure get a nice 32-bit setup (duet) but for a budget you can go with a handful of cheaper solutions. So if you want advice is just stick with what you know..


---
**Jeremiah Coley** *February 20, 2018 07:20*

**+cyris69** Thanks, I have a core xy that I had planned on putting my ReArm into. But smoothie vs marlin..... Well it was two different languages. Anyway I plan on getting to it sooner or later, running a Prusa mk2 clone at the moment with Ramps/ Mega/ tmc2130/ sensorless homing and all that good jazz running on marlin. But I like to keep looking into smoothie and use posts like your own as learning opportunities.


---
**Johan Jakobsson** *February 20, 2018 08:45*

What's the reason so many are running grid calibrations?

If you print on a flat surface, for example glass or a flat piece of aluminium, a 3 point platform calibration should suffice? 


---
**Johan Jakobsson** *February 20, 2018 09:00*

From the pictures it looks like the probe hits outside the printbed in Y? Is the probe in front of your hotend or does the Y axis home to a position where the hotend is outside the printbed?

If so, say that Y homes to a position 5mm in front of the printbed, i think you'd want to set alpha_min to -5. Same thing with X.

afaik,m you want your 0,0 X/Y to be in the lower left corner of the bed, not outside it. But i could be wrong. =)


---
**cyris69** *February 20, 2018 20:20*

Yeah its off because marlin when a probe is involved makes the probe act as the nozzle when homing. So a 0,0 with probe offset of -41 it puts the nozzle + 41 on the x axis where smoothie apparently doesn't. The reason for grid probing is for variances in the build platform/glass itself. No surface is perfectly flat and the larger the surface the more chances there are for variance. I will be uploading a video shortly showing what it is doing.

 


---
*Imported from [Google+](https://plus.google.com/117940551724978334733/posts/hM7jthgWtnh) &mdash; content and formatting may not be reliable*
