---
layout: post
title: "Is there basic xy probing capability in grbl mode?"
date: February 22, 2018 04:06
category: "General discussion"
author: "Aaron Pence"
---
Is there basic xy probing capability in grbl mode?





**"Aaron Pence"**

---
---
**Arthur Wolf** *February 22, 2018 10:00*

Sure, grbl mode just adjusts a few things about the way the board talks to the world. All functions are still there.


---
**Sébastien Plante** *February 22, 2018 12:18*

So we could do auto-calibration in X,Y and Z on a CNC ? 


---
**Arthur Wolf** *February 22, 2018 12:23*

Sure. 


---
**Aaron Pence** *February 23, 2018 01:21*

I have been drooling over craftycnc probe it software for mach3. but simple x and y to set work offsets is really all I would ever use.

 


---
**Sébastien Plante** *February 23, 2018 22:48*

I set X Y manually to the "0" of the part I work on, but the surface is rarely flat, so I wish I could calibrate the surface (Z) like I do with my 3D printer ! :D


---
**Aaron Pence** *February 24, 2018 16:05*

I thought guys were doing simple 3 point z plane in grbl for pcb milling.


---
**Sébastien Plante** *February 24, 2018 18:10*

Should be okay for small part, a I'll try that! 


---
*Imported from [Google+](https://plus.google.com/115744249065158172290/posts/CASj7L5vs5G) &mdash; content and formatting may not be reliable*
