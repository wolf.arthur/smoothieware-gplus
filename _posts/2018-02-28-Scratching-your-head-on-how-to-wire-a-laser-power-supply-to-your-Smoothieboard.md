---
layout: post
title: "Scratching your head on how to wire a laser power supply to your Smoothieboard ?"
date: February 28, 2018 18:38
category: "General discussion"
author: "Arthur Wolf"
---
Scratching your head on how to wire a laser power supply to your Smoothieboard ? We finally have a diagram for that. See : 



[http://smoothieware.org/laser#example-setup](http://smoothieware.org/laser#example-setup)



( this wiring is pretty cool as it removes the need for a level shifter to go to 5V, as it switches to Ground ).



Feedback very much welcome.





**"Arthur Wolf"**

---
---
**Paul Mott** *March 01, 2018 12:30*

I would not want to operate my laser at 100% output power (accidentally or otherwise).

Instead of connecting IN to +5Volt  I would connect IN to the wiper of a 10K w.w. potentiometer wired between +5V and GND. This way I could manually pre-set the maximum laser tube output power that was available to be used by my software.




---
**Arthur Wolf** *March 01, 2018 13:00*

Yep the page actually mentions this. Having it set at 100% is fine though for most laser setups ...


---
**Martin W** *March 18, 2018 08:47*

This works fine. I wanted to keep the POT in to set the maximum mA but wanted full PWM. Testfire button works too ;) Works great!



![missing image](https://lh3.googleusercontent.com/7l0UwdDw8pMZvzayL5kZLIw-P6g-ysxPbzx0YR5tJwvvBo6FvwOvTmI8-4z_cWnerTY1sv8EWzvvjIWksPC-Gteswc6X7xhmJFo=s0)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/Jaqq8sY6TRr) &mdash; content and formatting may not be reliable*
