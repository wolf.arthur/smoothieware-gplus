---
layout: post
title: "Hey everyone, i need help with testing a candidate feature"
date: March 03, 2018 12:08
category: "Development"
author: "Arthur Wolf"
---
Hey everyone, i need help with testing a candidate feature.



It's a new error reporting system that standardizes error messages in Smoothie ( we have over 100 ) via a single call and remembers config errors so you can get a list of them afterward ( allowing you to see problems even after they occurred ).



The branch is at [https://github.com/Smoothieware/Smoothieware/tree/feature/new-errors](https://github.com/Smoothieware/Smoothieware/tree/feature/new-errors) ( email me at wolf.arthur@gmail.com if you want a .bin ).



The current list of errors is at : [http://smoothieware.org/error](http://smoothieware.org/error)



If you are curious, the spec for the new system is at [http://smoothieware.org/error](http://smoothieware.org/error)



Ideally, if you could start using this new branch, test the "errors" command, and see if you can find any bug, that'd be extremely helpful.



Thanks !





**"Arthur Wolf"**

---
---
**Antonio Hernández** *March 08, 2018 00:06*

This new standard, will apply for older smoothieboards ? (Does it matter smoothieboard version ?).


---
**Arthur Wolf** *March 08, 2018 10:19*

Yes, and no.


---
**Antonio Hernández** *March 08, 2018 17:00*

Ok, great.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/bP37Wpj7ZBH) &mdash; content and formatting may not be reliable*
