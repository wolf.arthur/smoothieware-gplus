---
layout: post
title: "Is smoothie capable of running a cnc lathe?"
date: March 08, 2018 23:19
category: "General discussion"
author: "Steven Kirby"
---
Is smoothie capable of running a cnc lathe? Specifically, does it have the capability to interpret spindle encoder pulses to allow synchronization of the z axis motion and facilitate thread cutting?





**"Steven Kirby"**

---
---
**Jim Fong** *March 09, 2018 03:11*

As far as I know, smoothieware does not support lathe threading with encoder sync.  I don’t think there is any microcontroller based gcode controller that does a G76 threading cycle.  You will have to use a PC based gcode controller.  Mach3 and LinuxCNC supports G76 threading and of course the commercial industrial controllers. 



I use Mach3 and a Hall effect sensor to do threading on my mini lathe.  It works rather well.  


---
**Arthur Wolf** *March 09, 2018 09:20*

You can run a lathe, but you can't do thread cutting, we don't have the required sync code.  

It's a planned feature, but first one of us needs to get a lathe, and the time to work on it.


---
**Steven Kirby** *March 09, 2018 18:02*

Thanks for your responses guys. I'a assumed this was the case but with smoothie being so versatile and modular I wondered if there might be some sort of hack or workaround with one of the existing modules. With lathes being less popular than routers, mills, 3D printers and laser cutters for hobbyists, I can understand why it's not been implemented yet. 



I have a lathe which I plan to retrofit with CNC capabilites once I've found an appropriate motion controller. I also have a spare smoothieboard which I accidentally fried and subsequently repaired. Well, it functions now at least. Unfortunately I also I blew a mosfet so bought a replacement board for the 3D printer I was installing it in, since I wanted to use all the mosfet outputs. If you do come up with something Arthur, I'd be happy to do some testing with my spare board. :)



Jim, which motion controller did you settle on for your minilathe? There's so many dirt cheap motion controllers for almost any other type of robotic fabrication machine you can imagine. Many of these would work for basic lathe functionality too, but I'd rather keep my thread cutting ability, which as you point out rules out all the grbl based optons. Mach3 looks a bit clunky, Linux CNC I can get behind but there's no USB communication and most of the cheaper motion controllers use parallel port interface which would require an archaic computer. My best finds so far are the centroid acorn and planet cnc's usb cnc, but both are just a little more than I'd like to spend on a motion controller. If you've got any good suggestions I've not considered then I'm all ears.


---
**Jim Fong** *March 09, 2018 19:00*

**+Steven Kirby** I didn’t bother with adding a external motion controller for my mini lathe conversation.  I used a old dell core2duo PC with parallel port step direction output. ($25 on eBay) The parallel port is directly connected to two gecko stepper drives.  The parallel port is more than fast enough for the mini lathe.  It can go 200ipm but I have it set much lower at 100ipm.  The short bed travel of the lathe doesn’t really require fast rapids. I spent the money on buying really good ground ballscrews rather than a external motion adapter. 



Mach3 maybe old and clunky but it works. I would use LinuxCNC if I didn’t already have a Mach3 license. 



Even more important than threading for lathe work is diameter and radius mode.  If you don’t know, you will soon find out when using a CNC lathe and CAM program.  Mach3 and LinuxCNC both support it. 



 Good luck. 



Lathe threading video


{% include youtubePlayer.html id="2HpvWgHT8To" %}
[https://youtu.be/2HpvWgHT8To](https://youtu.be/2HpvWgHT8To)





Conversion pictures 

[http://www.embeddedtronics.com/cnc_lathe.html](http://www.embeddedtronics.com/cnc_lathe.html)


---
**Steven Kirby** *March 10, 2018 15:35*

Cheers Jim! looks like an ancient laptop may be the way to go then. i keep hitting dead ends looking for a reasonably priced usb/ethernet board capable of spindle sync. What did you use aside from the geckos? ie what extra electronics would I need aside from stepper drivers and old LPT laptop to connect an encoder/optical or hall effect sensor or some such for reading the spindle rpm?  I'm assuming there's a BOB in the mix somewhere?



Am I going to struggle with the stock screws for accurate cnc machining?  Ultimately I'll add ballscrews to the conversion but i was hoping I could get away with leaving the stock threaded rods in to get going.



Thanks for the all advice. your machine looks spot on, it certainly makes short work of those threads. Nice job!


---
**Jim Fong** *March 10, 2018 17:09*

**+Steven Kirby** I’ve read that using a laptop isn’t recommended for either Mach3 or LinuxCNC due to the hardware power saving features. Some laptops can turn off those features in the bios. You can try but if you get lockup’s or miss stepping, it’s probably due to that issue. 



I wired the stepper drivers directly to a DB25 connector on the lathe but I used this breakout board on my other cnc. Makes it easier with screw terminals. 



[https://www.ebay.com/itm/162144263602](https://www.ebay.com/itm/162144263602)



I used Hall effect and magnet but most use just a optical end stop (used in 3d printers) since you can buy them easier. You only need 1 pulse per revolution to do threading. 



The acme screws/nuts on the mini lathe are ok and can be adjusted for minimal backlash. Lots of videos on how that is done.  Mach3 and LinuxCNC has backlash compensation which works but try to keep actual backlash to a minimum.  Plenty of lathe conversions using original leadscrews. 



All of this is off topic for smoothieware. So not to get the mods pissed off, you can send me private message if you want for more info. 








---
**Steven Kirby** *March 11, 2018 03:56*

Thanks, Jim. Lots of great information there. You're correct though, this is off topic. Apologies, Arthur. 



I'm starting to get the picture of how I should put this together now. I'll see how far those nuggets of wisdom you gave will get me but I may well get in touch if I hit any snags. Appreciated! 


---
*Imported from [Google+](https://plus.google.com/112120819276379662423/posts/bGutFaFhXgK) &mdash; content and formatting may not be reliable*
