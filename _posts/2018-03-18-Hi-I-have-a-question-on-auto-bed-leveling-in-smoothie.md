---
layout: post
title: "Hi, I have a question on auto bed leveling in smoothie"
date: March 18, 2018 09:45
category: "Help"
author: "Marc Pentenrieder"
---
Hi, I have a question on auto bed leveling in smoothie.

I configured auto bed leveling (cartesian grid leveling) and the leveling works as expected, but the printer doesn't respect the leveling results (I think).

When the nozzle goes to Z0 at x0,y0 everything is fine, but when the nozzle goes to Z0 at x500,y500 (mid bed) it is way to high, so I can move a sheet of paper easily under the nozzle)

Do I have to activate the leveling mesh somehow ?



Here is my start gcode I use:



G28 ; home all axes (Z will be homed to max)

M280 S3.0 ;push probe down

G32 ; Bed auto level (Cartesian Grid Level)

G30 Z12.2 ; Find Z-Min

G1 Z20 ; move z up

M280 S7.0 ; pull probe up

G0 X5 Y5 Z5 ; prepare for print



Do I have to use the following M-Codes ?

M373 Z grid strategy: completes calibration and enables the Z compensation grid 



Do I have to use the following before each G32 if I want to level before each print?

M561 clears the plane and the bed leveling is disabled until G32 is run again 



Thanks for any suggestion







**"Marc Pentenrieder"**

---
---
**Marc Pentenrieder** *March 18, 2018 20:13*

When I change the G32 and G30 Z12.2 to do the G30 first, the G32 will be ignored/not done. Why?


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/S5LTkGGqxKn) &mdash; content and formatting may not be reliable*
