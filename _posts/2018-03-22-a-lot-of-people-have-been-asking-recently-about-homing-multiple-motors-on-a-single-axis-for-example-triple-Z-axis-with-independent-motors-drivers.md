---
layout: post
title: "a lot of people have been asking recently about homing multiple motors on a single axis ( for example triple Z axis with independent motors/drivers )"
date: March 22, 2018 20:55
category: "General discussion"
author: "Arthur Wolf"
---
a lot of people have been asking recently about homing multiple motors on a single axis ( for example triple Z axis with independent motors/drivers ). I've written documentation about how I think it can be implemented ( at least two users have gotten it to work this way ), I'd love some feedback from users trying it. The documentation is at : [http://smoothieware.org/switch#homing-a-multi-motor-axis](http://smoothieware.org/switch#homing-a-multi-motor-axis)





**"Arthur Wolf"**

---
---
**Anthony Bolgar** *March 22, 2018 21:01*

Nice write up Arthur. Keep up the great work!


---
**Douglas Pearless** *March 22, 2018 21:36*

This is super cool, I am looking at something that this would be perfect for!


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/Hp1cS8pBzEQ) &mdash; content and formatting may not be reliable*
