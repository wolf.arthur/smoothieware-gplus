---
layout: post
title: "Hey everyone, Im new to smoothie and whatnot, I just put the newest edge firmware on my genuine 5x, and wondering about the config file"
date: March 23, 2018 19:48
category: "General discussion"
author: "Marlowe Scott"
---
Hey everyone, Im new to smoothie and whatnot, I just put the newest edge firmware on my genuine 5x, and wondering about the config file. Is there an Edge specific config, or do I just grab the newest one from the big blue button on the config page of documentation? And also, do I just copy/paste the new over the old? Im only using microsoft word to edit, is that ok? 

One more thing for now, as far as resetting the board... unplugging the usb and plugging back in should be the same as hitting reset button correct? 

Thanks in advance for the help!





**"Marlowe Scott"**

---
---
**Wolfmanjm** *March 23, 2018 21:24*

use the relevant config from the config samples on github. do not edit with microsoft word

please read the wiki for why. yes reset is the same as power on


---
**Marlowe Scott** *March 24, 2018 01:29*

Thanks for the quick response, but i cant find anything about Word on [smoothieware.org](http://smoothieware.org). not the easiest for newcomers as far as coding goes. Seems easy for people crossing from Marlin to Smoothie, but i have no experience with 3d printing coding. Closest thing i could find was saying dont use Notepad++ and something about ANSI which i know nothing about. Ive never been formally trained with coding, but instead of saying what kind to not use, why cant i seem to find anywhere that just says, "use this program to edit smoothie config file" it seems to be a secret or something. I mean, the documentation is so thourough, and ive learned so much from reading it, but it should really just state what software you need right from the top in the Basics, or even the Configuration page... Im not tryin to be a jerk or anything, i just want to learn.


---
**Marlowe Scott** *March 24, 2018 02:37*

Im guessing C++ is different from Notepad++ and feel like a moron. Ill try that. Never heard of it or used it. This has been hanging me up for weeks. Should be good from here thanks again. The github reference you made helped me out. 


---
**Johan Jakobsson** *March 24, 2018 09:26*

C++ and Notepad++ are two very different things. C++ is a programming language, Notepad++ is a text editor.

While word can be used to edit text file, it is a word processor application. It should never be used for editing configuration files as it can add formatting to the config file which make the file unreadable.

You can use something like gedit  which is suggested in the smoothieware wiki; [smoothieware.org - configuring-smoothie [Smoothieware]](http://smoothieware.org/configuring-smoothie)


---
**Marlowe Scott** *March 24, 2018 19:33*

Thanks, i was actually up late tryin to figure it out and i got it. I downloaded visual studio from microsoft cause it said it does object oriented c++. Opened my config file with that, edited, and saved without changing the file type. Worked like a charm. I never imagined the hardest part was finding a program to edit some text. Im aware I jumped into this head first without knowing how to swim, but what fun is it otherwise? Haha 


---
**Johan Jakobsson** *March 25, 2018 14:42*

**+Marlowe Scott** I'm not sure if you never bothered reading what i wrote or if you're trolling me. I also have no idea why you keep mentioning C++ as it has nothing what so ever to do with the config file. 

No, the hardest part isn't finding a program to edit the config file. A suggestion on what application to use is right there in the wiki which i linked to. 

The hardest part seems to be, and it's way to common, to read the documentation. You have access to a really good wiki, use it!

Tbh, to me it seems like a really REALLY bad idea to start editing the config file before thoroughly reading through the wiki. I wouldn't be surprised if it resulted in a damaged printer or worse personal injuries. 


---
**Marlowe Scott** *March 25, 2018 20:44*

**+Johan Jakobsson** you gotta relax dude. I didnt see your reply till after i figured it out on my own. I have absolutely no computer programming experience so i dont know what im talking about with text editing, but i know what i got to work. Im mentioning C++ because of Wolfman's telling me to get config files from the github. When i went there it says clearly that it is written in object-oriented C++ (which ive never heard of) so i did a google search for object-oriented C++ text editior. Came up with visual studio... im not a programmer so i didnt have the correct text editor. So YES, finding a program to edit the files has been the HARDEST part for ME. Its only easy after knowing. Im not trolling anybody, i dont waste my time like that, i honestly had no idea. Trust me ive been reading through the documentation for the past 3 weeks just in preparing... Ask me anything. I went to school for electronics, and recently helped a friend put together an X-Carve, (which is what gave me the confidence to build my own 3d printer.) Ive also built my own (from scratch) heat treating oven for knives with a ramping pid controller and a custom made heating element that gets close to 2000 degrees and works perfectly, so please dont worry about me damaging the printer or myself. All i was trying to edit in the config file is adding "arm_solution corexy" and uncommenting the corexy homing. Now when i move x and y in pronterface, both motors spin together the correct way. That link you provided, i had already been talking about that same page in my original and 2nd post. I didnt see the suggestion cause i was just skipping over the part that was telling me not to use Notepad++. My bad! For real though, if the documentation wants people to use the Gnome Getit thing, it should state it in the software needed section, or in the basics, or on the top of the configuration page we keep talking about rather than only in a warning style sidenote halfway down the page. You wouldnt tell someone how to edit a photo without first telling them what program might work best for the job... just sayin. 


---
**Johan Jakobsson** *March 25, 2018 20:58*

Big misunderstanding, sorry if I was harsh. I've never come across someone who is technical enough to build hos own 3D printer and at the same time doesn't know about text editors.. there's a first for everything. =) So I drew some wrong conclusions for which I am sorry.

If you need further assistance I suggest you join the smoothieware IRC channel. You'll usually get answers to your questions a lot faster than on G+. You'll find a link to the web-based IRC client on the smoothieware homepage.




---
*Imported from [Google+](https://plus.google.com/106446479804426845058/posts/jViNkiq49Dd) &mdash; content and formatting may not be reliable*
