---
layout: post
title: "Hi group! Hope all is well. I have a problem that I think needs a better look at"
date: March 27, 2018 01:16
category: "Development"
author: "Chuck Comito"
---
Hi group! Hope all is well. I have a problem that I think needs a better look at. I've posted about it before but have always assumed it was something I might be doing. Here's the scenario...  ON OCCASION, movement will slow to a crawl on the x and y. This appears to happen intermittently when I change speeds within laserweb. For example, I'll have a job running at say 150mm/sec. I might cancel that job for one reason or another and return to my origin or maybe not. It doesn't seem to matter but, if I change the speed to 200mm/sec it defaults to something terribly slow (I'm guessing 10-20mm/sec). This behavior is consistent in laserweb and lightburn so that got me thinking it might be the firmware?? I'd like to test and find out what is the root cause but I don't know where to start. I thought some of you smart folks might be able to point me in the right direction to find the culprit. Please feel free to ask questions and I'll do my best to answer. I don't think I'm the only one that has experienced this. So for hardware I'm running a very custom K40 with smoothieboard. Looking forward to your input. Thanks!





**"Chuck Comito"**

---
---
**j.r. Ewing** *March 27, 2018 05:08*

Can you post the g code sniper where this happens?



Decimal point in right place?



F200with no decimal = F .2  or is it just eating so much code that’s all the faster it can process the data?





Does anyone know how many blocks per second this board can read?And whats in the look ahead buffer. Is there a code to turn on/look ahead? 



Is your software converting arcs to

Line segments


---
**Wolfmanjm** *March 27, 2018 10:48*

You really have not provided enough info to debug. However You may not be aware that feed rates are STICKY. There are two feedrates one for G0 and one for G1, both are modal and stick for the duration until reset. G0 in particular is an issue as the gcode produced by LW etc do not specify the feedrate for G0, so if you were to jog then that may set the feedrate for G0 to be very slow then the gcode you later run will have very slow G0 feedrates. The feedrate for G1 is usually specified in the gcode file and will eventually get set.




---
**Chuck Comito** *March 27, 2018 13:28*

**+j.r. Ewing**, I'm not sure what you mean by "g code sniper" but I'd be happy to do it if it helps. As far as the rest of your post I am simply using laserweb to create my g code. The code is created and I run the job. The slowness I refer to is not in the g code. I know this because the job will run as expected at first. If I stop the job or make a change in speed after creating the g code is when this issue arises. **+Wolfmanjm**, I was not aware that the feed rates are "STICKY". I'm not sure what this means.  I do not use the console to enter any commands so this is happening on it's own. I could see that if I made manual changes I could perhaps enter a G0 instead of a G1 or vise versa.  In a nutshell, I create my file in Adobe Illustrator, open the svg in laserweb and create my job. I use the move commands in the UI to get my laser where I want it. I will usually clear the origin once in position. Then I generate the g code from the perameters I've selected and run the job. This issue appears randomly when I stop the job and rerun it, or when I change a speed setting within the LW UI or when I make a Z axis movement. I can't pinpoint the exact when but I'm trying to duplicate it to know what takes place prior to it happening.




---
**Wolfmanjm** *March 27, 2018 14:00*

that is the issue, you need to understand gcode really it helps :)

When you jog it sets the G0 feedrate if you jog Z it sets a slow feedrate, that sticks when you then run the gcode file which does not set the G0 feedrate, so X and Y will then move at the slow z feedrate that was set when you jogged Z. To solve this you can put something like this at the top of your gcode files... G0 F5000, which will set the feedrate for G0 to 5000mm/min (of course you need to set this to what your machine can actually do for seeks rate). Sticky (or modal) feedrates are standard gcode. However in the standard G0 is meant to go as fast as the machine allows so allowing a feedrate on G0 is a smoothie "special" and is not totally correct as per the NIST gcode standard.


---
**Wolfmanjm** *March 27, 2018 17:20*

also make sure you are using the very latest edge firmware.


---
**Chuck Comito** *March 27, 2018 17:50*

**+Wolfmanjm**​, that makes perfect sense. I will try to duplicate the issue and then issue a G0 Fxxxx command and see if it fixes it and report back. Thank you for the suggestion. 


---
**Chuck Comito** *March 27, 2018 18:22*

**+Wolfmanjm** I'll have to check my firmware version. I update my config quite often but not the firmware. 


---
**Chuck Comito** *March 27, 2018 21:45*

**+Wolfmanjm**, during testing today I issued a G0 F9000 and it solved the problem. Now I need to figure out how it's getting this way. Is this something in the way the data in processed? I don't have the vocabulary to ask how the data is sent to and from the controller but I'd imagine it has to do with "what" data is passing through the controller and "when" it's passing through and what is overwritten and what is not during post processing. I could be totally off here... 




---
**j.r. Ewing** *March 28, 2018 00:21*

Allowing a federate on a rapid move is something I would love on some of the machines  I setup and program.  I don’t see a rapid over ride 1,5,25,50,100% on any printer.



But what posts can support it and use it. 


---
**Arthur Wolf** *March 28, 2018 11:36*

LW option to have custom gcode before each cut. Simply add a XY move at high speed there ( after homin,g ? ) and problem solved


---
**Chuck Comito** *March 30, 2018 16:50*

**+Arthur Wolf**, this method did solve the problem. Just wanted to post back in case anyone else ran into this. Thank you all!




---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/5XSNaT8PvEQ) &mdash; content and formatting may not be reliable*
