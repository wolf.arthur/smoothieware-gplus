---
layout: post
title: "Couldn't find any documentation on using the TMC2130 or TMC2208 drivers"
date: March 28, 2018 07:19
category: "General discussion"
author: "Dushyant Ahuja"
---
Couldn't find any documentation on using the TMC2130 or TMC2208 drivers. Are they supported?





**"Dushyant Ahuja"**

---
---
**Johan Jakobsson** *March 28, 2018 08:28*

As far as i know you cant de-solder the drivers that are on the board and replace them with TMC drivers. The pins doesn't match.

But you could use external drivers as step/dri/enable  are exposed on the board. You'd have to solder pins or wires to those pads and attach to something like this; [reprap.me - Stepper Expander X1](https://www.reprap.me/stepper-expander-x1.html)




---
**Dushyant Ahuja** *March 28, 2018 09:02*

Should have clarified. I'm using a cohesion3D ReMix board. It has support for stepsticks. Was wondering if Smoothieware has software support for the TMC2130 or TMC2208 drivers. Specifically configuration over SPI / UART. 


---
**Wolfmanjm** *March 28, 2018 10:54*

There is support for the TMC26X SPI in the advanced driver control. The newer TMC chips changed the SPI protocol and register size, so you would need to write a new advanced driver control plugin based on the TMC26X one. Serial control is not currently supported, but you could probably add it if you could find a spare uart, or use the soft uart driver.


---
**Dushyant Ahuja** *March 28, 2018 10:58*

**+Wolfmanjm** thanks. So not supported right now. Any chance of support being added soon?



How are you using TMC2130s? I thought I saw a post where you said you had used them. Or are you using them standalone?


---
**Wolfmanjm** *March 28, 2018 11:03*

I am not using them and I don't have any so i won't be adding support for them. If the chip has external jumpers then you should use those. Maybe someone who actually had these could add the support for them. But I wouldn't hold your breath waiting. Maybe you should ask Cohesion3D if they will add the support?


---
**Petr Sedlacek** *March 28, 2018 11:07*

Oops, gonna need it soon, too. I'm cheaping out initially and waiting for an AZSMZ board with stepsticks. Might look into adding support, sounds fairly straightforward from what **+Wolfmanjm** says.


---
**Dushyant Ahuja** *March 28, 2018 11:08*

**+Ray Kholodovsky** weren't you using TMC2130s on one of your printers?


---
**Griffin Paquette** *March 28, 2018 12:37*

**+Wolfmanjm** would you be interested in doing so saying you were provided the drivers/board? I get asked quite often if we support 2130’s like Marlin and it would be awesome if we had the support/stall detection. 


---
**Wolfmanjm** *March 28, 2018 13:07*

**+Griffin Paquette** I would consider it if I was provided boards and paid for the time spent. It is not something I am particularly interested to do as a volunteer. I do agree support for 2130's would be nice and I have tried to get others who have a vested interest to do it, with little luck so far.


---
**Johan Jakobsson** *March 28, 2018 13:25*

I'd imagine, as TCM drivers will be used on smoothieboard V2 according to the blog, that some effort would be put into supporting those drivers in smoothieware?




---
**Jim Fong** *March 28, 2018 15:22*

I use tmc2208 silent stepsticks in my c3dmini k40 laser.  They just plug in, nothing to configure but the current adjust. 



I bought the good ones from digikey.  The eBay ones made a weird clicking sound and died a  early death.  


---
**Dushyant Ahuja** *March 31, 2018 14:29*

**+Alex Skoruppa** I was thinking of using the TMC2130 - Marlin 2.0 has options for configuring it on the fly. Asked the question to understand if the same was possible in Smoothie. Looks like the answer is "not likely"




---
**Wolfmanjm** *April 21, 2018 10:00*

TMC2130 now has a Pull request that needs testing...  [github.com - Add TMC2130 stepper motor driver support by tiagojbalmeida · Pull Request #1315 · Smoothieware/Smoothieware](https://github.com/Smoothieware/Smoothieware/pull/1315)


---
**Dushyant Ahuja** *April 21, 2018 10:03*

Cool. Unfortunately I went ahead with TMC2208 instead. 


---
**Griffin Paquette** *April 21, 2018 13:32*

**+Wolfmanjm** I’ll gladly get a board up and running for testing this.



If this code works, which by the comments it seems it does, do you think we can get direct stallguard support for full feature set use?


---
**Tiago Almeida** *June 27, 2018 12:51*

There is a pull request for TMC2208 that needs testing [github.com - Add TMC2208 stepper motor driver support by tiagojbalmeida · Pull Request #1329 · Smoothieware/Smoothieware](https://github.com/Smoothieware/Smoothieware/pull/1329)


---
**Griffin Paquette** *June 27, 2018 18:56*

**+Tiago Almeida** is it pushed to the official Smoothieware Firmware.bin?


---
**Tiago Almeida** *June 27, 2018 19:02*

Not yet, this needs to be tested first, if you have TMC2208 drivers, test it with my PR code and provide feedback


---
**Dushyant Ahuja** *June 27, 2018 19:24*

**+Tiago Almeida** can you share a firmware.bin file. I'm not confident I can compile properly


---
**Griffin Paquette** *June 27, 2018 20:38*

**+Dushyant Ahuja** I am with you. I am an engineer but not a CS guy by any means lol.


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/1Q56cYwtfub) &mdash; content and formatting may not be reliable*
