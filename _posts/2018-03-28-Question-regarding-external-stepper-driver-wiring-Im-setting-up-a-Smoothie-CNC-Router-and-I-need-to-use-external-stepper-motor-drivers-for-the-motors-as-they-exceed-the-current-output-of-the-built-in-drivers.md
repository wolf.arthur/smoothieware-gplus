---
layout: post
title: "Question regarding external stepper driver wiring: I'm setting up a Smoothie CNC Router and I need to use external stepper motor drivers for the motors as they exceed the current output of the built in drivers"
date: March 28, 2018 10:39
category: "General discussion"
author: "Steven Kirby"
---
Question regarding external stepper driver wiring: 



I'm setting up a Smoothie CNC Router and I need to use external stepper motor drivers for the motors as they exceed the current output of the built in drivers. 



I've read the Smoothie documentation, which explains the setup well enough however I'm curious as to how this will be extrapolated for multiple drivers. I need to provide 5V logic power to the PUL+ DIR+ and ENBL+ terminals on the driver, which in the example is pulled from a 5V endstop pin on the board.  How can I scale this up to multiple drivers without using up all my endstop terminals? I need to run 4 drivers which will leave me with only 2 endstop terminals free for actually connecting endstops (min 3 needed).



My thoughts are to use a buck converter to step down 24V from my power supply to 5V then daisy-chain all the PUL+, DIR+ and ENBL+ terminals on all 4 drivers. Is there any reason I shouldn't do it this way? My buck converter is capable of delivering 3A will this be enough for supplying all the logic terminals for 4 drivers? This would then leave me with all my endstops still available. Also I suppose following this logic I could supply all drivers from one endstop pin and eliminalte the need for the buck converter altogether, once again will the current draw be too great for only using one pin? I'm using TB6600 drivers.



Thoughts on my plans would be greatly appreciated.



Cheers,



Steve







**"Steven Kirby"**

---
---
**Wolfmanjm** *March 28, 2018 10:49*

most of these external drivers have opto couplers on the inputs so the draw for each one is probably around 10-20mA, so using one 5V from the endstops and distributed to all the drivers should be more than enough.


---
**Arthur Wolf** *March 28, 2018 11:32*

If you have wo drivers for a single axis, simply wire them in  parralel. Youi only need one 5v pin and you can wire all things that go to 5v to that pin


---
**Antonio Hernández** *March 28, 2018 12:24*

Is there a limit for parallel config ? (number of stepper drivers allowed)  ?


---
**Miguel Sánchez** *March 28, 2018 13:13*

If the step pulse is active on the leading edge you may want to connect the signal to PUL+ and GND to PUL-.  (No need of +5V in this arrangement). You need to check whether or not your driver will work with 3.3V though.


---
**Wolfmanjm** *March 28, 2018 13:54*

using the open collector/drain method is recommended, and the step pulse can be inverted in the config.


---
**Steven Kirby** *March 28, 2018 15:06*

Thanks a lot guys for all your input.



Since I'm not sure that my drivers will run off 3v3 I'll stick with the open drain method as wolfmanjm suggests.  Good to know the drivers only pull a few milliamps and I can use just one endstop pin, it'll make cable management a lot easier!


---
**Miguel Sánchez** *March 28, 2018 16:02*

**+Wolfmanjm** Would an accidental short in the wiring fry the output pin? (by applying +5v to an output pin)




---
**Wolfmanjm** *March 28, 2018 16:03*

possibly


---
**Steven Kirby** *March 31, 2018 11:47*

I'm very careful with wiring into screw terminals. When building my smoothie 3D printer, an accidental short between the + and - terminals of the extruder output on my board blew a fuse on the main input and killed a mosfet. (always twist your wires, Steve!) 



Luckily I was able to resurrect the board with a replacement fuse. The mosfet is goosed but I'm now using the board for this CNC router project and the mosfet isn't required. I've mounted all  my drivers with the screw terminals facing upwards so I can check for stray wire strands.



Hopefully your comment hasn't jinxed me **+Miguel Sánchez**!



I just need to sort out my configuration file today then preliminary testing can commence. Wish me luck!


---
**Steven Kirby** *April 01, 2018 14:35*

Well **+Wolfmanjm** **+Arthur Wolf** **+Miguel Sánchez** initial tests have not been friutful. The magic smoke has remained inside all components thus far so that's a small victory at least. 



When I power everything up the steppers Immediatey engage, the alarm light on the TB660 drivers is illuminated. (according to what I can find on the web this is indicative of either under/over voltage or current situation, I'm guessing under in my case since they remian this way without issue and everything is cool) I'm reading 4.8V coming from the endstop supplying the logic (close enough for jazz right?). I definitely have 24V on the nose coming from my supply to the VCC and GND conncetion on the drivers. 



Probing between the step- dir- and ebl- terminals on the drivers and ground, I see a variety of different voltages ranging between 2-4v but it's not constant for all drivers ie. x driver vs z driver for same terminal eg enbl- is showing different voltage values, I would expect all these to be the same for a given terminal, right?  



Using Pronterface for testing, if I ask any axis to move the steppers disengage but nothing moves. seems like something is backwards. Sending motor off command causes motors to engage and hold. All step dir and enable pins on the controller are set to open drain the firmware, do any of these pins need their logic inverting also?



Any clues gents?



Cheers,



Steve


---
**Wolfmanjm** *April 01, 2018 14:38*

yes you need to invert the EN signal. and maybe the step signal.




---
**Steven Kirby** *April 01, 2018 14:41*

Just reading back through the previous comments **+Wolfmanjm**, I notice you mention the step pulse needs to be inverted in config so that's one question answered. From what I'm experiencing the enable should also be inverted? obviously Dir would only need to be inverted if my motors were moving in the "wrong" direction. I'll experiment with some "!" and see if this solves any issues.


---
**Miguel Sánchez** *April 01, 2018 14:43*

As far as I know, most drivers Enable signal is enabling the drivers while disconnected (or OFF). So Disable would probably be a better name :-)


---
**Steven Kirby** *April 01, 2018 15:13*

Probably **+Miguel Sánchez**! From what I'm experienceing with my setup the motors are engaged when they should be disengaged. However it should be termed, it seems to be the opposite of what I want it to be right now. I'll invert the logic of the enable [disable ;-)] and step pins and see where this gets me.



I've triple checked my wiring so this has to be a config thing. I'm just a bit confused about the alarm status lights on the drivers being illuminated, nothing seems to be melting so I'll not worry about it for now. :P


---
**Steven Kirby** *April 01, 2018 15:36*

Looks as though this solved it! Inverting the aforementioned pins and now the motors are responding as expected. Just need to look at acceleration values as I think X and Y axes are set far too high. Z moves normally, albeit loudly! but X and Y just make a farty grinding noise and do very little. Obviously Z is configured for leadscrew drive on 3D printer and X and Y for belt so I need to look at my acceleration/step per mm values in the config. 



What microstep settings would you reccomend for A leadscrew driven CNC? As I understand higher microstep equals smoother movement and less resonance at the expense of torque. I'm set at 1/8 currently.


---
**Steven Kirby** *April 01, 2018 22:01*

Well all is well with the steppers now. I've adjusted the steps/mm and acceleration speeds and all of the motion control elements of the build are sorted. However..... 



I've configured the Spindle module to start and stop my spindle via a relay and I have a PWM to analog converter "controlling" the spindle speed. I have a rock steady analog voltage coming out of the converter. Testing a range of speeds sent from the host via M3 commands and the voltages output from the converter are roughly proportional to the specified speeds. As you point out in the spindle module documentation **+Arthur Wolf** There's a bit of a hump in the middle but the voltage generated for a specified speed remains steady. But........



The VFD's frequency is all over the shop!! The only time I get constant speed is when the requested frequency is at the max or min of the allowed range. ie. if I ask for M3 S24000 I'll get a steady 400Hz at the VFD however if I specify M3 S12000 I get a huge frequency fluctuation from somewhere in the high 100s to mid 200s, It's hard to tell because the displayed frequency is changing so fast it's incomprehensible! I know this isn't an issue with smoothie, as confirmed by my readings from the PWM to analog converter and I've scoured the settings in the Chinglish instruction manual but I can't spot any settings that would help me out, any clues what my problem could be peeps? Did I just get a lemon with the cheap Chinese VFD I bought?



I've scoured the internet for solutions but despite some other people having minor frequency fluctuations (nothing as dramatic as mine from the videos I've watched) nobody appears to have a solution! :'( Looks as though I might just have to resort to manual control for the time being until I can afford a better VFD. if anyone has any clues though I'm all ears. 


---
*Imported from [Google+](https://plus.google.com/112120819276379662423/posts/izZvhh8KE44) &mdash; content and formatting may not be reliable*
