---
layout: post
title: "Has anyone had the Simplify3d issue lately?"
date: March 29, 2018 16:45
category: "General discussion"
author: "Brian Zellers"
---
Has anyone had the Simplify3d issue lately? (The issue where the firmware becomes unresponsive.)



I have had it happen to 3 parts in the past 2 weeks, on 2 different printers. I tried the gcode correction tools on the smoothieware site, but that didn't fix the issue. The parts stop at the exact same spot everytime and using pronterface and silc3r fixes the issue. So I can only conclude that its the same issue everyone had in the past.





**"Brian Zellers"**

---
---
**Arthur Wolf** *March 29, 2018 16:54*

Haven't heard of the S3D bug for two years now ( since they fixed it on their end ).

Maybe they made a new one ?


---
**Brian Zellers** *March 29, 2018 17:06*

Ya, it just randomly started happening to me. One printer has been running for over 6 months now with no changes made recently. And the other is about a month old.



When it happens the firmware unresponsive message will pop up in the command tab, but then it will run a few more lines of code. And then the message keeps popping up with fewer lines of code ran in between each time, until it eventually locks up permanently.


---
**Arthur Wolf** *March 29, 2018 17:09*

Can you try another computer ( with a file you know willl fail in S3D ) ?




---
**Brian Zellers** *March 29, 2018 19:35*

I have tried different computers. I have 3 computers with simplify on them. The 2 computers that are having this issue both have version 4.0.1 of simplify while one has version 4.0.0 and doesn't have the problem (so far).


---
**Arthur Wolf** *March 29, 2018 19:57*

I think you need to start using the computer with the version of s3d that doesn't exhibit the problem ( yet ). Then either nothing happens in case you'll know you need to tell s3D one version is buggy, or something will happen and then we can start working on this again.


---
**Brian Zellers** *March 29, 2018 20:00*

Ya that's my plan. I need to check to see if simplify has previous versions available on their site


---
*Imported from [Google+](https://plus.google.com/110884982661099816710/posts/RMisxY3mtxg) &mdash; content and formatting may not be reliable*
