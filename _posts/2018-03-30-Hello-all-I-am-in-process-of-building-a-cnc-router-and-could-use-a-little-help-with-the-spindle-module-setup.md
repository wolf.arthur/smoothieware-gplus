---
layout: post
title: "Hello all, I am in process of building a cnc router and could use a little help with the spindle module setup"
date: March 30, 2018 00:17
category: "Help"
author: "Chuck Comito"
---
Hello all, I am in process of building a cnc router and could use a little help with the spindle module setup. I bought a cheap spindle and power supply from Amazon. The image says for use with mach3 via 2 pins on the supply (No data sheet though). I went to read the smoothie spindle pages and I think I just confused myself even more. Based on the attached image,  can anyone help point me in the right direction to set up the spindle to be controlled by smoothie? I think I am going the pwm to analog route?? 

![missing image](https://lh3.googleusercontent.com/-dUDTemFVXRY/Wr2CDgEw0FI/AAAAAAAAL5w/PNbo3sQ6ZHYZkWrCYXrtIBvHdRsvpqZaQCJoC/s0/20180329_200913.png)



**"Chuck Comito"**

---
---
**Don Kleinschnitz Jr.** *March 30, 2018 02:42*

**+Chuck Comito** can you point me to the setup you bought?

You can use a PWM to analog 0-10V converter running from the same port we have our K40 connected to.


---
**Chuck Comito** *March 30, 2018 03:00*

Hello **+Don Kleinschnitz**​, this is the spindle: [amazon.com - CNC Spindle 500W Air Cooled 0.5kw Milling Motor and Spindle Speed Power Converter and 52mm Clamp and 13pcs ER11 Collet for DIY Engraving - - Amazon.com](https://www.amazon.com/dp/B01LNBOCDA?ref=yo_pop_ma_swf). I also ended up buying this today: [https://www.amazon.com/dp/B06XB6J4FV/ref=cm_sw_r_em_apa_TFAVAb2GZEZDV](https://www.amazon.com/dp/B06XB6J4FV/ref=cm_sw_r_em_apa_TFAVAb2GZEZDV)


---
**Chuck Comito** *March 30, 2018 03:02*

And yes the idea was to do exactly that using the same laser port pin. I pretty much thought I had this build in the bag until I saw the power supply for the spindle. 


---
**Gary Hangsleben** *March 30, 2018 03:09*

I just ordered the same unit from Amazon, so I am interested in what you learn as well.  I will also be controlling with a smoothie board.


---
**Chuck Comito** *March 30, 2018 03:11*

I will be sure to post back **+Gary Hangsleben**​. Don has helped me many times and I'm sure he'll have some insights as well. 


---
**Claudio Prezzi** *March 30, 2018 07:31*

You can use a PWM to 0-10V converter like that: [de.aliexpress.com - 1PCS PWM to Voltage Module 0%-100% PWM Converted to 0-10V Voltage](https://de.aliexpress.com/item/1PCS-PWM-to-Voltage-Module-0-100-PWM-Converted-to-0-10V-Voltage/32799217899.html?isOrigTitle=true)


---
**Claudio Prezzi** *March 30, 2018 07:35*

I would additionaly connect a "spindle enable" mosfet output to the switch input of the spindle power supply.


---
**Don Kleinschnitz Jr.** *March 30, 2018 11:18*

**+Claudio Prezzi** I think that is the same converter that **+Chuck Comito** purchased from amazon above.



Does the smoothieware turn off the PWM when a motor start  Mcode is not active? If so is a motor enable needed?




---
**Chuck Comito** *March 30, 2018 11:51*

**+Claudio Prezzi**​ so I can better understand.. the converter will change speed but not turn the spindle on and off so I would need to use the mosfet to do that function? 


---
**Don Kleinschnitz Jr.** *March 30, 2018 11:52*

**+Chuck Comito** 

I suggest testing your PWM-> Spindle speed controls linearity. This would include the (Smoothies PWM)+ (PWM to Analog) +(Spindle Driver).



I learned the following:

... All the drivers that I tested (purchased online) and the one I am using have very poor speed control linearity.  

... The linearity is caused by the motor drive erroneously turning back on during the end of the PWM cycle.

... Using a rpm meter you can map the actual speed vs programmed speed curve and then fudge it in the Gcode. This seems to be what most are doing. Some have firmware that makes this adjustment in software. For me this is a design defect and needs an improvement.

... Since speed setting is important in a router/mill for proper cutting, surface finish and tool cooling I decided to design a spindle driver that was more linear (I hope).



Your driver may behave better, I have not tested that model and would be interested in your results.



I am using my RioRand driver on my OX, meanwhile testing the new design.



<b>Background:</b>



[https://plus.google.com/+DonKleinschnitz/posts/AG6gZ7Lxhs7](https://plus.google.com/+DonKleinschnitz/posts/AG6gZ7Lxhs7)



[https://plus.google.com/+DonKleinschnitz/posts/jX6ZjgK9tgi](https://plus.google.com/+DonKleinschnitz/posts/jX6ZjgK9tgi)



[plus.google.com - #OXSpindleDriver This is an update on the "OXSpindleDriver" Caper Backgroun...](https://plus.google.com/+DonKleinschnitz/posts/M9xbk1JXryM)



[https://photos.app.goo.gl/9rzSbxexJqOGQJxF3](https://photos.app.goo.gl/9rzSbxexJqOGQJxF3)






---
**Don Kleinschnitz Jr.** *March 30, 2018 11:54*

**+Chuck Comito** for another purpose I tested this PWM to analog converter and found it had decent linearity.

[amazon.com - Amazon.com: MagiDeal PWM 0-10V Digital to Analog Signal Tranformer Converter Module MACH3 PLC: Home Audio & Theater](https://www.amazon.com/gp/product/B00UIZKYAS/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)




---
**Don Kleinschnitz Jr.** *March 30, 2018 12:05*

**+Chuck Comito** on my ox I do not have a spindle enable since the TinyG turns off the PWM with a M05 command and on with M03-M04.



I did add a switch in the 48V supply to the motor driver. When I screw up the Gcode and end up somewhere I shouldn't be I can shut down the "pointy end" to minimize damage :). I have used this switch a lot :(. Its kind of a Estop for the spindle.


---
**Chuck Comito** *March 30, 2018 13:28*

I've been reading your posts **+Don Kleinschnitz**, very interesting results. I'm not sure if this gets me anywhere though lol. I'm learing though! I saw you mentioned that you need a PCB layout guy from time to time. I'm very good at this. Let me know if you'd like something done. In the meantime, I'm trying to understand this whole thing. I think I've got my head around it but as I can see it is not as straight forward as I had hoped. I might be doing the spindle on/off and speed manually for a while...


---
**Don Kleinschnitz Jr.** *March 30, 2018 15:54*

**+Chuck Comito** 



<b>First:</b> I may later take you up on the PCB layout offer. I can hack together a board but its not a core skill. I plan to build one speed controller for myself and was willing to open source the design if someone wanted to make them for others. My current design is through hole and can be made much smaller with SMT, which I do not plan to undertake.

My speed controller will interface with 5V and 3V PWM signals directly without 0-10V analog conversion.



<b>Second:</b> Attached is a sketch of what I think you are trying to do. I looked at the parts you have chosen and think you will have some challenges with the interfaces because, as usual, they are poorly specified. This is why I trace these designs before using them.



The KNACRO pwm to analog suggests its needs a positive PWM of 0-5v but the input circuit is unknown, looks like an opto-coupler. My drawing shows a pull-up on the current common drain configuration from the smoothie. That approach needs a  5v supply which may be inconvenient. This module also requires 12VDC to run. Depending on the actual connection to the input no pull-up may be necessary.



The spindle controller wants a 0-10V signal but it is unclear what the output of the PWM-Analog converter looks like. I found one reference that suggested it was a LM393 with a 2200 ohm pull-up. There could be a mismatch to the spindle driver but only testing will tell. Its unclear how the pot works if the PWM input is used?



Of course you will have to config the smoothie with the correct PWM polarity and frequency. 



Tracing the input and output of these devices will help bring clarity to the interconnections and may help to simplify the supply voltages and interconnects.



<b>Some hints on wiring:</b> 

...do not run your motor drive wiring in or near a harness that has any digital signals like end stops :)!

... Run all low voltage signals like PWM and end-stops through twisted pair or shielded wire.



As always let me know if I can help.







![missing image](https://lh3.googleusercontent.com/lyNDjoR3bwQ25YWjpXEKmeNkOBquhrQDcLvhlnUzobPK_YRIP3RuF5RSXowWiak-yp2D23S6J9ydwn1kOtpHYuHIS7LhbvJ36em-=s0)


---
**Claudio Prezzi** *March 31, 2018 08:53*

**+Chuck Comito** The spindle driver in your picture has a switch input (that is short out). This can be used to switch the spindle on/off either  by a manual switch oder by a mosfet from the smoothieboard.

When you use mosfet you can safely enable (M3/M4) or disable (M5) the spindle with gcode. This way you reduce the risk of false signals (spikes, ground differences) on the PWM line that could start the spindle when it shouldn't.


---
**Claudio Prezzi** *March 31, 2018 09:39*

**+Don Kleinschnitz** You could use the psu of the smoothieboard to drive the 0-10V converter. And instead of the manual spindle on/off switch, I would use a mosfet output of the smoothie board. They are open drain mosfets, so they connect the output to ground when enabled. 

For security reason, I would connect a mechanical switch in the 110V line to the spindle driver (just in case the electronics for on/off has a bug). 


---
**Don Kleinschnitz Jr.** *March 31, 2018 12:50*

**+Claudio Prezzi** 



<i>This way you reduce the risk of false signals (spikes, ground differences) on the PWM line that could start the spindle when it shouldn't.</i>.... see your point, I never had this noise problem. Then again my PWM is open collector, opto isolated and shielded. If your PWM is not clean then it will cause other problems when running anyway.



<i>You could use the psu of the smoothieboard to drive the 0-10V converter.</i> Yes, that's a good idea.



<i>And instead of the manual spindle on/off switch, I would use a mosfet output of the smoothie board. They are open drain mosfets, so they connect the output to ground when enabled.</i>  I use the switch because I find that by the time I get to the console to turn off the motor from an Mcode the damage is done. I put the switch in the DC power output because switching the AC input still leaves the 48v caps to discharge before all the energy is gone and the motor stops. The switch is on a panel directly in front of me. 



![missing image](https://lh3.googleusercontent.com/ElAflmgTYXtLkt_x7xjPzGS1eY0EddwaWebz0WrVy_XpdtMPJ0O_Jd7q7BMfKfmA4TLmsW4B_CdYhNbCph90Xm4uk6BvfXIxC0c4=s0)


---
**Chuck Comito** *March 31, 2018 13:26*

**+Claudio Prezzi**​, I thought this was the case but doesn't switching on the mosfet also apply whatever voltage I have on it?


---
**Claudio Prezzi** *April 01, 2018 21:59*

**+Chuck Comito** The open drain mosfets are like a switch in the ground line of the load. The plus pin of the mosfet connector on the smoothieboard is directly connected with the corresponding power input, but you don't need it. 

You just need to connect the gnd pin of the spindle driver with the gnd of the smoothieboard and the positive pin of the switch input with the mosfet out (negative pin) of the smoothieboard. 

If you don't know which is the positive pin of the switch input, it's the one which has a voltage (against gnd) when the spindle switch is off (or short removed in your scematics).


---
**Steven Kirby** *April 01, 2018 22:17*

I'm also struggling with controlling my spindle speed through PWM to analog conversion. I have a separate board interpreting PWM signals from the smoothie and forwarding analog voltage to the VFD. Despite having a nice steady voltage coming out of the converter for a specified speed; the frequency output on the VFD is fluctuating wildly (by the best part of 100Hz.). Any ideas what my problem could be? Bad VFD or noisy converter maybe? I have no scope to check the cleanliness of the signal from the converter but multimeter shows a steady voltage. Could this be a settings issue on the VFD? It's a YL620-A 1.5 KW 220v I've done my best to read the terrible manual but i can see no settings that would help. Since you guys seem experienced with this, got any clues for me **+Claudio Prezzi** **+Don Kleinschnitz**?


---
**Don Kleinschnitz Jr.** *April 02, 2018 03:59*

**+Steven Kirby** how are you connected to the VFD? Can you post a wiring diagram. 

Have you tried using a power supply to provide an analog voltage to the VFD to eliminate any problems with the PWM to analog converter.



BTW the setup on these VFD's are a challenge :(.


---
**Steven Kirby** *April 02, 2018 12:52*

**+Don Kleinschnitz**, Here's a fantasic MS paint rendering of what's going on with my wiring:

![missing image](https://lh3.googleusercontent.com/iMlgVBnnmofl7qqpBXuqrlSNKwDVl_PVi3v0tEzthqrp2398oALg8lO4C0oSGN8Jdbt5aEYy39bPYpV3DM4CT2v26PQ--Dp-urE=s0)


---
**Steven Kirby** *April 02, 2018 13:13*

I'm pretty sure the wiring is correct and everything functions as it should aside from the wildly fluctuating frequency. M3 S12000 will cause the relay to switch on the spindle and set it's frequency via the DAC however the frequency is completely unstable.  



After some more digging on the web last night, I discovered that beneath the control panel there's a series of jumpers to be set for different input types. J1 J2 & you guessed it J3. No mention of this in the "instructions"! There is however a cryptic diagram of what the jumpers (J2/J3) do in different positions. One config is for 0-10v inputs, another for 0-5V and one for interpreting current 0-20mA. This is about all I can figure out as the pins aren't labelled and the diagram only shows the configuration of one jumper (I'm assuming both must be set in the same position for the corresponding function) Right now they're either set to both bridging pins 1+2 or 2+3 which would be the config for either 0-10v or 0-20mA, but it's hard to day as the pins arent labelled. If I take the orientation of the text as reference , their current configuration would be both J2 and J3 are bridging pins 2 and 3 which would mean it's in current sensing mode, which could well be my problem. I've not measured the current coming from the DAC but the voltage is steady, maybe the current is fluctuating and this is causing my problems. Thing is I'm not sure about the jumpers and I like to be sure before I set physical things like this, helps keep the magic smoke in. Any experience of these jumpers **+Don Kleinschnitz**?



Cheers,



Steve


---
**Chuck Comito** *April 02, 2018 14:57*

**+Claudio Prezzi** Like so?

![missing image](https://lh3.googleusercontent.com/lKXF0XCj1PccE_jqzJiF_dansvuKqpHw4OfK7NZ5x9mTVzp5561y8YBTCjlTVPHmviiltwtrdd-IWYEgnQ5khFNRY7oksRs5SdU=s0)


---
**Don Kleinschnitz Jr.** *April 02, 2018 15:20*

**+Steven Kirby** I dont have a VFD on my system but have used them before. You do NOT want it to be in the current loop mode (0-20ma).


---
**Claudio Prezzi** *April 02, 2018 16:47*

**+Chuck Comito** Yes, that's what I tried to explain. :)

Just make sure you use the separate PWR IN (jumper removed) for the small mosfets and only supply 5V to it.


---
**Claudio Prezzi** *April 02, 2018 16:51*

**+Steven Kirby** I would first try if the VFD works correctly if you connect a pot to the 0-10V input (pot side contacts to gnd and 10V, middle contact to the 0-10V input). But make sure you are in 0-10V mode.


---
**Steven Kirby** *April 02, 2018 17:32*

**+Don Kleinschnitz** ,  I know I don't want it in that mode. It's just difficult to tell what mode it's in from the diagram, I'll upload some pictures so you can see for yourself why I'm having trouble deciphering what mode it's in. Measuring the current output from the DAC It's spitting out ~60mA so way above the 0-20 that the circuit is designed to sense, I can only assume it is jumpered correctly for 0-10 v at the moment, if it was in current sensing mode I'd just see 400Hz constantly. (or something would break.) :P So it just looks like I have a sucky VFD or a problem with noise. I'm just using normal wires for the connection from DAC to VFD no shielding on the cables so this could be an issue. I could cut a short length of CY cable from my spindle run and use that to see if it stabilizes things. Maybe an electrolytic capacitor in the mix could clean things up but I wouldn't know how to hook this in or what rating to use. 



**+Claudio Prezzi** Unfortunately I have no spare 10k pots in my parts bin or I would have given this a try already, guess I could just hook the 10v from the vfd straight to VI1 and see if that gives me the expected 400Hz, which it probably will, everything is stable at that end of the scale. Short of moving those jumpers I'm at a loss on what to try next. I'm just concerned that if I move them over and it does switch to current mode the circuit won't handle the current from the DAC very well and could damage something. Should I risk it for a biscuit? Photos of the jumpers to follow.........


---
**Steven Kirby** *April 02, 2018 17:38*

Here's the diagram, but notice the orientation of the text on the jumpers, they're at 180 degrees to the diagram and there's no labeling on the pins to denote number!

![missing image](https://lh3.googleusercontent.com/OmDRh_v2jsqXWLEPEfGb1vKUoG03DatI4wWqf3qvDSCS9wQhbBZ-aoRvILyNlORqUIpqLE2p2yrP8r1ysEsuQYRZNwMhVVxdqg0=s0)


---
**Steven Kirby** *April 02, 2018 17:39*

and here's a closer look at the jumpers:

![missing image](https://lh3.googleusercontent.com/wT-A1dwCppy0h3hscm4JhZC35GsXUGoTUeXi_qCEGNJNfLj29iLN1RQi9W1tRpmK7XZOqyCDwZlqKcjzNqKwUjDpdvo2ysBDTGg=s0)


---
**Claudio Prezzi** *April 02, 2018 18:23*

**+Steven Kirby** I see what you mean. It's impossible to know what the diagram exactly means. I would do what you guess and connect 10V to the input, which should generate the maximum frequency (speed).

What PWM frequency range does your PWM to 0-10V converter accept? The one I linked only accepts PWM signals between 1 and 3 kHz. Default frequency of smoothieware ist 5 kHz (200ms PWM duration). So that param needs to be changed to something like 500ms.


---
**Steven Kirby** *April 02, 2018 18:45*

**+Claudio Prezzi** Cheers for the input, most useful. I've not looked at the config regarding pulse width so that's somethng to try. However I am getting a sensible output from the DAC with the settings I have in the spindle module right now. My DAC is also 1-3Khz input range so it's worth a punt to see if that changes anything.



I did just go on a salvaging mission and find a 10k pot though. The Psychology department at the university I work for were throwing out an old piece of equipment called neurolog, looks like a modular synthesizer. I thought, I can't let those components go to waste. There's some beautiful clicky mechanical switches etc on the modules that you just don't see on modern hardware. I almost felt bad removing the pot from the module I pulled. It was clearly hand made with beautifully soldered components. But hey at least it can get put to good use now instead of ending up in a landfill! I'll see what reults I get with the pot and if it works as it should I think we've narrowed it down to a problem with my DAC or the wiring run between DAC and VFD I still have my suspicions that the VFD is just crap though. I'll let you know how things turn out when I've conducted my tests. Thanks again for your input. :)


---
**Don Kleinschnitz Jr.** *April 02, 2018 20:13*

**+Steven Kirby** if you do not have a pot, as I previously posted, you can use any power source between 0-10v even a 9v battery. This way you  can prove that the VFD still misbehaves with a stable voltage and it proves your problem is the VFD setup not the analog conversion or noise.




---
**Don Kleinschnitz Jr.** *April 02, 2018 20:22*

**+Steven Kirby** have you tried the VFD in manual speed mode controlled from the panel without PWM and did it hold speed?


---
**Steven Kirby** *April 02, 2018 20:26*

Cheers **+Don Kleinschnitz** I have managed to find a pot now ^^ although connected as it should be according to the manual it's not doing anything to the frequency!? I do have a couple of power supplies but they're not adjustable, just generic LED PSUs and an ATX. Although I do have some buck converters so I could rig something up to give me 0-10v using the 12v rail on one of those.  



You weren't kidding when you said these VFDs we a pain to set up! I think VFD actually stands for Very F@#!king Demoralising! 😣


---
**Steven Kirby** *April 02, 2018 20:27*

Yes **+Don Kleinschnitz**  steady as a rock using the potentiometer on the front panel and manual control.


---
**Don Kleinschnitz Jr.** *April 02, 2018 20:45*

**+Steven Kirby** are you using the pot to create a simulated 0-10v signal on the VFD's analog terminal or trying to use it as a speed control dial? Can you show us how you have hooked it up?




---
**Steven Kirby** *April 02, 2018 20:59*

OK **+Don Kleinschnitz** **+Claudio Prezzi**. We have some progress! The pot was working it just wasn't linear so I had to turn it a long way before the frequency changed. And........ wait for it......... the set frequency is stable. So we're back to the problem being in the conversion of the pwm to analog voltage and it's transition to the VFD, at least we're narrowing things down. I'll have a look at the config on smoothie and see if adjusting the pulse width changes anything at this point; though my money is on electrical interference and shielded cable will be my solution looks like another trip to CPC for some CY cable in the morning, If I cut some from my spindle motor run it might end up being to short to reach where I intend to install the control box when this saga is over!



Also, **+Chuck Comito**, apologies, I'm totally hijacking your post here. I hope your Spindle setup is going more smoothly than mine!


---
**Don Kleinschnitz Jr.** *April 02, 2018 21:13*

**+Steven Kirby** is this the correct manual?

[img.banggood.com - img.banggood.com/images/upload/2014/12/SKU161841/SKU1618411.pdf](http://img.banggood.com/images/upload/2014/12/SKU161841/SKU1618411.pdf)



If so....

 it looks like if you are connected to terminal AI1 is not a current loop so that should not be the problem. You have to set it to 5 or 10V. It says use JP2 but I think that is a mistake it should be JP1.



F89 and 91 set the analog gain parameters.



It also says the factory value is in the +10v setting.



Then again the pictures in the manual do not look like the pictures of the circuit card :(.


---
**Steven Kirby** *April 02, 2018 21:27*

Unfortunately this is not the manual for my drive **+Don Kleinschnitz** 😥 it looks slightly more comprehensive than the garbage I received with mine. I'll see if I can find a .pdf of the manual for my drive if you've got the heart to look at it. I've read the manual from cover to cover several times but I can't find any info in there to help with this problem. On the plus side of there's ever a job going for a Chinglish translator I'll be a shoe in! 



I can find a setting for analog gain but unfortunately it's for the analog output, there seem to be many settings on these drives that might actually be useful for tuning but they don't seem to be present on this model. Or, if they are, they're hidden behind badly translated Chinese! 


---
**Steven Kirby** *April 02, 2018 21:30*

Here's the "catalogue" lol :



[https://www.google.co.uk/url?sa=t&source=web&rct=j&url=http://www.frequency-converter-china.com/uploads/soft/170823/1_2143083921.pdf&ved=2ahUKEwjBxf-ixJzaAhUIe8AKHYFhB0sQFjAAegQIABAB&usg=AOvVaw2IsATgQbNYw3oFkGWET_jn](https://www.google.co.uk/url?sa=t&source=web&rct=j&url=http://www.frequency-converter-china.com/uploads/soft/170823/1_2143083921.pdf&ved=2ahUKEwjBxf-ixJzaAhUIe8AKHYFhB0sQFjAAegQIABAB&usg=AOvVaw2IsATgQbNYw3oFkGWET_jn)

[google.co.uk - www.google.co.uk/url?sa=t&source=web&rct=j&url=http://www.frequency-converter-china.com/uploads/soft/170823/1_2143083921.pdf&ved=2ahUKEwjBxf-ixJzaAhUIe8AKHYFhB0sQFjAAegQIABAB&usg=AOvVaw2IsATgQbNYw3oFkGWET_jn](https://www.google.co.uk/url?sa=t&source=web&rct=j&url=http://www.frequency-converter-china.com/uploads/soft/170823/1_2143083921.pdf&ved=2ahUKEwjBxf-ixJzaAhUIe8AKHYFhB0sQFjAAegQIABAB&usg=AOvVaw2IsATgQbNYw3oFkGWET_jn)


---
**Steven Kirby** *April 02, 2018 21:39*

AI1 is the only input of any kind on my drive, it's obviously serving double duty, hence the jumpers to switch modes. Jp1 on mine is an all or nothing thing, removing it must just bypass external control all together. Jp2 and 3 seem to take care of the functions, unpopulated jumpers is 0-5v. Setting both to bridge 2 and 3 should be 0-10 and both to 1 and 2 should be 0-20mA. I'm just not sure what the designation of the pins is. No markings! Logic would dictate that they're set at 0-10v atm. Since the pot worked when I delivered the 10v from the analog output on the drive. 


---
**Steven Kirby** *April 02, 2018 23:53*

**+Claudio Prezzi** It looks like you may have been on to something with the PWM period suggestion. I just changed the value which was set a 1000 (I'm presuming they're milliseconds) down to 500 since 1000 would give me a frequency of 1000Hz and that's on the threshold of being out of range. If I'm getting this, 500ms gives me 2kHz slap bang in the middle of the range.  



Testing now and the range of the fluctuations has halved (I see a pattern here). I'm betting the lowest period my DAC can interpret will give the best signal It's still unusable as is but I feel I'm getting closer to the solution here. Going to try 340 and see what impact that has. It's looking like I may need a better converter.


---
**Steven Kirby** *April 03, 2018 00:22*

At 340 fluctuation is down to about 4Hz so about 1%. Much better than the 25% I started out with. A  fluctuation of 240RPM, would this be tolerable? Guess it will have more impact at lower speeds.


---
**Claudio Prezzi** *April 03, 2018 07:05*

So this looks like the converter doesn't produce a very stable output. You could try to connect a electrolyth capacitor between the pins (gnd/signal) to equalize the signal some more. Something like 2200uF (>15V) should be ok.


---
**Claudio Prezzi** *April 03, 2018 07:10*

You should also check the PID parameters of the VFD. They determine the behaviour of the regulation loop.


---
**Steven Kirby** *April 03, 2018 11:07*

Thanks **+Claudio Prezzi** I'll look into these suggestions. Would the capacitor be inserted at the DAC output terminals or the inverter input terminals?



I don't think the PID parameters will be relevant as I'm conducting my tests without a load (i.e. the motor) attached to the vfd output. So there's no feedback for the VFD to make adjustments for. I could be wrong here, I understand the purpose of PID loops but the mechanisms are a bit over my head.



Another thing I noticed last night, the converter I have is the magideal one **+Don Kleinschnitz** mentioned above. It's only supposed to accept PWM inputs of 5 or 24V and smoothie's logic is 3.3V. Could this be part of my problem? I'd assume if it was I just wouldn't be getting an output from the converter full stop. Although if the gate isn't being triggered by every pulse due to being on the threshold and slight fluctuations in voltage, it stands to reason that the output wouldn't be stable. Unfortunately I've no logic shifters to test this theory and it'll be the end of the week before I can get my hands on one. I might be able to build one from parts I already have, but I'd rather not waste my time figuring that out if I'm off the mark here. Thoughts?



I'll go and see if I can dig out a suitable cap and give that a try for now. Thanks once again for all your suggestions.


---
**Don Kleinschnitz Jr.** *April 03, 2018 11:15*

**+Steven Kirby** 

<i>..as I'm conducting my tests without a load (i.e. the motor) attached to the vfd output..</i> 



....you do not have a motor connected?  If not how are you measuring the RPM?



I don't think testing this without an inductive load can be trusted. The parameters that the VFD comes with are likely setup to handle the characteristics of a motor and its inductance which is part of the feedback path. 



BTW: You can use a open drain mosfet configuration with a pull-up to 5v to get the right voltage for the converter input. You do not need a level shifter.


---
**Steven Kirby** *April 03, 2018 11:59*

**+Don Kleinschnitz** I'm not measuring the RPM, that's how. 😋 



From what I gather this is an open loop system anyway, if it was closed loop the vfd wouldn't run, at least not for very long, without feedback from the motor. 



When I initially experienced the frequency fluctuations (you can clearly see these from the vfd display no need to measure anything) I connected the motor, following your above logic, to see if the problem was due to the absence of a load. Unfortunately it changed nothing. The sound from the motor was akin to that of a car on a learner driver's first lesson. You could audibly hear the RPM fluctuate massively, I'm betting with the ~1% fluctuation I have now it wouldn't be audible at all, but it will probably manifest as reduced cutting quality I'd guess. Also, if I run the vfd with no load but from the manual controls the frequency remains stable, so I think my problem lies elsewhere. 



I have some mosfets resistors and diodes kicking about that I should be able to use to make the voltage conversion. I know from my research that there's a number of circuits that will do the job, wouldn't know which one to pick though. Question is will it help? I'm barely even competent with electronics so it'll be taxing for me to figure out. I'm hoping the capacitor will be the solution but I don't have the right value here so it'll be a trip to the shop for that. If you've a suggestion on suitable mosfet and resistor values (and possibly an easy to follow diagram for an electronics n00b on how to build the thing) it would be greatfully received. 




---
**Don Kleinschnitz Jr.** *April 03, 2018 12:19*

**+Steven Kirby** below is a diagram that includes the Magdeal converter and smoothie.



The converters input is an opto-coupler so connecting to it is easy and you do not need any more parts if you hook it up like below.



You will need to use an open drain mosfet on your controller that is PWM capable.



It may solve your problem especially if you are driving it with 3V logic on the D+ line. However, I think something else is amiss. All you can do when docs are this bad is keep trying stuff :)!



Using 3V PWM will probably work but not be very reliable. The K817 must be driven to saturation to work properly and 3V logic probably is marginal.



......

I thought these VFD's were closed loop but I have never cracked one open so I'm not sure. I am pretty confident that the VFD parameters need to be tuned to the motor, that is why folks have a challenge getting it right.



BTW how are you generating the PWM?

![missing image](https://lh3.googleusercontent.com/uEMNTT1lcnnR9xIPKkr_lLBX8nNkij8QmH66_gDTOQlVAgOxBbwHND9NprnC1AdG8NwH7iKROQCATEcPg5iHZsdMtpCJ_tloe_tM=s0)


---
**Steven Kirby** *April 03, 2018 12:58*

Ok, you may be on to something here....



The pwm is coming from one of the header pin breakouts on the motion controller, not the mosfets that you would ordinarily use to drive the heaters and fans on a 3d printer, one that might be used to communicate with an LCD panel. 



I have the jumper on the magideal converter set to 5v as I was under the assumption that this was to be set for the voltage of the pwm, not the voltage of the supply. If I'm understanding your diagram correctly it's for the supply voltage? The reason I arrived at this conclusion is because the info on the eBay listing says the input power to drive the converter should be between 12 and 30 volts, so logic would dictate that the jumper is for the pwm voltage. 



So my suspicions could be correct about the 3v pwm just not being "loud" enough for the converter to "hear" properly would using the mosfet outs on smoothie and changing the config to suit solve this? I assumed they would also just output 3.3v in this config.



I think the even basic configuration of frequency curve parameters and motor ratings for these spindles and VFDs are enough of a challenge with the provided docs. Took me a couple of days just to get the motor running. Many people have burned up their motors form not adjusting the minimum frequencies set on the drive at the factory, which is too low for the air cooled motors. 



So you think if I set the jumper to the 12-24v config on the DAC and use a mosfet output on smoothie this could work? 


---
**Don Kleinschnitz Jr.** *April 03, 2018 13:26*

**+Steven Kirby** 

I am guessing that on the Magdeal you connected the 3V PWM signal from the controller to the D+ and the D- signal to ground ....Yes? This will work..... just not work reliably.



In the drawing think of the D+ as the supply voltage for the LED in the opto-coupler. The Mosfet acts like a switch that grounds the LED in the opto-coupler turning it on. Of course the LED is switching on/off at the PWM rate.



Without reading back through the posts did I recall that you measured the output of the Magdeal and saw a stable DC value??? If so its doubtful that the converter is at fault?



If you have a stable DC voltage between 0-10 going into the VFD you should have a stable frequency & drive at a specific speed and its not likely the fault of the converter. 



Then again noisy signals can make all this difficult to troubleshoot!



I assume you do not have an oscilloscope?



...........................



If it were me I would redo tests of each subsystem standalone with known stable inputs.



<b>1). To test the converter:</b> you should be able to put a known and constant PWM (like 50%) into the converter and get a stable voltage out (@50% = 5V). Test unconnected to the VFD.



<b>2.) To test the VFD:</b> connect a known voltage on the analog pin of the VFD referenced to ground. Use a battery. Not connected to the converter. Connect the motor.






---
**Steven Kirby** *April 03, 2018 18:56*

Hey **+Don Kleinschnitz**, thanks for your reply. Unfortunately my internetz went down this afternoon and It's only just come back online. The shops will now be closed so trying to deal with my noise issues (if that's what they are) will have to wait 'til tomorrow.



I have tested the converter, as you suspected, I did mention that in an earlier post. I'm fairly confident the pwm from the smoothie is working properly. To calibrate the converter I used an M3 S12000 command from pronterface which, given my settings for motor speed in smoothie's config, should be giving me a 50% pwm. Sensibly, with a multi-meter on the converter output at this pwm % I get ~5.4v out from the converter. similarly at M3 S24000, the top speed of my motor, I get ~10v. All working as expected. Maybe what we're not seeing, due to the meter not being able to keep up, is that the voltage is actually fluctuating very rapidly but on the display we just see the expected 5 or 10 volts. To the more sensitive voltage detection circuitry on the VFD this manifests as the oscillations in frequency I'm seeing.



You are also correct about my lack of scope. Pricey! I'll put one on my to buy list once I've recovered from buying expensive lathes, routers and associated paraphernalia.



I also did a test with the pot I found taking 10v from the inverter and feeding it back through the pot to VI1 on the inverter, the set voltage via the pot produced stable frequency, so I don't think the trouble is at the inverter end in the interpretation of the analog signal.



I'm also not 100% convinced it's noise. Since I don't have my spindle connected during the tests plus the cabling is shielded, it cant be rf from the spindle cable running near the wires from the converter causing issue. I can't imagine what else would be generating noise, any clues? 



Humour me here because this theory is based on a very limited understanding of electirckery. What I suspect the problem is, is the logic signal from the smoothie not being high enough:



As you state the pwm voltage triggers the optocoupler by switching an led on and off rapidly it must reach given brightness to be sensed as a pulse by the converter. the circuit is designed for 5v input, like you say 3.3v is probably just enough to illuminate the led but even a minor fluctuation in the voltage received from the smoothie might cause the brightness to occasionally drop below the required threshold to trigger the optocoupler, these "lost pulses" essentially result in a dip in the frequency on the output resulting in the observed frequency oscillations. 



I think this theory is backed up by my experimentation with the pwm period. by shortening the period and thus increasing the frequency, the train of pulses received by the converter is a much higher "resolution" so the dropped pulses have less impact on the frequency output.



So, my idea for the fix thanks to your wonderful pendoodles is as follows:



1. I set up the DAC for 12-24v with the jumper



2. I provide 24v to the DIN+ on the DAC from my 24V psu and connect DIN- to the pwm capable mosfet on the smoothieboard. 



3. Set the PWM capable mosfet to open drain in the config with a lower case o.



4. set the PWM capable mosfet as the frequency source for spindle speed control in the spindle module config.



5. Conduct a test with the vfd to see if it works, being sure to cross fingers that the magic smoke stays inside everything.



6. Rejoice at the success of the test and finding the solution to my unstable frequency problems.



My theory with all this in case you hadn't already guessed is, even with slight voltage fluctuations at 24v, the voltage will never dip below what is required to trigger the optocoupler on the DAC, resulting in a more stable analog output. Seems logical enough, right?



Just need to confirm that I've interpreted your diagram correctly and this is a legit way of wiring things, does that seem right to you?



Just to make this message even longer because I'm sure you love reading all my waffle! :P I just checked out your video of the ox cutting out your tape holders, cool invention! Gotta keep that tape organised! I've also built an openbuilds design, the workbee which is a variation on the ox designed by Ryan Lock who runs a 3D printing firm here in the UK called Oozenest. I wanted leadscrews on all axes and this design had it. Can't wait to get cutting. I'm also just getting my head around Fusion too. Like you say, steep learning curve but once it starts to click it's so powerful and quick to reiterate designs and refine things. I'm loving it! If you're interested there's some good tutorials at Udemy for fusion made by autodesk themselves and they're free, check them out if you didn't already find them.



Right, I'll shut up now but let me know what you think of the plan and if it's all safe for my hardware.



Cheers,



Steve






---
**Don Kleinschnitz Jr.** *April 03, 2018 20:25*

**+Steven Kirby** 

when you use an external Mosfet <b>you do not need to do #3</b>. The programmable open drain pertains to the processor chip not devices connected to it such as the mosfet. The port also should not be inverted. 

Note: you are wiring from a screw terminal on the edge of the smoothie not connectors internal to the board. This confuses most people.



Post a picture of your mosfet connection so it can be checked.

Also post the config file when you are ready.



*<i>...the voltage will never dip below what is required to trigger the optocoupler on the DAC, resulting in a more stable analog output. Seems logical enough, right?</i>*

The current in the diode and its subsequent amount of light biases the transistor receiver in the coupler. These devices are analog and nonlinear so they do not trigger.  They should be designed to operate on a linear part of their transfer function. If you operate without enough current in the diode they may end up operating in a nonlinear zone and its possible that can change the pulse width of the resulting PWM output. We will see....




---
**Steven Kirby** *April 03, 2018 21:56*

Ok, **+Don Kleinschnitz** Yep I know where the mosfets are so no problem with the connections at least. 



Thanks for the physics lesson though. So it's the current not the voltage that's responsible for the brightness of the diode and it's intensity based not pulse based. So hopefully this will put us in the linear zone of the receiving transistor. Hopefully!   



I've taken 24v to the DIN+ on the DAC and sent the DIN- from the DAC to the -ve terminal of the heated bed mosfet and configured it's respective pin 2.5 to be the pwm generator. The only thing I'm not sure of is the frequency setting as I recall reading smoothie's mosfets can only handle frequencies up to a certain threshold. PWM period is currently set to 340 so almost 3kHz will this be ok? Here's a hastily scrawled pen diagram of the result. not mentioned on the diagram but the jumper on the DAC is now set to 24V.

![missing image](https://lh3.googleusercontent.com/xkwqxarQFoXAP0653b-IRMUIbBj9ekoxBSjjRq72LyuIvZw0FU4qX90LZFQpmXSyjXZXNGNxgiVOz0nFyEq5Mf-dpTOI_CL804c=s0)


---
**Steven Kirby** *April 03, 2018 22:01*

Oh yeah and here's the config, well the bit that counts at least:



##Spindle Module



spindle.enable                                    true   		 

spindle.type                                        analog             

spindle.max_rpm                               24000              

spindle.pwm_pin                                2.5                

spindle.pwm_period                          340               

spindle.switch_on_pin                      1.22! 


---
**Don Kleinschnitz Jr.** *April 03, 2018 23:32*

**+Steven Kirby** 

-<b>So it's the current not the voltage that's responsible for the brightness of the diode and it's intensity based not pulse based.</b>_

...Both determine the current through the led (I=E/R). The voltage on the led has to reach a "forward voltage" to start conduction.The remainder of the supply voltage is dropped across the series resistor. The series resistor limits the LED's current to a level suitable for the application and not to exceed the max rating. 



The diagram looks correct....



The smoothies Mosfets can handle these PWM frequencies.



However the Magdeal's range is "1 KHZ - 3 KHZ", so stay under 3K hz.


---
**Steven Kirby** *April 04, 2018 00:59*

😥 Alas, **+Don Kleinschnitz** this was not the solution I'd hoped for. The frequency undulations persist. 



But hey, on the bright side it's another avenue explored, another question mark removed and another step closer to the right solution. 😀



Oh, and I learned a couple of things so cheers for that. 👍



One thing I did notice and maybe you can explain when I measure the voltage at the inputs on the DAC I'm getting ~13V there's definitely 24V coming out of the PSU so what's the deal there? 



So really, the only things I have left to try are:



A capacitor (This would go at the output of the DAC or the input of the VFD?) 



Shielding the signal cables between the DAC and VFD



Moving those jumpers. The Diagram is indecipherable some things suggest it's in 0-10v mode but I'm not sure, so the best way to find out is to try moving it and see what happens. Only problem is, could it be a risk to provide greater than 20mA to that circuit,if it does turn out that I'm switching it to the 0-20mA mode? 



I'll eliminate the first two but if they don't fix it I'm out of ideas, aside from buying a different converter.


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/KrLVUxPfhse) &mdash; content and formatting may not be reliable*
