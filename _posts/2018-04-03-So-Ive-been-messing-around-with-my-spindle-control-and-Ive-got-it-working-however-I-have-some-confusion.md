---
layout: post
title: "So I've been messing around with my spindle control and I've got it working however I have some confusion"
date: April 03, 2018 22:32
category: "Help"
author: "Chuck Comito"
---
So I've been messing around with my spindle control and I've got it working however I have some confusion.



This works but....



# Switch module for Spindle control

switch.spindle.enable                             true 

switch.spindle.input_on_command      M3 

switch.spindle.input_off_command      M5 

switch.spindle.output_pin                       2.13  

switch.spindle.output_type                     pwm  

switch_spindle_maximum_power         1  

switch_spindle_minimum_power          0.0

switch_spindle_default_power               0.3 

switch_spindle_pwm_period                  400



I don't have any reference to the actual rpm. If I send a M3 S255 it runs at it's maximum speed and M3 S10 runs really slow. Is there a way to convert this via the firmware that I can set the PWM values between 0 and 255 to equal a theoretical RPM between 0 and my spindle's maximum RPM?







**"Chuck Comito"**

---
---
**Steven Kirby** *April 03, 2018 22:45*

There's also a spindle module, that's what I'm using I have it set up for analog control of my VFD but I'm pretty sure You could use it instead of the switch module. It's just sending PWM exactly the same output as switch but it's handled differently by smoothie. I'm guessing it's converting RPM values to corresponding PWMs between 0 and 255.



It has a config for max spindle speed you just set that and your pwm output and you're pretty much good to go. Then you specify the spindle speed with M3 S24000 in my case; for max speed. M3 S12000 for half speed etc, and you don't need to worry about pwm values and how they correlate to the RPM you're after.


---
**Steven Kirby** *April 03, 2018 22:47*

[smoothieware.org - spindle-module [Smoothieware]](http://smoothieware.org/spindle-module)


---
**Chuck Comito** *April 03, 2018 23:38*

I read that but it didn't help. I'm not sure why. I tried the example for the analog module. Reading it seems like I should be able to cut and paste it but change my pin.


---
**Steven Kirby** *April 03, 2018 23:47*

Yeah essentially that's what I did, and don't forget to specify your max rpm for your spindle. 



Also you have to enable the module, that's not in the text on the analog module example settings. You'll need the line:



Spindle.enable              true



Which is a little further up the page in case you missed it.🖕


---
**Chuck Comito** *April 03, 2018 23:49*

**+Steven Kirby** This is what I tried. Do I need anything more than this?

spindle.type                           analog  

spindle.max_rpm                  12000

spindle.pwm_pin                  2.13 

spindle.pwm_period            1000 




---
**Steven Kirby** *April 03, 2018 23:50*

Just to enable the spindle module, see (previous comment) and you should be golden. 


---
**Steven Kirby** *April 03, 2018 23:53*

Oh and your switch pin for switching the spindle on, I'm presuming you have a relay connected for that? 


---
**Triffid Hunter** *April 04, 2018 01:47*

There's no direct relationship between PWM and RPM, you would need to add an encoder or something to measure it


---
**Chuck Comito** *April 04, 2018 02:14*

Hi **+Steven Kirby**​, I do not have a relay. Just turning on or off the pwm signal. I will add the enable comment. I thought it looked funny. I'll post back when I get to it tomorrow. Thanks for the input. **+Triffid Hunter**​, that did occur to me but bring the novice that I am I don't want open that can of worms. Lol.  


---
**Steven Kirby** *April 04, 2018 09:29*

**+Triffid Hunter** Yes I perhaps should have mentioned that but since **+Chuck Comito** was already aware that the resultant RPM would only be theoretical anyway I just gave the most straight to the point answer I could. 



I'm aware that the only true way to have 100% accurate spindle RPM, especially when other variables such as cutting friction are added, is to have a closed loop system that provides feedback to the controller about the current speed; so it's able to adjust it's output accordingly. It's a feature I'd love to see in smoothie actually, I was asking if it was possible a couple of weeks ago as I'm converting a mini lathe to CNC and it's essential for accurate thread cutting. 



Perhaps you can better elaborate on how the spindle module works, I know you're involved in the design of smoothie. But I'm guessing based on your specified max rpm smoothie distributes the pwm values between 0 and 255 across the range between 0 and the specified max RPM. Maybe with a touch of smoothie magic, some sort of fancy coefficient? 



Worms anyone? 😆



Chuck, I hope you manage to get your spindle running to your satisfaction, I think spindle module will work better than switch at least.  I checked out the self-designed machine you're putting it on, props! It looks beefy. I was going to design my own too but since I don't have a machine to cut plates etc. I just took the openbuilds route for now. Just need to get this darn VFD to behave! 


---
**Wolfmanjm** *April 04, 2018 09:52*

switch_spindle_maximum_power         1  

switch_spindle_minimum_power          0.0

switch_spindle_default_power               0.3 

switch_spindle_pwm_period                  400



are not legal switch statements even if they were formatted correctly, check the switch module docs in the wiki.


---
**Chuck Comito** *April 05, 2018 17:31*

So unfortunately I wasn't able to try the recommendations.. I lost communication with my board. Note to others, do not put your controller in your pocket when covered in aluminum shavings and then power on your board... I'm clearly wasn't thinking on so many levels. Lol. New controller on it's way. 


---
**Steven Kirby** *April 05, 2018 18:44*

Oh dear **+Chuck Comito** , "lost communication" is that a nice way of saying you fried it!? Was there smoke and fireworks? Does it still power up? I managed to accidentally fry a board by shorting the extruder heater cartridge terminals, one stray strand of copper is all it takes! Luckily it just blew a fuse on the main power input and liked the mosfet. After some fiddly solder work with a reflow station I borrowed from work I managed to replace the fuse and it works again. Cost me pence to fix and now I'm using it in this CNC build. Winner! Maybe you can rescue your board too? Guess I at least had a clue where to look for the fault, I suppose if yours was covered in aluminium chips you might not know where to start looking. It could be worth having a crack though! 



On a more positive note though, I've managed to sort out my VFD, a capacitor on the DAC output was all it took in the end. It seems I can't hit max speed now though so after all this messing about I'm going to try getting modbus communication going. I don't hold out much hope since this vfd isn't even a huanyang so god knows what the manufacturer's hex codes will be. Hopefully it's not some other deviation from the norm, there's nothing about their implementation in the manual! So yeah, should be really enjoyable! 🙄


---
**Chuck Comito** *April 05, 2018 19:13*

I didn't let any smoke out and I'm only hoping the aluminum was the issue. I found what looked like solder balls near and under the pins of the micro.  I can access the sd card but otherwise I get a serial communication error. It's definitely nothing obvious. I'm able to access some high end equipment and EE's at work and we couldn't sort it there. I'll call it a parts board for now lol. 



What vfd drive are you using? I'm glad you sort of got it sorted but sounds like it's not gonna work in the end?? What rpm will it reach the way it is?  


---
**Steven Kirby** *April 05, 2018 19:55*

Man that's a bummer! How long til your new board arrives? Guessing you're still working on the mechanical side of things if you're getting aluminium chips on your controller? How have you made your gantry plates etc? You got access to another machine? 



The drive I have is a yl620-a, it looks identical to the huanyang drives. So essentially it's a cheap Chinese copy of cheap Chinese VFD! I'm not sure what corners were cut but they certainly skimped out on the documentation. I know the genuine huanyang drives have their own implementation of the modbus protocol which smoothie actually supports, if I'm lucky they copied that too. If I'm unlucky, I'll have to figure out the code and from the looks of things edit smoothie's source code and compile my own version of the firmware with the right codes for the VFD I have. Mission! In over my head really but for the cost of the adapter it takes to try it out, I thought it was with a punt. 



The max speed of my motor is 24,000rpm. I'll probably not run it that fast but when I ask smoothie for top speed the driver is giving me ~350 Hz so ~21,000rpm. Conversely in the middle of the range it's above the speed I'm asking for. I mean it'll work but the feeds and speeds won't match my CAM so it probably won't work as well as it could. There's some settings on the drive I can mess with and I have ordered a tachometer to measure the actual speed so I might be able to fudge it somehow. It'll get me going but the drive will be the first thing I upgrade when I have the funds!






---
**Chuck Comito** *April 05, 2018 20:31*

I next day'd the controller so it'll be here tomorrow. I don't have access to another machine though. This is my first build. I went the smoothie route because I have a 60 watt co2 laser running smoothie. I had a buddy at work water jet the gantry sides but the rest of the design was made to utilize 1/2" thick x 6" wide aluminum bar. This way I could cut it to length and then just drill holes where needed. I wish I had used 2 ball screws and 2 steppers and moved them to the outsides  of the base though. I could have gotten more rigidity out of it that way. Good luck with the modbus. I wish I could help you with it but that's way over my head. 

![missing image](https://lh3.googleusercontent.com/xAXhnHRvllNqDVLVkTOabO-G2YklhbVKBtysI6Q0RkK1MrVnxw_TtjRA6Ct91RvbGs6UwJYK9iKH54-YdRPUx2PcFrem5T5SVow=s0)


---
**Steven Kirby** *April 05, 2018 23:37*

She's a beut **+Chuck Comito**! The gantry certainly looks nice and solid. So you're just driving it with one screw in the middle? It's a legit approach, those ballscrews ain't cheap either! I dare say it won't make much difference to the rigidity. Your frame and the linear rails will be more important for the load bearing. I'd be more worried about only having the torque of one stepper for moving all that gantry around. Looks like you maxed out on the Nema 23s though, so it probably won't be an issue. Your steppers are bigger than your spindle! lol Are they the guys that only draw 1.8A but deliver 2.4Nm and you're driving them straight from the smoothie?



You're pretty much building the machine I wanted to build. I found some reasonably priced ballscrew and linear rail kits from China on ebay. I also have a water jet guy at work that occasionally machines things for me and figured he'd hook me up for the plates.Then I was just going to throw it all on some aluminium box section in much the same configuration you have there. But I found a workbee kit on alibaba with motors for £360(~$550) delivered and thought screw it; I'll not beat that price buying everything separately. I'd just had the headache of designing and building 2 3D printers. Things always end up not quite going to plan with projects, which can be fun (fixing problems etc.) but I just wanted this to be straightforward. (Typically it still hasn't been! :P ) 



Yeah, I'm not looking forward to the modbus thing, It'll probably drive me mad but I'll get there in the end. I hope you manage to get the spindle going once you get your replacement board, let me know how it turns out.



Here's my machine, basically just a leadscrew driven ox, It should be good enough to get me started though:


---
**Steven Kirby** *April 05, 2018 23:38*

![missing image](https://lh3.googleusercontent.com/lfGFGM3-BtIrpig2nosFr29nXuP8xExewrYzWjvBExwfXZzoRSUxKrkv6tMMeIsx20XasR-xJZN55JCS1MrwsgVIvhGC_aYny48=s0)


---
**Steven Kirby** *April 05, 2018 23:38*

![missing image](https://lh3.googleusercontent.com/E0OK6dloZ3TL9j2h53NYH37UqzAc5tbnMvDYiBXNZKpJzhzcUgN5Z6U6bpBbmYQFPMTNfQd_GujDpFLa4EkSFjrZl_TkVU-fLlQ=s0)


---
**Chuck Comito** *April 06, 2018 16:14*

That looks nice **+Steven Kirby**. 

The ballscrews I bought are the typical ebay 1605 series. They're fairly cheap but do the job. Funny enough, my company's susidiary is a ballscrew manufacturer but ours are WAY too expensive.. The steppers I'm running are NEMA 23's but I have external drivers for them. They run at about 4 amps and can deliver 3Nm of force. To get enough torque out of them I had to set the acceleration really low (around 75mm/sec). I'm still not sure if they're big enough. I did really have to think twice about 3d printing parts for this build. Currently the only parts that are 3d printed are the stepper spacers. I wanted it to be as strong as possible. You're right about the headache/design time/cost. Although I bought everything spead out over the year, it's become quite the investment. I didn't want to end up wanting/needing more out of it. I hope to do a major spindle upgrade down the road and be able to cut aluminum. 



I too was looking for a straight forward build. My approach was to do as little fabricating as possible. The entire design was based around materials that fit the design. How nice would it be to go nuts and have everything made to suite though! Maybe one of these days. 



It seems like I always find a bottleneck in the electronics. I'm very mechanically versed but have only been in the electronics field for a few years and always seem to find myself in over my head.. LOL. This is how we learn though.. 



By the way, I'm sure you've seen this if it pertains to your drive.

[dropbox.com - 1_2143083921.pdf](https://www.dropbox.com/s/twz91c8yiffiz37/1_2143083921.pdf?dl=0)


---
**Steven Kirby** *April 06, 2018 17:55*

Cheers **+Chuck Comito**. Yeah it's a bit of a lottery with the Chinese stuff. Most of what I get from there these days ain't too bad, but this VFD drive is a lemon! It actually had a literal "offshore stink" to it (in the words of AvE), it stank of petroleum. I did buy the absolute cheapest combo I could though so, lesson learned!



I take it you work for an engineering firm then? I'm a lab technician at a university myself but we have a pretty decent engineering department and our fellow engineering technicians always sort us out with favours. The perks of the job!



So you literally do have the most powerful nema 23s available. There's another set with similar dimensions that will deliver 2.4 Nm at a measly 1.8A. I was considering these as a way to save on external drivers but then i ended up getting bundled motors with more demanding requirements so external drivers it was. 



I was initially going to build MPCNC, I liked the idea of bootstrapping something more robust from a machine I'd been able to make with my printers. The more I researched things though I just thought I'd cause myself loads of needless expense and time so I dropped that idea in favour of building a more robust machine at the outset. Aside from the drag chains only my spindle mount is 3D printed, It uses hose clamps to get everything snug and tbh it's pretty sturdy. Project number 1 will be to cut a proper mount from aluminium though, which this machine claims to be able to do out of the box (to an accuracy of 0.2mm at the very least). Now I can use the machine to fab custom plates for ballscrews and since I'm fond of modding stuff I'm gonna see how far I can get re-engineering it. Ultimately though, I'll probably end up using it to build something much more robust out of stronger materials. there's likely only so far you can go with V-slot extrusion and polycarbonate wheels.



I guess in industry each part of the design and manufacture is handled by a dedicated team of experts. Kinda impossible to keep up with that as a home-gamer. I haven't the foggiest about coding tbh, I mean, I can mess with other people's stuff edit configs etc. but it's really an Achilles heel for me. I'm not formally trained in any of this though. I learned my craft just having a pop at fixing broken lab equipment, way I see it if it's broken there's no harm in having a go at fixing it, even if you break it more in the process. Turns out I'm not half bad at that! (The fixing that is) My Grandfather was an engineer and an absolute maestro on the lathe by all accounts. Back when this country was actually renowned for it's engineering prowess. I think it's in my blood, as soon as I'd designed my first 3D printer upgrade and saw it working, that was it. I've spent almost every waking minute and probably too many minuets I should have been sleeping, learning about machining. 



Cheers for sending the documentation link. I did receive a paper copy of the "catalogue" XD with the drive. Trouble is it's completely useless. It's badly translated and the descriptions of some of the settings are completely indecipherable. Also no mention of the modbus protocol. There's settings related to it but nothing about the specific codes the drive uses. :'(



Any sign of your new board? Did you get a chance to try out the spindle module yet?


---
**Chuck Comito** *April 06, 2018 18:05*

Sounds like we have similar backgrounds.  My father was an engineer and I've been fixing mechanical and electrical things my entire life. Even before I knew a resistor was a resistor. I also can mess with other peoples stuff pretty good. Funny enough is that most of my friends think I'm a genius.. Little do they know that there are lots of people way smarter than I. LOL. 



My board is currently "out for delivery". I'm hoping its at my front door when I get off work! 



In regards to the "home-gamer", you're right. But good thing we have lots of people online to contribute. 



I have a pretty extensive 3D modelling career behind me and I'm now venturing into CAM. Fusion 360 seems to be the ticket as it's very familiar coming from an Autodesk background.. With any luck, I'll throw the new config file on the SD card after a firmware load and be off and running tonight!! The first thing I plan to machine is a touch probe assembly. I think I'll get the most use out it on the CNC router... At a very minimum I'll build a Z offset plate tonight and get the code sorted.


---
**Steven Kirby** *April 06, 2018 19:20*

Ha, yeah same here. Although in my early years I'm pretty sure I broke more of my toys than I fixed. I get similar genius comments from the better half. I'm like "yeah maybe by comparison to you I am" :P 



I hope your board shows up soon. 



Yeah the internet is a fantastic place! All kinds of information out there! I really envy the kids of today having the internet we have now as a resource. Back when I was a school it was dial-up modems and you were charged by the minute. My dad would let me go online for about 5 minutes and I'd probably spend that time downloading south park audio snippets to customise my windows sounds or something equally retarded. 



Yeah I've been learning Fusion also. Started off with Tinkercad because I had to make stuff NOW! and didn't want to get bogged down by a steep learning curve. Since the new year though I've been using fusion, did a few tutorials to get my head around the basics and now I'm just learning by making designs and figuring out the features I need to use to facilitate my ideas. The parametric penny dropped the other day and it was like an epiphany! Now I can get from an idea to a functional prototype in maybe 2 iterations tops, there was a lot more trial and error with Tinkercad. Fusion is so much more powerful and I know there's a ton more features I've not even used yet that will improve workflow. I've not looked at the CAM yet though. I'm guessing that's going to be a little more involved than using a slicer with all the different types of tooling these machines can use. But, I've built the machine now so I've no choice but to figure it out, probably at the expense of a lot of endmills! At least now I know who's brain to pick when I'm struggling with fusion. ;)



That's something I still need to do actually a touchplate. I figure you can just Macgyver something together out of a washer a crocodile clip and a few bits of wire?


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/J1WTeJTNAyf) &mdash; content and formatting may not be reliable*
