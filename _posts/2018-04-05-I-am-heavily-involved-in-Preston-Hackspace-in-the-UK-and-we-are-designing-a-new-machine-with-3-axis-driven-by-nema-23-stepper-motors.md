---
layout: post
title: "I am heavily involved in Preston Hackspace in the UK and we are designing a new machine with 3 axis driven by nema 23 stepper motors"
date: April 05, 2018 19:15
category: "General discussion"
author: "Andrew Wade"
---
I am heavily involved in Preston Hackspace in the UK and we are designing a new machine with 3 axis driven by nema 23 stepper motors. This cnc machine will use a smoothieboard and be designed to have interchangeable heads making the machine be able to cut, engrave, print many materials exclusively.



We are looking to design a bench top system for a hobbyist, makerspace etc where they can change heads to print filaments, uv resin by piezoelectric jets, router, water jet cutter, etc



We are looking to add linear encoders to the x, y and z axis and wonder if its possible to change the G code movement module to include a feedback loop.



Any suggestions and ideas on which modules we should change, we can write the code ourselves and test and publish it with all our finding and recommendations of hardware.





**"Andrew Wade"**

---
---
**Anthony Bolgar** *April 05, 2018 19:29*

**+Wolfmanjm** is the firmware developer, hopefully he can chime in here.




---
**Wolfmanjm** *April 05, 2018 19:37*

Sorry I have no idea how you would do that. the motion control is very complex and adding a correction based on feedback does not seem feasible to me. but Arthur may have some ideas.


---
**Anthony Bolgar** *April 05, 2018 20:02*

**+Wolfmanjm** Thanks for the quick reply.


---
**Douglas Pearless** *April 05, 2018 21:58*

It is something I too have considered, but have not had time to look at;  I was working on magnetic encoder feedback on Smoothie V1 but other priorities have taken over for the meantime so I am interested in what you achieve :-)



You may want (as an interim measure) to consider [https://hackaday.com/2016/06/01/mechaduino-closed-loop-stepper-servos-for-everyone/](https://hackaday.com/2016/06/01/mechaduino-closed-loop-stepper-servos-for-everyone/) 





Cheers

Douglas


---
**Steven Kirby** *April 05, 2018 23:44*

Small world eh? I live in Preston Lancashire, **+Andrew Wade**, I didn't know we had a hackspace, tell me more. Also, this sounds like a very ambitious machine you're building. Sounds epic!


---
**Andrew Wade** *April 06, 2018 08:29*

**+Steven Kirby** if you goto [prestonhackspace.org.uk - Preston Hackspace :: Home](https://prestonhackspace.org.uk/) and join slack in the menu you can see what we do.


---
**Andrew Wade** *April 06, 2018 08:33*

Thanks for the replies **+Wolfmanjm** do you have a flow chart of how the modules for movement on the smoothieware or link i can read about it. If adding the feedback loop is too complex to build into the code, the other way would be to do it via a stepper driver for each of the axis.


---
**Wolfmanjm** *April 06, 2018 08:37*



no flow chart, the code is the documentation :) the planner, block and stepticker is where it all is. most people doing this use a smart driver that does the closed loop control and takes step/dir inputs,


---
**Roland Mieslinger** *April 06, 2018 10:06*

[https://hackaday.io/project/26053-tool-switching-multi-extrusion](https://hackaday.io/project/26053-tool-switching-multi-extrusion)



works for tools that have to take zero to minimal loads; laser, extruder but not router spindles.

[hackaday.io - Tool Switching - Multi Extrusion](https://hackaday.io/project/26053-tool-switching-multi-extrusion)


---
**Arthur Wolf** *April 06, 2018 10:20*

Smoothie was designed all the way back with such a machine in mind. As far as doing both 3D printing and CNC milling the board should be able to do that no problem ( there is a caveat with the fact that 3D printing gcode isn't compatible with CNC milling gcode, but that's the reprap project's fault not ours, and it's fairly easy to work around ).

My point is : lots of people are working on what you are working on with Smoothie and there shouldn't be major obstacles.


---
**Andrew Wade** *April 06, 2018 10:53*

**+Arthur Wolf** so will be a new binary with the ability to add a feedback system via linear encoder or do we write one to work with the smoothieboard or help contribute to achieving one?


---
**Arthur Wolf** *April 06, 2018 10:55*

**+Andrew Wade** Was answering to the first part of your post about doing a mult-tool machine. About feedback loops, that's not something you want Smoothie to do ( it's not powerful enough ). In industrial machines it's something the drivers do ( with proprietary algos much more advanced than anything we could implement with volunteer labor )


---
**Andrew Wade** *April 06, 2018 10:57*

**+Roland Mieslinger** Thanks for the info but our machine design is where the head is manually swapped you also may have to the change the bed too, like if you were changing from a 3d printer to a water jet cutting system.


---
**Andrew Wade** *April 06, 2018 11:01*

**+Arthur Wolf** so we should look at the feedback loops through the drivers, which we will need to drive nema 23 motors? 


---
**Arthur Wolf** *April 06, 2018 11:02*

**+Andrew Wade** Yes that's definitely the correct way to do it. Stepper drivers with feedback control are only a bit more expensive than normal drivers of similar quality. Look up leadshine. I have this on most of my machines.


---
**Stephane Buisson** *April 06, 2018 13:20*

Try maybe another approach: moving the problem from smoothie to the steppers, smart steppers ->

[misfittech.net - Misfit Technologies](http://misfittech.net/)


---
**Steven Kirby** *April 06, 2018 18:06*

Hi **+Andrew Wade**, I tried to join slack via the menu on your website but the link seemed to be broken or at least the page wouldn't load. Just keeps timing out. :'( 


---
**Andrew Wade** *April 06, 2018 18:32*

**+Steven Kirby** you are right it’s bust, the guy that admins the site is on holiday for three weeks. Send me your email address and I will send you an invite.


---
**Steven Kirby** *April 06, 2018 19:29*

Cheers **+Andrew Wade**. I've sent you an email through the hackspace site so you should have it now. 


---
**Andrew Wade** *April 06, 2018 19:34*

**+Steven Kirby** sorry not received it yet more technical problems, please send your email address to andrew@andornot.co.uk and i will sort


---
**Steven Kirby** *April 06, 2018 19:37*

Sorted! Thanks, **+Andrew Wade**


---
**Roland Mieslinger** *April 08, 2018 08:13*

**+Arthur Wolf** **+Stephane Buisson**

IMHO: these 'smart steppers' hide/solve a problem that happens at a higher layer, path planing and axis acceleration.



Trimanic (e.g. TMC 2130) drivers are able to calculate the 'load angle'; simplified 'how much more torque could be provided'. The firmware could use this information to e. g. slow the machine down before step loss occurs. Compared to closed loop steppers, this would effect all axes simultaneously and reduce print artifacts.


---
**Arthur Wolf** *April 08, 2018 11:50*

**+Roland Mieslinger** Trying to compensate for forces and torque is already what the path planner in Smoothie does. This feature is in the drivers in case you want to offload it to the drivers, but that's not necessary in this case since Smoothie is already doing it. Same thing goes for drivers capable of ramping themselves.


---
*Imported from [Google+](https://plus.google.com/112929738014057369475/posts/9f54YVLKL1W) &mdash; content and formatting may not be reliable*
