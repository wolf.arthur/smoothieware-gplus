---
layout: post
title: "Another question regarding panel interface with the smoothieboard"
date: April 06, 2018 15:57
category: "General discussion"
author: "Andrew Wade"
---
Another question regarding panel interface with the smoothieboard. Is it possible to design a touch screen version of the GLCD panel that interfaces with the smoothieboard? 





**"Andrew Wade"**

---
---
**Anthony Bolgar** *April 06, 2018 16:15*

There is the Nextion HMI with GPIO's display that would work with the TX/RX lines on the smoothieboard. But you will need to program the display, they come blank. MKS makes a touchscreen (uses tx/rx as well)that someone has upgraded the icons too, it is the MKSTFT3.2 and is from the company that has ripped off Smoothieboard/ware without proper open source attribution, so unless you absolutely have to use it, as a matter of principal, it should be avoided. I really only mention it so you can avoid it, and if you have questions why, **+Arthur Wolf** can enlighten you :)


---
**Andrew Wade** *April 06, 2018 16:39*

**+Anthony Bolgar** would  a raspberry pi touch screen be capable of interfacing with the smoothieboard. Regarding the MKS I would never use them, I resell the smoothieboard in the UK and use them on my delta printers, I am not interested in companies ripping off other people’s work.


---
**Anthony Bolgar** *April 06, 2018 16:44*

Not sure if it is possible.




---
**Wolfmanjm** *April 06, 2018 21:20*

**+Andrew Wade** I have written a touch screen based Rpi smoothie host...  [github.com - wolfmanjm/kivy-smoothie-host](https://github.com/wolfmanjm/kivy-smoothie-host)


---
**Anthony Bolgar** *April 07, 2018 02:42*

Will that work with a Pi Zero **+Wolfmanjm**?


---
**Wolfmanjm** *April 07, 2018 08:45*

**+Anthony Bolgar** It may work but would be a tad slow with the touch screen. The RPI3  is a Quad processor and has more memory so is quite fast, and is what I recommend.


---
**Wolfmanjm** *April 07, 2018 10:30*

The Smoopi (as I  call it) connects to Smoothie over USB (or network), and acts a lot like Pronterface would, I took features from bCNC and Pronterface to design it. It has two modes, a 3D printer mode and a CNC/Laser mode. The gcode viewer has things like pinch to zoom and I plan on making more use of the multi touch capabilities of the RPI touch screen.

You cannot get smoothie to control an LCD and touch screen like this directly as the Smoothie is really very limited in its Flash and RAM, and is best to concentrate on just producing a clean step stream to the motors.




---
**Anthony Bolgar** *April 07, 2018 11:25*

I was going to use a Nextion display, but your solution would save me a ton of work. Thanks for sharing with us. And keep the features coming :)




---
**Wolfmanjm** *April 07, 2018 12:20*

AFAIK nextion does not currently work with smoothie as it uses some proprietary serial protocol.




---
**Wolfmanjm** *April 07, 2018 12:22*

I also have a 16gb image which has everything already setup (except for wifi). email me if you need it, the image is compressed to 2gb and needs to be unpacked on a linux system at the moment.




---
**Anthony Bolgar** *April 07, 2018 12:25*

Thanks.




---
**Andrew Wade** *April 09, 2018 22:09*

How does the GLCD screen communicate with the smoothieboard? Is the screen on the GLCD screen driven from the smoothieboard so in effect the GLCD is dumb and only a display?

 


---
**Wolfmanjm** *April 09, 2018 22:18*

May I suggest you read the code? it is an open source project ;)




---
**Andrew Wade** *April 10, 2018 13:00*

**+Wolfmanjm** i will certainty use your raspberry pi touch system for my 3d printers. The new machine i am designing has different functionality for each head  and therefore different screen designs, designing and programming a new touch screen is not out of the question. Could a system be designed like paneldue but designing specifically for the smoothieboard? Thats why i was asking the question.


---
**Wolfmanjm** *April 10, 2018 13:04*

smoopi is similar but uses standard gcodes not invented gcodes no  one else implements. also it runs over usb instead of serial. the screens are fully programmable if you want. and is bigger. paneldue is not currently fully supported as it uses different codes only implemented on the due.


---
**Andrew Wade** *April 10, 2018 14:14*

**+Wolfmanjm** i will try your system this week. Also i will speak with the hackspace members and see what we can do to help, we have graphic designers and programmers and many members interested in raspberry pi's. This looks to be the way forward for our new machine.


---
**Andrew Wade** *April 24, 2018 21:20*

**+Wolfmanjm** We got your system up and running tonight at the hackspace



![missing image](https://lh3.googleusercontent.com/3Xg8yo3LnfqRiReaC0CO4ow-9HP3cInnWMag-0VtVpphqD2NWOMDptUeQPGx0vx6f1dhMOOD_9rvlUC3uA75hGpP77SoPD1mNAs=s0)


---
**Wolfmanjm** *April 24, 2018 21:23*

awesome. does it work?


---
**Andrew Wade** *May 04, 2018 08:11*

**+Wolfmanjm** yes it does work. We are looking at designing and writing a custom version of your code, if that’s ok.


---
*Imported from [Google+](https://plus.google.com/112929738014057369475/posts/PUmcqksnX29) &mdash; content and formatting may not be reliable*
