---
layout: post
title: "I have a question regarding stepper motor encoders"
date: April 06, 2018 17:21
category: "General discussion"
author: "Jacob Gaietto"
---
I have a question regarding stepper motor encoders.  Is the current smoothie board capable of using encoders on the stepper motors?  Will Version 2 be able to use encoders?





**"Jacob Gaietto"**

---
---
**Jim Fong** *April 06, 2018 19:00*

That’s been answered in a recent thread by the developers, No encoder feedback on smoothieboard.  



It’s easier to let the driver handle encoder feedback due to the algorithm required for PID processing and high speed encoder reading. 



There are plenty of modern stepper drivers that feature encoder feedback.  Leadshine makes them. 



This is a little cheaper...

[http://tropical-labs.com/index.php/mechaduino](http://tropical-labs.com/index.php/mechaduino)



I use versions from Applied Motion but are very expensive.  



Usually if am going to use encoder feedback, I’ll step up to servo drives instead.  Three of my cnc’s use brushed servo drivers from Geckodrives.  



Almost all modern servo and stepper with encoder drivers are step/direction capable so can be hooked up to smoothieboard.  


---
**Jacob Gaietto** *April 06, 2018 19:16*

**+Jim Fong** Awesome, thanks!


---
*Imported from [Google+](https://plus.google.com/110126824074392442745/posts/HCp4ShhrUmm) &mdash; content and formatting may not be reliable*
