---
layout: post
title: "So after thinking my controller burned out I bought a new one and had it next day'd to my front door"
date: April 06, 2018 23:18
category: "Help"
author: "Chuck Comito"
---
So after thinking my controller burned out I bought a new one and had it next day'd to my front door. I connected the new board and it has the same issue the first one had. I took it over to another computer (where my laser resides with smoothie) and it connected immediately. So I think it's a driver issue but I can't figure it out. Can anyone recommend something to try? I've switched drivers, removed old drivers, changed com ports, baud rate, etc. I think it has something to do with Windows 10 (laser computer is Win7). Funny enough this WAS working just fine and then suddenly stopped... I don't have Windows updates on so nothing auto updated. I'm really stumped and looking for suggestions.

Thanks!





**"Chuck Comito"**

---
---
**Arthur Wolf** *April 06, 2018 23:21*

The documentation says not to install the drivers on Windows 10. Win10 already has drivers included and installing our driver can mess things up completely. I think you're good for using another computer or re-installing/resetting windows.


---
**Chuck Comito** *April 07, 2018 00:44*

I read the documentation and I'm aware of that. I didn't try new drivers until last resort. Sort of at the point of grasping at straws. On a side note I'm done for a while. I let the smoke out when connecting the usb. I have no idea what happened. I'm very frustrated at the moment. Thanks for the advice though. 


---
**Wolfmanjm** *April 07, 2018 10:25*

It should never smoke just on USB there simply is not enough current. Make sure you don't have a ground fault on your Computer or main outlets, if there is voltage on the ground that would be a serious (and dangerous) issue. (I once had a broken Neutral return in the USA, and the voltage differential burned several cables out!).


---
**Chuck Comito** *April 07, 2018 13:23*

**+Wolfmanjm**​, after some poking around I realized I do have a grounding issue. My 24v supply chassis was live and I know that's not right. I think my a.c. outlets aren't correct and I created a dangerous ground loop (for lack of a better term). 


---
**Wolfmanjm** *April 07, 2018 13:46*

That would do it.Make sure you have grounded outlets too and everything is plugged into the same outlet.


---
**Chuck Comito** *April 07, 2018 14:09*

I used my dmm to measure the outlet. I got the following:

L-N = 120VAC

L-G = 120VAC

N-G = 0VAC

L-power supply chassis = 120VAC

G-power supply chassis = 0VAC

N-power supply chassis = 0VAC



I believe this is correct with the exception of the live power supply chassis. I'm terrified I'm going to blow up another controller. I just ordered yet another one. This will be the 3rd one. I'm going to make a diagram of what I have going on here and post it. Hopefully you smart guys can help me diagnose it **+Wolfmanjm** and **+Don Kleinschnitz** :-)



It's a real bummer. I was so hoping to have the router running this weekend. I am convinced now that my communication issues and the trouble I've had with everything electronic wise is a result of the cheap power supplies I'm using. I don't think they're built very well.



My laser setup has meanwell power supplies and although they  work great they were expensive... 


---
**Wolfmanjm** *April 07, 2018 14:50*

Double check your PSU chassis is grounded and there is no short to Live or neutral. Make sure your power outlets are correctly grounded as well (may need an electrician to do that). In old houses quite often the ground pin is not connected to anything.


---
**Chuck Comito** *April 07, 2018 16:08*

**+Wolfmanjm**, I verified that there is no continuity between live and neutral. I do have continuity between ground and chassis. Here is a block diagram of my setup for the most part. Do you see anything out of the ordinary here or any potential ground loops I might be creating? Of course there is a PC connected via usb to this setup that I left off. When the 24v supply is connected I have 120v between Line and Chassis. Could this have wiped out my setup? I know it's dangerous but would it have fried the controller?

![missing image](https://lh3.googleusercontent.com/NyXI6qxGkwxXAeO66vJlwzmuXccBvWuRyWsurXm3eIMOWE_IwIndniMH-10nTpRCTKh61L8J0jCU8JCmxgj8O3u01qNsH7V5Iow=s0)


---
**Don Kleinschnitz Jr.** *April 07, 2018 16:14*

**+Chuck Comito** 



I do not remember, are you in Australia?



Measure the resistance of the PS chassis to the grnd/neutral and hot connections.



What PS are you using [link]?



..... your measurements suggest that your mains are ok. 



I like this plug to be sure that hot and neutral are not reversed which can create some weird problems on some equipment. 



[amazon.com - Electrical Receptacle Wall Plug AC Outlet Ground Tester with GFI Reset - Circuit Testers - Amazon.com](https://www.amazon.com/Electrical-Receptacle-Outlet-Ground-Tester/dp/B0012DHVQ0/ref=sr_1_3?ie=UTF8&qid=1523117330&sr=8-3&keywords=power+plug+checker)






---
**Chuck Comito** *April 07, 2018 16:31*

Hi **+Don Kleinschnitz**, here is the link: [amazon.com - Amazon.com: Aiposen 110V/220V AC to DC 24V 20A 480W Switch Power Supply Driver,Power Transformer for CCTV camera/ Security System/ LED Strip Light/Radio/Computer Project(24V 20A): Camera & Photo](https://www.amazon.com/gp/product/B01B1PRE60/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)



I am in the States. Measuring resistance resulted in 1.6k between ac ground and -24 output. All other connections were 0. 



I think I have access to that tester. I'll see if I can go get it today and double check everything. 



I'm still baffled as to what happened when I connected the USB. I simply blew up the board. I heard it arc (as I was looking at the USB connection on the back of the PC). By the time I looked up there was just a puff of smoke. 

 


---
**Don Kleinschnitz Jr.** *April 07, 2018 19:08*

Does your 24v supply read 24v to dc gnd ? Try measuring it's output on AC scale? 


---
**Chuck Comito** *April 07, 2018 19:39*

**+Don Kleinschnitz**​​, I measure 120vac between ac line and dc -24 and between line and dc +24 with an a.c. scale. 


---
**Don Kleinschnitz Jr.** *April 08, 2018 03:18*

**+Chuck Comito** measure the ac volts on the +24V to -24V while on ac scale of meter.


---
**Chuck Comito** *April 08, 2018 03:40*

**+Don Kleinschnitz**​, there is 24.5mVAC across + and -. 


---
**Don Kleinschnitz Jr.** *April 08, 2018 13:09*

**+Chuck Comito** lets recap to prevent me from being confused. Sorry to be redundant.



The AC-in to the 24VDC supply reads 120VAC from Line (L) to Neutral (N).

L to safety ground reads 120VAC 

N to safety ground read 0VAC

L to power supply chassis = 120VAC



<b>Measuring volts on the 24vdc supply:</b>

Between +V and COM there is 24mvAC  ?

+V to COM reads +24VDC

The case of the 24V supply to safety ground reads __?

The case of the 24V supply to COM reads__?



<b>On the ohms scale with AC power disconnected :</b>

COM to safety ground =__?



Notes:

Likely the COM, Safety grnd and the chassis are all tied together inside the supply. At least the safety grnd should be connected to the chassis. You can check with ohm meter.

The Safety ground is tied to N back in your power box.

Therefore L to safety ground should read 120VAC, and it does.

Therefore L to the chassis will read 120VAC and it does.



If safety ground to the chassis = 0 ohms and the safety ground is tied to the ground pin on the outlet, you do not have an unsafe condition. In fact if under these circumstances the case was hot it would have blown the house breaker when plugged in. 



I think something else is wrong!? However I do not see anything wrong with your wiring diagram.

Is is possible you got 24VDC reversed on the smoothie?



I would reconnect everything but the smoothie, turn it on and measure smoothie DC supply. 



BTW its a good idea to install a fuse block in these DC circuits to try (no guarantee) and prevent damage like this :).



[https://www.amazon.com/gp/product/B000VU9D1G/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B000VU9D1G/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)








---
**Chuck Comito** *April 08, 2018 14:12*

Good morning **+Don Kleinschnitz**, No worries on the redundancy. :-) I know these things are hard when you're not in front of it.



This is a correct statement:

The AC-in to the 24VDC supply reads 120VAC from Line (L) to Neutral (N).

L to safety ground reads 120VAC 

N to safety ground read 0VAC

L to power supply chassis = 120VAC



Measuring volts on the 24vdc supply: AC Setting on DMM

Between +V and COM there is 24mvAC  ? Yes

+V to COM reads +24VDC  No, it reads about 24mVAC on a AC scale and 0 VDC.

The case of the 24V supply to safety ground reads 1.5mVAC

The case of the 24V supply to COM reads 34mVAC



On the ohms scale with AC power disconnected :

COM to safety ground =Fluke meter say -OL (open circuit).



As far as something else being wrong I'm really stumped. At the time it blew up, I didn't have the 24vdc connected to the smoothie at all. 



Here is the full scenario and it doesn't make sense. 

Tuesday evening I was messing with the config file pretty much all night and everything was electrically and mechanically working. I was simply dialing everything in. I reset the board many time that evening. Late in the night I was satisfied with my progress and decided to shut it down. I disconnected the COM port from the smoothie, powered down the 24v supply and went to bed. At this point the USB was still connected. 



The next day I went to finish up and had communication errors. I tried all sorts of things from drivers to regedits to different computers. None of this worked and I determined the serial communications on the controller somehow fried.



I ordered another controller and had it next day'd to my door. I swapped out the controllers and had the same communication error so I removed the laptop I was using and went and got the PC i was using with the laser (after all I know this one works as it is already running smoothie.).



I reconnected everything and the last thing to plug in was the USB to the smoothie. As soon as the cable touched the USB input, I heard snap crackle pop and the board was dead... VERY DEAD.



I suppose it could be a fluke but with 2 boards down and a 3rd on its way, I honestly can't afford another one. I need to verify I'm correct before hooking up the new one.



By the way, can I kill a smoothie with a bad config file? I know I can screw it up from booting or loading the firmware but I can't "kill it" right?? Sounds silly but who know.



I suppose its possible to turn on or off a pin and accidentally send voltage the wrong way and kill it right?






---
**Don Kleinschnitz Jr.** *April 08, 2018 14:41*

**+Chuck Comito** Morning ...



I do not see any way that a bad configuration can fry a controller.



...

You posted;  <i>As far as something else being wrong I'm really stumped. At the time it blew up, I didn't have the 24vdc connected to the smoothie at all. _</i>



Then further down you posted: <i>I reconnected everything and the last thing to plug in was the USB to the smoothie. As soon as the cable touched the USB input, I heard snap crackle pop and the board was dead... VERY DEAD.</i> 



<b>When you connected the USB [in the above scenario ] was the 24v connected?</b>

...

You posted: <i>+V to COM reads +24VDC  No, it reads about 24mVAC on a AC scale and 0 VDC.</i> 

<b>What does it read on DC scale?</b>

...

<b>What does the resistance between the supply's case and the safety ground read __ ohms?</b>


---
**Wolfmanjm** *April 08, 2018 14:44*

If plugging in the USB killed it then the USB had a rogue voltage on it, maybe the PC has a bad ground and the USB shell was at a high voltage? 

It is not possible to permanently kill (or brick) a smoothieboard with a bad config.


---
**Chuck Comito** *April 08, 2018 14:45*

**+Don Kleinschnitz**​

When I connected the usb the  24v was not connected. 

It read 0 vdc on a dc scale. 

Case to ground was open. No resistance. 


---
**Don Kleinschnitz Jr.** *April 08, 2018 15:23*

**+Chuck Comito** 



<i>It read 0 vdc on a dc scale.</i>

<b>So the DC supply is dead?</b>






---
**Chuck Comito** *April 08, 2018 15:43*

It's not dead **+Don Kleinschnitz**​. I think I misunderstood what you asked and measured something else. It does work perfectly fine as far as outputting 24vdc. 


---
**Don Kleinschnitz Jr.** *April 08, 2018 16:12*

**+Chuck Comito** then I cannot imagine what went wrong everything looks normal to me. 



Clearly as **+Wolfmanjm** suggests there was a large potential difference on the USB connectors shell. 



Before connecting your usb next time I would measure the voltage on the USB connectors shell on both the smoothie and the PC. 



Have you measured the voltage on the PC's USB shell?



BTW can you see any fried part on the smoothie?


---
**Chuck Comito** *April 08, 2018 16:18*

I did not take measurements from the usb on the pc side. That said I'm running the laser as I type and all is well. It's virtually an identical setup. I cannot see anything fried but I clearly saw smoke and can smell the burnt area. I will measure the usb for verification. 


---
**Don Kleinschnitz Jr.** *April 08, 2018 18:57*

**+Chuck Comito** can you point me to the smelly area of the board :)


---
**Chuck Comito** *April 08, 2018 19:33*

Somewhere here. 

![missing image](https://lh3.googleusercontent.com/PNkVAKH9806lDk3zZTWnI2Pmi1zlLnyDf_T5B0avqwnhgipxBPb2zBJBx6axlsvHN4D5JU_9dIOX9WrV6wKmcW0T32mJBb3wIiA=s0)


---
**Chuck Comito** *April 10, 2018 00:46*

**+Don Kleinschnitz**, I found what I did wrong but have no idea how to fix it but you might know where to point me. I had reversed my plugs on my external drivers. The of connector on the drivers is the green phoenix style connectors so I had the Ena/dir/pul connector in the A+/- and B+/-. This would mean that I introduced a voltage from the output of the external driver to the smoothie driver signals. The red square was inserted at the far right in this images order and the blue square was to it's left occupying the first 6 locations next to the VCC and GND.

![missing image](https://lh3.googleusercontent.com/QcWHLYKHrlYrFZdb1QT7OLqNHLfLDpaotjWcRGgjdmbaij7b4hHqL_3YuSvXuYsfiWwB981aLkbw3ZUSNlIpVkPxFKF1d_Ta-IU=s0)


---
**Triffid Hunter** *April 10, 2018 07:37*

So you've dropped some high voltage into smoothie's sensitive 3.3v logic pins? yeah that'll fry all sorts of stuff, potentially including your computer's USB ports..


---
**Don Kleinschnitz Jr.** *April 10, 2018 12:31*

**+Chuck Comito**  "point you"....? 



So you had it connected something like:

B-    => <s>En</s>

B+   => +En

A-    => -Dir

A+   => +Dir

Gnd => -Pul

Vcc  => +Pul



I haven't wired a smoothie for external drivers but my guess is that the  steppers drivers on the smoothie are fried. Hopefully that is all.

You would have to fix the wiring and replace the smoothie (ouch) or  replace the driver chips on the smoothie. 



Someone on the smoothie team might have better advice (**+Wolfmanjm**).



I would do these in sequence to see if I can verify the smoothie:

...Check the power inputs resistance to ground and see if there is a short.

...Power up the smoothie and check all on board voltages.

...Power up the smoothie without any motors connected and see if it will connect to USB.

...Then try and connect stepper motor(s) (w/o external drivers) directly to the board and see if they run. Use a spare motor.

...Then try a driver with a stepper motor.


---
**Chuck Comito** *April 10, 2018 14:27*

Yes **+Don Kleinschnitz**​, that is exactly how I connected it. I was hoping it might just be the built in drivers.. Basically if there is any kind of protection on the processor end I'm confident I can fit it. I have already ordered another one so this will hopefully be a repair down the road. Looks like I'll have to figure out how to use a huntron...


---
**Chuck Comito** *April 15, 2018 02:12*

Hey **+Don Kleinschnitz**​, I got it working and cut my first dohicky. There's still lots to fix but the basics are done. I have to add m3 in my gcode start as the config file I'm using doesn't do the job the way the laser did. I also can't seem to get fusion 360 gcode to work. As long as I stick to laserweb for 2d stuff I'll be fine for a bit. Thanks for all your help. I'm sure I'll be bugging you again soon! :'l


---
**Don Kleinschnitz Jr.** *April 15, 2018 13:20*

**+Chuck Comito** by IT you mean your CNC mill? By Doohickey do you mean your making a laser meter?

F360 has a steep learning curve but hang in there its worth it. 

Let me know if you need help with F360, I am not an expert but am now designing in F360 and going direct to GCODE on my ox after CAM simulation. IMO it is way better than LW in terms of getting to the right GCODE, it does however have many options to flail with. 


---
**Chuck Comito** *April 15, 2018 15:01*

Hi **+Don Kleinschnitz**, yes... "it" means my cnc mill and by "doohickey" I just mean a little something I made and not the laser power meter although I might try one. I can design in F360 and have nearly 25 years of Autodesk product use under my belt so that part is easy. The part that's holding me up is the CAM functions. It seams relatively straight forward but I can seem to get a gcode file produced in F360 to work when sending it from LW4. I'm studying the differences in a LW4 gcode file and a F360 gcode at the moment. I might be better off using bCNC or something similar as my host. On a side note, this is my first ballscrew machine and I can't believe the difference in torque and accuracy. Much nicer than belt driven systems... Hopefully I'll be posting a show and tell video soon... :-)


---
**Don Kleinschnitz Jr.** *April 15, 2018 17:51*

I find F360 to better emilate how I understood cnc programming to work. 

You have to dig into each CAM menu to find changeable settings and the post processor is useful to customize for machines and their drivers. 

I actually run F360/ChilliPepper/TinyG simply because I wanted a setup I knew worked end to end. I planned to switch to a smoothie but since there is no CP workspace I have not undertaken that change.


---
**Arthur Wolf** *April 15, 2018 22:20*

Actually CP has a grbl workspace, and smoothie can be made to emulate grbl, so somebody needs to try that.


---
**Chuck Comito** *April 15, 2018 22:55*

I will try **+Arthur Wolf**​ if you point me in the right direction to emulate grbl. Is it as simple as enabling grbl mode?


---
**Chuck Comito** *April 15, 2018 23:10*



Like so **+Arthur Wolf**​?

grbl_mode 				 true	 # added as a result reading smoothie for bcnc


---
**Don Kleinschnitz Jr.** *April 16, 2018 12:22*

**+Arthur Wolf** yes, I just have to big a list of unfinished stuff to do.... and I do not want to take my k40 down again to do this as I only have one smoothie.


---
**Chuck Comito** *April 16, 2018 18:16*

**+Don Kleinschnitz**​, I now have 2 smoothies so I will give it a try and report back. 


---
**Chuck Comito** *April 16, 2018 22:28*

**+Arthur Wolf**​ and **+Don Kleinschnitz**​. That was a short run.. I don't know enough about this to figure it out. I added the line to the config file but wasn't able to connect. I did download and run the server end as stated on the chilipeppr site. It found my port but wouldn't connect. The information around the web is pretty thin on this stuff so I'm stuck. I'll fiddle around with it later and see what I can come up with. 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/2tdiDUJfKZ7) &mdash; content and formatting may not be reliable*
