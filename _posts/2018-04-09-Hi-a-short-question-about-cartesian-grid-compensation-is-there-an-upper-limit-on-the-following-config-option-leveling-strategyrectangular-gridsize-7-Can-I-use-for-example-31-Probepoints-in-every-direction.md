---
layout: post
title: "Hi, a short question about cartesian grid compensation, is there an upper limit on the following config option: leveling-strategy.rectangular-grid.size 7 Can I use for example 31 Probepoints in every direction ?"
date: April 09, 2018 08:55
category: "Help"
author: "Marc Pentenrieder"
---
Hi, a short question about cartesian grid compensation, is there an upper limit on the following config option:



leveling-strategy.rectangular-grid.size   7



Can I use for example 31 Probepoints in every direction ?

I have a large bed (1,3m x 1,3m) and probe only every 185,7mm is not enough



Thanks for your feedback

Marc





**"Marc Pentenrieder"**

---
---
**Arthur Wolf** *April 09, 2018 09:24*

That's going to be limited by RAM. As you increase that number you are going to have to disable other features and reduce other RAM-consuming options ( like planner queue size ).


---
**Marc Pentenrieder** *April 09, 2018 11:37*

**+Arthur Wolf** Do you know how I can find out how much RAM is available and eventually how much RAM more raising the probe pints will use per point.



Thanks for your help


---
**Arthur Wolf** *April 09, 2018 11:40*

RAM usage changes all the time, esepccially when printing. You need to increase your grid size and see if the machine crashes or not. The "mem -v" command might help, or it might not.


---
**Marc Pentenrieder** *April 09, 2018 11:41*

**+Arthur Wolf** Thanks, I will try it tonight.


---
**Marc Pentenrieder** *April 09, 2018 19:33*

**+Arthur Wolf** What is the intresting part ? Unused Heap or total free 10kb ?



This is the output after booting without printing:



SENT: mem -v

READ: Unused Heap: 5388 bytes

READ: Used Heap Size: 20948

READ:   Chunk: 1  Address: 0x10000D08  Size: 32  

READ:   Chunk: 2  Address: 0x10000D30  Size: 512  

READ:   Chunk: 3  Address: 0x10000F38  Size: 168  

READ:   Chunk: 4  Address: 0x10000FE8  Size: 12  

READ:   Chunk: 5  Address: 0x10000FF8  Size: 4  

READ:   Chunk: 6  Address: 0x10001008  Size: 40  

READ:   Chunk: 7  Address: 0x10001038  Size: 20  

READ:   Chunk: 8  Address: 0x10001050  Size: 4  

READ:   Chunk: 9  Address: 0x10001060  Size: 4  

READ:   Chunk: 10  Address: 0x10001068  Size: 88  

READ:   Chunk: 11  Address: 0x100010C8  Size: 16  

READ:   Chunk: 12  Address: 0x100010E0  Size: 28  

READ:   Chunk: 13  Address: 0x10001108  Size: 88  

READ:   Chunk: 14  Address: 0x10001168  Size: 428  

READ:   Chunk: 15  Address: 0x10001318  Size: 16  

READ:   Chunk: 16  Address: 0x10001330  Size: 16  

READ:   Chunk: 17  Address: 0x10001348  Size: 4  

READ:   Chunk: 18  Address: 0x10001358  Size: 28  

READ:   Chunk: 19  Address: 0x10001378  Size: 428  

READ:   Chunk: 20  Address: 0x10001530  Size: 16  

READ:   Chunk: 21  Address: 0x10001548  Size: 16  

READ:   Chunk: 22  Address: 0x10001560  Size: 20  

READ:   Chunk: 23  Address: 0x10001578  Size: 20  

READ:   Chunk: 24  Address: 0x10001598  Size: 32  

READ:   Chunk: 25  Address: 0x100015C0  Size: 4  

READ:   Chunk: 26  Address: 0x100015C8  Size: 12  

READ:   Chunk: 27  Address: 0x100015E0  Size: 28  

READ:   Chunk: 28  Address: 0x10001600  Size: 24  

READ:   Chunk: 29  Address: 0x10001620  Size: 224  

READ:   Chunk: 30  Address: 0x10001708  Size: 4  

READ:   Chunk: 31  Address: 0x10001718  Size: 64  

READ:   Chunk: 32  Address: 0x10001760  Size: 8  

READ:   Chunk: 33  Address: 0x10001770  Size: 16  

READ:   Chunk: 34  Address: 0x10001788  Size: 24  

READ:   Chunk: 35  Address: 0x100017A8  Size: 8  

READ:   Chunk: 36  Address: 0x100017B8  Size: 504  

READ:   Chunk: 37  Address: 0x100019B8  Size: 32  

READ:   Chunk: 38  Address: 0x100019E0  Size: 344  

READ:   Chunk: 39  Address: 0x10001B40  Size: 24  

READ:   Chunk: 40  Address: 0x10001B60  Size: 16  

READ:   Chunk: 41  Address: 0x10001B78  Size: 40  

READ:   Chunk: 42  Address: 0x10001BA8  Size: 76  

READ:   Chunk: 43  Address: 0x10001BF8  Size: 388  

READ:   Chunk: 44  Address: 0x10001D88  Size: 60  

READ:   Chunk: 45  Address: 0x10001DC8  Size: 4  CHUNK FREE

READ:   Chunk: 46  Address: 0x10001DD8  Size: 8  

READ:   Chunk: 47  Address: 0x10001DE8  Size: 20  

READ:   Chunk: 48  Address: 0x10001E00  Size: 4  

READ:   Chunk: 49  Address: 0x10001E10  Size: 52  CHUNK FREE

READ:   Chunk: 50  Address: 0x10001E48  Size: 16  

READ:   Chunk: 51  Address: 0x10001E60  Size: 24  

READ:   Chunk: 52  Address: 0x10001E80  Size: 20  CHUNK FREE

READ:   Chunk: 53  Address: 0x10001EA0  Size: 224  

READ:   Chunk: 54  Address: 0x10001F88  Size: 224  

READ:   Chunk: 55  Address: 0x10002070  Size: 4  

READ:   Chunk: 56  Address: 0x10002078  Size: 108  CHUNK FREE

READ:   Chunk: 57  Address: 0x100020F0  Size: 224  

READ:   Chunk: 58  Address: 0x100021D8  Size: 224  

READ:   Chunk: 59  Address: 0x100022C0  Size: 224  

READ:   Chunk: 60  Address: 0x100023A8  Size: 224  

READ:   Chunk: 61  Address: 0x10002490  Size: 224  

READ:   Chunk: 62  Address: 0x10002578  Size: 12  

READ:   Chunk: 63  Address: 0x10002588  Size: 172  CHUNK FREE

READ:   Chunk: 64  Address: 0x10002640  Size: 8  

READ:   Chunk: 65  Address: 0x10002650  Size: 112  CHUNK FREE

READ:   Chunk: 66  Address: 0x100026C8  Size: 224  

READ:   Chunk: 67  Address: 0x100027B0  Size: 224  

READ:   Chunk: 68  Address: 0x10002898  Size: 224  

READ:   Chunk: 69  Address: 0x10002980  Size: 16  

READ:   Chunk: 70  Address: 0x10002998  Size: 8  CHUNK FREE

READ:   Chunk: 71  Address: 0x100029A8  Size: 224  

READ:   Chunk: 72  Address: 0x10002A90  Size: 8  

READ:   Chunk: 73  Address: 0x10002AA0  Size: 72  CHUNK FREE

READ:   Chunk: 74  Address: 0x10002AF0  Size: 8  

READ:   Chunk: 75  Address: 0x10002B00  Size: 56  CHUNK FREE

READ:   Chunk: 76  Address: 0x10002B40  Size: 224  

READ:   Chunk: 77  Address: 0x10002C28  Size: 8  

READ:   Chunk: 78  Address: 0x10002C38  Size: 64  CHUNK FREE

READ:   Chunk: 79  Address: 0x10002C80  Size: 224  

READ:   Chunk: 80  Address: 0x10002D68  Size: 8  

READ:   Chunk: 81  Address: 0x10002D78  Size: 220  CHUNK FREE

READ:   Chunk: 82  Address: 0x10002E58  Size: 224  

READ:   Chunk: 83  Address: 0x10002F40  Size: 224  

READ:   Chunk: 84  Address: 0x10003028  Size: 224  

READ:   Chunk: 85  Address: 0x10003110  Size: 224  

READ:   Chunk: 86  Address: 0x100031F8  Size: 224  

READ:   Chunk: 87  Address: 0x100032E0  Size: 224  

READ:   Chunk: 88  Address: 0x100033C8  Size: 12  

READ:   Chunk: 89  Address: 0x100033E0  Size: 44  CHUNK FREE

READ:   Chunk: 90  Address: 0x10003410  Size: 12  

READ:   Chunk: 91  Address: 0x10003428  Size: 44  CHUNK FREE

READ:   Chunk: 92  Address: 0x10003458  Size: 12  

READ:   Chunk: 93  Address: 0x10003470  Size: 120  CHUNK FREE

READ:   Chunk: 94  Address: 0x100034F0  Size: 224  

READ:   Chunk: 95  Address: 0x100035D8  Size: 224  

READ:   Chunk: 96  Address: 0x100036C0  Size: 224  

READ:   Chunk: 97  Address: 0x100037A8  Size: 1024  

READ:   Chunk: 98  Address: 0x10003BB0  Size: 164  CHUNK FREE

READ:   Chunk: 99  Address: 0x10003C58  Size: 12  

READ:   Chunk: 100  Address: 0x10003C70  Size: 220  CHUNK FREE

READ:   Chunk: 101  Address: 0x10003D50  Size: 224  

READ:   Chunk: 102  Address: 0x10003E38  Size: 224  

READ:   Chunk: 103  Address: 0x10003F20  Size: 224  

READ:   Chunk: 104  Address: 0x10004008  Size: 12  

READ:   Chunk: 105  Address: 0x10004020  Size: 1044  CHUNK FREE

READ:   Chunk: 106  Address: 0x10004438  Size: 224  

READ:   Chunk: 107  Address: 0x10004520  Size: 224  

READ:   Chunk: 108  Address: 0x10004608  Size: 224  

READ:   Chunk: 109  Address: 0x100046F0  Size: 224  

READ:   Chunk: 110  Address: 0x100047D8  Size: 224  

READ:   Chunk: 111  Address: 0x100048C0  Size: 224  

READ:   Chunk: 112  Address: 0x100049A8  Size: 16  

READ:   Chunk: 113  Address: 0x100049C0  Size: 48  CHUNK FREE

READ:   Chunk: 114  Address: 0x100049F8  Size: 24  

READ:   Chunk: 115  Address: 0x10004A18  Size: 16  

READ:   Chunk: 116  Address: 0x10004A30  Size: 452  CHUNK FREE

READ:   Chunk: 117  Address: 0x10004C00  Size: 16  

READ:   Chunk: 118  Address: 0x10004C18  Size: 428  CHUNK FREE

READ:   Chunk: 119  Address: 0x10004DC8  Size: 28  

READ:   Chunk: 120  Address: 0x10004DF0  Size: 60  

READ:   Chunk: 121  Address: 0x10004E30  Size: 180  CHUNK FREE

READ:   Chunk: 122  Address: 0x10004EF0  Size: 24  

READ:   Chunk: 123  Address: 0x10004F10  Size: 56  CHUNK FREE

READ:   Chunk: 124  Address: 0x10004F50  Size: 32  

READ:   Chunk: 125  Address: 0x10004F78  Size: 44  

READ:   Chunk: 126  Address: 0x10004FA8  Size: 24  

READ:   Chunk: 127  Address: 0x10004FC8  Size: 32  

READ:   Chunk: 128  Address: 0x10004FF0  Size: 20  

READ:   Chunk: 129  Address: 0x10005010  Size: 28  

READ:   Chunk: 130  Address: 0x10005030  Size: 32  

READ:   Chunk: 131  Address: 0x10005058  Size: 16  

READ:   Chunk: 132  Address: 0x10005070  Size: 32  CHUNK FREE

READ:   Chunk: 133  Address: 0x10005098  Size: 16  

READ:   Chunk: 134  Address: 0x100050B0  Size: 24  

READ:   Chunk: 135  Address: 0x100050D0  Size: 132  

READ:   Chunk: 136  Address: 0x10005160  Size: 60  

READ:   Chunk: 137  Address: 0x100051A0  Size: 60  

READ:   Chunk: 138  Address: 0x100051E8  Size: 12  CHUNK FREE

READ:   Chunk: 139  Address: 0x100051F8  Size: 24  

READ:   Chunk: 140  Address: 0x10005218  Size: 128  

READ:   Chunk: 141  Address: 0x100052A0  Size: 24  

READ:   Chunk: 142  Address: 0x100052C0  Size: 28  

READ:   Chunk: 143  Address: 0x100052E8  Size: 24  

READ:   Chunk: 144  Address: 0x10005308  Size: 44  

READ:   Chunk: 145  Address: 0x10005338  Size: 4  CHUNK FREE

READ:   Chunk: 146  Address: 0x10005348  Size: 32  

READ:   Chunk: 147  Address: 0x10005370  Size: 80  

READ:   Chunk: 148  Address: 0x100053C8  Size: 60  

READ:   Chunk: 149  Address: 0x10005408  Size: 32  

READ:   Chunk: 150  Address: 0x10005430  Size: 128  

READ:   Chunk: 151  Address: 0x100054B8  Size: 12  CHUNK FREE

READ:   Chunk: 152  Address: 0x100054D0  Size: 24  

READ:   Chunk: 153  Address: 0x100054F0  Size: 52  

READ:   Chunk: 154  Address: 0x10005528  Size: 24  

READ:   Chunk: 155  Address: 0x10005548  Size: 4  CHUNK FREE

READ:   Chunk: 156  Address: 0x10005558  Size: 32  

READ:   Chunk: 157  Address: 0x10005580  Size: 64  

READ:   Chunk: 158  Address: 0x100055C8  Size: 80  

READ:   Chunk: 159  Address: 0x10005620  Size: 64  

READ:   Chunk: 160  Address: 0x10005668  Size: 8  CHUNK FREE

READ:   Chunk: 161  Address: 0x10005678  Size: 20  

READ:   Chunk: 162  Address: 0x10005690  Size: 72  

READ:   Chunk: 163  Address: 0x100056E0  Size: 8  CHUNK FREE

READ:   Chunk: 164  Address: 0x100056F0  Size: 16  

READ:   Chunk: 165  Address: 0x10005708  Size: 28  

READ:   Chunk: 166  Address: 0x10005730  Size: 16  

READ:   Chunk: 167  Address: 0x10005748  Size: 16  

READ:   Chunk: 168  Address: 0x10005760  Size: 16  

READ:   Chunk: 169  Address: 0x10005778  Size: 16  

READ:   Chunk: 170  Address: 0x10005790  Size: 8  CHUNK FREE

READ:   Chunk: 171  Address: 0x100057A0  Size: 16  

READ:   Chunk: 172  Address: 0x100057B8  Size: 64  

READ:   Chunk: 173  Address: 0x10005800  Size: 12  CHUNK FREE

READ:   Chunk: 174  Address: 0x10005810  Size: 24  

READ:   Chunk: 175  Address: 0x10005830  Size: 32  

READ:   Chunk: 176  Address: 0x10005858  Size: 24  

READ:   Chunk: 177  Address: 0x10005878  Size: 24  

READ:   Chunk: 178  Address: 0x10005898  Size: 24  

READ:   Chunk: 179  Address: 0x100058B8  Size: 24  

READ:   Chunk: 180  Address: 0x100058D8  Size: 64  

READ:   Chunk: 181  Address: 0x10005920  Size: 56  

READ:   Chunk: 182  Address: 0x10005960  Size: 24  

READ:   Chunk: 183  Address: 0x10005980  Size: 28  

READ:   Chunk: 184  Address: 0x100059A8  Size: 24  

READ:   Chunk: 185  Address: 0x100059C8  Size: 64  

READ:   Chunk: 186  Address: 0x10005A10  Size: 28  CHUNK FREE

READ:   Chunk: 187  Address: 0x10005A30  Size: 32  

READ:   Chunk: 188  Address: 0x10005A58  Size: 40  CHUNK FREE

READ:   Chunk: 189  Address: 0x10005A88  Size: 64  

READ:   Chunk: 190  Address: 0x10005AD0  Size: 1024  CHUNK FREE

READ: Allocated: 14548, Free: 4880

READ: Total Free RAM: 10268 bytes

READ: Free AHB0: 8604, AHB1: 10440

READ: Start: 15184b MemoryPool at 0x2007c4b0

READ: 	Chunk at 0x2007c4b0 (  +0): used, 268 bytes

READ: 	Chunk at 0x2007c5bc (+268): used, 140 bytes

READ: 	Chunk at 0x2007c648 (+408): used, 284 bytes

READ: 	Chunk at 0x2007c764 (+692): used, 88 bytes

READ: 	Chunk at 0x2007c7bc (+780): used, 24 bytes

READ: 	Chunk at 0x2007c7d4 (+804): used, 20 bytes

READ: 	Chunk at 0x2007c7e8 (+824): used, 72 bytes

READ: 	Chunk at 0x2007c830 (+896): used, 276 bytes

READ: 	Chunk at 0x2007c944 (+1172): used, 1028 bytes

READ: 	Chunk at 0x2007cd48 (+2200): used, 60 bytes

READ: 	Chunk at 0x2007cd84 (+2260): used, 488 bytes

READ: 	Chunk at 0x2007cf6c (+2748): used, 516 bytes

READ: 	Chunk at 0x2007d170 (+3264): used, 2692 bytes

READ: 	Chunk at 0x2007dbf4 (+5956): used, 36 bytes

READ: 	Chunk at 0x2007dc18 (+5992): used, 588 bytes

READ: 	Chunk at 0x2007de64 (+6580): free, 8604 bytes

READ: End: total 15184b, free: 8604b

READ: Start: 10440b MemoryPool at 0x20081738

READ: 	Chunk at 0x20081738 (  +0): free, 10440 bytes

READ: End: total 10440b, free: 10440b

READ: Block size: 84 bytes, Tickinfo size: 224 bytes




---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/Rs9g6RhQuzd) &mdash; content and formatting may not be reliable*
