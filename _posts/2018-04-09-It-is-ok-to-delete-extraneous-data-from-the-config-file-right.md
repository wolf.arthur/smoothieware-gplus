---
layout: post
title: "It is ok to delete extraneous data from the config file right?"
date: April 09, 2018 21:34
category: "Help"
author: "Chuck Comito"
---
It is ok to delete extraneous data from the config file right? Ie, anything already commented out does not need to be there and is safe to remove?





**"Chuck Comito"**

---
---
**Arthur Wolf** *April 09, 2018 21:37*

In theory yes. However when asking for help in the community we ask that users first make sure they only modified their config strictily no more than necessary.


---
**Chuck Comito** *April 09, 2018 21:40*

Thanks **+Arthur Wolf**, I've been working very hard on my config file and I'm worried I missed something. I thought the best bet would be strip it down to the bare minimum. I understand your position though. Plus [smoothie.org - smoothie.org](http://smoothie.org) is down at the moment so I can't use it for reference today.


---
**Chuck Comito** *April 09, 2018 22:15*

**+Arthur Wolf**, I took your advice and just modified as needed... :-)


---
**Antonio Hernández** *April 09, 2018 22:32*

**+Arthur Wolf** I made some testings using "mem" command, with and without comments on config file, and I saw the heap size and unused memory concepts does not change in both cases. Is there a problem if I have multiple comments along the config file ?. Are they ignored over memory space ?.


---
**Wolfmanjm** *April 10, 2018 10:09*

**+Antonio Hernández** yes comments are ignored and skipped.


---
**Antonio Hernández** *April 10, 2018 15:58*

Ok, thanks for the info  **+Arthur Wolf** !


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/7k9S5tkmfdZ) &mdash; content and formatting may not be reliable*
