---
layout: post
title: "Is shut down for good?"
date: April 10, 2018 05:37
category: "General discussion"
author: "Jason Drennen"
---
Is [smoothieware.org](http://smoothieware.org) shut down for good?





**"Jason Drennen"**

---
---
**Arthur Wolf** *April 10, 2018 10:03*

No, definitely not. Server problem, working on it 24/7 until it's fixed.


---
**Anthony Bolgar** *April 10, 2018 18:53*

It seems OK now.




---
**Arthur Wolf** *April 10, 2018 18:53*

Yep spend the whole day fighting apt-get, finally won.


---
**Chuck Comito** *April 10, 2018 23:44*

Good job **+Arthur Wolf**​! I was getting worried lol. 


---
*Imported from [Google+](https://plus.google.com/+JasonDrennen/posts/QaRooFMySJU) &mdash; content and formatting may not be reliable*
