---
layout: post
title: "Is it possible to add an additional y axis stepper BUT drive it on the opposite direction as commanded?"
date: April 11, 2018 11:13
category: "Help"
author: "Chuck Comito"
---
Is it possible to add an additional y axis stepper BUT drive it on the opposite direction as commanded? This way I can have y and say epsilon both going the same direction if the shafts are facing one another. I read how to slave another axis and it works but it only wanted to move identical to the y. 





**"Chuck Comito"**

---
---
**Douglas Pearless** *April 11, 2018 12:30*

If my brain is working correctly, you should simply need to reverse the order of the pins in the stepper motor connector (i.e. rotate it 180 degrees)


---
**Wylie Hilliard** *April 11, 2018 13:24*

You just need to reverse one of the pairs of wires to the stepper.


---
**Griffin Paquette** *April 11, 2018 13:56*

Sounds like a simple ! At the end of the direction line in your config would make that happen. 


---
**Douglas Pearless** *April 11, 2018 20:30*

Either way will work, invert the direction pin in the config ( [smoothieware.org - configuring-smoothie [Smoothieware]](http://smoothieware.org/configuring-smoothie#all-options) ) or 180 degree rotation of the stepper motor connector 


---
**Chuck Comito** *April 12, 2018 01:59*

Flipping the motor plug did the trick!! Thanks guys. 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/eWPdWgXHq6f) &mdash; content and formatting may not be reliable*
