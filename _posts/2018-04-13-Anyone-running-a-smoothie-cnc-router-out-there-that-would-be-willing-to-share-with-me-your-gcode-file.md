---
layout: post
title: "Anyone running a smoothie cnc router out there that would be willing to share with me your gcode file?"
date: April 13, 2018 23:49
category: "Help"
author: "Chuck Comito"
---
Anyone running a smoothie cnc router out there that would be willing to share with me your gcode file? I'd like to do some testing but I want to make sure I have something to compare to. Thanks in advance..





**"Chuck Comito"**

---
---
**Jason Bull** *April 14, 2018 02:35*

I am running a Smoothie 5xC for a router, please clarify what you are looking for in Gcode and i will send you some stuff.


---
**Chuck Comito** *April 14, 2018 02:46*

**+Jason Bull**​, anything really. I want to make sure a good usable file is my comparison. Right now my z appears to be moving in the plus direction away from the work during a job however I think my config and setup is correct. I'm very new to this style cnc so I'm guessing what is wrong at the moment. I want to rule out my gcode files that I'm creating. Thanks+!


---
**Don Kleinschnitz Jr.** *April 19, 2018 21:30*

**+Chuck Comito** huh isn't +Up and - Down in a routers Z? All from the 0 setting?


---
**Chuck Comito** *April 19, 2018 21:37*

Hi **+Don Kleinschnitz**, I just realized how poorly I worded this post.. What I should have said is that the router moves in the z+ direction when it should have been plunging into the work (-Z direction). Either way I got it sorted. I didn't set my Z axis correctly in Fusion. I have a happy CNC at the moment. Still working out the bugs but I managed to make something... 


---
**Don Kleinschnitz Jr.** *April 20, 2018 00:49*

**+Chuck Comito** nice!


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/gyEc2faRexP) &mdash; content and formatting may not be reliable*
