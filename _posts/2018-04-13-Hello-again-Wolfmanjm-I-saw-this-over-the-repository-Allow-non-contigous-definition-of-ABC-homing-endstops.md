---
layout: post
title: "Hello again. Wolfmanjm , I saw this over the repository: \"Allow non contigous definition of ABC homing endstops\""
date: April 13, 2018 23:53
category: "General discussion"
author: "Antonio Hern\u00e1ndez"
---
Hello again. **+Wolfmanjm**, I saw this over the repository: "Allow non contigous definition of ABC homing endstops". So..., the question is... Is it possible to declare A and C rotatory axes without B axis stepper motor definition and without B axis stopper definition ?. If I execute the home command (using this update), there is not required to detect B axis stopper before C axis stopper ? (B axis stopper could be ignored and the home command will work for A and C rotatory axes ?). Or this change only applies over config file definition but not over the behavior of home command ?. Nowadays, If I want to use C rotatory axis, A and B must be defined before and in sequence (stepper motor and stopper definitions). Any comment is appreciated. Thanks.





**"Antonio Hern\u00e1ndez"**

---
---
**Wolfmanjm** *April 14, 2018 09:07*

all axis definitions are still required. the fix was for the endstops only. 


---
**Wolfmanjm** *April 14, 2018 09:08*

endstops however can be defined only if you need them and do not have to be contiguous as before


---
**Antonio Hernández** *April 14, 2018 17:17*

Ok, so,with this update, I can use in home property (for example) Z,Y,X,A,C ? (B axis stopper definition could be ignored) ?


---
**Wolfmanjm** *April 14, 2018 17:42*

yes


---
**Antonio Hernández** *April 14, 2018 18:24*

Ok ! . Thanks for your comments **+Wolfmanjm** ! 


---
*Imported from [Google+](https://plus.google.com/107343842763021715447/posts/1ehkxtxmkTE) &mdash; content and formatting may not be reliable*
