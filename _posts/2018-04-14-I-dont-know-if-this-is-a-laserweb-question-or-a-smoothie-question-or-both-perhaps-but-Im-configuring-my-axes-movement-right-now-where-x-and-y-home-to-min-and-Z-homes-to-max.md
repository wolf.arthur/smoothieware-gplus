---
layout: post
title: "I don't know if this is a laserweb question or a smoothie question or both perhaps but I'm configuring my axes movement right now where x and y home to min and Z homes to max"
date: April 14, 2018 01:08
category: "Help"
author: "Chuck Comito"
---
I don't know if this is a laserweb question or a smoothie question or both perhaps but I'm configuring my axes movement right now where x and y home to min and Z homes to max. X and Y are in the front left corner and z max moves the spindle up and away from the work piece until the limit switches are reached respectively. This works perfectly however when i generate gcode, the spindle moves away from the work piece. If I'm not mistaken I have my setup correct and the tool plunge should be in a negative (-Z) movement? Did I miss something? Thanks all..





**"Chuck Comito"**

---
---
**Joe Alexander** *April 14, 2018 05:28*

if your having only one axis move the wrong way then invert the direction pin in the config by adding a "!" after the pin designator.(IE change P2.4 to P2.4!)


---
**Arthur Wolf** *April 14, 2018 09:18*

If the axes move the right way when moving manually, but don't do the right thing with generated Gcode, you need to go into your Gcode generating program and change the orientation of your part/gcode there.


---
**Chuck Comito** *April 14, 2018 13:27*

Thanks **+Arthur Wolf**​. I just wanted to be sure. I'd like my setup to be correct. I know I could do what **+Joe Alexander**​ suggested as well but that seamed like cheating. So the traditional setup would be homing to xy min (front left corner) and z to max which would be spindle up and away from the work?


---
**Arthur Wolf** *April 14, 2018 16:04*

It's really more a matter of taste than of tradition. Pro machines have all the variations you could imagine and more.


---
**Chuck Comito** *April 15, 2018 02:08*

**+Arthur Wolf**​, **+Joe Alexander**​, it's working. It was definitely the gcode. If I create within laserweb it's fine. If I create it in fusion I get the gcode out of bounds error or the wrong z direction. Still lots to learn but thank you both. 


---
**Arthur Wolf** *April 15, 2018 10:13*

Check the youtube tutorials for fusion360 on this, you need to set up the origin and axis orientation for both the "group" of operations ( not sure how it calls it ), and for each operation. Once you've done that it'll be the right z direction.


---
**j.r. Ewing** *April 15, 2018 10:43*

You wcs is flipped in other words z is pointing. The wrong direction in your cam software.  Z plus is always up the spindle even on 9 axis lathes. 


---
**j.r. Ewing** *April 15, 2018 10:44*

Remeber when you flip z to rotate x to where you need it cause it may be 180 out from current direction 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/BpEiwwiPX7Q) &mdash; content and formatting may not be reliable*
