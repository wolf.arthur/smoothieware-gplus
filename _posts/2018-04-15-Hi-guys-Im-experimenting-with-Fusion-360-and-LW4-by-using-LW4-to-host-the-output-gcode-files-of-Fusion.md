---
layout: post
title: "Hi guys, I'm experimenting with Fusion 360 and LW4 by using LW4 to host the output gcode files of Fusion"
date: April 15, 2018 19:24
category: "Help"
author: "Chuck Comito"
---
Hi guys, I'm experimenting with Fusion 360 and LW4 by using LW4 to host the output gcode files of Fusion. I'm using the built in Smoothie Mill/Laser config file as the post processor setup. While looking at the gcode I came across a few things I need help understanding as it pertains to smoothieware. Here is what F360 outputs (I've commented each line to better understand it. Please correct me if I'm wrong. In particular there is a line of G28 G91 Z0 and T1 that I don't completely understand as well as a M107 command that I'm not sure should be there:

%

; 1001

; T1 D=6.35 CR=0 - ZMIN=-2.286 - flat end mill



;Set's absolute value

G90



;Select XYZ plane (command is modal)

G17



;Millimeter mode ( default ) : passed coordinates will be considered as millimeters (command is modal)

G21



;Need help understanding this?

G28 G91 Z0



;Set's absolute value

G90



; 2D Pocket1

M107



;Stop or Unconditional stop

M0



;Need help understanding this:

T1



;Spindle Parameters (speed and what speed)

S10000 M3



;Turn fan off (what does this mean to smoothie)

M107





**"Chuck Comito"**

---
---
**Johan Jakobsson** *April 16, 2018 05:50*

afaik:

G28 home the machine

G91 Sets relative mode

T1 Calls tool 1 to be used - ; T1 D=6.35 CR=0 - ZMIN=-2.286 - flat end mill it seems from the first lines of the gcode snippet. Think this line can be removed. 

M107 turns off the cooling fan by default. Could also be whatever you have specified to be turned on/offwith M107 in the config file.  - search the wiki for "switch".


---
**Wolfmanjm** *April 16, 2018 11:06*

in cnc/grbl mode G28 is NOT home, as in nist gcode standard G28 is goto predefined park position. home is $H as in grbl. there is no standard home gcode in the nist standard. if 360 generates g28 for home then it is incorrect


---
**Chuck Comito** *April 16, 2018 11:21*

**+Wolfmanjm**​​ and **+Johan Jakobsson**​ Why does it generate the M0. While running this gcode in lw4, it can't get past this point. If I manually edit this file and remove that line as well as invert the S value to M3 S10000 it'll run?


---
**Johan Jakobsson** *April 16, 2018 11:22*

**+Wolfmanjm** Think the home command is specified in the post processor in fusion360 so you need to select the correct post processor for your machine. Perhaps GRBL Laser? [cam.autodesk.com - Autodesk CAM &#x7c; Post Library](http://cam.autodesk.com/posts/)


---
**Johan Jakobsson** *April 16, 2018 11:35*

**+Chuck Comito** Fusion360 generates the gcode you tell it to generate.

You need to select the correct post processor for it to generate gcode that's compatible with your machine. If you're running a smoothieboard in CNC mode i'd suggest you tru a GBRL post processor.




---
**Chuck Comito** *April 16, 2018 13:00*

**+Johan Jakobsson**​, i will try the grbl post processor but there is a smoothie post processor that I was using. 


---
**Steven Kirby** *May 04, 2018 18:04*

Hey **+Chuck Comito**,



Glad to see your machine is up and running, which post worked for you in the end? Just scratching my head over this now as both the smoothieware and grbl post processors use G28 instead of $H to home.


---
**Chuck Comito** *May 04, 2018 18:47*

Hi **+Steven Kirby** thanks! I ended up using the grbl post and turning off the homing function. Works perfectly. Did you ever sort your vfd? 


---
**Steven Kirby** *May 04, 2018 20:27*

I was having a look in the post code and I spotted that there was a line where you could set "use G28" to false. Then when I looked at it a bit longer I convinced myself that G28 was for safe retracts. Ie moving the tool out of the way of the workpiece. So I left it on. If you say you've switched it off then I guess I'll go with that.



I never got around to properly looking at modbus configuration. Despite the fact that it makes mention if it in the settings, I'm not sure it has the requisite inputs. The manual makes no mention of where to connect for modbus control. Got a bit distracted designing my friend a Delta in fusion. But now we need my machine operational to cut the parts!



 I think the plan to fine tune is going to be test a range of speeds with the tachometer and see what I get then make some sort of RPM conversion graph of programmed RPM vs actual. Then I should be able to find out what speed I need to program in order to get the speed I want. 



Wrapping my head around the feeds and speeds required for my tools and the materials I'm cutting and figuring out cam in fusion has been taxing. I think I've learned all I can about it though reading and simulations and now it's time to just get stuck in and snap some endmills. Just having a poke about in bCNC at the moment getting familiar with that. Hopefully by this time tomorrow I'll have run the surfacing operation I've programmed and my machine will be ready to cut something interesting. 👍



What happened with your boards in the end? Were they fried or was it just a software thing? Glad to see you got there in the end! Nice work!


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/iYPsPQShXJb) &mdash; content and formatting may not be reliable*
