---
layout: post
title: "Anyone have information on how to do homing of two motor with two endstop for an Axis?"
date: April 17, 2018 23:57
category: "Help"
author: "S\u00e9bastien Plante"
---
Anyone have information on how to do homing of two motor with two endstop for an Axis?



I wish to home the X and Y of my MPCNC.





**"S\u00e9bastien Plante"**

---
---
**Douglas Pearless** *April 18, 2018 00:15*

[smoothieware.org - endstops [Smoothieware]](http://smoothieware.org/endstops)


---
**Douglas Pearless** *April 18, 2018 00:16*

I assume you want to home to min or max on any given axis 


---
**Sébastien Plante** *April 18, 2018 00:22*

Yes, but I have two stepper per axis and I want to align both with two endstop so the axis is square.



The link doesn't provide informations about this.


---
**Johan Jakobsson** *April 18, 2018 05:51*

You'll have to use a switch. The normal homing routine doesn't support what your after as far as I know.


---
**Jose Salatino** *April 18, 2018 07:08*

connects the endstop in series. In this way the two sensors will have to be activated so that the microprocessor receives the signal


---
**Arthur Wolf** *April 18, 2018 08:23*

You are looking for homing multi-motor axes: [smoothieware.org - switch [Smoothieware]](http://smoothieware.org/switch#homing-a-multi-motor-axis)


---
**Sébastien Plante** *April 18, 2018 11:22*

this is what I was looking for ! Thanks :)



Wish it was implemented like in Marlin... but I don't want to go back to Arduino based board :(


---
**Arthur Wolf** *April 18, 2018 14:18*

Well the fact it's implemented this way is a bit more work, but it allows users to have many more ( and more complex ) setups, so that's the smoothie way of doing it. We'll likely be improving this a bit still by allowing users to override G28 and internal commands like it ( it's on the todo list, don't know if I can code it properly )


---
*Imported from [Google+](https://plus.google.com/+excessnet/posts/6y5qWCXWTRk) &mdash; content and formatting may not be reliable*
