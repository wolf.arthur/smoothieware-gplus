---
layout: post
title: "Would it be feasible to design a breakout board with a thermocouple to simulate it to be a thermistor so it can be hooked up to the thermistor pins on the smoothieboard?"
date: April 17, 2018 06:42
category: "General discussion"
author: "Andrew Wade"
---
Would it be feasible to design a breakout board with a thermocouple to simulate it to be a thermistor so it can be hooked up to the thermistor pins on the smoothieboard? This would allow not firmware changes and higher temperatures. 





**"Andrew Wade"**

---
---
**Arthur Wolf** *April 17, 2018 08:55*

Sure, definitely can be done. I think there's a chip that does that actually, AD485 or something like that.


---
**Jeff DeMaagd** *April 21, 2018 04:20*

Is the digital thermocouple board not an option? You don’t need to muck with the firmware for that.


---
**Andrew Wade** *April 21, 2018 08:48*

**+Jeff DeMaagd** I do not see how a digital output will work with the thermistor pins on the smoothieboard 


---
**Jeff DeMaagd** *April 21, 2018 12:23*

They won’t. I'm wondering if your SPI busses already occupied, or if you didn't know about the SPI thermocouple board, or if there's some other limitation you haven't told us yet.



 [https://shop.uberclock.com/collections/smoothie/products/spi-thermocouple](https://shop.uberclock.com/collections/smoothie/products/spi-thermocouple)


---
*Imported from [Google+](https://plus.google.com/112929738014057369475/posts/3CALPDR64NB) &mdash; content and formatting may not be reliable*
