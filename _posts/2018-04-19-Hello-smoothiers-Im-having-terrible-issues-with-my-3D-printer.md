---
layout: post
title: "Hello smoothiers, I'm having terrible issues with my 3D printer"
date: April 19, 2018 18:55
category: "Help"
author: "Kristofer Burbano"
---
Hello smoothiers,



I'm having terrible issues with my 3D printer. When I print, the extruder nozzle collides with layers that have just been laid down. I see the z axis rod threads rotate, but they don't rotate enough to avoid the previous layer. I've been trouble shooting this for 2 weeks. What is going on?



This is my setup: Printer - Rigidbot Regular with 10x10x10 build volume



Motherboard - Smoothieboard 5xC v1.1 with 1/32 micro-stepping, latest firmware, config file



My z motors have a step angle of 1.8 degrees, 1.68amps, and rods with 12 tpi or 2mm pitch



My config file:



[https://app.box.com/s/jtvfx1c43fu8p6lr6j7kmqjc9myqdbd3](https://app.box.com/s/jtvfx1c43fu8p6lr6j7kmqjc9myqdbd3)





**"Kristofer Burbano"**

---
---
**Thomas Herrmann** *April 19, 2018 19:18*

Hm, config looks ok to me. Did you had a good first layer and can you manually (host) move the Z- axis up and down?


---
**Kristofer Burbano** *April 19, 2018 19:19*

Yes, I can manually operate everything.


---
**Thomas Herrmann** *April 19, 2018 19:25*

Do you have apicture of the first layer? Sounds like you are just printing too close.


---
**Kristofer Burbano** *April 19, 2018 19:36*

hang on let me do a run


---
**Kristofer Burbano** *April 19, 2018 20:02*

So this is currently what my printer is doing after starting a print. My previous config file had me cutting the steps per mm on all my motors in half so I could "trick" my printer into even getting a first layer. So with my current config (with all the steps per mm as they SHOULD be) this is what happens...why does everything get so out of wack?

![missing image](https://video-downloads.googleusercontent.com/ABzBgw2RPS3oQmx4NPjx0Y4SxAk4fhifa4OtHWyhcnRHwgYrrwfYXQpf5CkbD4JPNI91rremN2U01tbAE6kiZdVIG6HEWKepiaUjlbnpyi8nrpiM19a8Dk_fcJjeVFE17y58ZvEBNXy3lcrfkSN8eH0li363vDmX9mIjhv0txX3hwVaHOslScwQqersToeloT5DFBctMW7Ln4uoapfctxCpgmlrV142uGW_D8RHAqg8rxQyln0A7XVPsrGpY4viZJqThHX2AheO8yk4NfTLAfpAifEkBmHP4iSdVApAMqtQP9owKRWcG1WerqnOnpKn0tLuDKlIOfAa7dxhLdbVQNJBLGIvkSn_2s6kO8y0iw4NG1nNSS0cNHmb8zzayPdlf-JWamJovROUfQF8WOxR03-Ne7mZTbIiHqgNOx9SSOYz0UV90cqHlJI1bz6S4wU2rG01I7TElNiy9z_4rJ8vV4hPMUxbvTcumxuRX0GUOr2lScBtQ3gon83VcWkLORojNBOOi9NjOPf9iclHWlkPIvFK8gDNMLMoyyP5sRSrb7Ti5PkJFIk95IpLYZZgQ594iUBarsBGeruDR4dS5vdn7qvZCRGVDMbGvW--j3ArPLG-B0ZWNN60Ql17ENG8kGTMX-crUkQ_k3fhXUwx82l_nELfC2Gwb-8htWk5o2-3A9wb5CO7FCIz2IlygKnY9NMsfJvuf6sNmHRFFqbbTqmCzh5lVp35BwcICYR1MqW61dJaHIdz5ouTo3DHDRsBAhNk-eBYABUjT7vertijB30k8YcfT3hgHHlOuBwyl_t2-FzWZHpdJNCFdCaY)


---
**Thomas Herrmann** *April 19, 2018 22:26*

Possibly something in the slicer side of things. Looks like a strange start gcode in the machine settings. Can you post the gcode?


---
**Johan Jakobsson** *April 19, 2018 22:35*

Your acceleration values look way off.

I'd suggest lowering it to 1000 for X/Y and 500 or even lower for Z to start with. You can raise and tweak those values later when you're fine tuning your printer.




---
**Kristofer Burbano** *April 19, 2018 22:56*

It's working WAAAAY better. Finally got to print a half decent calibration cube. The problem seemed to be my bed level after all, but I will fix the acceleration as you said. Check this out though, the last few layers came way off from the print, wondering what went wrong. I'll keep trying. Thanks guys!

![missing image](https://lh3.googleusercontent.com/E4zaQOIvbkq2V_mP3LxK-1NdbeVWaVZaVrNJV4CMeGHhdqR45DC4ScuF_HqQgiqYNIYbKLLAqeAwZnkEnz8iCVnzAmePvg7x7E8=s0)


---
**Thomas Herrmann** *April 19, 2018 23:08*

You will get this right. Just keep trying. A  tiny bit closer to the bed and a bit slower and smoother acc and youŕe good to go.


---
*Imported from [Google+](https://plus.google.com/101861041940313937315/posts/B3mNh1PHDh7) &mdash; content and formatting may not be reliable*
