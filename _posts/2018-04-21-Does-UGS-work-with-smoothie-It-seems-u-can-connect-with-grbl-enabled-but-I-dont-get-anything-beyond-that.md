---
layout: post
title: "Does UGS work with smoothie? It seems u can connect with grbl enabled but I don't get anything beyond that"
date: April 21, 2018 13:54
category: "Help"
author: "Chuck Comito"
---
Does UGS work with smoothie? It seems u can connect with grbl enabled but I don't get anything beyond that. I can't find any info as to the proper configuration. It might not be there yet so I thought I'd ask. Thanks. 





**"Chuck Comito"**

---
---
**Wolfmanjm** *April 21, 2018 15:24*

bCNC works much better, try that.


---
**Chuck Comito** *April 21, 2018 15:33*

Hi **+Wolfmanjm**​ I just got that up and running a second ago and will give it a try. I'm just looking for something that feels comfortable to use so I've been experimenting with different hosts.  


---
**Chuck Comito** *April 21, 2018 16:24*

Hey **+Wolfmanjm**, WPos X=0, Y=0, Z=0 buttons don't do anything. Is there a trick to this?? :-)


---
**Wolfmanjm** *April 21, 2018 16:35*

make sure you are using the latest version of smoothie cnc version. and make sure you set bcnc to smoothie mode. should work fine. you will see the WCS set correctly G54




---
**Wolfmanjm** *April 21, 2018 16:36*

read the linuxcnc docs on how coordinate systems work if you are unsure


---
**Chuck Comito** *April 21, 2018 16:36*

**+Wolfmanjm**​, so no need to enable grbl mode?


---
**Wolfmanjm** *April 21, 2018 16:37*

grbl mode is set by default in the firmware-cnc-latest


---
**Arthur Wolf** *April 22, 2018 15:09*

[smoothieware.org/bcnc](http://smoothieware.org/bcnc)


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/gFs33nyoKAL) &mdash; content and formatting may not be reliable*
