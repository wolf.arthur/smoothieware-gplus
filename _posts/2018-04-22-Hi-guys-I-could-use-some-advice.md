---
layout: post
title: "Hi guys, I could use some advice"
date: April 22, 2018 16:59
category: "General discussion"
author: "Chuck Comito"
---
Hi guys, I could use some advice. I'm running into my stepper drivers overheating. I know this isn't smoothie related but I thought some of you guys might have some experience here. I'm using Nema 23 motors rated at 4.2A 3.0Nm (425oz.in). I have a 24v supply and the drivers are rated at 4A and operate between 9-42V. I was told in another forum to multiply the inductance of the motor's square root by 32 to get the required voltage. (I'm not sure where the 32 comes from...) That said, if my inductance is 3.8mH that would equal 1.949 x 32 = 62.4v required. That leaves me with needing a much higher output supply and drivers that can handle upwards of 63v. Does this sound correct and can anyone help me to understand if this is correct and where does the 32 multiplier come from?





**"Chuck Comito"**

---
---
**Jonathon Thrumble** *April 22, 2018 17:19*

[https://forum.arduino.cc/index.php?topic=456150.0](https://forum.arduino.cc/index.php?topic=456150.0)


---
**Chuck Comito** *April 22, 2018 17:58*

I thought I'd add to this as I found this article. [geckodrive.com - Step Motor Basics &#x7c; Geckodrive](https://www.geckodrive.com/support/step-motor-basics.html)


---
**Jim Fong** *April 22, 2018 21:41*

That inductance formula is from Geckodrives. Used to figure out the the power supply voltage to maximize the performance of their drives.  They are capable of running 80volts.  I have about a dozen of them.  Geckos are 7 amp drives and will spin a motor several thousand rpm.  



Most lower end drivers can’t run voltage that high so use a PS that is rated for your driver.   On my machines, they are between 40 and 55volts. I get the required rpm neded so I don’t run higher voltage even though the formulas says I should.  



What stepper drivers are use using?  All mine are heatsinked to a big chunk of aluminum plus fan cooled.  Higher voltage PS will tend to make the motors/divers run hotter.   Motors are usually good for 100C case temperature. 


---
**Jeff DeMaagd** *April 23, 2018 01:25*

Running steppers at rated current makes them very hot. If you want, dial back the current until the temperature is reasonable. The drive voltage is just a recommendation for maximum performance. You can still get good performance with lower voltage drive. I’d still shoot for 48V for those motors though.


---
**Chuck Comito** *April 23, 2018 02:09*

Hi **+Jim Fong**, I've been all over there site today. I've been using these motors and drivers for various projects over the years but nothing as big as my CNC mill. I honestly didn't realize there was a calculation to the drives and motors to get optimal performance (I can be an idiot sometimes :O) Right now the drivers I have are the TB6600 chinese specials and seem to be running ok with the amps selectors on the drives set to 3 (just under their max) with a very large fan on them. I also had the power inputs daisy chained which per that website isn't a good idea. I have since got myself a terminal block and routed all the power individually to each motor.  I did some disassembly and these are also heat sinked to a large aluminum block with some thermal compound. 

**+Jeff DeMaagd**, I was considering just upping my supply a bit. They are rated for up to 42vdc and I think I have a 36vdc available here somewhere. I don't want to put good money after bad either by buying anything under rated from here on out. My thoughts were to find a solid 6A driver with the correct power supply. As all this costs money, I'm hoping the fan will do for a little bit until I can get some savings put away. I thought I might be able to mod these guys to squeeze a bit more out of them... Maybe I can mount fans directly to the heatsinks but how much performance will that actually get me....


---
**Jim Fong** *April 23, 2018 02:34*

**+Chuck Comito** upping the voltage from 24 to 36 will probably get 30% more RPM.  I wouldn’t go much above that to be safe.  TB6600 are ok, I have one that I use for testing.   It does run hot if pushed maximum output current. Make sure there is heatsink compound on the chip and use a fan.  



The minimum drivers I recommend is the leadshine DM542’s, around $30 each, for larger motors.  Higher end leadshine are good too.  The Geckodrives are great but costly.   They did repair all my drives for free when I had a power surge that destroyed everything electric in the House.  Some of the drives were over 10 years old and way out of warranty.  They certainly back their drives up so it was worth the added cost. 



For smaller nema17’s, I really like Trinamic’s.  


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/LBXACVBuMHN) &mdash; content and formatting may not be reliable*
