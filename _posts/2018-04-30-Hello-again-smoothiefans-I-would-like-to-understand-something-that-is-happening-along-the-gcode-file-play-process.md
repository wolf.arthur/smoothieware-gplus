---
layout: post
title: "Hello again smoothiefans. I would like to understand something that is happening along the gcode file play process"
date: April 30, 2018 17:14
category: "General discussion"
author: "Antonio Hern\u00e1ndez"
---
Hello again smoothiefans. I would like to understand something that is happening along the gcode file play process. What is the meaning of led number 2 ?. Depending conditions, it could be blinking or not (for example, it blinks if "kill play" is thrown, and that's ok, nothing bad happens). But... in this case, I want to know what means if this led number is going from "off state" to "on state" (remains solid along "x" minutes, and come back "green" again. "Solid state"(without light) could be at the beginning of play command, or even after, at any point). I have seen this behavior could be different playing the same file a lot of times. I don't know if this behavior could be related with something bad on my sd card, but, I changed the sd card using a new one, and the same  behavior is there (just a little different, but remains). I'm trying to figure out why my board suddenly reboots, and this could happen after some hours of work (nothing appears to be hot on my board, and my power supplies are grounded). To be honest, I believe this strange behavior could be related with bad state of my original sd card (it was used a lot of times), after I changed it, the led's behavior changed and it can be seen "different" and "better" than the previous one (that could be seen when I'm waiting to enable the sd card and copy my gcode file on it). I will continue with my testings, and see if this could be the reason to avoid random smoothieboard reboots. I have seen everything, config file, external buttons, and all wiring apppears to be ok (I'm using usb cable with ferrite beads, it's length is 480 mm) , but I don't know if there are more reasons that could cause this strange behavior. What happen if I install 4 or 5 axis firmware on board and sometimes all axes are used, and sometimes only 3 axes are used, it could be bad ?. At the moment, I have seen three to four reboots along 1 hour (after some hours of work). Any comment is appreciated. Thanks in advance.





**"Antonio Hern\u00e1ndez"**

---
---
**Jonathon Thrumble** *April 30, 2018 18:04*

I've only ever had this with sd printing where I Havnt disconnected from the pc properly. 


---
**Antonio Hernández** *April 30, 2018 19:07*

Ok **+Jonathon Thrumble**. By my side, the sd card is properly removed each time I have to copy gcode files, even I have disabled usb ports to avoid concurrent readings while smoothieboard is working, that solved other kind of issues that I had seen before.


---
**Antonio Hernández** *April 30, 2018 19:17*

I was wondering if it's bad to have enabled "new_status_format" property. I'm doing some testings with bCNC and cncjs. Sometimes, I forgot to disable that property (when I'm not working with bCNC) and I don't know if this property does not work properly if it's enabled and not used.


---
**Wolfmanjm** *April 30, 2018 20:55*

led 2 and 3 are pretty meaningless, one is flashed everytime the idle loop happens the other flashes in the mainloop. the mainloop one can stop when printing or flash very slowly, the idle one should never stop.


---
**Wolfmanjm** *April 30, 2018 20:56*

new format is harmless.


---
**Antonio Hernández** *May 01, 2018 00:44*

So **+Wolfmanjm**, mainloop one is led 2  and led 3 is the one that never stops ?. I thought led 2 could be related with something bad on sd card, but, finally the behavior (related to my question) is meaningless. Also, new_format property could be enabled without matter if its used or not for bCNC program, is that correct ?


---
*Imported from [Google+](https://plus.google.com/107343842763021715447/posts/HwcK1jMj4uY) &mdash; content and formatting may not be reliable*
