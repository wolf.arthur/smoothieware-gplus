---
layout: post
title: "Hi guys. I just wanted to stop by and say thank you for all the help I got while building my cnc router"
date: May 01, 2018 11:45
category: "Machine showcase"
author: "Chuck Comito"
---
Hi guys. I just wanted to stop by and say thank you for all the help I got while building my cnc router. I have a few more things to fo bit it is operational. 





**"Chuck Comito"**

---
---
**Don Kleinschnitz Jr.** *May 01, 2018 13:09*

Lookin good! 

What was the conclusion regarding the spindle driver problem that you had? I think it was you I am thinking of.

What is your current setup for the spindle and does the spindle turn the speed the Gcode expects? 


---
**Sébastien Plante** *May 01, 2018 22:29*

You are not afraid of the dust :o



Nice machine btw, look very stable !


---
**Chuck Comito** *May 02, 2018 00:13*

**+Don Kleinschnitz**, I was having issues with the spindle but I remember the other guy was having issues as well with a knock off VFD so it could be him you're remembering. I solved my issue with:



# Switch module for Spindle control

switch.spindle.enable                            true 

switch.spindle.input_on_command      			 M3 

switch.spindle.input_off_command    			 M5 

switch.spindle.output_pin                        1.23  

switch.spindle.output_type                       pwm  

switch_spindle_maximum_power        			 1  

switch_spindle_minimum_power        			 0.0

switch_spindle_default_power        		     0.3 

switch_spindle_pwm_period            		     400



My stepper drivers and stepper power supply are undersized but they work. I'll upgrade those next. The spindle I have is very weak with lots of play. I can't do aluminum yet because of if. The rest of the build is strong.



**+Sébastien Plante**, yes the dust is terrible. I wouldn't recommend doing this type of work in your basement. I was just working out the bugs. It will soon be moving to my garage!


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/dj9MN83S5NZ) &mdash; content and formatting may not be reliable*
