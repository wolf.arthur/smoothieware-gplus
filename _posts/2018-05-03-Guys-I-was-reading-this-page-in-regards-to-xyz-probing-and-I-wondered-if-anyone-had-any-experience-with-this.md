---
layout: post
title: "Guys, I was reading this page in regards to xyz probing: and I wondered if anyone had any experience with this?"
date: May 03, 2018 00:33
category: "General discussion"
author: "Chuck Comito"
---
Guys, I was reading this page in regards to xyz probing: [http://linuxcnc.org/docs/2.6/html/gcode/gcode.html#sec:G38-probe](http://linuxcnc.org/docs/2.6/html/gcode/gcode.html#sec:G38-probe) and I wondered if anyone had any experience with this? Is this a built in feature within the smoothie firmware using G38.x gcode commands? If so, I'd imagine it would have to be defined in the config file as well? I might be completely off base but none the less the idea was intriguing. 





**"Chuck Comito"**

---
---
**j.r. Ewing** *May 03, 2018 02:32*

i cant see probing working in a home built machine running off belts and acme lead screws. anything better than .01 inches. you need a probe on spindle centerline. a way to qualify the probe on xy and z and sphere diameter plus probing a part on the same machine it was made on is the worst way to check parts besides a tape measure.





the one use i could see is edge find 3 points to find enter of a circle if you had a mill Fadal's did this years ago. the entry haas renishaw probe is 5 grand thats 10x what i have into my printer. just for tool holder. 



if your thinking's point cloud to reverse engineer stiff get a laser scanner even if you have a point cloud you'll need a vector from center of probe to surface normal also



i have yet to see a home built machine that can hit true position under.01 repretadly




---
**Wolfmanjm** *May 03, 2018 08:50*

yes G38.2 - .5 is built in by default in smoothie. I use it regularly. (.4 and .5 were recently added)




---
**Arthur Wolf** *May 03, 2018 11:10*

Thanks **+James Richters** btw for the recent additions. can't wait to start using these.


---
**Chuck Comito** *May 04, 2018 16:21*

**+j.r. Ewing**, you're right about the belts and lead screws. But ballscrews on a solid machine could keep that tolerance! I think I'm going to mess around with it a bit and see if I can't build myself a nice probe! :-)


---
**James Richters** *May 04, 2018 19:01*

I have circle probe functions working now too... but they are non-standard Gcodes...  I could not get them to work smoothly with G38.3 and G38.5.



It's useless to probe in just an arc... why bother defining an end point, just do a complete circle until the probe hits, so I always probe in a complete circle back to the start point... 



The commands I used to test circular probes are:

G38.6 - probes in a clockwise direction back to current point until probe is hit

G38.7 - probes in a counter-clockwise direction back to current point until probe is hit

G38.8 - probes in a clockwise direction back to current point until probe is released

G38.9 - probes in a counter-clockwise direction back to current point until probe is released

none of these halt on errors (like G38.3 and G38.5)



My circular probe commands only accept I and J center center points relative to current position and will probe a complete circle back to the current position (or stop when probe is activated of course) they also accept F feedrates.  



The idea is to use circular probes to digitize parts of unknown shapes.  Here's how it is supposed to work... You find one edge of your part with a linear probe, like G38.3,  next you do a slower reverse probe with G38.5, now the probe is not longer touching and you can probe again... It's important you have your debounce settings correct so you get a firm contact on G38.3 and a firm release on G38.5  now you record this position.  I then perform a complete circular probe, setting only the I,J center point away from the current location and feedrate. with somethng like G38.7 at some point around this circle, I MUST hit the part again, no matter where I am on the part, no matter what angle the part may have.. once I hit the part again, I do a reverse circle probe slower to barely get off the probe, with something like G38.8 and now I record my point and I am also ready to probe again.  Using G38.8 to do a reverse circle off the part guarantees you will not hit the part at another point, because you are reversing the circle you made to do the probe.... but there is a catch to this.. the reverse circle has a completely different center point!!!  the new center for the reverse probe MUST be re-calculated as it is also relative to the new position.   Now the probe is released, I figure out a new center point and do this in a loop until the entire part is digitized... the tricky part is figuring out where the next center point should be... that is done by the host, and I'm still working on that part... lots and lots of trig involved here... and you can't just have a single method of calculating the next center point.. you have to determine if you went around a corner, or if you are pretty much inline with the direction you were doing.. etc... and calculate a new center accordingly.



I'm considering making I and J absolute coordinates, because then the forward probe and reverse release probe would have the same I,J values...  The host would need to get the current position to calculate absolute I and J... either way the host has to do all the math for I,J



This is still in testing.... but here's a link if anyone is interested:



[github.com - Smoothieware](https://github.com/Zaaphod/Smoothieware/tree/Probe_Circle)




---
**Chuck Comito** *May 06, 2018 00:34*

**+James Richters** this sounds great but I can't get my head around the arc need. Maybe a video of it in use?


---
**James Richters** *May 06, 2018 00:38*

**+Chuck Comito** It's for automatic digitizing parts of unknown geometry, it allows you to find corners and keep probing even if they are really sharp inside or outside corners.   I'll make a little video next week


---
**Chuck Comito** *May 06, 2018 00:44*

That'd be great **+James Richters** !


---
**Chuck Comito** *May 25, 2018 16:53*

**+James Richters**, any update on the video? Also, do you have a connection diagram or webpage that has information on physical connections?


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/8eNa87L3uro) &mdash; content and formatting may not be reliable*
