---
layout: post
title: "Hey, I need you help with a modbus thing !"
date: May 10, 2018 19:35
category: "General discussion"
author: "Arthur Wolf"
---
Hey, I need you help with a modbus thing ! I'm working on something really awesome for Smoothie ( more soon ) but I'm stuck on controlling a spindle VFD and can't make progress until that's fixed.



The spindle manual is at : [http://sion.rs/download/sunfar/E550manual.pdf](http://sion.rs/download/sunfar/E550manual.pdf) I'm trying to talk to it using "modbus" which I assumed is the same Smoothie supports ( the manual sorts of suggests their modbus implementation should be compatible with "common" vfds )

But when I send commands ( I made sure they are sent with a logic analyzer, and I made sure the right registers are set ), the VFD won't react to any of the commands.



My problem is I can't figure out from the manual if the protocol they define ( around page 90 ) is the same Smoothie outputs, see here : 

[https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/tools/spindle/HuanyangSpindleControl.cpp#L23](https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/tools/spindle/HuanyangSpindleControl.cpp#L23)



If it's the same protocol I am messing up somewhere ( ideas welcome )

If it's not the same protocol then I need to implement the new protocol into Smoothie, but I can't understand the spec ( in particular if the values in tables are always bytes are if they also are pairs of bits sometimes where I see 10 and  01 etc. would really have helped if they had added 0x / 0b etc ). Can somebody make sense of it for me so I can do the code ?



Thanks a lot to you all :)



Also v2 news coming soon.





**"Arthur Wolf"**

---
---
**Antonio Hernández** *May 10, 2018 19:58*

I don't know if this could help, but I had the same issue along my testings. Finally, I found these two chinese circuits that talked perfectly modbus protocol. I made some testings with smoothieboard, and, using these two circuits, worked. In some scenarios (by my side), the rmp's could be 20 units more than requested (for example, request 15,000 rpm and the vfd gave me 15020 - 15015). Direction pin (2.5) could not be used. I made some testings sending 8,000 rmp and 24,000 rpm and the result was perfect.

[es.aliexpress.com - Ttl a RS485 módulo RS485 a TTL con aislado solo chip UART puerto serie](https://www.aliexpress.com/item/TTL-to-RS485-module-RS485-to-TTL-with-isolated-single-chip-serial-port-UART/32820863224.html?spm=a2g0s.9042311.0.0.fyFwys)


---
**Antonio Hernández** *May 10, 2018 20:00*

This is the second one. Hope this could help. (Both of them worked). The final comment is, I tried using texas instruments circuit (MAX3485) and another kind of modbus circuits, and no one of them worked (for me). 

[es.aliexpress.com - Microcontrolador ttl a RS485 módulo, 485 módulo de comunicación serial UART Conversión de nivel, el flujo automático](https://www.aliexpress.com/item/microcontroller-TTL-to-RS485-module-485-to-serial-communication-module-UART-level-conversion-automatic-flow/32397727387.html?spm=a2g0s.9042311.0.0.fyFwys)


---
**Arthur Wolf** *May 10, 2018 20:00*

I'm using a sparkfun breakout, and I have confirmed it works with a logic analyzer, so that's not the problem. Thanks a lot for the information though, and glad it's on here as it could be useful to others.




---
**Antonio Hernández** *May 10, 2018 20:04*

Just for curiosity, I tried that sparkfun breakout board and didn't worked for me. It appeared to have communication with the vfd (led blinking for each command) but the vfd didn't started the motor. I tried different ways and no one worked.


---
**Antonio Hernández** *May 10, 2018 20:09*

I don't know if you made some special "fix" to work with that breakout. I followed the steps on the guide and, nope, it could not work for me.


---
**Arthur Wolf** *May 10, 2018 20:12*

Well I tried that board with three other VFDs and it worked for all of them. This and the logic analyzer test tells me it's likely alright, but I'll buy some of those boards you linked anyway. They'll take weeks to arrive though, I hope I have a solution before that ...


---
**Antonio Hernández** *May 10, 2018 20:20*

In my case they arrived after 30 days. I made some testings using both circuits and worked. Maybe using shielded wires could help the rpms to be more accurate.  I tried different ways (even using MAX485). Using max485 (not max3485, max485 is used for 5v signals) I could start the spindle, and stop it, but the requested rpm's always were wrong. Even there is a youtube user that used a similar circuit to start and stop the vfd using modbus protocol, but you can see he requested 10,000 rpm's and the vfd gave always 3,000 or a similar number, without mather the rmp's requested the result was always the same, and also that happened to me using max485 circuit. There is a sparkfun user that told the chip used on breakout board that sparkfun sells it's not the good one to work with modbus protocol. This is the video that I mentioned before...


{% include youtubePlayer.html id="tGGU0smK2Wc" %}
[youtube.com - Smoothieboard driving VFD with Modbus commands](https://www.youtube.com/watch?v=tGGU0smK2Wc)


---
**Antonio Hernández** *May 10, 2018 20:29*

I tried with these boards, and no one worked for me (just for reference).  [sparkfun.com - SparkFun Transceiver Breakout - RS-485 - BOB-10124 - SparkFun Electronics](https://www.sparkfun.com/products/10124) <==> [https://www.aliexpress.com/item/MAX3485-module-TTL-to-RS485-Usart-communications-accessories-serial-parallel-multi-machine-communication/32305987484.html?spm=a2g0s.9042311.0.0.CjFK4v](https://www.aliexpress.com/item/MAX3485-module-TTL-to-RS485-Usart-communications-accessories-serial-parallel-multi-machine-communication/32305987484.html?spm=a2g0s.9042311.0.0.CjFK4v) <==>  [https://www.aliexpress.com/item/Microcontroller-TTL-to-RS485-module-485-level-serial-UART-conversion-automatic-hardware-flow-control/32581784201.html?spm=a2g0s.9042311.0.0.CjFK4v](https://www.aliexpress.com/item/Microcontroller-TTL-to-RS485-module-485-level-serial-UART-conversion-automatic-hardware-flow-control/32581784201.html?spm=a2g0s.9042311.0.0.CjFK4v)


---
**Antonio Hernández** *May 10, 2018 20:37*

I forgot to mention, and I don't know if this could be important, my vfd belongs to Huanyang brand. It's for 2.2kw


---
**Miguel Sánchez** *May 11, 2018 13:34*

There seem to be several choices that can go wrong with Modbus, like binary or ascii implementation or the way of calculating LRC. No answer is an indication that your request is either not receiver or not understood. I assume, serial format is ok (8N1 or whatever), baudrate too, and you are using the right address ...  I get some success with Huangyang brand. For it it was mandatory to set the frequency input and the start input to the serial comms for these to have an impact. If you want to play with my code you it is linked in the blog entry [fightpc.blogspot.com.es - VFD control with Arduino using RS485 link](http://fightpc.blogspot.com.es/2014/10/vfd-control-with-arduino-using-rs485.html)


---
**Bernt Weber** *May 15, 2018 22:29*

Did you solve your Modbus problem? The error could be just a bad configuration of the VFD, that is not configured to take modbus commands. If that could be the error, search for "communication" and "modbus" in the parameter list to activate it everywhere. 


---
**Arthur Wolf** *May 16, 2018 09:13*

I finally understood the datasheet, and it's using a different protocol

from the Huangyang VFDs Smoothie is programmed to talk to. I'll need to add

a new module to Smothie to talk to them, I'll do that when I get +Antonio's

boards, in the meantime I'm limited to On/Off, which I can live with for

the moment.


---
**Antonio Hernández** *May 27, 2018 05:15*

**+Arthur Wolf**, the same behavior that you got it's possible to see if you use max485 breakout (only turn on or turn off commands worked (M3, M5), but the requested rpm's always be wrong and the spindle starts showing rpm values that does not have a relationship with the requested values). Those two boards mentioned before worked very well (both breakouts were tested with 8,000, 10,000, 15,000, 20,000 and 24,000 rpm's and in some scenarios some rpm's are missing (example: smoothie requested 10,000 using any of the two breakouts mentioned before and the vfd shows 9960 on display..., smoothie requested 15,000 rpm's and the vfd shows 15015 - 15020...) but there are other cases where the requested rpm's are the same that the vfd shows on display (24,000 rpm's for example), shielded wires could help to improve the accuracy of requested rpm's (I suppose). Another point to be considered is the minimal frequency the spindle could support. Simple maths must be done to set the quantity of rpm's when spindle is starting in it's minimal operational frequency (there are users that said the minimal operation frequency also depends of spindle's architecture, water cooling or air cooling have a different minimal starting frequency, so, the accuracy on requested rpm's is also related with that minimal frequency). In this moment I could not remember the meaning of program id from inverter (PD144), but the value of that program also it's very important to achieve more accuracy on requested rpm's. "Minimal frequency" programs that were changed on VFD (Huanyang brand) were PD011 and PD073 (I suppose it's possible that more programs could be changed for the same purpose, but by my side, only those two programs were enough). It's very very nice to know that maybe a tool change for smoothieboard is on the way, but I don't know if pin 2.5 for vfd purposes could be very important to achieve that tool change process...that pin can not be used with those two boards mentioned before..It will sound pretty obvious, but turn on and turn off commands worked without problems...


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/E2spXw6dgCp) &mdash; content and formatting may not be reliable*
