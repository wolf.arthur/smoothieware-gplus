---
layout: post
title: "Printed this on my upsized Cobblebot: Scaled up to 750mm x 250mm x 200mm Sunlu Gold PLA 0.4mm Nozzle 0.32mm layer height vase mode Took about 40 hours to print"
date: May 17, 2018 19:41
category: "Machine showcase"
author: "Dushyant Ahuja"
---
Printed this on my upsized Cobblebot: 

[https://www.thingiverse.com/thing:1546001/#files](https://www.thingiverse.com/thing:1546001/#files)

Scaled up to 750mm x 250mm x 200mm

Sunlu Gold PLA

0.4mm Nozzle

0.32mm layer height vase mode

Took about 40 hours to print

![missing image](https://lh3.googleusercontent.com/--ImGRdWYJaU/Wv3a6aDSWJI/AAAAAAABmXc/bf7LoNv_B3YUKvaVuRfTcTyFgcHWNloXwCJoC/s0/IMG_20180517_184403.jpg)



**"Dushyant Ahuja"**

---


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/QwvTjBHxaW5) &mdash; content and formatting may not be reliable*
