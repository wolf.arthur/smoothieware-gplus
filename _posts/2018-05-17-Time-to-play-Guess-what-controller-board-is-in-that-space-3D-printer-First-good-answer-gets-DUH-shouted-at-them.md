---
layout: post
title: "Time to play : Guess what controller board is in that space 3D printer : First good answer gets \"DUH\" shouted at them"
date: May 17, 2018 14:34
category: "General discussion"
author: "Arthur Wolf"
---
Time to play : Guess what controller board is in that space 3D printer : [https://3dprint.com/213793/consortium-delivers-esa-3d-printer/](https://3dprint.com/213793/consortium-delivers-esa-3d-printer/) First good answer gets "DUH" shouted at them.





**"Arthur Wolf"**

---
---
**Anthony Bolgar** *May 17, 2018 15:01*

My favorite board of course!




---
**Marc Miller** *May 17, 2018 15:23*

That's so cool.  :] 


---
**Hakan Evirgen** *May 18, 2018 11:34*

could not find anywhere a clue, what board they use...


---
**Arthur Wolf** *May 18, 2018 11:37*

**+Hakan Evirgen** Smoothieboard


---
**Johan Jakobsson** *May 18, 2018 16:32*

**+Arthur Wolf** V2? ; )


---
**Hakan Evirgen** *May 20, 2018 11:50*

**+Arthur Wolf** I was thinking so but where did they write it?


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/PxaHKzRsqvq) &mdash; content and formatting may not be reliable*
