---
layout: post
title: "Does anyone have any experience with the Azteeg X5 GT config setup for 0-10v VFD analog control & getting the Network up and running"
date: May 20, 2018 05:07
category: "General discussion"
author: "Jay Smith"
---
Does anyone have any experience with the Azteeg X5 GT config setup for 0-10v VFD analog control & getting the Network up and running. I've tried multiple config' but nothing. I've search smoothieware and Panucatt' websites, as well as anything on the internet I could find without any luck.

Any help or suggestions would be greatly appreciated.



Thanks



Jay





**"Jay Smith"**

---
---
**Arthur Wolf** *May 21, 2018 16:10*

[smoothieware.org - spindle-module [Smoothieware]](http://smoothieware.org/spindle-module) ?


---
**Jay Smith** *May 22, 2018 10:56*

**+Arthur Wolf** Unfortunately Smoothieware (or Panucatt) doesn’t cover the built in 0-10 vfd speed control function of the X5 GT. 



Thx



Jay


---
*Imported from [Google+](https://plus.google.com/113109603299108977985/posts/ZfhRWUjdRw6) &mdash; content and formatting may not be reliable*
