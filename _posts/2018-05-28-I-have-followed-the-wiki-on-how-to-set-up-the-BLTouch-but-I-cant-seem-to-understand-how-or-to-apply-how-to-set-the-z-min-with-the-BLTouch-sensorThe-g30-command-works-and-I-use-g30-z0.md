---
layout: post
title: "I have followed the wiki on how to set up the BLTouch but I can't seem to understand how or to apply how to set the z min with the BLTouch sensor.The g30 command works and I use g30 z0"
date: May 28, 2018 17:58
category: "Help"
author: "Antonio Gil"
---
I have followed the wiki on how to set up the BLTouch but I can't seem to understand how or to apply how to set the z min with the BLTouch sensor.The g30 command works and I use g30 z0. and store it with m500.The problem I am having and I have read soo many threads and I can't find the answer is how to set the homing position for z axis because after I store those settings if I hit g28 to home the z axis keeps trying to go lower until it hits the mechanical limit of the printer and keeps trying to go lower.Can someone please give me a hand with this I have been at this for almost two months and I am at my breaking point with this.Thanks





**"Antonio Gil"**

---
---
**Johan Jakobsson** *May 28, 2018 19:04*

The probe is used to probe, not set Z min.

You can use G30 to manually probe Z min but there is no automatic way of doing it (with a bltouch) as far as I know.


---
**Johan Jakobsson** *May 28, 2018 19:08*

The way I have it set up is with a endstop at Z max to home. Then I probe down to Z min with G30 and add the distance between probe and nozzle when setting M306 Znn


---
**Antonio Gil** *May 28, 2018 20:21*

So I am guessing the easiest way would be to add a z min endstop. I was really hoping I wouldn't have to do that because I have no way of printing my own mounts for the endstop now that I swapped parts to build this machine. I may have to see if someone is kind enough to print a few for me or pay someone to do it. 


---
**Antonio Gil** *May 28, 2018 20:23*

I have read tons of threads online where people say they have and haven't been able to use the Bltouch as a z min endstop. I hate that about looking online if you look long enough you can find contradictions to just  about anything. 


---
**Johan Jakobsson** *May 28, 2018 21:17*

afaik, the official standpoint from the "smoothie team" is that probes shouldn't be used as endstops. Especially true for the bltouch since you have to extend it before use and that isn't implemented in the homing routine.

Still not sure what your setup looks like. I'm assuming you have i3 style printer with and endstop at the bottom so you home to Z min, ie downwards?

My suggestion is: 

Home the machine then move the nozzle so it just touches the bed using a piece of paper to feel when it touches. 

Enter M306 Z0 in your host software terminal (pronterface?)

Home again.

Now you should be good to go to print. To probe Z0 if you want/need to;

Move to Z10  in the middle of the bed or wherever you want to probe (G1 Z10 F200 in the terminal.)

Extend probe with M280_S3.0 and probe Z using G30. Check terminal to see the probed distance. The difference between the G30 and and 10 is the (z axis) distance - lets call it Dist - between the probe and the nozzle. Retract probe with M280_S7.0.

If you ever need to probe Z0 again you use this fast, but manual, routine;

G28

M280_S3.0

G0 Z10 F2000

G30

M306 Z = 10 - ((10-Dist) - G30 Output )



Say your Dist is 0.3mm and the output of G30 is 10.2mm,  the calculation would be 10 - ((10-0.3) - 10.2 ) which is 10 - (9.7 - 10.2) which is 10 - (-0.5) ie 10 +0.5 = 10.5 so your command would be: 

M306 Z10.5

I know my probe is 0.2mm from my nozzle so I just use this calculation: M306 Z = 10 - (9.8 - G30 Output )

That said, you'll still use your endstops for homing. The probe is for probing. =)


---
**Arthur Wolf** *May 30, 2018 23:18*

A probe isn'ta zmin endstop, and vice-versa, but there's no limit on what you can do with your zprobe. If you want to use it to set your bed height ( with G30 ) just do that whenever you feel like it.

I think some users get confused because other firmwares mix them up and Smoothie has them well separated. 

Just use G30 instead of G28 to home your Z.


---
**Antonio Gil** *June 01, 2018 18:17*

I added a z min endstop to fix the problem. Everything is working now but when I try to probe the bed using g32 it starts in the middle of the bed. I am using a 220x220 bed. I was reading the wiki and it states that on a Cartesian when using an endstop at the min location to start in the center of the bed the alpha min should be - 110.would I also have to add the distance of the nozzle to edge of bed when triggering the endstop? 


---
**Arthur Wolf** *June 01, 2018 18:21*

You really shouldn't have to add a zmin endstop, a probe is all you need. It's ok if you do though.

Do you want your machine to have it's 0,0 in the center of the bed, or in the lower corner of the bed ( most machines do the second one ) ?


---
**Antonio Gil** *June 01, 2018 18:24*

It really makes no difference to me. I am just trying to make sure that the board knows where the bed actually is like I mentioned before the g32 command is starting the first probe point in the center and working its way to the left edge of the bed. 


---
**Arthur Wolf** *June 01, 2018 19:00*

Can I see your config file and the exact command you send ?


---
**Antonio Gil** *June 01, 2018 19:47*

I am using g32 and I have tried g28 but g28 doesn't work when entered into Octoprint.

[dropbox.com - config.txt](https://www.dropbox.com/s/ev1ul1ij4hhx9se/config.txt?dl=0)


---
**Arthur Wolf** *June 01, 2018 21:16*

Can I get the exact answer to the "version" command ?


---
**Antonio Gil** *June 01, 2018 23:55*

What do you mean by version command?I have no clue what that means?


---
**Arthur Wolf** *June 02, 2018 19:15*

go to the terminal tab in octoprint and type "version" then give us the answer it gives you ( precisely )


---
**Antonio Gil** *June 02, 2018 22:52*

Here is what it said



Send: version

Recv: Build version: edge-c819aa6, Build date: Apr  4 2018 16:52:08, MCU: LPC1768, System Clock: 100MHz

Recv: 5 axis


---
**Antonio Gil** *June 03, 2018 08:57*

If you need any more info just let me know. I just need to clean up my wiring a bit and start printing. 


---
**Arthur Wolf** *June 03, 2018 12:42*

Please ask MKS for help first and read : [smoothieware.org - troubleshooting [Smoothieware]](http://smoothieware.org/troubleshooting#somebody-refused-to-help-me-because-my-board-is-a-mks-what-s-that-all-about)


---
**Antonio Gil** *June 03, 2018 20:22*

Arthur I get not wanting to help someone who is using the knockoff boards but I have both and am trying to get the MKS running so that I can swap in the Smoothie with out damaging it or the printer.I am brand new to all of this and it seems like you guys don't really want to bother helping.I get that if I study the wiki I should know how to do it but I don't and all I want to do is print.I think that approach to helping new users will hurt the Smoothie project because it will frustrate new users into going in another direction.I appreciate the help you have given me but I am stuck and cannot print at the moment and I have to figure out what to do.


---
**Arthur Wolf** *June 03, 2018 20:43*

I'm sorry but reading " it seems like you guys don't really want to bother helping." just makes me upset, especially when this thread has 12 messages.



I spend several hours a day helping people with smoothie questions ( and live like a student at 33 because I've spend all my days the past 5 years helping people with smoothie issues, pushing smoothie forward, and spent all my money for smoothie too ), we definitedy do help, if you don't think so you have no idea what's going on here

. We just want to not waste that ( precious ) time doing MKS's job, it ensures we help people who deserve our help first. We tend to help everybody in the end, but we ask MKS users to ask MKS first when they have problems with that board. I don't think that's too much to ask.



I have been helping you. When I discovered you were a MKS user ( which you were not upfront about ), I only asked that you ask them first, which should be something you should have done anyway. MKS should help you before volunteers do. It's the least they can do.



About your issue I'm still confused what is the problem, this works for everyone else out of the box. Could you please make a video of trying to do G32 ? Start from when you boot the printer, show the commands you send, and send M114+ show it's answer before and after doing G32.








---
**Antonio Gil** *June 03, 2018 21:54*

I have posted a few times on here and in my first post I stated that I bought an MKS and  I also have a Smoothie.I am using the MkS to make sure everything is working fine before I use the Smoothie.I rather throw away 30 bucks than 100+ if I fry something.I get you must be swamped with issues everyday and every ones time is extremely valuable.I just wanted to point out that when starting out.You have to learn quite a bit and many things are handled differently between controllers..I will post a video in a little while.Thanks


---
**Antonio Gil** *June 04, 2018 04:05*

Ok Arthur I will get that video up but just checking the M114 code on my setup I noticed alpha and beta are reporting the wrong coordinates.When alpha and beta are on the min endstops and they are triggered the coordinates that I get after entering the M114 code are 



Recv: ok C: X:-470.0000 Y:180.0000 Z:0.0000 E:0.0000



I'm guessing that the g32 issue I am having isn't due to the leveling having issues but that the machine doesn't know where the extruder is in relation to the bed.You took a look at my config.I am using a  220x220x200 operating height machine.


---
**Antonio Gil** *June 04, 2018 06:38*

The g28 command doesn't work either whenever I send the g28 I hear a very low humming from the motors I think but they don't move. I can however move them through Octoprint with the arrow keys. 


---
**Johan Jakobsson** *June 04, 2018 07:30*

So when you get the MKS board up and running and nothing has fried you'll go and buy a genuine smoothieboard? 

Somehow I doubt that.

I can assure you BLtouch works on a genuine smoothieboard as I have that exact setup. I suggest you order one.




---
**Antonio Gil** *June 04, 2018 07:44*

I already have a Smoothieboard.I purchased the MKS as a 30 dollar write off in case I fried it.I  often do this when trying out new stuff.Like I said before I stated this in my first two or three posts on here.The BLTouch works it is just starting the first attempt at probing on the right side of the bed and continues towards the right.I also can't get g28 to work it says ok as the response on Octoprint but it doesn't move I just hear a faint humming sound.


---
**Johan Jakobsson** *June 04, 2018 07:47*

**+Antonio Gil** MKS boards are notorious for generating strange issues. 

I suggest you try it with your smoothieboard.




---
**Arthur Wolf** *June 04, 2018 08:31*

G32 will not work without a homed machine ( see documentation, this means you have to do G28 before you do G32 ).

If G28 doesn't work the way you describe, it likely means the motors are stalling because you are trying to move them too fast, try reducing your acceleration and homing speeds ( MKS is famous for stalling too easily ).


---
**Antonio Gil** *June 05, 2018 12:17*

Arthur I have everything working as it should the endstops and the BLTouch are working fine but when I home with g28 and then use g32 it raises the extruder an then comes back down and crushes the z min endstop.On its way down the endstop is triggered but it doesn't stop.G28 works as it should.


---
**Antonio Gil** *June 09, 2018 04:56*

Any idea what may be causing this? Everything seems to be working correctly I just can't seem to figure out why the extruder lifts up tgen comes crashing down onto the endstop after homing with g28 and then attempting g32. 


---
**Arthur Wolf** *June 12, 2018 13:34*

Could I get a video with both the machine's behavior, and pronterface's console before and after the G28 and G32 ?


---
**Antonio Gil** *June 12, 2018 18:04*

Here is a short vid of g28

[dropbox.com - 20180612_135316_58898050325584.mp4](https://www.dropbox.com/s/vkzade4gyxjuczr/20180612_135316_58898050325584.mp4?dl=0)



Here is a quick vid of the Octoprint terminal during the g28



[https://www.dropbox.com/s/r134jhj34juanhm/20180612_132134.mp4?dl=0](https://www.dropbox.com/s/r134jhj34juanhm/20180612_132134.mp4?dl=0)



The endstop bracket broke during the g32 command so I couldn't get you that vid but the next vid is of the terminal in Octoprint during the g32



[https://www.dropbox.com/s/g67s4z37cnrxsc7/20180612_135346_58928659723973.mp4?dl=0](https://www.dropbox.com/s/g67s4z37cnrxsc7/20180612_135346_58928659723973.mp4?dl=0)



The g32 commend when I use it and the printer is homed it drops until it activates he z min endstop then raises up maybe 10-20mm and then continues to drop down in the homed position untl it activates the zmin endstop again but continues to try to go lower and just crushes the z min andstop to bits and pieces.I have already crushed three endstops.I don't know if you specifically need Pronterface because I don't use it but if I get the bracket fixed for the endstop I will be more than happy to download it and give you the results.


---
**Arthur Wolf** *June 16, 2018 11:31*

You say the problem is when you do G28 then G32 it does something weird, I was expecting a video showing that ...


---
**Antonio Gil** *June 17, 2018 09:43*

The endstop and bracket broke while I was trying to film it. I am waiting for a new one since I can't print it at the moment. The g32 command is the problem it activates the probe pin down and then with the extruder in the home position it lifts 30-50mm and then comes down in the same position until it hits the endstop and continues trying to go lower. 


---
**Antonio Gil** *June 17, 2018 23:06*

Here is the video of what g32 does I stopped the printer before it could crush the endstop again

[dropbox.com - 20180617_190833_62070478597596.mp4](https://www.dropbox.com/s/sz4vxo5kkgko7je/20180617_190833_62070478597596.mp4?dl=0)


---
**Antonio Gil** *June 17, 2018 23:08*

It would have triggered the endstop and continued to try to go lower breaking the endstop.


---
**Antonio Gil** *June 24, 2018 23:35*

Does anyone have any clue why it is doing that?


---
**Antonio Gil** *June 28, 2018 08:36*

Any ideas on what can be causing this Arthur?


---
**Arthur Wolf** *June 30, 2018 10:21*

I'm sorry I have such a hard time understanding what's wrong. I've re-read

everything in this post and watched the videos and I'm still lost ...

What's the result of M114 after you do a G28 ?


---
**Antonio Gil** *July 03, 2018 06:17*



Arthur you helped me figure out quite a few problems so there is all kinds of information in here.



After the G28 command is issued and it homes the M114 shows



Recv: ok C: X:0.0000 Y:0.0000 Z:0.0000 E:0.0000

[...]



Let's just say I finished wring up the BLTouch for the first time and made sure it is working correctly.Would it work by just entering g30 and then g28 and finally g32?






---
**Arthur Wolf** *July 03, 2018 12:19*

Well first you do G28 so you are close to the bed, then you go up ( G1 Z20 ) so you are sure you don't hit the bed, then you move to the center of the bed ( this might be unnecessary but try : G1 X100 Y100 ), then you do your G32 which levels everything, then you do G30 zSomething so that the probe touches the bed and sets the Z position to "something". Also if you are using a bltouch you need to deploy the probe before you do all of this.




---
**Antonio Gil** *July 04, 2018 03:21*

Ok  G28 works 

G1 Z20   works

 G1 X100 Y100  works but th extruder only moves maybe 40-50mm to the corner of the bed.



G32 when it's centered on the bed following your steps it homes hits the z min endstop raises up then comes down and crushes the [endstop.It](http://endstop.It) does that from any position whenever G32 is issued it crushes the endstop.What I don't understand is if it hits it the first time and stops why doesn't it stop the second time?I also checked and made sure the endstops were being recognized as on or off with M119 and they are being read correctly.


---
**Antonio Gil** *July 09, 2018 03:39*

Arthur what would cause the board to recognize the endstop and stop the motors from moving when it is triggered and then ignore it?The G1 x100 Y100 command is supposed to center the extruder and it is only moving to the front left corner of the bed so I am guessing the bed dimensions are set up wrong somewhere in my config .I will post the latest config so maybe you can see something that I can't.

[dropbox.com - currentconfig4steps.txt](https://www.dropbox.com/s/ys9lf9klg7tds5w/currentconfig4steps.txt?dl=0)


---
**Arthur Wolf** *July 09, 2018 08:22*

You want to use M114 to detect where it thinks it is after homing, it's possible you need to adjust your homing offsets to make sure the board has an internal vision of it's position that makes sense.




---
**Antonio Gil** *July 23, 2018 18:04*

Ok Arthur I have gotten a little further figuring out my problem.When I issue the g28 then g1 x110 y110 command the extruder sort of centers on the bed and then I drop the probe and issue the g32 the extruder starts to lower but the problem now is that the bed homes to the endstop so the BLTouch never hits the bed.What would cause the bed to home after issuing the g32?I set the homing line to false in the leveling strategy.



Does the homing order section need to be active in the config?I have it commented out.Here is how it is set in my config



#homing_order                                 XYZ              # X axis followed by Y then Z last

#move_to_origin_after_home                    false            # Move XY to 0,0 after homing

#endstop_debounce_count                       100              # Uncomment if you get noise on your endstops, default is 100

#endstop_debounce_ms                          1                # Uncomment if you get noise on your endstops, default is 1 millisecond debounce

#home_z_first                                 true             # Uncomment and set to true to home the Z first, otherwise Z homes after XY






---
**Arthur Wolf** *July 23, 2018 18:41*

That's an option in your levelling strategy, just disable it.


---
**Antonio Gil** *July 23, 2018 19:34*

This is my leveling strategy and I have the homing line set to false is there another part that needs to be modified?Here is my leveling section of the config



'# Levelling strategy

 Example for 3-point levelling strategy, see wiki documentation for other strategies

leveling-strategy.three-point-leveling.enable         true        # a leveling strategy that probes three points to define a plane and keeps the Z parallel to that plane

leveling-strategy.three-point-leveling.point1         100.0,0.0   # the first probe point (x,y) optional may be defined with M557

leveling-strategy.three-point-leveling.point2         200.0,200.0 # the second probe point (x,y)

leveling-strategy.three-point-leveling.point3         0.0,200.0   # the third probe point (x,y)

leveling-strategy.three-point-leveling.home_first     false        # home the XY axis before probing

leveling-strategy.three-point-leveling.tolerance      0.03        # the probe tolerance in mm, anything less that this will be ignored, default is 0.03mm

leveling-strategy.three-point-leveling.probe_offsets  0,0,0       # the probe offsets from nozzle, must be x,y,z, default is no offset

leveling-strategy.three-point-leveling.save_plane     false       # set to true to allow the bed plane to be saved with M500 default is false


---
**Arthur Wolf** *July 23, 2018 19:41*

if you set home_first to false, it shouldn't home. try formatting your SD card and starting from a fresh config file off the wiki


---
**Antonio Gil** *July 23, 2018 19:56*

I'll try that but the weird thing is the only axis that homes is the bed the other two do not move.


---
**Arthur Wolf** *July 23, 2018 20:04*

and G28 homes all axes ? then it's not homing ...



why are you saying it is homing, does pressing the min endstop by hand at

that time stop the movement ?


---
**Antonio Gil** *July 23, 2018 21:41*

i have not tried manually stopping it but g28 works just fine and all of the endstops are working when i check with M119


---
**Arthur Wolf** *July 23, 2018 22:15*

I need to know if it's actually homing when you ask it to level, or if it's

doing something else which you mistake for homing. So when you think it is

homing, you need to press the endstop to see if it is actually homing or

not.


---
**Antonio Gil** *July 24, 2018 19:29*

It stops when I manually press the endstop after issuing the g32 command


---
**Arthur Wolf** *July 24, 2018 20:34*

so it's homing at the beginning of G32, despite you specifying in the config that you don't want it to ? I recommend you format the SD card, and start configuration from scratch using the config file on the wiki.


---
**Antonio Gil** *July 24, 2018 20:38*

I just did that with this card.I have 4 or 5 cards here and they are all doing the same thing.The cards are Sandisks and they are authentic.I am currently using  the config file and firmware bin supplied by Smoothie on their Github page and I also tried the config from the Smoothie site also.I can email you the config or link to it on here if you wouldn't mind taking a look at it.


---
**Arthur Wolf** *July 24, 2018 20:40*

email it to wolf.arthur@gmail.com


---
**Antonio Gil** *July 26, 2018 22:13*

Arthur the email isn't working with Gmail are you having any problems on your end? 


---
**Antonio Gil** *July 26, 2018 23:59*

Just in case I can't get th email to work.Here is a link to the config on my Dropbox

[dropbox.com - config original.txt](https://www.dropbox.com/s/mtgtz2xayp7iagk/config%20original.txt?dl=0)


---
**Antonio Gil** *August 02, 2018 01:00*

Hey Arthur did you ever get that email?No rush I just want to make sre you actually received it.


---
**Arthur Wolf** *August 04, 2018 14:50*

So to recap, you have a zprobe on 1.28, and a levelling strategy set to three-point on a cartesian. When you ask it to do levelling with G32, it starts homing the Z ( which I don't think three-point does even if it has homing_first to true ) even though you set homing_first to false. You know that it's homing and not probing because if you press the zmin endstop ( on 1.29 ) it stops.

Do I have that right ?



**+Wolfmanjm** I'm a bit lost here it's been 50 messages and I'm out of things to think of.


---
**Wolfmanjm** *August 04, 2018 15:08*

it will not home Z in three point when you do G32. as it will do G28 X0 Y0 if it is told to home first. So this is NOT a homing issue.


---
**Arthur Wolf** *August 04, 2018 15:13*

**+Antonio Gil** Are you <b>sure</b> it's homing ? Are you sure that when you issue G32, it moves towards the bed and stops when you hit the z endstop ( and not the zprobe ) ?


---
*Imported from [Google+](https://plus.google.com/111357991372285439712/posts/aCYZg8VasE8) &mdash; content and formatting may not be reliable*
