---
layout: post
title: "Anyone have issues with GLCD not working with smoothieware?"
date: May 31, 2018 03:09
category: "Help"
author: "Robert Hubeek"
---
Anyone have issues with GLCD not working with smoothieware? 



I have checked wiring, tried flipping tried using only a handful of wires to other troubleshooting guides. (below link being one) 

[https://3dprinterchat.com/2016/10/smoothiefy-your-3d-printer/](https://3dprinterchat.com/2016/10/smoothiefy-your-3d-printer/)



Turned contrast up and down



Running out of idea's the board is a Cohesion3D REMIX, i have asked the request there to but seeing as it is turning on and just not displaying anything i was thinking it could be firmware related. so any help would be awesome 



Below is config and firmware 

[http://www.technical.ciscokids.com.au/upload/config.txt](http://www.technical.ciscokids.com.au/upload/config.txt)

[http://www.technical.ciscokids.com.au/upload/FIRMWARE.CUR](http://www.technical.ciscokids.com.au/upload/FIRMWARE.CUR)

![missing image](https://lh3.googleusercontent.com/-oQE3DBTAdoQ/Ww9ned3w76I/AAAAAAAACMg/xsxTVhkEOkkiYoK0EcjGWPsbHphwz71_gCJoC/s0/33540644_10160543780805387_4118424310270394368_n.jpg)



**"Robert Hubeek"**

---
---
**Douglas Pearless** *May 31, 2018 03:19*

did you follow "By default, the SD Card on the Reprap Discount LCD conflicts with SD card on the board and you need to cut out some pins on the EXP2 connector (or connect only the encoder pins and the kill button pins). You need to connect only the following highlighted pins on the EXP2 connector:" from [3dprinterchat.com - Smoothiefy your 3D Printer &#x7c; 3D Printer Chat](https://3dprinterchat.com/2016/10/smoothiefy-your-3d-printer/) 


---
**Douglas Pearless** *May 31, 2018 03:21*

and the link to the config returned a blank webpage


---
**Robert Hubeek** *May 31, 2018 03:50*

**+Douglas Pearless** Yes copied the instructions and only connected the three pin's



[lh3.googleusercontent.com](https://lh3.googleusercontent.com/5kEwoCFiCoBfIlMEHYzgBY_ZFMEgmdum2mS0yM5lNkbrE0W7wHPuQW_CxvN0OwVJJE2vj4LxSw=w195-h147-n-rw)


---
**Robert Hubeek** *May 31, 2018 03:50*

Tried connecting it normally [lh3.googleusercontent.com](https://lh3.googleusercontent.com/iZJUwz0pokekYTWBmX51qbnQKa7ZRvc9lLiCyHyb88rjz8ycnU8YoDHj3UXp_l5xGWeR39WpVQ=w195-h147-n-rw)


---
**Robert Hubeek** *May 31, 2018 03:51*

Playing with contrast 



[lh3.googleusercontent.com](https://lh3.googleusercontent.com/u0CZta23WL_ym949DdP67d30gorvcGz261CtaXDWY4f9zPwv-r9qYGD7Vy-DzpP0rtW9HU7_wg=w195-h147-n-rw)


---
**Jeff DeMaagd** *June 03, 2018 11:25*

On some of the lcd clones, the connector housing was flipped so the tab was on the wrong side. It’s not a problem if you could have used their RAMPS adapter (housing also installed the wrong way) but if you’re not using RAMPS then you run into this problem.


---
**Robert Hubeek** *June 04, 2018 00:20*

**+Jeff DeMaagd** i'm under the impression it is a real one, but by flipped do you mean upside down or connector 1 is connector 2 type thing? 


---
**Marko Novak** *June 04, 2018 01:55*

flipped, meaning upside-down.

disassemble connector and flip it, or cut away orientation molding.



i had to do it too, with clone gLCD (red) and smintx6 board.


---
**Jeff DeMaagd** *June 04, 2018 03:03*

That's a clone glcd, the original manufacturer uses white boards. There's nothing wrong with that but the one I had had the socket wrong way around.


---
**Robert Hubeek** *June 04, 2018 04:30*

I have some single leeds, i will try flipping it tonight and see how it goes, i genuinely thought it was a legit GLCD but who knows these days. 




---
**Robert Hubeek** *June 04, 2018 09:27*

So was doing research and noticed the original with white boards looked more like the one on the bottom then top. With plugs correct it comes on as above just a blue square 

![missing image](https://lh3.googleusercontent.com/zlJkYQjkCvdA3n16BErod4P6X6OsxBProBrH9hTqe38Y8uvE4WWVDuj7EfppuN_QXqIrfX94kfXFmA=s0)


---
**Robert Hubeek** *June 04, 2018 09:29*

Flipped plugs around and now this screen does the same thing as the other. So as per above it seems I have two fakes 

![missing image](https://lh3.googleusercontent.com/S-RJ6xwqWJK7CFGpPcdPRrQmAhKrAhe1NeJ4KniKARE3NG_YYk5c4UIL-zkNP21fxLv-9ROEW1dAAw=s0)


---
**Robert Hubeek** *June 04, 2018 09:42*

setup atm. do i need to have everything connected? as right now it is just the board with power and the screen, i just assumed a menu would show up. 

![missing image](https://lh3.googleusercontent.com/L4d5QeQdmo15VFP-H-ajhd-nh__30-3bS_O3V4g9gScJwfQuf2RcfWeTYqZUlT6egS5yGWjWES_zIw=s0)


---
**Jeff DeMaagd** *June 12, 2018 23:40*

I’m sorry I can’t help further. I don’t know what could be wrong, assuming the contrast is correct.


---
**Robert Hubeek** *June 13, 2018 01:45*

**+Jeff DeMaagd** Dont worry im going the route of merlin now and purchasing a genuine GLCD.  i do have a feeling the pinouts are incorrect on the cohesion3D Remix  and not the GLCD because the pinouts dont match up with the standard GLCD pinouts. 


---
*Imported from [Google+](https://plus.google.com/+RobertHubeek/posts/JjSpAcUwGqY) &mdash; content and formatting may not be reliable*
