---
layout: post
title: "I am trying to setup a corexy system on an X5-GT and I can not get the system to home X or Y it just wants to crash into the home location and let the motors grind"
date: June 03, 2018 23:47
category: "Help"
author: "Jeremy Wasserstrass"
---
I am trying to setup a corexy system on an X5-GT and I can not get the system to home X or Y it just wants to crash into the home location and let the motors grind. If I do an M119 while holding the individual switches down I can see them register. Do I need to slow down the approach speed, or is there something else I should change?





**"Jeremy Wasserstrass"**

---
---
**Johan Jakobsson** *June 04, 2018 08:15*

You have coreXY homing commented out:

#corexy_homing                               true 

Try removing the # , ie:

corexy_homing                               true


---
**Jeremy Wasserstrass** *June 05, 2018 01:16*

Thanks. A bit of a face palm moment. I knew I set coreXY homing to true so I never really looked at that line again. Always better to have someone else check your work.


---
*Imported from [Google+](https://plus.google.com/104298484682589560474/posts/UqKHrQNqDUW) &mdash; content and formatting may not be reliable*
