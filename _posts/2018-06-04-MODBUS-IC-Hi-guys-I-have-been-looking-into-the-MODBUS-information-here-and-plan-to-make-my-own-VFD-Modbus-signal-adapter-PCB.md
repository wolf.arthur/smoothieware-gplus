---
layout: post
title: "MODBUS IC??? Hi guys, I have been looking into the MODBUS information here: and plan to make my own VFD Modbus signal adapter PCB"
date: June 04, 2018 18:43
category: "Help"
author: "Chuck Comito"
---
MODBUS IC???

Hi guys, I have been looking into the MODBUS information here: [http://smoothieware.org/spindle-module#modbus-spindle](http://smoothieware.org/spindle-module#modbus-spindle)



and plan to make my own VFD Modbus signal adapter PCB.  I want to incorporate the Huanyang VFD into my build.



I have access to an SN7517 ic that I wanted to use in place of the  SP3485 ic.The only difference I can see between the 2 ic's are the Vcc supply.



The 3485 requires 3.3v where the  7517 requires 5v. I do not fully understand the circtuit so I'm not sure if this voltage is passed along to the drive or not.



In other words, does the drive RS+ and RS-  inputs expect to see 3.3v from the 3485 where the 7517 would put out 5v and damage something or is this just the amount of power required by the IC to function? 



I'm not sure this made any real sense. Please let me know if I need to clarify and I'll do my best.



Thanks!





**"Chuck Comito"**

---
---
**Don Kleinschnitz Jr.** *June 05, 2018 12:46*

I have never used either part but best I can tell the 3845 is a lower power version of the 7517. They both meet  RS-485 and RS-422 serial protocols.

Since I have not used either I may not be the best source of advice.



I would just get one of these :):

[amazon.com - Amazon.com: KNACRO 3.3V UART serial to RS485 SP3485 Transceiver Converter Communication Module: Home Audio & Theater](https://www.amazon.com/KNACRO-serial-Transceiver-Converter-Communication/dp/B01ILVNU7E#HLCXComparisonWidget_feature_div)



For $8 its not worth any grief.


---
**Chuck Comito** *June 05, 2018 12:55*

Hi **+Don Kleinschnitz** so no have that in my Amazon cart now. And you are correct in your statement. I beleive rs485 is a 5v standard and both should work provided I have a 5v source. I think the 3.3v version is preferred because there's easy access to the 3.3v on the smoothie. I just didn't want to break things but I have a handful of the 75176 ic's.  


---
**Chuck Comito** *June 05, 2018 16:49*

I just notice on the smoothie schematic that the 3.3v pin has a 5v jumper setting. I'll have to look when I get home unless anyone can confirm this?


---
**Chuck Comito** *June 06, 2018 00:13*

**+Don Kleinschnitz**, I just wanted to mention that this IC works just fine. Make sure to connect it to a 5v source and all is good. My VFD is running. I have a couple of quirky things to work out but so far so good. I'd like to let the community know that this is a viable option but I'm not sure how. The schematic is the same as that posted on the VFD smoothie page.


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/fKD4dm8D77v) &mdash; content and formatting may not be reliable*
