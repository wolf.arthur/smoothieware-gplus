---
layout: post
title: "This might seem like a silly question but to be sure..."
date: June 05, 2018 23:01
category: "Help"
author: "Chuck Comito"
---
This might seem like a silly question but to be sure... Are there any differences in these pins as I have circled? 

![missing image](https://lh3.googleusercontent.com/-fFvGq_Nf9hE/WxcWRI8Qw1I/AAAAAAAAMVk/Q71Vs4521uAqhxWuQvXOtDue-Oc2eb0cACJoC/s0/smoothiepins.JPG)



**"Chuck Comito"**

---
---
**Douglas Pearless** *June 05, 2018 23:15*

The current capabilities of the MOSFETs on the left two are greater than the right hand two.  P2.5 is hardware PWM capable ([http://smoothieware.org/pwm-capable](http://smoothieware.org/pwm-capable)); is that what you were after?

[smoothieware.org - pwm-capable [Smoothieware]](http://smoothieware.org/pwm-capable)


---
**Chuck Comito** *June 06, 2018 02:11*

Hi **+Douglas Pearless** what I meant is that it appears there are 2 physical locations of the same pins. One set on the edge and the same pins in that center grouping. 


---
**Douglas Pearless** *June 06, 2018 04:58*

are you referring to the group of 4 pins and the group of two pins surrounded by the pink dotted boxes?  If so, then they are for three jumpers two on the 4 pins, one on the two pins) that control the power source for the two groups of MOSFETs per the comments ash the bottom of the picture




---
**Wolfmanjm** *June 06, 2018 11:37*

Yes there is a difference. One group is the raw pin the other on the connectors go through the mosfet controlled by that pin. When in doubt just check the schematics.


---
**Chuck Comito** *June 06, 2018 13:33*

Hello **+Wolfmanjm**, this is exaclty what I wanted to know. I did check the schematic and knew that they were potentially the same however I didn't understand why they had the same pin name. As a point of discussion the pins have different functions although they are on the same net. This is what confused me. I would have thought they would be designated as something different or as an example, 2.5 on the edge (after the mostfet) would be called 2.5a at the raw pin. I'm  still learning so I wanted to better understand... Thanks for the reply!


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/K4oWT1qdeoL) &mdash; content and formatting may not be reliable*
