---
layout: post
title: "Guys, when it comes to homemade cnc routers (I know there are many types, versions, rigidity, etc.) is there a process or smart way of figuring out a particular machines max IPM?"
date: June 08, 2018 18:24
category: "General discussion"
author: "Chuck Comito"
---
Guys, when it comes to homemade cnc routers (I know there are many types, versions, rigidity, etc.) is there a process or smart way of figuring out a particular machines max IPM? Is it all trial and error? So far I've been running mine pretty slow (in the realm of 25ipm max) but I'm curious if there are any general rule of thumb or advice that can be given to find out efficiently and without breaking stuff :-)..





**"Chuck Comito"**

---
---
**Arthur Wolf** *June 08, 2018 19:00*

definitely trial and error


---
**Wolfmanjm** *June 08, 2018 20:49*

You can find the maximum possible speed of the bare stepper with this [http://www.daycounter.com/Calculators/Stepper-Motor-Calculator.phtml](http://www.daycounter.com/Calculators/Stepper-Motor-Calculator.phtml), then work your way back from that.

[daycounter.com - Stepper Motor Calculator](http://www.daycounter.com/Calculators/Stepper-Motor-Calculator.phtml)


---
**Don Kleinschnitz Jr.** *June 08, 2018 22:52*

If you referring to ipm while cutting material, my experience is it depends on bit geometry, rpm, material type and depth. Start by calculating chip loads and then trial and error from there. 


---
**Chuck Comito** *June 11, 2018 14:33*

Hi **+Don Kleinschnitz** I was leaning toward what **+Wolfmanjm** had mentioned to find out my machines capability to better decide on the items you had mentioned. It all goes hand in hand after all. 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/eWjqFHYi1gV) &mdash; content and formatting may not be reliable*
