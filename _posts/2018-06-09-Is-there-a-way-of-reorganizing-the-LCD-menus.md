---
layout: post
title: "Is there a way of reorganizing the LCD menus?"
date: June 09, 2018 02:55
category: "General discussion"
author: "Anthony Bolgar"
---
Is there a way of reorganizing the LCD menus? I would like to customize it to my needs.





**"Anthony Bolgar"**

---
---
**Douglas Pearless** *June 09, 2018 02:59*

There is the custom menu screen which you can add you own things onto, BUT to customise the rest of the menu structure is not trivial as it is largely hardwired into a 3D printer and a CNC menu system.


---
**Arthur Wolf** *June 10, 2018 15:54*

You'd pretty much need to edit the code yes.


---
**Anthony Bolgar** *June 10, 2018 16:06*

Thanks guys.




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/GknzM1LN11t) &mdash; content and formatting may not be reliable*
