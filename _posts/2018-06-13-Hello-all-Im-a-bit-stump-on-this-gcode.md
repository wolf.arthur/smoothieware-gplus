---
layout: post
title: "Hello all, I'm a bit stump on this gcode"
date: June 13, 2018 23:20
category: "General discussion"
author: "Chuck Comito"
---
Hello all, I'm a bit stump on this gcode. This is an attempt at an automated zxy probing sequence:

G38.2 Z-50 F100 (Probe Z at F100)

G92 Z7.44 (set location to 7.44)

G91 G0 Z3 (move z up 3mm)

G91 G0 X-25 (move x axis negative 25mm)

G91 G0 Z-6 (move z negative 6mm, this puts the next section far enough down for the next probe gcode)

G38.2 X50 F100 (probe X at F100)

G92 X-8.175 (set location to -8.175)

G91 G0 X-3 (move away from touch plate)

G91 G0 Z6 (move z up 6mm)

G91 G0 Y-25 (move y axis negative 25mm)

G91 G0 X25 (move x axis 25mm, now probe is in front of touch plate)

G91 G0 Z-6 (move Z down 6mm, this puts the next section far enough down for the next probe gcode)

G38.2 Y50 F100 (probe Y at F100)

G92 Y-8.175 (set location to -8.175)

G91 G0 Y-3 (move y away from touch plate)

G91 G0 Z6 (move z above touch plate)

G90 X0 Y0 Z10 (go to new 0,0,10 location but doesn't work at all)



G90 X0 Y0 Z10 might work with a G0 move instead of G90?



Line by line it executes fine but if ran in a macro (Lw4 and bCNC) or .nc file it does weird things and doesn't finish. The last line doesn't work at all but I'm sure I'm using it wrong.



Now I don't think this is a smoothie question unless I'm using Gcodes that are not supported. I did try this in bCNC, Pronterface and LW4. All had the same issues.



Any advice would be much appreciated.





Update: I added G4 S1 prior to each axis probe and it seemed to work with the exception of the last command. I changed the last command to G0 instead of G90 but still no joy. 





**"Chuck Comito"**

---
---
**Douglas Pearless** *June 14, 2018 02:31*

have you considered G29/30 or G38 commands? [smoothieware.org - supported-g-codes [Smoothieware]](http://smoothieware.org/supported-g-codes) 




---
**Wolfmanjm** *June 14, 2018 08:20*

G90 is not a move command, please read the gcodes and what they do. your last command would be G90 G0 X0 Y0 Z10


---
**Chuck Comito** *June 14, 2018 10:50*

**+Wolfmanjm** thanks. I did read the gcode and what they do. I don't come here because I don't look into it first. I came here for help in understanding as I don't have all that much experience. 


---
**Chuck Comito** *June 14, 2018 15:36*

**+Douglas Pearless** I did not try those. It looks like G30 is for probing Z only. In a brief look, I didn't see G29 at all but I will go back and review it. What ended up working (at least so far) is adding a G4 S1 (dwell 1 sec) after each probing direction. I'm not exactly sure why this is. Seems like either the software or firmware is somehow skipping around the code randomly. It starts off ok but randomly misses portions of my code.  I'm pretty stumped and don't know why the dwell helped it (at least it seemed to in my preliminary testing).


---
**Wolfmanjm** *June 14, 2018 16:30*

Sorry for previous erroneous post. FWIW it should work presuming you fix the final move (remember G91 is modal so you need to set back to G90). Pause (G4) is recommended but not required. This does work as I use it all the time.


---
**Wolfmanjm** *June 14, 2018 16:34*

**+Chuck Comito** You probably  need to put a M400 after each probe to wait for it to finish. Although technically G4 will also do it it will only wait for 1 second, M400 will wait until the queue is completely finished. Generally if you look at how bCNC does probe commands will give you a better idea of how to automate this, but usually one would wait for the probe to complete and read the [PRB: ...] response after each probe


---
**Wolfmanjm** *June 14, 2018 18:45*

**+Chuck Comito** I suspect however you are sending these commands you are not waiting for the ok from the previous command before sending the next one. This is a violation of the reprap protocol, and would explain your odd behavior because you are executing G92 before the last command was complete. so even adding M400 won't work unless you wait for an ok.


---
**Chuck Comito** *June 14, 2018 20:20*

**+Wolfmanjm** so I believe you are correct in your last statement. Is there a workaround? Possibly running it as a .nc file?


---
**Wolfmanjm** *June 14, 2018 20:23*

The workaround is to comply with the protocol :) I am not entirely sure what your issues are as far as I can see it should work even if OK is not being waited for as gcodes are ALWAYS executed in order, even if they are stacked up in the serial buffer. G38 is a special code and is generally executed manually not in a gcode file, however having looked closely at what it does it should still work even if executed in a file.


---
**Douglas Pearless** *June 14, 2018 22:00*

if you remove the G91 prefix (so just G0) and now go to [ncviewer.com - NC Viewer // Online GCode Viewer and Machine Simulator](https://ncviewer.com) and enter the code (replacing the sample code in the top left window), you will be able to "play" and "single step" your code and see the resulting movements in the window on the right hand side 


---
**Chuck Comito** *June 15, 2018 02:16*

**+Wolfmanjm** Hello.. The issues (besides the last line) is that this code randomly misses sections. For instance, I execute the code and the z probe which is first always works. Then it does the first travel to the x probe location. Once it gets there it may or may not execute the x probe sequence. Sometimes it makes it past that and the x probes just fine and then the next move command will take it to the Y probe location and it fails to do the G38.2 Y. I'm not sure if this explains it. Basically, after the initial z G38, it randomly skips the x G38 but will complete the y or it will do the x G38 move to the new position and then skip the y G38. A video would explain better. I'll post one if I can. By adding the dwell it seems to work and I'll be happy with that if it stay consistent. In the meantime,  I can set up 3 macro and probe each axis individually.



**+Douglas Pearless**, I will give this a try. Thanks for the heads up!


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/DMsmqJTKTgT) &mdash; content and formatting may not be reliable*
