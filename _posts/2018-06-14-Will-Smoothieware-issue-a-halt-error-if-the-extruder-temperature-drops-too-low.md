---
layout: post
title: "Will Smoothieware issue a halt error if the extruder temperature drops too low?"
date: June 14, 2018 06:58
category: "Help"
author: "Anthony Bolgar"
---
Will Smoothieware issue a halt error if the extruder temperature drops too low?

I have been getting some random halts, but was not near the printer when it happened so I do not know what the temp was when it happened. What I do know is I changed the cooling fan to a more powerful one and am thinking it is cooling down the heat block too much.





**"Anthony Bolgar"**

---
---
**Wolfmanjm** *June 14, 2018 08:24*

yes, if you have usb connected and print from usb you will see exactly what error caused the halt in the console


---
**Subhash Sharma** *June 14, 2018 09:38*

Dewa


---
**Anthony Bolgar** *June 14, 2018 10:14*

Thanks **+Wolfmanjm**, I'll connect to USB and monitor it. It's a weird error, seems random, some prints complete, others stop after 2 or 3 layers, some half way through.




---
**Anthony Bolgar** *June 15, 2018 13:20*

What program should I use to connect  and print via USB **+Wolfmanjm**?


---
**Wolfmanjm** *June 15, 2018 13:26*

pronterface  or octoprint


---
**Anthony Bolgar** *June 16, 2018 16:36*

Turns out to be a problem with the thermistor getting loose from it';s mount


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/VUnLjBypLV1) &mdash; content and formatting may not be reliable*
