---
layout: post
title: "I have a cr10 s400. Have just fitted an sbase board and this is my first venture into smoothie.I am having trouble with y axis shift"
date: June 21, 2018 08:11
category: "General discussion"
author: "Bob Salter"
---
I have a cr10 s400. Have just fitted an sbase board and this is my first venture into smoothie.I am having trouble with y axis shift. In marlin I turned jerk down to 5 and acceleration down to a few hundred, but I cant quite figure out how to do this in smoothie. I have set junction deviation down to 0.005 and acceleration down to 500 but still getting shift. What am I missing? Thanks in advance



Bob





**"Bob Salter"**

---
---
**Arthur Wolf** *June 21, 2018 10:27*

Try reducing your junction deviation to 0.005 and acceleration to 50 and see if the problem goes away. If it does you can play with values in between, if not you've got another problem.

It's likely you do : the CR10 isn't a great printer, but I don't think it's "skips steps at 500 accel" bad.

Also, in case you aren't aware of the issues with MKS you can read [smoothieware.org - troubleshooting [Smoothieware]](http://smoothieware.org/troubleshooting#somebody-refused-to-help-me-because-my-board-is-a-mks-what-s-that-all-about) , on top of the "ethical" concerns, the MKS boards are pretty bad technically and are known for being much more likely to skip steps than a smoothieboard.


---
**Bob Salter** *June 21, 2018 21:08*

Thanks. I will try as you suggest. Its a cr10 clone I built from scratch and the bed is 430x430x4mm aluminium plate[ very heavy]. I had similar problems with marlin, its just finding the sweet spot. I knew the MKS board wasnt fabulous, just getting my feet wet with smoothie. Will probably upgrade later. Have ordered stepper breakout boards to add the tmc2130 drivers.


---
*Imported from [Google+](https://plus.google.com/102835399256322806809/posts/5rJrfYu4pSK) &mdash; content and formatting may not be reliable*
