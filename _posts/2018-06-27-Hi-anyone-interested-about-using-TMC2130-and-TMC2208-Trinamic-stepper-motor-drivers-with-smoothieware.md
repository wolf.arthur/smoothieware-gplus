---
layout: post
title: "Hi, anyone interested about using TMC2130 and TMC2208 Trinamic stepper motor drivers with smoothieware?"
date: June 27, 2018 11:05
category: "Development"
author: "Tiago Almeida"
---
Hi, anyone interested about using TMC2130 and TMC2208 Trinamic stepper motor drivers with smoothieware? There are two pull requests that require testing before being merged on edge branch. Any help will be appreciated.



Links



TMC2130: [https://github.com/Smoothieware/Smoothieware/pull/1315](https://github.com/Smoothieware/Smoothieware/pull/1315)



TMC2208: [https://github.com/Smoothieware/Smoothieware/pull/1329](https://github.com/Smoothieware/Smoothieware/pull/1329)





**"Tiago Almeida"**

---


---
*Imported from [Google+](https://plus.google.com/113668009206685098457/posts/UwhFs7YTiKy) &mdash; content and formatting may not be reliable*
