---
layout: post
title: "Hi folks, I wanted to implement grid probing with a bltouch today on a cartesian printer"
date: June 28, 2018 15:30
category: "Help"
author: "Paul Arnold"
---
Hi folks,



I wanted to implement grid probing with a bltouch today on a cartesian printer.

I noticed the following:

My alpha_min is -9, my probe offset in x direction from the nozzle is 41mm.

Therefore, the probe is unable to reach the far left side ob the bed but the carriage is trying to reach it. This causes lost steps and a false positioning.



Is there a way to manually set the probing points and / or range?



This is my leveling strategy section:

leveling-strategy.rectangular-grid.enable               true    

leveling-strategy.rectangular-grid.x_size               200     

leveling-strategy.rectangular-grid.y_size               200     

leveling-strategy.rectangular-grid.size                 7 		

																

																

leveling-strategy.rectangular-grid.tolerance      0.03        	

leveling-strategy.rectangular-grid.do_home        false

leveling-strategy.rectangular-grid.probe_offsets        41,20,0 

leveling-strategy.rectangular-grid.save                 true   	

leveling-strategy.rectangular-grid.initial_height       10      

leveling-strategy.rectangular-grid.human_readable		true

mm_per_line_segment                                     1       





Thanks in advance





**"Paul Arnold"**

---
---
**Paul Arnold** *June 28, 2018 20:26*

**+Arthur Wolf**, can you help out here? Any ideas?


---
**Douglas Pearless** *June 28, 2018 21:52*

I had the same issue and in about 8 hours when I am back near my cartesian printer I dig out the config and post the relevant section; I recall that in my case, it homes to X-MIN and I had to reduce the X_size to be the distance from the probe to the opposite side, i.e. reduce it by the Z-Probe X offset, so it was 200mm - 80mm which if I recall meant:

leveling-strategy.rectangular-grid.x_size               200

became

leveling-strategy.rectangular-grid.x_size               120

And the same is true for he Y Axis


---
**Paul Arnold** *June 29, 2018 05:23*

**+Douglas Pearless** I also thought about this. But wouldnt this result in a shifted grid?


---
**Douglas Pearless** *June 29, 2018 06:30*

I have not read the source code in this area for quite a while so I cannot remember if it does compensate for the offset:-) but it works for me :-)




---
**Paul Arnold** *June 29, 2018 12:27*

Maybe this makes it a bit clearer ![missing image](https://lh3.googleusercontent.com/WCiutzcK2QIJ5uglyw7i01BictL5jlkWapqY95Tp5-PDlZiGhEqtT7pRE4LJptd1pOc_fRhZy07aSRKtjHkKowZ-pMWm2P9IMOtl=s0)


---
**Douglas Pearless** *June 29, 2018 19:47*

OK, then you will need to reduce the:

leveling-strategy.rectangular-grid.x_size               200     

leveling-strategy.rectangular-grid.y_size               200  

until it fits over the bed :-)


---
**Paul Arnold** *June 29, 2018 20:02*

**+Douglas Pearless** And also the alpha_min and / or probe offset, right? If not, the probe still tries to reach the left side, but can't and slams into the x endstop




---
**Douglas Pearless** *June 29, 2018 23:15*

Here is my config for a 200x200 bed (Wanhao i3) with a BL Touch offset by X = -34 and Y= -50 from the nozzle:



leveling-strategy.rectangular-grid.enable               true     # The strategy must be enabled in the config, as well as the zprobe module.

leveling-strategy.rectangular-grid.x_size               125      # size of bed in the X axis

leveling-strategy.rectangular-grid.y_size               125      # size of bed in the Y axis

leveling-strategy.rectangular-grid.size                 9 . # The size of the grid, for example, 7 causes a 7x7 grid with 49 points. 

                                                                 # Must be an odd number.

leveling-strategy.rectangular-grid.do_home              true              

leveling-strategy.rectangular-grid.probe_offsets        -34,-50,1.8    # Optional probe offsets from the nozzle or tool head

leveling-strategy.rectangular-grid.save                 true     # If the saved grid is to be loaded on boot then this must be set to true

leveling-strategy.rectangular-grid.initial_height       10       # will move to Z10 before the first probe

leveling-strategy.rectangular-grid.dampening_start      0.5      # compensation decrease point (optional)

leveling-strategy.rectangular-grid.height_limit         1        # no compensation to apply after this point (optional)

mm_per_line_segment                                     1        # necessary for cartesians using rectangular-grid


---
**Paul Arnold** *June 30, 2018 06:05*

Thanks dude 


---
**Douglas Pearless** *June 30, 2018 06:24*

:-)


---
*Imported from [Google+](https://plus.google.com/102459443787174629948/posts/KZLrJR3bvDu) &mdash; content and formatting may not be reliable*
