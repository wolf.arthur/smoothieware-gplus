---
layout: post
title: "Has anyone used a filament run out detector to pause smoothieware?"
date: July 04, 2018 19:45
category: "General discussion"
author: "Anthony Bolgar"
---
Has anyone used a filament run out detector to pause smoothieware? If so, could you share how you did it?





**"Anthony Bolgar"**

---
---
**Wolfmanjm** *July 05, 2018 10:10*

use the filament out detector module as documented in the wiki [smoothieware.org - filament-detector [Smoothieware]](http://smoothieware.org/filament-detector)


---
**Anthony Bolgar** *July 05, 2018 11:04*

OK, I was thinking more along the lines of a limit switch type.


---
**Wolfmanjm** *July 05, 2018 11:44*

that won't work reliably you need to know if the extruder is running or not.


---
**Anthony Bolgar** *July 05, 2018 11:57*

Should be reliable on a bowden setup.


---
**Wolfmanjm** *July 05, 2018 12:19*

well of you want to do it that way there is an example in the switch wiki


---
**Anthony Bolgar** *July 05, 2018 12:33*

Thanks **+Wolfmanjm**Wolfmanjm




---
**Wolfmanjm** *July 05, 2018 17:08*

also the switch module method will not detect a jam whereas the filament module will.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/eHc8W3P7y57) &mdash; content and formatting may not be reliable*
