---
layout: post
title: "Shared on July 04, 2018 19:26...\n"
date: July 04, 2018 19:26
category: "Development"
author: "Arthur Wolf"
---
#v2HYPE #comingSoon #notKidding

![missing image](https://lh3.googleusercontent.com/-j8zIy2_1Kts/Wz0fb9jDUII/AAAAAAAAQYU/lzDK3AGTaOwPblbRVpcKvAsT7g1f9dhuwCJoC/s0/smoothieboard2standard-pre9.png)



**"Arthur Wolf"**

---
---
**Antonio Hernández** *July 04, 2018 23:06*

The kickstarter campaign ? (When ?)


---
**Gary Tolley - Grogyan** *July 05, 2018 00:06*

Still a lot of routing left to do


---
**Douglas Pearless** *July 05, 2018 03:00*

Looks awesome; I cannot wait to play with it :-)


---
**Anton Ovchinnikov** *July 05, 2018 04:49*

It's alive! Almost


---
**Petr Sedlacek** *July 05, 2018 06:55*

Awesome, it might be ready before I finish my large delta 😊 **+Arthur Wolf** is there any way to subscribe to receive a notification when the Kickstarter is ready? 😉


---
**Arthur Wolf** *July 05, 2018 09:22*

Yes, the Kickstarter should happen soon ( routing is nearly done, we still need to make a few protos for contributors before we launch KS ). If you want to be warned when the KS starts, email wolf.arthur@gmail.com and I'll add you to the ( now long ) list.




---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/jf8gNHxGVdd) &mdash; content and formatting may not be reliable*
