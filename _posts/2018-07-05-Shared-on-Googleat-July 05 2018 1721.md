---
layout: post
title: "Shared on July 05, 2018 17:21...\n"
date: July 05, 2018 17:21
category: "Development"
author: "Arthur Wolf"
---


![missing image](https://lh3.googleusercontent.com/-tfDvhfJfBBI/Wz5Tp-wbbxI/AAAAAAAAQZ0/sz6kCtsmiwgtSXza5W8ezIvw7RrkWiXzQCJoC/s0/Screenshot%252B%25252823%252529.png)



**"Arthur Wolf"**

---
---
**ThantiK** *July 05, 2018 17:29*

Ehrmagerd! 


---
**Gary Tolley - Grogyan** *July 06, 2018 05:30*

I'm slightly concerned about the trace lengths to each of the drivers.

However there is still much routing to do 


---
**Griffin Paquette** *July 06, 2018 13:24*

4 2660’s?


---
**Arthur Wolf** *July 06, 2018 14:52*

Yep. We'll sell kits for machines that need more axes with extension boards.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/1RpqRzwi2Yh) &mdash; content and formatting may not be reliable*
