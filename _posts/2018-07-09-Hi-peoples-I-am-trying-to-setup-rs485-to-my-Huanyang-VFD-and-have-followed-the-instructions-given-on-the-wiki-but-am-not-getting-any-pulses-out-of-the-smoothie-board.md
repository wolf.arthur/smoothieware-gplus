---
layout: post
title: "Hi peoples, I am trying to setup rs485 to my Huanyang VFD and have followed the instructions given on the wiki but am not getting any pulses out of the smoothie board"
date: July 09, 2018 13:46
category: "Help"
author: "Colin Wildsmith"
---
Hi peoples,

I am trying to setup rs485 to my Huanyang VFD and have followed the instructions given on the wiki but am not getting any pulses out of the smoothie board.  I have a  'smoothieboard V1.0a' and have updated the firmware to build edge-24da5ce. 



I am monitoring with my CRO with 1V horiz div and 25us vert div.

-> 3.3V is good

-> rx is 0V and no pulses when M3 S3000 or M5 sent

-> tx is 3.3V and no pulses when M3 S3000 or M5 sent



config is setup exactly as shown on wiki:

spindle.type                                     modbus   # set the spindle type to modbus/RS485

spindle.vfd_type                                 huanyang # set the VFD type, this is necessary because each inverter uses its own commands

spindle.rx_pin                                   2.6      # TX pin for the soft serial

spindle.tx_pin                                   2.4      # RX pin for the soft serial

spindle.dir_pin                                  2.5      # RS485 is only half-duplex, so we need a pin to switch between sending and receiving 



Does anyone have any suggestions?

Thanks

Colin





**"Colin Wildsmith"**

---
---
**Arthur Wolf** *July 09, 2018 13:51*

it works here. can you try using other pins ? also can we see your full config at [pastebin.com](http://pastebin.com) ?


---
**Colin Wildsmith** *July 09, 2018 14:00*

[pastebin.com - # NOTE Lines must not exceed 132 characters # '^' for pull up none is for pull - Pastebin.com](https://pastebin.com/b5RTYaEe)


---
**Arthur Wolf** *July 09, 2018 14:06*

You are not enabling the spindle module


---
**Antonio Hernández** *July 09, 2018 14:08*

Hope the following post could help in some way to solve your issue ( [plus.google.com - Hey, I need you help with a modbus thing ! I'm working on something really aw...](https://plus.google.com/u/1/+ArthurWolf/posts/E2spXw6dgCp) )


---
**Colin Wildsmith** *July 09, 2018 14:14*

I tried pins 1.30(RX)=2.5V constant,1.31(TX)=3.3V constant and 1.3(DIR)=0.3V constant.

And no pulses on any


---
**Arthur Wolf** *July 09, 2018 14:20*

You forgot to enable the spindle module


---
**Colin Wildsmith** *July 10, 2018 12:39*

Was getting cold in my shed last night and had to depart.Thanks Arthur, yes that was a rookie mistake. However, I put 'spindle.enable  true' in the config and restarted and still getting nothing on my CRO. I was hoping that would work. Looks like it needs a deeper dive and will look into Antonios post.


---
**Colin Wildsmith** *July 10, 2018 13:31*

I am also using a [sparkfun.com - SparkFun Transceiver Breakout - RS-485 - BOB-10124 - SparkFun Electronics](https://www.sparkfun.com/products/10124)

Antonio post stated 'and no one worked for me'  which means that it didn't work for him right?

Regardless if the board works or not I should still be able to see logic coming out of the smoothieboard on my CRO.


---
**Antonio Hernández** *July 10, 2018 13:41*

Hello **+Colin Wildsmith**. I hope the following comments could help in some way. Only two circuits (for me) worked for modbus connection. Sparkfun breakout board is one of the breakouts that didn't work for me. The other two breakout boards (chinese boards) mentioned in the post did the job.


---
**Colin Wildsmith** *July 10, 2018 13:45*

Thanks Antonio, I'll get one of the others.


---
**Antonio Hernández** *July 10, 2018 13:48*

Any of these two breakouts worked. My spindle belongs to Huanyang brand.  ==> [es.aliexpress.com - Ttl a RS485 módulo RS485 a TTL con aislado solo chip UART puerto serie](https://es.aliexpress.com/item/TTL-to-RS485-module-RS485-to-TTL-with-isolated-single-chip-serial-port-UART/32820863224.html?spm=a2g0s.9042311.0.0.fyFwys) <== , ==> [https://es.aliexpress.com/item/microcontroller-TTL-to-RS485-module-485-to-serial-communication-module-UART-level-conversion-automatic-flow/32397727387.html?spm=a2g0s.9042311.0.0.fyFwys](https://es.aliexpress.com/item/microcontroller-TTL-to-RS485-module-485-to-serial-communication-module-UART-level-conversion-automatic-flow/32397727387.html?spm=a2g0s.9042311.0.0.fyFwys) <==


---
**Antonio Hernández** *July 10, 2018 13:53*

I suggest to focus on vfd parameters related with frequency operation value. There are programs on vfd that must be changed in order to requested rpm's could work as expected.


---
**Colin Wildsmith** *July 10, 2018 14:03*

I have set up the parameters but can't see any logic on the UART comms bus or rs485 side


---
**Antonio Hernández** *July 10, 2018 14:13*

I don't know if I read wrong, but, Arthur said something about in detail related with sparkfun breakout board. He saw the signals in oscilloscope (send and receive operations were ok in the vfd, but nothing happens).


---
**Colin Wildsmith** *July 10, 2018 14:21*

I've bought both those boards so will wait for them to come in and just control spindle via digital input.


---
**Antonio Hernández** *July 10, 2018 14:26*

I don't know which brands are allowed to work using smoothieboard. In my case, those two boards were tested using a huanyang vfd and both worked.


---
**Wolfmanjm** *July 10, 2018 14:52*

did you load the cnc build? the spindle module is not compiled into the 3d build for obvious reasons.


---
**Antonio Hernández** *July 10, 2018 14:55*

Ah yeah, in my case is cnc building compiled from edge branch. I never tried to compile as 3d including spindle module. I don't know if it's possible.


---
**Colin Wildsmith** *July 11, 2018 11:07*

Ok I installed the'firmware-latest.bin' and your saying I should have just downloaded 'firmware-CNC-latest'? I looked at the read me file and it said you require s a pregnant style LCD. I chose the other one as I use my machine for both printing and machining. That's one of the features of the smoothie board that made me use it. I will install for this test and let you know if it works.

Thanks for the feedback


---
**Antonio Hernández** *July 11, 2018 13:35*

In my case the firmware it's compiled using the property CNC=1, the file that you downloaded from the site must be enough. 


---
**Colin Wildsmith** *July 11, 2018 13:44*

Lol I just looked at my previous post and it says pregnant style LCD. I meant pendant style LCD. Predictive text will get you.


---
**Antonio Hernández** *July 11, 2018 13:51*

I was wondering about that..., jeje, don't worry.


---
**Wolfmanjm** *July 11, 2018 14:21*

the LCD is optional in the firmware-cnc


---
*Imported from [Google+](https://plus.google.com/115945226833370093002/posts/aRSU3XFtnE5) &mdash; content and formatting may not be reliable*
