---
layout: post
title: "Shared on July 09, 2018 19:56...\n"
date: July 09, 2018 19:56
category: "Machine showcase"
author: "Arthur Wolf"
---

{% include youtubePlayer.html id="9RjoZI7NgI0" %}
[https://youtu.be/9RjoZI7NgI0](https://youtu.be/9RjoZI7NgI0)





**"Arthur Wolf"**

---
---
**Anthony Bolgar** *July 09, 2018 20:29*

Very nice!




---
**Arthur Wolf** *July 09, 2018 20:30*

\o/ Thanks. Lots of tears and cut fingers to get there, but it's really cool being able to use the machine now.


---
**Antonio Hernández** *July 10, 2018 02:23*

More work to be added to our machines... hope to see more details using ATC in the controller and more details about the spindle architecture (model, etc). That's a great job. In the video we can read problems related with the spindle, I don't know if the spindle model could be relevant to achieve ATC ( the issues related with the spindle docs, etc)


---
**Colin Wildsmith** *July 10, 2018 13:07*

Thats awesome, nice tool changer. Good work guys


---
**Jérémie Tarot** *July 13, 2018 08:40*

Hey **+Arthur Wolf** , would you share links to spindle maker/supplier please? 


---
**Arthur Wolf** *July 13, 2018 09:05*

**+Jérémie Tarot** jk@jian-ken.com


---
**Antonio Hernández** *July 13, 2018 09:12*

**+Arthur Wolf**, JGL Series ?. Any 'serie' can apply ?. [jian-ken.com - spindle motor, spindles for engraving and grinding, atc spindles, constant power spindles, constant torque spindles, Lathe machine spindles, milling machine spindles, drilling spindles - Jingjiangcity Jianken High-Speed Electricmotor Co.,Ltd](http://www.jian-ken.com/)


---
**Arthur Wolf** *July 13, 2018 09:14*

**+Antonio Hernández** It's going to depend on your needs. I needed one with collets so I would be able to use different diameter tools, and one with a high rotation speed so I could try high speed milling of aluminium. But maybe you want something different.


---
**Antonio Hernández** *July 13, 2018 09:18*

**+Arthur Wolf**, What kind of breakout board did you use to "talk" with the spindle ?. Some changes were made over code to "talk" without problems or current brands on code could be used to work with these kind of spindles ?


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/BysSwhcYFo1) &mdash; content and formatting may not be reliable*
