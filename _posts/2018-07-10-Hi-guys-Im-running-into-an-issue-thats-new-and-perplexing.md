---
layout: post
title: "Hi guys, I'm running into an issue that's new and perplexing"
date: July 10, 2018 22:25
category: "General discussion"
author: "Chuck Comito"
---
Hi guys, I'm running into an issue that's new and perplexing. Yesterday I was running a job on my mill. It's a fairly small job with short gcode. I was using bcnc as the host. I ran the job twice and all went fine. The 3rd time the program started, executed 15 lines of gcode and stopped moving. The spindle stays on. I don't have any error messages. I thought maybe the gcode file became corrupted so I generated it again within fusion and ran it. It also stopped at 15 lines and the spindle continued. Any thoughts on what I should be checking? Btw, I'm running the latest cnc smoothie firmware. Thanks.

Update: I get this in the python.exe:

Exception in thread Thread-2:

Traceback (most recent call last):

    File "C:\Python27\lib\threading.py", line 754, in run

         self.__target(*self.__args, **self.__kwargs)

    File "C:\bCNC-master\Sender.py", line 853, in serialIO

         self.serial.write(b"?")

    File "C:\Python27\lib\site-packages\pyserial-3.4-py2.7.egg\serial\serialwin32.py", line 323, in write

          raise writeTimeoutError

SerialTimeoutException: Write timeout



Sounds like an issue with the serial communications timing out. Probably a PC issue??  





**"Chuck Comito"**

---
---
**Brandon Satterfield** *July 11, 2018 01:55*

Post your Gcode bud. Not all boards understand all Gcode. Are you using a smoothie post processor from fusion? If I recall in bCNC you can select to ignore or pause on unsupported commands. 


---
**Chuck Comito** *July 11, 2018 02:04*

**+Brandon Satterfield** Here is a link to it in my dropbox. [dropbox.com - cup new.nc](https://www.dropbox.com/s/9hb96gnszvhv9lu/cup%20new.nc?dl=0)



I am not using the smoothie post processor but the grbl post processor. The strange thing is that I have 8 of the same part. I ran 2 successfully and then it started to fail on the 3rd. As of today I can't run the file at all. I switch pc's and had the same results. I then re-flashed the firmware just in case it was that and ran the job again. It was successful. So I swapped it out to a new part and it failed again with the same results. 



Thanks for looking into it!!


---
**Brandon Satterfield** *July 11, 2018 02:29*

Let me look at it this week, **+Arthur Wolf** what Gcode would smoothie not like in a GRBL post processor? G64, etc, hadn’t looked at his lines yet? 


---
**Wolfmanjm** *July 11, 2018 10:35*

Smoothie simply ignores unknown gcodes it does not error out or stop this sounds more like a communication problem between bCNC and smoothie. Is Smoothie in ALARM state? if so y need to monitor the serial port for the error message that caused it. For instance F0 would cause an ALARM. However if the same gcode worked once before then it is unlikely to be gcode related, maybe a noisy USB cable? or ground loop.






---
**Brandon Satterfield** *July 11, 2018 12:34*

Did not realize ignoring gcode was not left up to the gcode sender with smoothie. Takes that off the table. Sorry **+Chuck Comito** be worth getting a Python guy to look at it. I have never had a timeout with bCNC.


---
**Chuck Comito** *July 13, 2018 01:04*

Well I can't be sure but I opened the chassis and fiddled around with my connections. I'm wondering if this issue was caused by a vibrating ground on one of the switches... It's seems to be working though. Test 2 starts in 30 seconds.. :-)


---
**Chuck Comito** *July 16, 2018 22:46*

Well, scratch that. I'm still getting random drop outs. Any thoughts on what I should try next? I still think it's got to be noise somewhere but nothing has changed with my setup and it worked for days prior to this issue. 


---
**Chuck Comito** *July 21, 2018 17:05*

So I haven't had any dropouts since I removed the chassis mount usb pigtail. Bcnc will occasionally not complete fully but it's running. Wish me luck and thank for your input. 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/fmSx6cgfzqa) &mdash; content and formatting may not be reliable*
