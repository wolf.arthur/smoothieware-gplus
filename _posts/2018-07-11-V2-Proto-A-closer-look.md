---
layout: post
title: "V2 Proto: A closer look"
date: July 11, 2018 14:11
category: "General discussion"
author: "Arthur Wolf"
---
V2 Proto: A closer look

![missing image](https://lh3.googleusercontent.com/-brsBl24dEF4/W0YQGi5MuuI/AAAAAAAAQbI/Mqsy9UOSaAQW_NyquUWEKIwbpDjw3iTOACJoC/s0/screenshot__28_.png)



**"Arthur Wolf"**

---
---
**Antonio Hernández** *July 11, 2018 14:18*

V2 Hive !


---
**Ray Kholodovsky (Cohesion3D)** *July 11, 2018 14:23*

2 sided smd? 


---
**Arthur Wolf** *July 11, 2018 14:27*

**+Ray Kholodovsky** And 8 layers yeah ... didn't have much of a choice considering the featureset this time.


---
**Stephanie A** *July 11, 2018 17:03*

8 layers, top/bottom smd... I do hope a lower cost board is planned.


---
**Arthur Wolf** *July 11, 2018 17:05*

It is, it's called v2-mini


---
**Anton Fosselius** *July 11, 2018 19:41*

0.5mm pitch bga and 4 layers? Is the back silkscreen the same color as a cu layer? I see things i suspect is silkscreen but in 3 colors, white, yellow and blue. What am i missing? 


---
**Douglas Pearless** *July 11, 2018 20:54*

Please tell me that it has JTAG (or SWD) for the processor(s) on the board with and actual connector even if it is not populated, rather than just pads? :-)


---
**Anton Fosselius** *July 11, 2018 21:58*

**+Douglas Pearless** take it like a man: [aliexpress.com - 100Pcs/Bag Spring Test Probe Pogo Pin P50-B1 Dia 0.5mm Length 16.35mm](https://www.aliexpress.com/item/100Pcs-Bag-Spring-Test-Probe-Pogo-Pin-P50-B1-Dia-0-5mm-Length-16-35mm/32845302540.html?ws_ab_test=searchweb0_0,searchweb201602_3_10152_10151_10065_10344_10068_10342_10343_10340_10341_10696_10084_10083_10618_10307_10820_10821_10301_10303_10846_10059_100031_524_10103_10624_10623_10622_10621_10620,searchweb201603_6,ppcSwitch_7&algo_expid=d500ff4b-15c1-4aec-8d5d-2384b6e83621-6&algo_pvid=d500ff4b-15c1-4aec-8d5d-2384b6e83621&transAbTest=ae803_2&priceBeautifyAB=0)




---
**Douglas Pearless** *July 11, 2018 22:14*

Hehe, :-) 

I'd rather have an approved connector than a bunch of pogo probes for reliability; especially as I typically have very little above board clearance in the work that I am doing.


---
**Arthur Wolf** *July 11, 2018 22:43*

It does have a jtag+uart/debug connector :)


---
**Arthur Wolf** *July 11, 2018 22:44*

**+Anton Fosselius** Yes it's 0.5mm pitch bga, and it's 8 layers. I don't have the details of what color is what right now.


---
**Douglas Pearless** *July 11, 2018 22:49*

fantastic, is it the Cortex Debug Connector (10-pin) so I can plug a Segger J-Link (PRO) into it?  (referring to [infocenter.arm.com - infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf](http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf) )


---
**Arthur Wolf** *July 11, 2018 22:50*

**+Douglas Pearless** It's our own thing, but we'll have an adapter for the cortex connector.


---
**Douglas Pearless** *July 11, 2018 22:54*

OK, any chance (please) the design could instead use the ARM recommended 10 pin connector (even if it is not soldered on as most users won't want it)?


---
**Arthur Wolf** *July 11, 2018 22:58*

Nope, we wouldn't be able to provide the features we want to that way. But as I said we'll have an adapter for a few $, which is even easier than having to solder your connector.


---
**Douglas Pearless** *July 11, 2018 22:58*

ok, fair enough :-)


---
**Åbsølem** *July 13, 2018 20:47*

Hey Arthur, happy too see you're alive. Have you got a release date?


---
**Airhead Bit** *December 13, 2018 00:59*

**+Arthur Wolf** Perhaps try the daughter board concept for the Pro, a lot less costly. [https://goo.gl/iHs7CS](https://goo.gl/iHs7CS)  


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/UKyV1LjzvwV) &mdash; content and formatting may not be reliable*
