---
layout: post
title: "Shared on July 23, 2018 20:56...\n"
date: July 23, 2018 20:56
category: "Development"
author: "Arthur Wolf"
---
[http://gerblook.org/pcb/H278ah7RW35wL7T9SbEsrh#front](http://gerblook.org/pcb/H278ah7RW35wL7T9SbEsrh#front)





**"Arthur Wolf"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2018 21:03*

Looks great! 


---
**Eric Lien** *July 23, 2018 21:12*

Looking great. Are you planning on stallgaurd on the 2660 built in to the base config? I have heard it can be tricky to configure, and probably is very machine dependant... But sure would be cool to have it out of the box, even if it needs tweaking to get dialed in per application.  Looking great, thanks for all the posts showing updates. V2 smoothie is a long time coming, and I look forward to seeing it in action.


---
**Arthur Wolf** *July 23, 2018 21:13*

Nah that'll all be optional, I see no reason to have it by default. Might change if enough users use it.


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2018 21:18*

Based on my conversations with OEM’s, stallguard takes a lot of tuning to configure correctly - it depends on factors including the machine right down to the specs of the motor being used. 



Would result in a lot of support tickets if there was even remotely a whiff of “works out of the box” and will probably result in quite a few support inquiries anyways. 


---
**Eric Lien** *July 23, 2018 21:56*

Understood. I figured as much. I was just hoping if it were mainline perhaps there could be a calibration script written somehow ALA the PID tuning  or Delta Arm calibration , where it runs through some tests and autocalibrates. But that's just the lazy user in me wanting things handed to me :)



If it were I wonder if you could you use Stallgaurd as the feedback loop for the delta arm calibration when the hotend touches the bed. It would be pretty cool to auto bed level with just that feedback :)


---
**Arthur Wolf** *July 23, 2018 22:14*

Nah this has been tried years ago already, and it's just not good enough.


---
**Jonathon Thrumble** *July 24, 2018 10:38*

saving my pennies already. 🤣


---
**Jeremie Francois** *July 24, 2018 21:43*

Now that is neat, just a few overlapping labels on the silkscreen :) But much nicer than the v1 regarding connector placement!


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/3gM4apBC9PL) &mdash; content and formatting may not be reliable*
