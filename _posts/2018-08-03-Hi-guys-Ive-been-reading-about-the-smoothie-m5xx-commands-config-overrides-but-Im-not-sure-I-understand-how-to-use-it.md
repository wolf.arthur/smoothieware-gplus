---
layout: post
title: "Hi guys, I've been reading about the smoothie m5xx commands (config overrides) but I'm not sure I understand how to use it"
date: August 03, 2018 02:05
category: "Help"
author: "Chuck Comito"
---
Hi guys, I've been reading about the smoothie m5xx commands (config overrides) but I'm not sure I understand how to use it. Say for example I want to store g28.1 through a shutdown. What would the correct syntax be? Some examples of this command would be great!!





**"Chuck Comito"**

---
---
**Arthur Wolf** *August 03, 2018 09:28*

When you issue M500, <b>some</b> of your current config overrides are saved to the SD card. You can see which by doing M500 to save then M503 to display. Is G28.1 in there ?


---
**Wolfmanjm** *August 03, 2018 10:58*

Yes G28.1 is saved with M500. and  viewed with M503 as Arthur suggests.


---
**Chuck Comito** *August 06, 2018 14:31*

Hi **+Arthur Wolf** and **+Wolfmanjm** , I've been messing around with m500. I've homed my machine and then zero'd my work piece, issued g28.1 and the issued m500. This seems to work but bcnc doesn't show me the status when issuing m503. Is bcnc capable of showing the status or do I need another host and if so what is recommended? I'm a bit unclear as to what I'm doing :-)  thanks for the help!!


---
**Wolfmanjm** *August 06, 2018 14:34*

M503 in the terminal should work, if not then use a console program




---
**Chuck Comito** *August 06, 2018 14:39*

**+Wolfmanjm** so, can you recommend a console program please? Thanks. 


---
**Wolfmanjm** *August 06, 2018 14:41*

i don't use windows so sorry I can't. on linux i use picocom


---
**Wolfmanjm** *August 06, 2018 14:43*

I just tried bcnc in the terminal and M503 works fine.


---
**Chuck Comito** *August 06, 2018 17:19*

Hi **+Wolfmanjm**, you are correct and it works just fine in bcnc. I just had to know where to look. Thank you for your help!




---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/Tnwn6HWTfJH) &mdash; content and formatting may not be reliable*
