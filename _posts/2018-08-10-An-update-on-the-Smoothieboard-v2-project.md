---
layout: post
title: "An update on the Smoothieboard v2 project"
date: August 10, 2018 11:01
category: "General discussion"
author: "Arthur Wolf"
---
An update on the Smoothieboard v2 project.



Please share around !




{% include youtubePlayer.html id="vGpA_oNiEmY" %}
[https://www.youtube.com/watch?v=vGpA_oNiEmY](https://www.youtube.com/watch?v=vGpA_oNiEmY)





**"Arthur Wolf"**

---
---
**Antonio Hernández** *August 10, 2018 11:19*

Hope to hear very soon the official  kickstarter dates !


---
**Gary Tolley - Grogyan** *August 10, 2018 17:55*

Nice

Did you manage to sort out the trace lengths to make sure that they are all equal when heading to the stepper drivers?

Is CAN bus available? This will enable remote displays, which could have a flash drive to print the gcode from, as such without compromising on losing gcodes from being corrupted during transfer due to EMI. 


---
**Arthur Wolf** *August 10, 2018 18:22*

**+Gary Tolley - Grogyan** Why would the traces need to be equal heading to the stepper drivers ? I'm fairly sure we didn't do that.

CAN will be available as one of the many extension boards. For your use I think the simplest solution is to use the USB-A port with a thumb-drive, would that work for you ?


---
**Gary Tolley - Grogyan** *August 10, 2018 18:28*

**+Arthur Wolf** as your processor gets faster, the time it takes for an individual signal to reach each of the drivers will be mismatched when a gcode on at least 2 axes is done simultaneously, as such, artefacts looking like layer shifts can occur.

This isn't much of a problem with slower processors as this error is negligible. 


---
**Arthur Wolf** *August 10, 2018 18:30*

We don't expect speeds faster than 300khz with this board, on these distances of a few centimeters, and differences in distances of a few millimeters, it's really not going to have any sort of measurable/detectable impact.


---
**Gary Tolley - Grogyan** *August 10, 2018 18:53*

Speed limiting the board doesn't seem efficient, with so much more processing power available.

I would suggest scoping one of your prototypes to see how much there is an error.

You could be quite alright, and this error could be small enough to ignore, however, I would double check that, just to be on the safe side.



As a technician myself, I have have great respect for an engineer that does trace length equalisation, even if it does seem overkill at times. 


---
**Arthur Wolf** *August 10, 2018 18:58*

We are not speed limiting the board, even assuming best possible scenario ever ( fast gcode processing on the M4, and step execution on the M0 highly optimized ) we don't expect to get any faster than 1Mhz ( this is a hardware limit due to the task we are executing ), and even at that speed ( which even industrial controllers don't really typically use ) the trace length ( for a board this size ) is irrelevant ( in the context of stepper motor control, it might make sense for some other usage you are more familiar with ).


---
**Gary Tolley - Grogyan** *August 10, 2018 19:05*

👍


---
**Daniel Gliebe** *August 15, 2018 03:43*

Thanks for the update.  I use a Smoothie V1 on my 80W Co2 laser cutter I built nearly 1 year ago.  I could never get certain jobs like raster engraving to stream via Ethernet from VisiCut.  Had to use USB.  Now of course I use LightBurn (which I Love!) that also forces me to use USB.  Do you think Smoothie V2 will allow for the move to sending jobs in my scenario via Ethernet some day?  Any other benefits I should know about which would make V2 better than V1 for laser cutting?  Thanks again!


---
**Arthur Wolf** *August 15, 2018 10:07*

Yes we expect much better ( faster, more stable ) ethernet, and much faster engraving for v2. V1 could do these but had severe limitations.


---
**Matt Wils** *August 15, 2018 18:07*

Following. 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/RqRwpbra3VN) &mdash; content and formatting may not be reliable*
