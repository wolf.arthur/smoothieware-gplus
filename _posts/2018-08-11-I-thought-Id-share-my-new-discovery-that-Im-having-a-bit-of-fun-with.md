---
layout: post
title: "I thought I'd share my new discovery that I'm having a bit of fun with"
date: August 11, 2018 00:00
category: "General discussion"
author: "Chuck Comito"
---
I thought I'd share my new discovery that I'm having a bit of fun with. I'm sure this is not new news but it might help others that are looking for an alternative gcode sender that works well with smoothie.



[https://cnc.js.org/docs/](https://cnc.js.org/docs/)



So far it's pretty cool. Does anyone have an opinion on this software they'd like to share?









**"Chuck Comito"**

---
---
**Arthur Wolf** *August 11, 2018 09:44*

Do you find it works well ?


---
**Eric Lien** *August 11, 2018 11:41*

I have good luck using it. I like the easy setup of macro buttons, and the ui is more customizable and intuitive than others I have used. Only thing missing is a built in probing and autolevel Routine/UI like chilipeppr or bcnc.


---
**Chuck Comito** *August 11, 2018 13:52*

I honestly don't have an opinion yet. It looks promising. I had a job to run so I resorted to bcnc because I know it a bit better. Bcnc will stop when you tell it to. Cncjs sort of lingers for a bit before it stops.  I think it's got to empty it's queue and I don't have an estop connected yet. 


---
**James Rivera** *August 12, 2018 20:05*

I use it on my TinyG v8 board. Love it!


---
**James Rivera** *August 12, 2018 20:05*

**+Eric Lien** I use the built in probe tool.


---
**Eric Lien** *August 12, 2018 21:35*

**+James Rivera** but the cncjs probe cannot yet do mesh level. For cutting large sheet aluminum this is important or you can have too deep of a DOC, leading to chatter. For me mesh level is the only missing piece for cncjs.


---
**Chuck Comito** *August 13, 2018 00:47*

**+Eric Lien** , what host do you use that has mesh leveling, bcnc? 


---
**Eric Lien** *August 13, 2018 00:57*

**+Chuck Comito** both Chilipeppr (tinyg machines, or Grbl), and BCNC (Smoothieware) can do it.


---
**James Rivera** *August 13, 2018 15:59*

BCNC was on list of hosts to try, but once I got CNC.js working I didn’t bother. **+Eric Lien** it sounds like you have tried both. Outside of the mesh leveling, which do you prefer?


---
**Eric Lien** *August 13, 2018 18:48*

For UI... Cncjs, hands down. BCNC has a really dated feel as far as graphics and workflow.



But for features... BCNC has cncjs beat for now. Plus BCNC running as native compiled code, versus node.js setup... I would say it will be less likely to run into memory and performance issues.


---
**James Rivera** *August 13, 2018 19:08*

**+Eric Lien** Thanks for sharing your opinion. I just took a quick look at the github home page wiki and realized it is also a CAM tool! Have you used its CAM functionality, or do you only use it as a gcode sender?



[github.com - bCNC](https://github.com/vlachoudis/bCNC/wiki)


---
**Eric Lien** *August 13, 2018 22:01*

**+James Rivera** i have only used it as a sender.


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/YQhB48q6kuL) &mdash; content and formatting may not be reliable*
