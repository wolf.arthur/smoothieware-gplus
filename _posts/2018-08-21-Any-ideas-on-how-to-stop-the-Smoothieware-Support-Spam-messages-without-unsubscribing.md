---
layout: post
title: "Any ideas on how to stop the Smoothieware-Support Spam messages without unsubscribing ?"
date: August 21, 2018 14:45
category: "General discussion"
author: "Ronald Whittington"
---
Any ideas on how to stop the Smoothieware-Support Spam messages without unsubscribing ?   Google marks them important due to the group sending them.





**"Ronald Whittington"**

---
---
**Arthur Wolf** *August 21, 2018 14:50*

Yeah I'm very sorry about that. I didn't create the group, and the person who did doesn't answer. At the moment I'm working under the assumption that they are just on vacation since it's summer, but if by end of september they haven't answered, I'll start considering creating a new group.


---
**Ronald Whittington** *August 21, 2018 14:52*

**+Arthur Wolf** Thank you, If I have unsubscribed accidentally, you will see me reapply soon. Good Luck


---
**Ronald Whittington** *August 22, 2018 00:02*

Once I reported and unsubscribed it got worse, now I get a new one almost as soon as I delete the old one...too bad people think they need to mess with these places for no apparent reason.


---
*Imported from [Google+](https://plus.google.com/+RonaldWhittington/posts/gsGeygHVCeX) &mdash; content and formatting may not be reliable*
