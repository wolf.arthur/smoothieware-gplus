---
layout: post
title: "Request For Comments: Mark is putting the last touches on the silk for the proto run of v2"
date: August 22, 2018 19:36
category: "General discussion"
author: "Arthur Wolf"
---
Request For Comments: Mark is putting the last touches on the silk for the proto run of v2.

Would love it if you took a look at the current silk/board markings, and gave us feedback on what you would like to see labeled/indicated on the board? What can we improve? What information would you like to see that's missing? What's too small, too big, too much information, not enough ?

Think about what you look for when using the board, what a newcomer might be looking for/trying to understand, etc. 

Any comments/help extremely welcome. Be quick we are sending this to production in a few days at most :)



Thanks a ton in advance :)



![missing image](https://lh3.googleusercontent.com/-6BIZyN4KPGM/W327N3JRzGI/AAAAAAAAQiU/1H11ZgA5YHo2Sc-zeHgzspbTIYhH6AeugCJoC/s0/wvLp0TIg.png)
![missing image](https://lh3.googleusercontent.com/--R00dovAC4Q/W327NzJwlAI/AAAAAAAAQiU/-q5fI77VLWkUGGdWqHGu5HfqLNGRf1gBACJoC/s0/KqXHCykw.png)

**"Arthur Wolf"**

---
---
**Jonathon Thrumble** *August 22, 2018 20:19*

The gamma stepper driver isn't helping my ocd but other than that all looks clear and well set out. Good job, can't wait to get my hands on one. 


---
**Ray Kholodovsky (Cohesion3D)** *August 22, 2018 20:35*

I would remove the component designators from the top silk.  It is very cluttered right now.   It would be useful to have certain markings on the top of the board, for example the + - S of the endstop row.  If my board is already mounted, I cannot look at the bottom for this information. 


---
**Thomas Arthofer** *August 22, 2018 21:17*

Some points in any random order:

- Make the stepper designations (alpha, beta, ...) stand out more. (maybe KiCad can do inverted text as altium can do)

- Optionally: Do this for all user-accessible information (YMIN, Connectors, Pin Numbers)



Otherwise: Good work, do you need more testers? ;)


---
**Arthur Wolf** *August 22, 2018 22:12*

**+Ray Kholodovsky** The final version won't have the designators, we just keep it on the proto to help factory with setting things up. 

Endstops do have -/+s on top, it's just very tight against the border. It'll be readable though, we'll make sure.

**+Thomas Arthofer** Yeah negative ( a-la-arduino ) labels is something I want but that we are not taking time to do for the proto. It should be there in the final version though.



If anyone wants to help test the board, and wants to be warned as soon as the kickstarter starts, you can email me at wolf.arthur@gmail.com


---
**Sébastien Plante** *August 25, 2018 12:43*

I must say wow... pretty nice job !



There's some placement, like C23 on the top that feel a bit confusing, but it's pretty nice ! Lot of stuff on this !


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/PkeaVudMNTr) &mdash; content and formatting may not be reliable*
