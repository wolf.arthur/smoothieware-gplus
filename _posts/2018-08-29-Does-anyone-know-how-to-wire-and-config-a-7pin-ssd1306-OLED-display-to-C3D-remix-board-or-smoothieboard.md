---
layout: post
title: "Does anyone know how to wire and config a 7pin ssd1306 OLED display to C3D remix board or smoothieboard ?"
date: August 29, 2018 09:45
category: "General discussion"
author: "Xiaojun Liu"
---
Does anyone know how to wire and config a 7pin ssd1306 OLED display to C3D remix board or smoothieboard ?



<b>Originally shared by Xiaojun Liu</b>



Hi guys，



Can you show me how to wire/config a ssd1306 OLED display to the C3D remix？



I have a 7 pin，1.3inch OLED display with SPI interface. These 7 pins are GND VCC D0 D1 RES DC CS. The display is shown in the following pic. I connected these pins to the exp1 and exp2 pins on C3D board，and write the config.txt file，as pic 2 and 3 showed，but screen didnt work. Then I changed the RES pin to the 2.11 pin in ext2，changed config.txt file accordingly，as pic 4，5 showed，but didn't help. 



Unfortunately I couldn't find any good instructions on this one:( Only saw some talented guys have succeeded, like **+René Jurack** and **+Ray Kholodovsky** etc. I’ve tried my best to gather and understand the pieces of information they left. But I may understand or do things wrong. 



Any help is appreciated!



BR

XJ 



!![images/5dccc7bc2fdcd32b76f703a69ea3ef2f.jpeg](images/5dccc7bc2fdcd32b76f703a69ea3ef2f.jpeg)
!![images/499ee27d6ff0c6448013090ecf36ab41.jpeg](images/499ee27d6ff0c6448013090ecf36ab41.jpeg)
!![images/98a862b7288299d329a88401113285b1.png](images/98a862b7288299d329a88401113285b1.png)
!![images/ef15748125113573162eb315f415840f.jpeg](images/ef15748125113573162eb315f415840f.jpeg)
!![images/f138f6e875e739277c3a8e3ad843f5f9.png](images/f138f6e875e739277c3a8e3ad843f5f9.png)

**"Xiaojun Liu"**

---


---
*Imported from [Google+](https://plus.google.com/+XiaojunLiu/posts/6M2FeQAyC15) &mdash; content and formatting may not be reliable*
