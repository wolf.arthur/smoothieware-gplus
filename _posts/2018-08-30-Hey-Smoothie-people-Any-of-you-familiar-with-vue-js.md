---
layout: post
title: "Hey Smoothie people. Any of you familiar with vue-js ?"
date: August 30, 2018 12:49
category: "Development"
author: "Arthur Wolf"
---
Hey Smoothie people. Any of you familiar with vue-js ? In particular vue-cli.



I'm building the new web interface for v2 on top of that, and I have some questions on how to do things correctly, fix some warnings, etc. Having somebody with some ( well, <b>any</b> really ) vue-js knowledge to talk to, would be super helpful. 



Please if you feel you could help even a bit, email wolf.arthur@gmail.com

Also if you are very familiar with es6, you could similarly be of help.



Thanks !





**"Arthur Wolf"**

---
---
**Sébastien Mischler (skarab)** *August 31, 2018 05:41*

riot.js !


---
**Arthur Wolf** *August 31, 2018 07:01*

[medium.com - Vue.js vs Riot – tldr tech – Medium](https://medium.com/tldr-tech/vue-js-vs-riot-6392807ce5e6)



Anything to say in your defense ?!?



( eh, I should have guessed <b>you</b>'d be answering to this post :p )


---
**Sébastien Mischler (skarab)** *August 31, 2018 14:00*

il est un peu vieux cet article... sinon pas grand chose a défendre, j'ai pas mal utiliser vue.js que j'aime beaucoup, mais pour un petit/moyen projet riot.js est bien plus adapter (je pense), et bien plus simple a prendre en main.


---
**Arthur Wolf** *August 31, 2018 15:02*

Bah, c'est que la quatorzieme reprise a zero du projet d'interface web smoothie, si ca se trouve #15 utilisera riot :p


---
**Sébastien Mischler (skarab)** *August 31, 2018 16:10*

#metoo


---
**James Rivera** *August 31, 2018 19:18*

Jeez, another day, another js framework! I’m no expert on js, but is plain old jquery/jqueryui insufficient? Am I just a dinosaur?


---
**Arthur Wolf** *August 31, 2018 19:28*

**+James Rivera** I use jquery too :) It does a completely different job.

It's a different way to structure the app.

[gist.github.com - Hello.vue](https://gist.github.com/chrisvfritz/e2b6a6110e0829d78fa4aedf7cf6b235)

 


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/TXNMHJWY9TR) &mdash; content and formatting may not be reliable*
