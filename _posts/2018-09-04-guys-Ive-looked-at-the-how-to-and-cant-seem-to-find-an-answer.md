---
layout: post
title: "guys, I've looked at the \"how to\" and can't seem to find an answer"
date: September 04, 2018 00:17
category: "Help"
author: "Chuck Comito"
---
guys, I've looked at the "how to" and can't seem to find an answer. I have my y axis running from 2 steppers and 2 drivers. I've been looking at this: [http://smoothieware.org/general-appendixes#doubling-stepper-motor-drivers](http://smoothieware.org/general-appendixes#doubling-stepper-motor-drivers).

So I connected both drives and both motors to their respective drivers and then daisy chained the signal from one driver to the other (I did this on the drivers themselves). I'm pretty sure this is wrong because one motor works and the other doesn't. Perhaps I missed something, any ideas?









**"Chuck Comito"**

---
---
**Sébastien Plante** *September 04, 2018 01:21*

did you update the config?


---
**Chuck Comito** *September 04, 2018 01:57*

 **+Sébastien Plante** what would I need to update? I'm only using one port for the 2 drives. 


---
**Sébastien Plante** *September 04, 2018 02:10*

**+Chuck Comito** Oh, no config then. So I guess they are in series? 



Just watch to make sure you don't go over the Amp for the driver !



You might also want to try in parallel ?


---
**Sébastien Plante** *September 04, 2018 02:11*

Look at this wiring

[rigidtalk.com](http://rigidtalk.com/wiki/images/thumb/8/82/Series_Connected_Stepper_Motor_Wiring_Diagram.JPG/350px-Series_Connected_Stepper_Motor_Wiring_Diagram.JPG)


---
**j.r. Ewing** *September 04, 2018 03:41*

Did you turn on power in the config file for the second driver


---
**Sébastien Plante** *September 04, 2018 04:00*

**+j.r. Ewing** is using the same port for both :)


---
**Chuck Comito** *September 04, 2018 11:15*

I'm using 2 drivers from one port. Tb6600 cheapie. In parallel. 


---
**Sébastien Plante** *September 04, 2018 11:21*

**+Chuck Comito** Oh... external drivers...



look here then : [http://smoothieware.org/general-appendixes#external-stepper-drivers](http://smoothieware.org/general-appendixes#external-stepper-drivers)



You need to solder new pin on the board, did you do that ?



Tell exactly what you did, cause it's a little confusing right now !

[smoothieware.org - general-appendixes [Smoothieware]](http://smoothieware.org/general-appendixes#external-stepper-drivers)


---
**Chuck Comito** *September 04, 2018 13:22*

I wired both external drivers to their own power (36vdc supply) I then wired each driver to their respective stepper motor. Then, I wired the EN, DIR, STP, and ground to the the first driver and then added jumpers from the first driver to the second driver with for each of those signals. All my wiring has continuity and power. I may need to scope the second driver or even swap it out. Perhaps it's a dud...


---
**Sébastien Plante** *September 04, 2018 13:26*

Hum yeah... look good. Try swaping the stepper, if it dosent work, try swaping the driver and see if it's still working/not working... you might be able to isolate the problem! Good luck !




---
**Chuck Comito** *September 04, 2018 22:17*

So after some testing it was a faulty driver. The good thing is I figured it out. Bad thing is I gotta buy a new one. Thanks for your help all!!


---
**Sébastien Plante** *September 05, 2018 00:47*

you might be able to drive both motor from the same driver if it's powerful enough :)


---
**Chuck Comito** *September 05, 2018 00:58*

I thought about that but my drivers are slightly underrated to begin with. Ideally my motors want 60vdc. I was originally powering them all with a 24vdc supply but have since upgraded to a 36vdc. I can't find 60 volt stepper drivers that don't cost an arm and a leg. The drivers I have are capable of 42vdc max. I built most of this prior to fully understanding all the requirements so now I'm just trying to put a bandage on to get me by until I can afford a proper setup. 


---
**Sébastien Plante** *September 05, 2018 12:41*

oh... hope they will work well if you under-volt them that much. I feel like you will have a lot of step skipping under load :(



Maybe it will cost less to find some 12V or 24V stepper and use full power !


---
**Thibaut Robin** *September 17, 2018 10:59*

Hi Chuck I'm having the same problem except both motors do run but with one over heating.  For what I think, the ideal solution would have been a simple software "mirror or duplicate" function that would allow duplicating signal from one output to an other using the config file.  I'll take a look at the firmware ASAP to see if this is possible to implement it.  Take a look at my queue; Antonio Hernandez has a few suggestions worth trying.

[plus.google.com - Hi folks! Hopefully once this last problem solved, my OpenBuilds C-Beam Mach...](https://plus.google.com/u/1/113509964356441672141/posts/GUwM94NWzcj)

I'll keep you informed if I find a solution.

Cheers.




---
**Sébastien Plante** *September 17, 2018 11:21*

**+Thibaut Robin** [http://smoothieware.org/general-appendixes#doubling-stepper-motor-drivers](http://smoothieware.org/general-appendixes#doubling-stepper-motor-drivers) 

[smoothieware.org - general-appendixes [Smoothieware]](http://smoothieware.org/general-appendixes#doubling-stepper-motor-drivers)


---
**Chuck Comito** *September 19, 2018 01:33*

**+Thibaut Robin** that would be nice. I did get my new driver and got it to move properly. The problem now is that the y axis (the one with the dual motors and drivers) shut down after a short period of use. I'm trying to figure out if its an over heating problem or something else. Let me know if you find a solution and I'll do the same. 


---
**Thibaut Robin** *September 19, 2018 09:56*

**+Chuck Comito** Ok Chuck, I will keep you informed as soon as I have time to work on it.  What is your actual setup? is it open-drain or with level shifters?  Have you checked the micro switches setup on the drivers (max power setup, current reduction on hold, stepping)? Do the motors stay on hold if you disconnect the drivers from the SmoothieBoard?

I haven't got then time to work on it but chances are my problem comes from an unbalanced resistivity between both motors due to different cable lengths. have a look at the end of my message queue.  I'll keep you informed, maybe tooday...


---
**Chuck Comito** *September 19, 2018 10:23*

**+Thibaut Robin**  it is open drain. My setup is exactly like yours based on your photos. I have checked all the switches. I have not tried checking the motor hold when disconnected. I'm going to measure the current draw while running and see if that gets me anywhere. I was also thinking of making some changes internal to the drivers to share the load more equally. Maybe a breakout with a resistive load to equalize them. I'll let you know in more detail what I mean by that later today. I'll also post my wiring diagram for comparison. 


---
**Thibaut Robin** *September 19, 2018 11:36*

**+Chuck Comito** Could the shut down be a software behaviour?  What control soft do you use?


---
**Thibaut Robin** *September 19, 2018 14:12*

**+Chuck Comito** Just mesured both resistivity of all lines going from the drivers to the motors.  All of them are 0.55 Ohm, I also mesured the motors coils just in case but all of them are 1.7 Ohms. So my problem clearly has nothing to do with the motors nor the cabling...

I was thinking of connecting the motor outputs of both drivers toogether to deliver the samme amount of current to both motors. i.e.  A1+ to A2+, A1- to A2- etc... Do you think that would toast the drivers?  


---
**Sébastien Plante** *September 19, 2018 19:16*

Just to make sure, you wired the EN, DIR, STP from pin on the center of the board (you have to solder those pin) and not the one labeled as M1, M2, etc... ?



Since you are using two external driver, you can't drive the driver from the internal drivers. 



Looking at cable length is not necessary at this point, you will have a very small effect on a very big cable difference !


---
**Chuck Comito** *September 19, 2018 21:01*

**+Sébastien Plante**, yes both myself and **+Thibaut Robin** both wire them correctly using the pins and not the motor connections. Thibaut, I did a modification today. I opened the driver and removed the crappy thermal pad and tiny piece of aluminum all of it was mounted to and added a much larger piece of aluminum that is the correct thickness and the proper thermal compound. I also plan to add to 12vdc blowers. I should be done with that mod within the hour and will let you know how it performs. To answer your other question about wiring the motor phases across the 2 drivers, I don't that would hurt but I don't see it solving the problem. I did think that we could perhaps pull off the sense resistors and run them on a breakout board in series with both drivers. I think this is asking for trouble though.


---
**Thibaut Robin** *September 19, 2018 21:21*

**+Chuck Comito** On my side I've tried to directly connect the drivers on the 3.3v control pins with very short wires.  It works!  I still have a slight temp difference between the motors but no more overheating.  I'll go back to open drain tomorow but will probably wire the 5v directly from the small ATX power used to power the Smoothieboard rather than from the endstops 5v; with common ground...

As for the drivers cooling, I made an aluminium box to insure maximum heat dissipation (and faraday protection, planning to use plasma cutter).  I have 2 fans blowing fresh air directly on the drivers and both 24v AC and ATX sucking hot air outside the casing. 

The reason I wanted to wire the motor phases across the 2 drivers was so I'd be sure they both get the same Amps...  I'll keep you informed.  Cheers.


---
**Chuck Comito** *September 19, 2018 21:25*

Hmm. Send me a pic when you can. But I also am wondering where you're located. Yesterday it was in the 90's here and I'm in my garage so it was probably hotter. Today though is 77 so it should be cooler by default. 


---
**Thibaut Robin** *September 19, 2018 22:03*

South ouest of France near Bayonne 93 F°  34C° tooday and still 75° at 00:00 :-)  What about you?




---
**Chuck Comito** *September 19, 2018 22:08*

24c currently in Detroit Michigan 


---
**Thibaut Robin** *September 19, 2018 22:13*

Detroit! Love it!  I spent 5 years in N.J during the 80's

  


---
**Thibaut Robin** *September 19, 2018 22:17*

**+Chuck Comito** Went to Detroit a few times...


---
**Chuck Comito** *September 19, 2018 22:38*

**+Thibaut Robin** That's awesome. Glad you like it... I did want to mention though that with the lower ambient temperature and the new heatsink setup I also appear to be up and running. Looks like I'll need some climate control out in my garage. The wife will love that!! LOL Anyway, I plan to give the router a workout tonight so I'll post back how it kept up. Let me know if you modifications continue to work as well!




---
**Thibaut Robin** *September 19, 2018 22:41*

**+Chuck Comito** I will, take it easy.


---
**Chuck Comito** *September 19, 2018 23:07*

**+Thibaut Robin** Welp, for the record the Y axis is still going strong. The X axis however dropped out.. Too HOT! Looks like I'll have to make my modifications to all the drivers and then pray we don't have a hot day.. I'm really bummed. Good drivers are really expensive!


---
**Chuck Comito** *September 21, 2018 00:18*

So in case anyone else needs the help, I added a new heatsink and new thermal compound as well as 50mm blowers and now have a working machine. I plan to upgrade the drivers to 7amp versions in the near future. This should solve the problem completely. 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/784ntwJV6Y5) &mdash; content and formatting may not be reliable*
