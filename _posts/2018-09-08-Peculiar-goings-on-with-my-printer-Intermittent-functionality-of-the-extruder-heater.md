---
layout: post
title: "Peculiar goings on with my printer. Intermittent functionality of the extruder heater"
date: September 08, 2018 12:13
category: "Help"
author: "Steven Kirby"
---
Peculiar goings on with my printer. Intermittent functionality of the extruder heater. Temperatures of both the bed and extruder coasting way above specified temps. Heater cartridge for extruder replaced with new module seemed to restore heating but then it got so hot it was burning the pla and halted due to overtemp, now refuses to heat again (resistance still same as fresh cart). Sensible temperature readings coming from thermistors at room temperature so smoothie is getting the correct feedback. I'm using the printer with octoprint which has been somewhat buggy of late but I'm not sure how it could be causing the problem. I know this is all elementary stuff and I wouldn't normally have any problem troubleshooting it but I'm at a loss. Anyone experienced similar problems or got any clues for a fix?





**"Steven Kirby"**

---
---
**Arthur Wolf** *September 08, 2018 19:05*

Can you try printing with pronterface, trying to get the problem to occur, and when it does show us the temperature graph ?


---
**j.r. Ewing** *September 08, 2018 23:03*

**+Arthur Wolf** I had something similar happen to mine I thought the bed thermistor died replaced it. Temps were reading 870c. Replaced thermristor still reads 870c. Moved it to different port it works fine.  So the port on the board has died it’s working ok but I’m not going to have enough ports to upgrade with this board In the future. 


---
**Steven Kirby** *September 09, 2018 01:53*

**+Arthur Wolf** Octoprint has a temp graph too. There's a noticeable fluctuation in the hotend temp during prints that I've not seen before, usually it's a flat line. When I was checking on the print through the browser earlier I caught a snippet of the graph that had two identical dips a couple of minutes apart. A rapid drop of a few degrees and a somewhat slower recovery back to 200. When the print failed the temperature dropped significantly albeit slowly at first, until the point at which I'm assuming the print failed. The gradient then increases as no more attempts to fire the heater cartridge are made and the temperature cools to room temp. Interestingly though the reason reported by smoothie was temp runaway it reported a value of -23 to some insane number of decimal places! lol. This would usually make me think dodgy thermistor but everything checks out just fine with the multimeter and the thermistor is functioning perfectly when I restart the machine. It's a head scratcher, that's for sure!


---
**Douglas Pearless** *September 09, 2018 02:33*

I had a similar issue and it turned out to be the thermistor; have you run a PID [smoothieware.org - temperaturecontrol-pid-autotuning [Smoothieware]](http://smoothieware.org/temperaturecontrol-pid-autotuning) ?


---
**Steven Kirby** *September 09, 2018 10:22*

**+Douglas Pearless** I did the PID autotune when I built the machine. Nothing significant has changed with my setup since, so I wouldn't have though that was the problem. I do have some spare thermistors, so for the sake of being thorough and the minimal time cost of trying it out; I'll replace it see if that changes anything. 👌


---
**Steven Kirby** *September 09, 2018 18:44*

Well, thermistor replaced and still no joy, preheat competed just fine but print failed before it even got off the ground. If it's not software/firmware related then my only conclusion is that a solder joint somewhere in my cable runs that're routed through the cable chains, has been fatigued by constant flexing during prints. A design flaw on my part. (it's not the only one, but hey, you live you learn!) Clean wiring comes with a cost it seems. alas stock cable lengths on components don't allow for x and z axis cable chains! :'( Well i think I'm just gonna buy an ender 3 to get me printing again and tear my whole machine down to make KirbyBot v3. Always wanted a delta and I think my smoothieboard is wasted on a cartesian machine. Gotta put that processor to good use. =D


---
**Douglas Pearless** *September 09, 2018 21:46*

Before you tear it down, try a new and temporary separate cabling for the thermistor perhaps cable tie to the drag chain and eliminate the wiring 


---
**j.r. Ewing** *September 09, 2018 22:19*

Power supply ok? The point you mention that it fails is right at first peek power draw (all heaters on and motors start to move)


---
**Steven Kirby** *September 10, 2018 08:12*

PSU is overrated for the application. It's a 500w atx with a max draw of 25A on the 12v rail. Should be plenty of juice available no matter what the machine is doing. It doesn't always fail at the start of the print that was the first time. It'll happily chug along for several hours before any failure. 


---
*Imported from [Google+](https://plus.google.com/112120819276379662423/posts/NBV2736n78r) &mdash; content and formatting may not be reliable*
