---
layout: post
title: "Hi folks! Hopefully once this last problem solved, my OpenBuilds C-Beam Machine XLarge CNC mill should, at last, work"
date: September 16, 2018 16:06
category: "Help"
author: "Thibaut Robin"
---
Hi folks!  Hopefully once this last problem solved, my OpenBuilds C-Beam Machine XLarge CNC mill should, at last, work.

It's been a long and painful way to get all the electronics to (almost) work properly...

Unfortunately when I thought I had succeeded I noticed some very unpleasant and awkward fact.

This CNC is driven by 1 x axis Nema 23, 1 Z axis Nema 23 and 2 Y axis Nema 23.

All the 4 NEMA 23 (High Torque - Peak Current is 3.0A/phase) motors are individually driven by an External DQ542MA Stepper Motor Driver;

The 4 DQ542MA External Drivers are hooked up to alpha, beta and gamma breakout pins on the SmoothieBoard V1.1 5X

Since The smoothieboard ENABLE, DIRECTION and STEP breakout pins are 3.3v and External DQ542MA Stepper Motor Driver expects 5v I have used 4 Logic level converters to pull the 3.3v signals up to 5v.

Check the picture #1 it is self explanatory ;-)

Now here is my problem:  The Y axis beeing a dual motor axis I've connected both Y1 an Y2 DQ542MA Stepper Motor Driver in parallel to the beta output of the smoothieboard. (see picture #2)

That seemed to work and a few bCNC motion tests showed that both motors where synch and working together.  But for some awkward reason one of the two driven NEMA motors is heating a lot more than the other one!

Any suggestion on how to fix this problem??  I've tried swapping them and replacing them and always end up having one of the two motors heating to much as the other one stays almost cool.

Note that both Y1 and Y2 DQ542MA have the same dip switches settings and should deliver the same Amps to each motors. (also tried swapping an replacing DQ542MA with no success).

Also Note that the smoothieboard is an X5 so I could be using the beta (Y) output to drive Y1 and the epsilon (5th output) to drive Y2 but in this case the smoothieware documentation states that to enslave an output to an other one, both outputs breakout pins should be connected together. "To enslave a driver to another, you will need to connect the control pins for both drivers together". But this leaves no room for connecting external stepper drivers!!! Any thoughts?

Any known software solution to duplicate the beta signal to epsilon?





![missing image](https://lh3.googleusercontent.com/-GRhdBv_O-Bw/W55_bgtezTI/AAAAAAAAACg/Kf_a-toBqT0Baade6IBWFKPelOA2OUtuACJoC/s0/%2525232.jpg)
![missing image](https://lh3.googleusercontent.com/-hovYH9Q1N6U/W55_buMl4VI/AAAAAAAAACg/D7U4QHfH-sAevOp8IDDJG95f9T4ffJV8wCJoC/s0/%2525231.jpg)

**"Thibaut Robin"**

---
---
**James Rivera** *September 16, 2018 18:59*

I’m noob on this, but I think enslaving is specifically referring to a hardware solution, which in your case seems like one motor is drawing more power than the other. I think there should be a setting that makes the 2 motor drivers work in unison while still keeping the wires separated.


---
**Thibaut Robin** *September 16, 2018 21:06*

**+James Rivera** Thanks for answering James.  I wish It was the case! But I've been all around the documentation and I haven't found any sofware solution "that makes the 2 motor drivers work in unison while still keeping the wires separated".  I'll dig that a bit more when I'll have time. 


---
**James Rivera** *September 16, 2018 21:25*

**+Thibaut Robin** 

This looks like exactly what you need.  The motors are wired separately but the control pins are connected:



[smoothieware.org - general-appendixes [Smoothieware]](http://smoothieware.org/general-appendixes#doubling-stepper-motor-drivers)


---
**Thibaut Robin** *September 16, 2018 21:30*

**+James Rivera** Well I've seen that part, the problem is that i need the pins to be availlable so I can connect the drivers...




---
**James Rivera** *September 16, 2018 21:44*

**+Thibaut Robin** I'm not sure I see a problem here; looking at the docs on the page I linked, you should be able to do just that. It is the power that needs to be separated, and with the external drivers that should not be a problem because they each have their own power inputs.



But perhaps I'm misunderstanding the problem (as I said, I'm a noob on this). This is something I plan to do, too, so I need a valid answer to this question as well. Maybe **+Arthur Wolf** can answer this question definitively?


---
**Thibaut Robin** *September 16, 2018 21:48*

**+James Rivera** He probably, he has already answered 2 of my questions.  What I can tell you is that you will not have any problems doing it if you don't use drivers. because then you can applay the solution you where talking about.

  


---
**Jeff DeMaagd** *September 16, 2018 21:50*

I think the problem is totally unrelated to the drivers or Smoothieware. The motors might not actually be identical even if they’re labeled the same. One might be poorly made or it might be wound differently inside and given the wrong label. Check the resistance of the winding pairs of both motors to see if they match.


---
**James Rivera** *September 16, 2018 22:00*

**+Jeff DeMaagd** Ooh! Thinking outside the box. This might be worth looking into, **+Thibaut Robin**.


---
**Thibaut Robin** *September 16, 2018 22:03*

I've tryed different motors and drivers none work better.  My gess is both drivers can't be interconnected for some reasons.  I don't know much about electronics......




---
**Antonio Hernández** *September 16, 2018 22:27*

I had the same problem, but, in the first scenario, I found one signal was not properly screwed. (one motor was heating more than the other one). After that fix, the difference in temperature it's much smaller but the problem remains (I don't want to say it's not important, but the difference in temperature dropped a lot). I don't know if this could apply to all external drivers but, try to change the value of pin 4, depending of the driver, this pin could help to avoid unnecessary current consumptions when the driver is on idle state. (after 0.5 seconds without receiving signal motion or something like that). I suppose this problem could be related with the signal current on the two drivers working in parallel, and also... I'm not an expert about electronics...My stepper motors are for 4.2 amps.


---
**Thibaut Robin** *September 16, 2018 22:44*

**+Antonio Hernández** Hi & Thanks Antonio your case sound a lot like mine...  Yes switch 4 on the driver is already set for curent limitation when motor is not moving.  I've even lowered the peak curent output switches 1,2 and 3 for now so the motor doesn't heat up to fast an melt.  I'll check all the connexions and plug/cable solderings tomorow.  Thank again.  I'll keep you informed.


---
**Antonio Hernández** *September 16, 2018 23:01*

The weird case that I had is only one time that one stepper motor was very very very hot and the other was very very cold and both were working at the same time. I could say screwing the signal that was not properly screwed, the problem dropped a lot. By my side, stepper drivers were wired using open drain mode. In your case you're using the level shifter converter. Maybe if you try open drain mode and see what happens, could help. I used the level shifter but, I saw the two stepper motors not moved as requested and always was one behind the other. After some minutes, both stepper motors moved as expected but there was a lost on stepper motor steps. At the moment I don't know what could be wrong in my wiring, but that happened to me. Also, I lowered the current over my stepper motors but this could not help as expected, it's possible to lose steps if the stepper motor is not working with the rated amperage.


---
**Arthur Wolf** *September 17, 2018 08:31*

Don't use level shifters, follow [smoothieware.org - general-appendixes [Smoothieware]](http://smoothieware.org/general-appendixes#external-drivers)


---
**Thibaut Robin** *September 17, 2018 08:57*

**+Arthur Wolf** Hello Arthur, I don't understand your answer.  I have carefully read that section (like all others), and I don't understand what you are suggesting.  Are you suggesting the open drain rather than level shifters? Why not use level shifters? The level shifter solution works perfectly appart of this motor over heating problem. The appendixes does suggest a solution for two motors axes but unfortunatly this solution is a "non external stepper driver" one that uses the breakout pins needed to plug the external stepper drivers.  

I'm lost.


---
**Miguel Sánchez** *September 17, 2018 11:27*

Is it possible the two motors have very different heat-sinking in your mechanical structure?


---
**Thibaut Robin** *September 17, 2018 12:20*

Hi Miguel and thanks for your interest.  I have dismounted both motors. They are now side by side on a desk and in exact same environement. The cable are 1.5 m and 1.75 m that's the only difference.  I have tried multiple combinations with the 4 motors and 4  drivers, swaped the cables etc...  Nothing worked. As long as both drivers are used separatly, every things ok.  But when dasy chained one of the two motors is over heating.




---
**Arthur Wolf** *September 17, 2018 13:14*

**+Thibaut Robin** twice or more a year, I get somebody saying "I'm doing this with level shifters, it won't work", then I say "do the open-drain method", and then they say "oh it works now". I don't know why their level shifters wouldn't work, but why try to solve the issue when you can just get rid of them and not have a problem anymore ... don't make things more complicated than they have to be.




---
**Thibaut Robin** *September 17, 2018 13:39*

**+Arthur Wolf** Ok Arthur I wasn't aware you had already seen the case so many times.  I'll try open drain method as you suggest.  Maybe the level shifter solution should simply be removed from the documentation.  I chose this methode because it seamed "cleaner" to me.

By the way; I was thinking firmware could have a config option to duplicate/mirror  output on an other one. Something like : alpha_output_mirrorto   gamma #duplicate alpha output to gamma.

alpha_output_mirrordir  true       #duplicated alpha rotation direction true=same direction.

 Would that be possible?

 Is it already on the list for SmoothieBoard V2?

That would make things even simpler ;-)


---
**Jeff DeMaagd** *September 17, 2018 15:23*

I wouldn't eliminate them, but maybe they're something you try after pull-down doesn't work. Sometimes the level shifter really is necessary. I have genuine Gecko drivers that required too much current for the LPC chip to cleanly drive 9 lines (en/step/dir for 3 drivers). I too was told the level shifter wasn't needed but once I made one, my problems went away. ClearPath motors also require a similar amount of current on their inputs.


---
**Thibaut Robin** *September 17, 2018 15:49*

**+Jeff DeMaagd** I agree Jeff, The fact is that both Y motors do run correctly except one is over heating.  The x and z motors do run fine too. So before switching to open drain solution and re-cable everything, I'll investigate a bit more.  I tend to think the over heating could be due to the difference in length (resistivity) of the motors cables.  They are pretty long around 1.5 m   in 1.5 mm² diameter for 3 Amps peak current @24 volts; so a slight unbalance could cause one motor to drain more power than the other one.  My recent tests tend to show that the over heating motor is always the one on the shorter cable. 

Beside the problem stays the same when the drivers are disconnected from the SmoothieBoard So you are probably right 

Thanks for your message.




---
**Miguel Sánchez** *September 17, 2018 16:11*

 It was mentioned before a bad contact could be the culprit for a motor getting not enough current and so heating less than the other. Have you measured the resistance of the two coils of each motor as measured from the green screw terminals of the DM542 (A+ to A- and B+ to B-)? Both motors should give you a similar coil resistance if they do not then you have found where the problem is.


---
**Wolfmanjm** *September 17, 2018 16:46*

**+Thibaut Robin** It will not be added to the s/w as slaving in S/W would slow the whole system down for no reason as the external h/w slaving works perfectly well with no impact to the speed of the system. Especially with external drivers slaving in s/w makes no sense at all.


---
**Thibaut Robin** *September 17, 2018 17:12*

**+Wolfmanjm** That makes sens ;-)


---
**Ernst-Jan Lusink** *November 18, 2018 01:41*

Great setup, just found your post! I have it wired via Open, however like your clean cupboard setup.. 


---
**Thibaut Robin** *November 21, 2018 01:22*

**+Ernst-Jan Lusink**  Thanks Ernst I switched to open drain and every thing is ok now.  Still that problem was strange...  


---
*Imported from [Google+](https://plus.google.com/113509964356441672141/posts/GUwM94NWzcj) &mdash; content and formatting may not be reliable*
