---
layout: post
title: "Using Precision Piezo on CoreXY. Works fine as endstop"
date: September 19, 2018 02:21
category: "Help"
author: "William Batie"
---
Using Precision Piezo on CoreXY. Works fine as endstop. Also works fine as 3 Point bed leveling. Crashes on grid-leveling.

G32 code does the fallowing. Crashes into the X and Y endstops (do_home = false), tries to home Z (in space). If Z homing is tripped manually, the starts bed leveling routine (grid.size 5), but first 5 points are of bed in the Y0 direction. If I again manually trip Piezo then the first point in the in every set of 5 is off the bed in the X0 direction with the remaining 4 being OK.

G29 code again crashes into the X, Y home and the Z tries homing in space.

If during Z homing the Piezo is tripped manually, the leveling routine (do_home 5) works for all 25 points

All axes are home manually with G28 X Y, Moving to the center of the bed and G28 Z. Then return to G28 X Y, before issuing G29 or G32 command. 

Config leveling is:

# Z-probe

#gamma_min_endstop            nc

zprobe.enable		true	Set to true to enable the Z-probe module. This is used to scan surfaces, and to calibrate parameters and compensate for non-planar surfaces.

zprobe.probe_pin		1.28^!		Pin the probe is connected to.

zprobe.slow_feedrate		5		Speed in millimetres/second at which the probe seeks a surface.

zprobe.fast_feedrate		100		Speed in millimetres/second at which the probe does fast moves.

zprobe.return_feedrate		50		Speed in millimetres/second at which the probe does the return after a probe.

#zprobe.debounce_ms		1		Debounce the probe pin over this number of millisceconds. Set to 1 or 2 if your probe is too noisy and gives false readings.

zprobe.probe_height		5		Distance above the bed at which the probing is started, once the bed's height is known.

zprobe.max_z		200		Maximum Z (was gamma_max)

zprobe.dwell_before_probing		0.2		Dwell time in seconds before probing

leveling-strategy.rectangular-grid.enable         true



#################################################################################################################################

# Levelling strategy

# Example for 3-point levelling strategy, see wiki documentation for other strategies

leveling-strategy.three-point-leveling.enable         true        # a leveling strategy that probes three points to define a plane and keeps the Z parallel to that plane

leveling-strategy.three-point-leveling.point1         50.0,50.0   # the first probe point (x,y) optional may be defined with M557

leveling-strategy.three-point-leveling.point2         300.0,165.0 # the second probe point (x,y)

leveling-strategy.three-point-leveling.point3         50.0,300.0   # the third probe point (x,y)

leveling-strategy.three-point-leveling.home_first     true        # home the XY axis before probing

leveling-strategy.three-point-leveling.tolerance      0.03        # the probe tolerance in mm, anything less that this will be ignored, default is 0.03mm

leveling-strategy.three-point-leveling.probe_offsets  0,0,0       # the probe offsets from nozzle, must be x,y,z, default is no offset

leveling-strategy.three-point-leveling.save_plane     false       # set to true to allow the bed plane to be saved with M500 default is false





#################################################################################################################################

#

# Levelling strategy

#leveling-strategy.rectangular-grid.enable               true     # The strategy must be enabled in the config, as well as the zprobe module.

#leveling-strategy.rectangular-grid.x_size               330       # size of bed in the X axis

#leveling-strategy.rectangular-grid.y_size               330      # size of bed in the Y axis

#leveling-strategy.rectangular-grid.size                 5 	 # The size of the grid, for example, 7 causes a 7x7 grid with 49 points.                                                                 # Must be an odd number.

#leveling-strategy.rectangular-grid.do_home              false

#leveling-strategy.rectangular-grid.probe_offsets        0,0,0    # Optional probe offsets from the nozzle or tool head

#leveling-strategy.rectangular-grid.save                 false    # If the saved grid is to be loaded on boot then this must be set to true

#leveling-strategy.rectangular-grid.initial_height       10       # will move to Z10 before the first probe

#leveling-strategy.rectangular-grid.dampening_start      0.5      # compensation decrease point (optional)

#leveling-strategy.rectangular-grid.height_limit         1        # no compensation to apply after this point (optional)

#mm_per_line_segment                                     1        # necessary for cartesians using rectangular-grid



Where when 3 point leveling is run the leveling-strategy is rem out and vice versa. 





**"William Batie"**

---
---
**William Batie** *September 19, 2018 20:51*

Fixed the problem!!!

With a CoreXY printer, G32 crashes because when “leveling-strategy.rectangular-grid.probe_offsets        0,0,0 “ it jams the X, Y to 0, 0 disregarding the endstops.

With the Precision Piezo, since the Z-probe sensor is the nozzle itself, there is no displacement in any direction. There for I thought the setting of 0,0,0 should work, thus the crashes. 

Changing the grid.probe_offsets for the X and Y to the edge of the bed printing area, in my case X = 30 and Y = 45; i.e., leveling-strategy.rectangular-grid.probe_offsets        30,45,0, put the nozzle at the edge of the printing area at the start of the G32 routine and everything works fine.

So with Smoothieware, using a Precision Piezo as Z-probe, It can do both Homing as an endstop and bed leveling-strategy. 


---
*Imported from [Google+](https://plus.google.com/111005822772712105380/posts/RGZcb12uTYD) &mdash; content and formatting may not be reliable*
