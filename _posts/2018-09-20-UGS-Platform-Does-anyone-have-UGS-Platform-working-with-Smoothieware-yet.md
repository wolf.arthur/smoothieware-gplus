---
layout: post
title: "UGS Platform?? Does anyone have UGS Platform working with Smoothieware yet?"
date: September 20, 2018 21:06
category: "Help"
author: "Chuck Comito"
---
UGS Platform?? Does anyone have UGS Platform working with Smoothieware yet? I can't get it to work but (like usual) I figured I've missed something in the setup procedure. Any pointers would be great...

I've seen mixed reviews about it but would like to give it a shot with the Fusion 360 Plugin that is available.



Thanks!





**"Chuck Comito"**

---
---
**Antonio Hernández** *September 20, 2018 21:54*

What is UGS ?.  Ah, I see, docs in smoothieware site talks about them.


---
**Kamil Steglinski** *September 20, 2018 22:47*

Universal G-Code Sender, a Java based app for sending instructions to the 3D printer or CNC machine. 


---
**Antonio Hernández** *September 20, 2018 22:55*

Yes, docs talk about it. Thanks for the info !


---
**Chuck Comito** *September 21, 2018 00:16*

Do either of you guys use it for smoothie?


---
**Antonio Hernández** *September 21, 2018 01:14*

In my case, as a host program, I use cncjs and bcnc. At the moment, the testings were only for motion. cncjs appears to work very well and also bcnc, but, you can't update bcnc from git repository and run it, because it's unstable. Only use bcnc production releases. (commits related before or after production release must not be used to run it). That's my experience about it.


---
**Chuck Comito** *September 21, 2018 01:15*

I currently use bcnc. It's great but I have a lot of trouble with it. Ugs looks great but I can't get it to control the smoothie. 


---
**Antonio Hernández** *September 21, 2018 01:23*

I made some tetings with bcnc and nothing appears to be bad. There is a testing period that I had issues using bcnc and cncjs. I made some changes related with push buttons and both worked very well. Only I have a problem that I could not found the reason why it happens, and it's related with the kill button configuration. Without this button, all appears to work fine.


---
**Chuck Comito** *September 21, 2018 01:50*

I set my own kill button macro based on the same abort command that laserweb uses. At least it's recoverable if I use it. The bcnc kill requires a restart of the software. 


---
**Antonio Hernández** *September 21, 2018 02:03*

By my side I tried to use kill button configuration. The problem is I could not found why it suddenly trigger the kill event (without pressing the e-stop button). Something similar happened with push buttons. I decided to use pin numbers with resistors enabled (2.2k at least). That change solves the problem with push buttons and other e-stop buttons that suddenly were activated without pressing them. The button that I could not change that behavior was kill button (for that reason, I removed it from pin configuration file). These were the issues that I had and I thought in that moment that bcnc and cncjs had something wrong, but, finally, the main reason was another one. 


---
**Wolfmanjm** *September 21, 2018 13:55*

There is only one host that works perfectly with smoothie as it was written specifically for smoothie (by me). [github.com - wolfmanjm/kivy-smoothie-host](https://github.com/wolfmanjm/kivy-smoothie-host)



Most other systems (including bCNC) do mostly grbl commands, and smoothie tries to handle them. bCNC works fairly well but has issues with the reset and stop commands as it expects smoothie to behave exactly like grbl (and it doesn't and can''t). I have reported that but it did not get fixed yet. UGS is not smoothie compatible at all AFAIK and is unlikely to ever be.



Although smoopi was written for rpi touch screen it now has a desktop option and I am actively working on making it more usable on a desktop as well as the small rpi 7" touch screen. I'd welcome more testers for it.


---
**Chuck Comito** *September 21, 2018 14:45*

**+Wolfmanjm** , this is awesome news to me! I will surely check it out. Thanks so much for the info. Smoopi sounds like a winner as well. Do you have a site or anything to be able to stay up to date with your progress?


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/MdRWYe5sE44) &mdash; content and formatting may not be reliable*
