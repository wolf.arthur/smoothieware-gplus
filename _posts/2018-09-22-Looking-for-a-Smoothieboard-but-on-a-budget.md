---
layout: post
title: "Looking for a Smoothieboard but on a budget?"
date: September 22, 2018 09:19
category: "General discussion"
author: "Arthur Wolf"
---
Looking for a Smoothieboard but on a budget? Not afraid of a bit of hacking/ fixing? You can now buy broken Smoothieboards for a fraction of the price. They've got things like broken Ethernet, or a broken driver etc. They can be used as-is ( depending on your needs ), or fixed, depending on how courageous you feel :)



[https://www.ebay.fr/itm/142933845406](https://www.ebay.fr/itm/142933845406)





**"Arthur Wolf"**

---
---
**Anthony Bolgar** *September 22, 2018 16:14*

I can say these are a great deal, I previously got 5 faulty boards, and 4 of them are now running various 3D printers and CNC routers. The 5th board is waiting as a spare.




---
**Petr Sedlacek** *September 22, 2018 18:09*

Nice! I'll probably get one. Replacing a mosfet is easy.


---
**ThantiK** *September 22, 2018 21:46*

I really wish I were better at in-circuit diagnosing. :/


---
**Tobias** *February 03, 2019 12:24*

The link seems to be down, are there still some broken Smoothieboards available? :-)


---
**Arthur Wolf** *February 03, 2019 12:25*

Email wolf.arthur@gmail.com and we'll see if we can find something for you.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/E2vZABtcopC) &mdash; content and formatting may not be reliable*
