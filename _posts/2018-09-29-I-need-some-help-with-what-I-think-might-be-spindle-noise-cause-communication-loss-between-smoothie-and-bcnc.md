---
layout: post
title: "I need some help with what I think might be spindle noise cause communication loss between smoothie and bcnc"
date: September 29, 2018 21:47
category: "Help"
author: "Chuck Comito"
---
I need some help with what I think might be spindle noise cause communication loss between smoothie and bcnc. The problem is random and hard to determine exactly what is the cause of the drop outs. As an example, the other day I was running jobs all day without issue and now I can't finish a simple pocket. So far the things I've tried are:



1. Installed shielded wire in every place possible.

2. Added a new USB cable with chokes on each end.

3. Rewired my controller assembly making sure to isolate AC from DC and keeping all signal lines as separated as possible.

4. Changed USB cable to shortest one possible.

5. Other gcode senders but nothing really works outside of bcnc and laserweb...



I was thinking it might be able to add some sort of RC network to my spindle wiring but I don't know where to start or if this is even a good idea or not. 



Anyway, I'm looking for things to try. Any suggestions are appreciated. Thanks!





**"Chuck Comito"**

---
---
**Don Kleinschnitz Jr.** *September 30, 2018 00:23*

Where does your driver to spindle wire run? Where is the spindle driver mounted.


---
**j.r. Ewing** *September 30, 2018 01:06*

Sounds like a broken wire intermittent problems usually wire or power supply 


---
**Chuck Comito** *September 30, 2018 03:40*

Hello **+Don Kleinschnitz Jr.**, the vfd module is mounted to an aluminum plate with all the other components. I'm not sure if that make sense so, the plate is 24 x 24 inches. Top left is 2 power supplies right next to each other. The vfd drive sits to the right of those. Below the vfd and quit some distance away, is the smoothie. Directly to the smoothie left and below the power supplies are 3 external drivers. All outputs then go to a panel which includes all the min and max end stop outputs, the spindle output and the motor outputs. I have it tucked in so it's hard to get a photo. From the connector panel all the wiring run through flexible conduit up to each of their components, ie, x y z axis, spindle, end stops. **+j.r. Ewing** I know there aren't any broken wires. I been meticulously through this time and time again. Everything is properly crimped, soldered or terminated appropriately. I wish it was that though... I was hoping some type of filter might be in order that I could build. I'm sure it's not that easy though.


---
**Don Kleinschnitz Jr.** *September 30, 2018 04:59*

**+Chuck Comito** Does the wire that is the output of the spindle driver run through the drag chain with the other wires?



I ask because in most cases when I work on this problem with folks it is caused by daisy chained grounds or wiring high current cables in the drag chain with others especially end stops. This is why my driver is on my gantry next to the motor. Probably hard to do with your VFD.



<b>Grounds</b>

If you haven't insure that your PS grounds are not daisy chained and each supply's destination has a ground returns directly to its source and that each PS is grounded to a single point on the plate with other grounds.

Run the end-stops and other sensors in a separate cable harness from the motor drives output. 

Shorten all wires that you can.

Sometimes shields create a problem when the rest of the grounds are not well designed. Sometimes both, sometimes one or the other end of the shield should be grounded.

You could add a filter to the VFD but the challenge is knowing what freg. the filter should be cut for? The noise can also be different frequencies under different loads and Gcode.  I have never found filters to be other than a band aid for grounding problems :).



<b>Find the source</b>

Find the source of the noise. Finding the source of the noise can be hard but its even harder to get rid of it after you do. 

Try running a job that fails with everything on except the VFD. Maybe even take the VFD out of the circuit if possible.

Try disconnecting the end stops from the smoothie and run.








---
**Chuck Comito** *September 30, 2018 13:46*

**+Don Kleinschnitz Jr.** Yes it does. I'm guessing by that question that it should not weather shielded or not??


---
**Don Kleinschnitz Jr.** *September 30, 2018 18:26*

**+Chuck Comito** ideally not but one can argue that sheilding should help or elliminate noise. That is why my bet is that ground shifts are your problem. 



Try running job with it turned off. 

Try grounding one or both ends. 



Can you draw a physical representation of how your supplies are wired.


---
**Chuck Comito** *September 30, 2018 21:20*

**+Don Kleinschnitz Jr.** This is what I've got going on. This is not exactly to scale but you'll get the idea. As far as grounding and only grounding one end, what am I grounding it to? Should the AC and DC share a common ground and if not, should I ground the spindle output side or the input side or both? To explain my diagram better, AC comes in and feeds both power supplies. The DC out goes to a terminal block where needed and nothing is "split" or "spidered" out if that makes sense.

![missing image](https://lh3.googleusercontent.com/r4IO06cGsjjXGPfgar0MSOqbEodxS2_GRTNPWszgfLVVHPlUZvHYOmHxmPwyU8qYFa0PriUlURPi--0QPfVjguLpV_Hh3uHI_O4=s0)


---
**Don Kleinschnitz Jr.** *September 30, 2018 22:44*

**+Chuck Comito** lets break the above diagram down by harness type to try and make it simple.



I am not really worried about the AC side just now. Lets focus on the DC side of these supplies.



DC power supply:

Insure that <b>each</b> supply has <b>two</b> large as practical wires going from the supply to <b>each</b> of its loads. Example: If you have 3 loads on a supply there should be 6 wires coming from it.



Its hard to see in the picture what loads are connected to what supplies.


---
**Chuck Comito** *October 01, 2018 23:53*

**+Don Kleinschnitz Jr.** , I'll dig it out of the cabinet and post a picture and tag you when its posted. Hopefully I can get to it by tomorrow afternoon. Thanks so much!


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/d3RDueRx7PG) &mdash; content and formatting may not be reliable*
