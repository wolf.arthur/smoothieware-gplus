---
layout: post
title: "CNC Control Boards for Sale I'm currently cleaning up and getting rid of all the excess CNC parts and pieces, that I have purchased and accumulated over the years"
date: October 03, 2018 16:49
category: "General discussion"
author: "Rustin Guerin"
---
CNC Control Boards for Sale



I'm currently cleaning up and getting rid of all the excess CNC parts and pieces, that I have purchased and accumulated over the years. I have the following controller boards that are all either brand new or lightly used. Let me know if interested price includes shipping to US via USPS. I can send more pics if need be.

rgueri1@gmail.com





- Smoothieboard 5XC V1.0b - used but working fine $110

- xPRO-V2 - new in package $75

- xPRO-V3 - new in package $100

- PMDX-424 - (for Mach4) used but working fine $180







![missing image](https://lh3.googleusercontent.com/-vnc8GsWEjZI/W7TzN29bK3I/AAAAAAACksQ/Ze3oylklf2cXw1yqbvUTUWgpYzTGBI5sACJoC/s0/SMOOTHIEBOARD.jpg)
![missing image](https://lh3.googleusercontent.com/-Qu-IYchHEgM/W7TzN7TZ2pI/AAAAAAACksQ/Jc2ler2c5FsO3kIZj300jcCbRbOCjZ_QgCJoC/s0/XPRO%252BV2.jpg)
![missing image](https://lh3.googleusercontent.com/-sxE_kCz_ZbI/W7TzN3OhFiI/AAAAAAACksQ/qPQIc1U7RdUv0e-imOl_6I3Gflska81vACJoC/s0/XPRO%252BV3.jpg)
![missing image](https://lh3.googleusercontent.com/--jyZvAUF1eQ/W7TzNySITFI/AAAAAAACksQ/6qH4-YqEIag292x0o8o1tXC5r9NAxp6uACJoC/s0/PMDX-424.jpg)

**"Rustin Guerin"**

---


---
*Imported from [Google+](https://plus.google.com/112489114642705697365/posts/28ysWavuVzL) &mdash; content and formatting may not be reliable*
