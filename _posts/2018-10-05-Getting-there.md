---
layout: post
title: "Getting there ..."
date: October 05, 2018 08:34
category: "Development"
author: "Arthur Wolf"
---
Getting there ...

![missing image](https://lh3.googleusercontent.com/-2KWynA5VoVs/W7ciF-CMwqI/AAAAAAAAQm8/VY-W3vSO2D8NNKidDj9fWj_Hleg2DRQtQCJoC/s0/IMG_20181005_064946.jpg)



**"Arthur Wolf"**

---
---
**Douglas Pearless** *October 05, 2018 08:40*

Looking Good , can't wait to try one :-)


---
**Christian Lossendiere** *October 05, 2018 08:50*

Just a question, i don't see pin to connect external drivers, it's possible or no ?






---
**Arthur Wolf** *October 05, 2018 08:52*

See the 6 connectors at the top ? Those are gadgeteer ports, they'll let you add not only extenal drivers, but a crazy ton of other stuffs. [https://docs.google.com/document/d/144EbmhN6z-J2V_Zw7GfJpZrfD-B0dC3cuea9oWPgxNM/edit#](https://docs.google.com/document/d/144EbmhN6z-J2V_Zw7GfJpZrfD-B0dC3cuea9oWPgxNM/edit#)




---
**Jérémie Tarot** *October 05, 2018 09:09*

😍😍😍


---
**Jonathon Thrumble** *October 05, 2018 09:44*

Looking amazing! 👌


---
**Sébastien Mischler (skarab)** *October 05, 2018 14:19*

J'ai bien vérifié 3 fois, mais non elle pas dans ma boite aux lettres ^^


---
**Jérémie Tarot** *October 05, 2018 15:33*

After skimming again over the extension boards document, am I wrong to think we'll be able to use it with both servos and axis encoders? 


---
**James Rivera** *October 05, 2018 18:10*

LOL @ the Edison use case opening quote.


---
**Thomas T. Sørensen** *October 06, 2018 11:22*

Are the XT60 connectors for power? 


---
**Arthur Wolf** *October 06, 2018 11:23*

**+Jérémie Tarot** Using extension boards sure.

**+Thomas T. Sørensen** Yep !




---
**Jérémie Tarot** *October 06, 2018 11:43*

**+Arthur Wolf** great! Does firmware already supports feedback from encoders, both linear and rotary? 


---
**Marko Novak** *October 06, 2018 15:08*

What is expected maximum voltage on motor input? 


---
**Arthur Wolf** *October 06, 2018 16:41*

**+Jérémie Tarot** That's up to the drivers, not much to do with the firmware.

**+Marko Novak** 24v


---
**Jérémie Tarot** *October 06, 2018 17:28*

**+Arthur Wolf** I'm talking about double feedback loop: motor rotary encoder to driver for direction, and linear encoders on axes to controler for position


---
**Arthur Wolf** *October 06, 2018 17:29*

**+Jérémie Tarot** That's still up to the drivers, I don't know any that will do that but they do exist, this is just a very odd/rare setup.


---
**Jérémie Tarot** *October 06, 2018 17:44*

**+Arthur Wolf** Granite Devices drivers handle dual loop, and LinuxCNC supports encoders feedback. Allows to more easily higher accuracy on lower end structure and components


---
**Marko Novak** *October 06, 2018 17:44*

**+Arthur Wolf**  yes it's usually done on drivers, but I think it was thought more in line of industrial solution on the cheap ;)



Linear encoder for actual position and rotary for live... Linear is saved on power off, fw comparator for position (halt/force logic)...




---
**Antonio Hernández** *October 08, 2018 16:47*

So... kickstarter is ready ? date ?


---
**Arthur Wolf** *October 08, 2018 18:09*

**+Antonio Hernández** Boards ready at the factor, should ship to contributors in the coming days. Then still a few weeks before we start KS.


---
**Christian Lossendiere** *October 08, 2018 18:56*

This is V2 or V2 mini ? i don't remember the difference, and for the V2 pro, the project continue ? i don't listen news about V2 pro




---
**Antonio Hernández** *October 10, 2018 05:21*

**+Arthur Wolf** thanks for the info !.


---
**Arthur Wolf** *October 28, 2018 15:36*

**+Christian Lossendiere** It's the first proto of v2, it just ran it's first code recently. v2-pro will be in the same kickstarter as v2, but with a later release date.


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/GMfHdCZAFg2) &mdash; content and formatting may not be reliable*
