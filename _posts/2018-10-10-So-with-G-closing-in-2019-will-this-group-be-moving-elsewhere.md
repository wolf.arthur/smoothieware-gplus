---
layout: post
title: "So with G+ closing in 2019, will this group be moving elsewhere?"
date: October 10, 2018 02:05
category: "General discussion"
author: "Reverend Eric Ha"
---
So with G+ closing in 2019, will this group be moving elsewhere? I'd be willing to setup a Facebook group if there is any interest(assuming that hasn't already been done). I definitely don't want to lose access to a community like this. 





**"Reverend Eric Ha"**

---
---
**Petr Sedlacek** *October 10, 2018 08:21*

No, not fb, please... There's got to be something better. 


---
**Arthur Wolf** *October 10, 2018 09:03*

There has been a facebook group for years. It's linked to at the exact same places as G+, it's just people never choose to go there. See top menu at [smoothieware.org - start [Smoothieware]](http://smoothieware.org)

As G+ starts to disappear, I'll be posting here with links to the other, existing communities. 


---
**Hakan Evirgen** *October 10, 2018 18:31*

you have a Smoothieware page on Facebook but no group. A FB group would be better.


---
**John Cooper (choffee)** *October 12, 2018 16:13*

Some people seem to be moving to mastodon.social. Just set myself up there. @choffee 


---
**Reverend Eric Ha** *October 12, 2018 17:28*

I'm going to have to check out Mastodon, I have a couple other communities that I need to move somewhere and that might be perfect..




---
*Imported from [Google+](https://plus.google.com/+ReverendEricHa/posts/PVQiBYvTRij) &mdash; content and formatting may not be reliable*
