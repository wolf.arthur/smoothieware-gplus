---
layout: post
title: "I am liking what I am seeing with the new V2 smoothie protos"
date: October 14, 2018 02:32
category: "General discussion"
author: "Griffin Paquette"
---
I am liking what I am seeing with the new V2 smoothie protos. Trinamics really make it appealing. One thing that I have always wondered is if there will be a wifi connectivity feature built into any of the V2 boards? I know Luc from ESP3D has done a nice modified version of his ESP control interface for Panucatt. Anything like that might be making its way as an addon for V2? 





**"Griffin Paquette"**

---
---
**Arthur Wolf** *October 14, 2018 08:08*

We'll have plug-in extension boards for this and many other things.

We'll have <b>a lot</b> of extensions actually : [https://docs.google.com/document/d/144EbmhN6z-J2V_Zw7GfJpZrfD-B0dC3cuea9oWPgxNM/edit#](https://docs.google.com/document/d/144EbmhN6z-J2V_Zw7GfJpZrfD-B0dC3cuea9oWPgxNM/edit#)


---
**Griffin Paquette** *October 14, 2018 22:45*

That's awesome!



I like the idea of having so many expansion opportunities. Keeps users really open to upgrades. Keep us posted on these! Would love to do some hardware testing especially for Wifi


---
**Venelin Efremov** *October 15, 2018 06:11*

I struggle to understand the appeal of these primitive wifi options. With a Pi Zero, you get wifi + camera interface and octopi is very capable host. All with a very affordable price.


---
**Griffin Paquette** *October 15, 2018 12:38*

**+Venelin Efremov** I too think Octoprint is a wonderful option. I just think that the Duet style implementation is quite excellent as well as it allows an all in one solution that is also tailored to the host MCU.


---
**Jeff DeMaagd** *October 17, 2018 10:43*

Has the zero support in Octoprint gotten better? I’ve heard in the past it was hit or miss. I really like the Duet Web Control style of an interface. It’s really powerful.


---
*Imported from [Google+](https://plus.google.com/111302122377301084540/posts/JZZ6pMzGJSD) &mdash; content and formatting may not be reliable*
