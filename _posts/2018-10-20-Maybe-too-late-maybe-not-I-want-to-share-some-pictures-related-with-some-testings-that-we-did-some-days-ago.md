---
layout: post
title: "Maybe too late... maybe not... I want to share some pictures related with some testings that we did some days ago..."
date: October 20, 2018 08:29
category: "General discussion"
author: "Antonio Hern\u00e1ndez"
---
Maybe too late... maybe not... 



I want to share some pictures related with some testings that we did some days ago... 



The pictures are related with 3, 4 and 5 axis (4th & 5th are rotatory).

Thanks a lot for **+Arthur Wolf**, +Wolfmanjm, smoothiefans and all people related with this fantastic project. It was long time ago when I did the first question related with external drivers. We were testing the board a long period but we did not have the machine on our hands to connect it and see more action... it happened for a long long period... until some days ago...



The results were amazing.... we were (also nowadays) completely newbies with these topics related with numeric control and, now, we are surprised.... really really surprised... the results are fantastic...



This would not be possible if passionate and dreamer people didn't want to share their knowledge...... reading all of your comments (support, developer, google +, irc, cnczone and other social networks related with smoothieboard) is how it was possible that a completely pair of newbies on these topics could accomplish their goal...



Thanks a lot to all the people who belong to this fantastic community !



![missing image](https://lh3.googleusercontent.com/-cgUwP92e4EM/W8rnY5CTaQI/AAAAAAAAAVM/9jbEaLw3RUE99kwhuPbLRnMzkNC6DZvNwCJoC/s0/PANEL_CNC_2_4.JPG)
![missing image](https://lh3.googleusercontent.com/-7kKszK0eegQ/W8rnY4a5hvI/AAAAAAAAAVM/Kavm19Xq-Z4zE4Y-kYMbVE8oyLvTHnspQCJoC/s0/PANEL_CNC_3_4.JPG)
![missing image](https://lh3.googleusercontent.com/-Z9_OiLxyDTk/W8rnY9k84dI/AAAAAAAAAVM/JpNkp5RAl6oJ4nKoM4f8P2S7WSkiJX3LACJoC/s0/PANEL_CNC_4_4.JPG)
![missing image](https://lh3.googleusercontent.com/-fpG553KF0Ro/W8rnY47XiuI/AAAAAAAAAVM/x9ZGyBK-Rlk5nldhd-xpj8Xsw7sF5M7xQCJoC/s0/PANEL_CNC_1_4.JPG)
![missing image](https://lh3.googleusercontent.com/-OJQI7PeS1os/W8rnY3RFpAI/AAAAAAAAAVM/_Lj545Uoq2g8lEBiEaFRKLfLm4zRI7w3wCJoC/s0/CNC_5AXIS_TEST_3_3.JPG)
![missing image](https://lh3.googleusercontent.com/-YKsnKE1fg5g/W8rnY_fEaRI/AAAAAAAAAVM/4S40X-GsE-UHNo6n-aB0L9kFDN1rXTlMACJoC/s0/CNC_5_AXIS_1_2.JPG)
![missing image](https://lh3.googleusercontent.com/-1_2dd3eDwyU/W8rnYwMQrUI/AAAAAAAAAVM/WMXQBg4ZIuEuPJkq8inEcLAtyAso4EYGQCJoC/s0/4_AXIS_SCORPION_5_5.JPG)

**"Antonio Hern\u00e1ndez"**

---
---
**Thomas Arthofer** *October 20, 2018 15:35*

Looks good, but why is it not inside a dustproof box?




---
**Antonio Hernández** *October 20, 2018 16:01*

It's missing. Not only that. Control panel must be in inside something similar. Tests were enough to discover what kind of improvements must be done. 🖒


---
**Josh Rhodes** *October 20, 2018 18:00*

That's a really great build.

Seriously impressed!


---
**Antonio Hernández** *October 20, 2018 18:32*

Thanks ! . More improvements must be done !. 


---
**j.r. Ewing** *October 21, 2018 08:09*

 How many blocks per minute 


---
**Antonio Hernández** *October 21, 2018 16:32*

It depends. For snowflake there were 6 layers, each layer was 0.5mm. Each layer could be 10 or 15 minutes.... the machine could move faster than this example, but it depends what kind of material is going to be used, tool type, tool rpm's... If you're talking about gcode lines, we did not know (we didn't put attention to that). Next time we will put attention to cncjs gcode processing window (gcode line counter).


---
**j.r. Ewing** *October 21, 2018 21:26*

Yes raw blocks per min/ sec.  the mori seki  I program at work can about a 1000 doing really small stitching moves  using inverse time on a horizontal. but your code has to be smooth And look ahead has to be setup correctly.  Real world if you get above 500-700 per sec your doing good. 




---
**Antonio Hernández** *October 21, 2018 21:29*

Ahhh I see... next testings we will observe with more attention blocks per minute. Thanks for the info ! 


---
**Antonio Hernández** *October 24, 2018 04:36*

Finally, we believe making videos that reflect the capacity of smoothieboard is the way in which we can support and spread its reach. This was our first effort to talk about smoothieboard... the channel is new and unknown but, we hope this could help to the project just a little bit.... thanks a lot !

[youtube.com - CNC Amateur](https://www.youtube.com/channel/UCK0UDthfSCxRIwt_95KvTlA)


---
*Imported from [Google+](https://plus.google.com/107343842763021715447/posts/hwhndUVAAtV) &mdash; content and formatting may not be reliable*
