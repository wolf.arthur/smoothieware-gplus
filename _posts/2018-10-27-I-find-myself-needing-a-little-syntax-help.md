---
layout: post
title: "I find myself needing a little syntax help"
date: October 27, 2018 18:09
category: "Help"
author: "Jeremy Wasserstrass"
---
I find myself needing a little syntax help. I am using a Viki II display with an Azteeg X5 GT with bigfoot TMC drivers and I can not get the extruder to drive. I am sure I have something screwed up in my extruder setup,. I would appreciate it if someone could tell me what is wrong with the code below(or maybe how little I got right ;) ). Hmm and I see it did not retain my steps/mm for some reason.



# Extruder module configuration

extruder.hotend.enable                          true             # Whether to activate the extruder module at all. All configuration is ignored if false

extruder.hotend.steps_per_mm                    140              # Steps per mm for extruder stepper

extruder.hotend.default_feed_rate               600              # Default rate ( mm/minute ) for moves where only the extruder moves

extruder.hotend.acceleration                    1000             # Acceleration for the stepper motor mm/sec²

extruder.hotend.max_speed                       100              # mm/s



extruder.hotend.step_pin                        2.0              # Pin for extruder step signal

extruder.hotend.dir_pin                         0.5             # Pin for extruder dir signal

extruder.hotend.en_pin                          0.4             # Pin for extruder enable signal

extruder.hotend.en_current                      1.5             # Extruder 1 stepper current.

extruder.hotend.en_max_rate                  30000.0            # mm/min actuator max speed



motor_driver_control.delta.enable           true              # delta (E1) is a TMC26X

motor_driver_control.delta.designator       E1                # E1 to set the settings

motor_driver_control.delta.chip             TMC2660           # chip name

motor_driver_control.delta.current          1500              # current in milliamps

motor_driver_control.delta.max_current      3000              # max current in milliamps

motor_driver_control.delta.microsteps       16                # microsteps 256 max

motor_driver_control.delta.alarm            true              # set to true means the error bits are checked

motor_driver_control.delta.halt_on_alarm    false             # if set to true means ON_HALT is entered on any error bits being set

motor_driver_control.delta.spi_channel       0                # SPI channel 1 is sdcard channel

motor_driver_control.delta.spi_cs_pin        0.4              # SPI CS pin

#motor_driver_control.delta.spi_frequency     100000          # SPI frequency

motor_driver_control.delta.sense_resistor     100             # set the sense resistor used. 





**"Jeremy Wasserstrass"**

---
---
**Wolfmanjm** *October 28, 2018 17:56*

E1 is NOT a valid designator, for the first extruder it would be A

If you had a uart hooked up when it booted you would see the appropriate error printed out.And designator is a deprecated config it is now axis so you need...

motor_driver_control.delta.axis      A

     


---
**Wolfmanjm** *October 28, 2018 17:59*

[smoothieware.org - advancedmotordriver [Smoothieware]](http://smoothieware.org/advancedmotordriver) please read the documentation




---
**Eric Lien** *October 29, 2018 03:40*

You can look at one of the HercuLien/Eustathios groups printer configurations here. 



[https://github.com/eclsnowman/HercuLien/tree/master/Azteeg%20X5%20GT%20Smoothieware/Bigfoot%20BSD2660%20interpolation](https://github.com/eclsnowman/HercuLien/tree/master/Azteeg%20X5%20GT%20Smoothieware/Bigfoot%20BSD2660%20interpolation)


---
*Imported from [Google+](https://plus.google.com/104298484682589560474/posts/fFuP586Dxcm) &mdash; content and formatting may not be reliable*
