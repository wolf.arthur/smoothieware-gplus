---
layout: post
title: "Guys, I think this might be a config problem so I'm bringing it here first"
date: November 11, 2018 19:09
category: "Help"
author: "Chuck Comito"
---
Guys, I think this might be a config problem so I'm bringing it here first. Occasionally I'll get this sort of skip on certain shapes and I'm not sure how to fix it. Notice in the letter A how its got breaks in it. Is this something I can adjust in the config file to help with it?



Thanks!

![missing image](https://lh3.googleusercontent.com/-AEhdfpubayU/W-h-YV1QOQI/AAAAAAAAMe4/NNDt888UP10N-yEUMjgTeCYkKf9_5Yg5wCJoC/s0/20181111_131553.jpg)



**"Chuck Comito"**

---
---
**Arthur Wolf** *November 11, 2018 20:00*

I don't think anything in config could cause this. I'd look in the direction of a false contact/interference in the laser control line. Ferrite ?


---
**Chuck Comito** *November 11, 2018 20:34*

Interesting take **+Arthur Wolf**. I'll look into that. Currently there isn't any ferrite core. I can add it if you think it would help? Would I simply slip a ferrite core around the laser control line and test or is there more to it than that? Thanks!


---
**Chuck Comito** *November 11, 2018 21:25*

**+Arthur Wolf** , do you think the #laser_module_pwm_period has anything to do with it? 


---
**Arthur Wolf** *November 12, 2018 08:25*

Don't think so




---
**Don Kleinschnitz Jr.** *December 11, 2018 16:30*

I would tend to believe that the LPS arc'd at this point and lost power. The Open Drain configuration is pretty noise adverse in my experience. I bevel as the HVT starts to fail it randomly arcs. I am trying to prove this with my machine which from time to time I hear a crackle.  Changed your coolant lately :)!


---
**Chuck Comito** *December 11, 2018 18:30*

Hey **+Don Kleinschnitz Jr.** , I completely forgot about this post and started a new thread yesterday. The lps a d tube are fairly new (after market bought a couple months ago). Also, this is something I e been struggling with ever since I could remember so I also dont think it's a coolant issue but that too is also only a couple months old. 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/VTzPtDB9TKF) &mdash; content and formatting may not be reliable*
