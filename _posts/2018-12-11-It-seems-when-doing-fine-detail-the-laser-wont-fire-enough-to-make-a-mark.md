---
layout: post
title: "It seems when doing fine detail the laser won't fire enough to make a mark"
date: December 11, 2018 00:06
category: "Help"
author: "Chuck Comito"
---
It seems when doing fine detail the laser won't fire enough to make a mark. I think this has something to do with the time it takes the laser to turn on and off and it misses some sections. Is there something in the firmware to tweak to help with this?





**"Chuck Comito"**

---
---
**Douglas Pearless** *December 11, 2018 00:53*

What speed is the movement as it is possible for the internal movement control of Smoothie to be completing the blocks (a G0/G1 is chopped into little blocks of movement) so it is still moving when the next M/G code is presented and executed; try slowing the movement down and see if that helps.

Alternatively you could try to insert a M400 before the laser firing per [http://smoothieware.org/supported-g-codes](http://smoothieware.org/supported-g-codes) 


---
**Chuck Comito** *December 11, 2018 01:01*

Thanks **+Douglas Pearless** , but the movement is pretty slow. I'm running at 30mm/sec. It only does this in fine detail areas. I've seen people get great results at much higher speeds and much more detail. Im stumped. 


---
**Douglas Pearless** *December 11, 2018 01:03*

OK, as a test, can you edit the GCODE and add a M400 before each laser firing to force Smoothie to wait until the movement is finished and then execute the line of code after the M400?

Also, how is the laser wired into Smoothie?


---
**Chuck Comito** *December 11, 2018 01:08*

The laser is fired via pin 2.5 as illustrated on the smoothieware site. I can add the M400. For the test, I literally add it to each line of gcode? This sounds like that'd be difficult to do? Is there a quick way to do this?


---
**Douglas Pearless** *December 11, 2018 01:11*

Can you post a few lines of g-code and what tool are you using to generate the code?




---
**Chuck Comito** *December 11, 2018 01:24*

Here it is:



; Smoothie device profile

G00 G17 G40 G21 G54

G90

M3

; Scan+Cut @ 35 mm/sec, 12% power

M106

G0X222.66Y25.8

G1X223.11S0.12F2100

G0X223.51Y25.9

G1X222.35

G0X222.18Y26

G1X223.74

G0X223.94Y26.1

G1X223.23

G0X222.6

G1X222.06

G0X221.96Y26.2

G1X222.41

G0X223.44

G1X224.1

G0X224.24Y26.3

G1X223.61

G0X222.32

G1X221.89

G0X221.83Y26.4

G1X222.23

G0X223.76

G1X224.36

G0X226.56

G1X227.09

G0X229.93

G1X230.11

G0X231.61

G1X232.04

G0X232.38Y26.6

G1X231.98

G0X231.89

G1X231.45

G0X230.31

G1X229.81

G0X229.06

G1X228.64

G0X228.56

G1X228.18

G0X227.41

G1X227.08

G0X226.61

G1X226.32

G0X224.58

G1X223.99

G0X222.13

G1X221.76

G0X221.73Y26.7

G1X222.09

G0X224.09

G1X224.68

G0X226.27

G1X226.59

G0X227.24

G1X227.52

G0X228.12

G1X228.44





The file is huge though..




---
**Douglas Pearless** *December 11, 2018 01:31*

ok, if my brain is working, the G0 is the "move to" and the G1 is the "move while doing something, in this case, the laser is turned on.  Using a suitable editor try substituting "G1" for "M400<cr>G0" where <cr> is the carriage return character so effectively you insert a M400 line before every G1 line. 


---
**Chuck Comito** *December 11, 2018 01:34*

like so?



; Smoothie device profile

G00 M400 G0 7 G40 G21 G54

G90

M3

; Scan+Cut @ 35 mm/sec, 12% power

M106

G0X222.66Y25.8

M400 G0 X223.11S0.12F2100

G0X223.51Y25.9

M400 G0 X222.35

G0X222.18Y26

M400 G0 X223.74

G0X223.94Y26.1

M400 G0 X223.23

G0X222.6

M400 G0 X222.06

G0X221.96Y26.2

M400 G0 X222.41

G0X223.44

M400 G0 X224.1

G0X224.24Y26.3

M400 G0 X223.61

G0X222.32

M400 G0 X221.89

G0X221.83Y26.4

M400 G0 X222.23

G0X223.76

M400 G0 X224.36

G0X226.56

M400 G0 X227.09

G0X229.93

M400 G0 X230.11

G0X231.61

M400 G0 X232.04

G0X232.38Y26.6

M400 G0 X231.98

G0X231.89

M400 G0 X231.45

G0X230.31

M400 G0 X229.81

G0X229.06

M400 G0 X228.64


---
**Douglas Pearless** *December 11, 2018 01:35*

that could work, or you may have to change:

M400 G0 X223.11S0.12F2100

to

M400

G0 X223.11S0.12F2100




---
**Chuck Comito** *December 11, 2018 01:38*

Gotcha, now I see what you mean by carriage return... lol. Ok, I'll give it a shot on a very small file. Thanks for the input. I still dont understand how this will help though. What an i trying to work around if you dont mind explaining it to me?


---
**Douglas Pearless** *December 11, 2018 01:50*

I think that you accidentally turned all the G1 into G0:

You had:



G1X223.11S0.12F2100



anti it became:



M400

G0 X223.11S0.12F2100



but should be:



M400

G1 X223.11S0.12F2100



The M400 effectively stops Smoothie (and Marlin for that matter) until all movement has completed, and then executes the next line of code, so in your example:



M106 ; turn on the fan

G0X222.66Y25.8 ; move somewhere

M400 ; wait until the movement has finished

G1 X223.11S0.12F2100 ; move somewhere with the laser on

;you may need another M400 here but hopefully not

G0X223.51Y25.9 ; move with the laser off

M400 ; wait until the move has finished

G1 X222.35 ; move somewhere with the laser on

 ;etc


---
**Douglas Pearless** *December 11, 2018 01:53*

as noted above, you <b>may</b> also nee a M400 after each G1; but you will need to experiment to test this :-)




---
**Wolfmanjm** *December 11, 2018 09:45*

i think the M400 is totally a bogus thing to try. it has nothing to do with the issue I am afraid. how is your PWM connected, and what type of laser. what frequency is your PWM set to?




---
**Don Kleinschnitz Jr.** *December 11, 2018 16:51*

**+Chuck Comito** what are you seeing that makes you think the dots are missing. Do you have a photo and can you tell us the speeds. 



<b>Interesting:</b> the initial calculations that I did suggested that the fire time of these lasers is much longer than the dot times many jobs are expecting. 



It was purely a math exercise as I have never figured out how to measure this because I have not found a good sensor to see the lasers CO2 output. 



I also have not confirmed the actual rise and fall times of the light output. I found a couple of old references to CO2 laser response times. They were pretty long but not sure what to believe. 



I can logic that to ionize the laser must be slow in electronic terms. So when turning on-off a single dot from de-ionized to ionized I can imagine it being longer than a dot time. So this may not be visible in cases where the laser is ionized and is simply turning on vs cases where the laser must turn full on an off. 



These calculations are what caused me to conclude that the PWM period needed to be set to 200. Otherwise the pulse widths of the PWM at lower power levels were under what the laser could resolve. 



I am working toward a response test where we mark vertical bands that have increasing # of dots on-off i.e. 1 on 1 off, 2 on 2 off, 3 on 3 off....etc. This may show us the real system response.

To many things on my plate....



You may also eliminate this effect by going painfully slow. Unfortunately other laser settings start to override your ability to control a single dot ...


---
**Wolfmanjm** *December 11, 2018 17:23*

There is also the complexity that the power is set proportional to the trapezoidal slope of acceleration, so if it never accelerates to fullspeed the power will never reach full power. (The M400 suggestion should actually make this worse as it will never be allowed to get enough gcode in the planner queue to calculate a decent acceleration slope).

I've been thinking of adding an option to turn off the proportional acceleration power setting which may help in certain circumstances.




---
**Chuck Comito** *December 11, 2018 18:36*

**+Don Kleinschnitz Jr.** and **+Wolfmanjm** , these are both great responses. My frequency is currently set to 200 (I believe  but will confirm when I get home). Would changing the acceleration  help? Or perhaps changing the minimum power from say 0 to some number that is above the minimum laser power on threshold?


---
**Don Kleinschnitz Jr.** *December 12, 2018 00:42*

**+Wolfmanjm** does this mean that if the power was set to 10% the PWM signal ramps up to 10% during the acceleration of the move?



If the time to accelerate is longer than the the marking period (like a dot time) the laser may not have time to get to its programmed power?


---
**Chuck Comito** *December 12, 2018 01:14*

**+Don Kleinschnitz Jr.** This plausible playing with a combination of acceleration and pwm frequency might solve it. 


---
**Don Kleinschnitz Jr.** *December 12, 2018 03:54*

**+Chuck Comito** do you have a photo that shows to the phenomena you are seeing?


---
**Chuck Comito** *December 12, 2018 03:57*

This is the best picture I have right now but notice the letter A in this example. This is the sort of thing that happens all the time. Of course this being a letter I can vector engrave it to fix it bit this method doesnt work on everything. ![missing image](https://lh3.googleusercontent.com/eDYwHrWBsi7Rb5bK1oRqhb7tfRvrhq3o2ZDLLhY74FmjfpHbsAcFv8MBFJp2r1q1uzy-IBBhXQAgGKNMvUTmx8jKbmWGC7wamMw=s0)


---
**Don Kleinschnitz Jr.** *December 12, 2018 04:04*

Are you referring to the skip in the stroke. If so this is not the fine detail problem that you referred to is it?


---
**Chuck Comito** *December 12, 2018 04:21*

I am referring to that but it's the only example I have right now. I'll post more pictures tomorrow that better illustrate it. 


---
**Wolfmanjm** *December 12, 2018 10:25*

**+Don Kleinschnitz Jr.** Yes that is exactly what it does, there is a trapezoidal curve for acceleration and deceleration, and the laser power is set proportional to that curve. If there are not enough segments to allow it to reach full acceleration then the power will not reach full power. This was supposed to be an improvement so it didn't burn the edges when it slows down.


---
**Don Kleinschnitz Jr.** *December 12, 2018 16:52*

**+Wolfmanjm** so what happens if you move to a location and print just one dot :).


---
**Don Kleinschnitz Jr.** *December 12, 2018 16:53*

**+Chuck Comito** Ok looking forward to more pics, because I think that skip in the image is not due to the response discussion we are having.


---
**Wolfmanjm** *December 12, 2018 16:56*

**+Don Kleinschnitz Jr.** Presuming it was a longish move it would be at full power as moves are consolidated in the planning buffer and the ramps are calculated against those consolidated moves. The problem would be if there were a lot of very short moves such that it can never reach full speed.




---
**Chuck Comito** *December 12, 2018 18:02*

**+Wolfmanjm** , this may be the case here as the gantry was moving left and right of the vertical line in the A. They were very short moves and might be smaller than the acceleration/power can turn on?


---
**Wolfmanjm** *December 12, 2018 18:34*

you can increase acceleration and see if it helps at all, but in my experience the buffer is usually big enough to handle even short moves and when scanning is usually has planned far enough ahead to be at full speed. Also try playing the gcode from sdcard see if it helps, some hosts are too slow to stream short segments fast enough to keep the buffer full.




---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/7xqdDojEmjR) &mdash; content and formatting may not be reliable*
