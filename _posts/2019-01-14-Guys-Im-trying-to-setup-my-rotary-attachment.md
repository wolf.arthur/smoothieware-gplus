---
layout: post
title: "Guys, I'm trying to setup my rotary attachment"
date: January 14, 2019 17:48
category: "Help"
author: "Chuck Comito"
---
Guys, I'm trying to setup my rotary attachment. I've got it moving but I need help with rotary attachment steps per mm. How would I go about calculating this and putting the step per mm into the config file in the extruder.hotend.steps_per_mm section? My motor is a nema 17, 1.8 degree 200 steps per rev with a 39mm diameter drive wheel.  Thanks for any help.





**"Chuck Comito"**

---
---
**Arthur Wolf** *January 14, 2019 21:27*

Well, for rotary axes, it's steps per degrees not steps per millimeters, and you go about calculating it the same as for millimeters, either do the math ( there are 360 degrees in a rotation ), or do the "try and rule of three" method.

If you want to do it in millimeters ( because you'll always be using objects of the same diameter in the rotary axis ? ) then find the steps per degrees, then find how many degrees are in a millimeter at that diameter and factor it in.


---
**Chuck Comito** *January 14, 2019 22:28*

**+Arthur Wolf**, i have done this but still have trouble. My motor is 1.8 degrees, 200 steps per rev. My drive wheel is 39.5mm diameter so at the diameter the circumference is 124.09mm. Steps per mm at that diameter should be 1.611 steps to equal 1 degree? Then I have to account for 1/16 microstepping? Which would equal 25.787? I feel like this is way off. I'm currently using lightburn which askes for my roller diameter and the the object diameter so I believe it is doing the math after that. Is there a way in the config file to enter the degrees per rotation instead of steps per mm?


---
**Arthur Wolf** *January 14, 2019 22:42*

**+Chuck Comito** I would just test it with the value you found so far, then if it's off, ask it to move 100mm, measure how much it actually moved, and you can ajdust just by applying the ratio.


---
**Antonio Hernández** *January 15, 2019 01:40*

I hope this could help in some way...



Microsteps per degree = (motor steps * microsteps per step * gear's teeth) / 360 degrees



In my case: (200 * 25 * 36) / 360 = 500

It means, 1 degree = 500 microsteps = 20 steps)



My rotatory attachment (the gear) has 36 teeth for each full rotation.


---
**Chuck Comito** *January 15, 2019 01:48*

**+Antonio Hernández** Should I be using the cnc build for this with the A axis defined? I'm trying to set it up using the first extruder motor. So mine should be  200 * 1/16 microstep * what? I don't  have gear teeth. It's direct drive.


---
**Antonio Hernández** *January 15, 2019 01:56*

My rotatory attachment (inside) has a gear with 36 teeth. Maybe the datasheet about your rotatory device has this number (or maybe it could help if you open your device and count the gear's teeth... it's not an stylish solution, but, it could help). Yes, you have to compile the firmware using 4 axes option. No, extruder section must be commented.


---
**Chuck Comito** *January 15, 2019 01:58*

**+Antonio Hernández** anita a home built rotary attachment. But I might be having issues because I'm not using the right firmware. ![missing image](https://lh3.googleusercontent.com/-UOa4Ktya-RyxXtID7zh6FZc5zW_EPPDwxj-nVbfCJhjNCMNyl3RFUSM_GsjgDzesuMP4Fs1M9DSklfl86tRZv7RkrMudrdRl6g=s0)


---
**Antonio Hernández** *January 15, 2019 02:00*

For your rotatory attachment, maybe "ratio relation" concept could help ?. For example, it's a number something like this: 36:1.


---
**Chuck Comito** *January 15, 2019 02:01*

I'm going to try to compile smoothie for a 4th axis and see if that helps.


---
**Antonio Hernández** *January 15, 2019 02:09*

You have to change the firmware and update the config file definitions. Some smoothie users have used the extruder config as rotatory element but to be honest I didn't use that way. It's not possible to see with details the blue section => It appears to have some belts connected with gears ?.


---
**Chuck Comito** *January 15, 2019 02:14*

I changed the firmware and have updated my config file. I'm now running a 4th axis build with the ABC definitions plugged in. Still no change in behavior. The things that look like belts are really thick o-rings. I have an aluminum wheel machined that fits directly on the stepper motor shaft. The OD of the wheels are 39.5mm.




---
**Antonio Hernández** *January 15, 2019 02:21*

Try to include A definition and comment extruders section. In my case that is the scene. Try to do the maths as Arthur suggested. Uhm..., maybe the maths without ratio parameter could help ( (200 * 25 ) / 360 ). Test and see what happens. It works, just be patient.


---
**Chuck Comito** *January 15, 2019 02:33*

**+Antonio Hernández** , when you and **+Arthur Wolf** say "do the math" are you referring to a math function in the config file? I've done the math in an excel spreadsheet but the values are not working and are not what I'd expect. Or I should say that they are what I expect as the results are correct but perhaps I'm not inputting the right values to begin with. That's the part that is frustrating. 


---
**Antonio Hernández** *January 15, 2019 02:54*

Uhm, not. It's simple. The value that you calculated must be assigned to "delta_steps_per_mm" and that parameter will mean microsteps per degree (for A rotary axis). I will take the example mentioned above: 



In my case: (200 * 25 * 36) / 360 = 500

It means, 1 degree = 500 microsteps = 20 steps)



If 20 steps are required to achieve 1 degree, 200 steps (full stepper motor rotation) are equal to 10 degrees. In that scene, 45 degrees will be equal to 4.5 full stepper motor rotations. 



You can do a simple test, request with G0 something like this... "GO A45"... and, you must see 4.5 full stepper motor rotations.



The formula must be changed (in your case) because your device is direct and it's not present a gear or something like that in your rotatory attachment. Try the same formula without ratio parameter (36 in my case) and test the motion of your stepper motor. If the full rotations complains with what you expect, connect your rotatory attachment and go to the next step...



25 microsteps per step is what I have used on my external stepper drivers. If you are using smoothieboard stepper drivers, that value must be other. (I don't know how many microsteps per step are assigned in config file).


---
**Chuck Comito** *January 15, 2019 02:58*

**+Antonio Hernández** thank you so much for all your help today. I will start fresh tomorrow with clear head using your suggestions. 


---
**Antonio Hernández** *January 15, 2019 03:07*

Don't worry. Just be patient. This section must be included in config file (example from my config file).



# Rotatory A axis 



delta_steps_per_mm     500



delta_step_pin     2.3o



delta_dir_pin     0.22o!



delta_en_pin     nc



delta_current     0



delta_max_rate     200.0



delta_acceleration     200.0



Firmware must be compiled to use A rotatory axis. And finally, the calculation that you made must be assigned to "delta_steps_per_mm" parameter. This value will contain the microsteps required to achieve 1 degree...



If you have endstops defined, their format must be changed in order to work with rotatory axis firmware...


---
**Chuck Comito** *January 15, 2019 19:02*

Ok here's what I did. My roller diameter is 39.5mm. So, 39.5 x pi = 124.0929. 200 / 124.0929 = 1.6116. Now I divide 1/1.6116 = .6204 x 16 for microstepping which = 25.789713 steps per mm in the config file. But it's still really far off. I also tried G0 A360 and it traveled about 30 degrees past a full rotation.


---
**Antonio Hernández** *January 15, 2019 20:39*

In this case, to be honest, this scene is unknown for me, but, I found some info related with how it is possible to translate degrees to mm (it sounds weird but, it's possible... ¿ ?). I don't know if this could help you in some way... and obviously I don't know if this calculation is correct, but, it appears to be useful:



radians = ("x" degrees * 3.1416) / 180.

distance (mm) = radius * radians



In your case, radius will be (39.5 / 2).


---
**Chuck Comito** *January 18, 2019 03:47*

**+Arthur Wolf** **+Antonio Hernández** I have a solution. I will post the full results tomorrow but it works!! Thank you both for the help!


---
**Antonio Hernández** *January 18, 2019 06:43*

Ok ! 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/Jxaza797t7a) &mdash; content and formatting may not be reliable*
