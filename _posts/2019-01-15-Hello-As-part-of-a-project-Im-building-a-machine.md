---
layout: post
title: "Hello, As part of a project I'm building a machine"
date: January 15, 2019 12:46
category: "General discussion"
author: "Athul S Nair"
---
Hello, 

As part of a project I'm building a machine. Currently it runs on Marlin FW. What I want is a firmware feature that would make a pin HIGH and stau at that state for a given duration.



 My purpose is, 

                  I have made a machine, which runs on Marlin. What it does is, It has an impression making tool, that would make an impression on a powder, then another tool would come over this impression and dispense some liquid to it. I have a custom GUI to move machine to given coordinates, make impression, position Tool 2 over the impression. For dispensing liquid I'm thinking about buying diaphragm valve. I've downloaded user manual for it. As per documentation, when compressed air is given to valve it will lift diaphragm and will start dispensing. Amount of liquid dispensed depends on the how long air is given. So I'm thinking about using a relay to turn a solenoid valve ON and OFF to supply Air. To turn relay ON I need to make any GPIO pin HIGH and I can control the amount of liquid being dispensed by controlling the duration of this pulse. Is there any way I can make smoothiware do this.



[https://www.dosieren.de/en/products/dispensing-valves/diaphragm/dv-5625-t/diaphragm-valve-dv-5625-t](https://www.dosieren.de/en/products/dispensing-valves/diaphragm/dv-5625-t/diaphragm-valve-dv-5625-t)





**"Athul S Nair"**

---
---
**Wolfmanjm** *January 15, 2019 15:05*

Yes you would define a switch to turn the valve on and off then use G4 Pxxx to delay and determine how long it is on for.

Look at the switch documentation on the wiki, and gcodes for G4.


---
**Athul S Nair** *January 16, 2019 05:43*

**+Wolfmanjm** This is piece of gcode that needs to be executed. I don't know whether Smoothieware supports all gcodes of Marlin, and I have added Switch custom gcode (Based on Switch documentation of Smoothie).  Are these gcodes supported? or alternate gcode for similar purpose available?



Also the Waiting time. Is it based on Timer/Counter of the microcontroller?Because accuracy is important

![missing image](https://lh3.googleusercontent.com/mZuNNYbUO0rXNN7UrDmXRfD6aCJ7ehLlnvBAppcY8U5TcgDN48YlHEupApF6pts9S1QkWJ4U-Y7_QdRfjqNW5dB1JTt47Ci7MNw=s0)


---
**Wolfmanjm** *January 16, 2019 11:38*

gcode is gcode, it is a standard. what you have looks ok, but the switch m code needs to be different for on and off.


---
**Arthur Wolf** *January 16, 2019 12:44*

[http://smoothieware.org/supported-g-codes](http://smoothieware.org/supported-g-codes)


---
**Athul S Nair** *January 17, 2019 08:49*

**+Wolfmanjm** I have few more doubts

1. How accurate is the delay set using G4 Pxxx?

2. I know smoothie is not designed for this purpose, but when I was looking for dispensing valve with controllers, there are few out in the market which has a controller. User can set duration int hat controller. When it receives a pulse, it would start dispensing and when finishes returns a signal. Is it possible for Smoothie to sent a pulse to start dispensing and  wait there till return pulse is received and move to next position repeat this


---
**Wolfmanjm** *January 17, 2019 09:53*

not unless you write a module to do that. G4 is accurate.




---
*Imported from [Google+](https://plus.google.com/114978037514571097180/posts/i6HeRANujAH) &mdash; content and formatting may not be reliable*
