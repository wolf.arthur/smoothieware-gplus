---
layout: post
title: "So in wondering if this is still an issue with windows as the discussion is 2 years old now"
date: January 18, 2019 03:17
category: "General discussion"
author: "Chuck Comito"
---
So in wondering if this is still an issue with windows as the discussion is 2 years old now. [https://github.com/Smoothieware/Smoothieware/issues/1096](https://github.com/Smoothieware/Smoothieware/issues/1096)

If so, is a linux box still the best solution for fast rasters with a laser?





**"Chuck Comito"**

---
---
**Arthur Wolf** *January 18, 2019 07:18*

I haven't heard about this in a long while. My understanding is this was a limitation in how LW4 ( specifically ) interfaced their coms, which other hosts ( Octoprint, Pronterface etc ) doesn't have. Don't know if LW4 made a fix there ( I think it was quite a bit of work for them to change the way they do things ), maybe ask them too ?



**+Wolfmanjm** ?


---
**Chuck Comito** *January 18, 2019 12:17*

**+Arthur Wolf** I'm not so sure this is host specific to lw.  But I'm no expert and am still figuring stuff out. I notice in lightburn that i have raster speed limitations and read a post that smoothie was somehow limited to 80-100mm/sec when I came to rastering. Which is what led me to the discussion I referenced. 


---
**Arthur Wolf** *January 18, 2019 12:39*

**+Chuck Comito** Smoothe definitely has limitations, actual speed also depends on the image resolution/density. But this specific issue was about communication between LW and Smoothie, which isn't ( or at least wasn't at the time ) happening at the best possible speed, and was the bottleneck in that situation.


---
**LightBurn Software** *January 18, 2019 22:39*

Smoothieware has a speed limit of about 800 to 1000 GCodes per second. Faster than that and it starts to miss steps and skew.  You can reduce the GCode per second count by lowering the speed, reducing the DPI, or using the Newsprint dither mode in LightBurn, which clusters the commands together, allowing Smoothie to go faster.


---
**LightBurn Software** *January 18, 2019 22:52*

The 80/100 mm/sec limit come from the default DPI setting. If you use an interval of 0.1mm (254 DPI) you get a maximum of 10 dots per MM, each of which creates a GCode command. At 80 mm/sec, that’s 800 commands per second. This is what is meant by “GCode density”.


---
**Arthur Wolf** *January 18, 2019 23:44*

**+LightBurn Software** : 



> Faster than that and it starts to miss steps and skew. 



That's not what's reported by users, and it makes no sense this would happen ( at least as a general rule ) considering how the software is made.

Going too fast for Smoothie's gcode parsing rate would result in loosing speeds, possibly in a relatively untidy manner, but always staying within the defined ( configuration ) parameters for acceleration and junction deviation ( meaning if they are set right and the machine is well built, no missed steps ). You should see rattling and erratic speeds, but you shouldn't see missed steps are skew. 

If you do see these, then this is likely machine related. As in : maybe your acceleration setting, or machine structure, or stepper driver configuration etc, is such that your machine can't handle the stress caused by this erratic behavior ( which for any given setup would likely be amongst the worse possible cases ). My point being here, you can't really answer "what happens when it goes too fast" with "missed steps" as a general answer. Though it's possible it's true on a given machine, it's likely not going to be the most useful answer to a user you don't know anything about.


---
**LightBurn Software** *January 18, 2019 23:46*

It’s not what I’ve observed. On machines set with very conservative acceleration, if I try to exceed the GCode limit, it looks like instructions get dropped occasionally.


---
**Arthur Wolf** *January 19, 2019 00:11*

That's not something I've seen reported so far, we take this sort of thing very seriously. Would you mind trying to find some reliable way to reproduce the problem, and open an issue on Smoothie's github reposiory please ?


---
**LightBurn Software** *January 19, 2019 00:17*

I’m doing something similar to “Fast-Stream.py” in LightBurn, but also tracking the Smoothie RX buffer size, and making sure that I never send more than the RX buffer can hold, but not using (to my knowledge) hardware handshaking. I can double check to be sure, as I had enabled that as well at some point but found it made no difference.  I should be able to reproduce easily.


---
**Wolfmanjm** *January 19, 2019 22:56*

+LightBurn Software -  given that smoothie does not have an rx buffer as such it would be hard to track it. That was a grbl hack, and is not relevant to smoothie. This is true over USB serial and uart serial. USB serial has its own flow control that the USB drivers handle which is why fast-stream even works.




---
**LightBurn Software** *January 19, 2019 23:14*

Well, if characters or packets were being dropped I’d expect Smoothie to complain about invalid or partial commands, yet that has never happened. That implies it’s not a data loss issue between LightBurn and Smoo.  LightBurn sends no more than X characters ahead of what’s been ack’d so far, and X is set based on the firmware I’m talking to. For Smoothie, it’s 248 chars.


---
**Wolfmanjm** *January 20, 2019 00:33*

248 is totally arbitrary and somewhat irrelevant. you can just stream at full speed, the USB flow control takes care of the rest. There can be no data loss as USB also does retry and checksumming (similar to TCP/IP). You do have to make sure you read the return data though as if that backs up it will cause deadlock in the host o/s driver system.

For anything other than streaming raster though it is safer to use good old ping pong protocol.


---
**LightBurn Software** *January 20, 2019 00:36*

I do read all return data, there is no backlog, and the exact same streaming code works perfectly with GRBL-LPC, GRBL, and Marlin. So, if there can be no data loss, why does it skew if I push too hard?


---
**Wolfmanjm** *January 20, 2019 00:47*

Like Arthur I have never heard of that happening, I use it all the time and push hard and never had that happen, the worst that can happen with high dpi is the planning buffer dries up so it can't plan ahead so it cannot reach full speed, and gets a little jerky starting and stopping as it waits for more data.


---
**Chuck Comito** *January 20, 2019 01:04*

Is there a benchmark to what we all feel is fast or slow as far as performance of the machine is? I don't have any reference. I only know Smoothie for laser. My cnc mill and 3d printer run it also but they are very slow by comparison.

I'm not even sure what speed I top out on my machine. I can only say that the print speed doesn't seem to change after about 60-80mm/sec.


---
**gionata cannavo** *January 20, 2019 08:04*

Hello, I can confirm that using the firmware smoothie engraving images in either newspaper mode, dither or jarvis you always have consecutive blocks going over 80 / 100mms (254 dpi).

With lpc you can push up to a maximum of 230mms, and then have the same problem of slowing down.

Slowing down of the movement and not to be confused with loss of steps.

I'm using lightburn.

It's a pity because I much prefer the original smoothie.


---
**Wolfmanjm** *January 20, 2019 10:20*

**+Chuck Comito** my delta/smoothie can move at 500mm/sec, however the maximum one can actually print at is around 100mm/sec due to extruder limitations etc. My Laser (K40) maxes out moves at 100mm/sec anything over that it will skip and the motors stall. So as 100mm/s is the maximum it can reliably move and I can stream 0.1mm dots at 100mm/sec it has never been an issue for me. The only real smoothie limitation is the 100KHz maximum step rate, so long as you don't max that out it will issue steps reliably. The actual feedrate depends on your steps/mm, Smoothie won't let you set a feedrate that would exceed the 100KHz step rate.


---
**gionata cannavo** *January 20, 2019 10:52*

**+Wolfmanjm**  I also use it with the k40, I chose the moothie just to solve the problem of speed (jerky movements) that wax on marlin or mk4duo.

The steps set are 160, the problem is that to make a 240 dpi 240x320 mm raster takes about 80 minutes (210mms).

Having taken a smoothie to use grbl lpc saddens me a lot, when you could save money using other cards.


---
**Chuck Comito** *January 21, 2019 20:50*

**+Wolfmanjm**, not that I know anything about this sort of thing but when rastering, does the controller try to fire the laser to a "single" dot, move to the next "single" dot and fire again? In other words, Smoothie is not trying to accelerate and decelerate after every command during the raster process is it? I would think this would be ignored is some way unless coming to the end of it's row (so to speak). So about my terminology. LOL


---
**Arthur Wolf** *January 21, 2019 20:55*

**+Chuck Comito** Raster engraving is straight moves at constant speeds, there is no reason to decelerate/accelerate ( except of course beginning/end of lines )


---
**Wolfmanjm** *January 21, 2019 21:24*

unless the comms can't keep up, in which case the planning buffer dries up and it will be jerky.




---
**Chuck Comito** *January 22, 2019 00:46*

I haven't had any issues with it being jerky at all. Just the occasional skew like missed steps on the x axis (the entire image will shift). I don't believe the skew is hardware related as I've changed it out a few times over and it's only when attempting to run fast. This might be what **+LightBurn Software** was referring to.

**+Arthur Wolf**, this is what I suspected. I couldn't imagine that not being the case..  And this article explains it nicely: [http://smoothieware.org/queue-refactor](http://smoothieware.org/queue-refactor). 

[smoothieware.org - queue-refactor [Smoothieware]](http://smoothieware.org/queue-refactor)


---
**Arthur Wolf** *January 22, 2019 08:19*

**+Chuck Comito** I have no idea why you mention the queue refactor stuff, I can't see how it could relate in any way to this, it's just about freeing up a bit of RAM ( that's getting sparse on v1 ).



"It's only when attempting to run fast" sounds a lot like it's hardware related.



Also, " I don't believe it's hardware related " is something I hear very often, and " Well actually it was hardware related " is something I hear nearly as often. People tend to underestimate how likely it is to be hardware related. It's not <b>always</b>, but thinking it's not when it is, is very very common. What you've described so far sounds all the way around like other cases where it was.



Technically, if there was a problem with how **+LightBurn Software** sends the Gcode ( which reading here it sounds like this might be the case ), it could cause performance issues, which in turn could cause : 

* Coms errors ( which I think **+LightBurn Software** says they suspect is happening but have not properly tested for it ), which I don't think would result in skew, just weird anomalies in the engraving/cut ( going where it's not supposed to )

* Underperforming coms speeds, which could cause miniature jerks, which in turn combined with a hardware issue could cause skew. I think this is the most likely explanation for what's going on here : lightburn isn't sending the code in a way as optimized as Octoprint or Pronterface does, this causes weird issues with the buffer which in turn cause micro pauses/slow-downs, which in turn cause speed curves that don't look like the typical use you know your machine to be able to handle, in turn resulting in skew.






---
**LightBurn Software** *January 22, 2019 09:17*

“LightBurn isn’t sending as fast as....”  It should be - I do not wait for OK’s from the controller - not directly. I have a “push-ahead” count that is how many bytes I’m allowed to send the controller before I start worrying about OK’s. For Smoothie, it’s 248. So your RX buffer should always have commands in it. I experimented with a number of different methods and this was both the fastest and most reliable. It meets or beats the speed of fast-stream.py on Windows.


---
**LightBurn Software** *January 22, 2019 09:19*

Having said that, I should just run a couple jobs, one from Fast-Stream and one from LightBurn, to see if they both exhibit the same behavior.


---
**Wolfmanjm** *January 22, 2019 09:55*

**+LightBurn Software** as 

 said before there is NO RX buffer in smoothie. you should eliminate the 248 character count as it is bogus. and just consume oks as they come in.




---
**Wolfmanjm** *January 22, 2019 09:57*

skew or what I presume is missed steps is nearly always a h/w issue. especially on k40 where the motors are very weak and will skip steps at the slightest opportunity. i don't see anything in the smoothie code that could cause missed steps unless you are running up against the 100khz steprate limit, which is unlikely 


---
**Chuck Comito** *January 22, 2019 12:09*

**+Arthur Wolf**, the only reason I mentioned that is because I thought it might be what was mentioned earlier in this discussion (as I've said before, this is not my area of expertise). Anyway, I understand the "hardware related" argument and agree that it can go both ways. My reason of saying that is because I've had numerous hardware setups that behave the same way at higher speeds. **+Wolfmanjm**, I would totally agree with you with the exception that although I call my machine a K40, it is the farthest from a stock situation. As a matter of fact there isn't a single K40 part left on my setup. **+LightBurn Software**, if there is any way I can help with testing I would be happy to do so and for that matter if any of you need any testing help I would be happy to help. I'd like to see Smoothie (V1) be as good as possible.


---
**LightBurn Software** *January 22, 2019 16:52*

**+Wolfmanjm** I heard you, but Smoothie doesn’t stop until all the buffers are empty, so I want to limit how far ahead I get. I’m actually planning to vary that number based on the length of the G moves being submitted. Implement instant stop and I’ll take it out. :)


---
**gionata cannavo** *January 22, 2019 17:31*

**+LightBurn Software** If it can serve some tests, of my full availability.


---
**Wolfmanjm** *January 22, 2019 18:14*

**+LightBurn Software** This is true there is a trade off between faster comms and less control over pause. Unfortunately instant pause turned out to not be easy at all, and I have no idea when/if I will be able to implement it.


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/BRmGFZqgTpM) &mdash; content and formatting may not be reliable*
