---
layout: post
title: "Having an issue with my Smoothieboard 5XC"
date: January 22, 2019 13:11
category: "General discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Having an issue with my Smoothieboard 5XC.



I've recently had a chance to actually plug it in, after over a year of it sitting without being used.



I was originally having issues with the 24v power supply, which I've now resolved.



However now, I'm plugging in USB cable & unable to get any LED lights turning on or connection to the laptop. Also, tried plugging 5V into it (with the logic controller) and again no LED lights indicating anything going on.



It was working perfectly fine last time I had it running for tests/configuration.



I've checked with multimeter at the two pins behind the 5V input and there is power feeding into the board from the USB, but only 1.6V when powered by the USB. When powered by the 5V power supply, there is 4.8-5V at that same point.



Also, there is 4.8-5V (both on USB/5V PSU) at the endstop pins. So power seems to be getting through this part of the board at least.



Is there somewhere else I should be checking with multimeter to determine if something is dead?



![missing image](https://lh3.googleusercontent.com/-pxSAs3fHPEg/XEcWncyIrYI/AAAAAAAAK3A/EBM1U_-JJqoDag8Cke-RJ2EZXLsQZWQFQCJoC/s0/50227049_2266924743360011_3580635447795122176_n.jpg)
![missing image](https://lh3.googleusercontent.com/-qXTrv_yQ_zA/XEcWnQwjlTI/AAAAAAAAK3A/qLkc-SyhP-YpjwhLaRfUl4QpOms0rMP1ACJoC/s0/50314683_549160095592156_1900838197475147776_n.jpg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2019 13:12*

Sorry, accidentally posted in General. Can you move to HELP please? Thanks


---
**Arthur Wolf** *January 22, 2019 13:18*

Please check 3.3v


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2019 13:19*

**+Arthur Wolf** Where exactly?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2019 13:20*

**+Arthur Wolf** Actually, I found where


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2019 13:29*

Tested at these points. On VBB & GND shows 0.04V. On 3.3V & GND shows 0.00V. **+Arthur Wolf**

![missing image](https://lh3.googleusercontent.com/phRmpaIkPGCEYzemSlXs5ZbZoPY81PWn_IIFuusLJik9oB6CHTCysyuSHOrl6ncgzjKYOLYWq5ZOjw=s0)


---
**Arthur Wolf** *January 22, 2019 13:40*

Sounds like the board is dead. You possibly have to change the 3.3v vreg and/or the microcontroller. Are you sure nothing happened to it ? Was it stored with wires attached to it ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2019 13:45*

It was working with the 5V/3.3V earlier today when I was having issues with the 24V. Once I fixed the 24V issue, which I believe was just a loose connection, it no longer works on the 5V/3.3V. Could it possibly be the RECOM R-78E-0.5 that's gone & causing issue? Or the USB/5V PSU should not have issue with that?


---
**Arthur Wolf** *January 22, 2019 13:52*

it could be the recom.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 22, 2019 13:53*

**+Arthur Wolf** Cheers for the speedy help. I'll order another & give it a go when it arrives.


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2019 16:03*

What about plugging in the USB cable? Any lights that way? 

Check for shorts between Gnd and 3.3v, Gnd and 5v, and Gnd and Vin. 


---
**Douglas Pearless** *January 22, 2019 23:31*

I’d recommend Rays recommendation but add to unplug all cables and try just the USB.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/2UtJHTzUMKP) &mdash; content and formatting may not be reliable*
