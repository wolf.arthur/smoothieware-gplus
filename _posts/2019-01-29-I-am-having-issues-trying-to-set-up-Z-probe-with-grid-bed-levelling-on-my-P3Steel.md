---
layout: post
title: "I am having issues trying to set up Z-probe with grid bed levelling on my P3Steel"
date: January 29, 2019 12:52
category: "Help"
author: "Nils Martens"
---
I am having issues trying to set up Z-probe with grid bed levelling on my P3Steel. I have a BLtouch probe that is offset from the nozzle in such a way that the probe can't reach either X0 or Y0 (as it needs the head to move past the endstops). To circumvent this I want to move the endstops to max but first want to try out if it works by removing the endstops and manually setting the nozzle position.



I've moved the nozzle to X200Y200Z10 manually and then set the position accordingly with G92 X200Y200Z10. I then issue G1 command to move to center of the bed (G1 X100Y100), extend the probe (M280 S3) and probe the bed with G29 Z0.7. All is well up to this point.



However, when I then issue G32, the probe goes to X200Y200 and probes the bed thinking it's at X0Y0. Of course after it's done probing it goes crashing into the end of travel for my printer because it's going for X200+(200/7) Y200. I have absolutely no clue why it would go to X200Y200 thinking it's at X0Y0. Anyone have an idea why? My config file is pasted here: [https://pastebin.com/y8VD48w8](https://pastebin.com/y8VD48w8)





**"Nils Martens"**

---
---
**Arthur Wolf** *January 29, 2019 13:11*

Use M114 to check position before/after the problem.


---
**Wolfmanjm** *January 29, 2019 16:02*

G92 has no effect on grid probing as it uses machine coordinates not WCS. If it were to use WCS it would defeat the entire purpose.


---
**Wolfmanjm** *January 29, 2019 17:29*

If you want to relocate the origin you must do it with homing and setting the homing offsets (which can be done in the config or using M206) This sets the machine coordinate system. Smoothie does have a way to manually set the homing location if you do not have endstops enabled. It is described in the wiki.


---
**Nils Martens** *January 29, 2019 21:04*

**+Wolfmanjm** Thanks, I don't quite understand why bed levelling is not dependant on G92 but homing instead, but I appreciate the info. I'll look into it.


---
**Wolfmanjm** *January 30, 2019 10:47*

[wiki.linuxcnc.org - LinuxCNC Documentation Wiki: CoordinateSystems](http://wiki.linuxcnc.org/cgi-bin/wiki.pl?CoordinateSystems) may explain it a bit


---
*Imported from [Google+](https://plus.google.com/110435737466765899130/posts/9PkBZPuRmzx) &mdash; content and formatting may not be reliable*
