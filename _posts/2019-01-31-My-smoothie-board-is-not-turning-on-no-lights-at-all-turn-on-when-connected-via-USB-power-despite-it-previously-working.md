---
layout: post
title: "My smoothie board is not turning on (no lights at all turn on) when connected via USB power despite it previously working"
date: January 31, 2019 00:43
category: "General discussion"
author: "Peter Hall"
---
My smoothie board is not turning on (no lights at all turn on) when connected via USB power despite it previously working. I've checked it both by plugging it into my computer like I usually do as well as plugging it into a 5v adapter coming directly from the power strip. This problem arises regardless of if the power supply is connected to power.



When plugged into my computer I get a message saying "Power surge on the USB port – Unknown USB Device needs more power than the port can supply." 



I've never had this problem before, so any help is appreciated. Also I can provide more details if necessary.





**"Peter Hall"**

---
---
**Douglas Pearless** *January 31, 2019 02:41*

This indicates something is wrong with the 5V rails on the Smoothie and this could be because of a short somewhere or similar; I would suggest you completely unplug your smoothie from everything and ONLY plug in the USB cable, if this still occurs, look for shorts on the board, or unfortunately the board may've failed.


---
**Peter Hall** *January 31, 2019 03:22*

**+Douglas Pearless** I have checked it with only USB, and still no progress. I'll go look for shorts.


---
*Imported from [Google+](https://plus.google.com/115249928988980868136/posts/Ka2ogCS1CEL) &mdash; content and formatting may not be reliable*
