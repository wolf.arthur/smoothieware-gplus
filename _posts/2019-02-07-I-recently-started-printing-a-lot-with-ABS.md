---
layout: post
title: "I recently started printing a lot with ABS"
date: February 07, 2019 21:19
category: "General discussion"
author: "Tim Spracher"
---
I recently started printing a lot with ABS. My bed temp is 110C and I have burnt 3 connectors. I am using 14 gauge wire. I have tried tightening all connections prior to starting the print and the prints usually runs about 10hrs. My unit sits in a closed container which runs about 103deg during printing.



How or can I hook up and external Mosfet to this board. I am currently using P2.5 for the Heated Bed and P2.7 for the Hot head.



Or



Should I hook the heated bed to P1.23 with a separate power supply.



My Hot head shows no signs of ware and it is set at 215C



any thoughts



Tim





**"Tim Spracher"**

---
---
**Jonathon Thrumble** *February 07, 2019 22:06*

Same connectors the heated bed uses now, just run the power directly to the mosfet. 


---
**Arthur Wolf** *February 07, 2019 22:58*

You shouldn't be burning connectors, there's a problem to solve there first. What is the current rating of the bed ? What do you have connected to each/all of the mosfet, and what is the current rating for each ( don't omit any ) ?



Also what runs at 103degs ? Are you saying the Smoothieboard is in a 103deg environment ? If so, that's your problem, if not what temp does the Smoothieboard run in ?




---
**Marko Novak** *February 08, 2019 13:02*

You left one crucial thing out... What voltage are you running?

Running it on 12v with >120w bed is begging for problems in hot environment...



There's same issue with geeetech g2/s delta printer... And can be traced true gt2560 board revisions. Instead of paring it with 24v supply.


---
**Jeff DeMaagd** *February 09, 2019 10:42*

Enclosure is 103 degrees F or C? Either way I hope your electronics aren’t in that.


---
*Imported from [Google+](https://plus.google.com/109526178556206480319/posts/DynndHB2vXE) &mdash; content and formatting may not be reliable*
