---
layout: post
title: "Somebody is proposing this change to the GLCD watch screen for Smoothie, like it ?"
date: February 08, 2019 08:06
category: "General discussion"
author: "Arthur Wolf"
---
Somebody is proposing this change to the GLCD watch screen for Smoothie, like it ?

![missing image](https://lh3.googleusercontent.com/-UUmX12Rzlek/XF04oofdG7I/AAAAAAAAQ3M/CD9ff5MZ3YAvRYRTFxWdP1IMKx6CVJMyACJoC/s0/KQ84uaw.jpg)



**"Arthur Wolf"**

---
---
**Douglas Pearless** *February 08, 2019 08:17*

Very nice


---
**Roberto Fernandez** *February 08, 2019 12:32*

And for CNC??


---
**ThantiK** *February 08, 2019 14:24*

Very much so.


---
**Griffin Paquette** *February 08, 2019 19:45*

Looks good


---
**Jeff DeMaagd** *February 09, 2019 10:44*

Yes please 


---
**Claudio Prezzi** *February 09, 2019 12:04*

Very nice, but I would align the X,Y,Z values to the right, so the decimal points are below each other.


---
**Shai Schechter** *February 12, 2019 03:32*

Yes, much nicer. Would be even cooler if we can use some online tool to edit the layout to our own liking and share it. 


---
**Arthur Wolf** *February 12, 2019 09:04*

**+Shai Schechter** «Nice lemonade stand kids, it'd be cool if was a multi-billion dollar franchise » :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/BWDswhP4s64) &mdash; content and formatting may not be reliable*
