---
layout: post
title: "Does Soothie use CAN peripheral pins of LPC1769 for any other purposes, or are they left unused???"
date: February 15, 2019 05:45
category: "General discussion"
author: "Athul S Nair"
---
Does Soothie use CAN peripheral pins of LPC1769 for any other purposes, or are they left unused??? 





**"Athul S Nair"**

---
---
**Arthur Wolf** *February 15, 2019 10:52*

Which pins are you concerned about precisely ?


---
**Athul S Nair** *February 15, 2019 16:04*

As per user manual of LPC176x P0.0 - P0.1 & P0.4-P0.5


---
**Arthur Wolf** *February 15, 2019 17:41*

All used. [smoothieware.org - lpc1769-pin-usage [Smoothieware]](http://smoothieware.org/lpc1769-pin-usage)




---
**Arthur Wolf** *February 15, 2019 17:42*

( note you can disable them in config and use them for something else if you want, I'm just talking aboutw hat they are hardwired for on the smoothieboard here. there's no CAN code in smoothieware though )


---
**Gary Tolley - Grogyan** *February 15, 2019 20:44*

That is disappointing 


---
**Arthur Wolf** *February 15, 2019 20:50*

Well, we've got tens of thousands of boards in the wild, 7 years of development, and you are literally the first person to ask for CAN pins, so you'd have to admit you have a pretty niche request ... What do you want to use the CAN pins for ?

Also, you <b>can</b> use the pins, it just depends what board and what features you have/are ready to use/not use.

Smoothieboard v2 plans on using CAN for some things and the pins will be more accessible there.


---
**Gary Tolley - Grogyan** *February 15, 2019 21:00*

I have been asking for CAN support in the other Firmware, Marlin.

Due to inherent EMI noise.

With the majority of (mostly 8bit admittedly) printers with remote displays.



CAN brings more reliability,  expandability, and robustness of communications.




---
**Arthur Wolf** *February 15, 2019 21:04*

**+Gary Tolley - Grogyan** Noise on remote displays isn't a very common problem. It'd start to happen if you had longer wire lengths, but most setup have <30cm lengths on the market right now, and at those lengths SPI works fine for displays.

Smoothieboard v2 will support both CAN <b>and</b> SPI/Serial over differential lines.



[https://docs.google.com/document/d/144EbmhN6z-J2V_Zw7GfJpZrfD-B0dC3cuea9oWPgxNM/edit#](https://docs.google.com/document/d/144EbmhN6z-J2V_Zw7GfJpZrfD-B0dC3cuea9oWPgxNM/edit#)


---
**Athul S Nair** *February 16, 2019 11:26*

I have one spare smoothieboard, So I though I would try to learn programming LPC1769, isn't that possible??.( may be in distant future I would write my own firmware) and I wanna learn CAN. I have also seen a 3D printer which use "Integrated, Dual CAN-bus architecture" (printer name: Hyrel System 30M). Then I wondered what a CAN bus doing in 3D printer. ?? Is it used for display???


---
**Arthur Wolf** *February 16, 2019 11:29*

**+Athul S Nair**



> So I though I would try to learn programming LPC1769, isn't that possible??.



Smoothie is open-source so of course it's possible.

Read [smoothieware.org - start [Smoothieware]](http://smoothieware.org) there is <b>a lot</b> of information there.



> Then I wondered what a CAN bus doing in 3D printer. ?? 



It's not open-source, so I have no idea how to find the information. I would guess it's only available to you if you pay them ...




---
*Imported from [Google+](https://plus.google.com/114978037514571097180/posts/WFrVLHNU7LM) &mdash; content and formatting may not be reliable*
