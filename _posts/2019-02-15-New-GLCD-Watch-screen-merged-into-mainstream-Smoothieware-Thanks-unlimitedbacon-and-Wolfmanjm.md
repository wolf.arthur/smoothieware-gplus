---
layout: post
title: "New GLCD Watch screen, merged into mainstream Smoothieware : Thanks unlimitedbacon and Wolfmanjm"
date: February 15, 2019 21:06
category: "Development"
author: "Arthur Wolf"
---
New GLCD Watch screen, merged into mainstream Smoothieware : [https://github.com/Smoothieware/Smoothieware/pull/1365#issuecomment-464196644](https://github.com/Smoothieware/Smoothieware/pull/1365#issuecomment-464196644)



Thanks unlimitedbacon and **+Wolfmanjm**

![missing image](https://lh3.googleusercontent.com/-ni89n5LiVLE/XGcpuu6MRnI/AAAAAAAAQ50/4tOj-J19sVczIB7rTF4M-nx6G4deLsG4wCJoC/s0/68747470733a2f2f692e696d6775722e636f6d2f394474543273712e6a7067.jpeg)



**"Arthur Wolf"**

---
---
**Wolfmanjm** *February 15, 2019 21:08*

Not merged yet :)


---
**Arthur Wolf** *February 15, 2019 21:15*

//Insert inappropriate PE meme//


---
**Arthur Wolf** *February 15, 2019 21:42*

And it's merged, I can predict the future :)


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/iFmQ5rockPs) &mdash; content and formatting may not be reliable*
